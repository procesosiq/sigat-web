<?php
	/**
	*  CLASS FROM PRODUCTOS
	*/
	class Productos 
	{
		private $conexion;
		private $session;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		public function index(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();

			$sql = "SELECT 
				(SELECT nombre FROM cat_formuladoras WHERE id = id_formuladora) AS formuladora, 
				(SELECT nombre FROM cat_proveedores WHERE id = id_proveedor) AS proveedor, 
				(SELECT nombre FROM cat_tipo_productos WHERE id = id_tipo_producto) AS tipo_producto, 
				nombre_comercial AS nombreComercial,
				ingrediente_activo AS ingrediente_activo,
				frac AS frac,
				accion AS action,
				id AS id_producto,
				id AS codigo
				FROM products";
			$res = $this->conexion->link->query($sql);
			$datos [] ;
			while($fila = $res->fetch_assoc()){
				$datos = (object)$fila;
			}

			return json_encode($datos);
		}

		public function params(){
			$data = (object)json_decode(file_get_contents("php://input"));
			$data->id_producto = (int)$data->id_producto;
			return $data;
		}

		public function create(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();
			if($data->nombreComercial != ""){
				$sql = "INSERT INTO `products` SET
						id_formuladora = '{$data->formuladora}',
						id_proveedor = '{$data->proveedor}',
						id_tipo_producto = '{$data->tipo_producto}',
						nombre_comercial = '{$data->nombreComercial}',
						ingrediente_activo = '{$data->ingrediente_activo}',
						frac = '{$data->frac}',
						accion = '{$data->action}'";
				$ids = (int)$this->conexion->Consultas(1,$sql);
				if($ids > 0){
					$response->success = 200;
					$response->data = $ids;
				}
			}

			return json_encode($response);
		}

		public function update(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();
			if($data->id_producto > 0){
				$sql = "UPDATE `products` SET
						id_formuladora = '{$data->formuladora}',
						id_proveedor = '{$data->proveedor}',
						id_tipo_producto = '{$data->tipo_producto}',
						nombre_comercial = '{$data->nombreComercial}',
						ingrediente_activo = '{$data->ingrediente_activo}',
						frac = '{$data->frac}',
						accion = '{$data->action}'
						WHERE id = $data->id_producto";
				$this->conexion->Consultas(1,$sql);
				if($data->id_producto > 0){
					$response->success = 200;
					$response->data = $data->id_producto;
				}
			}
			return json_encode($response);
		}

		public function show(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();
			if($data->id_producto > 0){
				$sql = "SELECT 
						id_formuladora AS formuladora, 
						id_proveedor AS proveedor, 
						id_tipo_producto AS tipo_producto, 
						nombre_comercial AS nombreComercial,
						ingrediente_activo AS ingrediente_activo,
						frac AS frac,
						accion AS action,
						id AS id_producto,
						id AS codigo
					FROM products
					WHERE id = $data->id_producto";
				$res = $this->conexion->link->query($sql);
				$response->success = 200;
				$response->data = $res->fetch_assoc();
			}
			return json_encode($response);
		}

		public function changeStatus(){
			extract($_POST);
			if((int)$id > 0 ){
				$status = ($estado == 'activo') ? 1 : 0;
				$sql = "UPDATE cat_gerentes SET status={$status} WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				// echo $sql;
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		public function getFormuladoras(){
			$formuladoras = [];
			$sql = "SELECT id , nombre FROM cat_formuladoras WHERE status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$formuladoras[$fila['id']] = $fila['nombre'];
			}

			return json_encode($formuladoras);
		}

        public function getProveedores(){
			$proveedores = [];
			$sql = "SELECT id , nombre FROM cat_proveedores WHERE status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$proveedores[$fila['id']] = $fila['nombre'];
			}

			return json_encode($proveedores);
		}

        public function getTipoProductos(){
			$tipo_productos = [];
			$sql = "SELECT id , nombre FROM cat_tipo_productos WHERE status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$tipo_productos[$fila['id']] = $fila['nombre'];
			}

			return json_encode($tipo_productos);
		}

		public function getFrac(){
			$frac = [];
			$sql = "SELECT frac AS id , frac AS nombre FROM products WHERE frac != '' GROUP BY frac";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$frac[$fila['id']] = $fila['nombre'];
			}

			return json_encode($frac);
		}

		public function getAccion(){
			$accion = [];
			$sql = "SELECT accion AS id , accion AS nombre FROM products WHERE accion != '' GROUP BY accion";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$accion[$fila['id']] = $fila['nombre'];
			}

			return json_encode($accion);
		}

		private function getFincas($id_gerente){
			$selected = "";
			if($id_gerente){
				$selected = ", IF(id_gerente = '{$id_gerente}' , 'selected' , '') AS selected";
			}
			$cliente = [];
			$sql = "SELECT cat_fincas.id , cat_fincas.nombre  , 
					IF(id_gerente > 0 OR id_gerente != '' , 'Fincas con Gerentes' , 'Fincas sin Gerentes') AS grupo
					,id_gerente  {$selected}
					FROM cat_fincas 
					LEFT JOIN cat_gerentes ON cat_fincas.id_gerente = cat_gerentes.id
					WHERE cat_fincas.status > 0 
					ORDER BY id_gerente , nombre";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$cliente[] = (object)$fila;
			}

			return $cliente;
		}
	}
?>
