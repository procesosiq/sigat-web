<?php

    function D($data){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

    class Controller {

        public function __construct()
		{
			include '../controllers/conexion.php';
            $this->db = new M_Conexion('sigat');
        }
        
        public function init(){
            D("Inicio");
            $data = $this->getData();
            
            foreach($data as $row){
                $sql_main = $this->getSQLMain($row);
                D($sql_main);
            }
        }

        private function getSQLMain($row){
            $finca = $this->db->queryOne("SELECT nombre FROM cat_fincas WHERE id = $row->id_hacienda");
            $fumigadora = $this->db->queryOne("SELECT nombre FROM cat_fumigadoras WHERE id = $row->id_fumigadora");
            $piloto = $this->db->queryOne("SELECT nombre FROM cat_pilotos WHERE id = $row->id_piloto");
            $placa = $this->db->queryOne("SELECT nombre FROM cat_placas WHERE id = $row->id_placa_avion");

            $sql = "INSERT INTO cycle_application_reporte SET
                        id_finca = '{$row->id_hacienda}',
                        finca = '{$finca}',
                        fecha_real = '{$row->fecha}',
                        fecha_programada = '{$row->fecha_programada}',
                        hora = '{$row->hora}',
                        hectareas_fumigacion = '{$row->hectareas_fumigacion}',
                        notificacion = '{$row->notificacion}',
                        programa = '{$row->programa}',
                        tipo_ciclo = '{$row->tipo_ciclo}',
                        num_ciclo = '{$row->num_ciclo}',
                        id_fumigadora = '{$row->fumigadora}',
                        fumigadora = '{$row->fumigadora}',
                        precio_operacion = '{$row->ha_oper}',
                        id_piloto = '{$row->id_piloto}',
                        piloto = '{$piloto}',
                        id_placa = '{$row->id_placa_avion}',
                        placa_avion = '{$placa}',
                        hora_salida = '{$row->hora_salida}',
                        hora_llegada = '{$row->hora_llegada}',
                        motivo = '{$row->motivo}',
                        json  = '{$row->json}'";
            return $sql;
        }

        private function getData(){
            return $this->db->queryAll("SELECT 
                    GROUP_CONCAT(id SEPARATOR ',') AS ids,
                    id_hacienda,
                    proveedor,
                    motivo,
                    fecha, 
                    fecha_programada,
                    hora,
                    GROUP_CONCAT(sector SEPARATOR ',') AS sectores,
                    SUM(hectareas_fumigacion) AS hectareas_fumigacion,
                    notificacion,
                    programa,
                    tipo_ciclo,
                    num_ciclo,
                    id_fumigadora,
                    fumigadora,
                    ha_oper,
                    id_piloto,
                    id_placa_avion,
                    hora_salida,
                    hora_llegada,
                    json
                FROM cycle_application
                WHERE id > 9
                GROUP BY json");
        }
    }
    $control = new Controller();
    $control->init();
?>