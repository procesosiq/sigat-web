<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

include '../controllers/conexion.php';

/** Directorio de los JSON */
$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
            // D("Path Files ".$object->getPathName());
            // D("Path route Iterator ".$object->getPath());
            // D("Path origen ".$path);
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        $pos1 = strpos($object->getPath(), "/new");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        process_json($json, $object->getFileName());
                        move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        move_image($object->getPathName(), $object->getFileName());
                        break;

                    default:
                        
                        break;
                }
            }
        }
    }
}

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/old/$nameFile")){
        echo 'error';
    }
    /*if(!rename($file, __DIR__."/nuevo/semanas3/$nameFile")){
        echo 'error';
    }*/
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/img/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename){
    $conexion = new M_Conexion;

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';;
    $user_username = $user['username'].'';;
    $referenceNumber = $json["referenceNumber"];
    $pages = $json['pages'];

    $geoStamp = $json["geoStamp"];
    $coordinates = $geoStamp["coordinates"];
    $latitude = $coordinates["latitude"];
    $longitude = $coordinates["longitude"];

    $foliar = 0;
    $tres_metros = false;
    $cero_semanas = false;
    $once_semanas = false; 

    $Encabezado = [];
    $Fungicidas = [];
    $Productos = [];
    $Obseraviones = [];

    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = strtoupper(trim(limpiar($page_data['name'])));
        $answers = $page_data["answers"];
        $mode = "";
        $type = "";
        foreach($answers as $x => $answer){

            $question = strtoupper(trim(limpiar($answer['question'])));
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            // $question = $answer['question'];
            $values   = $answer['values'];

            $labelita = limpiar(trim(strtolower($label)));
            if($pagina_nombre == "ENCABEZADO"){
                if($question != "GEOLOCALIZACION"){
                    if($question == "SECTOR"){
                        if(is_array($values) && count($values) > 0){
                            $Encabezado[$question] = "'".implode("','",$values)."'";
                        }
                    }
                    elseif($question == "HORA" || $question == "HORA DE SALIDA" || $question == "HORA DE LLEGADA"){
                        $hora = ["00:00:00"];
                        if(isset($values[0]["shifted"])){
                            $hora = explode("-", $values[0]["shifted"]);
                            $Encabezado[$question] = $hora;
                        }
                    }else{
                        $Encabezado[$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    } 
                }  

                if($question == "GEOLOCALIZACION"){
                    if(isset($values[0])){
                        if($values[0]["coordinates"] != null){
                            $Encabezado[$question] = array("muestra"=>$pagina_nombre, "lat"=>$values[0]["coordinates"]["latitude"], "lng"=>$values[0]["coordinates"]["longitude"]);
                        }
                    }
                }
            }

            if($pagina_nombre == "FUNGICIDAS"){
                if($question == "GEOLOCALIZACIÓN"){
                    if(isset($values[0])){
                        if($values[0]["coordinates"] != null){
                            $Fungicidas[$question] = array("muestra"=>$pagina_nombre, "lat"=>$values[0]["coordinates"]["latitude"], "lng"=>$values[0]["coordinates"]["longitude"]);
                        }
                    }
                }else{
                    if($question == "FUNGICIDA 1"){
                        $mode = $question;
                        // D($mode);
                        $Fungicidas[$mode] = [];
                        $Fungicidas[$mode][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    }
                    if($question == "FUNGICIDA 2"){
                        $mode = $question;
                        $Fungicidas[$mode] = [];
                        $Fungicidas[$mode][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    }
                    /*----------  Detalle  ----------*/
                    if($question == "DOSIS"){
                        $Fungicidas[$mode][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    } 
                    if($question == "CANTIDAD"){
                        $Fungicidas[$mode][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    /*----------  Detalle  ----------*/
                    }
                    /*----------  FOTOS  ----------*/
                    if($question == "CAPTURA DE FOTOS"){
                        $Fungicidas["fotos"] = "'".(isset($values[0]["filename"]) ? "ciclos/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '')."'";
                    }
                }
            }

            if($pagina_nombre == "OTROS PRODUCTOS"){
                if($question == "ACEITE"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "EMULSIFICANTE"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "AGUA"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "FOLIAR 1"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "FOLIAR 2"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "FOLIAR 3"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                /*----------  Detalle  ----------*/
                if($question == "DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "CANTIDAD"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                /*----------  Detalle  ----------*/

                /*----------  FOTOS  ----------*/
                if($question == "CAPTURA DE FOTOS"){
                    $Productos["fotos"] = "'".(isset($values[0]["filename"]) ? "ciclos/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '')."'";
                }
            }

            if($pagina_nombre == "OBSERVACIONES GENERALES"){
                if($question == "OBSERVACIONES GENERALES"){
                    $Obseraviones[$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                /*----------  FOTOS  ----------*/
                if($question == "CAPTURA DE FOTOS GENERALES"){
                    $Obseraviones["fotos"] = "'".(isset($values[0]["filename"]) ? "ciclos/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '')."'";
                }
            }
        }
    }


    D($Encabezado);
    D($Fungicidas);
    D($Productos);
    D($Obseraviones);
    $sql = "INSERT INTO cycle_application SET
                id_usuario = '',
                id_hacienda = {$Encabezado['FINCA - ID']},                
                fecha = {$Encabezado['FECHA']},
                hora = '{$Encabezado['HORA'][0]}',
                id_sector = '',
                sector = '". str_replace("'", "", $Encabezado['SECTOR']) ."',
                hectareas_fumigacion = {$Encabezado['HECTAREAS DE FUMIGACION']},
                notificacion = {$Encabezado['NOTIFICACION']},
                programa = {$Encabezado['PROGRAMA']},
                tipo_ciclo = {$Encabezado['TIPO DE CICLO']},
                num_ciclo = {$Encabezado['CICLO #']},
                id_fumigadora = {$Encabezado['FUMIGADORA - ID']},
                fumigadora = {$Encabezado['FUMIGADORA']},
                id_piloto = {$Encabezado['PILOTO - ID']},
                id_placa_avion = {$Encabezado['PILOTO - ID_PLACA']},
                placa_avion = {$Encabezado['PLACA AVION']},
                hora_salida = '{$Encabezado['HORA DE SALIDA'][0]}',
                hora_llegada = '{$Encabezado['HORA DE LLEGADA'][0]}',
                status = '',
                fungicida = {$Fungicidas['FUNGICIDA 1']['FUNGICIDA 1']},
                cantidad = {$Fungicidas['FUNGICIDA 1']['CANTIDAD']},
                dosis = {$Fungicidas['FUNGICIDA 1']['DOSIS']},
                fungicida_2 = {$Fungicidas['FUNGICIDA 2']['FUNGICIDA 2']},
                cantidad_2 = {$Fungicidas['FUNGICIDA 2']['CANTIDAD']},
                dosis_2 = {$Fungicidas['FUNGICIDA 2']['DOSIS']},
                agua = {$Productos['AGUA']['AGUA']},
                cantidad_agua = {$Productos['AGUA']['CANTIDAD']},
                dosis_agua = {$Productos['AGUA']['DOSIS']},
                id_aceite = '',
                aceite = {$Productos['ACEITE']['ACEITE']},
                cantidad_aceite = {$Productos['ACEITE']['CANTIDAD']},
                dosis_aceite = {$Productos['ACEITE']['DOSIS']},
                id_emulsificante = '',
                emulsificante = {$Productos['EMULSIFICANTE']['EMULSIFICANTE']},
                cantidad_emulsificante = {$Productos['EMULSIFICANTE']['CANTIDAD']},
                dosis_emulsificante = {$Productos['EMULSIFICANTE']['DOSIS']},
                id_foliar_1 = '',
                foliar_1 = {$Productos['FOLIAR 1']['FOLIAR 1']},
                cantidad_foliar_1 = {$Productos['FOLIAR 1']['CANTIDAD']},
                dosis_foliar_1 = {$Productos['FOLIAR 1']['DOSIS']},
                id_foliar_2 = '',
                foliar_2 = {$Productos['FOLIAR 2']['FOLIAR 2']},
                cantidad_foliar_2 = {$Productos['FOLIAR 2']['CANTIDAD']},
                dosis_foliar_2 = {$Productos['FOLIAR 2']['DOSIS']},
                id_foliar_3 = '',
                foliar_3 = {$Productos['FOLIAR 3']['FOLIAR 3']},
                cantidad_foliar_3 = {$Productos['FOLIAR 3']['CANTIDAD']},
                dosis_foliar_3 = {$Productos['FOLIAR 3']['DOSIS']},
                fotos_1 = {$Productos['fotos']},
                fotos_2 = {$Obseraviones['fotos']},
                json = '{$json["referenceNumber"]}.json'";
    D($sql);
    $id_geoposicion = $conexion->Consultas(1, $sql);
}

function D($arreglo){
    echo "<pre>";
        print_r($arreglo);
    echo "</pre>";
}

function limpiar($String){
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
     
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);
    return $String;
}

function strtolower_utf8($string){ 
  $convert_to = array( 
    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", 
    "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", 
    "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж", 
    "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", 
    "ь", "э", "ю", "я" 
  ); 
  $convert_from = array( 
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", 
    "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", 
    "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", 
    "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ", 
    "Ь", "Э", "Ю", "Я" 
  ); 

  return str_replace($convert_from, $convert_to, $string); 
}

?>