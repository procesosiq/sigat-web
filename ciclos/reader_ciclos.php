<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

include '../controllers/conexion.php';

/** Directorio de los JSON */
$path = realpath('./');

#$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
$objects = [];
foreach($objects as $name => $object){
            // D("Path Files ".$object->getPathName());
            // D("Path route Iterator ".$object->getPath());
            // D("Path origen ".$path);
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        $pos1 = strpos($object->getPath(), "/new");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        process_json($json, $object->getFileName());
                        #move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        move_image($object->getPathName(), $object->getFileName());
                        break;

                    default:
                        
                        break;
                }
            }
        }
    }
}

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/old/$nameFile")){
        echo 'error';
    }
    /*if(!rename($file, __DIR__."/nuevo/semanas3/$nameFile")){
        echo 'error';
    }*/
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/img/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename){
    $conexion = new M_Conexion;

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';;
    $user_username = $user['username'].'';;
    $referenceNumber = $json["referenceNumber"];
    $pages = $json['pages'];

    $geoStamp = $json["geoStamp"];
    $coordinates = $geoStamp["coordinates"];
    $latitude = $coordinates["latitude"];
    $longitude = $coordinates["longitude"];

    $foliar = 0;
    $tres_metros = false;
    $cero_semanas = false;
    $once_semanas = false; 

    $Encabezado = [];
    $Fungicidas = [];
    $Productos = [];
    $Obseraviones = [];

    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = strtoupper(trim(limpiar($page_data['name'])));
        $answers = $page_data["answers"];
        $mode = "";
        $type = "";
        foreach($answers as $x => $answer){

            $question = strtoupper(trim(limpiar($answer['question'])));
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            // $question = $answer['question'];
            $values   = $answer['values'];

            $labelita = limpiar(trim(strtolower($label)));
            if($pagina_nombre == "ENCABEZADO"){
                if($question != "GEOLOCALIZACION"){
                    if($question == "SECTOR"){
                        if(is_array($values) && count($values) > 0){
                            $Encabezado[$question] = "'".$values[0]."'";
                        }
                    }
                    if($question == "SECTOR 2"){
                        if(is_array($values) && count($values) > 0){
                            $Encabezado[$question] = "'".$values[0]."'";
                        }
                    }
                    elseif($question == "HORA" || $question == "HORA DE SALIDA" || $question == "HORA DE LLEGADA"){
                        $hora = ["00:00:00"];
                        if(isset($values[0]["shifted"])){
                            $hora = explode("-", $values[0]["shifted"]);
                            $Encabezado[$question] = $hora;
                        }
                    }else{
                        $Encabezado[$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    } 
                }  

                if($question == "GEOLOCALIZACION"){
                    if(isset($values[0])){
                        if($values[0]["coordinates"] != null){
                            $Encabezado[$question] = array("muestra"=>$pagina_nombre, "lat"=>$values[0]["coordinates"]["latitude"], "lng"=>$values[0]["coordinates"]["longitude"]);
                        }
                    }
                }

                if($question == "FINCA"){
                    if(isset($values[0])){
                        $Encabezado[$question] = $values[0];
                    }
                }
            }

            if($pagina_nombre == "FUNGICIDAS"){
                if($question == "GEOLOCALIZACIÓN"){
                    if(isset($values[0])){
                        if($values[0]["coordinates"] != null){
                            $Fungicidas[$question] = array("muestra"=>$pagina_nombre, "lat"=>$values[0]["coordinates"]["latitude"], "lng"=>$values[0]["coordinates"]["longitude"]);
                        }
                    }
                }else{
                    if($question == "PROVEEDOR"){
                        $Fungicidas[$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    }
                    if($question == "FUNGICIDA 1"){
                        $mode = $question;
                        // D($mode);
                        $Fungicidas[$mode] = [];
                        $Fungicidas[$mode][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    }
                    if($question == "FUNGICIDA 2"){
                        $mode = $question;
                        $Fungicidas[$mode] = [];
                        $Fungicidas[$mode][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    }
                    /*----------  Detalle  ----------*/
                    if($question == "DOSIS"){
                        $Fungicidas[$mode][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    } 
                    if($question == "CANTIDAD"){
                        $Fungicidas[$mode][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    /*----------  Detalle  ----------*/
                    }
                    if($question == "FUNGICIDA 1 - ID" || $question == "FUNGICIDA 2 - ID"){
                        $Fungicidas[$mode][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    }
                    /*----------  FOTOS  ----------*/
                    if($question == "CAPTURA DE FOTOS"){
                        $Fungicidas["fotos"] = "'".(isset($values[0]["filename"]) ? "ciclos/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '')."'";
                    }
                }
            }

            if($pagina_nombre == "ACEITE Y AGUA"){
                if($question == "ACEITE"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "AGUA"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                
                
                /*----------  Detalle  ----------*/
                if($question == "DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "CANTIDAD"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "NUM_DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_ACEITE")
                    $Productos["ACEITE"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                
                
                /*----------  Detalle  ----------*/

                /*----------  FOTOS  ----------*/
                if($question == "CAPTURA DE FOTOS"){
                    $Productos["fotos"] = "'".(isset($values[0]["filename"]) ? "ciclos/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '')."'";
                }
            }

            if($pagina_nombre == "INSECTICIDA"){
                if($question == "INSECTICIDA"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "CANTIDAD"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "NUM_DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_INSECTICIDA")
                    $Productos["INSECTICIDA"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
            }

            if($pagina_nombre == "FOLIARES"){
                if($question == "FOLIAR 1"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "FOLIAR 2"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "FOLIAR 3"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "CANTIDAD"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "NUM_DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }

                if($question == "ID_FOLIAR_1")
                    $Productos["FOLIAR 1"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                if($question == "ID_FOLIAR_2")
                    $Productos["FOLIAR 2"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                if($question == "ID_FOLIAR_3")
                    $Productos["FOLIAR 3"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
            }

            if($pagina_nombre == "COADYUVANTES"){
                if($question == "EMULSIFICANTE"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "CANTIDAD"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "NUM_DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }

                if($question == "ID_EMULSIFICANTE")
                    $Productos["EMULSIFICANTE"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
            }

            if($pagina_nombre == "BIOESTIMULANTES"){
                if(strpos($type, "BIOESTIMULANTE") === NULL){
                    $type = "BIOESTIMULANTE 1";
                }
                if($question == "BIOESTIMULANTE 1"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "BIOESTIMULANTE 2"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "BIOESTIMULANTE 3"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "BIOESTIMULANTE 4"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "BIOESTIMULANTE 5"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "BIOESTIMULANTE 6"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "BIOESTIMULANTE 7"){
                    $type = $question;
                    $Productos[$type] = [];
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "CANTIDAD"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "NUM_DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "PROVEEDOR"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_PROVEEDOR"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }

                if($question == "ID_BIOESTIMULANTE_1")
                    $Productos["BIOESTIMULANTE 1"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                if($question == "ID_BIOESTIMULANTE_2")
                    $Productos["BIOESTIMULANTE 2"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                if($question == "ID_BIOESTIMULANTE_3")
                    $Productos["BIOESTIMULANTE 3"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                if($question == "ID_BIOESTIMULANTE_4")
                    $Productos["BIOESTIMULANTE 4"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                if($question == "ID_BIOESTIMULANTE_5")
                    $Productos["BIOESTIMULANTE 5"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                if($question == "ID_BIOESTIMULANTE_6")
                    $Productos["BIOESTIMULANTE 6"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                if($question == "ID_BIOESTIMULANTE_7")
                    $Productos["BIOESTIMULANTE 7"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
            }

            if($pagina_nombre == "OBSERVACIONES GENERALES"){
                if($question == "OBSERVACIONES GENERALES"){
                    $Obseraviones[$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                /*----------  FOTOS  ----------*/
                if($question == "CAPTURA DE FOTOS GENERALES"){
                    $Obseraviones["fotos"] = "'".(isset($values[0]["filename"]) ? "ciclos/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '')."'";
                }
            }
        }
    }


    D($Encabezado);
    D($Fungicidas);
    D($Productos);
    D($Obseraviones);
    if(str_replace("'", "", $Encabezado['SECTOR']) != ""){
        $sectores[] = str_replace("'", "", $Encabezado['SECTOR']);
    }

    if(isset($Encabezado['SECTOR 2']))
    if(str_replace("'", "", $Encabezado['SECTOR 2']) != ""){
        $sectores[] = str_replace("'", "", $Encabezado['SECTOR 2']);
    }

    foreach($sectores as $sector){
        $sql = "INSERT INTO cycle_application SET
                id_usuario = '',
                id_hacienda = {$Encabezado['FINCA - ID']},                
                fecha = {$Encabezado['FECHA']},
                hora = '{$Encabezado['HORA'][0]}',
                id_sector = '',
                sector = '{$sector}',
                hectareas_fumigacion = {$Encabezado['HECTAREAS DE FUMIGACION']},
                notificacion = {$Encabezado['NOTIFICACION']},
                programa = {$Encabezado['PROGRAMA']},
                tipo_ciclo = {$Encabezado['TIPO DE CICLO']},
                motivo = {$Encabezado['MOTIVO DE ATRASO']},
                num_ciclo = {$Encabezado['CICLO #']},
                id_fumigadora = {$Encabezado['FUMIGADORA - ID']},
                fumigadora = {$Encabezado['FUMIGADORA']},
                id_piloto = {$Encabezado['PILOTO - ID']},
                id_placa_avion = {$Encabezado['PILOTO - ID_PLACA']},
                placa_avion = {$Encabezado['PLACA AVION']},
                hora_salida = '{$Encabezado['HORA DE SALIDA'][0]}',
                hora_llegada = '{$Encabezado['HORA DE LLEGADA'][0]}',
                status = '',
                proveedor = {$Fungicidas['PROVEEDOR']},

                id_fungicida = {$Fungicidas['FUNGICIDA 1']['FUNGICIDA 1 - ID']},
                fungicida = {$Fungicidas['FUNGICIDA 1']['FUNGICIDA 1']},
                cantidad = {$Fungicidas['FUNGICIDA 1']['CANTIDAD']},
                dosis = {$Fungicidas['FUNGICIDA 1']['DOSIS']},

                id_fungicida2 = {$Fungicidas['FUNGICIDA 2']['FUNGICIDA 2 - ID']},
                fungicida_2 = {$Fungicidas['FUNGICIDA 2']['FUNGICIDA 2']},
                cantidad_2 = {$Fungicidas['FUNGICIDA 2']['CANTIDAD']},
                dosis_2 = {$Fungicidas['FUNGICIDA 2']['DOSIS']},

                agua = {$Productos['AGUA']['AGUA']},
                cantidad_agua = {$Productos['AGUA']['CANTIDAD']},
                dosis_agua = {$Productos['AGUA']['DOSIS']},

                id_aceite = {$Productos['ACEITE']['id']},
                aceite = {$Productos['ACEITE']['ACEITE']},
                cantidad_aceite = {$Productos['ACEITE']['CANTIDAD']},
                dosis_aceite = {$Productos['ACEITE']['DOSIS']},

                id_emulsificante = {$Productos['EMULSIFICANTE']['id']},
                emulsificante = {$Productos['EMULSIFICANTE']['EMULSIFICANTE']},
                cantidad_emulsificante = {$Productos['EMULSIFICANTE']['CANTIDAD']},
                dosis_emulsificante = {$Productos['EMULSIFICANTE']['DOSIS']},

                id_foliar_1 = {$Productos['FOLIAR 1']['id']},
                foliar_1 = {$Productos['FOLIAR 1']['FOLIAR 1']},
                cantidad_foliar_1 = {$Productos['FOLIAR 1']['CANTIDAD']},
                dosis_foliar_1 = {$Productos['FOLIAR 1']['DOSIS']},

                id_foliar_2 = {$Productos['FOLIAR 2']['id']},
                foliar_2 = {$Productos['FOLIAR 2']['FOLIAR 2']},
                cantidad_foliar_2 = {$Productos['FOLIAR 2']['CANTIDAD']},
                dosis_foliar_2 = {$Productos['FOLIAR 2']['DOSIS']},

                id_foliar_3 = {$Productos['FOLIAR 3']['id']},
                foliar_3 = {$Productos['FOLIAR 3']['FOLIAR 3']},
                cantidad_foliar_3 = {$Productos['FOLIAR 3']['CANTIDAD']},
                dosis_foliar_3 = {$Productos['FOLIAR 3']['DOSIS']},

                prov_bio_1 = {$Productos['BIOESTIMULANTE 1']['PROVEEDOR']},
                id_prov_bio_1 = {$Productos['BIOESTIMULANTE 1']['ID_PROVEEDOR']},
                num_dosis_bioestimulante_1 = {Productos['BIOESTIMULANTE 1']['NUM_DOSIS']},
                id_bioestimulante_1 = {$Productos['BIOESTIMULANTE 1']['id']},
                bioestimulante_1 = {$Productos['BIOESTIMULANTE 1']['BIOESTIMULANTE 1']},
                cantidad_bioestimulante_1 = {$Productos['BIOESTIMULANTE 1']['CANTIDAD']},
                dosis_bioestimulante_1 = {$Productos['BIOESTIMULANTE 1']['DOSIS']},                

                prov_bio_2 = {$Productos['BIOESTIMULANTE 2']['PROVEEDOR']},
                id_prov_bio_2 = {$Productos['BIOESTIMULANTE 2']['ID_PROVEEDOR']},
                num_dosis_bioestimulante_2 = {Productos['BIOESTIMULANTE 2']['NUM_DOSIS']},
                id_bioestimulante_2 = {$Productos['BIOESTIMULANTE 2']['id']},
                bioestimulante_2 = {$Productos['BIOESTIMULANTE 2']['BIOESTIMULANTE 2']},
                cantidad_bioestimulante_2 = {$Productos['BIOESTIMULANTE 2']['CANTIDAD']},
                dosis_bioestimulante_2 = {$Productos['BIOESTIMULANTE 2']['DOSIS']},
                
                prov_bio_3 = {$Productos['BIOESTIMULANTE 3']['PROVEEDOR']},
                id_prov_bio_3 = {$Productos['BIOESTIMULANTE 3']['ID_PROVEEDOR']},
                num_dosis_bioestimulante_3 = {Productos['BIOESTIMULANTE 3']['NUM_DOSIS']},
                id_bioestimulante_3 = {$Productos['BIOESTIMULANTE 3']['id']},
                bioestimulante_3 = {$Productos['BIOESTIMULANTE 3']['BIOESTIMULANTE 3']},
                cantidad_bioestimulante_3 = {$Productos['BIOESTIMULANTE 3']['CANTIDAD']},
                dosis_bioestimulante_3 = {$Productos['BIOESTIMULANTE 3']['DOSIS']},

                prov_bio_4 = {$Productos['BIOESTIMULANTE 4']['PROVEEDOR']},
                id_prov_bio_4 = {$Productos['BIOESTIMULANTE 4']['ID_PROVEEDOR']},
                num_dosis_bioestimulante_4 = {Productos['BIOESTIMULANTE 4']['NUM_DOSIS']},
                id_bioestimulante_4 = {$Productos['BIOESTIMULANTE 4']['id']},
                bioestimulante_4 = {$Productos['BIOESTIMULANTE 4']['BIOESTIMULANTE 4']},
                cantidad_bioestimulante_4 = {$Productos['BIOESTIMULANTE 4']['CANTIDAD']},
                dosis_bioestimulante_4 = {$Productos['BIOESTIMULANTE 4']['DOSIS']},

                prov_bio_5 = {$Productos['BIOESTIMULANTE 5']['PROVEEDOR']},
                id_prov_bio_5 = {$Productos['BIOESTIMULANTE 5']['ID_PROVEEDOR']},
                num_dosis_bioestimulante_5 = {Productos['BIOESTIMULANTE 5']['NUM_DOSIS']},
                id_bioestimulante_5 = {$Productos['BIOESTIMULANTE 5']['id']},
                bioestimulante_5 = {$Productos['BIOESTIMULANTE 5']['BIOESTIMULANTE 5']},
                cantidad_bioestimulante_5 = {$Productos['BIOESTIMULANTE 5']['CANTIDAD']},
                dosis_bioestimulante_5 = {$Productos['BIOESTIMULANTE 5']['DOSIS']},

                prov_bio_6 = {$Productos['BIOESTIMULANTE 6']['PROVEEDOR']},
                id_prov_bio_6 = {$Productos['BIOESTIMULANTE 6']['ID_PROVEEDOR']},
                num_dosis_bioestimulante_6 = {Productos['BIOESTIMULANTE 6']['NUM_DOSIS']},
                id_bioestimulante_6 = {$Productos['BIOESTIMULANTE 6']['id']},
                bioestimulante_6 = {$Productos['BIOESTIMULANTE 6']['BIOESTIMULANTE 6']},
                cantidad_bioestimulante_6 = {$Productos['BIOESTIMULANTE 6']['CANTIDAD']},
                dosis_bioestimulante_6 = {$Productos['BIOESTIMULANTE 6']['DOSIS']},

                prov_bio_7 = {$Productos['BIOESTIMULANTE 7']['PROVEEDOR']},
                id_prov_bio_7 = {$Productos['BIOESTIMULANTE 7']['ID_PROVEEDOR']},
                num_dosis_bioestimulante_7 = {Productos['BIOESTIMULANTE 7']['NUM_DOSIS']},
                id_bioestimulante_7 = {$Productos['BIOESTIMULANTE 7']['id']},
                bioestimulante_7 = {$Productos['BIOESTIMULANTE 7']['BIOESTIMULANTE 7']},
                cantidad_bioestimulante_7 = {$Productos['BIOESTIMULANTE 7']['CANTIDAD']},
                dosis_bioestimulante_7 = {$Productos['BIOESTIMULANTE 7']['DOSIS']},

                id_insecticida = {$Productos['INSECTICIDA']['id']},
                insecticida = {$Productos['INSECTICIDA']['INSECTICIDA']},
                cantidad_insecticida = {$Productos['INSECTICIDA']['CANTIDAD']},
                dosis_insecticida = {$Productos['INSECTICIDA']['DOSIS']},

                fotos_2 = {$Obseraviones['fotos']},
                json = '{$json["referenceNumber"]}.json'";
        #$conexion->query($sql);
        $id = $conexion->getLastID();
        D($sql);

        if($Productos['BIOESTIMULANTE 1']['id'] > 0){
            $precio =  $conexion->queryRow("SELECT precio FROM products_price WHERE id_producto = {$Productos['BIOESTIMULANTE 1']['id']} ORDER BY timestamp DESC")->precio;
            $cant = $Productos['BIOESTIMULANTE 1']['CANTIDAD'] > 0 ? $Productos['BIOESTIMULANTE 1']['CANTIDAD'] : 0; 
            $sql = "INSERT INTO ciclos_aplicacion_detalle 
                        SET id_producto = '{$Productos['BIOESTIMULANTE 1']['id']}',
                            id_proveedor = '{$Productos['BIOESTIMULANTE 1']['ID_PROVEEDOR']}',
                            finca = '{$Encabezado['FINCA']},',
                            ciclo = '{$Encabezado['CICLO #']}',
                            fecha = '{$Encabezado['FECHA']}',
                            hora = '{$Encabezado['HORA'][0]}',
                            dosis = '{$Productos['BIOESTIMULANTE 1']['DOSIS']}',
                            precio = '{$precio}',
                            cantidad = '{$Productos['BIOESTIMULANTE 1']['CANTIDAD']}',
                            ha = '{$Encabezado['HECTAREAS DE FUMIGACION']}',
                            total = $precio * $cant,
                            programa = '{$Encabezado['PROGRAMA']}',
                            tipo_ciclo = '{$Encabezado['TIPO DE CICLO']}'";
            D($sql);
            #$conexion->query($sql);
        }
        if($Productos['BIOESTIMULANTE 2']['id'] > 0){
            $precio =  $conexion->queryRow("SELECT precio FROM products_price WHERE id_producto = {$Productos['BIOESTIMULANTE 1']['id']} ORDER BY timestamp DESC")->precio;
            $cant = $Productos['BIOESTIMULANTE 2']['CANTIDAD'] > 0 ? $Productos['BIOESTIMULANTE 2']['CANTIDAD'] : 0; 
            $sql = "INSERT INTO ciclos_aplicacion_detalle 
                        SET id_producto = '{$Productos['BIOESTIMULANTE 2']['id']}',
                            id_proveedor = '{$Productos['BIOESTIMULANTE 2']['ID_PROVEEDOR']}',
                            finca = '{$Encabezado['FINCA']},',
                            ciclo = '{$Encabezado['CICLO #']}',
                            fecha = '{$Encabezado['FECHA']}',
                            hora = '{$Encabezado['HORA'][0]}',
                            dosis = '{$Productos['BIOESTIMULANTE 2']['DOSIS']}',
                            precio = '{$precio}',
                            cantidad = '{$Productos['BIOESTIMULANTE 2']['CANTIDAD']}',
                            ha = '{$Encabezado['HECTAREAS DE FUMIGACION']}',
                            total = $precio * $cant,
                            programa = '{$Encabezado['PROGRAMA']}',
                            tipo_ciclo = '{$Encabezado['TIPO DE CICLO']}'";
            D($sql);
            #$conexion->query($sql);
        }
    }
}

function D($arreglo){
    echo "<pre>";
        print_r($arreglo);
    echo "</pre>";
}

function limpiar($String){
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
     
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);
    return $String;
}

function strtolower_utf8($string){ 
  $convert_to = array( 
    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", 
    "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", 
    "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж", 
    "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", 
    "ь", "э", "ю", "я" 
  ); 
  $convert_from = array( 
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", 
    "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", 
    "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", 
    "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ", 
    "Ь", "Э", "Ю", "Я" 
  ); 

  return str_replace($convert_from, $convert_to, $string); 
}

?>