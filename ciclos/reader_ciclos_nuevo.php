<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

include '../controllers/conexion.php';

/** Directorio de los JSON */
$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        $pos1 = strpos($object->getPath(), "/new");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        process_json_v12($json, $object->getFileName());
                        //process_json_v12_copia($json, $object->getFileName());
                        move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        move_image($object->getPathName(), $object->getFileName());
                        break;

                    default:
                        
                        break;
                }
            }
        }
    }
}

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/old/$nameFile")){
        echo 'error';
    }
    /*if(!rename($file, __DIR__."/nuevo/semanas3/$nameFile")){
        echo 'error';
    }*/
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/img/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename){
    D($filename);
    $conexion = new M_Conexion('sigat');

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';;
    $user_username = $user['username'].'';;
    $referenceNumber = $json["referenceNumber"];
    $pages = $json['pages'];

    $geoStamp = $json["geoStamp"];
    $coordinates = $geoStamp["coordinates"];
    $latitude = $coordinates["latitude"];
    $longitude = $coordinates["longitude"];

    $foliar = 0;
    $tres_metros = false;
    $cero_semanas = false;
    $once_semanas = false; 

    $Encabezado = [];
    $Fungicidas = [];
    $Productos = [];
    $Obseraviones = [];

    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = strtoupper(trim(limpiar($page_data['name'])));
        $answers = $page_data["answers"];
        $mode = "";
        $type = "";
        foreach($answers as $x => $answer){

            $question = strtoupper(trim(limpiar($answer['question'])));
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            // $question = $answer['question'];
            $values   = $answer['values'];

            $labelita = limpiar(trim(strtolower($label)));
            if($pagina_nombre == "ENCABEZADO"){
                if($question != "GEOLOCALIZACION"){
                    if($question == "SECTOR"){
                        if(is_array($values) && count($values) > 0){
                            $Encabezado[$question] = "'".$values[0]."'";
                        }
                    }
                    if($question == "SECTOR 2"){
                        if(is_array($values) && count($values) > 0){
                            $Encabezado[$question] = "'".$values[0]."'";
                        }
                    }
                    if($question == "SECTOR 3"){
                        if(is_array($values) && count($values) > 0){
                            $Encabezado[$question] = "'".$values[0]."'";
                        }
                    }
                    if($question == "SECTOR 4"){
                        if(is_array($values) && count($values) > 0){
                            $Encabezado[$question] = "'".$values[0]."'";
                        }
                    }
                    if($question == "SECTOR 5"){
                        if(is_array($values) && count($values) > 0){
                            $Encabezado[$question] = "'".$values[0]."'";
                        }
                    }
                    elseif($question == "HORA" || $question == "HORA DE SALIDA" || $question == "HORA DE LLEGADA"){
                        $hora = ["00:00:00"];
                        if(isset($values[0]["shifted"])){
                            $hora = explode("-", $values[0]["shifted"]);
                            $Encabezado[$question] = $hora;
                        }
                    }else{
                        $Encabezado[$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    } 
                }  

                if($question == "GEOLOCALIZACION"){
                    if(isset($values[0])){
                        if($values[0]["coordinates"] != null){
                            $Encabezado[$question] = array("muestra"=>$pagina_nombre, "lat"=>$values[0]["coordinates"]["latitude"], "lng"=>$values[0]["coordinates"]["longitude"]);
                        }
                    }
                }

                if($question == "FINCA"){
                    if(isset($values[0])){
                        $Encabezado[$question] = $values[0];
                    }
                }
            }

            if($pagina_nombre == "FUNGICIDAS"){
                if($question == "GEOLOCALIZACIÓN"){
                    if(isset($values[0])){
                        if($values[0]["coordinates"] != null){
                            $Fungicidas[$question] = array("muestra"=>$pagina_nombre, "lat"=>$values[0]["coordinates"]["latitude"], "lng"=>$values[0]["coordinates"]["longitude"]);
                        }
                    }
                }else{
                    if(strpos($type, "FUNGICIDA") === false){
                        $type = "FUNGICIDA";
                        $count_type = 0;
                    }
                    if($question == "PROVEEDOR"){
                        $count_type++;
                        $type = "FUNGICIDA {$count_type}";
                        $Fungicidas[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    }
                    if($question == "PROVEEDOR - ID"){
                        $Fungicidas[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    }
                    if($question == "FUNGICIDA 1"){
                        $Fungicidas[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    }
                    if($question == "FUNGICIDA 2"){
                        $Fungicidas[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    }
                    /*----------  Detalle  ----------*/
                    if($question == "DOSIS"){
                        $Fungicidas[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    } 
                    if($question == "CANTIDAD"){
                        $Fungicidas[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    /*----------  Detalle  ----------*/
                    }
                    if($question == "FUNGICIDA 1 - ID" || $question == "FUNGICIDA 2 - ID"){
                        $Fungicidas[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                    }
                    /*----------  FOTOS  ----------*/
                    if($question == "CAPTURA DE FOTOS"){
                        $Fungicidas["fotos"] = "'".(isset($values[0]["filename"]) ? "ciclos/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '')."'";
                    }
                }
            }

            if($pagina_nombre == "ACEITE Y AGUA"){
                if(strpos($type, "ACEITE") === false && $type != 'AGUA'){
                    $type = "ACEITE";
                }
                if($question == "ACEITE"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "AGUA"){
                    $type = $question;
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                
                
                /*----------  Detalle  ----------*/
                if($question == "DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "CANTIDAD"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "NUM_DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_PROVEEDOR"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "PROVEEDOR"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_ACEITE")
                    $Productos["ACEITE"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                
                
                /*----------  Detalle  ----------*/

                /*----------  FOTOS  ----------*/
                if($question == "CAPTURA DE FOTOS"){
                    $Productos["fotos"] = "'".(isset($values[0]["filename"]) ? "ciclos/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '')."'";
                }
            }

            if($pagina_nombre == "INSECTICIDA"){
                if(strpos($type, "INSECTICIDA") === false){
                    $type = "INSECTICIDA";
                    $count_type = 0;
                }
                if($question == "PROVEEDOR"){
                    $count_type++;
                    $type = "INSECTICIDA {$count_type}";
                    $Productos[$type] = []; 
                }
                if($question == "INSECTICIDA 1"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "INSECTICIDA 2"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "PROVEEDOR"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_PROVEEDOR"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "NUM_DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "CANTIDAD"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_INSECTICIDA_1"){
                    $Productos["INSECTICIDA 1"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_INSECTICIDA_2"){
                    $Productos["INSECTICIDA 2"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
            }

            if($pagina_nombre == "FOLIARES"){
                if(strpos($type, "FOLIAR") === false){
                    $type = "FOLIAR";
                    $count_type = 0;
                }
                if($question == "PROVEEDOR"){
                    $count_type++;
                    $type = "FOLIAR {$count_type}";
                    $Productos[$type] = [];
                }
                if($question == "FOLIAR 1"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "FOLIAR 2"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "FOLIAR 3"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "FOLIAR 4"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "FOLIAR 5"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "FOLIAR 6"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "FOLIAR 7"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }

                if($question == "PROVEEDOR"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_PROVEEDOR"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "NUM_DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "CANTIDAD"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }

                if($question == "ID_FOLIAR_1"){
                    $Productos["FOLIAR 1"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }   
                if($question == "ID_FOLIAR_2"){
                    $Productos["FOLIAR 2"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }  
                if($question == "ID_FOLIAR_3"){
                    $Productos["FOLIAR 3"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_FOLIAR_4"){
                    $Productos["FOLIAR 4"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_FOLIAR_5"){
                    $Productos["FOLIAR 5"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_FOLIAR_6"){
                    $Productos["FOLIAR 6"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_FOLIAR_7"){
                    $Productos["FOLIAR 7"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }          
            }

            if($pagina_nombre == "COADYUVANTES"){
                if(strpos($type, "EMULSIFICANTE") === false){
                    $type = "EMULSIFICANTE";
                    $count_type = 0;
                }
                if($question == "PROVEEDOR"){
                    $count_type++;
                    $type = "EMULSIFICANTE {$count_type}";
                    $Productos[$type] = [];
                }
                if($question == "EMULSIFICANTE"){
                    $Productos[$type]["EMULSIFICANTE 1"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "EMULSIFICANTE 2"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "EMULSIFICANTE 3"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "EMULSIFICANTE 4"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "EMULSIFICANTE 5"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "EMULSIFICANTE 6"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "EMULSIFICANTE 7"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "PROVEEDOR"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_PROVEEDOR"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "NUM_DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "CANTIDAD"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }

                if($question == "ID_EMULSIFICANTE_1"){
                    $Productos["EMULSIFICANTE 1"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_EMULSIFICANTE_2"){
                    $Productos["EMULSIFICANTE 2"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_EMULSIFICANTE_3"){
                    $Productos["EMULSIFICANTE 3"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_EMULSIFICANTE_4"){
                    $Productos["EMULSIFICANTE 4"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_EMULSIFICANTE_5"){
                    $Productos["EMULSIFICANTE 5"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_EMULSIFICANTE_6"){
                    $Productos["EMULSIFICANTE 6"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_EMULSIFICANTE_7"){
                    $Productos["EMULSIFICANTE 7"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }          
            }

            if($pagina_nombre == "BIOESTIMULANTES"){
                
                if(strpos($type, "BIOESTIMULANTE") === false){
                    $type = "BIOESTIMULANTE";
                    $count_type = 0;
                }
                if($question == "PROVEEDOR"){
                    $count_type++;
                    $type = "BIOESTIMULANTE {$count_type}";
                    $Productos[$type] = [];
                }

                if($question == "BIOESTIMULANTE 1"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "BIOESTIMULANTE 2"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "BIOESTIMULANTE 3"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "BIOESTIMULANTE 4"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "BIOESTIMULANTE 5"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "BIOESTIMULANTE 6"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "BIOESTIMULANTE 7"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "PROVEEDOR"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "ID_PROVEEDOR"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "NUM_DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "DOSIS"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "CANTIDAD"){
                    $Productos[$type][$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }

                if($question == "ID_BIOESTIMULANTE_1")
                    $Productos["BIOESTIMULANTE 1"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                if($question == "ID_BIOESTIMULANTE_2")
                    $Productos["BIOESTIMULANTE 2"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                if($question == "ID_BIOESTIMULANTE_3")
                    $Productos["BIOESTIMULANTE 3"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                if($question == "ID_BIOESTIMULANTE_4")
                    $Productos["BIOESTIMULANTE 4"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                if($question == "ID_BIOESTIMULANTE_5")
                    $Productos["BIOESTIMULANTE 5"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                if($question == "ID_BIOESTIMULANTE_6")
                    $Productos["BIOESTIMULANTE 6"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                if($question == "ID_BIOESTIMULANTE_7")
                    $Productos["BIOESTIMULANTE 7"]["id"] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
            }

            if($pagina_nombre == "OBSERVACIONES GENERALES"){
                if($question == "OBSERVACIONES GENERALES"){
                    $Obseraviones[$question] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                /*----------  FOTOS  ----------*/
                if($question == "CAPTURA DE FOTOS GENERALES"){
                    $Obseraviones["fotos"] = "'".(isset($values[0]["filename"]) ? "ciclos/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '')."'";
                }
            }
        }
    }


    #D($Encabezado);
    #D($Fungicidas);
    #D($Productos);
    #D($Obseraviones);

    if(str_replace("'", "", $Encabezado['SECTOR']) != ""){
        $sectores[] = str_replace("'", "", $Encabezado['SECTOR']);
    }
    if(isset($Encabezado['SECTOR 2']))
    if(str_replace("'", "", $Encabezado['SECTOR 2']) != ""){
        $sectores[] = str_replace("'", "", $Encabezado['SECTOR 2']);
    }
    if(isset($Encabezado['SECTOR 3']))
    if(str_replace("'", "", $Encabezado['SECTOR 3']) != ""){
        $sectores[] = str_replace("'", "", $Encabezado['SECTOR 3']);
    }
    if(isset($Encabezado['SECTOR 4']))
    if(str_replace("'", "", $Encabezado['SECTOR 4']) != ""){
        $sectores[] = str_replace("'", "", $Encabezado['SECTOR 4']);
    }
    if(isset($Encabezado['SECTOR 5']))
    if(str_replace("'", "", $Encabezado['SECTOR 5']) != ""){
        $sectores[] = str_replace("'", "", $Encabezado['SECTOR 5']);
    }
    if(!isset($Encabezado['FECHA REAL'])){
        $Encabezado['FECHA REAL'] = $Encabezado['FECHA'];
        $Encabezado['FECHA PROGRAMACION'] = $Encabezado['FECHA'];
    }
    foreach($sectores as $sector){
        $sql = "INSERT INTO cycle_application SET
                id_usuario = '',
                id_hacienda = {$Encabezado['FINCA - ID']},                
                fecha = {$Encabezado['FECHA REAL']},
                fecha_programada = {$Encabezado['FECHA PROGRAMACION']},
                hora = '{$Encabezado['HORA'][0]}',
                id_sector = '',
                sector = '{$sector}',
                hectareas_fumigacion = {$Encabezado['HECTAREAS DE FUMIGACION']},
                notificacion = {$Encabezado['NOTIFICACION']},
                programa = {$Encabezado['PROGRAMA']},
                tipo_ciclo = {$Encabezado['TIPO DE CICLO']},
                motivo = {$Encabezado['MOTIVO DE ATRASO']},
                num_ciclo = {$Encabezado['CICLO #']},
                id_fumigadora = {$Encabezado['FUMIGADORA - ID']},
                fumigadora = {$Encabezado['FUMIGADORA']},
                ha_oper = {$Encabezado['PRECIO OPERACION']},
                id_piloto = {$Encabezado['PILOTO - ID']},
                id_placa_avion = {$Encabezado['PILOTO - ID_PLACA']},
                placa_avion = {$Encabezado['PLACA AVION']},
                hora_salida = '{$Encabezado['HORA DE SALIDA'][0]}',
                hora_llegada = '{$Encabezado['HORA DE LLEGADA'][0]}',
                status = '',
                proveedor = {$Fungicidas['FUNGICIDA 1']['PROVEEDOR']},

                id_fungicida = {$Fungicidas['FUNGICIDA 1']['FUNGICIDA 1 - ID']},
                fungicida = {$Fungicidas['FUNGICIDA 1']['FUNGICIDA 1']},
                cantidad = {$Fungicidas['FUNGICIDA 1']['CANTIDAD']},
                dosis = {$Fungicidas['FUNGICIDA 1']['DOSIS']},

                id_fungicida2 = {$Fungicidas['FUNGICIDA 2']['FUNGICIDA 2 - ID']},
                fungicida_2 = {$Fungicidas['FUNGICIDA 2']['FUNGICIDA 2']},
                cantidad_2 = {$Fungicidas['FUNGICIDA 2']['CANTIDAD']},
                dosis_2 = {$Fungicidas['FUNGICIDA 2']['DOSIS']},

                agua = {$Productos['AGUA']['AGUA']},
                cantidad_agua = {$Productos['AGUA']['CANTIDAD']},
                dosis_agua = {$Productos['AGUA']['DOSIS']},

                id_aceite = {$Productos['ACEITE']['id']},
                aceite = {$Productos['ACEITE']['ACEITE']},
                cantidad_aceite = {$Productos['ACEITE']['CANTIDAD']},
                dosis_aceite = {$Productos['ACEITE']['DOSIS']},

                /*EMULSIFICANTE */
                prov_emulsificante_1 = {$Productos['EMULSIFICANTE 1']['PROVEEDOR']},
                id_prov_emulsificante_1 = {$Productos['EMULSIFICANTE 1']['ID_PROVEEDOR']},
                emulsificante_1 = {$Productos['EMULSIFICANTE 1']['EMULSIFICANTE 1']},
                id_emulsificante_1 = {$Productos['EMULSIFICANTE 1']['id']},
                dosis_emulsificante_1 = {$Productos['EMULSIFICANTE 1']['DOSIS']},
                num_dosis_emulsificante_1 = {$Productos['EMULSIFICANTE 1']['NUM_DOSIS']},
                cantidad_emulsificante_1 = {$Productos['EMULSIFICANTE 1']['CANTIDAD']},

                prov_emulsificante_2 = {$Productos['EMULSIFICANTE 2']['PROVEEDOR']},
                id_prov_emulsificante_2 = {$Productos['EMULSIFICANTE 2']['ID_PROVEEDOR']},
                emulsificante_2 = {$Productos['EMULSIFICANTE 2']['EMULSIFICANTE 2']},
                id_emulsificante_2 = {$Productos['EMULSIFICANTE 2']['id']},
                dosis_emulsificante_2 = {$Productos['EMULSIFICANTE 2']['DOSIS']},
                num_dosis_emulsificante_2 = {$Productos['EMULSIFICANTE 2']['NUM_DOSIS']},
                cantidad_emulsificante_2 = {$Productos['EMULSIFICANTE 2']['CANTIDAD']},

                prov_emulsificante_3 = {$Productos['EMULSIFICANTE 3']['PROVEEDOR']},
                id_prov_emulsificante_3 = {$Productos['EMULSIFICANTE 3']['ID_PROVEEDOR']},
                emulsificante_3 = {$Productos['EMULSIFICANTE 3']['EMULSIFICANTE 3']},
                id_emulsificante_3 = {$Productos['EMULSIFICANTE 3']['id']},
                dosis_emulsificante_3 = {$Productos['EMULSIFICANTE 3']['DOSIS']},
                num_dosis_emulsificante_3 = {$Productos['EMULSIFICANTE 3']['NUM_DOSIS']},
                cantidad_emulsificante_3 = {$Productos['EMULSIFICANTE 3']['CANTIDAD']},

                prov_emulsificante_4 = {$Productos['EMULSIFICANTE 4']['PROVEEDOR']},
                id_prov_emulsificante_4 = {$Productos['EMULSIFICANTE 4']['ID_PROVEEDOR']},
                emulsificante_4 = {$Productos['EMULSIFICANTE 4']['EMULSIFICANTE 4']},
                id_emulsificante_4 = {$Productos['EMULSIFICANTE 4']['id']},
                dosis_emulsificante_4 = {$Productos['EMULSIFICANTE 4']['DOSIS']},
                num_dosis_emulsificante_4 = {$Productos['EMULSIFICANTE 4']['NUM_DOSIS']},
                cantidad_emulsificante_4 = {$Productos['EMULSIFICANTE 4']['CANTIDAD']},

                prov_emulsificante_5 = {$Productos['EMULSIFICANTE 5']['PROVEEDOR']},
                id_prov_emulsificante_5 = {$Productos['EMULSIFICANTE 5']['ID_PROVEEDOR']},
                emulsificante_5 = {$Productos['EMULSIFICANTE 5']['EMULSIFICANTE 5']},
                id_emulsificante_5 = {$Productos['EMULSIFICANTE 5']['id']},
                dosis_emulsificante_5 = {$Productos['EMULSIFICANTE 5']['DOSIS']},
                num_dosis_emulsificante_5 = {$Productos['EMULSIFICANTE 5']['NUM_DOSIS']},
                cantidad_emulsificante_5 = {$Productos['EMULSIFICANTE 5']['CANTIDAD']},

                prov_emulsificante_6 = {$Productos['EMULSIFICANTE 6']['PROVEEDOR']},
                id_prov_emulsificante_6 = {$Productos['EMULSIFICANTE 6']['ID_PROVEEDOR']},
                emulsificante_6 = {$Productos['EMULSIFICANTE 6']['EMULSIFICANTE 6']},
                id_emulsificante_6 = {$Productos['EMULSIFICANTE 6']['id']},
                dosis_emulsificante_6 = {$Productos['EMULSIFICANTE 6']['DOSIS']},
                num_dosis_emulsificante_6 = {$Productos['EMULSIFICANTE 6']['NUM_DOSIS']},
                cantidad_emulsificante_6 = {$Productos['EMULSIFICANTE 6']['CANTIDAD']},

                prov_emulsificante_7 = {$Productos['EMULSIFICANTE 7']['PROVEEDOR']},
                id_prov_emulsificante_7 = {$Productos['EMULSIFICANTE 7']['ID_PROVEEDOR']},
                emulsificante_7 = {$Productos['EMULSIFICANTE 7']['EMULSIFICANTE 7']},
                id_emulsificante_7 = {$Productos['EMULSIFICANTE 7']['id']},
                dosis_emulsificante_7 = {$Productos['EMULSIFICANTE 7']['DOSIS']},
                num_dosis_emulsificante_7 = {$Productos['EMULSIFICANTE 7']['NUM_DOSIS']},
                cantidad_emulsificante_7 = {$Productos['EMULSIFICANTE 7']['CANTIDAD']},
                /*END EMULSIFICANTE*/
                /*FOLIARES*/
                prov_foliar_1 = {$Productos['FOLIAR 1']['PROVEEDOR']},
                id_prov_foliar_1 = {$Productos['FOLIAR 1']['ID_PROVEEDOR']},
                foliar_1 = {$Productos['FOLIAR 1']['FOLIAR 1']},
                id_foliar_1 = {$Productos['FOLIAR 1']['id']},
                dosis_foliar_1 = {$Productos['FOLIAR 1']['DOSIS']},
                num_dosis_foliar_1 = {$Productos['FOLIAR 1']['NUM_DOSIS']},
                cantidad_foliar_1 = {$Productos['FOLIAR 1']['CANTIDAD']},

                prov_foliar_2 = {$Productos['FOLIAR 2']['PROVEEDOR']},
                id_prov_foliar_2 = {$Productos['FOLIAR 2']['ID_PROVEEDOR']},
                foliar_2 = {$Productos['FOLIAR 2']['FOLIAR 2']},
                id_foliar_2 = {$Productos['FOLIAR 2']['id']},
                dosis_foliar_2 = {$Productos['FOLIAR 2']['DOSIS']},
                num_dosis_foliar_2 = {$Productos['FOLIAR 2']['NUM_DOSIS']},
                cantidad_foliar_2 = {$Productos['FOLIAR 2']['CANTIDAD']},

                prov_foliar_3 = {$Productos['FOLIAR 3']['PROVEEDOR']},
                id_prov_foliar_3 = {$Productos['FOLIAR 3']['ID_PROVEEDOR']},
                foliar_3 = {$Productos['FOLIAR 3']['FOLIAR 3']},
                id_foliar_3 = {$Productos['FOLIAR 3']['id']},
                dosis_foliar_3 = {$Productos['FOLIAR 3']['DOSIS']},
                num_dosis_foliar_3 = {$Productos['FOLIAR 3']['NUM_DOSIS']},
                cantidad_foliar_3 = {$Productos['FOLIAR 3']['CANTIDAD']},

                prov_foliar_4 = {$Productos['FOLIAR 4']['PROVEEDOR']},
                id_prov_foliar_4 = {$Productos['FOLIAR 4']['ID_PROVEEDOR']},
                foliar_4 = {$Productos['FOLIAR 4']['FOLIAR 4']},
                id_foliar_4 = {$Productos['FOLIAR 4']['id']},
                dosis_foliar_4 = {$Productos['FOLIAR 4']['DOSIS']},
                num_dosis_foliar_4 = {$Productos['FOLIAR 4']['NUM_DOSIS']},
                cantidad_foliar_4 = {$Productos['FOLIAR 4']['CANTIDAD']},

                prov_foliar_5 = {$Productos['FOLIAR 5']['PROVEEDOR']},
                id_prov_foliar_5 = {$Productos['FOLIAR 5']['ID_PROVEEDOR']},
                foliar_5 = {$Productos['FOLIAR 5']['FOLIAR 5']},
                id_foliar_5 = {$Productos['FOLIAR 5']['id']},
                dosis_foliar_5 = {$Productos['FOLIAR 5']['DOSIS']},
                num_dosis_foliar_5 = {$Productos['FOLIAR 5']['NUM_DOSIS']},
                cantidad_foliar_5 = {$Productos['FOLIAR 5']['CANTIDAD']},

                prov_foliar_6 = {$Productos['FOLIAR 6']['PROVEEDOR']},
                id_prov_foliar_6 = {$Productos['FOLIAR 6']['ID_PROVEEDOR']},
                foliar_6 = {$Productos['FOLIAR 6']['FOLIAR 6']},
                id_foliar_6 = {$Productos['FOLIAR 6']['id']},
                dosis_foliar_6 = {$Productos['FOLIAR 6']['DOSIS']},
                num_dosis_foliar_6 = {$Productos['FOLIAR 6']['NUM_DOSIS']},
                cantidad_foliar_6 = {$Productos['FOLIAR 6']['CANTIDAD']},

                prov_foliar_7 = {$Productos['FOLIAR 7']['PROVEEDOR']},
                id_prov_foliar_7 = {$Productos['FOLIAR 7']['ID_PROVEEDOR']},
                foliar_7 = {$Productos['FOLIAR 7']['FOLIAR 7']},
                id_foliar_7 = {$Productos['FOLIAR 7']['id']},
                dosis_foliar_7 = {$Productos['FOLIAR 7']['DOSIS']},
                num_dosis_foliar_7= {$Productos['FOLIAR 7']['NUM_DOSIS']},
                cantidad_foliar_7 = {$Productos['FOLIAR 7']['CANTIDAD']},
                /*END FOLIARES*/
                /*BIOESTIMULANTES*/
                prov_bio_1 = {$Productos['BIOESTIMULANTE 1']['PROVEEDOR']},
                id_prov_bio1 = {$Productos['BIOESTIMULANTE 1']['ID_PROVEEDOR']},
                num_dosis_bioestimulante_1 = {$Productos['BIOESTIMULANTE 1']['NUM_DOSIS']},
                id_bioestimulante_1 = {$Productos['BIOESTIMULANTE 1']['id']},
                bioestimulante_1 = {$Productos['BIOESTIMULANTE 1']['BIOESTIMULANTE 1']},
                cantidad_bioestimulante_1 = {$Productos['BIOESTIMULANTE 1']['CANTIDAD']},
                dosis_bioestimulante_1 = {$Productos['BIOESTIMULANTE 1']['DOSIS']},                

                prov_bio_2 = {$Productos['BIOESTIMULANTE 2']['PROVEEDOR']},
                id_prov_bio_2 = {$Productos['BIOESTIMULANTE 2']['ID_PROVEEDOR']},
                num_dosis_bioestimulante_2 = {$Productos['BIOESTIMULANTE 2']['NUM_DOSIS']},
                id_bioestimulante_2 = {$Productos['BIOESTIMULANTE 2']['id']},
                bioestimulante_2 = {$Productos['BIOESTIMULANTE 2']['BIOESTIMULANTE 2']},
                cantidad_bioestimulante_2 = {$Productos['BIOESTIMULANTE 2']['CANTIDAD']},
                dosis_bioestimulante_2 = {$Productos['BIOESTIMULANTE 2']['DOSIS']},
                
                prov_bio_3 = {$Productos['BIOESTIMULANTE 3']['PROVEEDOR']},
                id_prov_bio_3 = {$Productos['BIOESTIMULANTE 3']['ID_PROVEEDOR']},
                num_dosis_bioestimulante_3 = {$Productos['BIOESTIMULANTE 3']['NUM_DOSIS']},
                id_bioestimulante_3 = {$Productos['BIOESTIMULANTE 3']['id']},
                bioestimulante_3 = {$Productos['BIOESTIMULANTE 3']['BIOESTIMULANTE 3']},
                cantidad_bioestimulante_3 = {$Productos['BIOESTIMULANTE 3']['CANTIDAD']},
                dosis_bioestimulante_3 = {$Productos['BIOESTIMULANTE 3']['DOSIS']},

                prov_bio_4 = {$Productos['BIOESTIMULANTE 4']['PROVEEDOR']},
                id_prov_bio_4 = {$Productos['BIOESTIMULANTE 4']['ID_PROVEEDOR']},
                num_dosis_bioestimulante_4 = {$Productos['BIOESTIMULANTE 4']['NUM_DOSIS']},
                id_bioestimulante_4 = {$Productos['BIOESTIMULANTE 4']['id']},
                bioestimulante_4 = {$Productos['BIOESTIMULANTE 4']['BIOESTIMULANTE 4']},
                cantidad_bioestimulante_4 = {$Productos['BIOESTIMULANTE 4']['CANTIDAD']},
                dosis_bioestimulante_4 = {$Productos['BIOESTIMULANTE 4']['DOSIS']},

                prov_bio_5 = {$Productos['BIOESTIMULANTE 5']['PROVEEDOR']},
                id_prov_bio_5 = {$Productos['BIOESTIMULANTE 5']['ID_PROVEEDOR']},
                num_dosis_bioestimulante_5 = {$Productos['BIOESTIMULANTE 5']['NUM_DOSIS']},
                id_bioestimulante_5 = {$Productos['BIOESTIMULANTE 5']['id']},
                bioestimulante_5 = {$Productos['BIOESTIMULANTE 5']['BIOESTIMULANTE 5']},
                cantidad_bioestimulante_5 = {$Productos['BIOESTIMULANTE 5']['CANTIDAD']},
                dosis_bioestimulante_5 = {$Productos['BIOESTIMULANTE 5']['DOSIS']},

                prov_bio_6 = {$Productos['BIOESTIMULANTE 6']['PROVEEDOR']},
                id_prov_bio_6 = {$Productos['BIOESTIMULANTE 6']['ID_PROVEEDOR']},
                num_dosis_bioestimulante_6 = {$Productos['BIOESTIMULANTE 6']['NUM_DOSIS']},
                id_bioestimulante_6 = {$Productos['BIOESTIMULANTE 6']['id']},
                bioestimulante_6 = {$Productos['BIOESTIMULANTE 6']['BIOESTIMULANTE 6']},
                cantidad_bioestimulante_6 = {$Productos['BIOESTIMULANTE 6']['CANTIDAD']},
                dosis_bioestimulante_6 = {$Productos['BIOESTIMULANTE 6']['DOSIS']},

                prov_bio_7 = {$Productos['BIOESTIMULANTE 7']['PROVEEDOR']},
                id_prov_bio_7 = {$Productos['BIOESTIMULANTE 7']['ID_PROVEEDOR']},
                num_dosis_bioestimulante_7 = {$Productos['BIOESTIMULANTE 7']['NUM_DOSIS']},
                id_bioestimulante_7 = {$Productos['BIOESTIMULANTE 7']['id']},
                bioestimulante_7 = {$Productos['BIOESTIMULANTE 7']['BIOESTIMULANTE 7']},
                cantidad_bioestimulante_7 = {$Productos['BIOESTIMULANTE 7']['CANTIDAD']},
                dosis_bioestimulante_7 = {$Productos['BIOESTIMULANTE 7']['DOSIS']},
                /*END BIOESTIMULANTES*/
                /*INSECTICIDAS*/
                prov_insecticida_1 = {$Productos['INSECTICIDA 1']['PROVEEDOR']},
                id_prov_insecticida_1 = {$Productos['INSECTICIDA 1']['ID_PROVEEDOR']},
                insecticida_1 = {$Productos['INSECTICIDA 1']['INSECTICIDA 1']},
                id_insecticida_1 = {$Productos['INSECTICIDA 1']['id']},
                dosis_insecticida_1 = {$Productos['INSECTICIDA 1']['DOSIS']},
                num_dosis_insecticida_1 = {$Productos['INSECTICIDA 1']['NUM_DOSIS']},
                cantidad_insecticida_1 = {$Productos['INSECTICIDA 1']['CANTIDAD']},

                prov_insecticida_2 = {$Productos['INSECTICIDA 2']['PROVEEDOR']},
                id_prov_insecticida_2 = {$Productos['INSECTICIDA 2']['ID_PROVEEDOR']},
                insecticida_2 = {$Productos['INSECTICIDA 2']['INSECTICIDA 2']},
                id_insecticida_2 = {$Productos['INSECTICIDA 2']['id']},
                dosis_insecticida_2 = {$Productos['INSECTICIDA 2']['DOSIS']},
                num_dosis_insecticida_2 = {$Productos['INSECTICIDA 2']['NUM_DOSIS']},
                cantidad_insecticida_2 = {$Productos['INSECTICIDA 2']['CANTIDAD']},
                /*END INSECTICIDAS*/

                fotos_2 = {$Obseraviones['fotos']},
                json = '{$json["referenceNumber"]}.json'";
        $conexion->query($sql);
        $id = $conexion->getLastID();
        D($sql);
    }

    for($x = 1; $x <= 7; $x++){
        if(trim($Productos["EMULSIFICANTE {$x}"]['id'], "'") > 0){
            $precio = 0;
            $row = $conexion->queryRow("SELECT precio FROM products_price WHERE id_producto = {$Productos["EMULSIFICANTE {$x}"]['id']} ORDER BY fecha DESC");
            if(isset($row->precio)) $precio = $row->precio;
            $cant = $Productos["EMULSIFICANTE {$x}"]['CANTIDAD'] > 0 ? $Productos["EMULSIFICANTE {$x}"]['CANTIDAD'] : 0; 
            $sql = "INSERT INTO ciclos_aplicacion_detalle 
                        SET id_producto = {$Productos["EMULSIFICANTE {$x}"]['id']},
                            id_proveedor = {$Productos["EMULSIFICANTE {$x}"]['ID_PROVEEDOR']},
                            finca = '{$Encabezado['FINCA']}',
                            ciclo = {$Encabezado['CICLO #']},
                            fecha = {$Encabezado['FECHA REAL']},
                            hora = '{$Encabezado['HORA'][0]}',
                            dosis = {$Productos["EMULSIFICANTE {$x}"]['DOSIS']},
                            precio = {$precio},
                            cantidad = {$Productos["EMULSIFICANTE {$x}"]['CANTIDAD']},
                            ha = {$Encabezado['HECTAREAS DE FUMIGACION']},
                            total = $precio * $cant,
                            programa = {$Encabezado['PROGRAMA']},
                            tipo_ciclo = {$Encabezado['TIPO DE CICLO']},
                            id_historico = $id,
                            json = '{$json["referenceNumber"]}.json'";
            D($sql);
            $conexion->query($sql);
        }  
        if(trim($Productos["BIOESTIMULANTE {$x}"]['id'], "'")> 0){
            $precio = 0;
            $row = $conexion->queryRow("SELECT precio FROM products_price WHERE id_producto = {$Productos["BIOESTIMULANTE {$x}"]['id']} ORDER BY fecha DESC");
            if(isset($row->precio)) $precio = $row->precio;
            if(isset($row->precio)) $precio = $row->precio;
            $cant = $Productos["BIOESTIMULANTE {$x}"]['CANTIDAD'] > 0 ? $Productos["BIOESTIMULANTE {$x}"]['CANTIDAD'] : 0; 
            $sql = "INSERT INTO ciclos_aplicacion_detalle 
                        SET id_producto = {$Productos["BIOESTIMULANTE {$x}"]['id']},
                            id_proveedor = {$Productos["BIOESTIMULANTE {$x}"]['ID_PROVEEDOR']},
                            finca = '{$Encabezado['FINCA']}',
                            ciclo = {$Encabezado['CICLO #']},
                            fecha = {$Encabezado['FECHA REAL']},
                            hora = '{$Encabezado['HORA'][0]}',
                            dosis = {$Productos["BIOESTIMULANTE {$x}"]['DOSIS']},
                            precio = {$precio},
                            cantidad = {$Productos["BIOESTIMULANTE {$x}"]['CANTIDAD']},
                            ha = {$Encabezado['HECTAREAS DE FUMIGACION']},
                            total = $precio * $cant,
                            programa = {$Encabezado['PROGRAMA']},
                            tipo_ciclo = {$Encabezado['TIPO DE CICLO']},
                            id_historico = $id,
                            json = '{$json["referenceNumber"]}.json'";
            D($sql);
            $conexion->query($sql);
        }
        if(trim($Productos["FOLIAR {$x}"]['id'], "'") > 0){
            $precio = 0; 
            $row = $conexion->queryRow("SELECT precio FROM products_price WHERE id_producto = {$Productos["FOLIAR {$x}"]['id']} ORDER BY fecha DESC");
            if(isset($row->precio)) $precio = $row->precio;
            $cant = $Productos["FOLIAR {$x}"]['CANTIDAD'] > 0 ? $Productos["FOLIAR {$x}"]['CANTIDAD'] : 0; 
            $sql = "INSERT INTO ciclos_aplicacion_detalle 
                        SET id_producto = {$Productos["FOLIAR {$x}"]['id']},
                            id_proveedor = {$Productos["FOLIAR {$x}"]['ID_PROVEEDOR']},
                            finca = '{$Encabezado['FINCA']}',
                            ciclo = {$Encabezado['CICLO #']},
                            fecha = {$Encabezado['FECHA REAL']},
                            hora = '{$Encabezado['HORA'][0]}',
                            dosis = {$Productos["FOLIAR {$x}"]['DOSIS']},
                            precio = {$precio},
                            cantidad = {$Productos["FOLIAR {$x}"]['CANTIDAD']},
                            ha = {$Encabezado['HECTAREAS DE FUMIGACION']},
                            total = $precio * $cant,
                            programa = {$Encabezado['PROGRAMA']},
                            tipo_ciclo = {$Encabezado['TIPO DE CICLO']},
                            id_historico = $id,
                            json = '{$json["referenceNumber"]}.json'";
            D($sql);
            $conexion->query($sql);
        }
    }
    for($x = 1;$x <= 2;$x++){
        if(trim($Productos["INSECTICIDA {$x}"]['id'], "'") > 0){
            $precio = 0;
            $row = $conexion->queryRow("SELECT precio FROM products_price WHERE id_producto = {$Productos["INSECTICIDA {$x}"]['id']} ORDER BY fecha DESC");
            if(isset($row->precio)) $precio = $row->precio;
            $cant = $Productos["INSECTICIDA {$x}"]['CANTIDAD'] > 0 ? $Productos["INSECTICIDA {$x}"]['CANTIDAD'] : 0; 
            $sql = "INSERT INTO ciclos_aplicacion_detalle 
                        SET id_producto = {$Productos["INSECTICIDA {$x}"]['id']},
                            id_proveedor = {$Productos["INSECTICIDA {$x}"]['ID_PROVEEDOR']},
                            finca = '{$Encabezado['FINCA']}',
                            ciclo = {$Encabezado['CICLO #']},
                            fecha = {$Encabezado['FECHA REAL']},
                            hora = '{$Encabezado['HORA'][0]}',
                            dosis = {$Productos["INSECTICIDA {$x}"]['DOSIS']},
                            precio = {$precio},
                            cantidad = {$Productos["INSECTICIDA {$x}"]['CANTIDAD']},
                            ha = {$Encabezado['HECTAREAS DE FUMIGACION']},
                            total = $precio * $cant,
                            programa = {$Encabezado['PROGRAMA']},
                            tipo_ciclo = {$Encabezado['TIPO DE CICLO']},
                            id_historico = $id,
                            json = '{$json["referenceNumber"]}.json'";
            D($sql);
            $conexion->query($sql);
        } 
        if(trim($Fungicidas["FUNGICIDA {$x}"]["FUNGICIDA {$x} - ID"], "'") > 0){
            $precio =  0;
            $row = $conexion->queryRow("SELECT precio FROM products_price WHERE id_producto = {$Fungicidas["FUNGICIDA {$x}"]["FUNGICIDA {$x} - ID"]} ORDER BY fecha DESC");
            if(isset($row->precio)) $precio = $row->precio;
            $cant = $Fungicidas["FUNGICIDA {$x}"]['CANTIDAD'] > 0 ? $Fungicidas["FUNGICIDA {$x}"]['CANTIDAD'] : 0; 
            $sql = "INSERT INTO ciclos_aplicacion_detalle
                        SET id_producto = {$Fungicidas["FUNGICIDA {$x}"]["FUNGICIDA {$x} - ID"]},
                            id_proveedor = {$Fungicidas["FUNGICIDA {$x}"]['PROVEEDOR - ID']},
                            finca = '{$Encabezado['FINCA']}',
                            ciclo = {$Encabezado['CICLO #']},
                            fecha = {$Encabezado['FECHA REAL']},
                            hora = '{$Encabezado['HORA'][0]}',
                            dosis = {$Fungicidas["FUNGICIDA {$x}"]['DOSIS']},
                            precio = {$precio},
                            cantidad = {$Fungicidas["FUNGICIDA {$x}"]['CANTIDAD']},
                            ha = {$Encabezado['HECTAREAS DE FUMIGACION']},
                            total = $precio * $cant,
                            programa = {$Encabezado['PROGRAMA']},
                            tipo_ciclo = {$Encabezado['TIPO DE CICLO']},
                            id_historico = $id,
                            json = '{$json["referenceNumber"]}.json'";
            D($sql);
            $conexion->query($sql);
        } 
    }
    if(trim($Productos["ACEITE"]['id'], "'") > 0){
        $precio = 0;  
        $row = $conexion->queryRow("SELECT precio FROM products_price WHERE id_producto = {$Productos['ACEITE']['id']} ORDER BY fecha DESC");
        if(isset($row->precio)) $precio = $row->precio;
        $cant = $Productos['ACEITE']['CANTIDAD'] > 0 ? $Productos['ACEITE']['CANTIDAD'] : 0; 
        $sql = "INSERT INTO ciclos_aplicacion_detalle 
                    SET id_producto = {$Productos['ACEITE']['id']},
                        id_proveedor = {$Productos['ACEITE']['ID_PROVEEDOR']},
                        finca = '{$Encabezado['FINCA']}',
                        ciclo = {$Encabezado['CICLO #']},
                        fecha = {$Encabezado['FECHA REAL']},
                        hora = '{$Encabezado['HORA'][0]}',
                        dosis = {$Productos['ACEITE']['DOSIS']},
                        precio = {$precio},
                        cantidad = {$Productos['ACEITE']['CANTIDAD']},
                        ha = {$Encabezado['HECTAREAS DE FUMIGACION']},
                        total = $precio * $cant,
                        programa = {$Encabezado['PROGRAMA']},
                        tipo_ciclo = {$Encabezado['TIPO DE CICLO']},
                        id_historico = $id,
                        json = '{$json["referenceNumber"]}.json'";
        D($sql);
        $conexion->query($sql);
    } 
}

function process_json_v12($json, $filename){
    $user = $json['user'];
    $user_identifier = $user['identifier'].'';;
    $user_username = $user['username'].'';;
    $referenceNumber = $json["referenceNumber"];
    $pages = $json['pages'];
    $form = $json["form"]["name"];
    $pagina_nombre = "";
    $objectMuestras = new stdClass;
    $objectMuestras->json = $filename;
    $objectMuestras->indentifier = $user_identifier;
    $objectMuestras->referenceNumber = $referenceNumber;
    $objectMuestras->auditor = $user["username"];
    $objectMuestras->auditorName = $user["displayName"];
    $contador = 0;
    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $sql_muestra_causas[] = [];
        if($pagina_nombre != limpiar(trim(strtoupper($page_data['name'])))){
            $pagina_nombre = limpiar(trim(strtoupper($page_data['name'])));
            $contador++;
        }
        $answers = $page_data["answers"];
        foreach($answers as $x => $answer){
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];
            $labelita = limpiar(trim(strtolower($label)));

            if(in_array($question, ["Dosis", "num_dosis", "Proveedor", "Proveedor - id", "Cantidad", "Emulsificante", "Sector"])){
                $c = 1;
                if(isset($objectMuestras->{$pagina_nombre})){
                    foreach($objectMuestras->{$pagina_nombre} as $q => $val){
                        if($q == "{$question} {$c}") $c++;
                    }
                }
                $objectMuestras->{$pagina_nombre}["{$question} {$c}"] = getValueFromDataType($dataType, $values, $referenceNumber);
            }else{
                $objectMuestras->{$pagina_nombre}[$question] = getValueFromDataType($dataType, $values, $referenceNumber);
            }
        }
    }

    $db = new M_Conexion('sigat');
    $objectMuestras->json = "/ciclos/old/".$objectMuestras->json;
    $fecha_real = $objectMuestras->ENCABEZADO['Fecha Real'];
    $id_finca = $objectMuestras->ENCABEZADO['Finca - id'];
    $programa = strtoupper($objectMuestras->ENCABEZADO['Programa']);

    $id_ciclo_sigatoka = null;
    $id_ciclo_plagas = null;
    $id_ciclo_foliar = null;
    $insecticidas = 0;
    $foliares = 0;
    $fungicidas = 0;
    $erwinia = 0;
    $coadyuvantes = 0;

    foreach($objectMuestras->FUNGICIDAS as $i => $insec){
        $x = $i+1;
        if(isset($objectMuestras->FUNGICIDAS["Fungicida {$x} - id"]))
        if((int) $objectMuestras->FUNGICIDAS["Fungicida {$x} - id"] > 0){
            $fungicidas++;
        }
    }
    foreach($objectMuestras->COADYUVANTES as $i => $insec){
        $x = $i+1;
        if(isset($objectMuestras->COADYUVANTES["id_emulsificante_{$x}"]))
        if((int) $objectMuestras->COADYUVANTES["id_emulsificante_{$x}"] > 0){
            $coadyuvantes++;
        }
    }
    foreach($objectMuestras->INSECTICIDA as $i => $insec){
        $x = $i+1;
        if(isset($objectMuestras->INSECTICIDA["id_insecticida_{$x}"]))
        if((int) $objectMuestras->INSECTICIDA["id_insecticida_{$x}"] > 0){
            $insecticidas++;
        }
    }
    foreach($objectMuestras->{"CONTROL ERWINIA"} as $i => $er){
        $x = $i+1;
        if(isset($objectMuestras->{"CONTROL ERWINIA"}["id_erwinia_{$x}"]))
        if((int) $objectMuestras->{"CONTROL ERWINIA"}["id_erwinia_{$x}"] > 0){
            $erwinia++;
        }
    }
    foreach($objectMuestras->FOLIARES as $i => $insec){
        $x = $i+1;
        if(isset($objectMuestras->FOLIARES["id_foliar_{$x}"]))
        if((int) $objectMuestras->FOLIARES["id_foliar_{$x}"] > 0){
            $foliares++;
        }
    }

    if($fungicidas > 0 || $coadyuvantes > 0){
        $precio_operacion = (float) $objectMuestras->ENCABEZADO['Precio operación'];
        if($foliares >= 2){
            $precio_operacion = round($precio_operacion / 2, 2);
        }

        $sql = "INSERT INTO cycle_application_reporte SET
                    fecha_programada = '{$objectMuestras->ENCABEZADO['Fecha Programación']}',
                    fecha_real = '{$objectMuestras->ENCABEZADO['Fecha Real']}',
                    hora = '{$objectMuestras->ENCABEZADO['Hora']}',
                    semana = getWeek('{$fecha_real}'),
                    anio = getYear('{$fecha_real}'),
                    id_finca = {$id_finca},
                    finca = '{$objectMuestras->ENCABEZADO['Finca']}',
                    hectareas_fumigacion = '{$objectMuestras->ENCABEZADO['Hectáreas de fumigación']}',
                    notificacion = '{$objectMuestras->ENCABEZADO['Notificación']}',
                    programa = 'SIGATOKA',
                    galones = '{$objectMuestras->ENCABEZADO['Galones']}',
                    tipo_ciclo = '{$objectMuestras->ENCABEZADO['Tipo de Ciclo']}',
                    num_ciclo = '{$objectMuestras->ENCABEZADO["Ciclo #"]}',
                    dosis_agua = '{$objectMuestras->{"ACEITE Y AGUA"}['Dosis 2']}',
                    id_fumigadora = '{$objectMuestras->ENCABEZADO['Fumigadora - id']}',
                    fumigadora = '{$objectMuestras->ENCABEZADO['Fumigadora']}',
                    precio_operacion = '{$precio_operacion}',
                    id_piloto = '{$objectMuestras->ENCABEZADO['Piloto - id']}',
                    piloto = '{$objectMuestras->ENCABEZADO['Piloto']}',
                    id_placa = '{$objectMuestras->ENCABEZADO['Piloto - id_placa']}',
                    placa_avion = '{$objectMuestras->ENCABEZADO['Placa avión']}',
                    hora_salida = '{$objectMuestras->ENCABEZADO['Hora de salida']}',
                    hora_llegada = '{$objectMuestras->ENCABEZADO['Hora de llegada']}',
                    motivo = '{$objectMuestras->ENCABEZADO['Motivo de atraso']}',
                    observaciones = '{$objectMuestras->{"OBSERVACIONES GENERALES"}['Observaciones Generales']}',
                    json = '{$objectMuestras->json}'";
        D($sql);
        $db->query($sql);
        $id_ciclo_sigatoka = $db->getLastID();
    }

    if($foliares > 0){
        $precio_operacion = (float) $objectMuestras->ENCABEZADO['Precio operación'];
        $id_fumigadora = (int) $objectMuestras->ENCABEZADO['Fumigadora - id'];

        if($programa == 'FOLIAR'){
            if($id_fumigadora == 1) $precio_operacion = 9.5;
            if($id_fumigadora == 2) $precio_operacion = 9.4;
        }
        if($programa == 'SIGATOKA FOLIAR' && $fungicidas >= 2 && $foliares >= 2){
            $precio_operacion = round($precio_operacion / 2, 2);
        }
        if($programa == 'SIGATOKA FOLIAR' && $fungicidas < 2 && $foliares > 0){
            $precio_operacion = 0;
        }


        $sql = "INSERT INTO cycle_application_reporte SET
                    fecha_programada = '{$objectMuestras->ENCABEZADO['Fecha Programación']}',
                    fecha_real = '{$objectMuestras->ENCABEZADO['Fecha Real']}',
                    hora = '{$objectMuestras->ENCABEZADO['Hora']}',
                    semana = getWeek('{$fecha_real}'),
                    anio = getYear('{$fecha_real}'),
                    id_finca = {$id_finca},
                    finca = '{$objectMuestras->ENCABEZADO['Finca']}',
                    hectareas_fumigacion = '{$objectMuestras->ENCABEZADO['Hectáreas de fumigación']}',
                    notificacion = '{$objectMuestras->ENCABEZADO['Notificación']}',
                    programa = 'FOLIAR',
                    tipo_ciclo = '{$objectMuestras->ENCABEZADO['Tipo de Ciclo']}',
                    num_ciclo = '{$objectMuestras->ENCABEZADO["Ciclo #"]}',
                    dosis_agua = '{$objectMuestras->{"ACEITE Y AGUA"}['Dosis 2']}',
                    id_fumigadora = '{$objectMuestras->ENCABEZADO['Fumigadora - id']}',
                    fumigadora = '{$objectMuestras->ENCABEZADO['Fumigadora']}',
                    galones = '{$objectMuestras->ENCABEZADO['Galones']}',
                    precio_operacion = '{$precio_operacion}',
                    id_piloto = '{$objectMuestras->ENCABEZADO['Piloto - id']}',
                    piloto = '{$objectMuestras->ENCABEZADO['Piloto']}',
                    id_placa = '{$objectMuestras->ENCABEZADO['Piloto - id_placa']}',
                    placa_avion = '{$objectMuestras->ENCABEZADO['Placa avión']}',
                    hora_salida = '{$objectMuestras->ENCABEZADO['Hora de salida']}',
                    hora_llegada = '{$objectMuestras->ENCABEZADO['Hora de llegada']}',
                    motivo = '{$objectMuestras->ENCABEZADO['Motivo de atraso']}',
                    observaciones = '{$objectMuestras->{"OBSERVACIONES GENERALES"}['Observaciones Generales']}',
                    json = '{$objectMuestras->json}'";
        D($sql);
        $db->query($sql);
        $id_ciclo_foliar = $db->getLastID();
    }

    if($insecticidas > 0 || $erwinia > 0){
        $precio_operacion = (float) $objectMuestras->ENCABEZADO['Precio operación'];
        if(in_array($programa, ['SIGATOKA', 'SIGATOKA_FOLIAR']) || $fungicidas > 0 || $coadyuvantes > 0) $precio_operacion = 0;

        $sql = "INSERT INTO cycle_application_reporte SET
                    fecha_programada = '{$objectMuestras->ENCABEZADO['Fecha Programación']}',
                    fecha_real = '{$objectMuestras->ENCABEZADO['Fecha Real']}',
                    hora = '{$objectMuestras->ENCABEZADO['Hora']}',
                    semana = getWeek('{$fecha_real}'),
                    anio = getYear('{$fecha_real}'),
                    id_finca = {$id_finca},
                    finca = '{$objectMuestras->ENCABEZADO['Finca']}',
                    hectareas_fumigacion = '{$objectMuestras->ENCABEZADO['Hectáreas de fumigación']}',
                    notificacion = '{$objectMuestras->ENCABEZADO['Notificación']}',
                    programa = 'PLAGAS',
                    galones = '{$objectMuestras->ENCABEZADO['Galones']}',
                    tipo_ciclo = '{$objectMuestras->ENCABEZADO['Tipo de Ciclo']}',
                    num_ciclo = '{$objectMuestras->ENCABEZADO["Ciclo #"]}',
                    dosis_agua = '{$objectMuestras->{"ACEITE Y AGUA"}['Dosis 2']}',
                    id_fumigadora = '{$objectMuestras->ENCABEZADO['Fumigadora - id']}',
                    fumigadora = '{$objectMuestras->ENCABEZADO['Fumigadora']}',
                    precio_operacion = '{$precio_operacion}',
                    id_piloto = '{$objectMuestras->ENCABEZADO['Piloto - id']}',
                    piloto = '{$objectMuestras->ENCABEZADO['Piloto']}',
                    id_placa = '{$objectMuestras->ENCABEZADO['Piloto - id_placa']}',
                    placa_avion = '{$objectMuestras->ENCABEZADO['Placa avión']}',
                    hora_salida = '{$objectMuestras->ENCABEZADO['Hora de salida']}',
                    hora_llegada = '{$objectMuestras->ENCABEZADO['Hora de llegada']}',
                    motivo = '{$objectMuestras->ENCABEZADO['Motivo de atraso']}',
                    observaciones = '{$objectMuestras->{"OBSERVACIONES GENERALES"}['Observaciones Generales']}',
                    json = '{$objectMuestras->json}'";
        D($sql);
        $db->query($sql);
        $id_ciclo_plagas = $db->getLastID();
    }

    if($objectMuestras->{"ACEITE Y AGUA"}["id_proveedor"] > 0){
        $id_producto = (int) $objectMuestras->{"ACEITE Y AGUA"}["id_aceite"];
        $nombre = $objectMuestras->{"ACEITE Y AGUA"}["Aceite"];
        $precio = $db->queryOne("SELECT precio FROM products_price WHERE id_producto = $id_producto AND fecha <= '{$fecha_real}' ORDER BY fecha DESC");
        $id_proveedor = (int) $objectMuestras->{"ACEITE Y AGUA"}["id_proveedor"];
        $dosis = $objectMuestras->{"ACEITE Y AGUA"}["Dosis 1"];
        $num_dosis = $objectMuestras->{"ACEITE Y AGUA"}["num_dosis 1"];
        $cantidad = $objectMuestras->{"ACEITE Y AGUA"}["Cantidad 1"];

        $sql_prod = "INSERT INTO cycle_application_reporte_productos SET
                        id_cycle_application_reporte = $id_ciclo_sigatoka,
                        id_tipo_producto = 6,
                        id_proveedor = {$id_proveedor},
                        id_producto = {$id_producto},
                        producto = '{$nombre}',
                        dosis = '{$dosis}',
                        num_dosis = '{$num_dosis}',
                        precio = '{$precio}',
                        cantidad = '{$cantidad}'";
        D($sql_prod);
        $db->query($sql_prod);
    }

    for($x = 1; $x <= 5; $x++){
        if($objectMuestras->ENCABEZADO["Sector {$x}"] != ''){
            $sector = $objectMuestras->ENCABEZADO["Sector {$x}"];
            $id_sector = $db->queryOne("SELECT id FROM fincas_sectores WHERE id_finca = $id_finca AND sector = '{$sector}'");
            $ha = $objectMuestras->ENCABEZADO["hect_{$x}"];

            if($id_ciclo_sigatoka){
                $sql_sector = "INSERT INTO cycle_application_reporte_sectores SET
                                    id_cycle_application_reporte = $id_ciclo_sigatoka,
                                    id_sector = {$id_sector},
                                    sector = '{$sector}',
                                    hectareas = '{$ha}'";
                D($sql_sector);
                $db->query($sql_sector);
            }

            if($id_ciclo_plagas){
                $sql_sector = "INSERT INTO cycle_application_reporte_sectores SET
                                    id_cycle_application_reporte = $id_ciclo_plagas,
                                    id_sector = {$id_sector},
                                    sector = '{$sector}',
                                    hectareas = '{$ha}'";
                D($sql_sector);
                $db->query($sql_sector);
            }

            if($id_ciclo_foliar){
                $sql_sector = "INSERT INTO cycle_application_reporte_sectores SET
                                    id_cycle_application_reporte = $id_ciclo_foliar,
                                    id_sector = {$id_sector},
                                    sector = '{$sector}',
                                    hectareas = '{$ha}'";
                D($sql_sector);
                $db->query($sql_sector);
            }
        }
    }

    for($x = 1; $x <= 2; $x++){
        if($objectMuestras->FUNGICIDAS["Fungicida {$x} - id"] > 0){
            $id_producto = (int) $objectMuestras->FUNGICIDAS["Fungicida {$x} - id"];
            $nombre = $objectMuestras->FUNGICIDAS["Fungicida {$x}"];
            $precio = (float) $db->queryOne("SELECT precio FROM products_price WHERE id_producto = $id_producto AND fecha <= '{$fecha_real}' ORDER BY fecha DESC");
            $dosis = (float) $objectMuestras->FUNGICIDAS["Dosis {$x}"];
            $num_dosis = $objectMuestras->FUNGICIDAS["num_dosis {$x}"];
            $cantidad = (float) $objectMuestras->FUNGICIDAS["Cantidad {$x}"];
            $ha = round($dosis * $cantidad, 2);
            $total = round($precio * $cantidad, 2);
            $id_proveedor = $db->queryOne("SELECT id_proveedor FROM products WHERE id = $id_producto");

            $sql_prod = "INSERT INTO cycle_application_reporte_productos SET
                            id_cycle_application_reporte = $id_ciclo_sigatoka,
                            id_tipo_producto = 4,
                            id_proveedor = '{$id_proveedor}',
                            id_producto = {$id_producto},
                            producto = '{$nombre}',
                            dosis = '{$dosis}',
                            num_dosis = '{$num_dosis}',
                            precio = '{$precio}',
                            cantidad = '{$cantidad}',
                            ha = '{$ha}',
                            total = '{$total}'";
            D($sql_prod);
            $db->query($sql_prod);
        }
    }

    for($x = 1; $x <= 2; $x++){
        if($objectMuestras->INSECTICIDA["id_insecticida_{$x}"] > 0){
            $id_producto = (int) $objectMuestras->INSECTICIDA["id_insecticida_{$x}"];
            $nombre = $objectMuestras->INSECTICIDA["Insecticida {$x}"];
            $precio = (float) $db->queryOne("SELECT precio FROM products_price WHERE id_producto = $id_producto AND fecha <= '{$fecha_real}' ORDER BY fecha DESC");
            $dosis = (float) $objectMuestras->INSECTICIDA["Dosis {$x}"];
            $num_dosis = $objectMuestras->INSECTICIDA["num_dosis {$x}"];
            $cantidad = (float) $objectMuestras->INSECTICIDA["Cantidad {$x}"];
            $ha = round($dosis * $cantidad, 2);
            $total = round($precio * $cantidad, 2);
            $id_proveedor = $db->queryOne("SELECT id_proveedor FROM products WHERE id = $id_producto");

            $sql_prod = "INSERT INTO cycle_application_reporte_productos SET
                            id_cycle_application_reporte = $id_ciclo_plagas,
                            id_tipo_producto = 5,
                            id_proveedor = '{$id_proveedor}',
                            id_producto = {$id_producto},
                            producto = '{$nombre}',
                            dosis = '{$dosis}',
                            num_dosis = '{$num_dosis}',
                            precio = '{$precio}',
                            cantidad = '{$cantidad}',
                            ha = '{$ha}',
                            total = '{$total}'";
            D($sql_prod);
            $db->query($sql_prod);
        }
    }

    for($x = 1; $x <= 7; $x++){
        if($objectMuestras->COADYUVANTES["id_emulsificante_{$x}"] > 0){
            $id_producto = (int) $objectMuestras->COADYUVANTES["id_emulsificante_{$x}"];
            $nombre = $objectMuestras->COADYUVANTES["Emulsificante {$x}"];
            $precio = (float) $db->queryOne("SELECT precio FROM products_price WHERE id_producto = $id_producto AND fecha <= '{$fecha_real}' ORDER BY fecha DESC");
            $dosis = (float) $objectMuestras->COADYUVANTES["Dosis {$x}"];
            $num_dosis = $objectMuestras->COADYUVANTES["num_dosis {$x}"];
            $cantidad = (float) $objectMuestras->COADYUVANTES["Cantidad {$x}"];
            $ha = round($dosis * $cantidad, 2);
            $total = round($precio * $cantidad, 2);
            $id_proveedor = $db->queryOne("SELECT id_proveedor FROM products WHERE id = $id_producto");

            $sql_prod = "INSERT INTO cycle_application_reporte_productos SET
                            id_cycle_application_reporte = $id_ciclo_sigatoka,
                            id_tipo_producto = 2,
                            id_proveedor = '{$id_proveedor}',
                            id_producto = {$id_producto},
                            producto = '{$nombre}',
                            dosis = '{$dosis}',
                            num_dosis = '{$num_dosis}',
                            precio = '{$precio}',
                            cantidad = '{$cantidad}',
                            ha = '{$ha}',
                            total = '{$total}'";
            D($sql_prod);
            $db->query($sql_prod);
        }
    }

    for($x = 1; $x <= 7; $x++){
        if($objectMuestras->FOLIARES["id_foliar_{$x}"] > 0){
            $id_producto = (int) $objectMuestras->FOLIARES["id_foliar_{$x}"];
            $nombre = $objectMuestras->FOLIARES["Foliar {$x}"];
            $precio = (float) $db->queryOne("SELECT precio FROM products_price WHERE id_producto = $id_producto AND fecha <= '{$fecha_real}' ORDER BY fecha DESC");
            $dosis = (float) $objectMuestras->FOLIARES["Dosis {$x}"];
            $num_dosis = $objectMuestras->FOLIARES["num_dosis {$x}"];
            $cantidad = (float) $objectMuestras->FOLIARES["Cantidad {$x}"];
            $ha = round($dosis * $cantidad, 2);
            $total = round($precio * $cantidad, 2);
            $id_proveedor = $db->queryOne("SELECT id_proveedor FROM products WHERE id = $id_producto");

            $sql_prod = "INSERT INTO cycle_application_reporte_productos SET
                            id_cycle_application_reporte = $id_ciclo_foliar,
                            id_tipo_producto = 3,
                            id_proveedor = '{$id_proveedor}',
                            id_producto = {$id_producto},
                            producto = '{$nombre}',
                            dosis = '{$dosis}',
                            num_dosis = '{$num_dosis}',
                            precio = '{$precio}',
                            cantidad = '{$cantidad}',
                            ha = '{$ha}',
                            total = '{$total}'";
            D($sql_prod);
            $db->query($sql_prod);
        }
    }

    for($x = 1; $x <= 7; $x++){
        if($objectMuestras->{"CONTROL ERWINIA"}["id_erwinia_{$x}"] > 0){
            $id_producto = (int) $objectMuestras->{"CONTROL ERWINIA"}["id_erwinia_{$x}"];
            $nombre = $objectMuestras->{"CONTROL ERWINIA"}["Control Erwinia {$x}"];
            $precio = (float) $db->queryOne("SELECT precio FROM products_price WHERE id_producto = $id_producto AND fecha <= '{$fecha_real}' ORDER BY fecha DESC");
            $dosis = (float) $objectMuestras->{"CONTROL ERWINIA"}["Dosis {$x}"];
            $num_dosis = $objectMuestras->{"CONTROL ERWINIA"}["num_dosis {$x}"];
            $cantidad = (float) $objectMuestras->{"CONTROL ERWINIA"}["Cantidad {$x}"];
            $ha = round($dosis * $cantidad, 2);
            $total = round($precio * $cantidad, 2);
            $id_proveedor = $db->queryOne("SELECT id_proveedor FROM products WHERE id = $id_producto");

            $sql_prod = "INSERT INTO cycle_application_reporte_productos SET
                            id_cycle_application_reporte = $id_ciclo_plagas,
                            id_tipo_producto = 8,
                            id_proveedor = '{$id_proveedor}',
                            id_producto = {$id_producto},
                            producto = '{$nombre}',
                            dosis = '{$dosis}',
                            num_dosis = '{$num_dosis}',
                            precio = '{$precio}',
                            cantidad = '{$cantidad}',
                            ha = '{$ha}',
                            total = '{$total}'";
            D($sql_prod);
            $db->query($sql_prod);
        }
    }

    for($x = 1; $x <= 7; $x++){
        if($objectMuestras->{"OTRO SIGATOKA"}["id_sigatoka_{$x}"] > 0){
            $id_producto = (int) $objectMuestras->{"OTRO SIGATOKA"}["id_sigatoka_{$x}"];
            $nombre = $objectMuestras->{"OTRO SIGATOKA"}["Otro Sigatoka {$x}"];
            $precio = (float) $db->queryOne("SELECT precio FROM products_price WHERE id_producto = $id_producto AND fecha <= '{$fecha_real}' ORDER BY fecha DESC");
            $dosis = (float) $objectMuestras->{"OTRO SIGATOKA"}["Dosis {$x}"];
            $num_dosis = $objectMuestras->{"OTRO SIGATOKA"}["num_dosis {$x}"];
            $cantidad = (float) $objectMuestras->{"OTRO SIGATOKA"}["Cantidad {$x}"];
            $ha = round($dosis * $cantidad, 2);
            $total = round($precio * $cantidad, 2);
            $id_proveedor = $db->queryOne("SELECT id_proveedor FROM products WHERE id = $id_producto");

            $sql_prod = "INSERT INTO cycle_application_reporte_productos SET
                            id_cycle_application_reporte = $id_ciclo_sigatoka,
                            id_tipo_producto = 9,
                            id_proveedor = '{$id_proveedor}',
                            id_producto = {$id_producto},
                            producto = '{$nombre}',
                            dosis = '{$dosis}',
                            num_dosis = '{$num_dosis}',
                            precio = '{$precio}',
                            cantidad = '{$cantidad}',
                            ha = '{$ha}',
                            total = '{$total}'";
            D($sql_prod);
            $db->query($sql_prod);
        }
    }
}

function getValueFromDataType($dataType, $values, $referenceNumber){
    switch ($dataType) {
        case 'GeoLocation':
            return getValueFromGeoLocation($values);
        case 'FreeText':
        case 'Date':
            return getValueFromFreeText($values);
        case 'Integer':
            return getValueFromInteger($values);
        case 'Signature':
            return getValueFromSignature($values, $referenceNumber);
        case 'Decimal':
            return getValueFromDecimal($values);
        case 'Time':
            return getValueFromTime($values);
        case 'Image':
            return getValueFromImage($values, $referenceNumber);
        default:
            D("No has agregado el tipo: ".$dataType);
            return "";
    }
}

function getValueFromGeoLocation($values){
    return isset($values[0])
        ? $values[0]["coordinates"]["latitude"].",".$values[0]["coordinates"]["longitude"]
        : "";
}

function getValueFromFreeText($values){
    return isset($values[0]) ? trim(addslashes($values[0])) : "";
}

function getValueFromInteger($values){
    return (int) isset($values[0]) ? trim(addslashes($values[0])) : "";
}

function getValueFromDecimal($values){
    return (float) isset($values[0]) ? trim(addslashes($values[0])) : "";
}

function getValueFromTime($values){
    return isset($values[0])
            ? substr($values[0]["provided"]["time"], 0, 8)
            : "";
}

function getValueFromSignature($values, $referenceNumber){
    return isset($values[0]) 
                        ? $referenceNumber ."_". $values[0]["filename"]
                        : "";
}

function getValueFromImage($values, $referenceNumber){
    $newVals = [];
    foreach($values as $val){
        $newVals[] = $referenceNumber ."_". $val["filename"];
    }
    return implode("|", $newVals);
}

function D($arreglo){
    echo "<pre>";
        print_r($arreglo);
    echo "</pre>";
}

function limpiar($String){
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
     
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);
    return $String;
}

function strtolower_utf8($string){ 
  $convert_to = array( 
    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", 
    "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", 
    "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж", 
    "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", 
    "ь", "э", "ю", "я" 
  ); 
  $convert_from = array( 
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", 
    "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", 
    "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", 
    "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ", 
    "Ь", "Э", "Ю", "Я" 
  ); 

  return str_replace($convert_from, $convert_to, $string); 
}
?>