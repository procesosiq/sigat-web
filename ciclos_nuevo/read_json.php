<?php
header('Content-Type: text/html; charset=utf-8');
// /*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/

// ini_set('max_execution_time', 300);
// ini_set('memory_limit', '-1');

// /*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/
ini_set('display_errors',1);
error_reporting(E_ALL);
include_once './../controllers/conexion.php';
/** Directorio de los JSON */
$path = realpath('./new');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName()){
        $pos2 = strpos($object->getPath(), 'new');
        
        if($pos2 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        process_json($json, $object->getFileName());
                        move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        move_image($object->getPathName(), $object->getFileName());
                        break;

                    default:
                        
                        break;
                }
            }
        }
    }
}

function move_json($file, $nameFile){
    if(!rename($file, __DIR__."/completados/$nameFile")){
        echo 'error';
    }
}

function move_image($file, $nameFile){
    if(!rename($file, __DIR__."/image/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename){
    $db = new M_Conexion();

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';;
    $user_username = $user['username'].'';;
    $referenceNumber = $json["referenceNumber"];
    $pages = $json['pages'];
    $geoStamp = $json["geoStamp"];
    $coordinates = $geoStamp["coordinates"];
    $latitude = $coordinates["latitude"];
    $longitude = $coordinates["longitude"];

    $_parts = explode("T", $json["shiftedDeviceSubmitDate"]);
    $fecha_formulario_enviado = $_parts[0]." ".substr($_parts[1], 0, 8);
    $pagina_nombre = "";
    $objectMuestras = new stdClass;
    $objectErros = new stdClass;
    $objectMuestras->json = $filename;
    $objectMuestras->indentifier = $user_identifier;
    $objectMuestras->referenceNumber = $referenceNumber;
    $objectMuestras->auditor = $user["username"];
    $objectMuestras->auditorName = $user["displayName"];
    $contador = 0;
    
    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $sql_muestra_causas[] = [];
        if($pagina_nombre != strtoupper(limpiar(trim($page_data['name'])))){
            $pagina_nombre = strtoupper(limpiar(trim($page_data['name'])));
            $objectMuestras->{$pagina_nombre} = [];
            $contador++;
        }
        $answers = $page_data["answers"];
        foreach($answers as $x => $answer){
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];
            $labelita = limpiar(trim(strtolower($label)));
            
            $objectMuestras->{$pagina_nombre}[$question] = getValueFromDataType($dataType, $values, $referenceNumber);
        }
    }

    $insecticidas = 0;
    $fungicidas = 0;
    $foliares = 0;
    $control_erwinia = 0;
    $productos = [];

    for($x = 1; $x <= 10; $x++){
        $producto = $objectMuestras->{"PRODUCTO {$x}"};
        if($producto["ID PRODUCTO"] > 0){
            if($producto["Tipo de producto"] == 'Foliar') $foliares++;
            if($producto["Tipo de producto"] == 'Fungicida') $fungicidas++;
            if($producto["Tipo de producto"] == 'Insecticida') $insecticidas++;
            if($producto["Tipo de producto"] == 'Control Erwinia') $control_erwinia++;

            $productos[] = $producto;
        }
    }

    $id_sigatoka = null;
    $id_foliar = null;
    $id_plagas = null;
    $id_erwinia = null;
    $programas = [];
    if($fungicidas > 0) $programas[] = "Sigatoka";
    if($insecticidas > 0) $programas[] = "Plagas";
    if($control_erwinia > 0) $programas[] = "Erwinia";
    if($foliares > 0) $programas[] = "Foliar";

    $precio_operacion = (float) $objectMuestras->{"ENCABEZADO"}['Precio operación'];
    if($fungicidas > 0 && $foliares > 0){
        $precio_operacion = $precio_operacion / 2;
    }

    $tipo_ciclo = $objectMuestras->{"ENCABEZADO"}['Tipo de Ciclo'];
    $num_ciclo = $objectMuestras->{"ENCABEZADO"}["Ciclo #"];
    $ha = (float) $objectMuestras->{"ENCABEZADO"}['Hectáreas de fumigación'];
    $id_finca = $objectMuestras->{"ENCABEZADO"}['Finca - id'];
    $fecha_real = $objectMuestras->{"ENCABEZADO"}['Fecha Real'];
    $ha_finca = (float) $db->queryOne("SELECT getHaFinca($id_finca, YEAR('{$fecha_real}'), getWeek('{$fecha_real}'), 'FUMIGACION') ha");

    if(in_array("Sigatoka", $programas)){
        $sql = "INSERT INTO `cycle_application_reporte` SET
                    fecha_programada = '{$objectMuestras->{"ENCABEZADO"}['Fecha Programación']}',
                    fecha_real = '{$fecha_real}',
                    hora = '{$objectMuestras->{"ENCABEZADO"}['Hora']}',
                    finca = '{$objectMuestras->{"ENCABEZADO"}['Finca']}',
                    id_finca = '{$id_finca}',
                    hectareas_fumigacion = '{$ha}',
                    sum_ciclo = IF('{$tipo_ciclo}' = 'CICLO', 1, ROUND($ha/$ha_finca, 2)),
                    notificacion = '{$objectMuestras->{"ENCABEZADO"}['Notificación']}',
                    programa = 'Sigatoka',
                    id_programa = 1,
                    tipo_ciclo = '{$tipo_ciclo}',
                    num_ciclo = IF('{$tipo_ciclo}' = 'CICLO','{$num_ciclo}', ROUND($ha/$ha_finca, 2)),
                    id_fumigadora = '{$objectMuestras->{"ENCABEZADO"}['Fumigadora - id']}',
                    fumigadora = '{$objectMuestras->{"ENCABEZADO"}['Fumigadora']}',
                    galones = '{$objectMuestras->{"ENCABEZADO"}['Galones']}',
                    dosis_agua = '{$objectMuestras->{"ENCABEZADO"}['Agua (Dosis)']}',
                    precio_operacion = $precio_operacion,
                    id_piloto =  '{$objectMuestras->{"ENCABEZADO"}['Piloto - id']}',
                    piloto = '{$objectMuestras->{"ENCABEZADO"}['Piloto']}',
                    id_placa = '{$objectMuestras->{"ENCABEZADO"}['Piloto - id_placa']}',
                    placa_avion = '{$objectMuestras->{"ENCABEZADO"}['Placa avión']}',
                    hora_salida = '{$objectMuestras->{"ENCABEZADO"}['Hora de salida']}',
                    hora_llegada = '{$objectMuestras->{"ENCABEZADO"}['Hora de llegada']}',
                    motivo = '{$objectMuestras->{"ENCABEZADO"}['Motivo de atraso']}',
                    json = '{$objectMuestras->json}',
                    semana = getWeek('{$objectMuestras->{"ENCABEZADO"}['Fecha Real']}'),
                    anio = YEAR('{$objectMuestras->{"ENCABEZADO"}['Fecha Real']}'),
                    observaciones = '{$objectMuestras->{"OBSERVACIONES GENERALES"}['Observaciones Generales']}'";
        D($sql);
        $db->query($sql);
        $id_sigatoka = $db->getLastID();
    }

    if(in_array("Foliar", $programas)){
        $sql = "INSERT INTO `cycle_application_reporte` SET
                    fecha_programada = '{$objectMuestras->{"ENCABEZADO"}['Fecha Programación']}',
                    fecha_real = '{$objectMuestras->{"ENCABEZADO"}['Fecha Real']}',
                    hora = '{$objectMuestras->{"ENCABEZADO"}['Hora']}',
                    finca = '{$objectMuestras->{"ENCABEZADO"}['Finca']}',
                    id_finca = '{$objectMuestras->{"ENCABEZADO"}['Finca - id']}',
                    hectareas_fumigacion = '{$objectMuestras->{"ENCABEZADO"}['Hectáreas de fumigación']}',
                    notificacion = '{$objectMuestras->{"ENCABEZADO"}['Notificación']}',
                    programa = 'Foliar',
                    id_programa = 3,
                    tipo_ciclo = '{$objectMuestras->{"ENCABEZADO"}['Tipo de Ciclo']}',
                    num_ciclo = IF('{$tipo_ciclo}' = 'CICLO','{$num_ciclo}', ROUND($ha/$ha_finca, 2)),
                    sum_ciclo = IF('{$tipo_ciclo}' = 'CICLO', 1, ROUND($ha/$ha_finca, 2)),
                    id_fumigadora = '{$objectMuestras->{"ENCABEZADO"}['Fumigadora - id']}',
                    fumigadora = '{$objectMuestras->{"ENCABEZADO"}['Fumigadora']}',
                    galones = '{$objectMuestras->{"ENCABEZADO"}['Galones']}',
                    dosis_agua = '{$objectMuestras->{"ENCABEZADO"}['Agua (Dosis)']}',
                    precio_operacion = $precio_operacion,
                    id_piloto =  '{$objectMuestras->{"ENCABEZADO"}['Piloto - id']}',
                    piloto = '{$objectMuestras->{"ENCABEZADO"}['Piloto']}',
                    id_placa = '{$objectMuestras->{"ENCABEZADO"}['Piloto - id_placa']}',
                    placa_avion = '{$objectMuestras->{"ENCABEZADO"}['Placa avión']}',
                    hora_salida = '{$objectMuestras->{"ENCABEZADO"}['Hora de salida']}',
                    hora_llegada = '{$objectMuestras->{"ENCABEZADO"}['Hora de llegada']}',
                    motivo = '{$objectMuestras->{"ENCABEZADO"}['Motivo de atraso']}',
                    json = '{$objectMuestras->json}',
                    semana = getWeek('{$objectMuestras->{"ENCABEZADO"}['Fecha Real']}'),
                    anio = YEAR('{$objectMuestras->{"ENCABEZADO"}['Fecha Real']}'),
                    observaciones = '{$objectMuestras->{"OBSERVACIONES GENERALES"}['Observaciones Generales']}'";
        D($sql);
        $db->query($sql);
        $id_foliar = $db->getLastID();
    }

    if(in_array("Erwinia", $programas)){
        $sql = "INSERT INTO `cycle_application_reporte` SET
                    fecha_programada = '{$objectMuestras->{"ENCABEZADO"}['Fecha Programación']}',
                    fecha_real = '{$objectMuestras->{"ENCABEZADO"}['Fecha Real']}',
                    hora = '{$objectMuestras->{"ENCABEZADO"}['Hora']}',
                    finca = '{$objectMuestras->{"ENCABEZADO"}['Finca']}',
                    id_finca = '{$objectMuestras->{"ENCABEZADO"}['Finca - id']}',
                    hectareas_fumigacion = '{$objectMuestras->{"ENCABEZADO"}['Hectáreas de fumigación']}',
                    notificacion = '{$objectMuestras->{"ENCABEZADO"}['Notificación']}',
                    programa = 'Plagas',
                    id_programa = 5,
                    tipo_ciclo = '{$objectMuestras->{"ENCABEZADO"}['Tipo de Ciclo']}',
                    num_ciclo = IF('{$tipo_ciclo}' = 'CICLO','{$num_ciclo}', ROUND($ha/$ha_finca, 2)),
                    sum_ciclo = IF('{$tipo_ciclo}' = 'CICLO', 1, ROUND($ha/$ha_finca, 2)),
                    id_fumigadora = '{$objectMuestras->{"ENCABEZADO"}['Fumigadora - id']}',
                    fumigadora = '{$objectMuestras->{"ENCABEZADO"}['Fumigadora']}',
                    galones = '{$objectMuestras->{"ENCABEZADO"}['Galones']}',
                    dosis_agua = '{$objectMuestras->{"ENCABEZADO"}['Agua (Dosis)']}',
                    precio_operacion = ".((in_array("Sigatoka", $programas) || in_array("Foliar", $programas)) ? 0 : $precio_operacion).",
                    id_piloto =  '{$objectMuestras->{"ENCABEZADO"}['Piloto - id']}',
                    piloto = '{$objectMuestras->{"ENCABEZADO"}['Piloto']}',
                    id_placa = '{$objectMuestras->{"ENCABEZADO"}['Piloto - id_placa']}',
                    placa_avion = '{$objectMuestras->{"ENCABEZADO"}['Placa avión']}',
                    hora_salida = '{$objectMuestras->{"ENCABEZADO"}['Hora de salida']}',
                    hora_llegada = '{$objectMuestras->{"ENCABEZADO"}['Hora de llegada']}',
                    motivo = '{$objectMuestras->{"ENCABEZADO"}['Motivo de atraso']}',
                    json = '{$objectMuestras->json}',
                    semana = getWeek('{$objectMuestras->{"ENCABEZADO"}['Fecha Real']}'),
                    anio = YEAR('{$objectMuestras->{"ENCABEZADO"}['Fecha Real']}'),
                    observaciones = '{$objectMuestras->{"OBSERVACIONES GENERALES"}['Observaciones Generales']}'";
        D($sql);
        $db->query($sql);
        $id_erwinia = $db->getLastID();
    }

    if(in_array("Plagas", $programas)){
        $sql = "INSERT INTO `cycle_application_reporte` SET
                    fecha_programada = '{$objectMuestras->{"ENCABEZADO"}['Fecha Programación']}',
                    fecha_real = '{$objectMuestras->{"ENCABEZADO"}['Fecha Real']}',
                    hora = '{$objectMuestras->{"ENCABEZADO"}['Hora']}',
                    finca = '{$objectMuestras->{"ENCABEZADO"}['Finca']}',
                    id_finca = '{$objectMuestras->{"ENCABEZADO"}['Finca - id']}',
                    hectareas_fumigacion = '{$objectMuestras->{"ENCABEZADO"}['Hectáreas de fumigación']}',
                    notificacion = '{$objectMuestras->{"ENCABEZADO"}['Notificación']}',
                    programa = 'Plagas',
                    id_programa = 4,
                    tipo_ciclo = '{$objectMuestras->{"ENCABEZADO"}['Tipo de Ciclo']}',
                    num_ciclo = IF('{$tipo_ciclo}' = 'CICLO','{$num_ciclo}', ROUND($ha/$ha_finca, 2)),
                    sum_ciclo = IF('{$tipo_ciclo}' = 'CICLO', 1, ROUND($ha/$ha_finca, 2)),
                    id_fumigadora = '{$objectMuestras->{"ENCABEZADO"}['Fumigadora - id']}',
                    fumigadora = '{$objectMuestras->{"ENCABEZADO"}['Fumigadora']}',
                    galones = '{$objectMuestras->{"ENCABEZADO"}['Galones']}',
                    dosis_agua = '{$objectMuestras->{"ENCABEZADO"}['Agua (Dosis)']}',
                    precio_operacion = ".((in_array("Sigatoka", $programas) || in_array("Foliar", $programas) || in_array("Erwinia", $programas)) ? 0 : $precio_operacion).",
                    id_piloto =  '{$objectMuestras->{"ENCABEZADO"}['Piloto - id']}',
                    piloto = '{$objectMuestras->{"ENCABEZADO"}['Piloto']}',
                    id_placa = '{$objectMuestras->{"ENCABEZADO"}['Piloto - id_placa']}',
                    placa_avion = '{$objectMuestras->{"ENCABEZADO"}['Placa avión']}',
                    hora_salida = '{$objectMuestras->{"ENCABEZADO"}['Hora de salida']}',
                    hora_llegada = '{$objectMuestras->{"ENCABEZADO"}['Hora de llegada']}',
                    motivo = '{$objectMuestras->{"ENCABEZADO"}['Motivo de atraso']}',
                    json = '{$objectMuestras->json}',
                    semana = getWeek('{$objectMuestras->{"ENCABEZADO"}['Fecha Real']}'),
                    anio = YEAR('{$objectMuestras->{"ENCABEZADO"}['Fecha Real']}'),
                    observaciones = '{$objectMuestras->{"OBSERVACIONES GENERALES"}['Observaciones Generales']}'";
        D($sql);
        $db->query($sql);
        $id_plagas = $db->getLastID();
    }

    /** PRODUCTOS */
    if(in_array("Sigatoka", $programas)){
        foreach($productos as $i => $prod){
            if(in_array($prod["Tipo de producto"], ["Fungicida", "Coadyuvante", "Bioestimulante", "Aceite", "Regulador de PH"])){
                $sql = "INSERT INTO cycle_application_reporte_productos SET
                            id_cycle_application_reporte = $id_sigatoka,
                            id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = {$prod['ID PRODUCTO']}),
                            id_proveedor = {$prod['ID PROVEEDOR']},
                            id_producto = {$prod['ID PRODUCTO']},
                            producto = '{$prod['Producto']}',
                            dosis = {$prod['Dosis']},
                            precio = getPrecioProducto({$prod['ID PRODUCTO']}, '{$objectMuestras->ENCABEZADO['Fecha Real']}'),
                            cantidad = {$prod['Dosis']} * {$objectMuestras->ENCABEZADO['Hectáreas de fumigación']},
                            total = {$prod['Dosis']} * {$objectMuestras->ENCABEZADO['Hectáreas de fumigación']} * getPrecioProducto({$prod['ID PRODUCTO']}, '{$objectMuestras->ENCABEZADO['Fecha Real']}')";
                D($sql);
                $db->query($sql);
                unset($productos[$i]);
            }
        }
    }

    if(in_array("Plagas", $programas)){
        foreach($productos as $i => $prod){
            if(in_array($prod["Tipo de producto"], ["Insecticida", "Coadyuvante", "Bioestimulante", "Aceite", "Regulador de PH"])){
                $sql = "INSERT INTO cycle_application_reporte_productos SET
                            id_cycle_application_reporte = $id_plagas,
                            id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = {$prod['ID PRODUCTO']}),
                            id_proveedor = {$prod['ID PROVEEDOR']},
                            id_producto = {$prod['ID PRODUCTO']},
                            producto = '{$prod['Producto']}',
                            dosis = {$prod['Dosis']},
                            precio = getPrecioProducto({$prod['ID PRODUCTO']}, '{$objectMuestras->ENCABEZADO['Fecha Real']}'),
                            cantidad = {$prod['Dosis']} * {$objectMuestras->ENCABEZADO['Hectáreas de fumigación']},
                            total = {$prod['Dosis']} * {$objectMuestras->ENCABEZADO['Hectáreas de fumigación']} * getPrecioProducto({$prod['ID PRODUCTO']}, '{$objectMuestras->ENCABEZADO['Fecha Real']}')";
                D($sql);
                $db->query($sql);
                unset($productos[$i]);
            }
        }
    }

    if(in_array("Erwinia", $programas)){
        foreach($productos as $i => $prod){
            if(in_array($prod["Tipo de producto"], ["Control Erwinia", "Coadyuvante", "Bioestimulante", "Aceite", "Regulador de PH"])){
                $sql = "INSERT INTO cycle_application_reporte_productos SET
                            id_cycle_application_reporte = $id_erwinia,
                            id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = {$prod['ID PRODUCTO']}),
                            id_proveedor = {$prod['ID PROVEEDOR']},
                            id_producto = {$prod['ID PRODUCTO']},
                            producto = '{$prod['Producto']}',
                            dosis = {$prod['Dosis']},
                            precio = getPrecioProducto({$prod['ID PRODUCTO']}, '{$objectMuestras->ENCABEZADO['Fecha Real']}'),
                            cantidad = {$prod['Dosis']} * {$objectMuestras->ENCABEZADO['Hectáreas de fumigación']},
                            total = {$prod['Dosis']} * {$objectMuestras->ENCABEZADO['Hectáreas de fumigación']} * getPrecioProducto({$prod['ID PRODUCTO']}, '{$objectMuestras->ENCABEZADO['Fecha Real']}')";
                D($sql);
                $db->query($sql);
                unset($productos[$i]);
            }
        }
    }

    if(in_array("Foliar", $programas)){
        foreach($productos as $i => $prod){
            if(in_array($prod["Tipo de producto"], ["Foliar", "Coadyuvante", "Bioestimulante", "Aceite", "Regulador de PH"])){
                $sql = "INSERT INTO cycle_application_reporte_productos SET
                            id_cycle_application_reporte = $id_foliar,
                            id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = {$prod['ID PRODUCTO']}),
                            id_proveedor = {$prod['ID PROVEEDOR']},
                            id_producto = {$prod['ID PRODUCTO']},
                            producto = '{$prod['Producto']}',
                            dosis = {$prod['Dosis']},
                            precio = getPrecioProducto({$prod['ID PRODUCTO']}, '{$objectMuestras->ENCABEZADO['Fecha Real']}'),
                            cantidad = {$prod['Dosis']} * {$objectMuestras->ENCABEZADO['Hectáreas de fumigación']},
                            total = {$prod['Dosis']} * {$objectMuestras->ENCABEZADO['Hectáreas de fumigación']} * getPrecioProducto({$prod['ID PRODUCTO']}, '{$objectMuestras->ENCABEZADO['Fecha Real']}')";
                D($sql);
                $db->query($sql);
                unset($productos[$i]);
            }
        }
    }

    D($tipo_ciclo);
    if($tipo_ciclo == 'Ciclo'){
        $row = (object)[
            "fecha_real" => $fecha_real,
            "id_finca" => $id_finca,
            "num_ciclo" => $num_ciclo
        ];
        procesar($db, $row);
    }
}

function procesar($db, $row){
    $sql = "SELECT GROUP_CONCAT(id SEPARATOR ',') FROM ciclos_aplicacion_hist WHERE anio = YEAR('{$row->fecha_real}') AND id_finca = $row->id_finca AND num_ciclo = '{$row->num_ciclo}'";
    $ids = $db->queryOne($sql);
    if($ids){
        $sql = "DELETE FROM ciclos_aplicacion_hist_unidos WHERE id_ciclo_aplicacion IN ({$ids})";
        $db->query($sql);
    }

    $sql = "SELECT finca, num_ciclo, 
                GROUP_CONCAT(DISTINCT id SEPARATOR ',') ids,
                GROUP_CONCAT(DISTINCT programa SEPARATOR ',') programa
            FROM ciclos_aplicacion_hist hist
            WHERE tipo_ciclo = 'CICLO' AND NOT EXISTS (SELECT * FROM ciclos_aplicacion_hist_unidos WHERE id_ciclo_aplicacion = hist.id)
            GROUP BY anio, id_finca, num_ciclo, fecha_prog, ha_oper";
    $data = $db->queryAll($sql);
    D($data);
    foreach($data as $row){
        insertJoin($db, $row);
    }
}

function insertJoin($db, $row){
    $ids = explode(",", $row->ids);
    $joins = [];
    for($x = 0; $x < count($ids); $x++){
        if($x == 0){
            $joins[] = [$ids[$x]];
        }else{
            $e = false;
            $p1 = $db->queryAll("SELECT * FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = {$ids[$x]} ORDER BY id_producto");
            for($y = 0; $y < count($joins); $y++){
                $p2 = $db->queryAll("SELECT * FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = {$joins[$y][0]} ORDER BY id_producto");
                if(compareProducts($p1, $p2)){
                    $joins[$y][] = $ids[$x];
                    $e = true;
                    break;
                }
            }

            if(!$e){
                $joins[] = [$ids[$x]];
            }
        }
    }

    foreach($joins as $j){
        $join = $db->queryOne("SELECT MAX(_join)+1 FROM ciclos_aplicacion_hist_unidos");
        D("join $join");
        if(!$join) $join = 1;
        foreach($j as $id){
            $sql = "INSERT INTO ciclos_aplicacion_hist_unidos SET
                        _join = $join,
                        id_ciclo_aplicacion = '{$id}'";
            $db->query($sql);
            D($sql);
        }
    }
}

function compareProducts($p1, $p2){
    if(count($p1) != count($p2)) return false;

    // true si coinciden
    $e = true;
    for($x = 0; $x < count($p1); $x++){
        if(
            ($p1[$x]->id_producto != $p2[$x]->id_producto)
            ||
            ($p1[$x]->id_producto == $p2[$x]->id_producto && ($p1[$x]->precio != $p2[$x]->precio || $p1[$x]->dosis != $p2[$x]->dosis))
        ){
            $e = false;
            break;
        }
    }

    return $e;
}

function D($arreglo){
    echo "<pre>";
        print_r($arreglo);
    echo "</pre>";
}

function limpiar($String){
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
    
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);

    $String = str_replace("\u00C1;","Á",$String);
    $String = str_replace("\u00E1;","á",$String);
    $String = str_replace("\u00C9;","É",$String);
    $String = str_replace("\u00E9;","é",$String);
    $String = str_replace("\u00CD;","Í",$String);
    $String = str_replace("\u00ED;","í",$String);
    $String = str_replace("\u00D3;","Ó",$String);
    $String = str_replace("\u00F3;","ó",$String);
    $String = str_replace("\u00DA;","Ú",$String);
    $String = str_replace("\u00FA;","ú",$String);
    $String = str_replace("\u00DC;","Ü",$String);
    $String = str_replace("\u00FC;","ü",$String);
    $String = str_replace("\u00D1;","Ṅ",$String);
    $String = str_replace("\u00F1;","ñ",$String);

    return $String;
}

function ltl($s){
    return limpiar(trim(strtolower($s)));
}

function espacios($string){
    return limpiar(str_replace(" ", "_", $string));
}

function getValueFromDataType($dataType, $values, $referenceNumber){
    switch ($dataType) {
        case 'GeoLocation':
            return getValueFromGeoLocation($values);
        case 'FreeText':
        case 'Date':
            return getValueFromFreeText($values);
        case 'Integer':
            return getValueFromInteger($values);
        case 'Signature':
            return getValueFromSignature($values, $referenceNumber);
        case 'Decimal':
            return getValueFromDecimal($values);
        case 'Time':
            return getValueFromTime($values);
        case 'Image':
            return getValueFromImage($values, $referenceNumber);
        case 'Timestamp':
            return getValueFromTimestamp($values);
        case 'EmailAddress' : 
            return "";
        default:
            D("No has agregado el tipo: ".$dataType);
            return "";
    }
}

function getValueFromGeoLocation($values){
    return isset($values[0])
        ? $values[0]["coordinates"]["latitude"].",".$values[0]["coordinates"]["longitude"]
        : "";
}

function getValueFromFreeText($values){
    return count($values) > 1
            ? implode(",", $values)
            : isset($values[0]) 
                ? trim(addslashes($values[0])) 
                : "";
}

function getValueFromInteger($values){
    return count($values) > 0
                ? addslashes(implode(",", $values))
                : "";
}

function getValueFromDecimal($values){
    return (float) isset($values[0]) ? trim(addslashes($values[0])) : "";
}

function getValueFromTime($values){
    return isset($values[0])
                        ? explode("-", $values[0]["provided"]["time"])[0]
                        : "";
}

function getValueFromTimestamp($values){
    if(isset($values[0]["provided"]["time"])){
        $sparts = explode("T", $values[0]["provided"]["time"]);
        return $sparts[0]." ".substr($sparts[1], 0 , 8);
    }else{
        return "";
    }
}

function getValueFromSignature($values, $referenceNumber){
    return isset($values[0]) 
                        ? $referenceNumber ."_". $values[0]["filename"]
                        : "";
}

function getValueFromImage($values, $referenceNumber){
    $newVals = [];
    foreach($values as $val){
        $newVals[] = $referenceNumber ."_". $val["filename"];
    }
    return implode("|", $newVals);
}