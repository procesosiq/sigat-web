<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?> 
    <style>
        .listado{
            list-style-type: none;
            display: inline-block;
        }
        .listado > li{
            display: inline;
            margin-right: 10px;
        }
        .miniature{
            display: inline-block;
        }

        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .legendLabel{
            padding: 3px !important;
        }
    </style>
                <!-- BEGIN CONTENT BODY -->
                <div ng-app="app" class="page-content" ng-controller="clima" ng-cloak>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head" ng-init="init()">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Clima
                                <small>Estadisticas del clima</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <?php include './views/clientes_tags.php';?>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Dashboard</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <!-- BEGIN DASHBOARD STATS 1-->
                        <?php include("clima_tags_marcel.php");?>
                    <!-- BEGIN INTERACTIVE CHART PORTLET-->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Temperatura promedio</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="label_temp_min" class="label_chart"> </div>
                                    <div id="temp_min" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_temp_minima" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END INTERACTIVE CHART PORTLET-->
                    <!-- END DASHBOARD STATS 1-->
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Temperatura Maxima</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="label_temp_max" class="label_chart"> </div>
                                    <div id="temp_max" class="chart"> </div>
                                    <div class="miniature">
                                        <ul id="check_temp_maxima" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Temperatura minima</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="label_temp_min" class="label_chart"> </div>
                                    <div id="temp_min" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_temp_minima" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END DASHBOARD STATS 1-->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Evapotranspiración</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="label_precp" class="label_chart"> </div>
                                    <div id="precp" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_lluvia" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Precipitacion (mm lluvia)</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="label_precp" class="label_chart"> </div>
                                    <div id="precp" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_lluvia" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Radiacion Solar</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="label_radiacion" class="label_chart"> </div>
                                    <div id="radiacion" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_rad_solar" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>                        
                    </div>
                    <!-- END DASHBOARD STATS 1-->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Dias Sol</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="label_diassol" class="label_chart"> </div>
                                    <div id="diassol" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_dias_sol" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Humedad Relativa Maxima</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="label_humedadmax" class="label_chart"> </div>
                                    <div id="humedadmax" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_hum_max" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Humedad Relativa Maxima</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="label_humedadmax" class="label_chart"> </div>
                                    <div id="humedadmax" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_hum_max" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Humedad Relativa Minima</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="label_humedadmin" class="label_chart"> </div>
                                    <div id="humedadmin" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_hum_min" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->