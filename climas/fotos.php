<?php

	/**
	*  CLASS FROM Fincas
	*/
	class fotos 
	{
		private $conexion;
		private $session;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		public function getPhotos(){
			// $sWhere = "";
			// $sOrder = " ORDER BY id";
			// $DesAsc = "ASC";
			// $sOrder .= " {$DesAsc}";
			// if(isset($_POST)){

			// 	/*----------  ORDER BY ----------*/
				
			// 	if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
			// 		$DesAsc = $_POST['order'][0]['dir'];
			// 		$sOrder = " ORDER BY id {$DesAsc}";
			// 	}
			// 	if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
			// 		$DesAsc = $_POST['order'][0]['dir'];
			// 		$sOrder = " ORDER BY (SELECT nombre from cat_haciendas WHERE id = cat_lotes.id_cliente) {$DesAsc}";
			// 	}
			// 	if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
			// 		$DesAsc = $_POST['order'][0]['dir'];
			// 		$sOrder = " ORDER BY nombre {$DesAsc}";
			// 	}
			// 	if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
			// 		$DesAsc = $_POST['order'][0]['dir'];
			// 		$sOrder = " ORDER BY email {$DesAsc}";
			// 	}
			// 	if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
			// 		$DesAsc = $_POST['order'][0]['dir'];
			// 		$sOrder = " ORDER BY status {$DesAsc}";
			// 	}
			// 	/*----------  ORDER BY ----------*/

			// 	if(isset($_POST['search_id']) && trim($_POST['search_id']) != ""){
			// 		$sWhere .= " AND id = ".$_POST["search_id"];
			// 	}				
			// 	if((isset($_POST['search_finca']) && trim($_POST['search_finca']) != "")){
			// 		$sWhere .= " AND (SELECT nombre from cat_haciendas WHERE id = cat_lotes.id_cliente) LIKE '%".$_POST['search_finca']."%'";
			// 	}
			// 	if(isset($_POST['search_name']) && trim($_POST['search_name']) != ""){
			// 		$sWhere .= " AND nombre LIKE '%".$_POST['search_name']."%'";
			// 	}
			// 	if(isset($_POST['search_email']) && trim($_POST['search_email']) != ""){
			// 		$sWhere .= " AND email LIKE '%".$_POST['search_email']."%'";
			// 	}
			// 	if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
			// 		$sWhere .= " AND status = ".$_POST["order_status"];
			// 	}
			// }

			$sql = "SELECT malezas , evi_malezas , deshoje ,evi_deshoje ,
					deshojefito,evi_deshojefito ,enfunde,evi_enfunde,
					drenaje , evi_drenaje, plaga , evi_plaga , otrasobs , evi_otrasobs ,WEEK(fecha) AS Semana
					FROM foliar
					WHERE 
					id_usuario IN ({$this->session->logges->users}) AND 
					id_cliente = '{$this->session->client}' AND 
					id_hacienda = '{$this->session->finca}'";
					// echo $sql;
			$fotos = $this->conexion->Consultas(2, $sql);
			$datos = (object)[
					'fotos' => [],
					'semana' => []
			];
			$sem = [];
			foreach ($fotos as $key => $value) {
				$fila = (object)$fila;
				if(!is_null($value['malezas']) && $value['malezas'] != "" && $value['malezas'] != '(NULL)'){
					$value['evi_malezas'] = trim($value['evi_malezas']);
					$datos->fotos['malezas'][] = [
						'semana' => $value["Semana"],
						'type' => 'Malezas',
						'description' => $value['malezas'],
						'fotos' => (is_array($value['evi_malezas']))?$value['evi_malezas'] : ($value['evi_malezas'] != "(NULL)" && $value['evi_malezas'] != "") ? $value['evi_malezas'] : [""]
					];
					$datos->semana[] = $value["Semana"];
				}
				if(!is_null($value['deshoje']) && $value['deshoje'] != "" && $value['deshoje'] != '(NULL)'){
					$value['evi_deshoje'] = trim($value['evi_deshoje']);
					$datos->fotos['deshoje'][] = [
						'semana' => $value["Semana"],
						'type' => 'Deshoje',
						'description' => $value['deshoje'],
						'fotos' => (is_array($value['evi_deshoje']))?$value['evi_deshoje'] : ($value['evi_deshoje'] != "(NULL)" && $value['evi_deshoje'] != "") ? $value['evi_deshoje'] : [""]
					];
					$datos->semana[] = $value["Semana"];
				}
				if(!is_null($value['deshojefito']) && $value['deshojefito'] != "" && $value['deshojefito'] != '(NULL)'){
					$value['evi_deshojefito'] = trim($value['evi_deshojefito']);
					$datos->fotos['deshojefito'][] = [
						'semana' => $value["Semana"],
						'type' => 'Deshojefito',
						'description' => $value['deshojefito'],
						'fotos' => (is_array($value['evi_deshojefito']))?$value['evi_deshojefito'] : ($value['evi_deshojefito'] != "(NULL)" && $value['evi_deshojefito'] != "") ? $value['evi_deshojefito'] : [""]
					];
					$datos->semana[] = $value["Semana"];
				}
				if(!is_null($value['enfunde']) && $value['enfunde'] != "" && $value['enfunde'] != '(NULL)'){
					$value['evi_enfunde'] = trim($value['evi_enfunde']);
					$datos->fotos['enfunde'][] = [
						'semana' => $value["Semana"],
						'type' => 'Enfunde',
						'description' => $value['enfunde'],
						'fotos' => (is_array($value['evi_enfunde']))?$value['evi_enfunde'] : ($value['evi_enfunde'] != "(NULL)" && $value['evi_enfunde'] != "") ? $value['evi_enfunde'] : [""]
					];
					$datos->semana[] = $value["Semana"];
				}
				if(!is_null($value['drenaje']) && $value['drenaje'] != "" && $value['drenaje'] != '(NULL)'){
					$value['evi_drenaje'] = trim($value['evi_drenaje']);
					$datos->fotos['drenaje'][] = [
						'semana' => $value["Semana"],
						'type' => 'Drenaje',
						'description' => $value['drenaje'],
						'fotos' => (is_array($value['evi_drenaje']))?$value['evi_drenaje'] : ($value['evi_drenaje'] != "(NULL)" && $value['evi_drenaje'] != "") ? $value['evi_drenaje'] : [""]
					];
					$datos->semana[] = $value["Semana"];
				}
				if(!is_null($value['plaga']) && $value['plaga'] != "" && $value['plaga'] != '(NULL)'){
					$value['evi_plaga'] = trim($value['evi_plaga']);
					$datos->fotos['plaga'][] = [
						'semana' => $value["Semana"],
						'type' => 'Plaga',
						'description' => $value['plaga'],
						'fotos' => (is_array($value['evi_plaga']))?$value['evi_plaga'] : ($value['evi_plaga'] != "(NULL)" && $value['evi_plaga'] != "") ? $value['evi_plaga'] : [""]
					];
					$datos->semana[] = $value["Semana"];
				}
				if(!is_null($value['otrasobs']) && $value['otrasobs'] != "" && $value['otrasobs'] != '(NULL)'){
					$value['evi_otrasobs'] = trim($value['evi_otrasobs']);
					$datos->fotos['otrasobs'][] = [
						'semana' => $value["Semana"],
						'type' => 'Obs',
						'description' => $value['otrasobs'],
						'fotos' => (is_array($value['evi_otrasobs']))?$value['evi_otrasobs'] : ($value['evi_otrasobs'] != "(NULL)" && $value['evi_otrasobs'] != "") ? $value['evi_otrasobs'] : [""]
					];
					$datos->semana[] = $value["Semana"];
				}
			}

			return $datos;
		}

	}
?>
