<?php
	/**
	*  CLASS FROM PRODUCTOS
	*/
	class CiclosAplicacionHistorico 
	{
		private $db;
		private $session;
		
		public function __construct()
		{
			$this->db = new M_Conexion();
        	$this->session = Session::getInstance();
		}
		
		public function tecnico(){
			$response = new stdClass;
			$sql = "SELECT *
					FROM ciclos_aplicacion_historico
					WHERE YEAR(fecha_real) = YEAR(CURRENT_DATE)";
			$response->table = $this->db->queryAll($sql);
			$response->table_real = $this->db->queryAll($sql);
			$response->gerentes = $this->db->queryAllSpecial("SELECT id , nombre AS label FROM cat_gerentes WHERE status > 0");
			print_r($response->gerentes[0][0]);
			print_r($response->gerentes);
			$response->fincas = $this->db->queryAllSpecial("SELECT FINCA AS id, FINCA AS label FROM ciclos_aplicacion_historico GROUP BY FINCA");
			return json_encode($response);
		}

		public function economico(){
			$response = new stdClass;
			$sql = "SELECT *, (Ha_1 + Ha_2) as Ha_COCTEL
					FROM ciclos_aplicacion_historico";
			$response->table = $this->db->queryAll($sql);
			$response->table_real = $this->db->queryAll($sql);
			$response->fincas = $this->db->queryAllSpecial("SELECT FINCA AS id, FINCA AS label FROM ciclos_aplicacion_historico GROUP BY FINCA");

			return json_encode($response);
		}
	}
?>
