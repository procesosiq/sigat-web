"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Images = function (_React$Component) {
    _inherits(Images, _React$Component);

    function Images(props) {
        _classCallCheck(this, Images);

        return _possibleConstructorReturn(this, (Images.__proto__ || Object.getPrototypeOf(Images)).call(this, props));
    }

    _createClass(Images, [{
        key: "render",
        value: function render() {
            var classType = "cbp-item " + this.props.images.position + " " + this.props.images.type;
            return React.createElement(
                "div",
                { ref: "contenedorImages", className: classType },
                React.createElement(
                    "a",
                    { className: "cbp-caption cbp-singlePageInline", "data-title": this.props.images.type, rel: "nofollow" },
                    React.createElement(
                        "div",
                        { className: "cbp-caption-defaultWrap" },
                        React.createElement("img", { src: this.props.images.image, alt: "" })
                    ),
                    React.createElement(
                        "div",
                        { className: "cbp-caption-activeWrap" },
                        React.createElement(
                            "div",
                            { className: "cbp-l-caption-alignLeft" },
                            React.createElement(
                                "div",
                                { className: "cbp-l-caption-body" },
                                React.createElement(
                                    "div",
                                    { className: "cbp-l-caption-title" },
                                    this.props.images.type
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);

    return Images;
}(React.Component);

var Filters = function (_React$Component2) {
    _inherits(Filters, _React$Component2);

    function Filters(props) {
        _classCallCheck(this, Filters);

        return _possibleConstructorReturn(this, (Filters.__proto__ || Object.getPrototypeOf(Filters)).call(this, props));
    }

    _createClass(Filters, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { "data-filter": this.props.filter, className: "cbp-filter-item-active cbp-filter-item btn blue btn-outline uppercase" },
                this.props.filterText
            );
        }
    }]);

    return Filters;
}(React.Component);

var ListImages = function (_React$Component3) {
    _inherits(ListImages, _React$Component3);

    function ListImages(props) {
        _classCallCheck(this, ListImages);

        var _this3 = _possibleConstructorReturn(this, (ListImages.__proto__ || Object.getPrototypeOf(ListImages)).call(this, props));

        _this3.listImages = [];
        return _this3;
    }

    _createClass(ListImages, [{
        key: "componentDidMount",
        value: function componentDidMount() {
            var portafolio = this.refs.gallery;
            $(portafolio).cubeportfolio({
                filters: '#js-filters-lightbox-gallery',
                loadMore: '#js-loadMore-lightbox-gallery',
                loadMoreAction: 'click',
                layoutMode: 'grid',
                mediaQueries: [{
                    width: 1500,
                    cols: 5
                }, {
                    width: 1100,
                    cols: 4
                }, {
                    width: 800,
                    cols: 3
                }, {
                    width: 480,
                    cols: 2
                }, {
                    width: 320,
                    cols: 1
                }],
                defaultFilter: '*',
                animationType: 'rotateSides',
                gapHorizontal: 10,
                gapVertical: 10,
                gridAdjustment: 'responsive',
                caption: 'zoom',
                displayType: 'sequentially',
                displayTypeSpeed: 100,

                // lightbox
                lightboxDelegate: '.cbp-lightbox',
                lightboxGallery: true,
                lightboxTitleSrc: 'data-title',
                lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

                // singlePageInline
                singlePageInlineDelegate: '.cbp-singlePageInline',
                singlePageInlinePosition: 'below',
                singlePageInlineInFocus: true
            });
        }
    }, {
        key: "render",
        value: function render() {
            var filters = this.props.tittles.map(function (element, i) {
                return React.createElement(Filters, { filter: "." + element, filterText: element });
            });
            var images = this.props.images.map(function (element, i) {
                return React.createElement(Images, { images: element });
            });
            var AllFilter = '';
            if (this.props.allfilter) {
                AllFilter = React.createElement(Filters, { filter: "*", filterText: "Todas" });
            }
            return React.createElement(
                "div",
                { className: "portfolio-content portfolio-3" },
                React.createElement(
                    "div",
                    { className: "clearfix" },
                    React.createElement(
                        "div",
                        { id: "js-filters-lightbox-gallery", className: "cbp-l-filters-button cbp-l-filters-left" },
                        AllFilter,
                        filters
                    )
                ),
                React.createElement(
                    "div",
                    { ref: "gallery", id: "js-grid-lightbox-gallery", className: "cbp" },
                    images
                )
            );
        }
    }]);

    return ListImages;
}(React.Component);
