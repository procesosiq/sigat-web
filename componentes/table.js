"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Tabla = function (_React$Component) {
    _inherits(Tabla, _React$Component);

    function Tabla(props) {
        _classCallCheck(this, Tabla);

        var _this = _possibleConstructorReturn(this, (Tabla.__proto__ || Object.getPrototypeOf(Tabla)).call(this, props));

        _this.angular = angular;
        return _this;
    }

    _createClass(Tabla, [{
        key: 'testAngular',
        value: function testAngular() {
            var controller = this.props.controller;
            var callback = this.props.callback;
        }
    }, {
        key: 'sort',
        value: function sort(mode) {
            var data = [];
            if (mode == 'finca') {
                this.props.data = this.props.data.sort(function (a, b) {
                    var x = a.name.toLowerCase();
                    var y = b.name.toLowerCase();
                    return x < y ? -1 : x > y ? 1 : 0;
                });
            } else if (mode == 'promedio') {
                this.props.data = this.props.data.sort(function (a, b) {
                    return a - b;
                });
            }

            this.data = this.setState({
                data: this.props.data
            });
        }
    }, {
        key: 'callback',
        value: function callback(e, idFinca) {
            console.log(this);
            console.log(this.angular);
            var scope = this.angular.element(document.getElementById('agroaudit')).scope();
            scope[this.props.callback](1, idFinca, 0, 0, 0, 0);
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {}
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            console.log(this.props.data);
            return React.createElement(
                'table',
                { className: 'table table-hover table-light' },
                React.createElement(
                    'thead',
                    null,
                    React.createElement(
                        'tr',
                        null,
                        React.createElement(
                            'td',
                            null,
                            React.createElement(
                                'a',
                                { onClick: this.sort.bind(this, 'finca') },
                                'Finca'
                            )
                        ),
                        React.createElement(
                            'td',
                            null,
                            React.createElement(
                                'a',
                                { onClick: this.sort.bind(this, 'promedio') },
                                'Promedio'
                            )
                        ),
                        React.createElement('td', null)
                    )
                ),
                React.createElement(
                    'tbody',
                    null,
                    this.props.data.map(function (e) {
                        return React.createElement(
                            'tr',
                            null,
                            React.createElement(
                                'td',
                                null,
                                e.finca
                            ),
                            React.createElement(
                                'td',
                                null,
                                e.promedio
                            ),
                            React.createElement(
                                'td',
                                null,
                                React.createElement(
                                    'button',
                                    { onClick: _this2.callback.bind(_this2, e.idFinca), className: 'btn green-jungle' },
                                    'Labores'
                                )
                            )
                        );
                    })
                )
            );
        }
    }]);

    return Tabla;
}(React.Component);
