<?php
include 'class.sesion.php';
include 'conexion.php';
class Graficas_S_C{
	
	private $conexion;
	private $session;
	
	public function __construct() {
        $this->conexion = new M_Conexion();
        $this->session = Session::getInstance();
        // $this->session->client = 1;
    }

    public function ConsultasCeroSemanasToken($token, $tipo,$tipo_semana){
    	$sql_get_info = "SELECT * FROM informe WHERE token = '{$token}'";
    	$result = $this->conexion->Consultas(2, $sql_get_info);
    	$id_usuario = $result[0]["id_usuario"];
    	$id_cliente = $result[0]["id_cliente"];
    	$id_finca = $result[0]["id_hacienda"];
    	$ids_membresia = $result[0]["ids_membresia"];
		// print_r($this->db2_conn_error()xion);
		$sql = "SELECT YEAR(prin.fecha) AS anoo , foco 
					FROM muestras_hacienda_detalle_3M as det
					INNER JOIN muestras_haciendas_3M as prin on det.id_Mhacienda = prin.id
				WHERE YEAR(prin.fecha) > 0 
				AND det.id_usuario IN ({$ids_membresia})
				AND det.id_cliente =  '{$id_cliente}'
				AND det.id_hacienda =  '{$id_finca}'
				GROUP BY foco ,YEAR(fecha) 
				ORDER BY anoo";
				// echo $sql;
				// return;
			$res = $this->conexion->link->query($sql);
			$datos = array();
			$min = [];
			$max = [];
			while($fila = $res->fetch_assoc()){
				
				if($tipo=='HMVLDQMEN5'){ 
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					foco,
					AVG(IF(hoja_mas_vieja_libre_quema_menor>0,hoja_mas_vieja_libre_quema_menor,NULL)) AS hojaViejaLibreQuemaMenor
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."'
					AND muestras_haciendas_3M.id_usuario IN ({$ids_membresia})
					AND muestras_haciendas_3M.id_cliente = '{$id_cliente}'
					AND muestras_haciendas_3M.id_hacienda = '{$id_finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["0"][] = [0 , 11];
					$datos["0"][] = [52 , 11];
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMenor"]);
						$min[] = $fila2["hojaViejaLibreQuemaMenor"];
						$max[] = $fila2["hojaViejaLibreQuemaMenor"];
					}
				}
				if($tipo=="HMVLDE"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(hoja_mas_vieja_de_estrias>0,hoja_mas_vieja_de_estrias,NULL)) AS hojaViejaLibreEstrias,
					foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario IN ({$ids_membresia})
					AND muestras_haciendas_3M.id_cliente = '{$id_cliente}'
					AND muestras_haciendas_3M.id_hacienda = '{$id_finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["0"][] = [0 , 6.5];
					$datos["0"][] = [52 , 6.5];
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreEstrias"]);
						$min[] = $fila2["hojaViejaLibreEstrias"];
						$max[] = $fila2["hojaViejaLibreEstrias"];
					}
				}
				if($tipo=="HOJA4"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(total_hoja_4>0,100,0)) AS hojaLibreCirugias,foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario IN ({$ids_membresia})
					AND muestras_haciendas_3M.id_cliente = '{$id_cliente}'
					AND muestras_haciendas_3M.id_hacienda = '{$id_finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaLibreCirugias"]);
						$min[] = $fila2["hojaLibreCirugias"];
						$max[] = $fila2["hojaLibreCirugias"];
					}
				}
				if($tipo=="HOJA3"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(total_hoja_3>0,100,0)) AS hojaViejaLibreQuemaMayor,
					foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario IN ({$ids_membresia})
					AND muestras_haciendas_3M.id_cliente = '{$id_cliente}'
					AND muestras_haciendas_3M.id_hacienda = '{$id_finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMayor"]);
						$min[] = $fila2["hojaViejaLibreQuemaMayor"];
						$max[] = $fila2["hojaViejaLibreQuemaMayor"];
					}
				}
				if($tipo=="HOJA5"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(total_hoja_5>0,100,0)) AS hojaViejaLibreQuemaMayor,
					foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario IN ({$ids_membresia})
					AND muestras_haciendas_3M.id_cliente = '{$id_cliente}'
					AND muestras_haciendas_3M.id_hacienda = '{$id_finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMayor"]);
						$min[] = $fila2["hojaViejaLibreQuemaMayor"];
						$max[] = $fila2["hojaViejaLibreQuemaMayor"];
					}
				}
				if($tipo=="HOJTOT"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(hojas_totales>0,hojas_totales,NULL)) AS hojasTotales,foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario IN ({$ids_membresia})
					AND muestras_haciendas_3M.id_cliente = '{$id_cliente}'
					AND muestras_haciendas_3M.id_hacienda = '{$id_finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["0"][] = [0 , 11];
					$datos["0"][] = [52 , 11];
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojasTotales"]);
						$min[] = $fila2["hojasTotales"];
						$max[] = $fila2["hojasTotales"];
					}
				}
			}
			
			$response = (object)[];
			$response->datos = (array)$datos;
			$response->max = max($max);
			$response->min = is_null(min($min))?0:min($min);

			return (array)$response;
	}
	
	public function ConsultasCeroSemanas($tipo,$tipo_semana){
		// print_r($this->db2_conn_error()xion);
		$sql = "SELECT YEAR(prin.fecha) AS anoo , foco 
					FROM muestras_hacienda_detalle_3M as det
					INNER JOIN muestras_haciendas_3M as prin on det.id_Mhacienda = prin.id
				WHERE YEAR(prin.fecha) > 0 
				AND det.id_usuario IN ({$this->session->logges->users}) 
				AND det.id_cliente =  '{$this->session->client}'
				AND det.id_hacienda =  '{$this->session->finca}'
				GROUP BY foco ,YEAR(fecha) 
				ORDER BY anoo";
				// echo $sql;
				// return;
			$res = $this->conexion->link->query($sql);
			$datos = array();
			while($fila = $res->fetch_assoc()){
				
				if($tipo=='HMVLDQMEN5'){ 
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					foco,
					AVG(IF(hoja_mas_vieja_libre_quema_menor>0,hoja_mas_vieja_libre_quema_menor,NULL)) AS hojaViejaLibreQuemaMenor
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."'
					AND muestras_haciendas_3M.id_usuario IN ({$this->session->logges->users}) 
					AND muestras_haciendas_3M.id_cliente = '{$this->session->client}'
					AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["0"][] = [0 , 11];
					$datos["0"][] = [52 , 11];
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						if($fila2["hojaViejaLibreQuemaMenor"] > 0){
							$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMenor"]);
							$min[] = $fila2["hojaViejaLibreQuemaMenor"];
							$max[] = $fila2["hojaViejaLibreQuemaMenor"];
						}
					}
				}
				if($tipo=="HMVLDE"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(hoja_mas_vieja_de_estrias>0,hoja_mas_vieja_de_estrias,NULL)) AS hojaViejaLibreEstrias,
					foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario IN ({$this->session->logges->users}) 
					AND muestras_haciendas_3M.id_cliente = '{$this->session->client}'
					AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["0"][] = [0 , 6.5];
					$datos["0"][] = [52 , 6.5];
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						if($fila2["hojaViejaLibreEstrias"] > 0){
							$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreEstrias"]);
							$min[] = $fila2["hojaViejaLibreEstrias"];
							$max[] = $fila2["hojaViejaLibreEstrias"];
						}
					}
				}
				if($tipo=="HOJA4"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(total_hoja_4 > 0 , 100 , 0)) AS hojaLibreCirugias , foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario IN ({$this->session->logges->users}) 
					AND muestras_haciendas_3M.id_cliente = '{$this->session->client}'
					AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					// print $sql2;
					$res2 = $this->conexion->link->query($sql2);
					$datos["0"][] = [0 , 0];
					$datos["0"][] = [52 , 0];
					while($fila2 = $res2->fetch_assoc()){
						// if($fila2["hojaLibreCirugias"] > 0){
							$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaLibreCirugias"]);
							$min[] = $fila2["hojaLibreCirugias"];
							$max[] = $fila2["hojaLibreCirugias"];
						// }
					}
					// AVG(IF(total_hoja_4>0,total_hoja_4,NULL)) AS hojaLibreCirugias,foco
				}
				if($tipo=="HOJA3"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(total_hoja_3 > 0 , 100 , 0)) AS hojaViejaLibreQuemaMayor , foco
					foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario IN ({$this->session->logges->users}) 
					AND muestras_haciendas_3M.id_cliente = '{$this->session->client}'
					AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					$datos["0"][] = [0 , 0];
					$datos["0"][] = [52 , 0];
					while($fila2 = $res2->fetch_assoc()){
						// if($fila2["hojaViejaLibreQuemaMayor"] > 0){
							$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMayor"]);
							$min[] = $fila2["hojaViejaLibreQuemaMayor"];
							$max[] = $fila2["hojaViejaLibreQuemaMayor"];
						// }
						// AVG(IF(total_hoja_3>0,total_hoja_3,NULL)) AS hojaViejaLibreQuemaMayor,
					}
				}
				if($tipo=="HOJA5"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(total_hoja_5 > 0 , 100 , 0)) AS hojaViejaLibreQuemaMayor , foco
					foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario IN ({$this->session->logges->users}) 
					AND muestras_haciendas_3M.id_cliente = '{$this->session->client}'
					AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["0"][] = [0 , 0];
					$datos["0"][] = [52 , 0];
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						// if($fila2["hojaViejaLibreQuemaMayor"] > 0){
							$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMayor"]);
							$min[] = $fila2["hojaViejaLibreQuemaMayor"];
							$max[] = 100;
						// }
					}
					// AVG(IF(total_hoja_5>0,total_hoja_5,NULL)) AS hojaViejaLibreQuemaMayor,
				}
				if($tipo=="HOJTOT"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(hojas_totales>0,hojas_totales,NULL)) AS hojasTotales,foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario IN ({$this->session->logges->users}) 
					AND muestras_haciendas_3M.id_cliente = '{$this->session->client}'
					AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["0"][] = [0 , 11];
					$datos["0"][] = [52 , 11];
					// print_r($sql2);
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						if($fila2["hojasTotales"] > 0){
							$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojasTotales"]);
							$min[] = $fila2["hojasTotales"];
							$max[] = $fila2["hojasTotales"];
						}
					}
				}
			}
			$response = (object)[];
			$response->datos = (array)$datos;
			$response->max = max($max);
			$response->min = is_null(min($min))?0:min($min);

			return (array)$response;
	}
	public function Grafica_semana($tipo,$tipo_semana){
		$datos = array();
		$datos = $this->ConsultasCeroSemanas($tipo,$tipo_semana);
		return json_encode($datos);
	}
	public function Grafica_semanaToken($token,$tipo,$tipo_semana){
		$datos = array();
		$datos = $this->ConsultasCeroSemanasToken($token,$tipo,$tipo_semana);
		return json_encode($datos);
	}
}

$postdata = (object)json_decode(file_get_contents("php://input"));
if($postdata->opt == "HMVLDQMEN5"){ #HOJA MAS VIEJA LIBRE DE QUEMA MENOR A 5%
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_C;
			echo $retval->Grafica_semanaToken($postdata->token, "HMVLDQMEN5", 0);
		}
	}else{
		$retval = new Graficas_S_C;
		echo $retval->Grafica_semana("HMVLDQMEN5",0);
	}
}
else if($postdata->opt == "HMVLDE"){  #hoja mas vieja libre de estias
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_C;
			echo $retval->Grafica_semanaToken($postdata->token, "HMVLDE", 0);
		}
	}else{
		$retval = new Graficas_S_C;
		echo $retval->Grafica_semana("HMVLDE",0);
	}
}
else if($postdata->opt == "HOJA4"){  #hojas libres de cirugias 
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_C;
			echo $retval->Grafica_semanaToken($postdata->token, "HOJA4", 0);
		}
	}else{
		$retval = new Graficas_S_C;
		echo $retval->Grafica_semana("HOJA4",0);
	}
}
else if($postdata->opt == "HOJA3"){  #HOJA MAS VIEJA LIBRE DE QUEMA MAYOR A 5%
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_C;
			echo $retval->Grafica_semanaToken($postdata->token, "HOJA3", 0);
		}
	}else{
		$retval = new Graficas_S_C;
		echo $retval->Grafica_semana("HOJA3",0);
	}
}
else if($postdata->opt == "HOJA5"){  #HOJA MAS VIEJA LIBRE DE QUEMA MAYOR A 5%
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_C;
			echo $retval->Grafica_semanaToken($postdata->token, "HOJA5", 0);
		}
	}else{
		$retval = new Graficas_S_C;
		echo $retval->Grafica_semana("HOJA5",0);
	}
}
else if($postdata->opt == "HOJTOT"){  #HOJAS TOTALES
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_C;
			echo $retval->Grafica_semanaToken($postdata->token, "HOJTOT", 0);
		}
	}else{
		$retval = new Graficas_S_C;
		echo $retval->Grafica_semana("HOJTOT",0);
	}
}
?>