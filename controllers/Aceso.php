<?php

// session_start();
include 'class.sesion.php';
include 'conexion.php';



class Aceso  { 

	private $conexion;
	private $session;


    public function __construct(){
    	$this->session = Session::getInstance();
        $this->conexion = new M_Conexion;

    }

	private function redirect($url = "login.php"){
		header('Location: http://sigat.procesos-iq.com/'.$url);
	}

	public function login_from(){
		$data = [
			'token' => $_GET['token']
		];

		if(empty($data['token'])){
			$this->redirect("login.php");
		}

		$consulta = "SELECT * FROM user_logged WHERE token = '".$data['token']."';";
		$result = $this->conexion->Consultas(2, $consulta);
		$filas = count($result);
		if($filas > 0){
			$account = $result[0];
			$_POST['username'] = $account["user"];
			$_POST['password'] = $account["pass"];
			$this->login();
		}else{
			$this->redirect("login.php");
		}

	}

	public function login(){
		
		$data = [
			'user' => preg_replace("~[^\-\_\@\.a-z0-9]~i", "", trim(strtolower($_POST["username"]))),
			'pass' => preg_replace("/[^a-z0-9\,\.\-\_]/i", "", $_POST["password"])
        ];

		if (empty($data['user']) OR empty($data['pass'])) {
			$this->redirect("login.php");
		}

        $consulta = "SELECT *,YEAR(CURRENT_DATE) AS current_years FROM cat_usuarios WHERE usuario = '".$data['user']."'  AND pass = '".$data['pass']."';";
		$result = array();
        $result = $this->conexion->Consultas(2, $consulta);

		$filas = count($result);
		if($filas > 0){
			$account = $result[0];
			$_SESSION["SIGAT"] = $account['id'];
			$this->session = new Session($account['usuario'] , $account['id']);
			$this->session->logged = $account['id'];
			$this->session->id = $account['id'];
            $this->session->tipo = $account['tipo'];
			$this->session->current_year = (int)$account["current_years"];
			$this->session->create = $account['fecha_create'];
			$this->session->membresia = $this->Membresias();
            $this->session->privileges = $this->getPrivileges();
            $this->session->isAdmin = $account['admin'];
            $this->session->year = date('Y');
			if(!isset($this->session->membresia->id_membresia) || $this->session->membresia->id_membresia <= 0){
				$this->redirect("login.php");
			} 
			$this->session->nombre = $account['nombre']."  ".$account['apellidos'];
			if($this->session->membresia->membresia == 'Completa' || $this->session->membresia->membresia == 'Media'){
				$this->session->logges = $this->getUsers();
				$this->session->clients = $this->getDetailAccountCliente();
				$this->session->fincas = $this->getDetailAccountHacienda();

				// print_r($this->session);
				if(isset($this->session->privileges->non_privileges)){
					$this->redirect("404");
				}elseif(isset($this->session->privileges->access) && $this->session->privileges->access == 1){
					$this->redirect("index");
				}elseif(isset($this->session->privileges->access) && $this->session->privileges->access == 2){
                    if($this->session->logged != 57)
                        $this->redirect("resumenCiclos");
                    else
                        $this->redirect("resumenCiclosDemo");
				}else{
					$this->session->access_fincas = $this->getProductoresFincas();
					// print_r($this->session->access_fincas);
					if(isset($this->session->access_fincas->non_access)){
						$this->redirect("404");
					}elseif(isset($this->session->access_fincas->access_special) && $this->session->access_fincas->access_special > 0){
						$this->redirect("index");
					}else{

						// print_r($this->session->access_fincas);
						$this->session->client = $this->session->access_fincas->id_clientes;
						$id_hacienda = $this->session->access_fincas->id_finca;
						$sql_hacienda = "SELECT Finca.id,Finca.nombre,Productor.nombre as Productor  
						FROM cat_haciendas as Finca, cat_clientes as Productor
						WHERE 
						Finca.id_cliente = Productor.id 
						AND Finca.id_cliente = '{$this->session->client}' 
						AND Finca.id = '{$id_hacienda}'";
						$hacienda = (object)$this->conexion->Consultas(2, $sql_hacienda)[0];
						$this->session->finca = $hacienda->id;
						$this->session->client_name = $hacienda->Productor;
                        $this->session->finca_name = $hacienda->nombre;
                        
                        if($this->session->client == 25){
                            $this->redirect("inspeccion");
                        }
						$this->redirect("inspeccion");
					}		
				}


			}elseif($this->session->membresia->membresia == 'Basica'){
				$this->session->access_fincas = $this->getProductoresFincas();
				if(isset($this->session->access_fincas->non_access)){
					$this->redirect("404");
				}else{
					$this->session->client = $this->session->access_fincas->id_clientes;
					$sql_hacienda = "SELECT Finca.id,Finca.nombre,Productor.nombre as Productor  
					FROM cat_haciendas as Finca, cat_clientes as Productor
					WHERE 
					Finca.id_cliente = Productor.id 
					AND Finca.id_usuario = '{$this->session->logged}' 
					AND Finca.id_cliente = '{$this->session->client}' 
					AND Finca.id = '{$id_hacienda}'";
					$hacienda = (object)$this->conexion->Consultas(2, $sql_hacienda)[0];
					$this->session->finca = $hacienda->id;
					$this->session->client_name = $hacienda->Productor;
					$this->session->finca_name = $hacienda->nombre;
					$this->redirect("inspeccion");
				}		
			}
		}else{
			$this->redirect("login.php");
		}

	}

	private function getUsers(){
		$users = (object)[];
		$sql = "SELECT id_clientes , id_finca , users.id_usuario , users.id_membresia FROM usuarios_clientes_fincas as info
				RIGHT JOIN (SELECT id_usuario,id_membresia FROM membresias_usuarios) as users ON info.id_usuario = users.id_usuario
				WHERE users.id_membresia = '{$this->session->membresia->id_membresia}'";
		$fila = (object)[];
		$fila = $this->conexion->Consultas(2, $sql);
		if(count($fila) > 0){
			foreach ($fila as $value) {
				$value = (object)$value;
				$users->users[] = $value->id_usuario;
				$users->fincas[] = $value->id_finca;
				$users->productores[] = $value->id_clientes;
			}
		}else{
			$users->non_access = 400;
		}
		$users->users = implode(",", array_unique($users->users));
		$users->fincas = implode(",", $users->fincas);
		$users->productores = implode(",", array_unique($users->productores));
		return $users;
	}

	private function getProductoresFincas(){
		$rowFincas = (object)[];
		$sql = "SELECT id_clientes , id_finca FROM usuarios_clientes_fincas WHERE id_usuario = '{$this->session->logged}'";
		$fila = (object)[];
		$fila = $this->conexion->Consultas(2, $sql);
		if(count($fila) > 0){
			$rowFincas->access_special = 0;
			if(count($fila) > 1){
				$rowFincas->access_special = 1;
				foreach ($fila as $value) {
					$value = (object)$value;
					$rowFincas->id_clientes[] = $value->id_clientes;
					$rowFincas->id_finca[] = $value->id_finca;
				}
				$users->id_clientes = array_unique($rowFincas->id_clientes);
				$users->id_finca = array_unique($rowFincas->id_finca);
			}else{
				foreach ($fila[0] as $key => $value) {
					$rowFincas->{$key} = $value;
				}
			}
		}else{
			$rowFincas->non_access = 400;
		}
		return $rowFincas;
	}

	private function getPrivileges(){
		$privileges = (object)[];
		$sql = "SELECT tresm,cerosem,oncesem,foliar,climas,fotos,mapas,ciclos,informe,fincas,productores,agrupaciones,access ,configuracion, orodelti,costos_orodelti, matriz_orodelti
                FROM users_privileges 
		        WHERE id_usuario = '{$this->session->logged}'";
		$fila = (object)[];
		$fila = $this->conexion->Consultas(2, $sql);
		if(count($fila) > 0){
			foreach ($fila[0] as $key => $value) {
				$privileges->{$key} = $value;
			}
		}else{
			$privileges->non_privileges = 400;
		}

		return $privileges;
	}

	private function getDetailAccountCliente(){
		$sql_client = "SELECT * FROM cat_clientes WHERE id_usuario = '{$this->session->logged}' AND status > 0";
		$cliente = (object)[];
		$cliente = (object)$this->conexion->Consultas(2, $sql_client);

		return $cliente;
	}

	private function getDetailAccountHacienda(){
		$sql_hacienda = "SELECT * FROM cat_haciendas WHERE id_usuario = '{$this->session->logged}' AND status > 0 ORDER BY id_cliente";
		$hacienda = (object)[];
		$hacienda = (object)$this->conexion->Consultas(2, $sql_hacienda);

		return $hacienda;
	}

	private function Membresias(){
		$membresia = [];
		$sql = "SELECT id_membresia , (SELECT membresia FROM membresias WHERE id = id_membresia) as membresia
				FROM membresias_usuarios , membresias
				WHERE id_usuario = '{$this->session->logged}' AND CURRENT_DATE BETWEEN fecha_start AND fecha_end";
		$membresia = $this->conexion->Consultas(2, $sql);
		if(count($membresia) > 0){
			$membresia = (object)$membresia[0];
		}
		return $membresia;
	}

	// public function is_logged() {
	// 	return isset($this->session->logged);
	// }

	// public function if_not_logged_redirect() {
	// 	if (!$this->is_logged()) {
	// 		$this->redirect("login.php");
	// 	}
	// }

	// public function if_logged_redirect() {
	// 	if ($this->is_logged()) {
	// 		$this->redirect("index.php");
	// 	}
	// }

	public function logout(){

		$this->session->kill();

	}

}



$aceso = new Aceso;
if(isset($_GET['token'])){
	$aceso->login_from();
}else{
	$aceso->login();
}
// if($aceso->login()){

// 	$_SESSION["login"] = 1;

	

//     exit;

// }else{

// 	header('Location: http://sigat.procesos-iq.com/login.php');

//     exit;

// }





?>