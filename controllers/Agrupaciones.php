<?php
	/**
	*  CLASS FROM Fincas
	*/
	class Agrupaciones 
	{
		private $conexion;
		private $session;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		public function index(){
			$sWhere = "";
			$sOrder = " ORDER BY id";
			$DesAsc = "ASC";
			$sOrder .= " {$DesAsc}";
			$sLimit = "";
			if(isset($_POST)){

				/*----------  ORDER BY ----------*/
				
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY agrupacion.id {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY FIC.nombre {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY Cli.nombre {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY agrupacion.nombre {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY status {$DesAsc}";
				}
				/*----------  ORDER BY ----------*/

				if(isset($_POST['search_id']) && trim($_POST['search_id']) != ""){
					$sWhere .= " AND id = ".$_POST["search_id"];
				}				
				if(isset($_POST['search_finca']) && trim($_POST['search_finca']) != ""){
					$sWhere .= " AND FIC.nombre LIKE '%".$_POST['search_finca']."%'";
				}
				if(isset($_POST['search_name']) && trim($_POST['search_name']) != ""){
					$sWhere .= " AND agrupacion.nombre LIKE '%".$_POST['search_name']."%'";
				}
				if(isset($_POST['search_client']) && trim($_POST['search_client']) != ""){
					$sWhere .= " AND Cli.nombre LIKE '%".$_POST['search_client']."%'";
				}
				if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
					$sWhere .= " AND status = ".$_POST["order_status"];
				}

				/*----------  LIMIT  ----------*/
				if(isset($_POST['length']) && $_POST['length'] > 0){
					$sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
				}
			}

			$sql = "SELECT agrupacion.id , agrupacion.nombre  ,Cli.nombre as cliente , FIC.nombre as finca , 
					agrupacion.status,agrupacion.id_usuario
					FROM cat_agrupaciones AS agrupacion, cat_clientes AS Cli, cat_haciendas AS FIC
					WHERE agrupacion.id_usuario = '{$this->session->logged}' $sWhere AND agrupacion.id_cliente = Cli.id AND agrupacion.id_hacienda = FIC.id
					GROUP BY agrupacion.id 
					$sOrder
					$sLimit";
			$res = $this->conexion->link->query($sql);
			$datos = (object)[
				"customActionMessage" => "Error al consultar la informacion",
				"customActionStatus" => "Error",
				"data" => [],
				"draw" => 0,
				"recordsFiltered" => 0,
				"recordsTotal" => 0,
			];
			$count = 0;
			while($fila = $res->fetch_assoc()){
				$fila = (object)$fila;
				$count++;
				$datos->data[] = [
					$count,
					$fila->id,
					$fila->cliente,
					$fila->finca,
					$fila->nombre,
					$fila->status,
					$fila->id_usuario,
					'<button class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
				];
			}

			$datos->recordsTotal = count($datos->data);
			$datos->customActionMessage = "Informacion completada con exito";
			$datos->customActionStatus = "OK";

			return json_encode($datos);
		}

		public function create(){
			extract($_POST);
			if($txtnom != "" && $id_cliente != ""){
				$sql = "INSERT INTO cat_agrupaciones SET 
				nombre = '{$txtnom}' , 
				id_cliente = '{$id_cliente}' , 
				id_hacienda = '{$id_hacienda}' , 
				id_usuario = '{$this->session->logged}'";
				$ids = $this->conexion->Consultas(1,$sql);
        		return $ids;
			}
		}

		public function update(){
			extract($_POST);
			if((int)$id > 0 && $txtnom != "" && $id_cliente != ""){
				$sql = "UPDATE cat_agrupaciones SET 
					nombre = '{$txtnom}' , 
					id_cliente = '{$id_cliente}' , 
					id_hacienda = '{$id_hacienda}' 
					WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		public function changeStatus(){
			extract($_POST);
			if((int)$id > 0 ){
				$status = ($estado == 'activo') ? 1 : 0;
				$sql = "UPDATE cat_agrupaciones SET status={$status} WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				// echo $sql;
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		public function edit(){
			$response = (object)[
				"success" => 400,
				"data" => [],
				"clientes" => [],
				"fincas" => [],
			];
			$response->clientes = $this->getClient();
			$response->fincas = $this->getFincas();
			if(isset($_GET['id'])){
				$id = (int)$_GET['id'];
				if($id > 0){
					$sql = "SELECT * FROM cat_agrupaciones WHERE id_usuario = '{$this->session->logged}' AND id = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->data = (object)$res->fetch_assoc();
					}
					$response->success = 200;
				}else{
					$response->fincas = [];
				}
			}else{
				$response->fincas = [];
			}
			return json_encode($response);
		}

		private function getClient(){
			$cliente = [];
			$sql = "SELECT id , nombre FROM cat_clientes WHERE id_usuario = '{$this->session->logged}' AND status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$cliente[] = (object)$fila;
			}

			return $cliente;
		}

		private function getFincas(){
			$cliente = [];
			$sql = "SELECT id , nombre FROM cat_haciendas WHERE id_usuario = '{$this->session->logged}' AND status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$cliente[] = (object)$fila;
			}

			return $cliente;
		}
	}
?>
