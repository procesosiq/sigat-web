<?php
class AnalisisProductos 
{
    private $db;
    private $session;

    public function __construct()
    {
        $this->db = new M_Conexion();
        $this->session = Session::getInstance();	
    }

    public function years(){
        $response = new stdClass;
        $response->years = $this->db->queryAllOne("SELECT anio FROM ciclos_aplicacion_hist WHERE anio > 0 GROUP BY anio");
        return json_encode($response);
    }
    
    public function productos(){
        $postdata = (object)json_decode(file_get_contents("php://input"))->params;
        $sWhere = "";

        if($postdata->tipo_producto != ""){
            $sWhere .= " AND products.id_tipo_producto = '{$postdata->tipo_producto}'";
        }
        if($postdata->year != ""){
            $sWhere .= " AND anio = {$postdata->year}";
        }else{
            $sWhere .= " AND anio = YEAR(CURRENT_DATE)";
        }

        $response = new stdClass;
        $response->data = $this->db->queryAll("SELECT UPPER(products.`nombre_comercial`) AS producto, SUM(cantidad) AS cantidad, AVG(detalle.precio) AS precio, SUM(total) AS total, products.`id_tipo_producto`
            FROM ciclos_aplicacion_hist h
            INNER JOIN ciclos_aplicacion_hist_productos detalle ON id_ciclo_aplicacion = h.id
            INNER JOIN products ON products.id = detalle.`id_producto`
            WHERE 1=1 $sWhere
            GROUP BY UPPER(products.nombre_comercial)");
        foreach($response->data as $row){
            $row->detalle = $this->db->queryAll("SELECT cat_proveedores.nombre as proveedor, AVG(detalle.precio) AS precio, SUM(cantidad) AS cantidad, SUM(total) AS total
                FROM ciclos_aplicacion_hist h
                INNER JOIN ciclos_aplicacion_hist_productos detalle ON id_ciclo_aplicacion = h.id
                INNER JOIN products ON products.id = detalle.`id_producto`
                INNER JOIN cat_proveedores ON cat_proveedores.id = detalle.id_proveedor
                WHERE UPPER(nombre_comercial) = '{$row->producto}' $sWhere
                GROUP BY detalle.id_proveedor");
        }

        return json_encode($response);
    }

    public function tipos(){
        $postdata = (object)json_decode(file_get_contents("php://input"))->params;
        $sWhere = "";
        if($postdata->tipo_producto != ""){
            $sWhere .= " AND products.id_tipo_producto = '{$postdata->tipo_producto}'";
        }
        if($postdata->year != ""){
            $sWhere .= " AND anio = {$postdata->year}";
        }else{
            $sWhere .= " AND anio = YEAR(CURRENT_DATE)";
        }

        $response = new stdClass;
        $response->data = $this->db->queryAll("SELECT cat_tipo_productos.nombre as tipo, SUM(cantidad) AS cantidad, SUM(total) AS total
            FROM ciclos_aplicacion_hist h
            INNER JOIN ciclos_aplicacion_hist_productos detalle ON id_ciclo_aplicacion = h.id
            INNER JOIN products ON products.id = detalle.`id_producto`
            INNER JOIN cat_tipo_productos ON products.id_tipo_producto = cat_tipo_productos.id
            WHERE 1=1 $sWhere
            GROUP BY products.id_tipo_producto");
        return json_encode($response);
    }

    public function proveedores(){
        $postdata = (object)json_decode(file_get_contents("php://input"))->params;
        $sWhere = "";
        if($postdata->tipo_producto != ""){
            $sWhere .= " AND products.id_tipo_producto = '{$postdata->tipo_producto}'";
        }
        if($postdata->year != ""){
            $sWhere .= " AND anio = {$postdata->year}";
        }else{
            $sWhere .= " AND anio = YEAR(CURRENT_DATE)";
        }

        $response = new stdClass;
        $response->data = $this->db->queryAll("SELECT cat_proveedores.nombre as proveedor, SUM(cantidad) AS cantidad, SUM(total) AS total
            FROM ciclos_aplicacion_hist h
            INNER JOIN ciclos_aplicacion_hist_productos detalle ON id_ciclo_aplicacion = h.id
            INNER JOIN products ON products.id = detalle.`id_producto`
            INNER JOIN cat_proveedores ON cat_proveedores.id = detalle.id_proveedor
            WHERE 1=1 $sWhere
            GROUP BY detalle.id_proveedor");
        return json_encode($response);
    }

    /*
    $response->data = $this->db->queryAll("SELECT proveedor, ROUND(SUM(total), 2) AS total
            FROM(
                SELECT total_1 AS total, UPPER(products.proveedor) AS proveedor, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(fungicida_1)) AND id_tipo_producto  = 4 AND precio = precio_1 AND IF(fungicida_1 = 'BRAVO' AND precio_1 = 10, 18, products.id) = products.id
                WHERE TRIM(`fungicida_1`) != '' AND YEAR(fecha_real) = 2017
                UNION ALL
                SELECT total_2 AS total, UPPER(products.proveedor) AS proveedor, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(fungicida_2)) AND id_tipo_producto  = 4 AND precio = precio_2 AND IF(fungicida_1 = 'BRAVO' AND precio_1 = 10, 18, products.id) = products.id
                WHERE TRIM(`fungicida_2`) != '' AND YEAR(fecha_real) = 2017
                UNION ALL
                SELECT total_3 AS total, UPPER(cat_proveedores.`nombre`) AS proveedor, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(bioestimulantes_1)) AND id_tipo_producto  = 1 AND IF(bioestimulantes_1 = 'BIOEMUGLOB', precio_3, precio) = precio_3
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`bioestimulantes_1`) != '' AND YEAR(fecha_real) = 2017
                UNION ALL 
                SELECT total_4 AS total, UPPER(cat_proveedores.`nombre`) AS proveedor, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(bioestimulantes_2)) AND id_tipo_producto  = 1 AND precio = precio_4
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`bioestimulantes_2`) != '' AND YEAR(fecha_real) = 2017
                UNION ALL
                SELECT total_5 AS total, UPPER(cat_proveedores.`nombre`) AS proveedor, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(coadyuvante_1)) AND id_tipo_producto  = 2 AND 
                    IF(coadyuvante_1 IN ('PTA 88', 'INDICATE', 'BREAK THRU', 'MIXER'), 
                        IF(precio = 9.64 AND coadyuvante_1 = 'INDICATE', 9.09, precio_5), 
                        precio
                    ) = precio_5
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`coadyuvante_1`) != '' AND YEAR(fecha_real) = 2017
                UNION ALL
                SELECT (total_6) AS total, UPPER(cat_proveedores.`nombre`) AS proveedor, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(coadyuvante_2)) AND id_tipo_producto  = 2 AND
                    IF(UPPER(TRIM(coadyuvante_2)) = 'INDICATE', 
                        IF(precio_6 = 9.03, 9.64, 
                            IF(precio_6 = 9.09, 9.64, precio_6)
                        ),
                        precio_6 
                    ) = precio
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`coadyuvante_2`) != '' AND YEAR(fecha_real) = 2017
                UNION ALL
                SELECT (total_7) AS total, UPPER(cat_proveedores.`nombre`) AS proveedor, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(aceite)) AND id_tipo_producto = 6 AND precio = precio_7
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`aceite`) != '' AND YEAR(fecha_real) = 2017
                UNION ALL
                SELECT (total_8) AS total, UPPER(cat_proveedores.`nombre`) AS proveedor, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(foliar_1)) AND id_tipo_producto  = 3
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`foliar_1`) != '' AND YEAR(fecha_real) = 2017
                UNION ALL
                SELECT (total_9) AS total, UPPER(cat_proveedores.`nombre`) AS proveedor, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(foliar_2)) AND id_tipo_producto  = 3
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`foliar_2`) != '' AND YEAR(fecha_real) = 2017
                UNION ALL
                SELECT (total_10) AS total, UPPER(cat_proveedores.`nombre`) AS proveedor, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(foliar_3)) AND id_tipo_producto  = 3
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`foliar_3`) != '' AND YEAR(fecha_real) = 2017
                UNION ALL
                SELECT (total_11) AS total, UPPER(cat_proveedores.`nombre`) AS proveedor, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(foliar_4)) AND id_tipo_producto  = 3
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`foliar_4`) != '' AND YEAR(fecha_real) = 2017
                UNION ALL
                SELECT (total_12) AS total, UPPER(cat_proveedores.`nombre`) AS proveedor, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(insecticida)) AND id_tipo_producto  = 5 AND precio = precio_12
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`insecticida`) != '' AND YEAR(fecha_real) = 2017 AND TRIM(insecticida) != 'AGUA'
            ) AS tbl
            WHERE proveedor != ''
            GROUP BY proveedor
            ORDER BY proveedor");
    */

    /*$response->data = $this->db->queryAll("SELECT 
                UPPER(TRIM(producto)) AS producto,
                ROUND(SUM(cantidad), 2) AS cantidad,
                ROUND(AVG(IFNULL(precio, 0)), 2) AS precio,
                ROUND(SUM(total), 2) AS total, 
                id_tipo_producto
            FROM(
                SELECT fungicida_1 AS producto, cantidad_1 AS cantidad, precio, total_1 AS total, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(fungicida_1)) AND id_tipo_producto  = 4 
                    AND precio = IF(
                        UPPER(TRIM(fungicida_1)) = 'RUBRIC', 21.75, 
                        IF(UPPER(TRIM(fungicida_1)) = 'ODEON', 9.4, 
                            IF(UPPER(TRIM(fungicida_1)) = 'SIGANEX' AND precio_1 = 10, 19, 
                                IF(UPPER(TRIM(fungicida_1)) = 'CUMORA', 62, 
                                    IF(UPPER(TRIM(fungicida_1)) = 'COMET GOLD' AND precio_1 = 34.62, 34.68, 
                                        IF(UPPER(TRIM(fungicida_1)) = 'INSTINCT', 29, 
                                            IF(UPPER(TRIM(fungicida_1)) = 'POLYRAM' AND precio_1 = 8.75, 8.8, 
                                                IF(UPPER(TRIM(fungicida_1)) = 'LUNA TRANQUILITY', 43, 
                                                    IF(UPPER(TRIM(fungicida_1)) = 'VOLLEY' AND precio_1 = 38, 22.5, 
                                                        IF(UPPER(TRIM(fungicida_1)) = 'SILVACUR' AND precio_1 = 34.3, 34.2, 
                                                            precio_1
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
			        AND IF(fungicida_1 = 'BRAVO' AND precio_1 = 10, 18, products.id) = products.id
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`fungicida_1`) != '' AND YEAR(fecha_real) = 2017

                UNION ALL

                SELECT fungicida_2 AS producto, cantidad_2 AS cantidad, precio AS precio, total_2 AS total, id_tipo_producto
                FROM ciclos_aplicacion_historico 
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(fungicida_2)) AND id_tipo_producto  = 4 
                    AND precio = IF(
                        UPPER(TRIM(fungicida_2)) = 'RUBRIC', 21.75, 
                        IF(UPPER(TRIM(fungicida_2)) = 'ODEON', 9.4, 
                            IF(UPPER(TRIM(fungicida_2)) = 'SIGANEX' AND precio_2 = 10, 19, 
                                IF(UPPER(TRIM(fungicida_2)) = 'CUMORA', 62, 
                                    IF(UPPER(TRIM(fungicida_2)) = 'COMET GOLD' AND precio_2 = 34.62, 34.68, 
                                        IF(UPPER(TRIM(fungicida_2)) = 'INSTINCT', 29, 
                                            IF(UPPER(TRIM(fungicida_2)) = 'POLYRAM' AND precio_2 = 8.75, 8.8, 
                                                IF(UPPER(TRIM(fungicida_2)) = 'LUNA TRANQUILITY', 43, 
                                                    IF(UPPER(TRIM(fungicida_2)) = 'VOLLEY' AND precio_2 = 38, 22.5, 
                                                        IF(UPPER(TRIM(fungicida_2)) = 'SILVACUR' AND precio_2 = 34.3, 34.2, 
                                                            precio_2
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
			        AND IF(fungicida_2 = 'BRAVO' AND precio_2 = 10, 18, products.id) = products.id
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE fungicida_2 != '' AND YEAR(fecha_real) = 2017

                UNION ALL

                SELECT bioestimulantes_1 AS producto, cantidad_3 AS cantidad, precio AS precio, total_3 AS total, id_tipo_producto
                FROM ciclos_aplicacion_historico 
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(bioestimulantes_1)) AND id_tipo_producto  = 1 AND IF(bioestimulantes_1 = 'BIOEMUGLOB', precio_3, precio) = precio_3
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`bioestimulantes_1`) != '' AND YEAR(fecha_real) = 2017

                UNION ALL
                
                SELECT bioestimulantes_2 AS producto, cantidad_4 AS cantidad, precio AS precio, total_4 AS total, id_tipo_producto
                FROM ciclos_aplicacion_historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(bioestimulantes_2)) AND id_tipo_producto  = 1 AND precio = precio_4
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`bioestimulantes_2`) != '' AND YEAR(fecha_real) = 2017

                UNION ALL

                SELECT coadyuvante_1 AS producto, cantidad_5 AS cantidad, precio AS precio, total_5 AS total, id_tipo_producto
                FROM ciclos_aplicacion_historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(coadyuvante_1)) AND id_tipo_producto  = 2 AND 
                    IF(coadyuvante_1 IN ('PTA 88', 'INDICATE', 'BREAK THRU', 'MIXER'), 
                        IF(precio = 9.64 AND coadyuvante_1 = 'INDICATE', 9.09, precio_5), 
                        precio
                    ) = precio_5
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`coadyuvante_1`) != '' AND YEAR(fecha_real) = 2017

                UNION ALL

                SELECT coadyuvante_2 AS producto, cantidad_6 AS cantidad, precio AS precio, total_6 AS total, id_tipo_producto
                FROM ciclos_aplicacion_historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(coadyuvante_2)) AND id_tipo_producto  = 2 AND
                    IF(UPPER(TRIM(coadyuvante_2)) = 'INDICATE', 
                        IF(precio_6 = 9.03, 9.64, 
                            IF(precio_6 = 9.09, 9.64, precio_6)
                        ),
                        precio_6 
                    ) = precio
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`coadyuvante_2`) != '' AND YEAR(fecha_real) = 2017

                UNION ALL

                SELECT aceite AS producto, cantidad_7 AS cantidad, precio AS precio, total_7 AS total, id_tipo_producto
                FROM ciclos_aplicacion_historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(aceite)) AND id_tipo_producto = 6 AND precio = precio_7
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`aceite`) != '' AND YEAR(fecha_real) = 2017

                UNION ALL

                SELECT foliar_1 AS producto, cantidad_8 AS cantidad, precio AS precio, total_8 AS total, id_tipo_producto
                FROM ciclos_aplicacion_historico 
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(foliar_1)) AND id_tipo_producto  = 3
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`foliar_1`) != '' AND YEAR(fecha_real) = 2017

                UNION ALL
                
                SELECT foliar_2 AS producto, cantidad_9 AS cantidad, precio AS precio, total_9 AS total, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(foliar_2)) AND id_tipo_producto  = 3
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`foliar_2`) != '' AND YEAR(fecha_real) = 2017

                UNION ALL

                SELECT foliar_3 AS producto, cantidad_10 AS cantidad, precio AS precio, total_10 AS total, id_tipo_producto
                FROM ciclos_aplicacion_historico historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(foliar_3)) AND id_tipo_producto  = 3
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`foliar_3`) != '' AND YEAR(fecha_real) = 2017

                UNION ALL

                SELECT foliar_4 AS producto, cantidad_11 AS cantidad, precio AS precio, total_11 AS total, id_tipo_producto
                FROM ciclos_aplicacion_historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(foliar_4)) AND id_tipo_producto  = 3
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`foliar_4`) != '' AND YEAR(fecha_real) = 2017

                UNION ALL

                SELECT insecticida AS producto, cantidad_12 AS cantidad, precio AS precio, total_12 AS total, id_tipo_producto
                FROM ciclos_aplicacion_historico
                LEFT JOIN products ON UPPER(nombre_comercial) = TRIM(UPPER(insecticida)) AND id_tipo_producto  = 5 AND precio = precio_12
                LEFT JOIN cat_proveedores ON products.`id_proveedor` = cat_proveedores.`id`
                WHERE TRIM(`insecticida`) != '' AND YEAR(fecha_real) = 2017 AND TRIM(insecticida) != 'AGUA'
            ) AS tbl
            WHERE TRIM(producto) != '' AND UPPER(TRIM(producto)) != 'AGUA' AND total > 0
            GROUP BY UPPER(TRIM(producto))
            ORDER BY TRIM(producto)");*/

    /*foreach($response->data as $row){
            $row->detalle = $this->db->queryAll("SELECT 
                    cat_proveedores.nombre as proveedor,
                    products.precio,
                    id_tipo_producto,
                    IF(products.id_tipo_producto = 1,
                        IFNULL((SELECT SUM(cantidad_3)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_3 = products.precio AND UPPER(TRIM(bioestimulantes_1)) = '{$row->producto}' AND cantidad_3 > 0), 0)
                        +
                        IFNULL((SELECT SUM(cantidad_4)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_4 = products.precio AND UPPER(TRIM(bioestimulantes_2)) = '{$row->producto}' AND cantidad_4 > 0), 0)
                    , 0) +
                    IF(products.id_tipo_producto = 2,
                        IFNULL((SELECT SUM(cantidad_5)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_5 = products.precio AND UPPER(TRIM(coadyuvante_1)) = '{$row->producto}' AND cantidad_5 > 0), 0)
                        +
                        IFNULL((SELECT SUM(cantidad_6)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_6 = products.precio AND UPPER(TRIM(coadyuvante_2)) = '{$row->producto}' AND cantidad_6 > 0), 0)
                    , 0) +
                    IF(products.id_tipo_producto = 3,
                        IFNULL((SELECT SUM(cantidad_8)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_8 = products.precio AND UPPER(TRIM(foliar_1)) = '{$row->producto}' AND cantidad_8 > 0), 0)
                        +
                        IFNULL((SELECT SUM(cantidad_9)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_9 = products.precio AND UPPER(TRIM(foliar_2)) = '{$row->producto}' AND cantidad_9 > 0), 0)
                        +
                        IFNULL((SELECT SUM(cantidad_10)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_10 = products.precio AND UPPER(TRIM(foliar_3)) = '{$row->producto}' AND cantidad_10 > 0), 0)
                        +
                        IFNULL((SELECT SUM(cantidad_11)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_11 = products.precio AND UPPER(TRIM(foliar_4)) = '{$row->producto}' AND cantidad_11 > 0), 0)
                    , 0) +
                    IF(products.id_tipo_producto = 4,
                        IFNULL((SELECT SUM(cantidad_1)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_1 = products.precio AND UPPER(TRIM(fungicida_1)) = '{$row->producto}' AND cantidad_1 > 0), 0)
                        +
                        IFNULL((SELECT SUM(cantidad_2)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_2 = products.precio AND UPPER(TRIM(fungicida_2)) = '{$row->producto}' AND cantidad_2 > 0), 0)
                    , 0) +
                    IF(products.id_tipo_producto = 5,
                        IFNULL((SELECT SUM(cantidad_12)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_12 = products.precio AND UPPER(TRIM(insecticida)) = '{$row->producto}' AND cantidad_12 > 0), 0)
                    , 0) +
                    IF(products.id_tipo_producto = 6,
                        IFNULL((SELECT SUM(cantidad_7)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_7 = products.precio AND UPPER(TRIM(aceite)) = '{$row->producto}' AND cantidad_7 > 0), 0)
                    , 0) AS cantidad,

                    IF(products.id_tipo_producto = 1,
                        IFNULL((SELECT SUM(total_3)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_3 = products.precio AND UPPER(TRIM(bioestimulantes_1)) = '{$row->producto}' AND total_3 > 0), 0)
                        +
                        IFNULL((SELECT SUM(total_4)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_4 = products.precio AND UPPER(TRIM(bioestimulantes_2)) = '{$row->producto}' AND total_4 > 0), 0)
                    , 0) +
                    IF(products.id_tipo_producto = 2,
                        IFNULL((SELECT SUM(total_5)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_5 = products.precio AND UPPER(TRIM(coadyuvante_1)) = '{$row->producto}' AND total_5 > 0), 0)
                        +
                        IFNULL((SELECT SUM(total_6)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_6 = products.precio AND UPPER(TRIM(coadyuvante_2)) = '{$row->producto}' AND total_6 > 0), 0)
                    , 0) +
                    IF(products.id_tipo_producto = 3,
                        IFNULL((SELECT SUM(total_8)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_8 = products.precio AND UPPER(TRIM(foliar_1)) = '{$row->producto}' AND total_8 > 0), 0)
                        +
                        IFNULL((SELECT SUM(total_9)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_9 = products.precio AND UPPER(TRIM(foliar_2)) = '{$row->producto}' AND total_9 > 0), 0)
                        +
                        IFNULL((SELECT SUM(total_10)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_10 = products.precio AND UPPER(TRIM(foliar_3)) = '{$row->producto}' AND total_10 > 0), 0)
                        +
                        IFNULL((SELECT SUM(total_11)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_11 = products.precio AND UPPER(TRIM(foliar_4)) = '{$row->producto}' AND total_11 > 0), 0)
                    , 0) +
                    IF(products.id_tipo_producto = 4,
                        IFNULL((SELECT SUM(total_1)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_1 = products.precio AND UPPER(TRIM(fungicida_1)) = '{$row->producto}' AND total_1 > 0), 0)
                        +
                        IFNULL((SELECT SUM(total_2)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_2 = products.precio AND UPPER(TRIM(fungicida_2)) = '{$row->producto}' AND total_2 > 0), 0)
                    , 0) +
                    IF(products.id_tipo_producto = 5,
                        IFNULL((SELECT SUM(total_12)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_12 = products.precio AND UPPER(TRIM(insecticida)) = '{$row->producto}' AND total_12 > 0), 0)
                    , 0) +
                    IF(products.id_tipo_producto = 6,
                        IFNULL((SELECT SUM(total_7)
                        FROM ciclos_aplicacion_historico
                        WHERE YEAR(fecha_real) = 2017 AND precio_7 = products.precio AND UPPER(TRIM(aceite)) = '{$row->producto}' AND total_7 > 0), 0)
                    , 0) AS total
                FROM products
                LEFT JOIN cat_proveedores ON products.id_proveedor = cat_proveedores.id
                WHERE nombre_comercial = '{$row->producto}'");
        }*/
}
?>

