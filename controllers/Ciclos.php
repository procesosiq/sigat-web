<?php
	/**
	*  CLASS FROM PRODUCTORES
	*/
	class Ciclos 
	{
		private $conexion;
		private $session;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		public function index(){
			$sWhere = "";
			$sOrder = " ORDER BY id";
			$DesAsc = "ASC";
			$sOrder .= " {$DesAsc}";
			$sLimit = "";
			if(isset($_POST)){

				/*----------  ORDER BY ----------*/
				
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY ciclos_aplicacion.fecha {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY ciclo {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY WEEK(ciclos_aplicacion.fecha) {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY producto_1 {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY producto_2 {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY nombre {$DesAsc}";
				}
				/*----------  ORDER BY ----------*/

				if(isset($_POST['search_ciclo']) && trim($_POST['search_ciclo']) != ""){
					$sWhere .= " AND WEEK(ciclos_aplicacion.fecha) = ".$_POST["search_ciclo"];
				}				
				if((isset($_POST['search_date_from']) && trim($_POST['search_date_from']) != "") && (isset($_POST['search_date_to']) && trim($_POST['search_date_to']) != "")){
					$sWhere .= " AND ciclos_aplicacion.fecha BETWEEN '".$_POST["search_date_from"]."' AND '".$_POST["search_date_to"]."'";
				}
				if(isset($_POST['search_producto_1']) && trim($_POST['search_producto_1']) != ""){
					$sWhere .= " AND producto_1 LIKE '%".$_POST['search_producto_1']."%'";
				}
				if(isset($_POST['search_producto_2']) && trim($_POST['search_producto_2']) != ""){
					$sWhere .= " AND producto_2 LIKE '%".$_POST['search_producto_2']."%'";
				}
				if(isset($_POST['search_finca']) && trim($_POST['search_finca']) != ""){
					$sWhere .= " AND nombre LIKE '%".$_POST['search_finca']."%'";
				}
				if(isset($_POST['order_ciclo']) && trim($_POST['order_ciclo']) != ""){
					$sWhere .= " AND ciclo = ".$_POST["order_ciclo"];
				}

				/*----------  LIMIT  ----------*/
				if(isset($_POST['length']) && $_POST['length'] > 0){
					$sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
				}
			}

			$sql = "SELECT ciclos_aplicacion.id,ciclos_aplicacion.fecha,WEEK(ciclos_aplicacion.fecha) AS semana,producto_1,producto_2,ciclo,nombre FROM ciclos_aplicacion INNER JOIN cat_haciendas ON id_finca = cat_haciendas.id 
				WHERE ciclos_aplicacion.id_usuario IN ({$this->session->logges->users}) 
				AND ciclos_aplicacion.id_finca = '{$this->session->finca}' $sWhere $sOrder $sLimit";
			$res = $this->conexion->link->query($sql);
			$datos = (object)[
				"customActionMessage" => "Error al consultar la informacion",
				"customActionStatus" => "Error",
				"data" => [],
				"draw" => 0,
				"recordsFiltered" => 0,
				"recordsTotal" => 0,
			];
			$count = 0;
			while($fila = $res->fetch_assoc()){
				$fila = (object)$fila;
				$count++;
				$datos->data[] = [
					$fila->fecha,
					$fila->semana,
					$fila->ciclo,
					$fila->producto_1,
					$fila->producto_2,
					$fila->nombre,
					''
					//'<button class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
				];
			}

			$datos->recordsTotal = count($datos->data);
			$datos->customActionMessage = "Informacion completada con exito";
			$datos->customActionStatus = "OK";

			return json_encode($datos);
		}

		public function create(){
			echo '<script>alert("lalal");</script>';
			extract($_POST);
			if($txtnom != "" && $id_cliente != ""){
				$sql = "INSERT INTO ciclos_de_app SET 
				id_usuario = '{$this->session->logged}',
				fecha = CURRENT_DATE ,
				ciclo = '{$txtciclo}', 
				compañia_de_aplicacion = '{$txtcompañia}' , 
				area_app = {$txtarea} , 
				costo_app_ha = {$txtcosto} , 
				costo_total_app = {$txtotal}";
				$ids = $this->conexion->Consultas(1,$sql);
        		return $ids;
			}
		}

		public function update(){
			extract($_POST);
			if((int)$id > 0 && $txtnom != "" && $txtemail != ""){
				$sql = "UPDATE cat_clientes SET nombre = '{$txtnom}' , email = '{$txtemail}' , fecha = CURRENT_DATE WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		public function changeStatus(){
			extract($_POST);
			if((int)$id > 0 ){
				$status = ($estado == 'activo') ? 1 : 0;
				$sql = "UPDATE cat_clientes SET status={$status} WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				// echo $sql;
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		private function getUserProductor($id = 0){
			$user_id = 0;
			if($id > 0){
				$sql = "SELECT * FROM cat_clientes WHERE id_usuario = '{$this->session->logged}' AND id = {$id}";
				$res = $this->conexion->link->query($sql);
				if($res->num_rows > 0){
					$fila = $res->fetch_assoc();
					$fila = (object)$fila;
					if($fila->usuario_id > 0){
						$usuario_id = $fila->usuario_id;
					}else{
						$pass = $this->randomPassword();
						$sql_user = "INSERT INTO cat_usuarios SET 
								nombre = '{$fila->nombre}',
								email = '{$fila->email}',
								usuario = '{$fila->email}',
								pass = '{$pass}', 
								fecha_create =CURRENT_TIMESTAMP";
						$ids = $this->conexion->link->query($sql_user);
						$user_id = $this->conexion->link->insert_id;
						$usuario_id = (int)$user_id;
						if((int)$user_id > 0){
							$sql_cliente = "UPDATE cat_clientes SET usuario_id = '{$user_id}' WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
							$this->conexion->link->query($sql_cliente);

							$sql_privileges = "INSERT INTO users_privileges SET
											id_usuario ='{$user_id}',
											3m = 'Activo',
											0sem = 'Activo',
											11sem = 'Activo',
											foliar = 'Activo',
											climas = 'Activo',
											fotos = 'Activo',
											mapas = 'Activo',
											informe = 'Inactivo',
											fincas = 'NONE',
											productores = 'NONE',
											agrupaciones = 'NONE'";
							$this->conexion->link->query($sql_privileges);

							$sql_membresia = "INSERT INTO users_privileges SET
											id_usuario ='{$user_id}',
											id_membresia = '{$this->session->membresia->id_membresia}'";
							$this->conexion->link->query($sql_membresia);
						}
					}
				}
			}

			return $usuario_id;
		}

		private function randomPassword() {
			$alphabet = 'abcdefghijklmnopqrstuvwxyz1234567890';
			$pass = array(); //remember to declare $pass as an array
			$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
			for ($i = 0; $i < 8; $i++) {
				$n = rand(0, $alphaLength);
				$pass[] = $alphabet[$n];
			}
			return implode($pass); //turn the array into a string
		}

		public function createUser(){
			$response = (object)[
				"success" => 400,
				"data" => []
			];
			if(isset($_POST['id'])){
				$id = (int)$_POST['id'];
				if($id > 0){
					$sql = "SELECT id as id_hacienda , id_cliente as id_productor from cat_haciendas 
							WHERE id_usuario = '{$this->session->logged}' AND id_cliente = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$id_user = $this->getUserProductor($id);
						$sql_delete = "DELETE FROM usuarios_clientes_fincas WHERE 
								id_membresia = '{$this->session->membresia->id_membresia}' AND 
								id_usuario = '{$id_user}'";
						$this->conexion->link->query($sql_delete);
						while($fila = $res->fetch_assoc()){
							$fila = (object)$fila;
							$sql = "INSERT INTO usuarios_clientes_fincas SET
								id_membresia = '{$this->session->membresia->id_membresia}',
								id_usuario = '{$id_user}',
								id_clientes = '{$fila->id_productor}',
								id_finca = '{$fila->id_hacienda}'";
							$this->conexion->link->query($sql);
						}
					}
					$response->success = 200;
				}
			}
			return json_encode($response);
		}

		public function edit(){
			$response = (object)[
				"success" => 400,
				"data" => []
			];
			if(isset($_GET['id'])){
				$id = (int)$_GET['id'];
				if($id > 0){
					$sql = "SELECT * FROM cat_clientes WHERE id_usuario = '{$this->session->logged}' AND id = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->data = (object)$res->fetch_assoc();
					}
					$response->success = 200;
				}
			}
			return json_encode($response);
		}*/
	}
?>
