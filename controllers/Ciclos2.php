<?php
	/**
	*  CLASS FROM Fincas
	*/
	class Ciclos2 
	{
		private $conexion;
		private $session;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		public function index(){
			$sWhere = "";
			$sOrder = " ORDER BY id";
			$DesAsc = "ASC";
			$sOrder .= " {$DesAsc}";
			$sLimit = "";

			$filtersColumns = $_POST["show_columns"] == "";
			$columns = $filtesColumns 
				? array("id", "fecha", "ciclo", "semana", "frecuencia", "producto1", "dosis1", "producto2", "dosis2", "area_app")
				: explode(",", $_POST["show_columns"]);

			if(isset($_POST)){
				/*----------  ORDER BY ----------*/
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY id {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1 && count($columns) >= 2){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[1]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2 && count($columns) >= 3){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[2]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3 && count($columns) >= 4){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[3]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4 && count($columns) >= 5){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[4]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5 && count($columns) >= 6){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[5]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6 && count($columns) >= 7){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[6]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7 && count($columns) >= 8){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[7]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 8 && count($columns) >= 9){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[8]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 9 && count($columns) >= 10){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[9]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 10 && count($columns) >= 11){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[10]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 11 && count($columns) >= 12){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[11]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 12 && count($columns) >= 13){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[12]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 13 && count($columns) >= 14){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[13]} {$DesAsc}";
				}
				/*----------  ORDER BY ----------*/

				if(isset($_POST['search_id']) && trim($_POST['search_id']) != ""){
					$sWhere .= " AND id = ".$_POST["search_id"];
				}				
				if((isset($_POST['search_date_from']) && trim($_POST['search_date_from']) != "") && (isset($_POST['search_date_to']) && trim($_POST['search_date_to']) != "")){
					$sWhere .= " AND fecha BETWEEN '".$_POST["search_date_from"]."' AND '".$_POST["search_date_to"]."'";
				}
				if(isset($_POST['search_semana']) && trim($_POST['search_semana']) != ""){
					$sWhere .= " AND semana LIKE '%".$_POST['search_semana']."%'";
				}
				if(isset($_POST['search_ciclo']) && trim($_POST['search_ciclo']) != ""){
					$sWhere .= " AND ciclo LIKE '%".$_POST['search_ciclo']."%'";
				}
				if(isset($_POST['search_producto_1']) && trim($_POST['search_producto_1']) != ""){
					$sWhere .= " AND producto1 LIKE '%".$_POST["search_producto_1"]."%'";
				}
				if(isset($_POST['search_dosis_1']) && trim($_POST['search_dosis_1']) != ""){
					$sWhere .= " AND dosis1 LIKE '%".$_POST["search_dosis_1"]."%'";
				}
				if(isset($_POST['search_producto_2']) && trim($_POST['search_producto_2']) != ""){
					$sWhere .= " AND producto2 LIKE '%".$_POST["search_producto_2"]."%'";
				}
				if(isset($_POST['search_dosis_2']) && trim($_POST['search_dosis_2']) != ""){
					$sWhere .= " AND dosis2 LIKE '%".$_POST["search_dosis_2"]."%'";
				}
				if(isset($_POST['search_area']) && trim($_POST['search_area']) != ""){
					$sWhere .= " AND area_app LIKE '%".$_POST["search_area"]."%'";
				}
				if(isset($_POST['search_fecha']) && trim($_POST['search_fecha']) != ""){
					$sWhere .= " AND YEAR(fecha) = '".$_POST["search_fecha"]."'";
					$sYear = $_POST["search_fecha"];
				}else{
					$sWhere .= " AND YEAR(fecha) = YEAR(CURRENT_DATE)";
					$sYear = "YEAR(CURRENT_DATE)";
				}

				if(isset($_POST['search_tipo']) && trim($_POST["search_tipo"]) != ""){
					$sWhere .= " AND tipo = '{$_POST["search_tipo"]}'";
					$sTipo = "{$_POST["search_tipo"]}";
				}else{
					$sWhere .= " AND tipo = 'Real'";
					$sTipo = "Real";
				}

				/*----------  LIMIT  ----------*/
				if(isset($_POST['length']) && $_POST['length'] > 0){
					$sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
				}
			}

			$tieneFinca = "SELECT * FROM usuarios_clientes_fincas WHERE id_usuario = '{$this->session->logged}' AND id_finca = '{$this->session->finca}'";
			$res = $this->conexion->link->query($tieneFinca);
			$tiene = 0;
			while($fill = $res->fetch_assoc()){
				$tiene++;
			}

			$sql = "SELECT *
					FROM 
					(
					SELECT main.*, 
						WEEK(main.fecha) AS semana, 
						IFNULL((SELECT nombre_comercial FROM productos WHERE id = id_producto_1), (SELECT ingrediente_activo FROM productos WHERE id = id_producto_1)) AS producto1, 
						(SELECT IF(dosis_num_1 = 1, dosis, IF(dosis_num_1 = 2, dosis2, dosis3)) FROM productos WHERE id = id_producto_1) AS dosis1,
						IFNULL((SELECT nombre_comercial FROM productos WHERE id = id_producto_2), (SELECT ingrediente_activo FROM productos WHERE id = id_producto_2)) AS producto2, 
						(SELECT IF(dosis_num_2 = 1, dosis, IF(dosis_num_2 = 2, dosis2, dosis3)) FROM productos WHERE id = id_producto_2) AS dosis2,
						IF(ciclo > 1, 
							DATEDIFF(DATE(main.fecha), (SELECT DATE(fecha) FROM ciclos_de_app WHERE ({$tiene} > 0 OR '{$this->session->logged}' = 1) AND id_finca = '{$this->session->finca}' AND ciclo = (main.ciclo-1) AND tipo = '{$sTipo}' AND YEAR(fecha) = $sYear LIMIT 1))
							, DATEDIFF(DATE(main.fecha), (SELECT DATE(fecha) FROM ciclos_de_app WHERE ({$tiene} > 0 OR '{$this->session->logged}' = 1) AND id_finca = '{$this->session->finca}' AND tipo = '{$sTipo}' AND YEAR(fecha) = $sYear-1 ORDER BY fecha DESC LIMIT 1))) AS frecuencia
					FROM ciclos_de_app main
					LEFT JOIN ciclo_producto1 ON ciclo_producto1.id_ciclo = main.id 
					LEFT JOIN ciclo_producto2 ON ciclo_producto2.id_ciclo = main.id 
					WHERE ({$tiene} > 0 OR '{$this->session->logged}' = 1) AND id_finca = '{$this->session->finca}'
					) AS tbl
					WHERE 1=1 $sWhere
					$sOrder $sLimit";
			$res = $this->conexion->link->query($sql);
			$datos = (object)[
				"customActionMessage" => "Error al consultar la informacion",
				"customActionStatus" => "Error",
				"data" => [],
				"draw" => 0,
				"recordsFiltered" => 0,
				"recordsTotal" => 0,
			];
			$count = 0;

			while($fila = $res->fetch_assoc()){
				$fila = (object)$fila;
				$count++;

				$datos->data[$count-1][] = $fila->id;
				if(in_array("fecha", $columns) || $filtersColumns){
					$datos->data[$count-1][] = $fila->fecha;
				}
				if(in_array("semana", $columns) || $filtersColumns){
					$datos->data[$count-1][] = $fila->semana;
				}
				if(in_array("ciclo", $columns) || $filtersColumns){
					$datos->data[$count-1][] = $fila->ciclo;
				}
				if(in_array("compania_de_aplicacion", $columns)){
					$datos->data[$count-1][] = $fila->compania_de_aplicacion;
				}
				if(in_array("equipo_aplicacion", $columns)){
					$datos->data[$count-1][] = $fila->equipo_aplicacion;
				}
				if(in_array("costo_app_ha", $columns)){
					$datos->data[$count-1][] = round($fila->costo_app_ha, 2);
				}
				if(in_array("costo_total_app", $columns)){
					$datos->data[$count-1][] = round($fila->costo_total_app, 2);
				}
				if(in_array("frecuencia", $columns) || $filtersColumns){
					$datos->data[$count-1][] = $fila->frecuencia;
				}
				if(in_array("producto1", $columns) || $filtersColumns){
					$datos->data[$count-1][] = $fila->producto1;
				}
				if(in_array("dosis1", $columns) || $filtersColumns){
					$datos->data[$count-1][] = round($fila->dosis1, 2);
				}
				if(in_array("producto2", $columns) || $filtersColumns){
					$datos->data[$count-1][] = $fila->producto2;
				}
				if(in_array("dosis2", $columns) || $filtersColumns){
					$datos->data[$count-1][] = round($fila->dosis2, 2);
				}
				if(in_array("area_app", $columns) || $filtersColumns){
					$datos->data[$count-1][] = $fila->area_app;
				}
				$datos->data[$count-1][] = '<button id="edit" class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Editar</button>';
			}
			$datos->filtering = $filtersColumns;
			$datos->filterColumns = $columns;
			$datos->recordsTotal = count($datos->data);
			$datos->customActionMessage = "Informacion completada con exito";
			$datos->customActionStatus = "OK";

			return json_encode($datos);
		}

		public function indexSemaforo(){
			$sWhere = "";
			$sOrder = " ORDER BY id";
			$DesAsc = "ASC";
			$sOrder .= " {$DesAsc}";
			$sLimit = "";

			$filtersColumns = $_POST["show_columns"] == "";
			$columns = $filtesColumns 
				? array("id", "fecha", "ciclo", "semana", "frecuencia", "producto1", "dosis1", "producto2", "dosis2", "area_app")
				: explode(",", $_POST["show_columns"]);

			if(isset($_POST)){
				/*----------  ORDER BY ----------*/
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY id {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1 && count($columns) >= 2){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[1]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2 && count($columns) >= 3){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[2]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3 && count($columns) >= 4){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[3]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4 && count($columns) >= 5){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[4]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5 && count($columns) >= 6){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[5]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6 && count($columns) >= 7){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[6]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7 && count($columns) >= 8){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[7]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 8 && count($columns) >= 9){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[8]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 9 && count($columns) >= 10){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[9]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 10 && count($columns) >= 11){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[10]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 11 && count($columns) >= 12){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[11]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 12 && count($columns) >= 13){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[12]} {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 13 && count($columns) >= 14){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY {$columns[13]} {$DesAsc}";
				}
				/*----------  ORDER BY ----------*/

				if(isset($_POST['search_id']) && trim($_POST['search_id']) != ""){
					$sWhere .= " AND id = ".$_POST["search_id"];
				}				
				if((isset($_POST['search_date_from']) && trim($_POST['search_date_from']) != "") && (isset($_POST['search_date_to']) && trim($_POST['search_date_to']) != "")){
					$sWhere .= " AND fecha BETWEEN '".$_POST["search_date_from"]."' AND '".$_POST["search_date_to"]."'";
				}
				if(isset($_POST['search_semana']) && trim($_POST['search_semana']) != ""){
					$sWhere .= " AND semana LIKE '%".$_POST['search_semana']."%'";
				}
				if(isset($_POST['search_ciclo']) && trim($_POST['search_ciclo']) != ""){
					$sWhere .= " AND ciclo LIKE '%".$_POST['search_ciclo']."%'";
				}
				if(isset($_POST['search_producto_1']) && trim($_POST['search_producto_1']) != ""){
					$sWhere .= " AND producto1 LIKE '%".$_POST["search_producto_1"]."%'";
				}
				if(isset($_POST['search_dosis_1']) && trim($_POST['search_dosis_1']) != ""){
					$sWhere .= " AND dosis1 LIKE '%".$_POST["search_dosis_1"]."%'";
				}
				if(isset($_POST['search_producto_2']) && trim($_POST['search_producto_2']) != ""){
					$sWhere .= " AND producto2 LIKE '%".$_POST["search_producto_2"]."%'";
				}
				if(isset($_POST['search_dosis_2']) && trim($_POST['search_dosis_2']) != ""){
					$sWhere .= " AND dosis2 LIKE '%".$_POST["search_dosis_2"]."%'";
				}
				if(isset($_POST['search_area']) && trim($_POST['search_area']) != ""){
					$sWhere .= " AND area_app LIKE '%".$_POST["search_area"]."%'";
				}
				if(isset($_POST['search_fecha']) && trim($_POST['search_fecha']) != ""){
					$sWhere .= " AND YEAR(fecha) = '".$_POST["search_fecha"]."'";
					$sYear = $_POST["search_fecha"];
				}else{
					$sWhere .= " AND YEAR(fecha) = YEAR(CURRENT_DATE)";
					$sYear = "YEAR(CURRENT_DATE)";
				}

				if(isset($_POST['search_tipo']) && trim($_POST["search_tipo"]) != ""){
					$sWhere .= " AND tipo = '{$_POST["search_tipo"]}'";
					$sTipo = "{$_POST["search_tipo"]}";
				}else{
					$sWhere .= " AND tipo = 'Real'";
					$sTipo = "Real";
				}

				/*----------  LIMIT  ----------*/
				if(isset($_POST['length']) && $_POST['length'] > 0){
					$sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
				}
			}

			$tieneFinca = "SELECT * FROM usuarios_clientes_fincas WHERE id_usuario = '{$this->session->logged}' AND id_finca = '{$this->session->finca}'";
			$res = $this->conexion->link->query($tieneFinca);
			$tiene = 0;
			while($fill = $res->fetch_assoc()){
				$tiene++;
			}

			$sql = "SELECT *
					FROM 
					(
					SELECT main.*, 
						WEEK(main.fecha) AS semana, 
						IFNULL((SELECT nombre_comercial FROM productos WHERE id = id_producto_1), (SELECT ingrediente_activo FROM productos WHERE id = id_producto_1)) AS producto1, 
						(SELECT IF(dosis_num_1 = 1, dosis, IF(dosis_num_1 = 2, dosis2, dosis3)) FROM productos WHERE id = id_producto_1) AS dosis1,
						IFNULL((SELECT nombre_comercial FROM productos WHERE id = id_producto_2), (SELECT ingrediente_activo FROM productos WHERE id = id_producto_2)) AS producto2, 
						(SELECT IF(dosis_num_2 = 1, dosis, IF(dosis_num_2 = 2, dosis2, dosis3)) FROM productos WHERE id = id_producto_2) AS dosis2,
						IF(ciclo > 1, 
							DATEDIFF(DATE(main.fecha), (SELECT DATE(fecha) FROM ciclos_de_app WHERE ({$tiene} > 0 OR '{$this->session->logged}' = 1) AND id_finca = '{$this->session->finca}' AND ciclo = (main.ciclo-1) AND tipo = 'Real' AND YEAR(fecha) = $sYear LIMIT 1))
							, DATEDIFF(DATE(main.fecha), (SELECT DATE(fecha) FROM ciclos_de_app WHERE ({$tiene} > 0 OR 1 = '{$this->session->logged}') AND id_finca = '{$this->session->finca}' AND tipo = 'Real' AND YEAR(fecha) = $sYear-1 ORDER BY fecha DESC LIMIT 1))) AS frecuencia,
						IF(ciclo > 1, 
							DATEDIFF(DATE(main.fecha), (SELECT DATE(fecha) FROM ciclos_de_app WHERE ({$tiene} > 0 OR '{$this->session->logged}' = 1) AND id_finca = '{$this->session->finca}' AND ciclo = (main.ciclo-1) AND tipo = 'Estimado' AND YEAR(fecha) = $sYear LIMIT 1))
							, DATEDIFF(DATE(main.fecha), (SELECT DATE(fecha) FROM ciclos_de_app WHERE ({$tiene} > 0 OR 1 = '{$this->session->logged}') AND id_finca = '{$this->session->finca}' AND tipo = 'Estimado' AND YEAR(fecha) = $sYear-1 ORDER BY fecha DESC LIMIT 1))) AS frecuencia_estimada
					FROM ciclos_de_app main
					LEFT JOIN ciclo_producto1 ON ciclo_producto1.id_ciclo = main.id 
					LEFT JOIN ciclo_producto2 ON ciclo_producto2.id_ciclo = main.id 
					WHERE ({$tiene} > 0 OR 1 = '{$this->session->logged}') AND id_finca = '{$this->session->finca}'
					) AS tbl
					WHERE 1=1 $sWhere
					$sOrder $sLimit";
			$res = $this->conexion->link->query($sql);
			$datos = (object)[
				"customActionMessage" => "Error al consultar la informacion",
				"customActionStatus" => "Error",
				"data" => [],
				"draw" => 0,
				"recordsFiltered" => 0,
				"recordsTotal" => 0,
			];
			$count = 0;

			while($fila = $res->fetch_assoc()){
				$fila = (object)$fila;
				$count++;

				$datos->data[$count-1][] = $fila->id;
				if(in_array("fecha", $columns) || $filtersColumns){
					$datos->data[$count-1][] = $fila->fecha;
				}
				if(in_array("semana", $columns) || $filtersColumns){
					$datos->data[$count-1][] = $fila->semana;
				}
				if(in_array("ciclo", $columns) || $filtersColumns){
					$datos->data[$count-1][] = $fila->ciclo;
				}
				if(in_array("compania_de_aplicacion", $columns)){
					$datos->data[$count-1][] = $fila->compania_de_aplicacion;
				}
				if(in_array("equipo_aplicacion", $columns)){
					$datos->data[$count-1][] = $fila->equipo_aplicacion;
				}
				if(in_array("costo_app_ha", $columns)){
					$datos->data[$count-1][] = round($fila->costo_app_ha, 2);
				}
				if(in_array("costo_total_app", $columns)){
					$datos->data[$count-1][] = round($fila->costo_total_app, 2);
				}
				if(in_array("frecuencia", $columns) || $filtersColumns){
					if($fila->frecuencia_estimada == null || $fila->frecuencia_estimada == ""){
						$datos->data[$count-1][] = $fila->frecuencia;
					}else{
						$datos->data[$count-1][] = 
						(($fila->frecuencia <= $fila->frecuencia_estimada)
						 ? '<i class="fa fa-arrow-down font-green-jungle">'
						 : '<i class="fa fa-arrow-up font-red-thunderbird">')
						."{$fila->frecuencia}</i>";
					}
				}
				if(in_array("producto1", $columns) || $filtersColumns){
					$datos->data[$count-1][] = $fila->producto1;
				}
				if(in_array("dosis1", $columns) || $filtersColumns){
					$datos->data[$count-1][] = round($fila->dosis1, 2);
				}
				if(in_array("producto2", $columns) || $filtersColumns){
					$datos->data[$count-1][] = $fila->producto2;
				}
				if(in_array("dosis2", $columns) || $filtersColumns){
					$datos->data[$count-1][] = round($fila->dosis2, 2);
				}
				if(in_array("area_app", $columns) || $filtersColumns){
					$datos->data[$count-1][] = $fila->area_app;
				}
				$datos->data[$count-1][] = '<button id="edit" class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Editar</button>';
			}
			$datos->filtering = $filtersColumns;
			$datos->filterColumns = $columns;
			$datos->sql = $sql;
			$datos->recordsTotal = count($datos->data);
			$datos->customActionMessage = "Informacion completada con exito";
			$datos->customActionStatus = "OK";

			return json_encode($datos);
		}

		public function create(){
			//echo '<script>alert("lalal");</script>';
			extract($_POST);
				
				$sql = "INSERT INTO ciclos_de_app SET 
					id_usuario = '{$this->session->logged}',
					fecha = CURRENT_DATE ,
					ciclo = '{$txtciclo}', 
					compania_de_aplicacion = '{$txtcompañia}' , 
					area_app = '{$txtarea}' , 
					costo_app_ha = '{$txtcosto}' , 
					costo_total_app = '{$txtotal}',
					equipo_aplicacion = '{$equipo_aplicacion}'";
				$ids = $this->conexion->Consultas(1,$sql);
				$sql = "INSERT INTO ciclo_producto1 SET id_ciclo = '{$ids}'";
				$this->conexion->Consultas(1,$sql);
				$sql = "INSERT INTO ciclo_producto2 SET id_ciclo = '{$ids}'";
				$this->conexion->Consultas(1,$sql);
				$sql = "INSERT INTO datos_generales_ciclos SET id_ciclo = '{$ids}'";
				$this->conexion->Consultas(1,$sql);
        		return $ids;
			
		}

		public function update(){
			extract($_POST);
			if((int)$id > 0){
				$sql = "UPDATE ciclos_de_app SET 
					ciclo = '{$txtciclo}', 
					compania_de_aplicacion = '{$txtcompañia}' , 
					area_app = '{$txtarea}' , 
					costo_app_ha = '{$txtcosto}' , 
					costo_total_app = '{$txtotal}',
					equipo_aplicacion = '{$equipo_aplicacion}'
					WHERE id = '{$id}' AND id_usuario = '{$this->session->logged}'";
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}
		public function updateProducto1(){
			extract($_POST);
			if((int)$id > 0){
				$sql = "UPDATE ciclo_producto1 SET 
					nombre_comun = '{$nomProd1}', 
					nombre_molecula = '{$molProd1}' , 
					proveedor = '{$provProd1}' , 
					dosis = '{$dosProd1}', 
					costo = '{$costoProd1}',
					costo_total = '{$totalProd1}'
					WHERE id_ciclo = '{$id}'";
				$this->conexion->Consultas(1,$sql);
				$sql = "UPDATE ciclos_de_app SET id_producto_1 = '{$nomProd1}', dosis_num_1 = '{$dosProd1}' WHERE id = {$id}";
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
			else{
				$sql = "INSERT INTO ciclos_de_app SET 
					id_producto_1 = '{$nomProd1}',
					dosis_num_1 = '{$dosProd1}',
					id_usuario = '{$this->session->logged}',
					fecha = CURRENT_DATE";
				$ids = $this->conexion->Consultas(1,$sql);
				$sql = "INSERT INTO ciclo_producto1 SET 
					nombre_comun = '{$nomProd1}', 
					nombre_molecula = '{$molProd1}' , 
					proveedor = '{$provProd1}' , 
					dosis = '{$dosProd1}' , 
					costo = '{$costoProd1}',
					costo_total = '{$totalProd1}',
					id_ciclo = '{$ids}'";
				$this->conexion->Consultas(1,$sql);
				$sql = "INSERT INTO ciclo_producto2 SET id_ciclo = '{$ids}'";
				$this->conexion->Consultas(1,$sql);
				$sql = "INSERT INTO datos_generales_ciclos SET id_ciclo = '{$ids}'";
				$this->conexion->Consultas(1,$sql);
				return $ids;
			}
		}
		
		public function updateProducto2(){
			extract($_POST);
			if((int)$id > 0){
				$sql = "UPDATE ciclo_producto2 SET 
					nombre_comun = '{$nomProd2}', 
					nombre_molecula = '{$molProd2}' , 
					proveedor = '{$provProd2}' , 
					dosis = '{$dosProd2}' , 
					costo = '{$costoProd2}',
					costo_total = '{$totalProd2}'
					WHERE id_ciclo = '{$id}'";
				$this->conexion->Consultas(1,$sql);
				$sql = "UPDATE ciclos_de_app SET id_producto_2 = '{$nomProd2}', dosis_num_2 = '{$dosProd2}' WHERE id = {$id}";
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
			else{
				$sql = "INSERT INTO ciclos_de_app SET 
					id_producto_2 = '{$nomProd2}',
					dosis_num_2 = '{$dosProd2}',
					id_usuario = '{$this->session->logged}',
					fecha = CURRENT_DATE";
				$ids = $this->conexion->Consultas(1,$sql);
				$sql = "INSERT INTO ciclo_producto1 SET id_ciclo = '{$ids}'";
				$this->conexion->Consultas(1,$sql);
				$sql = "INSERT INTO ciclo_producto2 SET 
					nombre_comun = '{$nomProd2}', 
					nombre_molecula = '{$molProd2}' , 
					proveedor = '{$provProd2}' , 
					dosis = '{$dosProd2}' , 
					costo = '{$costoProd2}',
					costo_total = '{$totalProd2}',
					id_ciclo = '{$ids}'";
				$this->conexion->Consultas(1,$sql);
				$sql = "INSERT INTO datos_generales_ciclos SET id_ciclo = '{$ids}'";
				$this->conexion->Consultas(1,$sql);
				return $ids;
			}
		}
		public function updateDatos(){
			extract($_POST);
			if((int)$id > 0){
				$sql = "UPDATE datos_generales_ciclos SET 
					nombre_emu = '{$nomEmul}', 
					costo_litro_emu = '{$litroEmul}' , 
					costo_total_emu = '{$totalEmul}' , 
					proveedor_emu = '{$provEmul}' , 
					nombre_ace = '{$nomAceite}', 
					costo_litro_ace = '{$litroAceite}' , 
					costo_total_ace = '{$totalAceite}' , 
					proveedor_ace = '{$provAceite}' , 
					total_costo = '{$totalCiclo}',
					total_costo_ha = '{$totalHa}'
					WHERE id_ciclo = '{$id}'";
				$this->conexion->Consultas(1,$sql);
				$sql = "UPDATE ciclos_de_app SET id_aceite = '{$nomAceite}', id_emulsificante = '{$nomEmul}', dosis_num_emulsificante = '{$select_dosis_emulsificante}', dosis_num_aceite = '{$select_dosis_aceite}' WHERE id = {$id}";
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
			else{
				$sql = "INSERT INTO ciclos_de_app SET id_aceite = '{$nomAceite}', id_emulsificante = '{$nomEmul}', id_usuario = '{$this->session->logged}',
				fecha = CURRENT_DATE, dosis_num_emulsificante = '{$select_dosis_emulsificante}', dosis_num_aceite = '{$select_dosis_aceite}'";
				$ids = $this->conexion->Consultas(1,$sql);
				$sql = "INSERT INTO ciclo_producto1 SET 
				id_ciclo = '{$ids}'";
				$this->conexion->Consultas(1,$sql);
				$sql = "INSERT INTO ciclo_producto2 SET 
				id_ciclo = '{$ids}'";
				$this->conexion->Consultas(1,$sql);
				
				$sql = "INSERT INTO datos_generales_ciclos SET 
					nombre_emu = '{$nomEmul}', 
					costo_litro_emu = '{$litroEmul}' , 
					costo_total_emu = '{$totalEmul}' , 
					proveedor_emu = '{$provEmul}' , 
					nombre_ace = '{$nomAceite}', 
					costo_litro_ace = '{$litroAceite}' , 
					costo_total_ace = '{$totalAceite}' , 
					proveedor_ace = '{$provAceite}' , 
					total_costo = '{$totalCiclo}',
					total_costo_ha = '{$totalHa}',
					id_ciclo = '{$ids}'";
				$this->conexion->Consultas(1,$sql);
				return $ids;
			}
		}

		public function changeStatus(){
			extract($_POST);
			console.log($_POST);
			if((int)$id > 0 ){
				$status = ($estado == 'activo') ? 1 : 0;
				$sql = "UPDATE ciclos_de_app SET status={$status} WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				// echo $sql;
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		public function edit(){
			$response = (object)[
				"success" => 400,
				"data" => [],
				"lotes" => [],
			];
			

			if(isset($_GET['id'])){
				$id = (int)$_GET['id'];
				if($id > 0){
					$sql = "SELECT ciclos_de_app.*, (SELECT ingrediente_activo FROM productos WHERE id = id_producto_1) as ingrediente1, (SELECT ingrediente_activo FROM productos WHERE id = id_producto_2) as ingrediente2 FROM ciclos_de_app WHERE id_usuario = '{$this->session->logged}' AND ciclos_de_app.id = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->data = (object)$res->fetch_assoc();
					}
					$sql = "SELECT ciclo_producto1.* from ciclo_producto1 WHERE ciclo_producto1.id_ciclo = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->producto1 = (object)$res->fetch_assoc();
					}
					$sql = "SELECT ciclo_producto2.* from ciclo_producto2 WHERE ciclo_producto2.id_ciclo = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->producto2 = (object)$res->fetch_assoc();
					}
					/*
					$sql = "SELECT * from productos WHERE id = (SELECT id_producto_1 FROM ciclos_de_app WHERE id = {$id})";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->producto1 = (object)$res->fetch_assoc();
					}
					$sql = "SELECT * from productos WHERE id = (SELECT id_producto_2 FROM ciclos_de_app WHERE id = {$id})";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->producto2 = (object)$res->fetch_assoc();
					}
					*/
					$sql = "SELECT datos_generales_ciclos.* from datos_generales_ciclos WHERE datos_generales_ciclos.id_ciclo = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->datosg = (object)$res->fetch_assoc();
					}
					
					$response->success = 200;
					$response->sql = $sql;
				}
			}
			$sql = "SELECT * FROM productos WHERE id_usuario = '{$this->session->logged}'";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$fila = (object) $fila;
				$response->productos[] = $fila;
			}
			return json_encode($response);
		}

		public function indexProductos(){
			$sWhere = "";
			$sOrder = " ORDER BY id";
			$DesAsc = "ASC";
			$sOrder .= " {$DesAsc}";
			$sLimit = "";
			if(isset($_POST)){

				/*----------  ORDER BY ----------*/
				
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY id {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY agroquimico {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY nombre_comercial {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY ingrediente_activo {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY casa_comercial {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY dosis {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY dosis2 {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY dosis3 {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 8){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY precio {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 9){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY status {$DesAsc}";
				}
				/*----------  ORDER BY ----------*/

				if(isset($_POST['search_id']) && trim($_POST['search_id']) != ""){
					$sWhere .= " AND id = ".$_POST["search_id"];
				}				
				if(isset($_POST['search_agroquimico']) && trim($_POST['search_agroquimico']) != ""){
					$sWhere .= " AND agroquimico LIKE '%".$_POST['search_agroquimico']."%'";
				}
				if(isset($_POST['search_nombre_comercial']) && trim($_POST['search_nombre_comercial']) != ""){
					$sWhere .= " AND nombre_comercial LIKE '%".$_POST['search_nombre_comercial']."%'";
				}
				if(isset($_POST['search_ingrediente_activo']) && trim($_POST['search_ingrediente_activo']) != ""){
					$sWhere .= " AND ingrediente_activo LIKE '%".$_POST['search_ingrediente_activo']."%'";
				}
				if(isset($_POST['search_casa_comercial']) && trim($_POST['search_casa_comercial']) != ""){
					$sWhere .= " AND casa_comercial LIKE '%".$_POST["search_casa_comercial"]."%'";
				}
				if(isset($_POST['search_dosis']) && trim($_POST['search_dosis']) != ""){
					$sWhere .= " AND dosis LIKE '%".$_POST["search_dosis"]."%'";
				}
				if(isset($_POST['search_dosis_2']) && trim($_POST['search_dosis_2']) != ""){
					$sWhere .= " AND dosis2 LIKE '%".$_POST["search_dosis_2"]."%'";
				}
				if(isset($_POST['search_dosis_3']) && trim($_POST['search_dosis_3']) != ""){
					$sWhere .= " AND dosis3 LIKE '%".$_POST["search_dosis_3"]."%'";
				}
				if(isset($_POST['search_precio']) && trim($_POST['search_precio']) != ""){
					$sWhere .= " AND precio LIKE '%".$_POST["search_precio"]."%'";
				}
				if(isset($_POST['search_status']) && trim($_POST['search_status']) != ""){
					$sWhere .= " AND status = '".$_POST["search_status"]."'";
				}

				/*----------  LIMIT  ----------*/
				if(isset($_POST['length']) && $_POST['length'] > 0){
					$sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
				}
			}

			$sql = "SELECT *, (SELECT COUNT(*) FROM productos) as totalRows
					FROM productos
					WHERE id_usuario = '{$this->session->logged}' $sWhere $sOrder $sLimit";
			$res = $this->conexion->link->query($sql);
			$datos = (object)[
				"customActionMessage" => "Error al consultar la informacion",
				"customActionStatus" => "Error",
				"data" => [],
				"draw" => 0,
				"recordsFiltered" => 0,
				"recordsTotal" => 0,
			];
			$count = 1;

			$buttons = [
				0 => '<button id="status" class="form-control btn red">Inactivo</button>',
				1 => '<button id="status" class="form-control btn green">Activo</button>'
			];

			while($fila = $res->fetch_assoc()){
				$fila = (object)$fila;
				$count++;
				$datos->data[] = [
					$fila->id,
					$fila->agroquimico,
					$fila->nombre_comercial,
					$fila->ingrediente_activo,
					$fila->casa_comercial,
					round($fila->dosis, 2),
					round($fila->dosis2, 2),
					round($fila->dosis3, 2),
					round($fila->precio, 2),
					$buttons[$fila->status],
					'<button id="edit" class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
				];
				$datos->recordsTotal = $fila->totalRows;
			}
			$datos->recordsTotal = count($datos->data);
			$datos->customActionMessage = "Informacion completada con exito";
			$datos->customActionStatus = "OK";

			return json_encode($datos);
		}

		public function createProducto(){
			//extract($_POST);

			if(((int)$_POST["id"]) > 0){
				$sql = "
					UPDATE productos
					SET
						agroquimico = '{$_POST["agroquimico"]}',
						nombre_comercial = '{$_POST["nombre_comercial"]}',
						ingrediente_activo = '{$_POST["ingrediente_activo"]}',
						casa_comercial = '{$_POST["casa_comercial"]}',
						dosis = '{$_POST["dosis"]}',
						dosis2 = '{$_POST["dosis_2"]}',
						dosis3 = '{$_POST["dosis_3"]}',
						precio = '{$_POST["precio"]}'
					WHERE id = '{$_POST["id"]}' AND id_usuario = '{$this->session->logged}'";
				return $this->conexion->Consultas(1,$sql);
			}else{
				$sql = "
					INSERT INTO productos
					SET
						agroquimico = '{$_POST["agroquimico"]}',
						nombre_comercial = '{$_POST["nombre_comercial"]}',
						ingrediente_activo = '{$_POST["ingrediente_activo"]}',
						casa_comercial = '{$_POST["casa_comercial"]}',
						dosis = '{$_POST["dosis"]}',
						dosis2 = '{$_POST["dosis_2"]}',
						dosis3 = '{$_POST["dosis_3"]}',
						precio = '{$_POST["precio"]}',
						id_usuario = '{$this->session->logged}'";
				return $this->conexion->Consultas(1,$sql);
			}
		}

		public function editProducto(){
			$response = (object)[
				"success" => 400,
				"data" => [],
				"lotes" => [],
			];
			

			if(isset($_GET['id'])){
				$id = (int)$_GET['id'];
				if($id > 0){
					$sql = "SELECT * FROM productos WHERE id = {$_GET["id"]} AND id_usuario = '{$this->session->logged}'";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->data = (object)$res->fetch_assoc();
					}
					
					$response->success = 200;
				}
			}
			return json_encode($response);
		}

		public function getProductos(){
			$sql = "SELECT * FROM productos WHERE id_usuario = '{$this->session->logged}'";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$fila = (object) $fila;
				$productos[] = $fila;
			}
			return json_encode($productos);
		}

		public function changeStatusProducto(){
			$sql = "UPDATE productos SET status = IF(status = 1, 0, 1) WHERE id = {$_POST["id"]} AND id_usuario = '{$this->session->logged}'";
			$this->conexion->link->query($sql);
		}
	}
?>
