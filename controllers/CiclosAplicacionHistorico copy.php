<?php
	/**
	*  CLASS FROM PRODUCTOS
	*/
	class CiclosAplicacionHistorico 
	{
		private $db;
		private $session;
		private $gerenteDefault;
		private $fincarDefault;
		public function __construct()
		{
			$this->db = new M_Conexion();
        	$this->session = Session::getInstance();
			$this->gerenteDefault = 2;
			$this->fincarDefault = 49;
		}
		
		public function tecnico(){
			$response = new stdClass;

			$filters = $this->getFilters();

			$sql = "SELECT *
					FROM ciclos_aplicacion_historico
					WHERE YEAR(fecha_real) = YEAR(CURRENT_DATE) AND finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $filters->gerente AND id = $filters->finca)";
			$response->table = $this->db->queryAll($sql);
			foreach ($response->table as $key => $value) {
				$value->ciclo = (double)$value->ciclo;
				$value->sem = (double)$value->sem;
				$value->ha = (double)$value->ha;
			}
			$response->table_real = $this->db->queryAll($sql);
			$gerentes = $this->db->queryAllSpecial("SELECT id , nombre AS label FROM cat_gerentes WHERE status > 0");
			foreach ($gerentes as $key => $value) {
				$response->gerentes[] = (object)[
					"id" => (double)$key,
					"label" => $value,
				];
			}
			$response->years = $this->db->queryAllSpecial("SELECT YEAR(fecha_real) AS id ,  YEAR(fecha_real) AS label 
			FROM ciclos_aplicacion_historico
			WHERE YEAR(fecha_real) > 0
			GROUP BY YEAR(fecha_real)");
			$response->fincas = $this->getFincas();
			return json_encode($response);
		}

		public function economico(){
			$response = new stdClass;

			$filters = $this->getFilters();

			$sql = "SELECT * , (Ha_1 + Ha_2) as ha_coctel , (ha_oper * ha) AS oper , 
					(total_1 + total_2 + total_5 + total_7 + (ha_oper * ha)) AS costo_total
					FROM ciclos_aplicacion_historico
					WHERE YEAR(fecha_real) = YEAR(CURRENT_DATE) AND finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $filters->gerente AND id = $filters->finca)";
			$response->table = $this->db->queryAll($sql);
			foreach ($response->table as $key => $value) {
				$value->ciclo = (double)$value->ciclo;
				$value->sem = (double)$value->sem;
				$value->precio_1 = (double)$value->total_1;
				$value->precio_2 = (double)$value->precio_2;
				$value->precio_5 = (double)$value->precio_5;
				$value->precio_7 = (double)$value->precio_7;
				$value->ha_1 = (double)$value->ha_1;
				$value->ha_2 = (double)$value->ha_2;
				$value->ha_5 = (double)$value->ha_5;
				$value->ha_7 = (double)$value->ha_7;
				$value->total_1 = (double)$value->total_1;
				$value->total_2 = (double)$value->total_2;
				$value->total_5 = (double)$value->total_5;
				$value->total_7 = (double)$value->total_7;
				$value->costo_total = (double)round($value->costo_total,2);
				$value->oper = (double)$value->oper;
				$value->costo_ha = (double)round(($value->costo_total / $value->ha),2);
			}
			$response->table_real = $this->db->queryAll($sql);
			$gerentes = $this->db->queryAllSpecial("SELECT id , nombre AS label FROM cat_gerentes WHERE status > 0");
			foreach ($gerentes as $key => $value) {
				$response->gerentes[] = (object)[
					"id" => (double)$key,
					"label" => $value,
				];
			}
			$response->years = $this->db->queryAllSpecial("SELECT YEAR(fecha_real) AS id ,  YEAR(fecha_real) AS label 
			FROM ciclos_aplicacion_historico
			WHERE YEAR(fecha_real) > 0
			GROUP BY YEAR(fecha_real)");
			$response->fincas = $this->getFincas();
			return json_encode($response);
		}

		private function getFincas(){
			$response = new stdClass; 
			$response->data = [];
			$sql = "SELECT id , id_gerente , nombre FROM cat_fincas WHERE status > 0";
			$data = $this->db->queryAll($sql);
			foreach ($data as $key => $value) {
				$response->data[$value->id_gerente][] = (object)[
					"id" => (double)$value->id,
					"label" => $value->nombre,
				];	
			}
			return $response->data;
		}
		
		private function getFilters(){
			$data = (object)json_decode(file_get_contents("php://input"));
			$response = new stdClass;
			$response->gerente = (int)$data->params->gerente;
			$response->finca = (int)$data->params->finca;
			$response->year = (int)$data->params->year;

			if($response->gerente <= 0){
				$response->gerente = $this->gerenteDefault;
			}
			
			if($response->finca <= 0){
				$response->finca = $this->fincarDefault;
			}
		

			return $response;
		}
	}
?>
