<?php
/**
*  CLASS FROM PRODUCTOS
*/
class CiclosAplicacionHistorico 
{
    private $db;
    private $session;
    private $gerenteDefault;
    private $fincarDefault;

    public function __construct(){
        $this->db = new M_Conexion();
        $this->session = Session::getInstance();
        $this->gerenteDefault = 2;
        $this->fincarDefault = 49;
        $available = $this->db->queryRow("SELECT GROUP_CONCAT(CONCAT(\"'\", f.nombre, \"'\") SEPARATOR ',') AS fincas, GROUP_CONCAT(f.id SEPARATOR ',') AS ids
            FROM cat_usuarios u
            INNER JOIN users_fincas_privileges ON id_user = u.id
            INNER JOIN cat_fincas f ON id_finca = f.`id`
            WHERE u.tipo = 'CICLOS' AND u.id = {$this->session->id}");
        $this->available_fincas = $available->fincas;
        $this->available_id_fincas = $available->ids;
    }
    
    public function tecnico(){
        $response = new stdClass;

        $filters = $this->getFilters();
        $tipoHec = ($filters->tipoHectarea ? $filters->tipoHectarea : "''");
        $sWhere = "";
        if($filters->programa == 'sigatoka') $sWhere .= " AND programa IN ('SIGATOKA', 'HISTORICO') AND tipo_ciclo = 'CICLO'";
        if($filters->programa == 'foliar') $sWhere .= " AND programa IN ('FOLIAR') AND tipo_ciclo = 'CICLO' ";
        if($filters->programa == 'plagas') $sWhere .= " AND programa IN ('PLAGAS') AND tipo_ciclo = 'CICLO' ";
        if($filters->programa == 'erwinia') $sWhere .= " AND programa IN ('ERWINIA') AND tipo_ciclo = 'CICLO' ";
        if($filters->programa == 'sanidad') $sWhere .= " AND programa IN ('SIGATOKA', 'PLAGAS', 'ERWINIA') AND tipo_ciclo = 'CICLO'";
        if($filters->programa == 'todos') $sWhere = "";

        /* TABLAS PRINCIPALES */
        $sql = "SELECT hist.id, num_ciclo AS ciclo, SUM(hectareas_fumigacion) AS ha, fecha_prog, fecha_real, getWeek(fecha_real) AS sem, atraso, motivo, 
                    GROUP_CONCAT(DISTINCT hist.id SEPARATOR ',') as ids,
                    (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1) AS fungicida_1,
                    (SELECT dosis FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1) AS dosis_1,
                    (SELECT cantidad FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1) AS cantidad_1,
                    (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1,1) AS fungicida_2,
                    (SELECT dosis FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1,1) AS dosis_2,
                    (SELECT cantidad FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1,1) AS cantidad_2,

                    (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1) AS coadyuvante_1,
                    (SELECT dosis FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1) AS dosis_5,
                    (SELECT cantidad FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1) AS cantidad_5,
                    (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1,1) AS coadyuvante_2,
                    (SELECT dosis FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1,1) AS dosis_6,
                    (SELECT cantidad FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1,1) AS cantidad_6,

                    (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 6 ORDER BY id LIMIT 1) AS aceite,
                    (SELECT dosis FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 6 ORDER BY id LIMIT 1) AS dosis_7,
                    (SELECT cantidad FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 6 ORDER BY id LIMIT 1) AS cantidad_7,

                    'AGUA' AS agua, dosis_agua AS dosis_13, 0 AS cantidad_13,
                    DATEDIFF(fecha_real, (
                        SELECT MAX(fecha_real) 
                        FROM ciclos_aplicacion_hist
                        WHERE fecha_real < hist.fecha_real
                            AND id_finca = hist.id_finca
                            AND programa = hist.programa
                        )
                    ) AS frec
                FROM ciclos_aplicacion_hist hist
                INNER JOIN ciclos_aplicacion_hist_unidos unidos ON id_ciclo_aplicacion = hist.id
                WHERE anio = {$filters->year}
                    AND finca IN(
                        SELECT nombre 
                        FROM cat_fincas 
                        WHERE id_gerente = $filters->gerente
                            AND id = $filters->finca
                    )
                    AND (
                        (programa IN ('Sigatoka', 'Sigatoka Foliar', 'Sigatoka Plagas') AND UPPER(tipo_ciclo) = 'CICLO') 
                        OR programa = 'Historico'
                    )
                    AND finca IN ({$this->available_fincas})
                    $sWhere
                GROUP BY unidos._join";
        $response->table = $this->db->queryAll($sql);
        foreach ($response->table as $key => $value) {
            $value->ciclo = (double)$value->ciclo;
            $value->sem = (double)$value->sem;
            $value->ha = (double)$value->ha;

            $value->detalle = $this->db->queryAll("SELECT 
                    products.nombre_comercial, 
                    cat_tipo_productos.nombre AS tipo, 
                    prod.dosis, 
                    prod.precio, 
                    SUM(prod.cantidad) AS cantidad, 
                    (prod.precio * SUM(prod.cantidad)) AS prod, 
                    (prod.precio * SUM(prod.cantidad)) / $value->ha as prod_ha
                FROM `ciclos_aplicacion_hist` hist
                INNER JOIN ciclos_aplicacion_hist_productos prod ON id_ciclo_aplicacion = hist.id
                INNER JOIN products ON id_producto = products.id
                INNER JOIN cat_tipo_productos ON prod.id_tipo_producto = cat_tipo_productos.id
                WHERE hist.id IN({$value->ids})
                    $sWhere
                GROUP BY prod.id_producto
                ORDER BY prod.id");
        }

        /* PARCIALES */
        $sql = "SELECT hist.id, num_ciclo AS ciclo, hectareas_fumigacion AS ha, fecha_prog, fecha_real, getWeek(fecha_real) AS sem, atraso, motivo, 
                    oper,
                    costo_total,
                    costo_ha,
                    ha_oper,
                    (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1) AS fungicida_1,
                    (SELECT dosis FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1) AS dosis_1,
                    (SELECT cantidad FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1) AS cantidad_1,
                    (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1,1) AS fungicida_2,
                    (SELECT dosis FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1,1) AS dosis_2,
                    (SELECT cantidad FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1,1) AS cantidad_2,

                    (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 2  ORDER BY id LIMIT 1) AS coadyuvante_1,
                    (SELECT dosis FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1) AS dosis_5,
                    (SELECT cantidad FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1) AS cantidad_5,
                    (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1,1) AS coadyuvante_2,
                    (SELECT dosis FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1,1) AS dosis_6,
                    (SELECT cantidad FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1,1) AS cantidad_6,

                    (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 6 ORDER BY id LIMIT 1) AS aceite,
                    (SELECT dosis FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 6 ORDER BY id LIMIT 1) AS dosis_7,
                    (SELECT cantidad FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 6 ORDER BY id LIMIT 1) AS cantidad_7,

                    'AGUA' AS agua, dosis_agua AS dosis_13, 0 AS cantidad_13,
                    DATEDIFF(fecha_real, (
                        SELECT MAX(fecha_real) 
                        FROM ciclos_aplicacion_hist
                        WHERE fecha_real < hist.fecha_real
                            AND finca IN(
                                SELECT nombre 
                                FROM cat_fincas 
                                WHERE id_gerente = $filters->gerente 
                                    AND id = $filters->finca
                            )
                            AND (
                                (programa IN ('Sigatoka', 'Sigatoka Foliar', 'Plagas') AND UPPER(tipo_ciclo) = 'PARCIAL') 
                                OR programa = 'Historico'
                            )
                        )
                    ) AS frec
                FROM ciclos_aplicacion_hist hist
                WHERE anio = {$filters->year}
                    AND id_finca IN(
                        SELECT id 
                        FROM cat_fincas 
                        WHERE id_gerente = $filters->gerente
                            AND id = $filters->finca
                    )
                    AND (
                        (programa IN ('Sigatoka', 'Sigatoka Foliar', 'Sigatoka Plagas', 'Plagas') AND UPPER(tipo_ciclo) = 'PARCIAL') 
                        OR programa = 'Historico'
                    )
                    AND finca IN ({$this->available_fincas})
                    $sWhere
                GROUP BY anio, ciclo";
        $response->table_parcial = $this->db->queryAll($sql);
        foreach ($response->table_parcial as $key => $value) {
            $value->ciclo = (double)$value->ciclo;
            $value->sem = (double)$value->sem;
            $value->ha = (double)$value->ha;

            $value->detalle = $this->db->queryAll("SELECT 
                    products.nombre_comercial, 
                    cat_tipo_productos.nombre AS tipo, 
                    prod.dosis, 
                    prod.precio, 
                    SUM(prod.cantidad) AS cantidad, 
                    (prod.precio * SUM(prod.cantidad)) AS prod, 
                    (prod.precio * SUM(prod.cantidad)) / $value->ha as prod_ha
                FROM `ciclos_aplicacion_hist` hist
                INNER JOIN ciclos_aplicacion_hist_productos prod ON id_ciclo_aplicacion = hist.id
                INNER JOIN products ON id_producto = products.id
                INNER JOIN cat_tipo_productos ON prod.id_tipo_producto = cat_tipo_productos.id
                WHERE finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $filters->gerente AND id = $filters->finca)
                    AND hist.num_ciclo = {$value->ciclo} 
                    AND anio = YEAR('{$value->fecha_real}')
                    AND prod.id_tipo_producto IN(2,4,6,7,8)
                    AND ((programa IN ('Sigatoka', 'Sigatoka Foliar', 'Sigatoka Plagas', 'Plagas') AND UPPER(tipo_ciclo) = 'PARCIAL') OR programa = 'Historico')
                    AND finca IN ({$this->available_fincas})
                    $sWhere
                GROUP BY hist.num_ciclo, prod.id_producto
                ORDER BY prod.id");
        }
        
        /* VARIABLES */
        $gerentes = $this->db->queryAllSpecial("SELECT id , nombre AS label FROM cat_gerentes g WHERE status > 0 AND (SELECT COUNT(1) FROM cat_fincas WHERE id_gerente = g.id AND nombre IN ({$this->available_fincas})) > 0");
        foreach ($gerentes as $key => $value) {
            $response->gerentes[] = (object)[
                "id" => (double)$key,
                "label" => $value,
            ];
        }
        $response->years = $this->db->queryAllSpecial("SELECT anio AS id, anio AS label 
            FROM ciclos_aplicacion_hist hist
            WHERE anio > 0 AND (programa IN ('Sigatoka', 'Sigatoka Foliar', 'Sigatoka Plagas', 'Historico') AND UPPER(tipo_ciclo) = 'CICLO') AND finca IN ({$this->available_fincas}) $sWhere
            GROUP BY anio");
        $response->fincas = $this->getFincas();
        
        /* TABLA DE FRECUENCIA */
        $sql = "SELECT num_ciclo AS ciclo, finca, fecha_prog, fecha_real, atraso, motivo, 
                    (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1) AS fungicida_1,
                    (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1,1) AS fungicida_2
                FROM ciclos_aplicacion_hist hist
                WHERE anio = {$filters->year}
                    AND finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $filters->gerente AND id = $filters->finca)
                    AND ((programa IN ('Sigatoka', 'Sigatoka Foliar') AND UPPER(tipo_ciclo) = 'CICLO') OR programa = 'Historico')
                    AND finca IN ({$this->available_fincas})
                    $sWhere
                GROUP BY anio, ciclo
                ORDER BY fecha_real";
        $response->table_frecuencia = $this->db->queryAll($sql);
        foreach($response->table_frecuencia as $key => $row){
            if($key == 0){
                $last = $this->db->queryRow("SELECT MAX(fecha_real) AS fecha_real 
                                            FROM ciclos_aplicacion_hist hist
                                            WHERE fecha_real < '{$row->fecha_real}'
                                                AND finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $filters->gerente AND id = $filters->finca)
                                                AND ((programa IN ('Sigatoka', 'Sigatoka Foliar') AND UPPER(tipo_ciclo) = 'CICLO') OR programa = 'Historico')
                                                AND finca IN ({$this->available_fincas})
                                                $sWhere");
            }else{
                $last = $response->table_frecuencia[$key-1];
            }

            $datetime1 = date_create($row->fecha_prog);
            $datetime2 = date_create($row->fecha_real);
            $interval = date_diff($datetime1, $datetime2);
            $row->dias_diff = $interval->days * ($datetime1 > $datetime2 ? 1 : -1);

            $datetime1 = date_create($row->fecha_real);
            $datetime2 = date_create($last->fecha_real);
            $interval = date_diff($datetime1, $datetime2);
            $row->frec_real = $interval->days * ($datetime1 > $datetime2 ? 1 : -1);

            $datetime1 = date_create($row->fecha_prog);
            $datetime2 = date_create($last->fecha_real);
            $interval = date_diff($datetime1, $datetime2);
            $row->frec_prog = $interval->days * ($datetime1 > $datetime2 ? 1 : -1);
        }

        return json_encode($response);
    }

    public function economico(){ 
        $response = new stdClass;

        $filters = $this->getFilters();
        $tipoHec = ($filters->tipoHectarea ? $filters->tipoHectarea : "''");
        $sWhere = "";
        if($filters->programa == 'sigatoka') $sWhere .= " AND programa IN ('SIGATOKA', 'HISTORICO')";
        if($filters->programa == 'foliar') $sWhere .= " AND programa IN ('FOLIAR')";
        if($filters->programa == 'plagas') $sWhere .= " AND programa IN ('PLAGAS')";
        if($filters->programa == 'erwinia') $sWhere .= " AND programa IN ('ERWINIA')";
        if($filters->programa == 'sanidad') $sWhere .= " AND programa IN ('SIGATOKA', 'PLAGAS', 'ERWINIA')";
        if($filters->programa == 'todos') $sWhere = "";

        $tipoHa = 'FUMIGACION';
        switch($filters->tipoHectarea){
            case 'h_produccion':
                $tipoHa = 'PRODUCCION';
            break;
            case 'h_neta':
                $tipoHa = 'NETA';
            break;
            case 'h_banano':
                $tipoHa = 'BANANO';
            break;
            default : 
                $tipoHa = 'FUMIGACION';
            break;
        }
        $sql = "SELECT *,
                    IF(sigatokas > 1 OR foliares > 1, ha_oper / IF(foliares > 1, foliares, sigatokas), ha_oper) ha_oper,
                    DATEDIFF(fecha_real, (
                        SELECT MAX(fecha_real) 
                        FROM ciclos_aplicacion_hist hist
                        WHERE fecha_real < tbl.fecha_real 
                            AND id_finca = $filters->finca
                            AND UPPER(tipo_ciclo) = 'CICLO'
                            {$sWhere}
                        )
                    ) AS frec
                FROM (
                    SELECT 
                        ids,
                        IFNULL(getHaFinca(id_finca, anio, semana, '{$tipoHa}'), 1) ha_total_sem,
                        ciclo,
                        ciclo_programa,
                        sum_ciclo,
                        fecha_prog,
                        MAX(fecha_real) fecha_real,
                        SUM(ha) ha,
                        SUM(costo_total) costo_total,
                        sem,
                        atraso,
                        motivo,
                        SUM(IF(programa = 'Sigatoka', 1, 0)) sigatokas,
                        SUM(IF(programa = 'Foliar', 1, 0)) foliares,
                        SUM(costo_ha) costo_ha,
                        SUM(ha_oper) ha_oper,
                        SUM(oper) oper,
                        GROUP_CONCAT(DISTINCT programa SEPARATOR '-') programa,
                        GROUP_CONCAT(id_cycle_application SEPARATOR ',') id_cycle_application
                    FROM (
                        SELECT 
                            GROUP_CONCAT(DISTINCT hist.id SEPARATOR ',') as ids,
                            id_finca,
                            anio,
                            getWeek(fecha_real) semana,
                            num_ciclo as ciclo, 
                            num_ciclo_programa as ciclo_programa,
                            sum_ciclo,
                            SUM(IF('{$filters->tipoHectarea}' = '', hectareas_fumigacion, (SELECT {$tipoHec} FROM cat_fincas WHERE nombre = finca LIMIT 1))) AS ha,
                            fecha_prog, 
                            fecha_real, 
                            getWeek(fecha_real) AS sem, 
                            atraso, 
                            motivo,
                            costo_ha, 
                            ha_oper,
                            MAX(oper) oper,
                            GROUP_CONCAT(DISTINCT programa SEPARATOR '-') programa,
                            id_cycle_application,
                            SUM(costo_total) costo_total,
                            COUNT(1) vuelos
                        FROM ciclos_aplicacion_hist hist
                        LEFT JOIN ciclos_aplicacion_hist_unidos unidos ON id_ciclo_aplicacion = hist.id
                        WHERE anio = {$filters->year}
                            AND id_finca = $filters->finca
                            AND UPPER(tipo_ciclo) = 'CICLO'
                            {$sWhere}
                        GROUP BY unidos._join
                    ) tbl
                    GROUP BY ids
                ) AS tbl
                ORDER BY fecha_real";
                
        $response->table = $this->db->queryAll($sql);

        #$sql_parcial = str_replace("UPPER(tipo_ciclo) = 'CICLO'", "UPPER(tipo_ciclo) = 'PARCIAL'", $sql);
        #$sql_parcial = str_replace("GROUP BY unidos._join", "GROUP BY hist.id", $sql_parcial);
        #$sql_parcial = str_replace("SUM", "", $sql_parcial);

        $sql_parcial = "SELECT *,
                    IF(sigatokas > 1 OR foliares > 1, ha_oper / IF(foliares > 1, foliares, sigatokas), ha_oper) ha_oper,
                    DATEDIFF(fecha_real, (
                        SELECT MAX(fecha_real) 
                        FROM ciclos_aplicacion_hist hist
                        WHERE fecha_real < tbl.fecha_real 
                            AND finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $filters->gerente AND id = $filters->finca)
                            AND UPPER(tipo_ciclo) = 'CICLO'
                            {$sWhere}
                        )
                    ) AS frec
                FROM (
                    SELECT 
                        ids,
                        IFNULL(getHaFinca(id_finca, anio, semana, '{$tipoHa}'), 1) ha_total_sem,
                        ciclo,
                        ciclo_programa,
                        sum_ciclo,
                        fecha_prog,
                        MAX(fecha_real) fecha_real,
                        (ha) ha,
                        (costo_total) costo_total,
                        sem,
                        atraso,
                        motivo,
                        (IF(programa = 'Sigatoka', 1, 0)) sigatokas,
                        (IF(programa = 'Foliar', 1, 0)) foliares,
                        (costo_ha) costo_ha,
                        (ha_oper) ha_oper,
                        (oper) oper,
                        GROUP_CONCAT(DISTINCT programa SEPARATOR '-') programa,
                        GROUP_CONCAT(id_cycle_application SEPARATOR ',') id_cycle_application
                    FROM (
                        SELECT 
                            GROUP_CONCAT(DISTINCT hist.id SEPARATOR ',') as ids,
                            id_finca,
                            anio,
                            getWeek(fecha_real) semana,
                            num_ciclo as ciclo, 
                            num_ciclo_programa as ciclo_programa,
                            sum_ciclo,
                            hectareas_fumigacion AS ha,
                            fecha_prog, 
                            fecha_real, 
                            getWeek(fecha_real) AS sem, 
                            atraso, 
                            motivo,
                            costo_ha, 
                            ha_oper,
                            MAX(oper) oper,
                            GROUP_CONCAT(DISTINCT programa SEPARATOR '-') programa,
                            id_cycle_application,
                            (costo_total) costo_total,
                            COUNT(1) vuelos
                        FROM ciclos_aplicacion_hist hist
                        LEFT JOIN ciclos_aplicacion_hist_unidos unidos ON id_ciclo_aplicacion = hist.id
                        WHERE anio = {$filters->year}
                            AND finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $filters->gerente AND id = $filters->finca)
                            AND UPPER(tipo_ciclo) = 'PARCIAL'
                            {$sWhere}
                        GROUP BY hist.id
                    ) tbl
                    GROUP BY ids
                ) AS tbl
                ORDER BY fecha_real";
        
        $response->table_parcial = $this->db->queryAll($sql_parcial);
        
        foreach ($response->table as $key => $value) {
            /* DETALLE */

            $ss = "";
            if($value->id_cycle_application > 0) $ss .= " AND id_cycle_application IN ({$value->id_cycle_application})";
            else {
                $ss .= " AND id_cycle_application IS NULL";
            }
            $value->ha = $this->db->queryOne("SELECT MAX(ha)
                FROM (
                    SELECT programa, SUM(hectareas_fumigacion) ha
                    FROM ciclos_aplicacion_hist hist
                    WHERE hist.id IN ({$value->ids})
                    GROUP BY programa
                ) tbl
            ");
            $value->costo_total = (double)round($value->costo_total,2);
            $value->costo_ha = (double)round(($value->costo_total / $value->ha_total_sem),2);

            $sql = "SELECT 
                        products.nombre_comercial, 
                        cat_tipo_productos.nombre AS tipo, 
                        prod.dosis, 
                        prod.precio, 
                        SUM(prod.cantidad) AS cantidad, 
                        (prod.precio * SUM(prod.cantidad)) AS prod, 
                        (prod.precio * SUM(prod.cantidad)) / $value->ha_total_sem as prod_ha
                    FROM `ciclos_aplicacion_hist` hist
                    INNER JOIN ciclos_aplicacion_hist_productos prod ON id_ciclo_aplicacion = hist.id
                    INNER JOIN products ON id_producto = products.id
                    INNER JOIN cat_tipo_productos ON prod.id_tipo_producto = cat_tipo_productos.id
                    WHERE 
                        hist.id IN ({$value->ids})
                        AND tipo_ciclo = 'CICLO'
                        {$sWhere}
                    GROUP BY prod.id_producto
                    ORDER BY prod.id";
            
            $value->detalle = $this->db->queryAll($sql);
        }

        foreach($response->table_parcial as $key => $value){
            $value->costo_total = (double)round($value->costo_total,2);
            $value->costo_ha = (double)round(($value->costo_total / $value->ha),2);

            $value->detalle = $this->db->queryAll("SELECT 
                    products.nombre_comercial, 
                    cat_tipo_productos.nombre AS tipo, 
                    prod.dosis, 
                    prod.precio, 
                    prod.cantidad, 
                    (prod.precio * prod.cantidad) AS prod, 
                    (prod.precio * prod.cantidad) / $value->ha_total_sem as prod_ha
                FROM `ciclos_aplicacion_hist` hist
                INNER JOIN ciclos_aplicacion_hist_productos prod ON id_ciclo_aplicacion = hist.id
                INNER JOIN products ON id_producto = products.id
                INNER JOIN cat_tipo_productos ON prod.id_tipo_producto = cat_tipo_productos.id
                WHERE finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $filters->gerente AND id = $filters->finca)
                    AND hist.id IN( {$value->ids} )
                    AND fecha_real = '{$value->fecha_real}'
                    AND anio = YEAR('{$value->fecha_real}')
                    AND UPPER(tipo_ciclo) = 'PARCIAL'
                    {$sWhere}");
        }

        $response->table_real = $this->db->queryAll($sql);
        $gerentes = $this->db->queryAllSpecial("SELECT id , nombre AS label FROM cat_gerentes WHERE status > 0");
        foreach ($gerentes as $key => $value) {
            $response->gerentes[] = (object)[
                "id" => (double)$key,
                "label" => $value,
            ];
        }

        $response->years = $this->db->queryAllSpecial("SELECT anio AS id, anio AS label 
            FROM ciclos_aplicacion_hist hist
            WHERE anio > 0 AND UPPER(tipo_ciclo) = 'CICLO' {$sWhere}
            GROUP BY anio");
        $response->fincas = $this->getFincas();
        
        return json_encode($response);
    }

    public function lastFoliar(){
        $response = new stdClass;

        $response->last_year = end($this->db->queryAllOne("SELECT anio FROM ciclos_aplicacion_hist WHERE programa = 'Sigatoka' GROUP BY anio"));

        $gerentes = $this->db->queryAllOne("SELECT f.id_gerente FROM ciclos_aplicacion_hist INNER JOIN cat_fincas f ON id_finca = f.id WHERE anio = $response->last_year AND programa IN('Sigatoka foliar', 'foliar') GROUP BY f.id_gerente ORDER BY f.id_gerente");
        $response->gerente = count($gerentes) > 0 ? $gerentes[0] : '';

        if($response->gerente > 0){
            $fincas = $this->db->queryAllOne("SELECT f.id 
                                                FROM ciclos_aplicacion_hist hist
                                                INNER JOIN cat_fincas f ON id_finca = f.id 
                                                WHERE anio = $response->last_year AND id_gerente = $response->gerente 
                                                    AND programa IN ('Sigatoka foliar', 'foliar') 
                                                    AND (SELECT COUNT(1) 
                                                            FROM ciclos_aplicacion_hist_productos 
                                                            WHERE id_ciclo_aplicacion = hist.id 
                                                                AND id_tipo_producto IN (1, 3)) > 0
                                                GROUP BY f.id 
                                                ORDER BY f.id");
            $response->finca = $fincas[0];
        }

        return json_encode($response);
    }

    public function foliares(){
        $response = new stdClass;

        $filters = $this->getFilters();

        /* TABLA PRINCIPAL */
        $tipoHec = ($filters->tipoHectarea ? $filters->tipoHectarea : "''");
        $sql = "SELECT *,
                    (total_3 + total_4 + total_8 + total_9 + total_10 + total_6 + (IF(foliares >= 2, ha_oper / 2, 0) * ha)) AS costo_total,
                    DATEDIFF(fecha_real, (
                        SELECT MAX(fecha_real) 
                        FROM ciclos_aplicacion_hist
                        WHERE fecha_real < tbl.fecha_real 
                            AND id_finca = $filters->finca
                            AND programa IN ('Sigatoka', 'Sigatoka Foliar', 'Historico') 
                            AND UPPER(tipo_ciclo) = 'CICLO'
                    )) AS frec
                FROM (
                    SELECT *,
                        (SELECT COUNT(1)
                        FROM ciclos_aplicacion_hist hist
                        INNER JOIN ciclos_aplicacion_hist_productos ON id_ciclo_aplicacion = hist.id AND id_tipo_producto = 3
                        WHERE anio = {$filters->year}
                            AND id_finca = $filters->finca
                            AND getWeek(fecha) = tbl.sem
                        ) AS foliares
                    FROM (
                        SELECT id, 
                            num_ciclo AS ciclo, 
                            SUM(IF('{$filters->tipoHectarea}' = '', hectareas_fumigacion, (SELECT {$tipoHec} FROM cat_fincas WHERE nombre = finca LIMIT 1) )) AS ha, 
                            fecha_prog, 
                            fecha_real, 
                            getWeek(fecha_real) AS sem, 
                            atraso, 
                            motivo,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 1 ORDER BY id LIMIT 1) AS total_3,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 1 ORDER BY id LIMIT 1,1) AS total_4,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1,1) AS total_6,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 3 ORDER BY id LIMIT 1) AS total_8,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 3 ORDER BY id LIMIT 1,1) AS total_9,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 3 ORDER BY id LIMIT 2,1) AS total_10,
                            ha_oper,
                            oper
                        FROM ciclos_aplicacion_hist tbl
                        WHERE anio = {$filters->year}
                            AND id_finca = $filters->finca
                            AND programa IN ('Sigatoka Foliar', 'Foliar', 'Historico')
                            AND UPPER(tipo_ciclo) = 'CICLO'
                        GROUP BY anio, ciclo
                        ORDER BY getWeek(fecha_real)
                    ) AS tbl
                ) AS tbl";
        $response->table = $this->db->queryAll($sql);
        foreach ($response->table as $key => $value) {
            $value->sem = (int)$value->sem;
            $value->total_3 = (double)$value->total_3;
            $value->total_4 = (double)$value->total_4;
            $value->total_8 = (double)$value->total_8;
            $value->total_9 = (double)$value->total_9;
            $value->total_10 = (double)$value->total_10;
            $value->total_6 = (double)$value->total_6;
            $value->costo_total = (double)round($value->costo_total,2);
            $value->oper = (double)$value->oper;
            $value->costo_ha = (double)round(($value->costo_total / $value->ha),2);

            $value->detalle = $this->db->queryAll("SELECT 
                    products.nombre_comercial, 
                    cat_tipo_productos.nombre AS tipo, 
                    prod.dosis, 
                    prod.precio, 
                    SUM(prod.cantidad) cantidad, 
                    SUM(total) AS prod, 
                    SUM(total) / $value->ha as prod_ha
                FROM `ciclos_aplicacion_hist` hist
                INNER JOIN ciclos_aplicacion_hist_productos prod ON id_ciclo_aplicacion = hist.id
                INNER JOIN products ON id_producto = products.id
                INNER JOIN cat_tipo_productos ON prod.id_tipo_producto = cat_tipo_productos.id
                WHERE finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $filters->gerente AND id = $filters->finca)
                    AND (prod.id_tipo_producto = 1 OR prod.id_tipo_producto = 3)
                    AND num_ciclo = '{$value->ciclo}'
                    AND anio = YEAR('{$value->fecha_real}')
                    AND (programa IN('Foliar', 'Sigatoka Foliar') AND UPPER(tipo_ciclo) = 'CICLO'
                        OR (programa = 'Historico'))
                GROUP BY products.id");
        }
        $response->table_real = $this->db->queryAll($sql);

        /* TABLA PARCIALES */
        $sql_parcial = "SELECT *,
                    (total_3 + total_4 + total_8 + total_9 + total_10 + total_6 + (IF(foliares >= 2, ha_oper / 2, 0) * ha)) AS costo_total,
                    DATEDIFF(fecha_real, (
                        SELECT MAX(fecha_real) 
                        FROM ciclos_aplicacion_hist
                        WHERE fecha_real < tbl.fecha_real 
                            AND id_finca = $filters->finca
                            AND programa IN ('Sigatoka', 'Sigatoka Foliar', 'Historico') 
                            AND UPPER(tipo_ciclo) = 'PARCIAL'
                    )) AS frec
                FROM (
                    SELECT *,
                        (
                            SELECT COUNT(1)
                            FROM ciclos_aplicacion_hist hist
                            INNER JOIN ciclos_aplicacion_hist_productos ON id_ciclo_aplicacion = hist.id AND id_tipo_producto = 3
                            WHERE hist.id = tbl.id
                        ) AS foliares
                    FROM (
                        SELECT id, 
                            num_ciclo AS ciclo, 
                            SUM(IF('{$filters->tipoHectarea}' = '', hectareas_fumigacion, (SELECT {$tipoHec} FROM cat_fincas WHERE nombre = finca LIMIT 1) )) AS ha, 
                            fecha_prog, 
                            fecha_real, 
                            semana AS sem, 
                            atraso, 
                            motivo,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 1 ORDER BY id LIMIT 1) AS total_3,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 1 ORDER BY id LIMIT 1,1) AS total_4,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1,1) AS total_6,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 3 ORDER BY id LIMIT 1) AS total_8,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 3 ORDER BY id LIMIT 1,1) AS total_9,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 3 ORDER BY id LIMIT 2,1) AS total_10,
                            ha_oper,
                            oper
                        FROM ciclos_aplicacion_hist tbl
                        WHERE anio = {$filters->year}
                            AND id_finca = $filters->finca
                            AND programa IN ('Sigatoka Foliar', 'Foliar', 'Historico')
                            AND UPPER(tipo_ciclo) = 'PARCIAL'
                        ORDER BY semana
                    ) AS tbl
                ) AS tbl
                HAVING ha > 0";
                
        $response->parciales = $this->db->queryAll($sql_parcial);
        foreach ($response->parciales as $key => $value) {
            $value->sem = (int)$value->sem;
            $value->total_3 = (double)$value->total_3;
            $value->total_4 = (double)$value->total_4;
            $value->total_8 = (double)$value->total_8;
            $value->total_9 = (double)$value->total_9;
            $value->total_10 = (double)$value->total_10;
            $value->total_6 = (double)$value->total_6;
            $value->costo_total = (double)round($value->costo_total,2);
            $value->oper = (double)$value->oper;
            $value->costo_ha = (double)round(($value->costo_total / $value->ha),2);

            $value->detalle = $this->db->queryAll("SELECT 
                    products.nombre_comercial, 
                    cat_tipo_productos.nombre AS tipo, 
                    prod.dosis, 
                    prod.precio, 
                    prod.cantidad, 
                    (prod.precio * prod.cantidad) AS prod, 
                    (prod.precio * prod.cantidad) / $value->ha as prod_ha
                FROM `ciclos_aplicacion_hist` hist
                INNER JOIN ciclos_aplicacion_hist_productos prod ON id_ciclo_aplicacion = hist.id
                INNER JOIN products ON id_producto = products.id
                INNER JOIN cat_tipo_productos ON prod.id_tipo_producto = cat_tipo_productos.id
                WHERE finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $filters->gerente AND id = $filters->finca)
                    AND (prod.id_tipo_producto = 1 OR prod.id_tipo_producto = 3)
                    AND num_ciclo = '{$value->ciclo}'
                    AND anio = YEAR('{$value->fecha_real}')
                    AND (programa IN('Foliar', 'Sigatoka Foliar') AND UPPER(tipo_ciclo) = 'PARCIAL'
                        OR (programa = 'Historico'))");
        }
        
        /* VARIABLES */
        $response->gerentes = $this->db->queryAll("SELECT id, nombre AS label 
            FROM cat_gerentes 
            WHERE status > 0 
                AND id IN(
                    SELECT id_gerente
                    FROM ciclos_aplicacion_hist
                    INNER JOIN cat_fincas f ON f.nombre = finca
                    WHERE anio = {$filters->year}
                        AND programa IN ('Foliar', 'Sigatoka Foliar', 'Historico') 
                )");

        $response->fincas = [
            "{$filters->gerente}" => $this->db->queryAll("SELECT id, nombre AS label
                FROM cat_fincas
                WHERE status > 0
                    AND id_gerente = {$filters->gerente}
                    AND nombre IN(
                        SELECT finca
                        FROM ciclos_aplicacion_hist
                        WHERE anio = {$filters->year}
                            AND programa IN ('Foliar', 'Sigatoka Foliar', 'Historico') 
                            AND UPPER(tipo_ciclo) = 'CICLO'
                    )")
        ];

        /*$response->years = $this->db->queryAllSpecial("SELECT anio AS id, anio AS label 
            FROM ciclos_aplicacion_hist
            WHERE anio > 0 AND (programa IN ('Foliar', 'Sigatoka Foliar') AND finca IN ({$this->available_fincas})
            GROUP BY anio");
        */
        $response->years = $this->db->queryAllSpecial("SELECT anio AS id, anio AS label 
            FROM ciclos_aplicacion_hist
            WHERE anio > 0 AND programa IN('Foliar', 'Sigatoka Foliar') 
            GROUP BY anio");

        return json_encode($response);
    }
    
    public function plagas(){
        $response = new stdClass;

        $filters = $this->getFilters();

        $tipoHec = ($filters->tipoHectarea ? $filters->tipoHectarea : "''");
        $sql = "SELECT *,
                    (total_insecticida_1 + total_insecticida_2) AS costo_total,
                    DATEDIFF(fecha_real, 
                        (
                            SELECT MAX(fecha_real) 
                            FROM ciclos_aplicacion_hist
                            WHERE fecha_real < tbl.fecha_real 
                                AND id_finca = $filters->finca
                                AND programa IN ('Sigatoka Plagas', 'Plagas')
                                AND tipo_ciclo = 'CICLO'
                        )
                    ) AS frec
                FROM (
                    SELECT *,
                        (
                            SELECT COUNT(1)
                            FROM ciclos_aplicacion_hist hist
                            INNER JOIN ciclos_aplicacion_hist_productos ON id_ciclo_aplicacion = hist.id AND id_tipo_producto = 5
                            WHERE anio = {$filters->year}
                                AND id_finca = $filters->finca
                                AND num_ciclo = tbl.ciclo
                                AND programa IN ('Sigatoka Plagas', 'Plagas')
                                AND tipo_ciclo = 'CICLO'
                        ) AS insecticidas
                    FROM (
                        SELECT id, 
                            num_ciclo AS ciclo, 
                            SUM(IF('{$filters->tipoHectarea}' = '', hectareas_fumigacion, (SELECT {$tipoHec} FROM cat_fincas WHERE nombre = finca LIMIT 1) )) AS ha, 
                            fecha_prog, 
                            fecha_real, 
                            semana AS sem, 
                            atraso, 
                            motivo,
                            (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 5 ORDER BY id LIMIT 1) AS insecticida_1,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 5 ORDER BY id LIMIT 1) AS total_insecticida_1,
                            (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 5 ORDER BY id LIMIT 1,1) AS insecticida_2,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 5 ORDER BY id LIMIT 1,1) AS total_insecticida_2,
                            ha_oper,
                            oper
                        FROM ciclos_aplicacion_hist tbl
                        WHERE anio = {$filters->year}
                            AND id_finca = $filters->finca
                            AND programa IN ('Sigatoka Plagas', 'Plagas')
                            AND tipo_ciclo = 'CICLO'
                            AND EXISTS (SELECT * FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 5)
                        GROUP BY anio, ciclo
                        ORDER BY semana
                    ) AS tbl
                ) AS tbl";
        $response->table = $this->db->queryAll($sql);
        foreach ($response->table as $key => $value) {
            $value->sem = (double)$value->sem;
            $value->costo_total = (double)round($value->costo_total,2);
            $value->oper = (double)$value->oper;
            $value->costo_ha = (double)round(($value->costo_total / $value->ha),2);

   
            $value->detalle = $this->db->queryAll("SELECT products.nombre_comercial, cat_tipo_productos.nombre AS tipo, detalle.dosis, detalle.precio, detalle.cantidad, (detalle.precio * detalle.cantidad) AS prod, (detalle.precio * detalle.cantidad) / $value->ha as prod_ha
                FROM ciclos_aplicacion_hist hist
                INNER JOIN `ciclos_aplicacion_hist_productos` detalle ON id_ciclo_aplicacion = hist.id 
                INNER JOIN products ON id_producto = products.id
                INNER JOIN cat_tipo_productos ON products.id_tipo_producto = cat_tipo_productos.id
                WHERE id_finca IN(SELECT id FROM cat_fincas WHERE id_gerente = $filters->gerente AND id = $filters->finca)
                    AND (products.id_tipo_producto = 5)
                    AND num_ciclo = '{$value->ciclo}'
                    AND hist.anio = YEAR('{$value->fecha_real}')
                    AND (programa IN('Plagas', 'Sigatoka Plagas') OR (programa = 'Historico'))
                    AND tipo_ciclo = 'CICLO'");
        }
        $response->table_real = $this->db->queryAll($sql);

        // PARCIALES
        $sql = "SELECT *,
                    (total_insecticida_1 + total_insecticida_2) AS costo_total,
                    DATEDIFF(fecha_real, 
                        (
                            SELECT MAX(fecha_real) 
                            FROM ciclos_aplicacion_hist
                            WHERE fecha_real < tbl.fecha_real 
                                AND id_finca = $filters->finca
                                AND programa IN ('Sigatoka Plagas', 'Plagas')
                                AND tipo_ciclo = 'PARCIAL'
                        )
                    ) AS frec
                FROM (
                    SELECT *,
                        (
                            SELECT COUNT(1)
                            FROM ciclos_aplicacion_hist hist
                            INNER JOIN ciclos_aplicacion_hist_productos ON id_ciclo_aplicacion = hist.id AND id_tipo_producto = 5
                            WHERE anio = {$filters->year}
                                AND id_finca = $filters->finca
                                AND num_ciclo = tbl.ciclo
                                AND programa IN ('Sigatoka Plagas', 'Plagas')
                                AND tipo_ciclo = 'PARCIAL'
                        ) AS insecticidas
                    FROM (
                        SELECT id, 
                            num_ciclo AS ciclo, 
                            SUM(IF('{$filters->tipoHectarea}' = '', hectareas_fumigacion, (SELECT {$tipoHec} FROM cat_fincas WHERE nombre = finca LIMIT 1) )) AS ha, 
                            fecha_prog, 
                            fecha_real, 
                            semana AS sem, 
                            atraso, 
                            motivo,
                            (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 5 ORDER BY id LIMIT 1) AS insecticida_1,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 5 ORDER BY id LIMIT 1) AS total_insecticida_1,
                            (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 5 ORDER BY id LIMIT 1,1) AS insecticida_2,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 5 ORDER BY id LIMIT 1,1) AS total_insecticida_2,
                            ha_oper,
                            oper
                        FROM ciclos_aplicacion_hist tbl
                        WHERE anio = {$filters->year}
                            AND id_finca = $filters->finca
                            AND programa IN ('Sigatoka Plagas', 'Plagas')
                            AND tipo_ciclo = 'PARCIAL'
                        GROUP BY anio, ciclo
                        ORDER BY semana
                    ) AS tbl
                ) AS tbl";
        
        $response->parciales = $this->db->queryAll($sql);
        foreach ($response->parciales as $key => $value) {
            $value->sem = (double)$value->sem;
            $value->costo_total = (double)round($value->costo_total,2);
            $value->oper = (double)$value->oper;
            $value->costo_ha = (double)round(($value->costo_total / $value->ha),2);

   
            $value->detalle = $this->db->queryAll("SELECT products.nombre_comercial, cat_tipo_productos.nombre AS tipo, detalle.dosis, detalle.precio, detalle.cantidad, (detalle.precio * detalle.cantidad) AS prod, (detalle.precio * detalle.cantidad) / $value->ha as prod_ha
                FROM ciclos_aplicacion_hist hist
                INNER JOIN `ciclos_aplicacion_hist_productos` detalle ON id_ciclo_aplicacion = hist.id 
                INNER JOIN products ON id_producto = products.id
                INNER JOIN cat_tipo_productos ON products.id_tipo_producto = cat_tipo_productos.id
                WHERE id_finca IN(SELECT id FROM cat_fincas WHERE id_gerente = $filters->gerente AND id = $filters->finca)
                    AND (products.id_tipo_producto = 5)
                    AND num_ciclo = '{$value->ciclo}'
                    AND hist.anio = YEAR('{$value->fecha_real}')
                    AND (programa IN('Plagas', 'Sigatoka Plagas') OR (programa = 'Historico'))
                    AND tipo_ciclo = 'PARCIAL'");
        }
        
        // VARIABLES
        $response->gerentes = $this->db->queryAll("SELECT id, nombre AS label 
            FROM cat_gerentes 
            WHERE status > 0 
                AND id IN(
                    SELECT id_gerente
                    FROM ciclos_aplicacion_hist
                    INNER JOIN cat_fincas f ON f.nombre = finca
                    WHERE anio = {$filters->year}
                        AND programa IN ('Sigatoka Plagas', 'Plagas')
                )");

        $response->fincas = [
            "{$filters->gerente}" => $this->db->queryAll("SELECT id, nombre AS label
                FROM cat_fincas
                WHERE status > 0
                    AND id_gerente = {$filters->gerente}
                    AND nombre IN(
                        SELECT finca
                        FROM ciclos_aplicacion_hist
                        WHERE anio = {$filters->year}
                            AND programa IN ('Sigatoka Plagas', 'Plagas')
                    )")
        ];
        
        $response->years = $this->db->queryAllSpecial("SELECT anio AS id, anio AS label 
            FROM ciclos_aplicacion_hist
            WHERE anio > 0 
                AND programa IN('Plagas', 'Sigatoka Plagas')
            GROUP BY anio");
        return json_encode($response);
    }

    public function lastPlagas(){
        $response = new stdClass;

        $response->last_year = end($this->db->queryAllOne("SELECT anio FROM ciclos_aplicacion_hist WHERE programa = 'PLAGAS' GROUP BY anio"));

        $gerentes = $this->db->queryAllOne("SELECT f.id_gerente FROM ciclos_aplicacion_hist INNER JOIN cat_fincas f ON id_finca = f.id WHERE anio = $response->last_year AND programa = 'PLAGAS' GROUP BY f.id_gerente ORDER BY f.id_gerente");
        $response->gerente = count($gerentes) > 0 ? $gerentes[0] : '';

        if($response->gerente > 0){
            $fincas = $this->db->queryAllOne("SELECT f.id FROM ciclos_aplicacion_hist INNER JOIN cat_fincas f ON id_finca = f.id WHERE anio = $response->last_year AND id_gerente = $response->gerente AND programa IN ('Sigatoka Plagas', 'Plagas') AND UPPER(tipo_ciclo) = 'CICLO' GROUP BY f.id ORDER BY f.id");
            $response->finca = $fincas[0];
        }

        return json_encode($response);
    }

    public function otrosSigatokas(){
        $response = new stdClass;

        $filters = $this->getFilters();

        $tipoHec = ($filters->tipoHectarea ? $filters->tipoHectarea : "''");
        $sql = "SELECT *,
                    (total_insecticida_1 + total_insecticida_2) AS costo_total,
                    DATEDIFF(fecha_real, 
                        (SELECT MAX(fecha_real) 
                        FROM ciclos_aplicacion_hist
                        WHERE fecha_real < tbl.fecha_real 
                            AND id_finca = $filters->finca
                            AND programa IN ('Sigatoka') 
                            AND UPPER(tipo_ciclo) = 'CICLO'
                        )
                    ) AS frec
                FROM (
                    SELECT *,
                        (
                            SELECT COUNT(1)
                            FROM ciclos_aplicacion_hist hist
                            INNER JOIN ciclos_aplicacion_hist_productos ON id_ciclo_aplicacion = hist.id AND id_tipo_producto = 9
                            WHERE anio = {$filters->year}
                                AND id_finca = $filters->finca
                                AND semana = tbl.sem
                                AND programa IN ('Sigatoka') 
                        ) AS insecticidas
                    FROM (
                        SELECT id, 
                            num_ciclo AS ciclo, 
                            SUM(IF('{$filters->tipoHectarea}' = '', hectareas_fumigacion, (SELECT {$tipoHec} FROM cat_fincas WHERE nombre = finca LIMIT 1) )) AS ha, 
                            fecha_prog, 
                            fecha_real, 
                            semana AS sem, 
                            atraso, 
                            motivo,
                            (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 9 ORDER BY id LIMIT 1) AS insecticida_1,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 9 ORDER BY id LIMIT 1) AS total_insecticida_1,
                            (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 9 ORDER BY id LIMIT 1,1) AS insecticida_2,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 9 ORDER BY id LIMIT 1,1) AS total_insecticida_2,
                            ha_oper,
                            oper
                        FROM ciclos_aplicacion_hist tbl
                        WHERE anio = {$filters->year}
                            AND id_finca = $filters->finca
                            AND programa IN ('Sigatoka')  
                            AND UPPER(tipo_ciclo) = 'CICLO'
                        GROUP BY anio, ciclo
                        ORDER BY semana
                    ) AS tbl
                    HAVING insecticidas > 0
                ) AS tbl";
        $response->table = $this->db->queryAll($sql);
        foreach ($response->table as $key => $value) {
            $value->sem = (double)$value->sem;
            $value->costo_total = (double)round($value->costo_total,2);
            $value->oper = (double)$value->oper;
            $value->costo_ha = (double)round(($value->costo_total / $value->ha),2);

            $value->detalle = $this->db->queryAll("SELECT products.nombre_comercial, cat_tipo_productos.nombre AS tipo, prod.dosis, prod.precio, prod.cantidad, (prod.precio * prod.cantidad) AS prod, (prod.precio * prod.cantidad) / $value->ha as prod_ha
                FROM `ciclos_aplicacion_hist` hist
                INNER JOIN ciclos_aplicacion_hist_productos prod ON id_ciclo_aplicacion = hist.id
                INNER JOIN products ON id_producto = products.id
                INNER JOIN cat_tipo_productos ON prod.id_tipo_producto = cat_tipo_productos.id
                WHERE finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $filters->gerente AND id = $filters->finca)
                    AND (prod.id_tipo_producto = 9)
                    AND num_ciclo = '{$value->ciclo}'
                    AND anio = YEAR('{$value->fecha_real}')
                    AND programa IN('Sigatoka') 
                    AND UPPER(tipo_ciclo) = 'CICLO'");
        }
        $response->table_real = $this->db->queryAll($sql);

        $sql = "SELECT *,
                    (total_insecticida_1 + total_insecticida_2) AS costo_total,
                    DATEDIFF(fecha_real, 
                        (SELECT MAX(fecha_real) 
                        FROM ciclos_aplicacion_hist
                        WHERE fecha_real < tbl.fecha_real 
                            AND id_finca = $filters->finca
                            AND programa IN ('Sigatoka') 
                            AND UPPER(tipo_ciclo) = 'PARCIAL'
                        )
                    ) AS frec
                FROM (
                    SELECT *,
                        (
                            SELECT COUNT(1)
                            FROM ciclos_aplicacion_hist hist
                            INNER JOIN ciclos_aplicacion_hist_productos ON id_ciclo_aplicacion = hist.id AND id_tipo_producto = 9
                            WHERE anio = {$filters->year}
                                AND id_finca = $filters->finca
                                AND semana = tbl.sem
                                AND programa IN ('Sigatoka') 
                        ) AS insecticidas
                    FROM (
                        SELECT id, 
                            num_ciclo AS ciclo, 
                            SUM(IF('{$filters->tipoHectarea}' = '', hectareas_fumigacion, (SELECT {$tipoHec} FROM cat_fincas WHERE nombre = finca LIMIT 1) )) AS ha, 
                            fecha_prog, 
                            fecha_real, 
                            semana AS sem, 
                            atraso, 
                            motivo,
                            (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 9 ORDER BY id LIMIT 1) AS insecticida_1,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 9 ORDER BY id LIMIT 1) AS total_insecticida_1,
                            (SELECT producto FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 9 ORDER BY id LIMIT 1,1) AS insecticida_2,
                            (SELECT total FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = tbl.id AND id_tipo_producto = 9 ORDER BY id LIMIT 1,1) AS total_insecticida_2,
                            ha_oper,
                            oper
                        FROM ciclos_aplicacion_hist tbl
                        WHERE anio = {$filters->year}
                            AND id_finca = $filters->finca
                            AND programa IN ('Sigatoka')  
                            AND UPPER(tipo_ciclo) = 'PARCIAL'
                        GROUP BY anio, ciclo
                        ORDER BY semana
                    ) AS tbl
                    HAVING insecticidas > 0
                ) AS tbl";
        $response->parciales = $this->db->queryAll($sql);
        foreach ($response->parciales as $key => $value) {
            $value->sem = (double)$value->sem;
            $value->costo_total = (double)round($value->costo_total,2);
            $value->oper = (double)$value->oper;
            $value->costo_ha = (double)round(($value->costo_total / $value->ha),2);

            $value->detalle = $this->db->queryAll("SELECT products.nombre_comercial, cat_tipo_productos.nombre AS tipo, detalle.dosis, detalle.precio, detalle.cantidad, (detalle.precio * detalle.cantidad) AS prod, (detalle.precio * detalle.cantidad) / $value->ha as prod_ha
                FROM ciclos_aplicacion_detalle detalle 
                INNER JOIN products ON id_producto = products.id
                INNER JOIN cat_tipo_productos ON id_tipo_producto = cat_tipo_productos.id
                WHERE finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $filters->gerente AND id = $filters->finca)
                    AND (id_tipo_producto = 9)
                    AND ciclo = '{$value->ciclo}'
                    AND tipo_ciclo = 'PARCIAL'
                    AND YEAR(fecha) = YEAR('{$value->fecha_real}')
                    AND (programa IN('Sigatoka') OR (programa = 'Historico'))");
        }
        
        $response->gerentes = $this->db->queryAll("SELECT id, nombre AS label 
            FROM cat_gerentes 
            WHERE status > 0 
                AND id IN(
                    SELECT id_gerente
                    FROM ciclos_aplicacion_hist hist
                    INNER JOIN cat_fincas f ON f.nombre = finca
                    WHERE anio = {$filters->year}
                        AND programa IN ('Sigatoka') 
                        AND (SELECT COUNT(1) FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 9) > 0
                )");

        $response->fincas = [
            "{$filters->gerente}" => $this->db->queryAll("SELECT id, nombre AS label
                FROM cat_fincas
                WHERE status > 0
                    AND id_gerente = {$filters->gerente}
                    AND nombre IN(
                        SELECT finca
                        FROM ciclos_aplicacion_hist hist
                        WHERE anio = {$filters->year}
                            AND programa IN ('Sigatoka') 
                            AND (SELECT COUNT(1) FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 9) > 0
                    )")
        ];
        
        $response->years = $this->db->queryAllSpecial("SELECT anio AS id, anio AS label 
            FROM ciclos_aplicacion_hist
            WHERE anio > 0 
                AND programa IN('Sigatoka') 
            GROUP BY anio");
        return json_encode($response);
    }

    public function lastOtrosSigatokas(){
        $response = new stdClass;

        $response->last_year = end($this->db->queryAllOne("SELECT anio FROM ciclos_aplicacion_hist WHERE programa = 'Sigatoka' GROUP BY anio"));

        $gerentes = $this->db->queryAllOne("SELECT f.id_gerente FROM ciclos_aplicacion_hist INNER JOIN cat_fincas f ON id_finca = f.id WHERE anio = $response->last_year AND programa = 'Sigatoka' GROUP BY f.id_gerente ORDER BY f.id_gerente");
        $response->gerente = count($gerentes) > 0 ? $gerentes[0] : '';

        if($response->gerente > 0){
            $fincas = $this->db->queryAllOne("SELECT f.id 
                                                FROM ciclos_aplicacion_hist hist
                                                INNER JOIN cat_fincas f ON id_finca = f.id 
                                                WHERE anio = $response->last_year AND id_gerente = $response->gerente 
                                                    AND programa IN ('Sigatoka') 
                                                    AND UPPER(tipo_ciclo) = 'CICLO' 
                                                    AND (SELECT COUNT(1) FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = hist.id AND id_tipo_producto = 9) > 0
                                                GROUP BY f.id 
                                                ORDER BY f.id");
            $response->finca = $fincas[0];
        }

        return json_encode($response);
    }

    private function getFincas(){
        $response = new stdClass; 
        $response->data = [];
        $sql = "SELECT id , id_gerente , nombre FROM cat_fincas WHERE status > 0 AND nombre IN ({$this->available_fincas})";
        $data = $this->db->queryAll($sql);
        foreach ($data as $key => $value) {
            $response->data[$value->id_gerente][] = (object)[
                "id" => (double)$value->id,
                "label" => $value->nombre,
            ];	
        }
        return $response->data;
    }

    public function insertarHistorico(){
        $path = realpath('../excel/ciclos/user_1');
        $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
        foreach($objects as $name => $object){
            if('.' != $object->getFileName() && '..' != $object->getFileName()){
                $pos1 = strpos($object->getPathName(), "ciclos/");

                if($pos1 !== false){
                    $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
                    if('xlsx' == $ext){
                        switch ($ext){
                            case 'xlsx':
                                try {
                                    $ruta = pathinfo($object->getPathName(), PATHINFO_DIRNAME);
                                    $carpetas = explode("/", $ruta);
                                    $num_dir = count($carpetas)-1;
                                    $filename = "../excel/ciclos/user_1/".pathinfo($object->getPathName(), PATHINFO_BASENAME);
                                    $this->procesar($filename);
                                } catch (Exception $e) {
                                    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                                }
                                break;

                            default:
                                break;
                        }
                    }
                }
            }
        }
    }

    public function fracTecnico(){
        $response = new stdClass;
        $filters = $this->getFilters();

        $sql = "SELECT nombre, cat_frac.frac
                FROM cat_frac
                ORDER BY cat_frac.frac DESC";
        $response->data = $this->db->queryAll($sql);
        foreach($response->data as $frac){            
            $frac->prog = $this->db->queryOne("SELECT COUNT(DISTINCT num_ciclo) AS val 
                FROM products
                LEFT JOIN `ciclos_aplicacion_hist` h ON id_finca = {$filters->finca} AND anio = {$filters->year} AND tipo_ciclo = 'CICLO' AND programa = 'Sigatoka'
                LEFT JOIN `ciclos_aplicacion_hist_productos` p ON h.id = p.`id_ciclo_aplicacion` AND id_producto = products.id
                WHERE p.id_tipo_producto = 4
                    AND (frac = '{$frac->nombre}' OR frac_2 = '{$frac->nombre}')");
        }

        return json_encode($response);
    }

    private function procesar($filename){
        require('../excel/simplexlsx.class.php');
        $xlsx = new SimpleXLSX($filename);
        $clientes = $xlsx->sheetNames();
        $registros = array();
        foreach ($clientes as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $row){
                if($key > 0 && $row[0] == "FOLIAR"){
                    $finca = $row[1];
                    $ciclo = $row[2];

                    $fecha_real = $row[5];
                    if(is_numeric($fecha_real)){
                        $UNIX_DATE = ($fecha_real - 25569) * 86400;
                        $fecha_real = gmdate("Y-m-d", $UNIX_DATE);
                    }

                    $fecha_prog = $row[4];
                    if(is_numeric($fecha_prog)){
                        $UNIX_DATE = ($fecha_prog - 25569) * 86400;
                        $fecha_prog = gmdate("Y-m-d", $UNIX_DATE);
                    }

                    $exist = $this->db->queryAll("SELECT * FROM ciclos_aplicacion_historico WHERE finca = '{$finca}' AND ciclo = '{$ciclo}' AND YEAR(fecha_real) = YEAR('{$fecha_real}')");
                    if(count($exist) == 0 && $finca != '' && $ciclo != ''){
                        $sql = "INSERT INTO ciclos_aplicacion_historico SET
                                    finca = '{$finca}',
                                    ciclo = '{$ciclo}',
                                    ha = '{$row[3]}',
                                    fecha_prog = '{$fecha_prog}',
                                    fecha_real = '{$fecha_real}',
                                    sem = '{$row[6]}',
                                    atraso = '{$row[7]}',
                                    motivo = '{$row[8]}',
                                    fungicida_1 = '{$row[9]}',
                                    dosis_1 = '{$row[10]}',
                                    cantidad_1 = '{$row[11]}',
                                    precio_1 = '{$row[12]}',
                                    ha_1 = '{$row[13]}',
                                    total_1 = '{$row[14]}',
                                    
                                    fungicida_2 = '{$row[15]}',
                                    dosis_2 = '{$row[16]}',
                                    cantidad_2 = '{$row[17]}',
                                    precio_2 = '{$row[18]}',
                                    ha_2 = '{$row[19]}',
                                    total_2 = '{$row[20]}',
                                    
                                    bioestimulantes_1 = '{$row[21]}',
                                    dosis_3 = '{$row[22]}',
                                    cantidad_3 = '{$row[23]}',
                                    precio_3 = '{$row[24]}',
                                    ha_3 = '{$row[25]}',
                                    total_3 = '{$row[26]}',

                                    bioestimulantes_2 = '{$row[27]}',
                                    dosis_4 = '{$row[28]}',
                                    cantidad_4 = '{$row[29]}',
                                    precio_4 = '{$row[30]}',
                                    ha_4 = '{$row[31]}',
                                    total_4 = '{$row[32]}',

                                    coadyuvante_1 = '{$row[33]}',
                                    dosis_5 = '{$row[34]}',
                                    cantidad_5 = '{$row[35]}',
                                    precio_5 = '{$row[36]}',
                                    ha_5 = '{$row[37]}',
                                    total_5 = '{$row[38]}',

                                    coadyuvante_2 = '{$row[39]}',
                                    dosis_6 = '{$row[40]}',
                                    cantidad_6 = '{$row[41]}',
                                    precio_6 = '{$row[42]}',
                                    ha_6 = '{$row[43]}',
                                    total_6 = '{$row[44]}',

                                    aceite = '{$row[45]}',
                                    dosis_7 = '{$row[46]}',
                                    cantidad_7 = '{$row[47]}',
                                    precio_7 = '{$row[48]}',
                                    ha_7 = '{$row[49]}',
                                    total_7 = '{$row[50]}',

                                    foliar_1 = '{$row[51]}',
                                    dosis_8 = '{$row[52]}',
                                    cantidad_8 = '{$row[53]}',
                                    precio_8 = '{$row[54]}',
                                    ha_8 = '{$row[55]}',
                                    total_8 = '{$row[56]}',

                                    foliar_2 = '{$row[57]}',
                                    dosis_9 = '{$row[58]}',
                                    cantidad_9 = '{$row[59]}',
                                    precio_9 = '{$row[60]}',
                                    ha_9 = '{$row[61]}',
                                    total_9 = '{$row[62]}',

                                    foliar_3 = '{$row[63]}',
                                    dosis_10 = '{$row[64]}',
                                    cantidad_10 = '{$row[65]}',
                                    precio_10 = '{$row[66]}',
                                    ha_10 = '{$row[67]}',
                                    total_10 = '{$row[68]}',

                                    foliar_4 = '{$row[69]}',
                                    dosis_11 = '{$row[70]}',
                                    cantidad_11 = '{$row[71]}',
                                    precio_11 = '{$row[72]}',
                                    ha_11 = '{$row[73]}',
                                    total_11 = '{$row[74]}',

                                    insecticida = '{$row[75]}',
                                    dosis_12 = '{$row[76]}',
                                    cantidad_12 = '{$row[77]}',
                                    precio_12 = '{$row[78]}',
                                    ha_12 = '{$row[79]}',
                                    total_12 = '{$row[80]}',

                                    agua = '{$row[81]}',
                                    dosis_13 = '{$row[82]}',
                                    cantidad_13 = '{$row[83]}',

                                    ha_oper = '{$row[84]}',
                                    oper = '{$row[85]}',
                                    costo_total = '{$row[86]}',
                                    costo_ha = '{$row[87]}',
                                    years = YEAR('{$fecha_real}'),
                                    tipo_aplicacion = 'FOLIAR'
                                    ";
                        echo "<pre>".$sql."</pre>";
                        $this->db->query($sql);
                    }
                }
            }
        }
    }
    
    private function getFilters(){
        $data = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->gerente = (int)$data->params->gerente;
        $response->finca = (int)$data->params->finca;
        $response->year = (int)$data->params->year;
        $response->ciclo = (int)$data->params->ciclo;
        $response->finca = (int)$data->params->finca;
        $response->tipoHectarea = $data->params->tipoHectarea;
        $response->programa = $data->params->programa;
        //if($response->tipoHectarea == "") $response->tipoHectarea = "''";

        if($response->gerente <= 0){
            $response->gerente = $this->gerenteDefault;
        }
        
        if($response->finca <= 0){
            $response->finca = $this->fincarDefault;
        }

        return $response;
    }
}
?>
