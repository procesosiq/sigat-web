<?php
class ClimaDiarioOrodelti {
	
	private $db;
	private $session;
	
	public function __construct() {
        $this->db = new M_Conexion();
        $this->session = Session::getInstance();
    }

    private function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));


        $filters = (object)[
            "fecha_inicial" => $postdata->params->fecha_inicial,
            "fecha_final" => $postdata->params->fecha_final,
            "hora_inicio" => $postdata->params->hora_inicio,
            "hora_fin" => $postdata->params->hora_fin,
            "luz" => $postdata->params->luz,
            "estacion" => $postdata->params->estacion
        ];
        return $filters;
    }

    public function last(){
        $response = new stdClass;
        $filters = $this->params();
        if($filters->estacion != ''){
            $sWhere .= " AND estacion_id = '{$filters->estacion}'";
        }
        
        $response->fecha = $this->db->queryOne("SELECT MAX(fecha) FROM datos_clima_orodelti WHERE 1=1 $sWhere");
        $response->hora = $this->db->queryOne("SELECT MAX(hora) FROM datos_clima_orodelti WHERE fecha = '{$response->fecha}' ");

        return json_encode($response);
    }

    public function estaciones(){
        $response = new stdClass;
        $filters = $this->params();

        if($filters->fecha_inicial != '' && $filters->fecha_final != ''){
            $sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->hora_inicio != ''){
            $sWhere .= " AND hora >= '{$filters->hora_inicio}'";
        }
        if($filters->hora_fin != ''){
            $sWhere .= " AND hora <= '{$filters->hora_fin}'";
        }

        $response->estaciones = $this->db->queryAll("SELECT station_id AS id, station_name AS label FROM datos_clima_orodelti d INNER JOIN cat_estaciones_clima e ON d.estacion_id = e.station_id WHERE 1=1 $sWhere GROUP BY estacion_id");
        return json_encode($response);
    }

	public function tags(){
        $response = new stdClass;
        $filters = $this->params();

        if($filters->fecha_inicial != '' && $filters->fecha_final != ''){
            $sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->hora_inicio != ''){
            $sWhere .= " AND hora >= '{$filters->hora_inicio}'";
        }
        if($filters->hora_fin != ''){
            $sWhere .= " AND hora <= '{$filters->hora_fin}'";
        }
        if($filters->estacion != ''){
            $sWhere .= $sGerente = " AND estacion_id = '{$filters->estacion}'";
        }

        $response->tags = new stdClass;
        $response->tags->temp_maxima = [
            "value" => $this->db->queryOne("SELECT MAX(temp_maxima) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
            "acumulado" => $this->db->queryOne("SELECT ROUND(AVG(temp), 2)
                FROM (
                    SELECT fecha, MAX(temp_maxima) AS temp
                    FROM datos_clima_orodelti 
                    WHERE YEAR(fecha) = YEAR('{$filters->fecha_inicial}') $sGerente
                    GROUP BY fecha
                ) AS tbl")
        ];
        $response->tags->temp_minima = [
            "value" => $this->db->queryOne("SELECT MIN(temp_minima) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
            "acumulado" => $this->db->queryOne("SELECT ROUND(AVG(temp), 2)
                FROM (
                    SELECT fecha, MIN(temp_minima) AS temp
                    FROM datos_clima_orodelti 
                    WHERE YEAR(fecha) = YEAR('{$filters->fecha_inicial}') $sGerente
                    GROUP BY fecha
                ) AS tbl")
        ];
        $response->tags->lluvia = [
            "value" => $this->db->queryOne("SELECT IFNULL(ROUND(SUM(horas_lluvia), 2), 0)
                                            FROM datos_clima_orodelti
                                            WHERE 1=1 $sWhere"),
            "acumulado" => $this->db->queryOne("SELECT ROUND(SUM(horas_lluvia), 2)
                                                FROM datos_clima_orodelti
                                                WHERE YEAR(fecha) = YEAR('{$filters->fecha_inicial}') $sGerente")
        ];
        return json_encode($response);
    }

    public function detalle(){
        $response = new stdClass;

        $filters = $this->params();
        $has = (int) $this->db->queryOne("SELECT COUNT(1) FROM datos_clima_orodelti WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' ");
        if($has == 0){
            return json_encode($response);
        }

        if($filters->fecha_inicial != '' && $filters->fecha_final != ''){
            $sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->hora_inicio != ''){
            $sWhere .= " AND hora >= '{$filters->hora_inicio}'";
        }
        if($filters->hora_fin != ''){
            $sWhere .= " AND hora <= '{$filters->hora_fin}'";
        }
        if($filters->estacion != ''){
            $sWhere .= " AND estacion_id = '{$filters->estacion}'";
        }

        $response->data = [
            [
                "detalle" => "TEMPERATURA MÍNIMA",
                "acumulado" => "",
                "avg" => $this->db->queryOne("SELECT ROUND(AVG(temp_minima), 2) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "max" => $this->db->queryOne("SELECT MAX(temp_minima) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "min" => $this->db->queryOne("SELECT MIN(temp_minima) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
            ],
            [
                "detalle" => "TEMPERATURA MAXIMA",
                "acumulado" => "",
                "avg" => $this->db->queryOne("SELECT ROUND(AVG(temp_maxima), 2) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "max" => $this->db->queryOne("SELECT MAX(temp_maxima) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "min" => $this->db->queryOne("SELECT MIN(temp_maxima) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
            ],
            [
                "detalle" => "LLUVIA (MM)",
                "acumulado" => $this->db->queryOne("SELECT SUM(lluvia) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "avg" => $this->db->queryOne("SELECT AVG(lluvia) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "max" => $this->db->queryOne("SELECT MAX(lluvia) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "min" => $this->db->queryOne("SELECT MIN(lluvia) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
            ],
            [
                "detalle" => "HUMEDAD RELATIVA",
                "acumulado" => "",
                "avg" => $this->db->queryOne("SELECT ROUND(AVG(humedad), 2) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "max" => $this->db->queryOne("SELECT MAX(humedad) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "min" => $this->db->queryOne("SELECT MIN(humedad) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
            ],
            [
                "detalle" => "RADIACIÓN SOLAR",
                "acumulado" => $this->db->queryOne("SELECT SUM(rad_solar) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "avg" => $this->db->queryOne("SELECT ROUND(AVG(rad_solar), 2) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "max" => $this->db->queryOne("SELECT MAX(rad_solar) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "min" => $this->db->queryOne("SELECT MIN(rad_solar) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
            ],
            [
                "detalle" => "HORAS LUZ",
                "acumulado" => $this->db->queryOne("SELECT ROUND(SUM(horas_luz), 2) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "avg" => $this->db->queryOne("SELECT ROUND(AVG(horas_luz), 2) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "max" => $this->db->queryOne("SELECT ROUND(MAX(horas_luz), 2) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
                "min" => $this->db->queryOne("SELECT ROUND(MIN(horas_luz), 2) FROM datos_clima_orodelti WHERE 1=1 $sWhere"),
            ]
        ];

        return json_encode($response);
    }

    public function horasLuz(){
        $response = new stdClass;

        $filters = $this->params();
        if($filters->estacion != ''){
            $sWhere .= " AND estacion_id = '{$filters->estacion}'";
        }

        $sql = "SELECT label, legend, cant AS 'value'
                FROM(
                    SELECT DATE_FORMAT(CONCAT(CURRENT_DATE,' ',hora_t), '%h %p') AS label,
                        (SELECT f.nombre FROM estaciones_haciendas INNER JOIN cat_haciendas f ON id_hacienda = f.id WHERE id_hacienda = estaciones.id_hacienda $sWhere) AS legend,
                        ROUND((SELECT AVG(rad_solar) FROM datos_clima_orodelti WHERE HOUR(hora) = estaciones.hora AND fecha BETWEEN '$filters->fecha_inicial' AND '$filters->fecha_final' AND id_hacienda = estaciones.id_hacienda), 2) AS cant
                    FROM (
                        SELECT estaciones.estacion_id, hora, id_hacienda, hora_t
                        FROM (
                            SELECT HOUR(hora) AS hora, hora AS hora_t
                            FROM datos_clima_orodelti
                            WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' $sWhere
                            GROUP BY HOUR(hora)
                        ) AS horas
                        JOIN (
                            SELECT estacion_id
                            FROM datos_clima_orodelti c
                            INNER JOIN cat_fincas f ON f.id = c.id_finca AND alias IS NOT NULL $sWhere
                            WHERE 1=1 
                            GROUP BY estacion_id
                        ) AS estaciones
                        INNER JOIN estaciones_haciendas e ON estaciones.estacion_id = e.estacion_id
                        GROUP BY hora, estaciones.estacion_id
                    ) AS estaciones
                    GROUP BY hora, estacion_id
                ) AS estaciones";

        $response->data = $this->generateSeries($sql);

        return json_encode($response);
    }

    public function graficasViento(){
        $response = new stdClass;
        $filters = $this->params();

        if($filters->fecha_inicial != '' && $filters->fecha_final != ''){
            $sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->hora_inicio != ''){
            $sWhere .= " AND hora >= '{$filters->hora_inicio}'";
        }
        if($filters->hora_fin != ''){
            $sWhere .= " AND hora <= '{$filters->hora_fin}'";
        }
        if($filters->estacion != ''){
            $sWhere .= " AND estacion_id = '{$filters->estacion}'";
        }
        
        $sql = "SELECT ROUND(AVG(velocidad_viento), 1) AS velocidad_viento
                FROM datos_clima_orodelti
                WHERE velocidad_viento IS NOT NULL  $sWhere";
        $response->velocidad_viento = $this->db->queryOne($sql);

        $sql = "SELECT direccion_viento AS id, COUNT(1) AS label
                FROM sigat.datos_clima_orodelti
                WHERE 1=1 AND direccion_viento IS NOT NULL $sWhere
                GROUP BY direccion_viento";
        $response->direccion_viento = $this->db->queryAllSpecial($sql);

        return json_encode($response);
    }

    private function generateSeries($sql){
		$res = $this->db->queryAll($sql);

		$response->data = [];
        $response->legend = [];
        
		$series = new stdClass;
		$markLine = new stdClass;
        $labels = [];
        
        $min = null;
        $max = null;
        $avg = null;
        $total = 0;
        $count = 0;

		foreach ($res as $key => $value) {
            if(!in_array($value->label, $response->legend)){
                $response->legend[] = $value->label;
            }
		}

        foreach ($res as $key => $value) {
            $value = (object)$value;
            $value->position = array_search($value->legend, $response->legend);
            $value->promedio = (float)$value->value;

            if(($min == null || $min > $value->value) && $value->value != null) $min = $value->value;
            if(($max == null || $max < $value->value) && $value->value != null) $max = $value->value;
            if($value->value){
                $total += (float) $value->value;
                $count++;
            }
			
			if(!isset($response->data[$value->legend]->itemStyle)){
				$series = new stdClass;
				$series->name = $value->legend;
				$series->type = "line";
				$series->connectNulls = true;
				$series->data[] = $value->value;
				$series->itemStyle = (object)[
					"normal" => [
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => true , "position" => "insideTop"]
					]
				];
				$response->data[$value->legend] = $series;
			}else{
				$response->data[$value->legend]->data[] = $value->value;
			}
		}

        $response->min = (float) $min;
        $response->max = (float) $max;
        $response->avg = ROUND($total / $count, 2);
        $response->total = $total;

        return $response;
	}
}

?>