<?php
class ConfigFincasOrodelti {
	
	private $db;
	private $session;
	
	public function __construct() {
        header('Content-Type: application/json');
        $this->db = new M_Conexion();
        $this->session = Session::getInstance();
        $this->postdata = (object)json_decode(file_get_contents("php://input"));
    }

    public function index(){
        $response = new stdClass;
        if($this->postdata->anio == date('Y')){
            $response->current_week = $this->db->queryOne("SELECT getWeek(CURRENT_DATE)");
        }else {
            $response->current_week = 52;
        }
        $response->semanas = [];
        for($x = 1; $x <= $response->current_week; $x++){
            $response->semanas[] = $x;
        }

        $response->fincas = $this->db->queryAll("SELECT id_finca, finca FROM ciclos_aplicacion_hist WHERE anio = {$this->postdata->anio} GROUP BY id_finca");
        $response->fumigacion = [];
        $response->neta = [];
        $response->produccion = [];
        $response->banano = [];

        foreach($response->fincas as $row){
            $fu = new stdClass;
            $neta = new stdClass;
            $prod = new stdClass;
            $ban = new stdClass;

            $fu->id_finca = $neta->id_finca = $prod->id_finca = $ban->id_finca = $row->id_finca;
            $fu->finca = $neta->finca = $prod->finca = $ban->finca = $row->finca;

            foreach($response->semanas as $sem){
                $fu->{"sem_{$sem}"} = (float) $this->db->queryOne("SELECT getHaFinca($row->id_finca, {$this->postdata->anio}, $sem, 'FUMIGACION')");
                $neta->{"sem_{$sem}"} = (float) $this->db->queryOne("SELECT getHaFinca($row->id_finca, {$this->postdata->anio}, $sem, 'NETA')");
                $prod->{"sem_{$sem}"} = (float) $this->db->queryOne("SELECT getHaFinca($row->id_finca, {$this->postdata->anio}, $sem, 'PRODUCCION')");
                $ban->{"sem_{$sem}"} = (float) $this->db->queryOne("SELECT getHaFinca($row->id_finca, {$this->postdata->anio}, $sem, 'BANANO')");
            }
            
            $response->fumigacion[] = $fu;
            $response->neta[] = $neta;
            $response->produccion[] = $prod;
            $response->banano[] = $ban;
        }

        /*foreach($response->neta as &$row){
            foreach($response->semanas as $sem){
                
            }
        }

        /*foreach($response->produccion as $row){
            foreach($response->semanas as $sem){
                
            }
        }

        foreach($response->banano as $row){
            foreach($response->semanas as $sem){
                
            }
        }*/
        return json_encode($response);
    }

    public function saveHa(){
        $response = new stdClass;
        $response->status = 400;
        $filters = $this->postdata;

        if($filters->id_finca > 0 && $filters->semana > 0 && $filters->tipo != ''){
            $e = (int) $this->db->queryOne("SELECT COUNT(1) FROM fincas_ha_semana WHERE id_finca = $filters->id_finca AND anio = $filters->anio AND semana = $filters->semana AND tipo = '{$filters->tipo}'");
            if($e > 0){
                $sql = "UPDATE fincas_ha_semana SET
                            hectareas = $filters->hectareas
                        WHERE anio = $filters->anio AND semana = $filters->semana AND id_finca = $filters->id_finca AND tipo = '{$filters->tipo}'";
                $this->db->query($sql);
            }else{
                $sql = "INSERT INTO fincas_ha_semana SET
                            id_finca = $filters->id_finca,
                            hectareas = $filters->hectareas,
                            anio = $filters->anio,
                            semana = $filters->semana,
                            tipo = '{$filters->tipo}'";
                $this->db->query($sql);
            }
            $response->status = 200;
        }

        return json_encode($response);
    }
}

?>