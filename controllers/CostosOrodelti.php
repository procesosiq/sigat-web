<?php

class CostosOrodelti {
	
	private $db;
	private $session;
	
	public function __construct(){
		$this->db = new M_Conexion();
		$this->session = Session::getInstance();
	}

	public function productos(){
		$response = new stdClass;
		$response->proveedores = $this->db->queryAll("SELECT * FROM cat_proveedores WHERE status = 1");
		$response->productos = $this->db->queryAll("SELECT * FROM products WHERE status = 'Activo' ORDER BY nombre_comercial");
		foreach($response->productos as $producto){
			$producto->dosis_array = [];
			if($producto->dosis > 0){
				$producto->dosis_array[] = (float)$producto->dosis;
			}
			if($producto->dosis_2 > 0){
				$producto->dosis_array[] = (float)$producto->dosis_2;
			}
			if($producto->dosis_3 > 0){
				$producto->dosis_array[] = (float)$producto->dosis_3;
			}
			if($producto->dosis_4 > 0){
				$producto->dosis_array[] = (float)$producto->dosis_4;
			}
			if($producto->dosis_5 > 0){
				$producto->dosis_array[] = (float)$producto->dosis_5;
			}
			if($producto->dosis_6 > 0){
				$producto->dosis_array[] = (float)$producto->dosis_6;
			}
			if($producto->dosis_7 > 0){
				$producto->dosis_array[] = (float)$producto->dosis_7;
			}
			if($producto->dosis_8 > 0){
				$producto->dosis_array[] = (float)$producto->dosis_8;
			}
			if($producto->dosis_9 > 0){
				$producto->dosis_array[] = (float)$producto->dosis_9;
			}
			if($producto->dosis_10 > 0){
				$producto->dosis_array[] = (float)$producto->dosis_10;
			}
		}
		return json_encode($response);
	}

	public function index(){
		$response = new stdClass;
		$response->sigatoka = $this->sigatoka();
		$response->parcial = $this->parcial();
		$response->foliar = $this->foliar();
		$response->plagas = $this->plagas();
		return json_encode($response);
	}

	private function sigatoka(){
		$params = (object)json_decode(file_get_contents("php://input"));

		$sql = "SELECT *,
					getHaFinca(tbl.id_finca, {$params->params->year}, semana, '{$params->params->tipoHectarea}') ha_total_sem
				FROM (
					SELECT 
						GROUP_CONCAT(DISTINCT hist.id SEPARATOR ',') as ids,
						num_ciclo,
						GROUP_CONCAT(DISTINCT programa SEPARATOR '-') programa,
						getWeek(hist.fecha_real) semana,
						fecha_prog,
						fecha_real,
						SUM(hectareas_fumigacion) hectareas_fumigacion,
						atraso,
						motivo,
						SUM(DISTINCT ha_oper) ha_oper,
						SUM(DISTINCT oper) oper,
						SUM(costo_total) costo_total,
						dosis_agua,
						COUNT(DISTINCT programa) programas,
						id_finca,
                        IF(fecha_aprobacion IS NULL, TRUE, FALSE) need_aprovee
					FROM `ciclos_aplicacion_hist` hist
					INNER JOIN ciclos_aplicacion_hist_unidos unidos ON id_ciclo_aplicacion = hist.id
					WHERE anio = {$params->params->year} AND finca = '{$params->params->finca}' AND (programa = 'Sigatoka' OR programa = 'Plagas' OR programa = 'Erwinia') AND tipo_ciclo = 'CICLO'
					GROUP BY unidos._join
					ORDER BY fecha_real
				) tbl";
		$data = $this->db->queryAll($sql);

		foreach($data as $row){
            $sql =  "SELECT * FROM ciclos_aplicacion_hist_tmp WHERE ids = '{$row->ids}'";
            $row->por_aprobar = $this->db->queryRow($sql);

            $sql = "SELECT GROUP_CONCAT(DISTINCT programa SEPARATOR '+') 
                    FROM ciclos_aplicacion_hist
                    WHERE anio = {$params->params->year} AND finca = '{$params->params->finca}' AND tipo_ciclo = 'CICLO' AND num_ciclo = '{$row->num_ciclo}'";
            $row->programa_display = $this->db->queryOne($sql);

            $row->por_borrar = (int) $this->db->queryOne("SELECT COUNT(1) FROM ciclos_aplicacion_hist_tmp_borrar WHERE ids = '{$row->ids}'");
            $row->por_borrar = $row->por_borrar > 0;

			$row->costo_total = (float) $row->costo_total;
            
            $sql = "SELECT id_producto, prod.id_proveedor, prod.nombre_comercial, p.precio, SUM(p.cantidad) cantidad, p.dosis, SUM(p.total) total, prov.nombre as proveedor
					FROM `ciclos_aplicacion_hist` h
					INNER JOIN ciclos_aplicacion_hist_productos p ON p.`id_ciclo_aplicacion` = h.id
					INNER JOIN products prod ON p.id_producto = prod.id
					LEFT JOIN cat_tipo_productos tipos ON prod.id_tipo_producto = tipos.id
					LEFT JOIN cat_proveedores prov ON prod.id_proveedor = prov.id
					WHERE h.anio = {$params->params->year} AND h.finca = '{$params->params->finca}' 
                        AND (h.programa = 'Sigatoka' OR h.programa = 'Plagas' OR programa = 'Erwinia') 
						AND tipo_ciclo = 'CICLO' 
						AND h.id IN ({$row->ids})
					GROUP BY prod.id
					ORDER BY tipos.orden, p.id";
            $row->productos = $this->db->queryAll($sql);
            
            $sql = "SELECT id_producto, prod.id_proveedor, prod.nombre_comercial, p.precio, SUM(p.cantidad) cantidad, p.dosis, SUM(p.total) total, prov.nombre as proveedor
					FROM `ciclos_aplicacion_hist_productos_tmp` p
					INNER JOIN products prod ON p.id_producto = prod.id
					LEFT JOIN cat_tipo_productos tipos ON prod.id_tipo_producto = tipos.id
					LEFT JOIN cat_proveedores prov ON prod.id_proveedor = prov.id
					WHERE p.ids = '{$row->ids}'
                    GROUP BY p.id_producto
					ORDER BY tipos.orden, p.id";
            $row->productos_tmp = $this->db->queryAll($sql);
		}

		return $data;
	}

	private function parcial(){
		$params = (object)json_decode(file_get_contents("php://input"));

		$sql = "SELECT *,
					getHaFinca(tbl.id_finca, {$params->params->year}, semana, '{$params->params->tipoHectarea}') ha_total_sem
				FROM (
					SELECT 
						GROUP_CONCAT(DISTINCT hist.id SEPARATOR ',') as ids,
						num_ciclo,
						sum_ciclo,
						GROUP_CONCAT(DISTINCT programa SEPARATOR '-') programa,
						getWeek(hist.fecha_real) semana,
						fecha_prog,
						fecha_real,
						SUM(hectareas_fumigacion) hectareas_fumigacion,
						atraso,
						motivo,
						SUM(DISTINCT ha_oper) ha_oper,
						SUM(DISTINCT oper) oper,
						SUM(costo_total) costo_total,
						dosis_agua,
						COUNT(DISTINCT programa) programas,
						id_finca,
                        IF(fecha_aprobacion IS NULL, TRUE, FALSE) need_aprovee
					FROM `ciclos_aplicacion_hist` hist
					WHERE anio = {$params->params->year} AND finca = '{$params->params->finca}' AND tipo_ciclo = 'Parcial'
					GROUP BY hist.id
					ORDER BY fecha_real
				) tbl";

		$data = $this->db->queryAll($sql);

		foreach($data as $row){
            $sql =  "SELECT * FROM ciclos_aplicacion_hist_tmp WHERE ids = '{$row->ids}'";
            $row->por_aprobar = $this->db->queryRow($sql);

            $row->por_borrar = (int) $this->db->queryOne("SELECT COUNT(1) FROM ciclos_aplicacion_hist_tmp_borrar WHERE ids = '{$row->ids}'");
            $row->por_borrar = $row->por_borrar > 0;

            $row->costo_total = (float) $row->costo_total;
            
			$row->costo_total = (float) $row->costo_total;
			$sql = "SELECT id_producto, prod.id_proveedor, prod.nombre_comercial, p.precio, SUM(p.cantidad) cantidad, p.dosis, SUM(p.total) total, prov.nombre as proveedor
					FROM `ciclos_aplicacion_hist` h
					INNER JOIN ciclos_aplicacion_hist_productos p ON p.`id_ciclo_aplicacion` = h.id
					INNER JOIN products prod ON p.id_producto = prod.id
					LEFT JOIN cat_tipo_productos tipos ON prod.id_tipo_producto = tipos.id
					LEFT JOIN cat_proveedores prov ON prod.id_proveedor = prov.id
					WHERE h.anio = {$params->params->year} AND h.finca = '{$params->params->finca}'
						AND tipo_ciclo = 'Parcial' 
						AND h.id IN ({$row->ids})
					GROUP BY prod.id
					ORDER BY tipos.orden, p.id";
			$row->productos = $this->db->queryAll($sql);
            
            $sql = "SELECT id_producto, prod.id_proveedor, prod.nombre_comercial, p.precio, SUM(p.cantidad) cantidad, p.dosis, SUM(p.total) total, prov.nombre as proveedor
					FROM `ciclos_aplicacion_hist_productos_tmp` p
					INNER JOIN products prod ON p.id_producto = prod.id
					LEFT JOIN cat_tipo_productos tipos ON prod.id_tipo_producto = tipos.id
					LEFT JOIN cat_proveedores prov ON prod.id_proveedor = prov.id
					WHERE p.ids = '{$row->ids}'
                    GROUP BY p.id_producto
					ORDER BY tipos.orden, p.id";
            $row->productos_tmp = $this->db->queryAll($sql);
		}

		return $data;
	}

	private function foliar(){
		$params = (object)json_decode(file_get_contents("php://input"));

		$sql = "SELECT *,
					getHaFinca(tbl.id_finca, {$params->params->year}, semana, '{$params->params->tipoHectarea}') ha_total_sem
				FROM (
					SELECT 
						id_finca,
						GROUP_CONCAT(DISTINCT hist.id SEPARATOR ',') as ids,
						num_ciclo,
						num_ciclo_programa ciclo_programa,
						programa,
						getWeek(hist.fecha_real) semana,
						fecha_real,
						fecha_prog,
						CONCAT(DAY(fecha_prog),'-',getMonthName(fecha_prog)) fecha_prog_c,
						CONCAT(DAY(fecha_real),'-',getMonthName(fecha_real)) fecha_real_c,
						SUM(hectareas_fumigacion) hectareas_fumigacion,
						atraso,
						motivo,
						ha_oper,
						SUM(oper) oper,
						SUM(costo_total) costo_total,
						dosis_agua,
                        IF(fecha_aprobacion IS NULL, TRUE, FALSE) need_aprovee
					FROM `ciclos_aplicacion_hist` hist
					INNER JOIN ciclos_aplicacion_hist_unidos unidos ON id_ciclo_aplicacion = hist.id
					WHERE anio = {$params->params->year} AND finca = '{$params->params->finca}' AND tipo_ciclo = 'CICLO' AND programa = 'Foliar'
					GROUP BY unidos._join
					ORDER BY fecha_real
				) tbl";
		$data = $this->db->queryAll($sql);

		foreach($data as $row){
            $sql =  "SELECT * FROM ciclos_aplicacion_hist_tmp WHERE ids = '{$row->ids}'";
            $row->por_aprobar = $this->db->queryRow($sql);

            $sql = "SELECT GROUP_CONCAT(DISTINCT programa SEPARATOR '+') 
                    FROM ciclos_aplicacion_hist
                    WHERE anio = {$params->params->year} AND finca = '{$params->params->finca}' AND tipo_ciclo = 'CICLO' AND num_ciclo = '{$row->num_ciclo}'";
            $row->programa_display = $this->db->queryOne($sql);

            $row->por_borrar = (int) $this->db->queryOne("SELECT COUNT(1) FROM ciclos_aplicacion_hist_tmp_borrar WHERE ids = '{$row->ids}'");
            $row->por_borrar = $row->por_borrar > 0;

			$row->costo_total = (float) $row->costo_total;

			$sql = "SELECT prod.nombre_comercial, p.precio, p.cantidad, p.dosis, SUM(p.total) total
					FROM `ciclos_aplicacion_hist` h
					INNER JOIN ciclos_aplicacion_hist_productos p ON p.`id_ciclo_aplicacion` = h.id
					INNER JOIN products prod ON p.id_producto = prod.id
					LEFT JOIN cat_tipo_productos tipos ON prod.id_tipo_producto = tipos.id
					WHERE h.anio = {$params->params->year} AND h.finca = '{$params->params->finca}' AND h.programa = 'Foliar' AND tipo_ciclo = 'CICLO' AND h.id IN ('{$row->ids}')
					GROUP BY prod.id
					ORDER BY tipos.orden, p.id";
			$row->productos = $this->db->queryAll($sql);
            $sql = "SELECT id_producto, prod.id_proveedor, prod.nombre_comercial, p.precio, SUM(p.cantidad) cantidad, p.dosis, SUM(p.total) total, prov.nombre as proveedor
					FROM `ciclos_aplicacion_hist_productos_tmp` p
					INNER JOIN products prod ON p.id_producto = prod.id
					LEFT JOIN cat_tipo_productos tipos ON prod.id_tipo_producto = tipos.id
					LEFT JOIN cat_proveedores prov ON prod.id_proveedor = prov.id
					WHERE p.ids = '{$row->ids}'
                    GROUP BY p.id_producto
					ORDER BY tipos.orden, p.id";
            $row->productos_tmp = $this->db->queryAll($sql);
		}

		return $data;
	}

	private function plagas(){
		$params = (object)json_decode(file_get_contents("php://input"));

		$sql = "SELECT 
					num_ciclo ciclo,
					programa,
					semana,
					CONCAT(DAY(fecha_prog),'-',getMonthName(fecha_prog)) fecha_prog,
					CONCAT(DAY(fecha_real),'-',getMonthName(fecha_real)) fecha_real,
					SUM(hectareas_fumigacion) hectareas_fumigacion,
					atraso,
					motivo,
					ha_oper,
					SUM(oper) oper,
					SUM(costo_total) costo_total,
					dosis_agua
				FROM `ciclos_aplicacion_hist`
				WHERE anio = {$params->params->year} AND finca = '{$params->params->finca}' AND tipo_ciclo = 'CICLO' AND programa = 'Plagas'
				GROUP BY num_ciclo
				ORDER BY fecha_real";
		$data = $this->db->queryAll($sql);

		foreach($data as $row){
			$sql = "SELECT prod.nombre_comercial, p.precio, p.cantidad, p.dosis, SUM(p.total) total
					FROM `ciclos_aplicacion_hist` h
					INNER JOIN ciclos_aplicacion_hist_productos p ON p.`id_ciclo_aplicacion` = h.id
					INNER JOIN products prod ON p.id_producto = prod.id
					WHERE h.anio = {$params->params->year} AND h.finca = '{$params->params->finca}' AND h.programa = 'Plagas' AND tipo_ciclo = 'CICLO' AND h.num_ciclo = '{$row->ciclo}'
					GROUP BY prod.id
					ORDER BY p.id";
			$row->productos = $this->db->queryAll($sql);
		}

		return $data;
	}

	// con esta funcion cambiar la agrupacion
	public function updateProductoTemporal(){
		$response = new stdClass;
		$response->status = 200;
		$params = (object)json_decode(file_get_contents("php://input"));

		$ciclo = $params->params->ciclo;
		$producto_viejo = $params->params->productoViejo;
		$producto_nuevo = $params->params->productoNuevo;

		if($ciclo->ids != ''){
			$this->generarCambiosTemporales($ciclo->ids);

            $hectareas_fumigacion = $this->getHectareasFumigacion($ciclo->ids);
            $fecha_real = $ciclo->por_aprobar->fecha_real ? $ciclo->por_aprobar->fecha_real : $ciclo->fecha_real;
            $sql = "UPDATE ciclos_aplicacion_hist_productos_tmp
                    SET 
                        id_producto = {$producto_nuevo->id_producto},
                        id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = {$producto_nuevo->id_producto}),
                        dosis = '{$producto_nuevo->dosis}',
                        id_proveedor = {$producto_nuevo->id_proveedor},
                        producto = (SELECT nombre_comercial FROM products WHERE id = {$producto_nuevo->id_producto}),
                        proveedor = (SELECT nombre FROM cat_proveedores WHERE id = {$producto_nuevo->id_proveedor}),
                        cantidad = {$hectareas_fumigacion}*{$producto_nuevo->dosis},
                        precio = getPrecioProducto({$producto_nuevo->id_producto}, '{$fecha_real}'),
                        total = {$hectareas_fumigacion}*{$producto_nuevo->dosis}*getPrecioProducto({$producto_nuevo->id_producto}, '{$fecha_real}')
                    WHERE ids = '{$ciclo->ids}' AND id_producto = {$producto_viejo->id_producto}";
            
            $this->db->query($sql);

            $sql = "UPDATE ciclos_aplicacion_hist SET
                        fecha_aprobacion = NULL
                    WHERE id IN ({$ciclo->ids})";
            $this->db->query($sql);
        }

		return json_encode($response);
	}

	// con esta funcion cambiar la agrupacion
	public function addProductoTemporal(){
		$response = new stdClass;
		$response->status = 200;
		$params = (object)json_decode(file_get_contents("php://input"));

		$ciclo = $params->params->ciclo;
		$producto_nuevo = $params->params->producto;

		if($ciclo->ids != ''){
			$this->generarCambiosTemporales($ciclo->ids);
			
            $ids = explode(",", $ciclo->ids);
            $hectareas_fumigacion = $this->getHectareasFumigacion($ciclo->ids);
            $fecha_real = $ciclo->por_aprobar->fecha_real ? $ciclo->por_aprobar->fecha_real : $ciclo->fecha_real;

			foreach($ids as $id_ciclo){
				$sql = "INSERT INTO ciclos_aplicacion_hist_productos_tmp
						SET 
							id_ciclo_aplicacion = $id_ciclo,
                            ids = '{$ciclo->ids}',
							id_producto = {$producto_nuevo->id_producto},
							id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = {$producto_nuevo->id_producto}),
							dosis = '{$producto_nuevo->dosis}',
							id_proveedor = {$producto_nuevo->id_proveedor},
							producto = (SELECT nombre_comercial FROM products WHERE id = {$producto_nuevo->id_producto}),
							proveedor = (SELECT nombre FROM cat_proveedores WHERE id = {$producto_nuevo->id_proveedor}),
							cantidad = {$hectareas_fumigacion}*{$producto_nuevo->dosis},
							precio = getPrecioProducto({$producto_nuevo->id_producto}, '{$fecha_real}'),
							total = {$hectareas_fumigacion} * {$producto_nuevo->dosis} * getPrecioProducto({$producto_nuevo->id_producto}, '{$fecha_real}')";
				$this->db->query($sql);
            }
            
            $sql = "UPDATE ciclos_aplicacion_hist SET
                        fecha_aprobacion = NULL
                    WHERE id IN ({$ciclo->ids})";
            $this->db->query($sql);
        }
        
		return json_encode($response);
	}

	public function getPrecioProducto(){
		$response = new stdClass;
		$params = (object)json_decode(file_get_contents("php://input"));

		if($params->params->id_producto){
			$response->precio = (float) $this->db->queryOne("SELECT getPrecioProducto({$params->params->id_producto},'{$params->params->fecha}')");
		}else{
			$response->precio = 0;
		}
		
		return json_encode($response);
	}

	// con esta funcion cambiar la agrupacion
	public function updateCicloTemporal(){
		$response = new stdClass;
		$params = (object)json_decode(file_get_contents("php://input"));
		$params = $params->params;
		$response->status = 200;

        $sql_add = "";
        if($params->editing == 'ha_oper'){
            $sql_add .= ", oper = hectareas_fumigacion * {$params->por_aprobar->{$params->editing}}";
        }

        if($params->editing == 'hectareas_fumigacion'){
            $sql_add .= ", oper = {$params->por_aprobar->{$params->editing}} * ha_oper";
        }
        
        $this->generarCambiosTemporales($params->ids);

        $sql = "UPDATE ciclos_aplicacion_hist_tmp SET
                    {$params->editing} = '{$params->por_aprobar->{$params->editing}}'
                    $sql_add
                WHERE ids = '{$params->ids}'";
        $this->db->query($sql);

        $sql = "UPDATE ciclos_aplicacion_hist SET
                    fecha_aprobacion = NULL
                WHERE id IN ({$params->ids})";
        $this->db->query($sql);

		return json_encode($response);
	}

	public function procesar(){
		$sql = "SELECT finca, num_ciclo, 
					GROUP_CONCAT(DISTINCT id SEPARATOR ',') ids,
					GROUP_CONCAT(DISTINCT programa SEPARATOR ',') programa
				FROM ciclos_aplicacion_hist hist
				WHERE tipo_ciclo = 'CICLO' AND NOT EXISTS (SELECT * FROM ciclos_aplicacion_hist_unidos WHERE id_ciclo_aplicacion = hist.id)
				GROUP BY anio, id_finca, num_ciclo, fecha_prog, ha_oper";
		$data = $this->db->queryAll($sql);
		
		foreach($data as $row){
			$this->insertJoin($row);
		}
	}

	private function insertJoin($row){
		$ids = explode(",", $row->ids);
		$joins = [];
		for($x = 0; $x < count($ids); $x++){
			if($x == 0){
				$joins[] = [$ids[$x]];
			}else{
				$e = false;
				$p1 = $this->db->queryAll("SELECT * FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = {$ids[$x]} ORDER BY id_producto");
				for($y = 0; $y < count($joins); $y++){
					$p2 = $this->db->queryAll("SELECT * FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = {$joins[$y][0]} ORDER BY id_producto");
					if($this->compareProducts($p1, $p2)){
						$joins[$y][] = $ids[$x];
						$e = true;
						break;
					}
				}

				if(!$e){
					$joins[] = [$ids[$x]];
				}
			}
		}

		foreach($joins as $j){
			$join = $this->db->queryOne("SELECT MAX(_join)+1 FROM ciclos_aplicacion_hist_unidos");
			if(!$join) $join = 1;
			foreach($j as $id){
				$sql = "INSERT INTO ciclos_aplicacion_hist_unidos SET
							_join = $join,
							id_ciclo_aplicacion = '{$id}'";
				$this->db->query($sql);
			}
		}
	}

	private function compareProducts($p1, $p2){
		if(count($p1) != count($p2)) return false;

		// true si coinciden
		$e = true;
		for($x = 0; $x < count($p1); $x++){
			if(
				($p1[$x]->id_producto != $p2[$x]->id_producto)
				||
				($p1[$x]->id_producto == $p2[$x]->id_producto && ($p1[$x]->precio != $p2[$x]->precio || $p1[$x]->dosis != $p2[$x]->dosis))
			){
				$e = false;
				break;
			}
		}

		return $e;
	}

	public function agregarSigatoka(){
		$response = new stdClass;
		$response->status = 200;
		$params = (object)json_decode(file_get_contents("php://input"));
		$params = $params->params;

		$sql = "INSERT INTO ciclos_aplicacion_hist SET
					anio = {$params->anio},
					finca = '{$params->finca}',
					id_finca = (SELECT id FROM cat_fincas WHERE nombre = '{$params->finca}' AND status = 1),
					id_encargado = (SELECT id_encargado FROM cat_fincas WHERE nombre = '{$params->finca}' AND status = 1),
					programa = 'Sigatoka',
					tipo_ciclo = 'Ciclo',
					id_programa = 1,
					sum_ciclo = 1,
					oper = 0,
					ha_oper = 0";
		$r = $this->db->query($sql);
		$id = $this->db->getLastID();

		$sql = "SELECT MAX(_join) FROM ciclos_aplicacion_hist_unidos";
		$_join = (int) $this->db->queryOne($sql);
		$_join = $_join + 1;

		$sql = "INSERT INTO ciclos_aplicacion_hist_unidos SET
					id_ciclo_aplicacion = {$id},
					_join = {$_join}";
		$this->db->query($sql);
		
		return json_encode($response);
	}

	public function agregarFoliar(){
		$response = new stdClass;
		$response->status = 200;
		$params = (object)json_decode(file_get_contents("php://input"));
		$params = $params->params;

		$sql = "INSERT INTO ciclos_aplicacion_hist SET
					anio = {$params->anio},
					finca = '{$params->finca}',
					id_finca = (SELECT id FROM cat_fincas WHERE nombre = '{$params->finca}' AND status = 1),
					id_encargado = (SELECT id_encargado FROM cat_fincas WHERE nombre = '{$params->finca}' AND status = 1),
					programa = 'Foliar',
					tipo_ciclo = 'Ciclo',
					id_programa = 3,
					sum_ciclo = 1,
					oper = 0,
					ha_oper = 0";
		$r = $this->db->query($sql);
		$id = $this->db->getLastID();

		$sql = "SELECT MAX(_join) FROM ciclos_aplicacion_hist_unidos";
		$_join = (int) $this->db->queryOne($sql);
		$_join = $_join + 1;

		$sql = "INSERT INTO ciclos_aplicacion_hist_unidos SET
					id_ciclo_aplicacion = {$id},
					_join = {$_join}";
		$this->db->query($sql);
		
		return json_encode($response);
	}

	public function agregarParcial(){
		$response = new stdClass;
		$response->status = 200;
		$params = (object)json_decode(file_get_contents("php://input"));
		$params = $params->params;

		$sql = "INSERT INTO ciclos_aplicacion_hist SET
					anio = {$params->anio},
					finca = '{$params->finca}',
					id_finca = (SELECT id FROM cat_fincas WHERE nombre = '{$params->finca}' AND status = 1),
					id_encargado = (SELECT id_encargado FROM cat_fincas WHERE nombre = '{$params->finca}' AND status = 1),
					programa = 'Sigatoka',
					tipo_ciclo = 'Parcial',
					id_programa = 1,
					sum_ciclo = 1,
					oper = 0,
					ha_oper = 0";

		$r = $this->db->query($sql);

		$id = $this->db->getLastID();

		$response->id = $id;
		
		return json_encode($response);
	}

	public function deleteRowConfirmar($ids){
		$sql = "DELETE FROM ciclos_aplicacion_hist WHERE id IN ({$ids})";
        $this->db->query($sql);
        
        $sql = "DELETE FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion IN({$ids})";
        $this->db->query($sql);
    }

    public function deleteRowTemporal(){
		$response = new stdClass;
		$response->status = 200;
		$params = (object)json_decode(file_get_contents("php://input"));
		$params = $params->params;

        $sql = "SELECT COUNT(1) e FROM ciclos_aplicacion_hist_tmp_borrar WHERE ids = '{$params->ids}'";
        $e = (int) $this->db->queryOne($sql);
        if(!$e){
            $sql = "SELECT COUNT(1) FROM ciclos_aplicacion_hist WHERE num_ciclo IS NULL AND fecha_aprobacion IS NULL AND fecha_real IS NULL AND id IN ({$params->ids})";
            $ee = (int) $this->db->queryOne($sql);
            if($ee > 0){
                $this->deleteRowConfirmar($params->ids);
            }else{
                $sql = "INSERT INTO ciclos_aplicacion_hist_tmp_borrar SET ids = '{$params->ids}'";
                $this->db->query($sql);
            }
        }

        $sql = "UPDATE ciclos_aplicacion_hist SET fecha_aprobacion = NULL WHERE id IN ({$params->ids})";
        $this->db->query($sql);

		return json_encode($response);
    }
    
    public function aproveeRow(){
        $response = new stdClass;
		$response->status = 200;
		$params = (object)json_decode(file_get_contents("php://input"));
        $params = $params->params;
        
        $delete = (int) $this->db->queryOne("SELECT COUNT(1) FROM ciclos_aplicacion_hist_tmp_borrar WHERE ids = '{$params->ids}'");
        if($delete > 0){
            $this->deleteRowConfirmar($params->ids);
            return json_encode($response);
        }

		$sql = "SELECT * FROM ciclos_aplicacion_hist_tmp WHERE ids = '{$params->ids}'";
        $changes = $this->db->queryRow($sql);

        $sql = "SELECT * FROM ciclos_aplicacion_hist_productos_tmp WHERE ids = '{$params->ids}'";
        $productos = $this->db->queryAll($sql);

        $this->db->query("DELETE FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion IN ({$params->ids})");

        $invalid_keys = ['id', 'ids', 'created_at'];
        $comodin = ['atraso'];
        foreach($changes as $key => $value){
            if(!in_array($key, $invalid_keys)){
                if($value || in_array($key, $comodin)){
                    $sql_add = '';
                    if($key == 'fecha_real'){
                        $sql_add .= ", semana = getWeek('{$value}')";
                        $sql_add .= ", anio = YEAR('{$value}')";
                    }

                    if($key == 'ha_oper'){
                        $sql_add .= ", oper = hectareas_fumigacion * {$value}";
                    }

                    if($key == 'hectareas_fumigacion'){
                        $sql_add .= ", oper = {$value} * ha_oper";
                    }

                    if($key == 'num_ciclo'){
                        $sql_add .= ", num_ciclo_programa = '{$value}'";
                    }

                    $sql = "UPDATE ciclos_aplicacion_hist SET
                                {$key} = '{$value}'
                                $sql_add
                            WHERE id IN ({$params->ids})";
                    $this->db->query($sql);
                }
            }
        }

        foreach($productos as $prod){
            $sql = "INSERT INTO ciclos_aplicacion_hist_productos (id_ciclo_aplicacion, id_tipo_producto, id_proveedor, id_producto, producto, cantidad, precio, dosis, total)
                    SELECT 
                        h.id,
                        products.id_tipo_producto,
                        products.id_proveedor,
                        products.id,
                        products.nombre_comercial,
                        h.hectareas_fumigacion * {$prod->dosis},
                        {$prod->precio},
                        {$prod->dosis},
                        h.hectareas_fumigacion * {$prod->dosis} * {$prod->precio}
                    FROM ciclos_aplicacion_hist h
                    INNER JOIN products on products.id = {$prod->id_producto}
                    WHERE h.id IN ({$params->ids})";
            $this->db->query($sql);
        }

        $sql = "UPDATE ciclos_aplicacion_hist h SET
                    oper = ha_oper * hectareas_fumigacion,
                    costo_total = IFNULL(ha_oper * hectareas_fumigacion, 0) + IFNULL((SELECT SUM(total) FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = h.id), 0),
                    fecha_aprobacion = CURRENT_TIMESTAMP
                WHERE id IN ({$params->ids})";
        $this->db->query($sql);

        $this->db->query("DELETE FROM ciclos_aplicacion_hist_tmp WHERE ids = '{$params->ids}'");
        $this->db->query("DELETE FROM ciclos_aplicacion_hist_productos_tmp WHERE ids = '{$params->ids}'");

		return json_encode($response);
    }

    private function generarCambiosTemporales($ids){
        $sql = "SELECT COUNT(1) FROM ciclos_aplicacion_hist_tmp WHERE ids = '{$ids}'";
        $e = (int) $this->db->queryOne($sql);
        if(!$e > 0){
            $sql = "INSERT INTO ciclos_aplicacion_hist_tmp SET ids = '{$ids}'";
            $this->db->query($sql);

            $sql = "SELECT id_producto, id_proveedor, dosis, precio, cantidad, total
                    FROM ciclos_aplicacion_hist h
                    INNER JOIN ciclos_aplicacion_hist_productos p ON h.id = p.id_ciclo_aplicacion
                    WHERE h.id IN ({$ids})
                    GROUP BY id_producto
                    ORDER BY p.id";
            $productos = $this->db->queryAll($sql);
            $hectareas_fumigacion = (float) $this->getHectareasFumigacion($ids);

            foreach($productos as $row){
                $sql = "INSERT INTO ciclos_aplicacion_hist_productos_tmp SET
                            ids = '{$ids}',
                            id_producto = {$row->id_producto},
                            id_proveedor = {$row->id_proveedor},
                            dosis = {$row->dosis},
                            cantidad = {$row->cantidad},
                            precio = {$row->precio},
                            total = {$row->total}";
                $this->db->query($sql);
            }
        }
    }

    private function getHectareasFumigacion($ids){
        $sql = "SELECT IFNULL(
                    (SELECT hectareas_fumigacion FROM ciclos_aplicacion_hist_tmp WHERE ids = '{$ids}'),
                    (SELECT hectareas_fumigacion FROM ciclos_aplicacion_hist WHERE id IN ({$ids}) LIMIT 1)
                )";
        return (float) $this->db->queryOne($sql);
    }
}
?>
