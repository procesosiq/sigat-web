<?php
	class CostosOrodelti {
		
		private $db;
		private $session;
		
		public function __construct()
		{
			$this->db = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		public function index(){
			$response = new stdClass;
			$response->sigatoka = $this->sigatoka();
			$response->parcial = $this->parcial();
			$response->foliar = $this->foliar();
			$response->plagas = $this->plagas();
			return json_encode($response);
		}

		private function sigatoka(){
			$params = (object)json_decode(file_get_contents("php://input"));

			$sql = "SELECT *
					FROM (
						SELECT 
							num_ciclo ciclo,
							GROUP_CONCAT(DISTINCT programa SEPARATOR '-') programa,
							semana,
							CONCAT(DAY(fecha_prog),'-',getMonthName(fecha_prog)) fecha_prog,
							CONCAT(DAY(fecha_real),'-',getMonthName(fecha_real)) fecha_real,
							SUM(DISTINCT hectareas_fumigacion) hectareas_fumigacion,
							atraso,
							motivo,
							SUM(DISTINCT ha_oper) ha_oper,
							SUM(DISTINCT oper) oper,
							SUM(costo_total) costo_total,
							dosis_agua,
							COUNT(DISTINCT programa) programas,
							id_finca
						FROM `ciclos_aplicacion_hist`
						WHERE anio = 2018 AND finca = '{$params->params->finca}' AND (programa = 'Sigatoka' OR programa = 'Plagas') AND tipo_ciclo = 'CICLO'
						GROUP BY num_ciclo
						ORDER BY num_ciclo+0
					) tbl";
			$data = $this->db->queryAll($sql);

			foreach($data as $row){
				$row->costo_total = (float) $row->costo_total;
				$sql = "SELECT prod.nombre_comercial, p.precio, SUM(p.cantidad) cantidad, p.dosis, SUM(p.total) total
						FROM `ciclos_aplicacion_hist` h
						INNER JOIN ciclos_aplicacion_hist_productos p ON p.`id_ciclo_aplicacion` = h.id
						INNER JOIN products prod ON p.id_producto = prod.id
						LEFT JOIN cat_tipo_productos tipos ON prod.id_tipo_producto = tipos.id
						WHERE h.anio = 2018 AND h.finca = '{$params->params->finca}' AND (h.programa = 'Sigatoka' OR h.programa = 'Plagas') AND tipo_ciclo = 'CICLO' AND h.num_ciclo = '{$row->ciclo}'
						GROUP BY prod.id
						ORDER BY tipos.orden, p.id";
				$row->productos = $this->db->queryAll($sql);

				$row->productos[] = [
					"nombre_comercial" => 'AGUA',
					"dosis" => $row->dosis_agua,
					"cantidad" => round($row->dosis_agua * $row->hectareas_fumigacion, 2)
				];
			}

			return $data;
		}

		private function parcial(){
			$params = (object)json_decode(file_get_contents("php://input"));

			$sql = "SELECT 
						num_ciclo ciclo,
						programa,
						semana,
						CONCAT(DAY(fecha_prog),'-',getMonthName(fecha_prog)) fecha_prog,
						CONCAT(DAY(fecha_real),'-',getMonthName(fecha_real)) fecha_real,
						SUM(hectareas_fumigacion) hectareas_fumigacion,
						atraso,
						motivo,
						ha_oper,
						SUM(oper) oper,
						SUM(costo_total) costo_total,
						dosis_agua
					FROM `ciclos_aplicacion_hist`
					WHERE anio = 2018 AND finca = '{$params->params->finca}' AND tipo_ciclo = 'PARCIAL'
					GROUP BY num_ciclo
					ORDER BY num_ciclo+0";
			$data = $this->db->queryAll($sql);

			foreach($data as $row){
				$sql = "SELECT prod.nombre_comercial, p.precio, p.cantidad, p.dosis, SUM(p.total) total
						FROM `ciclos_aplicacion_hist` h
						INNER JOIN ciclos_aplicacion_hist_productos p ON p.`id_ciclo_aplicacion` = h.id
						INNER JOIN products prod ON p.id_producto = prod.id
						LEFT JOIN cat_tipo_productos tipos ON prod.id_tipo_producto = tipos.id
						WHERE h.anio = 2018 AND h.finca = '{$params->params->finca}' AND tipo_ciclo = 'PARCIAL' AND h.num_ciclo = '{$row->ciclo}'
						GROUP BY prod.id
						ORDER BY tipos.orden, p.id";
				$row->productos = $this->db->queryAll($sql);

				$row->productos[] = [
					"nombre_comercial" => 'AGUA',
					"dosis" => $row->dosis_agua,
					"cantidad" => round($row->dosis_agua * $row->hectareas_fumigacion, 2)
				];
			}

			return $data;
		}

		private function foliar(){
			$params = (object)json_decode(file_get_contents("php://input"));

			$sql = "SELECT 
						num_ciclo ciclo,
						programa,
						semana,
						CONCAT(DAY(fecha_prog),'-',getMonthName(fecha_prog)) fecha_prog_c,
						CONCAT(DAY(fecha_real),'-',getMonthName(fecha_real)) fecha_real_c,
						SUM(hectareas_fumigacion) hectareas_fumigacion,
						atraso,
						motivo,
						ha_oper,
						SUM(oper) oper,
						SUM(costo_total) costo_total,
						dosis_agua
					FROM `ciclos_aplicacion_hist`
					WHERE anio = 2018 AND finca = '{$params->params->finca}' AND tipo_ciclo = 'CICLO' AND programa = 'Foliar'
					GROUP BY num_ciclo
					ORDER BY fecha_real";
			$data = $this->db->queryAll($sql);

			foreach($data as $row){
				$sql = "SELECT prod.nombre_comercial, p.precio, p.cantidad, p.dosis, SUM(p.total) total
						FROM `ciclos_aplicacion_hist` h
						INNER JOIN ciclos_aplicacion_hist_productos p ON p.`id_ciclo_aplicacion` = h.id
						INNER JOIN products prod ON p.id_producto = prod.id
						LEFT JOIN cat_tipo_productos tipos ON prod.id_tipo_producto = tipos.id
						WHERE h.anio = 2018 AND h.finca = '{$params->params->finca}' AND h.programa = 'Foliar' AND tipo_ciclo = 'CICLO' AND h.num_ciclo = '{$row->ciclo}'
						GROUP BY prod.id
						ORDER BY tipos.orden, p.id";
				$row->productos = $this->db->queryAll($sql);

				$row->productos[] = [
					"nombre_comercial" => 'AGUA',
					"dosis" => $row->dosis_agua,
					"cantidad" => round($row->dosis_agua * $row->hectareas_fumigacion, 2)
				];
			}

			return $data;
		}

		private function plagas(){
			$params = (object)json_decode(file_get_contents("php://input"));

			$sql = "SELECT 
						num_ciclo ciclo,
						programa,
						semana,
						CONCAT(DAY(fecha_prog),'-',getMonthName(fecha_prog)) fecha_prog,
						CONCAT(DAY(fecha_real),'-',getMonthName(fecha_real)) fecha_real,
						SUM(hectareas_fumigacion) hectareas_fumigacion,
						atraso,
						motivo,
						ha_oper,
						SUM(oper) oper,
						SUM(costo_total) costo_total,
						dosis_agua
					FROM `ciclos_aplicacion_hist`
					WHERE anio = 2018 AND finca = '{$params->params->finca}' AND tipo_ciclo = 'CICLO' AND programa = 'Plagas'
					GROUP BY num_ciclo
					ORDER BY fecha_real";
			$data = $this->db->queryAll($sql);

			foreach($data as $row){
				$sql = "SELECT prod.nombre_comercial, p.precio, p.cantidad, p.dosis, SUM(p.total) total
						FROM `ciclos_aplicacion_hist` h
						INNER JOIN ciclos_aplicacion_hist_productos p ON p.`id_ciclo_aplicacion` = h.id
						INNER JOIN products prod ON p.id_producto = prod.id
						WHERE h.anio = 2018 AND h.finca = '{$params->params->finca}' AND h.programa = 'Plagas' AND tipo_ciclo = 'CICLO' AND h.num_ciclo = '{$row->ciclo}'
						GROUP BY prod.id
						ORDER BY p.id";
				$row->productos = $this->db->queryAll($sql);

				$row->productos[] = [
					"nombre_comercial" => 'AGUA',
					"dosis" => $row->dosis_agua,
					"cantidad" => round($row->dosis_agua * $row->hectareas_fumigacion, 2)
				];
			}

			return $data;
		}
	}
?>
