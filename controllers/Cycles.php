<?php
	/**
	*  CLASS FROM CYCLES
	*/
	class Cycles
	{
		private $conexion;
		private $session;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		public function index(){
			$sWhere = "";
			$sOrder = " ORDER BY cycle_application.id";
			$DesAsc = "ASC";
			$sOrder .= " {$DesAsc}";
			$sLimit = "";
			if(isset($_POST)){

				/*----------  ORDER BY ----------*/
				
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY cycle_application.fecha {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == (1+1)){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY cycle_application.fecha {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == (2+1)){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY cycle_application.sector {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == (3+1)){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY tipo_ciclo {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == (4+1)){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY cat_fumigadoras.nombre {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == (5+1)){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY cat_pilotos.nombre {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == (6+1)){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY fungi1.nombre_comercial {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == (7+1)){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY fungi2.nombre_comercial {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == (8+1)){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY cycle_application.status {$DesAsc}";
				}
				/*----------  ORDER BY ----------*/

				if(isset($_POST['search_id']) && trim($_POST['search_id']) != ""){
					$sWhere .= " AND cycle_application.id = ".$_POST["search_id"];
				}				
				if((isset($_POST['search_date_from']) && trim($_POST['search_date_from']) != "") && (isset($_POST['search_date_to']) && trim($_POST['search_date_to']) != "")){
					$sWhere .= " AND cycle_application.fecha BETWEEN '".$_POST["search_date_from"]."' AND '".$_POST["search_date_to"]."'";
				}
				if(isset($_POST['search_sector']) && trim($_POST['search_sector']) != ""){
					$sWhere .= " AND cycle_application.sector LIKE '%".$_POST['search_sector']."%'";
				}
				if(isset($_POST['search_ciclo']) && trim($_POST['search_ciclo']) != ""){
					$sWhere .= " AND tipo_ciclo LIKE '%".$_POST['search_ciclo']."%'";
				}
				if(isset($_POST['search_fumigadora']) && trim($_POST['search_fumigadora']) != ""){
					$sWhere .= " AND cat_fumigadoras.nombre LIKE '%".$_POST['search_fumigadora']."%'";
				}
				if(isset($_POST['search_piloto']) && trim($_POST['search_piloto']) != ""){
					$sWhere .= " AND cat_pilotos.nombre LIKE '%".$_POST['search_piloto']."%'";
				}
				if(isset($_POST['search_fungicida_1']) && trim($_POST['search_fungicida_1']) != ""){
					$sWhere .= " AND fungi1.nombre_comercial LIKE '%".$_POST['search_fungicida_1']."%'";
				}
				if(isset($_POST['search_fungicida_2']) && trim($_POST['search_fungicida_2']) != ""){
					$sWhere .= " AND fungi2.nombre_comercial LIKE '%".$_POST['search_fungicida_2']."%'";
				}
				if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
					$sWhere .= " AND cycle_application.status = ".$_POST["order_status"];
				}

				/*----------  LIMIT  ----------*/
				if(isset($_POST['length']) && $_POST['length'] > 0){
					$sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
				}
			}

			$sql = "SELECT 
                    cycle_application.id ,
                    cycle_application.status,
                    id_hacienda ,
                    cat_fincas.nombre AS finca,
                    cycle_application.sector,
                    tipo_ciclo,
                    cycle_application.id_fumigadora,
                    cat_fumigadoras.nombre AS fumigadora,
                    id_piloto,
                    cat_pilotos.nombre AS piloto,
                    cycle_application.id_fungicida,
                    fungi1.nombre_comercial AS fungi1,
                    cycle_application.id_fungicida2,
                    fungi2.nombre_comercial AS fungi2
                    FROM cycle_application
                    LEFT JOIN cat_fincas ON cycle_application.id_hacienda = cat_fincas.id
                    LEFT JOIN cat_fumigadoras ON cycle_application.id_fumigadora = cat_fumigadoras.id
                    LEFT JOIN cat_pilotos ON cycle_application.id_piloto = cat_pilotos.id AND cycle_application.id_fumigadora = cat_pilotos.id_fumigadora
                    LEFT JOIN products AS fungi1 ON cycle_application.id_fungicida = fungi1.id
                    LEFT JOIN products AS fungi2 ON cycle_application.id_fungicida2 = fungi2.id";
                    // $sWhere $sOrder $sLimit";
			$res = $this->conexion->link->query($sql);
			$datos = (object)[
				"customActionMessage" => "Error al consultar la informacion",
				"customActionStatus" => "Error",
				"data" => [],
				"draw" => 0,
				"recordsFiltered" => 0,
				"recordsTotal" => 0,
			];
			$count = 0;
			while($fila = $res->fetch_assoc()){
				$fila = (object)$fila;
				$count++;
				$datos->data[] = [
                    (int)$fila->id,
                    (int)$fila->id,
					$fila->fecha,
					$fila->sector,
					$fila->tipo_ciclo,
					$fila->fumigadora,
					$fila->piloto,
					$fila->fungi1,
					$fila->fungi2,
					$fila->status,
					'<button class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
				];
			}

			$datos->recordsTotal = count($datos->data);
			$datos->customActionMessage = "Informacion completada con exito";
			$datos->customActionStatus = "OK";

			return json_encode($datos);
		}

		public function create(){
			extract($_POST);
			if($txtnom != "" && $id_cliente != ""){
				$sql = "INSERT INTO ciclos_de_app SET 
				id_usuario = '{$this->session->logged}',
				fecha = CURRENT_DATE ,
				ciclo = '{$txtciclo}', 
				compañia_de_aplicacion = '{$txtcompañia}' , 
				area_app = {$txtarea} , 
				costo_app_ha = {$txtcosto} , 
				costo_total_app = {$txtotal}";
				$ids = $this->conexion->Consultas(1,$sql);
        		return $ids;
			}
		}

		public function update(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();
			if($data->id_cycle > 0){
				$sql = "";
				$sql = "UPDATE cycle_application SET
						id_hacienda = '{$data->id_hacienda}',
						fecha = '{$data->fecha}',
						id_sector = '{$data->id_sector}',
						sector = '{$data->sector}',
						programa = '{$data->programa}',
						tipo_ciclo = '{$data->tipo_ciclo}',
						num_ciclo = '{$data->num_ciclo}',
						id_fumigadora = '{$data->id_fumigadora}',
						id_piloto = '{$data->id_piloto}',
						id_fungicida = '{$data->id_fungicida}',
						cantidad = '{$data->cantidad}',
						dosis = '{$data->dosis}',
						id_fungicida2 = '{$data->id_fungicida2}',
						cantidad_2 = '{$data->cantidad_2}',
						dosis_2 = '{$data->dosis_2}',
						cantidad_agua = '{$data->cantidad_agua}',
						dosis_agua = '{$data->dosis_agua}',
						cantidad_aceite = '{$data->cantidad_aceite}',
						dosis_aceite = '{$data->dosis_aceite}',
						id_emulsificante = '{$data->id_emulsificante}',
						cantidad_emulsificante = '{$data->cantidad_emulsificante}',
						dosis_emulsificante = '{$data->dosis_emulsificante}',
						id_foliar_1 = '{$data->id_foliar_1}',
						cantidad_foliar_1 = '{$data->cantidad_foliar_1}',
						dosis_foliar_1 = '{$data->dosis_foliar_1}',
						id_foliar_2 = '{$data->id_foliar_2}',
						cantidad_foliar_2 = '{$data->cantidad_foliar_2}',
						dosis_foliar_2 = '{$data->dosis_foliar_2}',
						id_foliar_3 = '{$data->id_foliar_3}',
						cantidad_foliar_3 = '{$data->cantidad_foliar_3}',
						dosis_foliar_3 = '{$data->dosis_foliar_3}'
					WHERE id = {$data->id_cycle}";
				$this->conexion->Consultas(1,$sql);
        		if($data->id_cycle > 0){
					$response->success = 200;
					$response->data = $data->id_cycle;
				}
			}
			return json_encode($response);
		}

		public function changeStatus(){
			extract($_POST);
			if((int)$id > 0 ){
				$status = ($estado == 'activo') ? 1 : 0;
				$sql = "UPDATE cycle_application SET status={$status} WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				// echo $sql;
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		public function edit(){
			$response = (object)[
				"success" => 400,
				"data" => []
			];
			if(isset($_GET['id'])){
				$id = (int)$_GET['id'];
				if($id > 0){
					$sql = "SELECT * FROM cycle_application WHERE id_usuario = '{$this->session->logged}' AND id = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->data = (object)$res->fetch_assoc();
					}
					$response->success = 200;
				}
			}
			return json_encode($response);
		}

		public function params(){
			$data = (object)json_decode(file_get_contents("php://input"));
			$data->id_cycle = (int)$data->id_cycle;
			return $data;
		}

		public function show(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();
			if($data->id_cycle > 0){
				$sql = "SELECT 
						* , id AS id_cycle , id AS codigo
					FROM cycle_application
					WHERE id = $data->id_cycle";
				$res = $this->conexion->link->query($sql);
				$response->success = 200;
				$response->data = $res->fetch_assoc();
			}
			return json_encode($response);
		}

		// 02/05/2017 - TAG: ADD OPTIONS CYCLES
		public function getFincas(){
			$accion = [];
			$sql = "SELECT id AS id , nombre AS nombre FROM cat_fincas WHERE status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$accion[$fila['id']] = $fila['nombre'];
			}
			return json_encode($accion);
		}

		public function getFumigadoras(){
			$accion = [];
			$sql = "SELECT id AS id , nombre AS nombre FROM cat_fumigadoras WHERE status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$accion[$fila['id']] = $fila['nombre'];
			}
			return json_encode($accion);
		}

		public function getPilotos(){
			$accion = [];
			$sql = "SELECT id AS id , nombre AS nombre FROM cat_pilotos WHERE status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$accion[$fila['id']] = $fila['nombre'];
			}
			return json_encode($accion);
		}

		public function getFungicidas(){
			$accion = [];
			$sql = "SELECT id AS id , nombre_comercial AS nombre FROM products WHERE tipo_producto = 'Fungicida'";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$accion[$fila['id']] = $fila['nombre'];
			}
			return json_encode($accion);
		}

		public function getFoliares(){
			$accion = [];
			$sql = "SELECT id AS id , nombre_comercial AS nombre FROM products WHERE tipo_producto = 'Foliar'";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$accion[$fila['id']] = $fila['nombre'];
			}
			return json_encode($accion);
		}

		public function getEmulsificantes(){
			$accion = [];
			$sql = "SELECT id AS id , nombre_comercial AS nombre FROM products WHERE tipo_producto = 'Coadyuvante'";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$accion[$fila['id']] = $fila['nombre'];
			}
			return json_encode($accion);
		}
		// 02/05/2017 - TAG: ADD OPTIONS CYCLES

	}
?>
