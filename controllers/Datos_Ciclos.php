<?php

class Datos_Ciclos{
	
	private $bd;
	private $session;
	
	public function __construct() {
        header('Content-Type: application/json');
        $this->bd = new M_Conexion();
        $this->session = Session::getInstance();
    }

	private function Indicadores(){
        $filters = $this->getFilters();
        $sWhereGerentes = $filters->fincas;
        if($filters->year  > 0){
            $sWhere .= " AND anio = $filters->year";
            $sYear .= " AND anio = $filters->year";
		}
        if($filters->sem > 0){
			$sWhereGerentes .= " AND semana >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND semana <= $filters->sem2";
        }
        if($filters->variable != ""){
            if($filters->variable == 'sigatoka') $sVariable .= " AND programa IN ('SIGATOKA', 'HISTORICO')";
            if($filters->variable == 'foliar') $sVariable .= " AND programa IN ('FOLIAR')";
            if($filters->variable == 'plagas') $sVariable .= " AND programa IN ('PLAGAS')";
            if($filters->variable == 'erwinia') $sVariable .= " AND programa IN ('ERWINIA')";
            if($filters->variable == 'sanidad') $sVariable .= " AND programa IN ('SIGATOKA', 'PLAGAS', 'ERWINIA')";
            if($filters->variable == 'todos') $sVariable .= "";
        }
        if($filters->tipoCiclo != ''){
            $sVariable .= " AND tipo_ciclo = '{$filters->tipoCiclo}'";
        }
        if($filters->encargado > 0){
            $sWhereGerentes .= " AND id_encargado = $filters->encargado";
        }
		$response = new stdClass;
        $response->tags = [];

        // DATA CICLOS POR FINCA
        $miVar = $filters->variable == '' ? 'TODOS' : $filters->variable;
        $sql = "SELECT finca, SUM(fila) as valor
                FROM (
                    SELECT finca, anio AS years, COUNT(DISTINCT num_ciclo) AS fila, tipo_ciclo
                    FROM ciclos_aplicacion_hist hist
                    WHERE anio > 0 AND 1=1 {$sVariable} {$sWhereGerentes} {$sYear} AND tipo_ciclo = 'CICLO'
                    GROUP BY finca , anio, num_ciclo
                    UNION ALL
                    SELECT finca, anio AS years, SUM(IF(UPPER(tipo_ciclo) = 'PARCIAL', sum_ciclo, 0)) AS fila, tipo_ciclo
                    FROM ciclos_aplicacion_hist hist
                    WHERE anio > 0 AND 1=1 {$sVariable} {$sWhereGerentes} {$sYear} AND tipo_ciclo = 'PARCIAL'
                    GROUP BY finca , anio, num_ciclo
                ) tbl
                GROUP BY finca
                ORDER BY valor";
        $data = $this->bd->queryAll($sql);

        // 11/05/2017 - TAG: CICLOS PROMEDIO
        $ciclos_promedio = 0;
        $count = 0;
        foreach($data as $row){            
            $ciclos_promedio += $row->valor;
            $count++;
        }
        $ciclos_promedio /= $count;
        $response->tags[] = (object)[
            "tittle" => "Ciclos Promedio",
            "subtittle" => "",
            "valor" => (double)round($ciclos_promedio,2),
            "promedio" => (double)round($ciclos_promedio,2),
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: CICLOS PROMEDIO
        // 11/05/2017 - TAG: MAYOR CICLO
        $response->tags[] = (object)[
            "tittle" => end($data)->finca.". Mayor Ciclos",
            "subtittle" => "",
            "valor" => (double)round(end($data)->valor,2),
            "promedio" => (double)round(end($data)->valor,2),
            "cssClass" => "red-thunderbird"
        ];
        // 11/05/2017 - TAG: MAYOR CICLO
        // 11/05/2017 - TAG: MENOR CICLO
        $response->tags[] = (object)[
            "tittle" => $data[0]->finca.". Menor Ciclos",
            "subtittle" => "",
            "valor" => (double)round($data[0]->valor,2),
            "promedio" => (double)round($data[0]->valor,2),
            "cssClass" => "green-jungle"
        ];
        // 11/05/2017 - TAG: MENOR CICLO
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO

        $data_dll_ha_ciclo = $this->getDolaresPorHectareaCiclo($filters->year, $filters);
        
        $ha_ciclos_sum = 0;
        $ha_ciclos_count = 0;
        $ha_ciclos_max = null;
        $ha_ciclos_min = null;
        $ha_ciclos = 0;

        foreach($data_dll_ha_ciclo as $row){
            if($row->value > 0){
                $ha_ciclos_sum += $row->value;
                $ha_ciclos_count ++;

                if($ha_ciclos_max == null || $ha_ciclos_max->value < $row->value){
                    $ha_ciclos_max = $row;
                }
                if($ha_ciclos_min == null || $ha_ciclos_min->value > $row->value){
                    $ha_ciclos_min = $row;
                }
            }
        }

        $ha_ciclos = $ha_ciclos_sum / $ha_ciclos_count;
        
        $response->tags[] = (object)[
            "tittle" => "$/Ha Ciclos Promedio",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos,2),
            "promedio" => (double)round($ha_ciclos,2),
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO
        $response->tags[] = (object)[
            "tittle" => $ha_ciclos_max->finca.". Mayor $/Ha Ciclos",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos_max->value,2),
            "promedio" => (double)round($ha_ciclos_max->value,2),
            "cssClass" => "red-thunderbird"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO MAX
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO MIN
        $response->tags[] = (object)[
            "tittle" => $ha_ciclos_min->finca.". Menor $/Ha Ciclos",
            //"subtittle" => "CICLO {$ha_ciclos_min->num_ciclo}",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos_min->value,2),
            "promedio" => (double)round($ha_ciclos_min->value,2),
            "cssClass" => "green-jungle"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO MIN
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO
        $sql = "SELECT 
                    IF('{$filters->tipoHectarea}' = '', getHaFinca(id_finca, anio, semana, 'FUMIGACION'), getHaFinca(id_finca, anio, semana, '$filters->tipoHectarea')) AS hectareas_fumigacion, 
                    finca,
                    ROUND(SUM(total)/
                        IF('{$filters->tipoHectarea}' = '', 
                            getHaFinca(id_finca, anio, semana, 'FUMIGACION'),
                            getHaFinca(id_finca, anio, semana, '$filters->tipoHectarea')
                    ), 2) AS dll_ha
                    
                FROM ciclos_aplicacion_hist hist
                INNER JOIN ciclos_aplicacion_hist_productos productos ON id_ciclo_aplicacion = hist.id
                WHERE anio > 0 $sYear $sWhereGerentes $sVariable
                GROUP BY finca
                ORDER BY dll_ha";
        $data_dolares_anio = $this->bd->queryAll($sql);
         
        $ha_ciclos_anio = 0;
        $sum = 0;
        $count = 0;
        foreach($data_dolares_anio as $row){
            $sum += $row->dll_ha;
            $count ++;
        }
        $ha_ciclos_anio = $sum/$count;
        $response->tags[] = (object)[
            "tittle" => "$/Ha año promedio",
            "subtittle" => "",
            "valor" => (double)0,
            "promedio" => (double)0,
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MAX
        $ha_ciclos_anio_max = $data_dolares_anio[count($data_dolares_anio)-1];
        $response->tags[] = (object)[
            "tittle" => $ha_ciclos_anio_max->finca.". Mayor $/Ha año",
            "subtittle" => "",
            "valor" => (double)0,
            "promedio" => (double)0,
            "cssClass" => "red-thunderbird"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MAX
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MIN
        $ha_ciclos_anio_min = $data_dolares_anio[0];
        $response->tags[] = (object)[
            "tittle" => $ha_ciclos_anio_min->finca.". Menor $/Ha año",
            "subtittle" => "",
            "valor" => (double)0,
            "promedio" => (double)0,
            "cssClass" => "green-jungle"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MIN

        return $response;
    }
    
    private function getDolaresPorHectareaCiclo($year, $filters){
        $sWhereGerentes = $filters->fincas;
		$sWhereGerent = $sWhereGerentes;
        $sWhere = "";
        $sVariable = "";
		if($filters->variable != ""){
            if($filters->variable == 'sigatoka') $sVariable .= " AND programa IN ('SIGATOKA', 'HISTORICO')";
            if($filters->variable == 'foliar') $sVariable .= " AND programa IN ('FOLIAR')";
            if($filters->variable == 'plagas') $sVariable .= " AND programa IN ('PLAGAS')";
            if($filters->variable == 'erwinia') $sVariable .= " AND programa IN ('ERWINIA')";
            if($filters->variable == 'sanidad') $sVariable .= " AND programa IN ('SIGATOKA', 'PLAGAS', 'ERWINIA')";
            if($filters->variable == 'todos') $sVariable = "";
        }
        if($filters->tipoCiclo != ''){
            $sVariable .= " AND tipo_ciclo = '{$filters->tipoCiclo}'";
        }
        if($filters->year > 0){
			$sWhere .= " AND anio = $filters->year";
		}
        if($filters->sem > 0){
			$sWhereGerentes .= " AND semana >= $filters->sem";
			$sWhereSem .= " AND WEEK(fecha) >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND semana <= $filters->sem2";
			$sWhereSem .= " AND WEEK(fecha) <= $filters->sem2";
        }
        if($filters->encargado > 0){
            $sWhereGerentes .= " AND id_encargado = $filters->encargado";
        }

        $years_sql = "IFNULL((SUM(IF(t2.years={$year},t2.dll,0))/COUNT(DISTINCT t2.years))/(SUM(IF(t2.years={$year},t2.has,0))/SUM(IF(t2.years={$year},t2.ciclos,0)))/(SUM(IF(t2.years={$year},t2.ciclos,0))/COUNT(DISTINCT t2.years)), 0) AS 'value'";
        $sql = "SELECT
                    t1.finca,
                    {$years_sql}
                FROM(
                    SELECT 2015 AS years, finca, 0 AS dll, 0 AS has, 'PARCIAL' AS tipo_ciclo, 0 AS ciclos
                    FROM ciclos_aplicacion_hist c
                    WHERE 1=1 {$sWhereGerentes}
                    GROUP BY anio, finca
                ) t1
                LEFT JOIN 
                (
                    SELECT anio AS years, finca, SUM(costo_total) AS dll, 
                                SUM(
                                    IF('{$filters->tipoHectarea}' = '', 
                                        getHaFinca(id_finca, anio, semana, 'FUMIGACION'), 
                                        getHaFinca(id_finca, anio, semana, '$filters->tipoHectarea')
                                    )
                                ) AS has,  tipo_ciclo,
                            (SELECT COUNT(DISTINCT num_ciclo) FROM ciclos_aplicacion_hist WHERE tipo_ciclo = 'CICLO' AND finca = hist.finca AND anio = hist.anio {$sVariable}) AS ciclos
                    FROM ciclos_aplicacion_hist hist
                    WHERE tipo_ciclo = 'CICLO' {$sVariable} {$sWhereGerentes} AND anio < 2018
                    GROUP BY anio, finca
                    UNION ALL
                    SELECT anio AS years, finca, SUM(costo_total) AS dll, 
                                IF('{$filters->tipoHectarea}' = '', 
                                    getHaFinca(id_finca, anio, semana, 'FUMIGACION'), 
                                    getHaFinca(id_finca, anio, semana, '$filters->tipoHectarea')
                                ) AS has,  tipo_ciclo,
                            (SELECT COUNT(DISTINCT num_ciclo) FROM ciclos_aplicacion_hist WHERE tipo_ciclo = 'CICLO' AND finca = hist.finca AND anio = hist.anio {$sVariable}) AS ciclos
                    FROM ciclos_aplicacion_hist hist
                    LEFT JOIN ciclos_aplicacion_hist_unidos unidos ON id_ciclo_aplicacion = hist.id
                    WHERE tipo_ciclo = 'CICLO' {$sVariable} {$sWhereGerentes} AND anio >= 2018
                    GROUP BY unidos._join
                    UNION ALL
                    SELECT anio AS years, finca, SUM(costo_total) AS dll, 
                            SUM(
                                IF('{$filters->tipoHectarea}' = '', 
                                    getHaFinca(id_finca, anio, semana, 'FUMIGACION'), 
                                    getHaFinca(id_finca, anio, semana, '$filters->tipoHectarea')
                                )
                            ) AS has,  tipo_ciclo,
                            (SELECT SUM(num_ciclo) FROM ciclos_aplicacion_hist WHERE tipo_ciclo = 'PARCIAL' AND finca = hist.finca and anio = hist.anio {$sVariable}) AS ciclos
                    FROM ciclos_aplicacion_hist hist
                    WHERE tipo_ciclo = 'PARCIAL' {$sVariable} {$sWhereGerentes}
                    GROUP BY anio, finca
                ) AS t2 ON t1.finca = t2.finca
                GROUP BY t1.finca";
                
        return $this->bd->queryAll($sql);
    }

    private function getFilters(){
        $data = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->gerente = (double)$data->params->gerente;
        $response->encargado = (int) $data->params->encargado;
        $response->sem = (int)$data->params->sem;
        $response->sem2 = (int)$data->params->sem2;
        $response->year = (double)$data->params->year;
        $response->fincas = "";
        $response->tipoHectarea = $data->params->tipoHectarea;
        $response->tipoCiclo = $data->params->tipoCiclo;
        $response->variable = $data->params->variable;

		if($response->gerente > 0){
			$response->fincas = " AND finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $response->gerente)";
		}
        return $response;
    }

	public function getIndicadores(){ 
		$response = new stdClass;
		$response = $this->Indicadores();
		return json_encode($response);
	}
}

?>