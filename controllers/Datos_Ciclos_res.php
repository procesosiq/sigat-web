<?php

class Datos_Ciclos{
	
	private $bd;
	private $session;
	
	public function __construct() {
        header('Content-Type: application/json');
        $this->bd = new M_Conexion();
        $this->session = Session::getInstance();
    }

	private function Indicadores(){
        $filters = $this->getFilters();
        $sWhereGerentes = $filters->fincas;
        $sWhereGerentes .= " AND programa != 'Foliar'";
        if($filters->year  > 0){
            $sWhere .= " AND years = $filters->year";
            $sYear .= " AND years = $filters->year";
		}
        if($filters->sem > 0){
			$sWhereGerentes .= " AND sem >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND sem <= $filters->sem2";
        }
        if($filters->variable != ""){
            $sWhere .= " AND UPPER(tipo_ciclo) = '{$filters->variable}'";
        }
		$response = new stdClass;
        $response->tags = [];

        // DATA CICLOS POR FINCA
        $miVar = $filters->variable == '' ? 'TODOS' : $filters->variable;
        $data = $this->bd->queryAll("SELECT 
            finca,
            SUM(IF(tipo_ciclo='CICLO',1,0)) AS CICLO,
            ROUND(SUM(IF(tipo_ciclo='CICLO',0,fila)), 2) AS PARCIAL,
            ROUND(SUM(IF(tipo_ciclo='CICLO',1,fila)), 2) AS TODOS,

            SUM(IF(tipo_ciclo='CICLO',fila,0)) AS ciclos,
            SUM(IF(tipo_ciclo='PARCIAL',fila,0)) AS parciales
        FROM
        (
            SELECT finca , years AS years , 1 AS fila, tipo_ciclo
            FROM ciclos_aplicacion_historico 
            WHERE years > 0 AND tipo_ciclo = 'CICLO' $sYear {$sWhereGerentes}
            GROUP BY finca , years, ciclo
            UNION ALL
            SELECT finca , years AS years , SUM(ciclo) AS fila, tipo_ciclo
            FROM ciclos_aplicacion_historico 
            WHERE years > 0 AND tipo_ciclo = 'PARCIAL' $sYear {$sWhereGerentes}
            GROUP BY finca , years, ciclo
        ) AS totalRows
        GROUP BY finca
        ORDER BY {$miVar}");

        // 11/05/2017 - TAG: CICLOS PROMEDIO
        $ciclos_promedio = 0;
        $count = 0;
        foreach($data as $row){            
            $ciclos_promedio += $row->{$miVar};
            $count++;
        }
        $ciclos_promedio /= $count;
        $response->tags[] = (object)[
            "tittle" => "Ciclos Promedio",
            "subtittle" => "",
            "valor" => (double)round($ciclos_promedio,2),
            "promedio" => (double)round($ciclos_promedio,2),
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: CICLOS PROMEDIO
        // 11/05/2017 - TAG: MAYOR CICLO
        $ciclos_mayor = $data[count($data)-1];
        $response->tags[] = (object)[
            "tittle" => $ciclos_mayor->finca.". Mayor Ciclos",
            "subtittle" => "",
            "valor" => (double)round($ciclos_mayor->{$miVar},2),
            "promedio" => (double)round($ciclos_mayor->{$miVar},2),
            "cssClass" => "red-thunderbird"
        ];
        // 11/05/2017 - TAG: MAYOR CICLO
        // 11/05/2017 - TAG: MENOR CICLO
        $ciclos_menor = $data[0];
        $response->tags[] = (object)[
            "tittle" => $ciclos_menor->finca.". Menor Ciclos",
            "subtittle" => "",
            "valor" => (double)round($ciclos_menor->{$miVar},2),
            "promedio" => (double)round($ciclos_menor->{$miVar},2),
            "cssClass" => "green-jungle"
        ];
        // 11/05/2017 - TAG: MENOR CICLO
        $costo_ha ="IFNULL(((IFNULL(total_1,0) + IFNULL(total_2,0) + IFNULL(total_5,0) + IFNULL(total_7,0) + (IFNULL(ha_oper,0) * IFNULL(ha,0))) / IFNULL(ha,0)),0)";
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO
        $data_dolares = $this->bd->queryAll("SELECT 
                finca,
                (SUM(IF(tipo_ciclo='CICLO',dll,0))
                    / (has / ciclos)
                    / ciclos) AS CICLO,
                (SUM(IF(tipo_ciclo='CICLO',0,dll))
                    / (has / ciclos)
                    / ciclos) AS PARCIAL,
                (SUM(dll)
                    / (has / ciclos)
                    / ciclos) AS TODOS,

                SUM(IF(tipo_ciclo='CICLO',dll,0)) AS dll_CICLO,
                SUM(IF(tipo_ciclo='CICLO',has,0)) AS has_CICLO,
                SUM(IF(tipo_ciclo='CICLO',ciclos,0)) AS ciclos_CICLO,
            
                SUM(IF(tipo_ciclo='CICLO',0,dll)) AS dll_PARCIAL,
                SUM(IF(tipo_ciclo='CICLO',0,has)) AS has_PARCIAL,
                SUM(IF(tipo_ciclo='CICLO',0,ciclos)) AS ciclos_PARCIAL,
            
                SUM(IF(tipo_ciclo='CICLO',dll,0)) AS dll_TODOS,
                SUM(has) AS has_TODOS,
                SUM(ciclos) AS ciclos_TODOS
            FROM
            (
                SELECT finca, SUM(costo_total) AS dll, 
                        SUM(
                            IF('{$filters->tipoHectarea}' = '', ha, (SELECT ".($filters->tipoHectarea ? $filters->tipoHectarea : "''")." FROM cat_fincas WHERE nombre = c.finca LIMIT 1))
                        ) AS has, 
                        tipo_ciclo,
                        (SELECT COUNT(DISTINCT ciclo) FROM ciclos_aplicacion_historico WHERE tipo_ciclo = 'CICLO' AND finca = c.finca {$sYear} {$sWhereGerentes}) AS ciclos
                FROM ciclos_aplicacion_historico c
                WHERE tipo_ciclo = 'CICLO' {$sYear} {$sWhereGerentes}
                GROUP BY finca
                UNION ALL
                SELECT finca, SUM(costo_total) AS dll, 
                        SUM(
                            IF('{$filters->tipoHectarea}' = '', ha, (SELECT ".($filters->tipoHectarea ? $filters->tipoHectarea : "''")." FROM cat_fincas WHERE nombre = c.finca LIMIT 1))
                        ) AS has,  tipo_ciclo,
                        (SELECT SUM(ciclo) FROM ciclos_aplicacion_historico WHERE tipo_ciclo = 'PARCIAL' AND finca = c.finca {$sYear} {$sWhereGerentes}) AS ciclos
                FROM ciclos_aplicacion_historico c
                WHERE tipo_ciclo = 'PARCIAL' {$sYear} {$sWhereGerentes}
                GROUP BY finca
            ) AS totalRows
            GROUP BY finca
            ORDER BY {$miVar}");
        
        $dll = 0;
        $has = 0;
        foreach($data_dolares as $row){
            $dll += $row->{"dll_{$miVar}"};
            $has += $row->{"has_{$miVar}"};
            $ciclos += $row->{"ciclos_{$miVar}"};
        }
        $ha_ciclos = $dll / ($has / $ciclos) / $ciclos;
        #$sql = "SELECT AVG($costo_ha) ha_ciclos FROM ciclos_aplicacion_historico WHERE 1 = 1 {$sWhere}";
        #$ha_ciclos = $this->bd->queryRow($sql);
        $response->tags[] = (object)[
            "tittle" => "$/Ha Ciclos Promedio",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos,2),
            "promedio" => (double)round($ha_ciclos,2),
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO
        $ha_ciclos_max = $data_dolares[count($data_dolares)-1];
        $response->tags[] = (object)[
            "tittle" => $ha_ciclos_max->finca.". Mayor $/Ha Ciclos",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos_max->{$miVar},2),
            "promedio" => (double)round($ha_ciclos_max->{$miVar},2),
            "cssClass" => "red-thunderbird"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO MAX
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO MIN
        $ha_ciclos_min = $data_dolares[0];
        $response->tags[] = (object)[
            "tittle" => $ha_ciclos_min->finca.". Menor $/Ha Ciclos",
            //"subtittle" => "CICLO {$ha_ciclos_min->ciclo}",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos_min->{$miVar},2),
            "promedio" => (double)round($ha_ciclos_min->{$miVar},2),
            "cssClass" => "green-jungle"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO MIN
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO
        $data_dolares_anio = $this->bd->queryAll("SELECT 
                finca,
                (SUM(IF(tipo_ciclo='CICLO',dll,0))
                    / (has / ciclos)) AS CICLO,
                (SUM(IF(tipo_ciclo='CICLO',0,dll))
                    / (has / ciclos)) AS PARCIAL,
                (SUM(dll)
                    / (has / ciclos)) AS TODOS,

                SUM(IF(tipo_ciclo='CICLO',dll,0)) AS dll_CICLO,
                SUM(IF(tipo_ciclo='CICLO',has,0) / IF(tipo_ciclo='CICLO',ciclos,0)) AS has_CICLO,
            
                SUM(IF(tipo_ciclo='CICLO',0,dll)) AS dll_PARCIAL,
                SUM(IF(tipo_ciclo='CICLO',0,has) / IF(tipo_ciclo='CICLO',0,ciclos)) AS has_PARCIAL,
            
                SUM(dll) AS dll_TODOS,
                SUM(has / ciclos) AS has_TODOS
            FROM
            (
                SELECT finca, SUM(costo_total) AS dll, 
                        SUM(
                            IF('{$filters->tipoHectarea}' = '', ha, (SELECT ".($filters->tipoHectarea ? $filters->tipoHectarea : "''")." FROM cat_fincas WHERE nombre = c.finca LIMIT 1))
                        ) AS has,  tipo_ciclo,
                    (SELECT COUNT(DISTINCT ciclo) FROM ciclos_aplicacion_historico WHERE tipo_ciclo = 'CICLO' {$sYear} {$sWhereGerentes} AND finca = c.finca) AS ciclos
                FROM ciclos_aplicacion_historico c
                WHERE tipo_ciclo = 'CICLO' {$sYear} {$sWhereGerentes}
                GROUP BY finca
                UNION ALL
                SELECT finca, SUM(costo_total) AS dll, 
                    IF('{$miVar}' = 'TODOS', 0, SUM(
                        IF('{$filters->tipoHectarea}' = '', ha, (SELECT ".($filters->tipoHectarea ? $filters->tipoHectarea : "''")." FROM cat_fincas WHERE nombre = c.finca LIMIT 1))
                    )) AS has, tipo_ciclo,
                    (SELECT SUM(ciclo) FROM ciclos_aplicacion_historico WHERE tipo_ciclo = 'PARCIAL' {$sYear} {$sWhereGerentes} AND finca = c.finca) AS ciclos
                FROM ciclos_aplicacion_historico c
                WHERE tipo_ciclo = 'PARCIAL' {$sYear} {$sWhereGerentes}
                GROUP BY finca
            ) AS totalRows
            GROUP BY finca
            ORDER BY {$miVar}");
        
        $ha_ciclos_anio = 0;
        $dll = 0;
        $has = 0;
        foreach($data_dolares_anio as $row){
            $dll += $row->{"dll_{$miVar}"};
            $has += $row->{"has_{$miVar}"};
        }
        $ha_ciclos_anio = $dll / $has;
        $response->tags[] = (object)[
            "tittle" => "$/Ha año promedio",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos_anio,2),
            "promedio" => (double)round($ha_ciclos_anio,2),
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MAX
        $ha_ciclos_anio_max = $data_dolares_anio[count($data_dolares_anio)-1];
        $response->tags[] = (object)[
            "tittle" => $ha_ciclos_anio_max->finca.". Mayor $/Ha año",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos_anio_max->{$miVar},2),
            "promedio" => (double)round($ha_ciclos_anio_max->{$miVar},2),
            "cssClass" => "red-thunderbird"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MAX
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MIN
        $ha_ciclos_anio_min = $data_dolares_anio[0];
        $response->tags[] = (object)[
            "tittle" => $ha_ciclos_anio_min->finca.". Menor $/Ha año",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos_anio_min->{$miVar},2),
            "promedio" => (double)round($ha_ciclos_anio_min->{$miVar},2),
            "cssClass" => "green-jungle"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MIN

        return $response;
	}

    private function getFilters(){
        $data = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->gerente = (double)$data->params->gerente;
        $response->sem = (int)$data->params->sem;
        $response->sem2 = (int)$data->params->sem2;
        $response->year = (double)$data->params->year;
        $response->fincas = "";
        $response->tipoHectarea = $data->params->tipoHectarea;
        $response->variable = $data->params->variable;

		if($response->gerente > 0){
			$response->fincas = " AND finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $response->gerente)";
		}
        return $response;
    }

	public function getIndicadores(){ 
		$response = new stdClass;
		$response = $this->Indicadores();
		return json_encode($response);
	}
}

?>