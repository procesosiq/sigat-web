<?php
class Datos_Resumen {
	
	private $bd;
	private $session;
	
	public function __construct() {
        header('Content-Type: application/json');
        $this->bd = new M_Conexion();
        $this->session = Session::getInstance();
    }

    public function last(){
        $response = new stdClass;
        $params = $this->getFilters();
    
        $sWhere = $params->fincas;
        if($params->year > 0){
            $sWhere .= " AND anio = {$params->year}";
        }

        $sql = "SELECT semana AS id, semana AS label
                FROM ciclos_aplicacion_hist
                WHERE 1=1 $sWhere
                GROUP BY semana
                ORDER BY semana";
        $response->semanas = $this->bd->queryAll($sql);
        return json_encode($response);
    }

	private function Indicadores(){
		$response = new stdClass;
		$filters = $this->getFilters();
		$sWhereGerentes = $filters->fincas;
		$sWhereGerent = $sWhereGerentes;
        $sWhere = "";
        $sVariable = "";
        $sWhereEncargado = "";

		if($filters->variable != ""){
            if($filters->variable == 'sigatoka') $sVariable .= " AND programa IN ('SIGATOKA', 'HISTORICO')";
            if($filters->variable == 'foliar') $sVariable .= " AND programa IN ('FOLIAR')";
            if($filters->variable == 'plagas') $sVariable .= " AND programa IN ('PLAGAS')";
            if($filters->variable == 'erwinia') $sVariable .= " AND programa IN ('ERWINIA')";
            if($filters->variable == 'sanidad') $sVariable .= " AND programa IN ('SIGATOKA', 'PLAGAS', 'ERWINIA')";
            if($filters->variable == 'todos') $sVariable .= "";
        }
        if($filters->tipoCiclo != ''){
            $sVariable .= " AND tipo_ciclo = '{$filters->tipoCiclo}'";
        }
        if($filters->year > 0){
			$sWhere .= " AND anio = $filters->year";
		}
        if($filters->sem > 0){
			$sWhereGerentes .= " AND semana >= $filters->sem";
			$sWhereSem .= " AND semana >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND semana <= $filters->sem2";
			$sWhereSem .= " AND semana <= $filters->sem2";
        }
        if($filters->encargado > 0){
            $sWhereGerentes .= " AND id_encargado = $filters->encargado";
            $sWhereEncargado .= " AND id_encargado = $filters->encargado";
        }
        $miVar = $filters->variable == '' ? 'TODOS' : $filters->variable;
        
        // Gerentes
        $sql = "SELECT id AS id , nombre AS label FROM cat_gerentes WHERE status = 1";
        $response->gerentes = $this->bd->queryAll($sql);

        // Semanas
        $sql = "SELECT semana AS id , semana AS label FROM ciclos_aplicacion_hist hist WHERE 1=1 {$sVariable} GROUP BY semana";
        $response->semanas = $this->bd->queryAll($sql);

        // Fincas
        $sql = "SELECT finca AS id , finca AS label FROM ciclos_aplicacion_hist hist WHERE 1=1 {$sVariable} {$sWhereEncargado} GROUP BY finca";
        $response->fincas = $this->bd->queryAll($sql);

        // Años
		$sql = "SELECT anio AS years
				FROM ciclos_aplicacion_hist hist
				WHERE anio > 0 AND 1=1 {$sVariable}
                GROUP BY anio";
        $response->years = $this->bd->queryAll($sql);
        
        // Encargados
        $sql = "SELECT id, nombre label
                FROM cat_encargados";
        $response->encargados = $this->bd->queryAll($sql);

        $response->ciclos_aplicados = $this->getCiclosAplicados($response->years, $filters);
        $response->ciclos_aplicados_anio = $this->getCiclosAplicadosAnio($response->years, $filters);
        $response->total_ciclos_aplicados_anio = $this->getTotalCiclosAplicadosAnio($response->years, $filters);
        $response->ciclos_aplicados_sum_anio = $this->getDolaresPorHectareaAnio($response->years, $filters);
        $response->total_ciclos_aplicados_sum_anio = $this->getTotalCiclosAplicadosSumAnio($response->years, $filters);
        $response->ciclos_aplicados_chart = $this->getCiclosAplicadosChart($response->years, $filters);
        $response->ciclos_aplicados_sum_chart = $this->getCiclosAplicadosSumChart($response->years, $filters);
        $response->ciclos_aplicados_avg_chart = $this->getCiclosAplicadosAvgChart($response->years, $filters);
        $response->resumen_costos = $this->getResumenCostos($response->years, $filters);
		
		/* TABLE FRAC */
		
        $fincas_frac = $this->bd->queryAll("SELECT finca, id_finca
            FROM ciclos_aplicacion_hist hist
            INNER JOIN ciclos_aplicacion_hist_productos ON hist.id = id_ciclo_aplicacion
            INNER JOIN products ON products.id = id_producto
            LEFT JOIN cat_frac ON products.frac = cat_frac.nombre
            WHERE anio = $filters->year
                AND tipo_ciclo = 'CICLO'
                AND products.id_tipo_producto = 4 {$sWhereSem} {$sWhereGerent}
            GROUP BY id_finca");
        $grupos = $this->bd->queryAll("SELECT nombre, frac FROM cat_frac WHERE frac > 0 ORDER BY frac DESC");
        foreach($fincas_frac as $finca){
            
            foreach($grupos as $value){
                $usadas = (int) $this->bd->queryOne("SELECT COUNT(DISTINCT num_ciclo) AS val 
                    FROM products
                    LEFT JOIN `ciclos_aplicacion_hist_productos` p ON p.id_producto = products.`id`
                    LEFT JOIN `ciclos_aplicacion_hist` h ON p.id_ciclo_aplicacion = h.id
                    WHERE anio = $filters->year
                        AND tipo_ciclo = 'CICLO'
                        AND products.id_tipo_producto = 4
                        AND id_finca = '{$finca->id_finca}' {$sWhereSem} {$sWhereGerent}
                        AND (frac = '{$value->nombre}' OR frac_2 = '{$value->nombre}')");
                $finca->{$value->nombre} = ($value->frac - $usadas);
            }

            unset($finca->id_finca);
        }
        $response->table_frac = $fincas_frac;


        // 12/05/2017 - TAG: CICLOS ATRASADOS
        $sql = "SELECT num_ciclo AS ciclo
                FROM ciclos_aplicacion_hist 
                WHERE num_ciclo > 0 
                    AND tipo_ciclo = 'CICLO' {$sWhereGerentes} {$sWhere} 
                GROUP BY num_ciclo";
        $response->ciclos = $this->bd->queryAll($sql);
        // 12/05/2017 - TAG: CICLOS ATRASADOS


        // 12/05/2017 - TAG: CICLOS ATRASADOS DATA
        $sql = "SELECT finca, num_ciclo, DATEDIFF(fecha_prog, fecha_real) AS atraso
				FROM ciclos_aplicacion_hist 
				WHERE num_ciclo > 0 
                    AND tipo_ciclo = 'CICLO'
                    AND programa IN ('Sigatoka', 'Sigatoka Foliar')
                    {$sWhereGerentes} {$sWhere}
				ORDER BY finca, num_ciclo";

        $data = $this->bd->queryAll($sql);
        $response->data_ciclos = [];
        $bgColor = "";
        $atraso = 0;
        foreach ($data as $key => $value) {
            $atraso = (int)$value->atraso;
            $response->data_ciclos[$value->finca]["finca"] = $value->finca;
            $response->data_ciclos[$value->finca]["ciclos"][$value->num_ciclo] = (int)$atraso;
            if($atraso < 0){
                $bgColor = "bg-red-thunderbird bg-font-red-thunderbird";
            }else if($atraso == 0){
                $bgColor = "bg-green-haze bg-font-green-haze";
            }else{
                $bgColor = "bg-yellow-gold bg-font-yellow-gold";
            }
            $response->data_ciclos[$value->finca]["className"][$value->num_ciclo] = $bgColor;
        }
        // 12/05/2017 - TAG: CICLOS ATRASADOS DATA

        // 12/05/2017 - TAG: MOTIVO
        $sql = "SELECT motivo AS label , COUNT(motivo) AS value FROM ciclos_aplicacion_hist hist WHERE 1=1 {$sVariable} {$sWhereGerentes} {$sWhere} GROUP BY motivo";
        $data = $this->bd->queryAll($sql);
        if(count($data) == 0){
            $data[] = ['label' => 'SIN INFORMACION', 'value' => 0];
        }
        $response->motivo = $this->pie($data , ['0%', '50%'] , "");
        // 12/05/2017 - TAG: MOTIVO

        return $response;
    }

    private function getResumenCostos($years, $filters){
        $sWhereGerentes = $filters->fincas;

        $tipo_hec = "";
        switch($filters->tipoHectarea){
            case "produccion":
                $tipo_hec = "PRODUCCION";
                break;
            case "neta":
                $tipo_hec = "NETA";
                break;
            case "banano":
                $tipo_hec = "BANANO";
                break;
            case "":
            default : 
                $tipo_hec = "FUMIGACION";
                break;
        }
        $sWhere = "";
        $sVariable = "";
		if($filters->variable != ""){
            if($filters->variable == 'sigatoka') $sVariable .= " AND programa IN ('SIGATOKA', 'HISTORICO')";
            if($filters->variable == 'foliar') $sVariable .= " AND programa IN ('FOLIAR')";
            if($filters->variable == 'plagas') $sVariable .= " AND programa IN ('PLAGAS')";
            if($filters->variable == 'erwinia') $sVariable .= " AND programa IN ('ERWINIA')";
            if($filters->variable == 'sanidad') $sVariable .= " AND programa IN ('SIGATOKA', 'PLAGAS', 'ERWINIA')";
            if($filters->variable == 'todos') $sVariable = "";
        }
        if($filters->tipoCiclo != ''){
            $sVariable .= " AND tipo_ciclo = '{$filters->tipoCiclo}'";
        }
        if($filters->sem > 0){
			$sWhereGerentes .= " AND semana >= $filters->sem";
			$sWhereSem .= " AND semana >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND semana <= $filters->sem2";
			$sWhereSem .= " AND semana <= $filters->sem2";
        }
        if($filters->encargado > 0){
            $sWhereGerentes .= " AND id_encargado = $filters->encargado";
        }

        $data = $this->getDolaresPorHectareaAnio($years, $filters);
        $total_ha = 0;
        foreach($data as $row){
            $fechas = $this->bd->queryRow("SELECT getWeek(MAX(fecha_real)) semana, YEAR(MAX(fecha_real)) anio FROM ciclos_aplicacion_hist WHERE id_finca = {$row->id_finca} AND tipo_ciclo = 'CICLO' {$sWhereSem}");
            $row->ha = (float) $this->bd->queryOne("SELECT getHaFinca({$row->id_finca}, {$fechas->anio}, {$fechas->semana}, '{$tipo_hec}')");
            $total_ha += $row->ha;

            // SEGUNDO NIVEL TIPO DE PRODUCTO
            $sql = "SELECT 
                        products.id_tipo_producto, 
                        t.nombre AS tipo_producto
                    FROM ciclos_aplicacion_hist hist
                        USE INDEX (id_finca,programa,tipo_ciclo)

                    INNER JOIN ciclos_aplicacion_hist_productos productos 
                        ON id_ciclo_aplicacion = hist.id
                    INNER JOIN `products` ON productos.id_producto = products.`id`
                    INNER JOIN cat_tipo_productos t ON t.id = products.`id_tipo_producto`
                    WHERE id_finca = {$row->id_finca} {$sWhere} {$sWhereSem} {$sWhereGerentes} {$sVariable}
                    GROUP BY products.id_tipo_producto";
            $row->detalle = $this->bd->queryAll($sql);

            $row->detalle[] = (object)["id_tipo_producto" => 0, "tipo_producto" => 'OPER'];

            foreach($row->detalle as $d){
                $sTipo = "";
                if($d->id_tipo_producto > 0){
                    $sTipo = " AND products.id_tipo_producto = {$d->id_tipo_producto}";
                }
                // TERCER NIVEL CICLOS POR TIPO DE PRODUCTO
                $sql = "SELECT *
                        FROM (
                            SELECT 
                                num_ciclo AS ciclo,
                                num_ciclo_programa AS ciclo_programa,
                                UPPER(tipo_ciclo) tipo_ciclo

                            FROM ciclos_aplicacion_hist hist
                                USE INDEX (id_finca,programa,anio,tipo_ciclo)

                            INNER JOIN ciclos_aplicacion_hist_productos productos 
                                ON id_ciclo_aplicacion = hist.id
                            INNER JOIN `products` 
                                ON productos.id_producto = products.`id`
                            INNER JOIN cat_tipo_productos t 
                                ON t.id = products.`id_tipo_producto`
                            WHERE id_finca = {$row->id_finca} AND tipo_ciclo = 'CICLO' {$sTipo} {$sWhere} {$sWhereSem} {$sWhereGerentes} {$sVariable}
                            GROUP BY num_ciclo
                            UNION ALL
                            SELECT 
                                num_ciclo AS ciclo,
                                num_ciclo_programa AS ciclo_programa,
                                UPPER(tipo_ciclo) tipo_ciclo

                            FROM ciclos_aplicacion_hist hist
                                USE INDEX (id_finca,programa,anio,tipo_ciclo)

                            INNER JOIN ciclos_aplicacion_hist_productos productos 
                                ON id_ciclo_aplicacion = hist.id
                            INNER JOIN `products` 
                                ON productos.id_producto = products.`id`
                            INNER JOIN cat_tipo_productos t 
                                ON t.id = products.`id_tipo_producto`
                            WHERE id_finca = {$row->id_finca} AND tipo_ciclo = 'PARCIAL' {$sTipo} {$sWhere} {$sWhereSem} {$sWhereGerentes} {$sVariable}
                            GROUP BY num_ciclo_programa
                        ) tbl
                        ORDER BY ciclo+0";
                $d->detalle = $this->bd->queryAll($sql);
                
                foreach($d->detalle as $c){
                    foreach($years as $y){
                        if($d->id_tipo_producto > 0){
                            $sql = "SELECT 
                                        SUM(costo_total/ha_total_sem) total
                                    FROM (
                                        SELECT *,
                                            getHaFinca(tbl.id_finca, anio, semana, '{$tipo_hec}') ha_total_sem
                                        FROM (
                                            SELECT 
                                                id_finca,
                                                anio,
                                                semana,
                                                SUM(p.total) costo_total
                                            FROM `ciclos_aplicacion_hist` hist
                                                USE INDEX (id_finca,programa,anio,tipo_ciclo,num_ciclo)
                                            INNER JOIN ciclos_aplicacion_hist_unidos unidos 
                                                USE INDEX (id_ciclo, _join)
                                                ON unidos.id_ciclo_aplicacion = hist.id
                                            INNER JOIN `ciclos_aplicacion_hist_productos` p 
                                                ON p.id_ciclo_aplicacion = hist.id
                                            INNER JOIN `products` 
                                                ON p.id_producto = products.`id`
                                            INNER JOIN cat_tipo_productos t 
                                                ON t.id = {$d->id_tipo_producto}
                                            WHERE 
                                                anio = {$y->years}
                                                AND products.id_tipo_producto = {$d->id_tipo_producto}
                                                AND id_finca = {$row->id_finca}
                                                AND num_ciclo = '{$c->ciclo}'
                                                AND tipo_ciclo = 'CICLO'
                                                AND tipo_ciclo = '{$c->tipo_ciclo}'
                                                {$sWhere} {$sWhereGerentes} {$sVariable} {$sWhereSem}
                                            GROUP BY unidos._join
                                        ) tbl
                                        UNION ALL
                                        SELECT 
                                            id_finca,
                                            anio,
                                            semana,
                                            p.total costo_total,
                                            getHaFinca(id_finca, anio, semana, 'FUMIGACION') ha_total_sem
                                            #hectareas_fumigacion ha_total_sem
                                        FROM `ciclos_aplicacion_hist` hist
                                            USE INDEX (id_finca,programa,anio,tipo_ciclo,num_ciclo)
                                        INNER JOIN `ciclos_aplicacion_hist_productos` p 
                                            ON p.id_ciclo_aplicacion = hist.id
                                        INNER JOIN `products` 
                                            ON p.id_producto = products.`id`
                                        INNER JOIN cat_tipo_productos t 
                                            ON t.id = {$d->id_tipo_producto}
                                        WHERE 
                                            anio = {$y->years}
                                            AND products.id_tipo_producto = {$d->id_tipo_producto}
                                            AND id_finca = {$row->id_finca}
                                            AND num_ciclo_programa = '{$c->ciclo_programa}'
                                            AND tipo_ciclo = 'PARCIAL'
                                            AND tipo_ciclo = '{$c->tipo_ciclo}'
                                            {$sWhere} {$sWhereGerentes} {$sVariable} {$sWhereSem}
                                    ) tbl";
                        }else{
                            $sql = "SELECT 
                                        SUM(costo_total/ha_total_sem) total
                                    FROM (
                                        SELECT *,
                                            getHaFinca(tbl.id_finca, anio, semana, '{$tipo_hec}') ha_total_sem
                                        FROM (
                                            SELECT 
                                                id_finca,
                                                anio,
                                                semana,
                                                SUM(oper) costo_total
                                            FROM `ciclos_aplicacion_hist` hist
                                                USE INDEX (id_finca,programa,anio,tipo_ciclo,num_ciclo)
                                            LEFT JOIN ciclos_aplicacion_hist_unidos unidos ON unidos.id_ciclo_aplicacion = hist.id
                                            WHERE 
                                                anio = {$y->years}
                                                AND id_finca = {$row->id_finca}
                                                AND num_ciclo = '{$c->ciclo}'
                                                AND tipo_ciclo = 'CICLO'
                                                AND tipo_ciclo = '{$c->tipo_ciclo}'
                                                {$sWhere} {$sWhereGerentes} {$sVariable} {$sWhereSem}
                                            GROUP BY unidos._join
                                        ) tbl
                                        UNION ALL
                                        SELECT 
                                            id_finca,
                                            anio,
                                            semana,
                                            SUM(oper) costo_total,
                                            getHaFinca(id_finca, anio, semana, 'FUMIGACION') ha_total_sem
                                            #SUM(hectareas_fumigacion) ha_total_sem
                                        FROM `ciclos_aplicacion_hist` hist
                                            USE INDEX (id_finca,programa,anio,tipo_ciclo,num_ciclo)
                                        WHERE 
                                            anio = {$y->years}
                                            AND id_finca = {$row->id_finca}
                                            AND num_ciclo_programa = '{$c->ciclo_programa}'
                                            AND tipo_ciclo = 'PARCIAL'
                                            AND tipo_ciclo = '{$c->tipo_ciclo}'
                                            {$sWhere} {$sWhereGerentes} {$sVariable} {$sWhereSem}
                                        GROUP BY num_ciclo_programa, programa
                                    ) tbl";
                        }

                        $c->{$y->years} = $this->bd->queryOne($sql);
                    }
                }
            }
        }

        foreach($data as $row){
            $row->porc = $row->ha / $total_ha;
        }

        return $data;
    }

    private function getCiclosAplicadosAvgChart($years, $filters){
        $sWhereGerentes = $filters->fincas;
		$sWhereGerent = $sWhereGerentes;
        $sWhere = "";
        $sVariable = "";
		if($filters->variable != ""){
            if($filters->variable == 'sigatoka') $sVariable .= " AND programa IN ('SIGATOKA', 'HISTORICO')";
            if($filters->variable == 'foliar') $sVariable .= " AND programa IN ('FOLIAR')";
            if($filters->variable == 'plagas') $sVariable .= " AND programa IN ('PLAGAS')";
            if($filters->variable == 'erwinia') $sVariable .= " AND programa IN ('ERWINIA')";
            if($filters->variable == 'sanidad') $sVariable .= " AND programa IN ('SIGATOKA', 'PLAGAS', 'ERWINIA')";
            if($filters->variable == 'todos') $sVariable = "";
        }
        if($filters->tipoCiclo != ''){
            $sVariable .= " AND tipo_ciclo = '{$filters->tipoCiclo}'";
        }
        if($filters->year > 0){
			$sWhere .= " AND anio = $filters->year";
		}
        if($filters->sem > 0){
			$sWhereGerentes .= " AND semana >= $filters->sem";
			$sWhereSem .= " AND semana >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND semana <= $filters->sem2";
			$sWhereSem .= " AND semana <= $filters->sem2";
        }
        if($filters->encargado > 0){
            $sWhereGerentes .= " AND id_encargado = $filters->encargado";
        }
        $miVar = $filters->variable == '' ? 'TODOS' : $filters->variable;

        $sql = "SELECT
                    fincas.finca AS label,
                    years.years AS legend,
                    IFNULL(ROUND(SUM(IFNULL(t2.dll,0))/(SUM(IFNULL(t2.has,0))/SUM(IFNULL(t2.ciclos, 0)))/(SUM(IFNULL(t2.ciclos,0))), 2), 0) AS 'value',
                    IF(years.years > 2013 ,true,false) AS selected
                FROM(
                    SELECT finca
                    FROM ciclos_aplicacion_hist c
                    WHERE 1=1 {$sWhereGerentes}
                    GROUP BY finca
                ) fincas
                JOIN (
                    SELECT anio AS years
                    FROM ciclos_aplicacion_hist
                    WHERE anio >= 2015
                    GROUP BY anio
                ) years
                LEFT JOIN 
                (
                    SELECT anio AS years, finca, SUM(costo_total) AS dll, 
                                SUM(
                                    IF('' = '', hectareas_fumigacion, (SELECT '' FROM cat_fincas WHERE nombre = finca LIMIT 1))
                                ) AS has,  tipo_ciclo,
                            (SELECT COUNT(DISTINCT num_ciclo) FROM ciclos_aplicacion_hist WHERE tipo_ciclo = 'CICLO' AND finca = hist.finca AND anio = hist.anio AND programa != 'Foliar') AS ciclos
                    FROM ciclos_aplicacion_hist hist
                    WHERE 1=1 {$sVariable} {$sWhereGerentes}
                    GROUP BY anio, finca
                    UNION ALL
                    SELECT anio AS years, finca, SUM(costo_total) AS dll, 
                            SUM(
                                IF('' = '', hectareas_fumigacion, (SELECT '' FROM cat_fincas WHERE nombre = finca LIMIT 1))
                            ) AS has,  tipo_ciclo,
                            (SELECT SUM(num_ciclo) FROM ciclos_aplicacion_hist WHERE tipo_ciclo = 'PARCIAL' AND finca = hist.finca AND anio = hist.anio AND programa != 'Foliar') AS ciclos
                    FROM ciclos_aplicacion_hist hist
                    WHERE 1=1 {$sVariable} {$sWhereGerentes}
                    GROUP BY anio, finca
                ) AS t2 ON fincas.finca = t2.finca AND years.years = t2.years
                GROUP BY fincas.finca, years.years";
                
        return $this->generateSeries($sql , $filters);
    }

    private function getCiclosAplicadosSumChart($years, $filters){
        $sWhereGerentes = $filters->fincas;
		$sWhereGerent = $sWhereGerentes;
        $sWhere = "";
        $sVariable = "";
		if($filters->variable != ""){
            if($filters->variable == 'sigatoka') $sVariable .= " AND programa IN ('SIGATOKA', 'HISTORICO')";
            if($filters->variable == 'foliar') $sVariable .= " AND programa IN ('FOLIAR')";
            if($filters->variable == 'plagas') $sVariable .= " AND programa IN ('PLAGAS')";
            if($filters->variable == 'erwinia') $sVariable .= " AND programa IN ('ERWINIA')";
            if($filters->variable == 'sanidad') $sVariable .= " AND programa IN ('SIGATOKA', 'PLAGAS', 'ERWINIA')";
            if($filters->variable == 'todos') $sVariable = "";
        }
        if($filters->tipoCiclo != ''){
            $sVariable .= " AND tipo_ciclo = '{$filters->tipoCiclo}'";
        }
        if($filters->year > 0){
			$sWhere .= " AND anio = $filters->year";
		}
        if($filters->sem > 0){
			$sWhereGerentes .= " AND semana >= $filters->sem";
			$sWhereSem .= " AND semana >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND semana <= $filters->sem2";
			$sWhereSem .= " AND semana <= $filters->sem2";
        }
        if($filters->encargado > 0){
            $sWhereGerentes .= " AND id_encargado = $filters->encargado";
        }
        $miVar = $filters->variable == '' ? 'TODOS' : $filters->variable;

        $sql = "SELECT 
                    fincas.finca AS label,
                    years.years AS legend,
                    ROUND(SUM(total)/(SUM(has)/SUM(ciclos)), 2) AS 'value',
                    IF(years.years > 2013, true, false) as selected
                FROM (
                    SELECT anio AS years
                    FROM ciclos_aplicacion_hist
                    WHERE anio >= 2015
                    GROUP BY anio
                ) AS years
                JOIN (
                    SELECT finca
                    FROM ciclos_aplicacion_hist
                    WHERE 1=1 {$sWhereGerentes}
                    GROUP BY finca
                ) AS fincas
                LEFT JOIN
                (
                    SELECT finca, anio AS years, SUM(costo_total) AS total, 
                            SUM(
                                IF('{$filters->tipoHectarea}' = '', hectareas_fumigacion, getHaFinca(id_finca, anio, semana, '$filters->tipoHectarea'))
                            ) AS has,  COUNT(DISTINCT num_ciclo) AS ciclos, 'CICLO' AS tipo_ciclo
                    FROM ciclos_aplicacion_hist  hist
                    WHERE anio > 0 AND 1=1 AND tipo_ciclo = 'CICLO' {$sWhereGerentes} {$sVariable}
                    GROUP BY finca , anio 
                    UNION ALL
                    SELECT finca, anio AS years, SUM(costo_total) AS total, 
                            SUM(
                                IF('{$filters->tipoHectarea}' = '', hectareas_fumigacion, getHaFinca(id_finca, anio, semana, '$filters->tipoHectarea'))
                            ) AS has,  SUM(num_ciclo) AS ciclos, 'PARCIAL' AS tipo_ciclo
                    FROM ciclos_aplicacion_hist  hist
                    WHERE anio > 0 AND 1=1 AND tipo_ciclo = 'PARCIAL' {$sWhereGerentes} {$sVariable}
                    GROUP BY finca , anio 
                ) AS tbl ON tbl.years = years.years AND tbl.finca = fincas.finca
                GROUP BY years.years, fincas.finca";
        return $this->generateSeries($sql , $filters);
    }

    private function getCiclosAplicadosChart($years, $filters){
        $sWhereGerentes = $filters->fincas;
		$sWhereGerent = $sWhereGerentes;
        $sWhere = "";
        $sVariable = "";
		if($filters->variable != ""){
            if($filters->variable == 'sigatoka') $sVariable .= " AND programa IN ('SIGATOKA', 'HISTORICO')";
            if($filters->variable == 'foliar') $sVariable .= " AND programa IN ('FOLIAR')";
            if($filters->variable == 'plagas') $sVariable .= " AND programa IN ('PLAGAS')";
            if($filters->variable == 'erwinia') $sVariable .= " AND programa IN ('ERWINIA')";
            if($filters->variable == 'sanidad') $sVariable .= " AND programa IN ('SIGATOKA', 'PLAGAS', 'ERWINIA')";
            if($filters->variable == 'todos') $sVariable = "";
        }
        if($filters->tipoCiclo != ''){
            $sVariable .= " AND tipo_ciclo = '{$filters->tipoCiclo}'";
        }
        if($filters->year > 0){
			$sWhere .= " AND anio = $filters->year";
		}
        if($filters->sem > 0){
			$sWhereGerentes .= " AND semana >= $filters->sem";
			$sWhereSem .= " AND semana >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND semana <= $filters->sem2";
			$sWhereSem .= " AND semana <= $filters->sem2";
        }
        if($filters->encargado > 0){
            $sWhereGerentes .= " AND id_encargado = $filters->encargado";
        }
        $miVar = $filters->variable == '' ? 'TODOS' : $filters->variable;

        $sql = "SELECT 
					fincas.finca AS label,
                    years.years AS legend,
					SUM(IF(t2.tipo_ciclo='CICLO',1,t2.fila)) AS 'value',
                    IF(years.years > 2013, true, false) AS selected
                FROM(
                    SELECT anio AS years
                    FROM ciclos_aplicacion_hist
                    WHERE anio >= 2015
                    GROUP BY anio
                ) AS years
                JOIN (
                    SELECT finca
                    FROM ciclos_aplicacion_hist
                    WHERE 1=1 {$sWhereGerentes}
                    GROUP BY finca
                ) AS fincas
				LEFT JOIN
				(
					SELECT finca , anio AS years , SUM(IF(UPPER(tipo_ciclo) = 'CICLO', 1, num_ciclo)) AS fila, tipo_ciclo
					FROM ciclos_aplicacion_hist hist
					WHERE anio > 0 AND 1=1 {$sVariable} {$sWhereGerentes}
					GROUP BY finca , anio, num_ciclo
				) AS t2 ON fincas.finca = t2.finca AND years.years = t2.years
                GROUP BY years.years, fincas.finca
                ORDER BY fincas.finca DESC";
                
        return $this->generateSeries($sql , $filters);
    }

    private function getTotalCiclosAplicadosSumAnio($years, $filters){
        $sWhereGerentes = $filters->fincas;
		$sWhereGerent = $sWhereGerentes;
        $sWhere = "";
        $sVariable = "";
		if($filters->variable != ""){
            if($filters->variable == 'sigatoka') $sVariable .= " AND programa IN ('SIGATOKA', 'HISTORICO')";
            if($filters->variable == 'foliar') $sVariable .= " AND programa IN ('FOLIAR')";
            if($filters->variable == 'plagas') $sVariable .= " AND programa IN ('PLAGAS')";
            if($filters->variable == 'erwinia') $sVariable .= " AND programa IN ('ERWINIA')";
            if($filters->variable == 'sanidad') $sVariable .= " AND programa IN ('SIGATOKA', 'PLAGAS', 'ERWINIA')";
            if($filters->variable == 'todos') $sVariable = "";
        }
        if($filters->tipoCiclo != ''){
            $sVariable .= " AND tipo_ciclo = '{$filters->tipoCiclo}'";
        }
        if($filters->year > 0){
			$sWhere .= " AND anio = $filters->year";
		}
        if($filters->sem > 0){
			$sWhereGerentes .= " AND semana >= $filters->sem";
			$sWhereSem .= " AND semana >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND semana <= $filters->sem2";
			$sWhereSem .= " AND semana <= $filters->sem2";
        }
        if($filters->encargado > 0){
            $sWhereGerentes .= " AND id_encargado = $filters->encargado";
        }
        $miVar = $filters->variable == '' ? 'TODOS' : $filters->variable;

        $years_sql = [];
        foreach($years as $y) {
            $years_sql[] = "SUM(IF(years = {$y->years},dll,0))/SUM(IF(years = {$y->years},has,0)) AS '{$y->years}'";
        }
        $years_sql = implode(",", $years_sql);
        $sql = "SELECT 
                    {$years_sql}
                FROM(
                    SELECT anio AS years, finca, SUM(costo_total) AS dll, 
                                SUM(
                                    IF('{$filters->tipoHectarea}' = '', hectareas_fumigacion, getHaFinca(id_finca, anio, semana, '$filters->tipoHectarea'))
                                )/COUNT(DISTINCT num_ciclo) AS has
                    FROM ciclos_aplicacion_hist hist
                    WHERE anio > 0 {$sWhereGerentes} {$sVariable}
                    GROUP BY finca, anio
                ) AS tbl";
        return $this->bd->queryRow($sql);
    }

    private function getDolaresPorHectareaAnio($years, $filters){

        $tipo_hec = "";
        switch($filters->tipoHectarea){
            case "produccion":
                $tipo_hec = "PRODUCCION";
                break;
            case "neta":
                $tipo_hec = "NETA";
                break;
            case "banano":
                $tipo_hec = "BANANO";
                break;
            case "":
            default : 
                $tipo_hec = "FUMIGACION";
                break;
        }

        $sWhereGerentes = $filters->fincas;
		$sWhereGerent = $sWhereGerentes;
        $sWhere = "";
        $sVariable = "";
        $sWhereEncargado = "";

		if($filters->variable != ""){
            if($filters->variable == 'sigatoka') $sVariable .= " AND programa IN ('SIGATOKA', 'HISTORICO')";
            if($filters->variable == 'foliar') $sVariable .= " AND programa IN ('FOLIAR')";
            if($filters->variable == 'plagas') $sVariable .= " AND programa IN ('PLAGAS')";
            if($filters->variable == 'erwinia') $sVariable .= " AND programa IN ('ERWINIA')";
            if($filters->variable == 'sanidad') $sVariable .= " AND programa IN ('SIGATOKA', 'PLAGAS', 'ERWINIA')";
            if($filters->variable == 'todos') $sVariable = "";
        }
        if($filters->tipoCiclo != ''){
            $sVariable .= " AND tipo_ciclo = '{$filters->tipoCiclo}'";
        }
        if($filters->sem > 0){
			$sWhere .= " AND semana >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhere .= " AND semana <= $filters->sem2";
        }
        if($filters->encargado > 0){
            $sWhereGerentes .= " AND id_encargado = $filters->encargado";
            $sWhereEncargado .= " AND hist.id_encargado = $filters->encargado";
        }

        $sql = "SELECT 
                    id_finca,
                    f.nombre as finca
                FROM `ciclos_aplicacion_hist` hist
                INNER JOIN cat_fincas f ON f.id = id_finca
                WHERE 1=1 {$sWhere} {$sVariable} {$sWhereEncargado}
                GROUP BY id_finca";
        $fincas = $this->bd->queryAll($sql);
        
        foreach($fincas as $row){
            $row = (object) $row;
            foreach($years as $y){
                $sql = "SELECT 
                            SUM(costo_total/ha_total_sem) total
                        FROM (
                            SELECT *,
                                getHaFinca(tbl.id_finca, anio, semana, '{$tipo_hec}') ha_total_sem
                            FROM (
                                SELECT 
                                    anio,
                                    GROUP_CONCAT(DISTINCT hist.id SEPARATOR ',') AS ids,
                                    num_ciclo ciclo,
                                    GROUP_CONCAT(DISTINCT programa SEPARATOR '-') programa,
                                    semana,
                                    fecha_prog,
                                    fecha_real,
                                    SUM(DISTINCT hectareas_fumigacion) hectareas_fumigacion,
                                    atraso,
                                    motivo,
                                    SUM(DISTINCT ha_oper) ha_oper,
                                    SUM(DISTINCT oper) oper,
                                    SUM(costo_total) costo_total,
                                    dosis_agua,
                                    COUNT(DISTINCT programa) programas,
                                    id_finca
                                FROM `ciclos_aplicacion_hist` hist
                                    USE INDEX (id_finca,programa,anio)
                                INNER JOIN ciclos_aplicacion_hist_unidos unidos 
                                    USE INDEX (id_ciclo) 
                                    ON id_ciclo_aplicacion = hist.id
                                WHERE anio = {$y->years} AND id_finca = {$row->id_finca} {$sVariable} {$sWhere}
                                GROUP BY unidos._join
                                ORDER BY num_ciclo+0
                            ) tbl
                        ) tbl";
                $row->{$y->years} = $this->bd->queryOne($sql);
            }
        }

        return $fincas;
    }

    private function getTotalCiclosAplicadosAnio($years, $filters){
        $sWhereGerentes = $filters->fincas;
		$sWhereGerent = $sWhereGerentes;
        $sWhere = "";
        $sVariable = "";
		if($filters->variable != ""){
            if($filters->variable == 'sigatoka') $sVariable .= " AND programa IN ('SIGATOKA', 'HISTORICO')";
            if($filters->variable == 'foliar') $sVariable .= " AND programa IN ('FOLIAR')";
            if($filters->variable == 'plagas') $sVariable .= " AND programa IN ('PLAGAS')";
            if($filters->variable == 'erwinia') $sVariable .= " AND programa IN ('ERWINIA')";
            if($filters->variable == 'sanidad') $sVariable .= " AND programa IN ('SIGATOKA', 'PLAGAS', 'ERWINIA')";
            if($filters->variable == 'todos') $sVariable = "";
        }
        if($filters->tipoCiclo != ''){
            $sVariable .= " AND tipo_ciclo = '{$filters->tipoCiclo}'";
        }
        if($filters->year > 0){
			$sWhere .= " AND anio = $filters->year";
		}
        if($filters->sem > 0){
			$sWhereGerentes .= " AND semana >= $filters->sem";
			$sWhereSem .= " AND semana >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND semana <= $filters->sem2";
			$sWhereSem .= " AND semana <= $filters->sem2";
        }
        if($filters->encargado > 0){
            $sWhereGerentes .= " AND id_encargado = $filters->encargado";
        }
        $miVar = $filters->variable == '' ? 'TODOS' : $filters->variable;
        
        $years_sql = [];
        foreach($years as $y) {
            $years_sql[] = "(dll_{$y->years} / (has_{$y->years} / ciclos_{$y->years}) / ciclos_{$y->years}) AS '{$y->years}'";
        }
        $years_sql = implode(",", $years_sql);

        $years_sql_2 = [];
        foreach($years as $y) {
            $years_sql_2[] = "
            SUM(IF(years={$y->years},dll,0)) AS dll_{$y->years},
            SUM(IF(years={$y->years},has,0)) AS has_{$y->years},
            SUM(IF(years={$y->years},ciclos,0)) AS ciclos_{$y->years}";
        }
        $years_sql_2 = implode(",", $years_sql_2);

        $sql = "SELECT {$years_sql}
            FROM(
            SELECT
                finca,
                {$years_sql_2}
            FROM
            (
                SELECT anio AS years, finca, SUM(costo_total) AS dll, 
                            SUM(
                                IF('{$filters->tipoHectarea}' = '', hectareas_fumigacion, getHaFinca(id_finca, anio, semana, '$filters->tipoHectarea'))
                            ) AS has,  tipo_ciclo,
                        (SELECT COUNT(DISTINCT num_ciclo) FROM ciclos_aplicacion_hist WHERE tipo_ciclo = 'CICLO' AND finca = hist.finca AND anio = hist.anio) AS ciclos
                FROM ciclos_aplicacion_hist hist
                WHERE tipo_ciclo = 'CICLO'  {$sWhereGerentes} {$sVariable} AND anio < 2018
                GROUP BY anio, finca
                UNION ALL
                    SELECT anio AS years, finca, SUM(costo_total) AS dll, 
                                SUM(
                                    IF('{$filters->tipoHectarea}' = '', hectareas_fumigacion, getHaFinca(id_finca, anio, semana, '$filters->tipoHectarea'))
                                ) AS has,  tipo_ciclo,
                            (SELECT COUNT(DISTINCT num_ciclo) FROM ciclos_aplicacion_hist WHERE finca = hist.finca AND anio = hist.anio AND programa != 'Foliar') AS ciclos
                    FROM ciclos_aplicacion_hist hist
                    LEFT JOIN ciclos_aplicacion_hist_unidos unidos ON id_ciclo_aplicacion = hist.id
                    WHERE tipo_ciclo = 'CICLO' {$sVariable} {$sWhereGerentes} AND anio >= 2018
                    GROUP BY unidos._join
                UNION ALL
                SELECT anio AS years, finca, SUM(costo_total) AS dll, 
                        SUM(
                            IF('{$filters->tipoHectarea}' = '', hectareas_fumigacion, getHaFinca(id_finca, anio, semana, '$filters->tipoHectarea'))
                        ) AS has,  tipo_ciclo,
                        (SELECT SUM(num_ciclo) FROM ciclos_aplicacion_hist WHERE tipo_ciclo = 'PARCIAL' AND finca = hist.finca and anio = hist.anio) AS ciclos
                FROM ciclos_aplicacion_hist hist
                WHERE tipo_ciclo = 'PARCIAL' {$sWhereGerentes} {$sVariable}
                GROUP BY anio, finca
            ) AS totalRows) AS tbl";
        return $this->bd->queryRow($sql);
    }

    private function getCiclosAplicadosAnio($years, $filters){
        $sWhereGerentes = $filters->fincas;
		$sWhereGerent = $sWhereGerentes;
        $sWhere = "";
        $sVariable = "";
		if($filters->variable != ""){
            if($filters->variable == 'sigatoka') $sVariable .= " AND programa IN ('SIGATOKA', 'HISTORICO')";
            if($filters->variable == 'foliar') $sVariable .= " AND programa IN ('FOLIAR')";
            if($filters->variable == 'plagas') $sVariable .= " AND programa IN ('PLAGAS')";
            if($filters->variable == 'erwinia') $sVariable .= " AND programa IN ('ERWINIA')";
            if($filters->variable == 'sanidad') $sVariable .= " AND programa IN ('SIGATOKA', 'PLAGAS', 'ERWINIA')";
            if($filters->variable == 'todos') $sVariable = "";
        }
        if($filters->tipoCiclo != ''){
            $sVariable .= " AND tipo_ciclo = '{$filters->tipoCiclo}'";
        }
        if($filters->year > 0){
			$sWhere .= " AND anio = $filters->year";
		}
        if($filters->sem > 0){
			$sWhereGerentes .= " AND semana >= $filters->sem";
			$sWhereSem .= " AND semana >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND semana <= $filters->sem2";
			$sWhereSem .= " AND semana <= $filters->sem2";
        }
        if($filters->encargado > 0){
            $sWhereGerentes .= " AND id_encargado = $filters->encargado";
        }
        $miVar = $filters->variable == '' ? 'TODOS' : $filters->variable;

        $years_sql = [];
        foreach($years as $y) {
            $years_sql[] = "IFNULL((SUM(IF(t2.years={$y->years},t2.dll,0))/COUNT(DISTINCT t2.years))/(SUM(IF(t2.years={$y->years},t2.has,0))/SUM(IF(t2.years={$y->years},t2.ciclos,0)))/(SUM(IF(t2.years={$y->years},t2.ciclos,0))/COUNT(DISTINCT t2.years)), 0) AS '{$y->years}'";
        }
        $years_sql = implode(",", $years_sql);
        $sql = "SELECT
                    t1.finca,
                    {$years_sql}
                FROM(
                    SELECT 2015 AS years, finca, 0 AS dll, 0 AS has, 'PARCIAL' AS tipo_ciclo, 0 AS ciclos
                    FROM ciclos_aplicacion_hist c
                    WHERE 1=1 {$sWhereGerentes}
                    GROUP BY anio, finca
                ) t1
                LEFT JOIN 
                (
                    SELECT anio AS years, finca, SUM(costo_total) AS dll, 
                                SUM(
                                    IF('{$filters->tipoHectarea}' = '', 
                                        getHaFinca(id_finca, anio, semana, 'FUMIGACION'), 
                                        getHaFinca(id_finca, anio, semana, '$filters->tipoHectarea')
                                    )
                                ) AS has,  tipo_ciclo,
                            (SELECT COUNT(DISTINCT num_ciclo) FROM ciclos_aplicacion_hist WHERE tipo_ciclo = 'CICLO' AND finca = hist.finca AND anio = hist.anio {$sVariable}) AS ciclos
                    FROM ciclos_aplicacion_hist hist
                    WHERE tipo_ciclo = 'CICLO' {$sVariable} {$sWhereGerentes} AND anio < 2018
                    GROUP BY anio, finca
                    UNION ALL
                    SELECT years, finca, IF('{$filters->tipoHectarea}' = '', SUM(dll)/SUM(has), SUM(dll)/AVG(has)/ciclos) dll, 1 has, tipo_ciclo, ciclos
                    FROM (
                        SELECT anio AS years, finca, SUM(costo_total) AS dll, 
                                    IF('{$filters->tipoHectarea}' = '', 
                                        SUM(hectareas_fumigacion), 
                                        getHaFinca(id_finca, anio, semana, '$filters->tipoHectarea')
                                    ) AS has,
                                    tipo_ciclo,
                                (SELECT MAX(num_ciclo_programa+0) FROM ciclos_aplicacion_hist WHERE tipo_ciclo = 'CICLO' AND finca = hist.finca AND anio = hist.anio {$sVariable}) AS ciclos
                        FROM ciclos_aplicacion_hist hist
                        LEFT JOIN ciclos_aplicacion_hist_unidos unidos ON id_ciclo_aplicacion = hist.id
                        WHERE tipo_ciclo = 'CICLO' {$sVariable} {$sWhereGerentes} AND anio >= 2018
                        GROUP BY unidos._join
                    ) tbl
                    GROUP BY years, finca
                    UNION ALL
                    SELECT anio AS years, finca, SUM(costo_total) AS dll, 
                            SUM(IF('{$filters->tipoHectarea}' = '', 
                                getHaFinca(id_finca, anio, semana, 'FUMIGACION'), 
                                getHaFinca(id_finca, anio, semana, '$filters->tipoHectarea')
                            )) AS has,
                            tipo_ciclo,
                            (SELECT SUM(num_ciclo) FROM ciclos_aplicacion_hist WHERE tipo_ciclo = 'PARCIAL' AND finca = hist.finca and anio = hist.anio AND programa != 'Foliar') AS ciclos
                    FROM ciclos_aplicacion_hist hist
                    WHERE tipo_ciclo = 'PARCIAL' {$sVariable} {$sWhereGerentes}
                    GROUP BY anio, finca
                ) AS t2 ON t1.finca = t2.finca
                GROUP BY t1.finca";
                
        $data = $this->bd->queryAll($sql);
        
        return $data;
    }
    
    private function getCiclosAplicados($years, $filters){
        $sWhereGerentes = $filters->fincas;
		$sWhereGerent = $sWhereGerentes;
        $sWhere = "";
        $sVariable = "";
		if($filters->variable != ""){
            if($filters->variable == 'sigatoka') $sVariable .= " AND programa IN ('SIGATOKA', 'HISTORICO')";
            if($filters->variable == 'foliar') $sVariable .= " AND programa IN ('FOLIAR')";
            if($filters->variable == 'plagas') $sVariable .= " AND programa IN ('PLAGAS')";
            if($filters->variable == 'erwinia') $sVariable .= " AND programa IN ('ERWINIA')";
            if($filters->variable == 'sanidad') $sVariable .= " AND programa IN ('SIGATOKA', 'PLAGAS', 'ERWINIA')";
            if($filters->variable == 'todos') $sVariable = "";
        }
        if($filters->tipoCiclo != ''){
            $sVariable .= " AND tipo_ciclo = '{$filters->tipoCiclo}'";
        }
        if($filters->year > 0){
			$sWhere .= " AND anio = $filters->year";
		}
        if($filters->sem > 0){
			$sWhereGerentes .= " AND semana >= $filters->sem";
			$sWhereSem .= " AND semana >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND semana <= $filters->sem2";
			$sWhereSem .= " AND semana <= $filters->sem2";
        }
        if($filters->encargado > 0){
            $sWhereGerentes .= " AND id_encargado = $filters->encargado";
        }
        $miVar = $filters->variable == '' ? 'TODOS' : $filters->variable;

        $years_sql = [];
        foreach($years as $y) {
            $years_sql[] = "SUM(CASE WHEN t2.years = {$y->years} THEN IF(t2.tipo_ciclo='CICLO',1,t2.fila) ELSE 0 END) AS '{$y->years}'";
        }
        $years_sql = implode(",", $years_sql);
        $sql = "SELECT 
					t1.finca,
                    {$years_sql}
                FROM(
                    SELECT 2015 AS years, finca, 0 AS dll, 0 AS has, 'PARCIAL' AS tipo_ciclo, 0 AS ciclos
                    FROM ciclos_aplicacion_hist c
                    WHERE 1=1 {$sWhereGerentes}
                    GROUP BY finca
                ) t1
				LEFT JOIN
				(
					SELECT finca, anio AS years, SUM(IF(UPPER(tipo_ciclo) = 'CICLO', 1, sum_ciclo)) AS fila, tipo_ciclo
					FROM ciclos_aplicacion_hist hist
					WHERE anio > 0 AND 1=1 {$sVariable} {$sWhereGerentes}
					GROUP BY finca , anio, num_ciclo, tipo_ciclo
				) AS t2 ON t1.finca = t2.finca
				GROUP BY t1.finca
                ORDER BY t1.finca DESC";
        return $this->bd->queryAll($sql);
    }

	public function getIndicadores(){ 
		$response = new stdClass;
		$response = $this->Indicadores();
		return json_encode($response);
	}

	private function getFilters(){
        $data = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->gerente = (int)$data->params->gerente;
        $response->encargado = (int)$data->params->encargado;
        $response->sem = (int)$data->params->sem;
        $response->sem2 = (int)$data->params->sem2;
        $response->year = (int)$data->params->year;
        $response->variable = $data->params->variable;
        $response->tipoHectarea = $data->params->tipoHectarea;
        $response->tipoCiclo = $data->params->tipoCiclo;
		$response->fincas = "";

		if($response->gerente > 0){
			$response->fincas = "AND finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $response->gerente)";
		}

        return $response;
    }

	private function generateSeries($sql , $filters){
        $sWhereGerentes = $filters->fincas;
        if($filters->sem > 0){
			$sWhereGerentes .= " AND semana >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND semana <= $filters->sem2";
        }
		$res = $this->bd->queryAll($sql);

        $response = new stdClass;
		$response->data = [];
		$response->legend = [];
		$response->avg = 0;
		$count = 0;
		$sum = 0;
		$series = new stdClass;
		$finca = "";
		$flag_count = 0;
		$markLine = new stdClass;
		$labels = [];

		$sql = "SELECT  TRIM(finca) AS id , TRIM(finca) AS label FROM ciclos_aplicacion_hist WHERE 1=1 {$sWhereGerentes} GROUP BY finca";
        $fincas = $this->bd->queryAllSpecial($sql);
		foreach ($fincas as $key => $value) {
			$labels[] = $value;
			$response->legend[] = $value;
		}

		// $sql = "SELECT years AS id , years AS label FROM ciclos_aplicacion_hist WHERE 1 = 1 AND years > 0 {$sWhereGerentes} GROUP BY years";
        // $years = $this->bd->queryAllSpecial($sql);
		// foreach ($years as $key => $value) {
		// }

        foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->promedio = (float)$value->value;
			$sum += $value->value;
			if(!isset($response->data[$value->legend]->itemStyle)){
				$series = new stdClass;
				$series->name = $value->legend;
				$series->type = "bar";
				$series->connectNulls = true;
				foreach($labels as $lbl){
					$series->data[] = null;
				}
				$series->data[array_search($value->label, $labels)] = round($value->value ,2);
				$series->itemStyle = (object)[
					"normal" => [
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => true , "position" => "insideTop"]
					]
				];
				$response->data[$value->legend] = $series;
			}else{
				$response->data[$value->legend]->data[array_search($value->label, $labels)] = round($value->value ,2);
			}
			$count++;
		}

		$response->avg = ($sum / $count);

        return $response;
	}

    private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = []){
		$response = new stdClass;
		$response->chart = [];
		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["orient"] = "vertical";
			$response->chart["legend"]["bottom"] = "1%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "15%";
			$response->chart["grid"]["containLabel"] = true;
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""] , "axisLabel" => ["rotate" => 60] , "margin" => 2];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value"];
			}
			$response->chart["series"] = [];
			$count = 0;
			$position = -1;
			$colors = [];
			if($type == "line"){
				$response->chart["xAxis"]["data"] = [];
			}
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = ucwords(strtolower($value->legend));
				$value->label = ucwords(strtolower($value->label));
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if("line" == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						$response->chart["xAxis"]["data"][] = $value->label;
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					}

					$response->chart["series"][$position]["data"][] = ROUND($value->value,2);

				}
                // else{
				// 	$response->chart["legend"]["data"][] = $value->label;
				// 	// if($count == 0){
				// 		$response->chart["series"][$count] = [
				// 			"name" => $value->label,
				// 			"type" => $type,
				// 			"data" => [(int)$value->value],
				// 			"label" => [
				// 				"normal" => ["show" => true , "position" => "top"],
				// 				"emphasis" => ["show" => true , "position" => "top"],
				// 			],
				// 			"itemStyle" => $colors
				// 		];
				// 	// }
				// }
				// $count++;
			}
		}

		return $response->chart;
    }
    
	private function pie($data = [] , $radius = ['50%', '70%'] , $name = "CATEGORIAS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['45%', '40%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = $this->session->sloganCompany;
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["magicType"]["show"] = false;
			$response->pie["toolbox"]["feature"]["magicType"]["type"] = ['pie'];
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["x"] = "center";
			$response->pie["legend"]["y"] = "bottom";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];
			$colors = [];
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->label = ucwords(strtolower($value->label));
				$response->pie["legend"]["data"][] = $value->label;
				if($version === 3){
					if(isset($value->color)){
							$colors = [
								"normal" => ["color" => $value->color],
							];
					}
					$response->pie["series"]["data"][] = [
                            "name" => $value->label  , 
                            "value" => $value->value != null ? (float)$value->value : $value->value, 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							],
							"itemStyle" => $colors
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value];
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value,
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
							"emphasis" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}
}

?>