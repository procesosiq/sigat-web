<?php
class Datos_Resumen_Demo {
	
	private $bd;
	private $session;
	
	public function __construct() {
        header('Content-Type: application/json');
        $this->bd = new M_Conexion();
        $this->session = Session::getInstance();
    }

	private function Indicadores(){
		$response = new stdClass;
		$filters = $this->getFilters();
		$sWhereGerentes = $filters->fincas;
		$sWhereGerent = $sWhereGerentes;
		$costo_ha ="((total_1 + total_2 + total_5 + total_7 + (ha_oper * ha)) / ha)";
        $sWhere = "";
        $sVariable .= " AND programa != 'Foliar'";
		if($filters->variable != ""){
			$sVariable .= " AND UPPER(tipo_ciclo) = '{$filters->variable}'";
		}
        if($filters->year > 0){
			$sWhere .= " AND years = $filters->year";
		}
        if($filters->sem > 0){
			$sWhereGerentes .= " AND sem >= $filters->sem";
			$sWhereSem .= " AND WEEK(fecha) >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND sem <= $filters->sem2";
			$sWhereSem .= " AND WEEK(fecha) <= $filters->sem2";
        }
        $miVar = $filters->variable == '' ? 'TODOS' : $filters->variable;
         // 12/05/2017 - TAG: GERENTES
        $sql = "SELECT id AS id , alias AS label FROM cat_gerentes WHERE status = 1";
        $response->gerentes = $this->bd->queryAll($sql);
        // 12/05/2017 - TAG: GERENTES

        // 12/05/2017 - TAG: SEMANAS
        $sql = "SELECT sem AS id , sem AS label FROM ciclos_aplicacion_historico WHERE 1=1 {$sVariable} GROUP BY sem";
        $response->semanas = $this->bd->queryAll($sql);
        // 12/05/2017 - TAG: SEMANAS

         // 12/05/2017 - TAG: FINCAS
        $sql = "SELECT finca AS id , (SELECT alias_2 FROM cat_fincas WHERE nombre = finca AND status = 1) AS label FROM ciclos_aplicacion_historico WHERE 1=1 {$sVariable} GROUP BY finca";
        $response->fincas = $this->bd->queryAll($sql);
        // 12/05/2017 - TAG: FINCAS

        // 12/05/2017 - TAG: AÑOS
		$sql = "SELECT years AS years 
				FROM ciclos_aplicacion_historico
				WHERE years > 0 AND 1=1 {$sVariable}
				GROUP BY years";
		$response->years = $this->bd->queryAll($sql);
        // 12/05/2017 - TAG: AÑOS

        // 12/05/2017 - TAG: TABLA DE CICLOS APLICADOS
        $sql = "SELECT 
					(SELECT alias_2 FROM cat_fincas WHERE nombre = t1.finca AND status = 1) AS finca,
					SUM(CASE WHEN t2.years = 2017 THEN IF(t2.tipo_ciclo='CICLO',1,t2.fila) ELSE 0 END) AS '2017',
					SUM(CASE WHEN t2.years = 2016 THEN IF(t2.tipo_ciclo='CICLO',1,t2.fila) ELSE 0 END) AS '2016',
					SUM(CASE WHEN t2.years = 2015 THEN IF(t2.tipo_ciclo='CICLO',1,t2.fila) ELSE 0 END) AS '2015'
                FROM(
                    SELECT 2015 AS years, finca, 0 AS dll, 0 AS has, 'PARCIAL' AS tipo_ciclo, 0 AS ciclos
                    FROM ciclos_aplicacion_historico c
                    WHERE 1=1 {$sWhereGerentes}
                    GROUP BY finca
                ) t1
				LEFT JOIN
				(
					SELECT finca , years AS years , SUM(IF(UPPER(tipo_ciclo) = 'CICLO', 1, ciclo)) AS fila, tipo_ciclo
					FROM ciclos_aplicacion_historico 
					WHERE years > 0 AND 1=1 {$sVariable} {$sWhereGerentes}
					GROUP BY finca , years, ciclo
				) AS t2 ON t1.finca = t2.finca
				GROUP BY t1.finca
                ORDER BY t1.finca DESC";
        
        $response->ciclos_aplicados = $this->bd->queryAll($sql);
        // 12/05/2017 - TAG: TABLA DE CICLOS APLICADOS
        
        // 12/05/2017 - TAG: TABLA DE CICLOS APLICADOS AÑO
        $sql = "SELECT
                    (SELECT alias_2 FROM cat_fincas WHERE nombre = t1.finca AND status = 1) AS finca,
                    IFNULL((SUM(IF(t2.years=2015,t2.dll,0))/COUNT(DISTINCT t2.years))/(SUM(IF(t2.years=2015,t2.has,0))/SUM(IF(t2.years=2015,t2.ciclos,0)))/(SUM(IF(t2.years=2015,t2.ciclos,0))/COUNT(DISTINCT t2.years)), 0) AS '2015',
                    IFNULL((SUM(IF(t2.years=2016,t2.dll,0))/COUNT(DISTINCT t2.years))/(SUM(IF(t2.years=2016,t2.has,0))/SUM(IF(t2.years=2016,t2.ciclos,0)))/(SUM(IF(t2.years=2016,t2.ciclos,0))/COUNT(DISTINCT t2.years)), 0) AS '2016',
                    IFNULL((SUM(IF(t2.years=2017,t2.dll,0))/COUNT(DISTINCT t2.years))/(SUM(IF(t2.years=2017,t2.has,0))/SUM(IF(t2.years=2017,t2.ciclos,0)))/(SUM(IF(t2.years=2017,t2.ciclos,0))/COUNT(DISTINCT t2.years)), 0) AS '2017'
                FROM(
                    SELECT 2015 AS years, finca, 0 AS dll, 0 AS has, 'PARCIAL' AS tipo_ciclo, 0 AS ciclos
                    FROM ciclos_aplicacion_historico c
                    WHERE 1=1 {$sWhereGerentes}
                    GROUP BY years, finca
                ) t1
                LEFT JOIN 
                (
                    SELECT years, finca, SUM(costo_total) AS dll, 
                                SUM(
                                    IF('{$filters->tipoHectarea}' = '', ha, (SELECT ".($filters->tipoHectarea ? $filters->tipoHectarea : "''")." FROM cat_fincas WHERE nombre = finca LIMIT 1))
                                ) AS has,  tipo_ciclo,
                            (SELECT COUNT(DISTINCT ciclo) FROM ciclos_aplicacion_historico WHERE tipo_ciclo = 'CICLO' AND finca = c.finca AND years = c.years AND programa != 'Foliar') AS ciclos
                    FROM ciclos_aplicacion_historico c
                    WHERE tipo_ciclo = 'CICLO' {$sVariable} {$sWhereGerentes}
                    GROUP BY years, finca
                    UNION ALL
                    SELECT years, finca, SUM(costo_total) AS dll, 
                            SUM(
                                IF('{$filters->tipoHectarea}' = '', ha, (SELECT ".($filters->tipoHectarea ? $filters->tipoHectarea : "''")." FROM cat_fincas WHERE nombre = finca LIMIT 1))
                            ) AS has,  tipo_ciclo,
                            (SELECT SUM(ciclo) FROM ciclos_aplicacion_historico WHERE tipo_ciclo = 'PARCIAL' AND finca = c.finca and years = c.years AND programa != 'Foliar') AS ciclos
                    FROM ciclos_aplicacion_historico c
                    WHERE tipo_ciclo = 'PARCIAL' {$sVariable} {$sWhereGerentes}
                    GROUP BY years, finca
                ) AS t2 ON t1.finca = t2.finca
                GROUP BY t1.finca";

        $response->ciclos_aplicados_anio = $this->bd->queryAll($sql);
        $response->total_ciclos_aplicados_anio = $this->bd->queryAll("SELECT 
                (dll_2015 / (has_2015 / ciclos_2015) / ciclos_2015) AS '2015',
                (dll_2016 / (has_2016 / ciclos_2016) / ciclos_2016) AS '2016',
                (dll_2017 / (has_2017 / ciclos_2017) / ciclos_2017) AS '2017'
            FROM(
            SELECT
                finca,
                SUM(IF(years=2015,dll,0)) AS dll_2015,
                SUM(IF(years=2015,has,0)) AS has_2015,
                SUM(IF(years=2015,ciclos,0)) AS ciclos_2015,
                
                SUM(IF(years=2016,dll,0)) AS dll_2016,
                SUM(IF(years=2016,has,0)) AS has_2016,
                SUM(IF(years=2016,ciclos,0)) AS ciclos_2016,
                
                SUM(IF(years=2017,dll,0)) AS dll_2017,
                SUM(IF(years=2017,has,0)) AS has_2017,
                SUM(IF(years=2017,ciclos,0)) AS ciclos_2017
            FROM
            (
                SELECT years, finca, SUM(costo_total) AS dll, 
                            SUM(
                                IF('{$filters->tipoHectarea}' = '', ha, (SELECT ".($filters->tipoHectarea ? $filters->tipoHectarea : "''")." FROM cat_fincas WHERE nombre = finca LIMIT 1))
                            ) AS has,  tipo_ciclo,
                        (SELECT COUNT(DISTINCT ciclo) FROM ciclos_aplicacion_historico WHERE tipo_ciclo = 'CICLO' AND finca = c.finca AND years = c.years) AS ciclos
                FROM ciclos_aplicacion_historico c
                WHERE tipo_ciclo = 'CICLO'  {$sWhereGerentes} {$sVariable}
                GROUP BY years, finca
                UNION ALL
                SELECT years, finca, SUM(costo_total) AS dll, 
                        SUM(
                            IF('{$filters->tipoHectarea}' = '', ha, (SELECT ".($filters->tipoHectarea ? $filters->tipoHectarea : "''")." FROM cat_fincas WHERE nombre = finca LIMIT 1))
                        ) AS has,  tipo_ciclo,
                        (SELECT SUM(ciclo) FROM ciclos_aplicacion_historico WHERE tipo_ciclo = 'PARCIAL' AND finca = c.finca and years = c.years) AS ciclos
                FROM ciclos_aplicacion_historico c
                WHERE tipo_ciclo = 'PARCIAL' {$sWhereGerentes} {$sVariable}
                GROUP BY years, finca
            ) AS totalRows) AS tbl")[0];
        // 12/05/2017 - TAG: TABLA DE CICLOS APLICADOS AÑO

        // 12/05/2017 - TAG: TABLA DE CICLOS APLICADOS AÑO
        $sql = "SELECT 
                    (SELECT alias_2 FROM cat_fincas WHERE nombre = finca AND status = 1) AS finca,
                    ROUND(SUM(CASE WHEN years = 2015 THEN total ELSE 0 END)/(SUM(CASE WHEN years = 2015 THEN has ELSE 0 END)/SUM(CASE WHEN years = 2015 THEN ciclos ELSE 0 END)), 2) AS '2015',
                    ROUND(SUM(CASE WHEN years = 2016 THEN total ELSE 0 END)/(SUM(CASE WHEN years = 2016 THEN has ELSE 0 END)/SUM(CASE WHEN years = 2016 THEN ciclos ELSE 0 END)), 2) AS '2016',
                    ROUND(SUM(CASE WHEN years = 2017 THEN total ELSE 0 END)/(SUM(CASE WHEN years = 2017 THEN has ELSE 0 END)/SUM(CASE WHEN years = 2017 THEN ciclos ELSE 0 END)), 2) AS '2017'
                FROM
                (
                    SELECT finca, years, SUM(costo_total) AS total, 
                            SUM(
                                IF('{$filters->tipoHectarea}' = '', ha, (SELECT ".($filters->tipoHectarea ? $filters->tipoHectarea : "''")." FROM cat_fincas WHERE nombre = finca LIMIT 1))
                            ) AS has,  COUNT(DISTINCT ciclo) AS ciclos, 'CICLO' AS tipo_ciclo
                    FROM ciclos_aplicacion_historico 
                    WHERE years > 0 AND 1=1 AND tipo_ciclo = 'CICLO' {$sWhereGerentes} {$sVariable}
                    GROUP BY finca , years 
                    UNION ALL
                    SELECT finca, years, SUM(costo_total) AS total, 
                            SUM(
                                IF('{$filters->tipoHectarea}' = '', ha, (SELECT ".($filters->tipoHectarea ? $filters->tipoHectarea : "''")." FROM cat_fincas WHERE nombre = finca LIMIT 1))
                            ) AS has,  SUM(ciclo) AS ciclos, 'PARCIAL' AS tipo_ciclo
                    FROM ciclos_aplicacion_historico 
                    WHERE years > 0 AND 1=1 AND tipo_ciclo = 'PARCIAL' {$sWhereGerentes} {$sVariable}
                    GROUP BY finca , years 
                ) AS tbl
                GROUP BY finca";

        $response->ciclos_aplicados_sum_anio = $this->bd->queryAll($sql);

        $sql = "SELECT 
                    SUM(IF(years = 2015,dll,0))/SUM(IF(years = 2015,has,0)) AS '2015',
                    SUM(IF(years = 2016,dll,0))/SUM(IF(years = 2016,has,0)) AS '2016',
                    SUM(IF(years = 2017,dll,0))/SUM(IF(years = 2017,has,0)) AS '2017'
                FROM(
                
                SELECT years, finca, SUM(costo_total) AS dll, 
                            SUM(
                                IF('{$filters->tipoHectarea}' = '', ha, (SELECT ".($filters->tipoHectarea ? $filters->tipoHectarea : "''")." FROM cat_fincas WHERE nombre = finca LIMIT 1))
                            )/COUNT(DISTINCT ciclo) AS has
                FROM ciclos_aplicacion_historico
                WHERE tipo_ciclo = 'CICLO' AND years IN (2015, 2016, 2017) {$sWhereGerentes}
                GROUP BY finca, years
                
                ) AS tbl";
        $response->total_ciclos_aplicados_sum_anio = $this->bd->queryAll($sql)[0];
        // 12/05/2017 - TAG: TABLA DE CICLOS APLICADOS AÑO

        // 12/05/2017 - TAG: GRAFICA CICLOS APLICADOS
        $sql = "SELECT (SELECT alias_2 FROM cat_fincas WHERE nombre = TRIM(finca) AND status = 1) AS label , years AS legend,COUNT(IF(UPPER(tipo_ciclo) = 'CICLO', 1, ciclo)) AS value , IF(years > 2013 ,true,false) AS selected
                FROM ciclos_aplicacion_historico
				WHERE years > 0 AND 1=1 {$sVariable} {$sWhereGerentes}
                GROUP BY years , finca
                ORDER BY fecha_real ASC  , finca";
        // $response->ciclos_aplicados_chart = (object)$this->chartInit($data,"vertical","Ciclos Aplicados","bar");
        $response->ciclos_aplicados_chart = $this->generateSeries($sql , $filters);
        // 12/05/2017 - TAG: GRAFICA CICLOS APLICADOS

        // 12/05/2017 - TAG: GRAFICA CICLOS APLICADOS AÑOS SUM
        $sql = "SELECT (SELECT alias_2 FROM cat_fincas WHERE nombre = TRIM(finca) AND status = 1) AS label , years AS legend ,IFNULL(SUM($costo_ha),0) AS value , IF(years > 2013 ,true,false) AS selected
                FROM ciclos_aplicacion_historico
				WHERE years > 0 AND 1=1 {$sVariable} {$sWhereGerentes}
                GROUP BY years , finca
                ORDER BY fecha_real ASC  , finca";
		// print_r($sql);
		#left join ciclos_aplicacion_historico_economico ON ciclos_aplicacion_historico.id = ciclos_aplicacion_historico_economico.id_ciclo_aplicacion

        $data = $this->bd->queryAll($sql);
        $response->ciclos_aplicados_sum_chart = $this->generateSeries($sql , $filters);
        // $response->ciclos_aplicados_sum_chart =(object)$this->chartInit($data,"vertical","HECTAREA/AÑO","bar");
        // 12/05/2017 - TAG: GRAFICA CICLOS APLICADOS AÑOS SUM

         // 12/05/2017 - TAG: GRAFICA CICLOS APLICADOS AÑOS AVG
        $sql = "SELECT (SELECT alias_2 FROM cat_fincas WHERE nombre = TRIM(finca) AND status = 1) AS label , years AS legend ,IFNULL(AVG($costo_ha),0) AS value , IF(years > 2013 ,true,false) AS selected
                FROM ciclos_aplicacion_historico
				WHERE years > 0 AND 1=1 {$sVariable} {$sWhereGerentes}
                GROUP BY years , finca
                ORDER BY fecha_real ASC  , finca";
		#left join ciclos_aplicacion_historico_economico ON ciclos_aplicacion_historico.id = ciclos_aplicacion_historico_economico.id_ciclo_aplicacion
        $data = $this->bd->queryAll($sql);
        $response->ciclos_aplicados_avg_chart = $this->generateSeries($sql , $filters);
        // $response->ciclos_aplicados_avg_chart =(object)$this->chartInit($data,"vertical","HECTAREA/CICLO","bar");
        // 12/05/2017 - TAG: GRAFICA CICLOS APLICADOS AÑOS AVG
		
		/* TABLE FRAC */
		
        $fincas_frac = $this->bd->queryAll("SELECT (SELECT alias_2 FROM cat_fincas WHERE nombre = TRIM(finca) AND status = 1) AS finca, finca AS finca_dvd
            FROM ciclos_aplicacion_detalle
            INNER JOIN products ON products.id = id_producto
            LEFT JOIN cat_frac ON products.frac = cat_frac.nombre
            WHERE YEAR(fecha) = $filters->year
                AND UPPER(tipo_ciclo) = 'CICLO'
                AND products.id_tipo_producto = 4 {$sWhereSem} {$sWhereGerent}
            GROUP BY finca");
        $grupos = $this->bd->queryAll("SELECT nombre FROM cat_frac WHERE frac > 0 ORDER BY frac DESC");
        foreach($fincas_frac as $finca){
            $saldos = $this->bd->queryAllSpecial("SELECT (cat_frac.frac - COUNT(1)) AS label, cat_frac.`nombre` AS id
                FROM ciclos_aplicacion_detalle
                INNER JOIN products ON products.id = id_producto
                LEFT JOIN cat_frac ON products.frac = cat_frac.nombre
                WHERE YEAR(fecha) = 2017
                    AND UPPER(tipo_ciclo) = 'CICLO'
                    AND products.id_tipo_producto = 4
                    AND finca = '{$finca->finca_dvd}'
                    AND cat_frac.`frac` > 0 {$sWhereSem} {$sWhereGerent}
                GROUP BY cat_frac.`id`");
            foreach($grupos as $value){
                $finca->{$value->nombre} = $saldos[$value->nombre];
            }
        }
        $response->table_frac = $fincas_frac;


        // 12/05/2017 - TAG: CICLOS ATRASADOS
        $sql = "SELECT ciclo FROM ciclos_aplicacion_historico WHERE ciclo > 0 AND UPPER(tipo_ciclo) = 'CICLO' {$sWhereGerentes} {$sWhere} GROUP BY TRIM(ciclo)";
        $response->ciclos = $this->bd->queryAll($sql);
        // 12/05/2017 - TAG: CICLOS ATRASADOS


        // 12/05/2017 - TAG: CICLOS ATRASADOS DATA
        $sql = "SELECT (SELECT alias_2 FROM cat_fincas WHERE nombre = TRIM(finca) AND status = 1) AS finca , ciclo , DATEDIFF(fecha_prog, fecha_real) AS atraso
				FROM ciclos_aplicacion_historico 
				WHERE ciclo > 0 AND UPPER(tipo_ciclo) = 'CICLO' {$sWhereGerentes} {$sWhere} AND programa IN ('Sigatoka', 'Sigatoka Foliar')
				ORDER BY finca , ciclo";
		#left join ciclos_aplicacion_historico_tecnico ON  ciclos_aplicacion_historico.id = ciclos_aplicacion_historico_tecnico.id_ciclo_aplicacion

        $data = $this->bd->queryAll($sql);
        $response->data_ciclos = [];
        $bgColor = "";
        $atraso = 0;
        foreach ($data as $key => $value) {
            $atraso = (int)$value->atraso;
            $response->data_ciclos[$value->finca]["finca"] = $value->finca;
            $response->data_ciclos[$value->finca]["ciclos"][$value->ciclo] = (int)$atraso;
            if($atraso < 0){
                $bgColor = "bg-red-thunderbird bg-font-red-thunderbird";
            }else if($atraso == 0){
                $bgColor = "bg-green-haze bg-font-green-haze";
            }else{
                $bgColor = "bg-yellow-gold bg-font-yellow-gold";
            }
            $response->data_ciclos[$value->finca]["className"][$value->ciclo] = $bgColor;
        }
        // 12/05/2017 - TAG: CICLOS ATRASADOS DATA

        // 12/05/2017 - TAG: MOTIVO
        $sql = "SELECT motivo AS label , COUNT(motivo) AS value FROM ciclos_aplicacion_historico WHERE 1=1 {$sVariable} {$sWhereGerentes} {$sWhere} GROUP BY motivo";
        $data = $this->bd->queryAll($sql);
        $response->motivo = $this->pie($data , ['0%', '50%'] , "");
        // 12/05/2017 - TAG: MOTIVO

        return $response;
	}

	public function getIndicadores(){ 
		$response = new stdClass;
		$response = $this->Indicadores();
		return json_encode($response);
	}

	private function getFilters(){
        $data = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->gerente = (int)$data->params->gerente;
        $response->sem = (int)$data->params->sem;
        $response->sem2 = (int)$data->params->sem2;
        $response->year = (int)$data->params->year;
        $response->variable = $data->params->variable;
		$response->fincas = "";

		if($response->gerente > 0){
			$response->fincas = "AND finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $response->gerente)";
		}

        return $response;
    }


	private function generateSeries($sql , $filters){
        $sWhereGerentes = $filters->fincas;
        if($filters->sem > 0){
			$sWhereGerentes .= " AND sem >= $filters->sem";
        }
        if($filters->sem2 > 0){
			$sWhereGerentes .= " AND sem <= $filters->sem2";
        }
		$res = $this->bd->queryAll($sql);

		$response->data = [];
		$response->legend = [];
		$response->avg = 0;
		$count = 0;
		$sum = 0;
		$series = new stdClass;
		$finca = "";
		$flag_count = 0;
		$markLine = new stdClass;
		$labels = [];

		$sql = "SELECT  TRIM(finca) AS id , (SELECT alias_2 FROM cat_fincas WHERE nombre = TRIM(finca) AND status = 1) AS label FROM ciclos_aplicacion_historico WHERE 1=1 {$sWhereGerentes} GROUP BY finca";
        $fincas = $this->bd->queryAllSpecial($sql);
		foreach ($fincas as $key => $value) {
			$labels[] = $value;
			$response->legend[] = $value;
		}

		// $sql = "SELECT years AS id , years AS label FROM ciclos_aplicacion_historico WHERE 1 = 1 AND years > 0 {$sWhereGerentes} GROUP BY years";
        // $years = $this->bd->queryAllSpecial($sql);
		// foreach ($years as $key => $value) {
		// }

        foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->promedio = (float)$value->value;
			$sum += $value->value;
			if(!isset($response->data[$value->legend]->itemStyle)){
				$series = new stdClass;
				$series->name = $value->legend;
				$series->type = "bar";
				$series->connectNulls = true;
				foreach($labels as $lbl){
					$series->data[] = null;
				}
				$series->data[array_search($value->label, $labels)] = round($value->value ,2);
				$series->itemStyle = (object)[
					"normal" => [
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => true , "position" => "insideTop"]
					]
				];
				$response->data[$value->legend] = $series;
			}else{
				$response->data[$value->legend]->data[array_search($value->label, $labels)] = round($value->value ,2);
			}
			$count++;
		}

		$response->avg = ($sum / $count);

        return $response;
	}

    private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = []){
		$response = new stdClass;
		$response->chart = [];
		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["orient"] = "vertical";
			$response->chart["legend"]["bottom"] = "1%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "15%";
			$response->chart["grid"]["containLabel"] = true;
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""] , "axisLabel" => ["rotate" => 60] , "margin" => 2];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value"];
			}
			$response->chart["series"] = [];
			$count = 0;
			$position = -1;
			$colors = [];
			if($type == "line"){
				$response->chart["xAxis"]["data"] = [];
			}
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = ucwords(strtolower($value->legend));
				$value->label = ucwords(strtolower($value->label));
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if("line" == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						$response->chart["xAxis"]["data"][] = $value->label;
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					}

					$response->chart["series"][$position]["data"][] = ROUND($value->value,2);

				}
                // else{
				// 	$response->chart["legend"]["data"][] = $value->label;
				// 	// if($count == 0){
				// 		$response->chart["series"][$count] = [
				// 			"name" => $value->label,
				// 			"type" => $type,
				// 			"data" => [(int)$value->value],
				// 			"label" => [
				// 				"normal" => ["show" => true , "position" => "top"],
				// 				"emphasis" => ["show" => true , "position" => "top"],
				// 			],
				// 			"itemStyle" => $colors
				// 		];
				// 	// }
				// }
				// $count++;
			}
		}

		return $response->chart;
	}
	private function pie($data = [] , $radius = ['50%', '70%'] , $name = "CATEGORIAS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['45%', '40%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = $this->session->sloganCompany;
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["magicType"]["show"] = false;
			$response->pie["toolbox"]["feature"]["magicType"]["type"] = ['pie'];
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["x"] = "center";
			$response->pie["legend"]["y"] = "bottom";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];
			$colors = [];
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->label = ucwords(strtolower($value->label));
				$response->pie["legend"]["data"][] = $value->label;
				if($version === 3){
					if(isset($value->color)){
							$colors = [
								"normal" => ["color" => $value->color],
							];
					}
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value , 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							],
							"itemStyle" => $colors
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value];
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value,
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
							"emphasis" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}
}

?>