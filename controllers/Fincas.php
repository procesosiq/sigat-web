<?php
	/**
	*  CLASS FROM Fincas
	*/
	class Fincas 
	{
		private $conexion;
		private $session;
		private $Prontoforms;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();

		}

		public function index(){
			$sWhere = "";
			$sOrder = " ORDER BY id";
			$DesAsc = "ASC";
			$sOrder .= " {$DesAsc}";
			$sLimit = "";
			if(isset($_POST)){

				/*----------  ORDER BY ----------*/
				
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY cat_fincas.id {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY cat_fincas.fecha {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY cat_fincas.nombre {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY cat_gerentes.nombre {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY cat_fincas.status {$DesAsc}";
				}
				/*----------  ORDER BY ----------*/

				if(isset($_POST['search_id']) && trim($_POST['search_id']) != ""){
					$sWhere .= " AND cat_fincas.id = ".$_POST["search_id"];
				}				
				if((isset($_POST['search_date_from']) && trim($_POST['search_date_from']) != "") && (isset($_POST['search_date_to']) && trim($_POST['search_date_to']) != "")){
					$sWhere .= " AND cat_fincas.fecha BETWEEN '".$_POST["search_date_from"]."' AND '".$_POST["search_date_to"]."'";
				}
				if(isset($_POST['search_name']) && trim($_POST['search_name']) != ""){
					$sWhere .= " AND cat_fincas.nombre LIKE '%".$_POST['search_name']."%'";
				}
				if(isset($_POST['search_gerente']) && trim($_POST['search_gerente']) != ""){
					$sWhere .= " AND cat_gerentes.nombre LIKE '%".$_POST['search_gerente']."%'";
				}
				if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
					$sWhere .= " AND cat_fincas.status = ".$_POST["order_status"];
				}

				/*----------  LIMIT  ----------*/
				if(isset($_POST['length']) && $_POST['length'] > 0){
					$sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
				}
			}

			$sql = "SELECT cat_fincas.*,
						cat_gerentes.nombre as gerente
					FROM cat_fincas 
					LEFT JOIN cat_gerentes ON id_gerente = cat_gerentes.id
					WHERE 1 = 1 $sWhere $sOrder $sLimit";
			$res = $this->conexion->link->query($sql);
			$datos = (object)[
				"customActionMessage" => "Error al consultar la informacion",
				"customActionStatus" => "Error",
				"data" => [],
				"draw" => 0,
				"recordsFiltered" => 0,
				"recordsTotal" => 0,
			];
			$count = 0;
			while($fila = $res->fetch_assoc()){
				$fila = (object)$fila;
				$count++;
				$datos->data[] = [
					$fila->fecha,
					$fila->id,
					$fila->nombre,
					$fila->status,
					$fila->gerente,
					'<button class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
				];
			}

			$datos->recordsTotal = count($datos->data);
			$datos->customActionMessage = "Informacion completada con exito";
			$datos->customActionStatus = "OK";

			return json_encode($datos);
		}

		public function create(){
			extract($_POST);
			if($txtnom != "" && $id_gerente_selected != ""){
				$sql = "INSERT INTO cat_fincas SET 
				nombre = '{$txtnom}' , 
				id_gerente = '{$id_gerente_selected}',
				h_produccion = '{$h_produccion}',
				h_banano = '{$h_banano}',
				h_neta = '{$h_neta}',
				fecha = CURRENT_DATE , 
				id_usuario = '{$this->session->logged}'";
				$ids = (int)$this->conexion->Consultas(1,$sql);
				if($ids > 0){
					$this->conexion->Consultas(1, "DELETE FROM gerentes_fincas_asignadas WHERE id_finca = {$ids}");
					if((int)$params->id_gerente_selected > 0){
						$this->conexion->Consultas(1, "INSERT INTO gerentes_fincas_asignadas 
							SET id_finca = {$ids}, id_gerente = {$params->id_gerente_selected}");
					}
				}
				$this->changeProntoforms();
        		return $ids;
			}
		}

		public function update(){
			$params = (object) $_POST;

			if((int)$params->id > 0 && $params->txtnom != ""){

				$id_gerente = (int)$params->id_gerente_selected;
				$sql = "UPDATE cat_fincas SET 
					nombre = '{$params->txtnom}' , 
					fecha = CURRENT_DATE ,
					h_produccion = '{$params->h_produccion}',
					h_neta = '{$params->h_neta}',
					h_banano = '{$params->h_banano}',
					id_gerente = '{$id_gerente}'
					WHERE id = {$params->id} ";
				$this->conexion->Consultas(1,$sql);
				$this->changeProntoforms();
        		return $params->id;
			}
		}


		public function changeStatus(){
			extract($_POST);
			if((int)$id > 0 ){
				$status = ($estado == 'activo') ? 1 : 0;
				$sql = "UPDATE cat_fincas SET status={$status} WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				// echo $sql;
				$this->conexion->Consultas(1,$sql);
				$this->changeProntoforms();
        		return $id;
			}
		}

		public function edit(){
			$response = (object)[
				"success" => 400,
				"data" => [],
				"clientes" => [],
				"agrupaciones" => [],
				"lotes" => [],
			];
			$id = (int)$_GET['id'];
			$response->clientes = $this->getClient();
			$response->gerentes = $this->conexion->queryAll("SELECT id, nombre, 
				IFNULL((SELECT 'selected' FROM cat_fincas WHERE id = '{$id}' AND id_gerente = cat_gerentes.id), '') as selected 
				FROM cat_gerentes WHERE status > 0");
			if(isset($_GET['id'])){
				$response->agrupaciones = $this->getAgrupaciones($id);
				if($id > 0){
					$sql = "SELECT * FROM cat_fincas WHERE id = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->data = (object)$res->fetch_assoc();
					}
					$response->sectores = $this->getSectores($id);
					$response->lotes = $this->getLotesFinca($id);
					$response->success = 200;
				}
			}
			return json_encode($response);
		}

		public function addSector(){
			$params = (object)$_POST;
			$sectores = [];
			if((int)$params->id_finca > 0 && $params->sector != ""){
				$sql = "INSERT INTO fincas_sectores SET id_finca = '{$params->id_finca}', sector = '{$params->sector}', hec_fumigacion = '{$params->fumigacion}', hec_produccion = '{$params->produccion}', hec_neta = '{$params->neta}'";
				$this->conexion->Consultas(1,$sql);
				$this->changeProntoforms();
				$sectores = $this->getSectores($params->id_finca);
			}
			return json_encode($sectores);
		}

		public function addLoteFinca(){
			$params = (object)$_POST;
			$sectores = [];
			if((int)$params->id_finca > 0 && $params->lote != ""){
				$sql = "INSERT INTO fincas_lotes SET id_finca = '{$params->id_finca}', lote = '{$params->lote}', hec_fumigacion = '{$params->fumigacion}', hec_produccion = '{$params->produccion}', hec_neta = '{$params->neta}'";
				$this->conexion->Consultas(1,$sql);
				$this->changeProntoforms();
				$sectores = $this->getLotesFinca($params->id_finca);
			}
			return json_encode($sectores);
		}

		public function getLotesFinca($id_finca = 0){
			$params = (object) $_POST;
			if($id_finca > 0) $params->id_finca = $id_finca;

			$lotes = [];
			if((int)$params->id_finca > 0){
				$sql = "SELECT * FROM fincas_lotes WHERE id_finca = '{$params->id_finca}'";
				$lotes = $this->conexion->queryAll($sql);
			}
			return $lotes;
		}

		public function getSectores($id_finca = 0){
			$params = (object) $_POST;
			if($id_finca > 0)
				$params->id_finca = $id_finca;
			$sectores = [];
			if((int)$params->id_finca > 0){
				$sql = "SELECT * FROM fincas_sectores WHERE id_finca = '{$params->id_finca}'";
				$sectores = $this->conexion->queryAll($sql);
			}
			return $sectores;
		}

		public function removeSector(){
			$params = (object) $_POST;
			$sectores = [];
			if((int)$params->id_finca > 0 && (int)$params->id_sector > 0){
				$sql = "DELETE FROM fincas_sectores WHERE id_finca = '{$params->id_finca}' AND id = '{$params->id_sector}'";
				$this->conexion->Consultas(1,$sql);
				$this->changeProntoforms();
				$sectores = $this->getSectores($params->id_finca);
			}
			return json_encode($sectores);
		}

		public function removeLoteFinca(){
			$params = (object) $_POST;
			$sectores = [];
			if((int)$params->id_finca > 0 && (int)$params->id_lote > 0){
				$sql = "DELETE FROM fincas_lotes WHERE id_finca = '{$params->id_finca}' AND id = '{$params->id_lote}'";
				$this->conexion->Consultas(1,$sql);
				$this->changeProntoforms();
				$sectores = $this->getLotesFinca($params->id_finca);
			}
			return json_encode($sectores);
		}

		public function positionLote(){
			extract($_POST);
			$lotes = [];
			if(is_array($id_lotes) && count($id_lotes) > 0){
				foreach ($id_lotes as $key => $value) {
					$sql = "UPDATE cat_lotes SET position={$key} 
						WHERE id = '{$value}' AND id_usuario = '{$this->session->logged}' AND id_hacienda = '{$id}'";
					$this->conexion->Consultas(1,$sql);
				}
				$this->changeProntoforms();
			}
			$lotes = $this->getLotes($id);
			return json_encode($lotes);
		}

		public function removeLote(){
			extract($_POST);
			$lotes = [];
			if((int)$id > 0 && $id_cliente != "" && $id_lote> 0){
				$sql = "UPDATE cat_lotes SET status=0 WHERE id = {$id_lote} AND id_usuario = '{$this->session->logged}' AND id_hacienda = '{$id}'";
				$this->conexion->Consultas(1,$sql);
				$this->changeProntoforms();
			}
			$lotes = $this->getLotes($id);
			return json_encode($lotes);
		}

		private function getLotes($id_hacienda){
			$lotes = [];
			if((int)$id_hacienda > 0 && $id_hacienda != ""){
				$sql="SELECT 
					id,
					(SELECT nombre FROM cat_agrupaciones WHERE id = id_agrupacion) AS agrupacion, 
					nombre, 
					area 
				FROM cat_lotes  
				WHERE id_usuario = '{$this->session->logged}' AND id_hacienda = '{$id_hacienda}' AND status > 0
				ORDER BY position";
				$res = $this->conexion->link->query($sql);
				while($fila = $res->fetch_assoc()){
					$lotes[] = (object)$fila;
				}
			}
			return $lotes;
		}

		private function getClient(){
			$cliente = [];
			$sql = "SELECT id , nombre FROM cat_clientes WHERE id_usuario = '{$this->session->logged}' AND status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$cliente[] = (object)$fila;
			}
			return $cliente;
		}

		private function getAgrupaciones($id_hacienda){
			$cliente = [];
			$sql = "SELECT id , nombre FROM cat_agrupaciones WHERE id_usuario = '{$this->session->logged}' AND id_hacienda = '{$id_hacienda}' AND status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$cliente[] = (object)$fila;
			}

			return $cliente;
		}

		private function changeProntoforms(){
            include ("ProntoformsOrigenes.php");
            $Prontoforms = new ProntoformsOrigenes();
            $Prontoforms->actualizarFincas();
        }
	}
?>
