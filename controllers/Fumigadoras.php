<?php
	/**
	*  CLASS FROM Fincas
	*/
	class Fumigadoras 
	{
		private $conexion;
		private $session;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		public function index(){
			$sWhere = "";
			$sOrder = " ORDER BY id";
			$DesAsc = "ASC";
			$sOrder .= " {$DesAsc}";
			$sLimit = "";
			if(isset($_POST)){

				/*----------  ORDER BY ----------*/
				
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY programas.id {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY programas.nombre {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY status {$DesAsc}";
				}
				/*----------  ORDER BY ----------*/

				if(isset($_POST['search_id']) && trim($_POST['search_id']) != ""){
					$sWhere .= " AND id = ".$_POST["search_id"];
				}				
				if(isset($_POST['search_name']) && trim($_POST['search_name']) != ""){
					$sWhere .= " AND programas.nombre LIKE '%".$_POST['search_name']."%'";
				}
				if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
					$sWhere .= " AND status = ".$_POST["order_status"];
				}

				/*----------  LIMIT  ----------*/
				if(isset($_POST['length']) && $_POST['length'] > 0){
					$sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
				}
			}

			$sql = "SELECT programas.id , programas.nombre  ,
					programas.status,programas.id_usuario
					FROM cat_fumigadoras AS programas
					-- WHERE programas.id_usuario = '{$this->session->logged}' 
					$sWhere 
					GROUP BY programas.id 
					$sOrder
					$sLimit";
			$res = $this->conexion->link->query($sql);
			$datos = (object)[
				"customActionMessage" => "Error al consultar la informacion",
				"customActionStatus" => "Error",
				"data" => [],
				"draw" => 0,
				"recordsFiltered" => 0,
				"recordsTotal" => 0,
			];
			$count = 0;
			while($fila = $res->fetch_assoc()){
				$fila = (object)$fila;
				$count++;
				$datos->data[] = [
					$count,
					$fila->id,
					$fila->nombre,
					$fila->status,
					$fila->id_usuario,
					'<button class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
				];
			}

			$datos->recordsTotal = count($datos->data);
			$datos->customActionMessage = "Informacion completada con exito";
			$datos->customActionStatus = "OK";

			return json_encode($datos);
		}

		public function create(){
			extract($_POST);
			if($txtnom != ""){
				$sql = "INSERT INTO cat_fumigadoras SET 
				nombre = '{$txtnom}' , 
				id_usuario = '{$this->session->logged}'";
				$ids = $this->conexion->Consultas(1,$sql);
				$this->changeProntoforms();
        		return $ids;
			}
		}

		public function update(){
			extract($_POST);
			if((int)$id > 0 && $txtnom != ""){
				$sql = "UPDATE cat_fumigadoras SET 
					nombre = '{$txtnom}' 
					WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				$this->conexion->Consultas(1,$sql);
				$this->changeProntoforms();
        		return $id;
			}
		}

		public function changeStatus(){
			extract($_POST);
			if((int)$id > 0 ){
				$status = ($estado == 'activo') ? 1 : 0;
				$sql = "UPDATE cat_fumigadoras SET status={$status} WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				// echo $sql;
				$this->conexion->Consultas(1,$sql);
				$this->changeProntoforms();
        		return $id;
			}
		}

		public function edit(){
			$response = (object)[
				"success" => 400,
				"data" => [],
				"clientes" => [],
				"fincas" => [],
			];
			if(isset($_GET['id'])){
				$id = (int)$_GET['id'];
				if($id > 0){
					$sql = "SELECT * FROM cat_fumigadoras WHERE  id = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->data = (object)$res->fetch_assoc();
					}
					$response->fincas = $this->getFincas();
					$response->success = 200;
				}else{
					$response->fincas = [];
				}
			}else{
				$response->fincas = [];
			}
			return json_encode($response);
		}

		private function getFincas(){
			$sql = "SELECT id , nombre AS label FROM cat_fincas WHERE status > 0";
			$response = $this->conexion->queryAllSpecial($sql);
			return $response;
		}

		public function params(){
			$data = (object)$_POST;
			$data->id = (int)$data->id;
			if(isset($data->precio)){
				$data->precio = (double)$data->precio;
			}
			return $data;
		}

		public function getPrecios(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = [];
            $data = $this->params();

			if($data->id > 0){
                // AIFA
                $group = 'galones';
                if($data->id == 1) $group = 'id_finca';

                $sql = "SELECT p.id, id_finca, f.nombre AS finca, galones, precio FROM cat_fumigadoras_prices p INNER JOIN cat_fincas f ON id_finca = f.id WHERE id_fumigadora = $data->id $sWhere GROUP BY {$group}";
				$res = $this->conexion->link->query($sql);
				while($fila = $res->fetch_assoc()){
					$fila = (object)$fila;
					$fila->precio = (double)$fila->precio;
					$fila->id = (int)$fila->id;
					$response->data[] = $fila;
				}
			}

			return json_encode($response);
		}

		public function savePrecios(){
			$response = new stdClass;
			$data = $this->params();
			if($data->id > 0 && $data->precio > 0){
				$fecha = "CURRENT_DATE";
				if($data->fecha != ""){
					$fecha = "'{$data->fecha}'";
                }

                if($data->id == 1){
                    // AIFA REPLICAR FINCA A 5 Y 6 GALONES
                    $sql = "INSERT INTO cat_fumigadoras_prices SET id_fumigadora = {$data->id} , id_finca = {$data->id_finca}, galones = 5, precio = '{$data->precio}'";
                    $res = $this->conexion->link->query($sql);
                    $sql = "INSERT INTO cat_fumigadoras_prices SET id_fumigadora = {$data->id} , id_finca = {$data->id_finca}, galones = 6, precio = '{$data->precio}'";
                    $res = $this->conexion->link->query($sql);
                }else{
                    // REPLICAR GALONES A TODAS LAS FINCAS
                    $fincas = $this->getFincas();
                    foreach($fincas as $id_finca => $finca){
                        $sql = "INSERT INTO cat_fumigadoras_prices SET id_fumigadora = {$data->id} , id_finca = {$id_finca}, galones = {$data->galones}, precio = '{$data->precio}'";
                        $res = $this->conexion->link->query($sql);
                    }
                }
                $this->changeProntoforms();
				return $this->getPrecios();
			}
			return json_encode(["error" => 500]);
		}

		public function dropPrecios(){
			$response = new stdClass;
			$data = $this->params();
			if($data->id > 0){
                if($data->id == 1){
                    $sql = "DELETE FROM cat_fumigadoras_prices WHERE id_fumigadora = $data->id AND id_finca = {$data->finca}";
                    $res = $this->conexion->link->query($sql);
                }else{
                    $sql = "DELETE FROM cat_fumigadoras_prices WHERE id_fumigadora = $data->id AND galones = {$data->galones}";
                    $res = $this->conexion->link->query($sql);
                }
				
                $this->changeProntoforms();
			}
			return $this->getPrecios();
		}

		private function changeProntoforms(){
            include ("ProntoformsOrigenes.php");
            $Prontoforms = new ProntoformsOrigenes();
            $Prontoforms->actualizarFumigadoras();
            $Prontoforms->actualizarPreciosFumigadoras();
        }
	}
?>
