<?php
	/**
	*  CLASS FROM Fincas
	*/
	class Haciendas 
	{
		private $conexion;
		private $session;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		public function index(){
			$sWhere = "";
			$sOrder = " ORDER BY id";
			$DesAsc = "ASC";
			$sOrder .= " {$DesAsc}";
			$sLimit = "";
			if(isset($_POST)){

				/*----------  ORDER BY ----------*/
				
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 0){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY id {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY nombre {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY (SELECT nombre from cat_clientes WHERE id = cat_haciendas.id_cliente) {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY status {$DesAsc}";
				}
				/*----------  ORDER BY ----------*/

				if(isset($_POST['search_id']) && trim($_POST['search_id']) != ""){
					$sWhere .= " AND id = ".$_POST["search_id"];
				}				
				if((isset($_POST['search_date_from']) && trim($_POST['search_date_from']) != "") && (isset($_POST['search_date_to']) && trim($_POST['search_date_to']) != "")){
					$sWhere .= " AND fecha BETWEEN '".$_POST["search_date_from"]."' AND '".$_POST["search_date_to"]."'";
				}
				if(isset($_POST['search_name']) && trim($_POST['search_name']) != ""){
					$sWhere .= " AND nombre LIKE '%".$_POST['search_name']."%'";
				}
				if(isset($_POST['search_gerente']) && trim($_POST['search_gerente']) != ""){
					$sWhere .= " AND (SELECT nombre from cat_clientes WHERE id = cat_haciendas.id_cliente) LIKE '%".$_POST['search_gerente']."%'";
				}
				if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
					$sWhere .= " AND status = ".$_POST["order_status"];
				}

				/*----------  LIMIT  ----------*/
				if(isset($_POST['length']) && $_POST['length'] > 0){
					$sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
				}
			}

			$sql = "SELECT *,(SELECT nombre from cat_clientes WHERE id = cat_haciendas.id_cliente) as cliente FROM cat_haciendas WHERE id_usuario = '{$this->session->logged}' $sWhere $sOrder $sLimit";
			$res = $this->conexion->link->query($sql);
			$datos = (object)[
				"customActionMessage" => "Error al consultar la informacion",
				"customActionStatus" => "Error",
				"data" => [],
				"draw" => 0,
				"recordsFiltered" => 0,
				"recordsTotal" => 0,
			];
			$count = 0;
			while($fila = $res->fetch_assoc()){
				$fila = (object)$fila;
				$count++;
				$datos->data[] = [
					$fila->id,
					$fila->nombre,
					$fila->status,
					$fila->cliente,
					'<button class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
				];
			}

			$datos->recordsTotal = count($datos->data);
			$datos->customActionMessage = "Informacion completada con exito";
			$datos->customActionStatus = "OK";

			return json_encode($datos);
		}

		public function create(){
			extract($_POST);
			if($txtnom != "" && $id_cliente != ""){
				$sql = "INSERT INTO cat_haciendas SET 
				nombre = '{$txtnom}' , 
				id_cliente = '{$id_cliente}' , 
				fecha = CURRENT_DATE , 
				id_usuario = '{$this->session->logged}'";
				$ids = $this->conexion->Consultas(1,$sql);
        		return $ids;
			}
		}

		public function update(){
			extract($_POST);
			if((int)$id > 0 && $txtnom != "" && $id_cliente != ""){
				$sql = "UPDATE cat_haciendas SET 
					nombre = '{$txtnom}' , 
					id_cliente = '{$id_cliente}' , 
					fecha = CURRENT_DATE 
					WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		public function changeStatus(){
			extract($_POST);
			if((int)$id > 0 ){
				$status = ($estado == 'activo') ? 1 : 0;
				$sql = "UPDATE cat_haciendas SET status={$status} WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				// echo $sql;
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		public function edit(){
			$response = (object)[
				"success" => 400,
				"data" => [],
				"clientes" => [],
				"agrupaciones" => [],
				"lotes" => [],
			];
			$response->clientes = $this->getClient();
			if(isset($_GET['id'])){
				$id = (int)$_GET['id'];
				$response->agrupaciones = $this->getAgrupaciones($id);
				if($id > 0){
					$sql = "SELECT * FROM cat_haciendas WHERE id_usuario = '{$this->session->logged}' AND id = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->data = (object)$res->fetch_assoc();
					}
					$response->lotes = $this->getLotes($id);
					$response->success = 200;
				}
			}
			return json_encode($response);
		}

		public function addLote(){
			extract($_POST);
			$lotes = [];
			if((int)$id > 0 && $id_cliente != ""){
				$sql = "INSERT INTO cat_lotes SET
						id_hacienda = '{$id}',
						id_agrupacion = '{$id_agrupacion}',
						nombre = '{$lote}',
						area = '{$area}',
						id_usuario = '{$this->session->logged}',
						id_cliente = '{$id_cliente}'";
				$this->conexion->Consultas(1,$sql);
				$lotes = $this->getLotes($id);
			}
			return json_encode($lotes);
		}

		public function positionLote(){
			extract($_POST);
			$lotes = [];
			if(is_array($id_lotes) && count($id_lotes) > 0){
				foreach ($id_lotes as $key => $value) {
					$sql = "UPDATE cat_lotes SET position={$key} 
						WHERE id = '{$value}' AND id_usuario = '{$this->session->logged}' AND id_hacienda = '{$id}'";
					$this->conexion->Consultas(1,$sql);
				}
			}
			$lotes = $this->getLotes($id);
			return json_encode($lotes);
		}

		public function removeLote(){
			extract($_POST);
			$lotes = [];
			if((int)$id > 0 && $id_cliente != "" && $id_lote> 0){
				$sql = "UPDATE cat_lotes SET status=0 WHERE id = {$id_lote} AND id_usuario = '{$this->session->logged}' AND id_hacienda = '{$id}'";
				$this->conexion->Consultas(1,$sql);
			}
			$lotes = $this->getLotes($id);
			return json_encode($lotes);
		}

		private function getLotes($id_hacienda){
			$lotes = [];
			if((int)$id_hacienda > 0 && $id_hacienda != ""){
				$sql="SELECT id,(SELECT nombre FROM cat_agrupaciones WHERE id = id_agrupacion) AS agrupacion, nombre , 
				area FROM cat_lotes  
				WHERE id_usuario = '{$this->session->logged}' AND id_hacienda = '{$id_hacienda}' AND status > 0
				ORDER BY position";
				$res = $this->conexion->link->query($sql);
				while($fila = $res->fetch_assoc()){
					$lotes[] = (object)$fila;
				}
			}
			return $lotes;
		}

		private function getClient(){
			$cliente = [];
			$sql = "SELECT id , nombre FROM cat_clientes WHERE id_usuario = '{$this->session->logged}' AND status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$cliente[] = (object)$fila;
			}

			return $cliente;
		}

		private function getAgrupaciones($id_hacienda){
			$cliente = [];
			$sql = "SELECT id , nombre FROM cat_agrupaciones WHERE id_usuario = '{$this->session->logged}' AND id_hacienda = '{$id_hacienda}' AND status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$cliente[] = (object)$fila;
			}

			return $cliente;
		}
	}
?>
