<?
/**
* 
*/
class Informes 
{
	
    private $conexion;
    private $session;
    
    public function __construct()
    {
        $this->conexion = new M_Conexion();
        $this->session = Session::getInstance();
    }

    public function index(){
        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = " LIMIT 10";
        if(isset($_POST)){

            // /*----------  ORDER BY ----------*/
            
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY (SELECT nombre FROM cat_clientes  WHERE id = id_cliente) {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY (SELECT nombre FROM cat_haciendas WHERE id = id_hacienda) {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY WEEK(fecha) {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }
            // /*----------  ORDER BY ----------*/

            // if(isset($_POST['search_id']) && trim($_POST['search_id']) != ""){
            // 	$sWhere .= " AND id = ".$_POST["search_id"];
            // }				
            // if((isset($_POST['search_date_from']) && trim($_POST['search_date_from']) != "") && (isset($_POST['search_date_to']) && trim($_POST['search_date_to']) != "")){
            // 	$sWhere .= " AND fecha BETWEEN '".$_POST["search_date_from"]."' AND '".$_POST["search_date_to"]."'";
            // }
            // if(isset($_POST['search_name']) && trim($_POST['search_name']) != ""){
            // 	$sWhere .= " AND nombre LIKE '%".$_POST['search_name']."%'";
            // }
            // if(isset($_POST['search_email']) && trim($_POST['search_email']) != ""){
            // 	$sWhere .= " AND email LIKE '%".$_POST['search_email']."%'";
            // }
            // if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
            // 	$sWhere .= " AND status = ".$_POST["order_status"];
            // }

            // id_usuario = '{$this->session->logged}' $sWhere $sOrder $sLimit";
            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT 
                id ,
                (SELECT nombre FROM cat_clientes WHERE id = id_cliente) AS Productor,
                (SELECT nombre FROM cat_haciendas WHERE id = id_hacienda) AS Hacienda,
                semana_selected AS semana,
                status,
                id_usuario
                FROM informe 
                WHERE 1 = 1 
                    AND id_cliente = '{$this->session->client}'
                    AND id_hacienda =  '{$this->session->finca}'
                    AND YEAR(fecha) = '{$this->session->year}'
                    $sOrder
                    $sLimit";
                    #die($sql);
        // print $sql;
        $res = $this->conexion->link->query($sql);
        $datos = (object)[
            "customActionMessage" => "Error al consultar la informacion",
            "customActionStatus" => "Error",
            "data" => [],
            "draw" => 0,
            "recordsFiltered" => 0,
            "recordsTotal" => 0,
        ];
        $count = 1;
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            $count++;
            $datos->data[] = [
                $count,
                $fila->id,
                $fila->Productor,
                $fila->Hacienda,
                $fila->semana,
                $fila->status,
                $fila->id_usuario,
                '<button class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
            ];
        }

        $datos->recordsTotal = count($datos->data);
        $datos->customActionMessage = "Informacion completada con exito";
        $datos->customActionStatus = "OK";

        return json_encode($datos);
    }

    public function getInforme(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

        return $this->edit();
    }

    public function getUrl(){
        extract($_POST);
        $response = 0;
        if((int)$id > 0){
            $sql = "SELECT token 
                    FROM informe 
                    WHERE id_cliente = '{$this->session->client}'
                        AND id_hacienda =  '{$this->session->finca}'
                        AND id = {$id}";
            // print $sql;
            $res = $this->conexion->link->query($sql);
            $fila = (object)$res->fetch_assoc();
            $ruta = "http://sigat.procesos-iq.com/informes/{$fila->token}.pdf";
            $response = $ruta;
        }

        return json_encode($response);
    }

    public function saveInformeRL(){
        $response = (object)["success" => 400 , "id" => 0];
        $postdata = (object)json_decode(file_get_contents("php://input"));
        #print_r($postdata);
        #return ["200"];
        if(isset($postdata->descripcion) &&  $postdata->descripcion != "" && isset($postdata->informe) &&  $postdata->informe != ""){
            $sql = "SELECT id,descripcion , informe 
                    FROM informe 
                    WHERE id_usuario = '{$this->session->logged}'
                        AND id_cliente = '{$this->session->client}'
                        AND id_hacienda =  '{$this->session->finca}'
                        AND status = 0
                        AND YEAR(fecha) = {$this->session->year}
                    LIMIT 1";
            // print $sql;
            $res = $this->conexion->link->query($sql);
            if($res->num_rows > 0){
                $fila = (object)$res->fetch_assoc();
                $response->id = $fila->id;
                
                $sql_informe = "UPDATE informe SET 
                                    descripcion = '{$postdata->descripcion}' , informe = '{$postdata->informe}' 
                                WHERE id_usuario = '{$this->session->logged}'
                                    AND id_cliente = '{$this->session->client}'
                                    AND id_hacienda =  '{$this->session->finca}'
                                    AND id = $response->id";
                $this->conexion->link->query($sql_informe);
            }else{
                $sql_informe = "INSERT INTO informe SET descripcion = '{$postdata->descripcion}' , informe = '{$postdata->informe}', id_hacienda = '{$this->session->finca}', id_cliente = '{$this->session->client}', id_usuario = '{$this->session->logged}'";
                // $this->conexion->link->query($sql_informe);
                $this->conexion->link->query($sql_informe);
                $response->id = $this->conexion->link->insert_id;
            }
            $response->success = 200;
        }else{
            $response->success = 200;
        }

        return json_encode($response);
    }

    public function edit(){
        $response = (object)[
            "success" => 400,
            "data" => []
        ];
        
        $sql = "SELECT id, descripcion , informe 
                FROM informe 
                WHERE id_usuario = '{$this->session->logged}'
                    AND id_cliente = '{$this->session->client}'
                    AND id_hacienda =  '{$this->session->finca}'
                    AND status = 0
                    AND YEAR(fecha) = {$this->session->year}
                LIMIT 1";
        
        $res = $this->conexion->link->query($sql);
        if($res->num_rows > 0){
            $response->data = (object)$res->fetch_assoc();
        }
        $response->ultima_semana_fotos = $this->conexion->queryOne("SELECT WEEK(MAX(fecha)) FROM foliar WHERE (evi_malezas != '' OR evi_deshoje != '' OR evi_deshojefito != '' OR evi_enfunde != '' OR evi_drenaje != '' OR evi_plaga != '' OR evi_otrasobs != '')");
        $response->success = 200;
        
        return json_encode($response);
    }

    public function comprobarInformes(){
        $informes = $this->conexion->queryAll("SELECT cat_haciendas.nombre, token, year_selected, semana_selected, token FROM informe INNER JOIN cat_haciendas ON id_hacienda = cat_haciendas.id WHERE year_selected = 2018");

        foreach($informes as $row){
            $exist = file_exists("../informes/{$row->token}.pdf");
            if(!$exist){
                $line = "{$row->nombre} AÑO {$row->year_selected} SEMANA {$row->semana_selected} {$row->token}";
                echo "{$line}\n";
            }
        }
    }

    public function guardarImagenReporte(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $base64 = base64_decode(explode(",", $postdata->data)[1]);
        $token_reporte = $postdata->token;
        $name_image = $postdata->name;
        mkdir("./../imagenes_pdf/{$token_reporte}");
        D("./../imagenes_pdf/{$token_reporte}/{$name_image}.png");
        file_put_contents("./../imagenes_pdf/{$token_reporte}/{$name_image}.png", $base64);
    }

    public function enviar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if($postdata->token){
            $html = $postdata->introduccion ."<br>".$postdata->descripcion." <p><a href='http://sigat.procesos-iq.com/informes/{$postdata->token}' download> Descargar Archivo</a></p>";
            #$cachar_token = $this->sendEmail("calleruilovahector@hotmail.com" , "Informe Semanal" ,"",$html,"contacto@procesos-iq.com", "Sigat Soporte");
            #$cachar_token2 = sendEmail("{$email_cliente}" , "Informe Semanal" ,"",$html,"contacto@procesos-iq.com", "Sigat Soporte");
            #$cachar_token3 = $this->sendEmail("arturo.calle@procesos-iq.com" , "Informe Semanal" ,"",$html,"contacto@procesos-iq.com", "Sigat Soporte");
            $cachar_token4 = $this->sendEmail("victor.alvarez@procesos-iq.com" , "Informe Semanal" ,"",$html,"contacto@procesos-iq.com", "Sigat Soporte");
        }
    }

    private function sendEmail($to, $subject, $body_text, $body_html, $from, $from_name) {
        // Initialize cURL
        $ch = curl_init();
        
        // Set cURL options
        curl_setopt($ch, CURLOPT_URL, 'https://api.elasticemail.com/mailer/send');
        curl_setopt($ch, CURLOPT_POST, 1);
      
        // Parameter data
        $data = 'username='.urlencode('javiprocesosiq@gmail.com').
            '&api_key='.urlencode('f3e09b72-eed3-4ca8-bd02-03a0080e1a81').
            '&from='.urlencode($from).
            '&from_name='.urlencode($from_name).
            '&to='.urlencode($to).
            '&subject='.urlencode($subject);
      
        if($body_html)  $data .= '&body_html='.urlencode($body_html);
        if($body_text)  $data .= '&body_text='.urlencode($body_text);
        
        // Set parameter data to POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      
        // Header data
            $header = "Content-Type: application/x-www-form-urlencoded\r\n";
            $header .= "Content-Length: ".strlen($data)."\r\n\r\n";
      
        // Set header
        curl_setopt($ch, CURLOPT_HEADER, $header);
        
        // Set to receive server response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        // Set cURL to verify SSL
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        
        // Set the path to the certificate used by Elastic Mail API
        // curl_setopt($ch, CURLOPT_CAINFO, getcwd()."/DOWNLOADED_CERTIFICATE.CRT");
        
        // Get result
        $result = curl_exec($ch);
        
        // Close cURL
        curl_close($ch);
        
        // print_r($result);
        // Return the response or NULL on failure
        return ($result === false) ? NULL : $result;
        
        // Alternative error checking return
        // return ($result === false) ? 'Curl error: ' . curl_error($ch): $result;
      }
}

?>