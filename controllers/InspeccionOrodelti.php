<?php

class InspeccionOrodelti {
	
	private $bd;
	private $session;
	
	public function __construct() {
        $this->db = new M_Conexion();
        $this->session = Session::getInstance();
    }

    private function params(){
        $data = (object)json_decode(file_get_contents("php://input"));
        $params = new stdClass;
        $params->semana = $data->semana;
        $params->luz = $data->luz;
        $params->fecha_inicial = $data->fecha_inicial;
        $params->fecha_final = $data->fecha_final;
        $params->id_gerente = $data->gerente;
        $params->year = $data->year;
        $params->tipo_finca = $data->tipo_finca;
        return $params;
    }

    public function gerentes(){
        $response = new stdClass;
        $response->data = $this->db->queryAll("SELECT id, nombre AS label FROM cat_gerentes WHERE status = 1");
        return json_encode($response);
    }

    public function years(){
        $response = new stdClass;
        $response->selected = $this->db->queryRow("SELECT MAX(anio) AS anio FROM muestras_haciendas_orodelti")->anio;
        $response->data = $this->db->queryAll("SELECT anio AS id, anio AS label FROM muestras_haciendas_orodelti WHERE anio >= 2018 GROUP BY anio");
        return json_encode($response);
    }

    public function tipos_fincas(){
        $response = new stdClass;
        $filters = $this->params();
        $sWhere = "";
        if($filters->id_gerente > 0){
            $sWhere .= " AND id_gerente = $filters->id_gerente";
        }
        if($filters->year > 0){
            $sWhere .= " AND anio = $filters->year";
        }
        $response->data = $this->db->queryAll("SELECT tipo_finca AS id, tipo_finca AS label FROM muestras_haciendas_orodelti WHERE tipo_finca != '' AND tipo_finca IS NOT NULL $sWhere GROUP BY tipo_finca");
        return json_encode($response);
    }

    public function index(){
        $response = new stdClass;
        $filters = $this->params();

        $response->semanas = $this->getSemanas();
        if($filters->semana == null){
            $keys = array_keys($response->semanas);
            $response->last_semana = $filters->semana = $keys[count($keys)-1];
        }
        $response->table_lote = $this->getLotes($filters);

        foreach($response->table_lote as $lote){
            $m3 = $this->db->queryRow("SELECT 
                    ROUND(AVG(IF(total_hoja_3 = 100 OR total_hoja_3 = 1, 1, 0)) * 100, 2) AS h3, 
                    ROUND(AVG(IF(total_hoja_4 = 100 OR total_hoja_4 = 1, 1, 0)) * 100, 2) AS h4, 
                    ROUND(AVG(IF(total_hoja_5 = 100 OR total_hoja_5 = 1, 1, 0)) * 100, 2) AS h5,
                    ROUND(AVG(hoja_mas_vieja_de_estrias)) AS hmvle,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor)) AS hmvlqm
                FROM muestras_hacienda_detalle_3M
                WHERE id_lote = '{$lote->id}' AND WEEK(fecha) = '{$filters->semana}'");
            $lote->m3_h3 = $m3->h3;
            $lote->m3_h4 = $m3->h4;
            $lote->m3_h5 = $m3->h5;
            $lote->m3_hmvle = $m3->hmvle;
            $lote->m3_hmvlqm = $m3->hmvlqm;

            $s0 = $this->db->queryRow("SELECT 
                    ROUND(AVG(hojas_totales)) AS ht,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor)) AS hmvlqm,
                    ROUND(AVG(hojas_mas_vieja_libre)) AS hmvle
                FROM muestras_hacienda_detalle
                WHERE id_lote = '{$lote->id}' AND tipo_semana = 0 AND WEEK(fecha) = '{$filters->semana}'");
            $lote->s0_ht = $s0->ht;
            $lote->s0_hmvlqm = $s0->hmvlqm;
            $lote->s0_hmvle = $s0->hmvle;

            $s11 = $this->db->queryRow("SELECT 
                    ROUND(AVG(hojas_totales)) AS ht,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor)) AS hmvlqm
                FROM muestras_hacienda_detalle
                WHERE id_lote = '{$lote->id}' AND tipo_semana = 11 AND WEEK(fecha) = '{$filters->semana}'");
            $lote->s11_ht = $s11->ht;
            $lote->s11_hmvlqm = $s11->hmvlqm;
        }

        return json_encode($response);
    }

    public function getAplicaciones(){
        $response = new stdClass;
        $sql = "SELECT fecha, IF(ciclo = '(NULL)', '', ciclo) AS ciclo, IF(producto_1 IS NULL OR producto_1 = '(NULL)', '', producto_1) AS producto_1, IF(producto_2 = '(NULL)', '', producto_2) AS producto_2
                FROM ciclos_aplicacion
                WHERE id_finca = '{$this->session->finca}' AND ((producto_1 != '' AND producto_1 != '(NULL)') OR (producto_2 != '' AND producto_2 != '(NULL)'))
                ORDER BY fecha";
        $response->data = $this->db->queryAll($sql);
        return json_encode($response);
    }

    private function getSemanas($filters = []){
        $response = new stdClass;
        $sql = "SELECT id, label FROM(
            SELECT WEEK(fecha) AS id, WEEK(fecha) AS label 
            FROM muestras_haciendas
            WHERE YEAR(fecha) = YEAR(CURRENT_DATE)
                AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
            GROUP BY WEEK(fecha)
            UNION ALL
            SELECT WEEK(fecha) AS id, WEEK(fecha) AS label 
            FROM muestras_haciendas_3M
            WHERE YEAR(fecha) = YEAR(CURRENT_DATE)
                AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
            GROUP BY WEEK(fecha)
        ) AS tbl
        GROUP BY id
        ORDER BY id";
        $response = $this->db->queryAllSpecial($sql);
        return $response;
    }

    private function getLotes($filters = []){
        if($filters->semana > 0){
            $sWhere .= "AND semana = '{$filters->semana}'";
            $sWhere3 .= "AND semana = '{$filters->semana}'";
        }
        $sql = "SELECT id, UPPER(lote) AS lote
                FROM(
                    SELECT cat_lotes.id, cat_lotes.`nombre` AS lote
                    FROM muestras_haciendas
                    INNER JOIN muestras_hacienda_detalle detalle ON id_Mhacienda = muestras_haciendas.`id`
                    INNER JOIN cat_lotes ON cat_lotes.id = id_lote
                    WHERE anio = 2017 
                        AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                        AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                        AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}') $sWhere
                    GROUP BY cat_lotes.id 
                    UNION ALL
                    SELECT cat_lotes.id, cat_lotes.`nombre` AS lote 
                    FROM muestras_haciendas_3M
                    INNER JOIN muestras_hacienda_detalle_3M detalle ON id_Mhacienda = muestras_haciendas_3M.`id`
                    INNER JOIN cat_lotes ON cat_lotes.id = id_lote
                    WHERE anio = 2017 
                        AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                        AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                        AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}') $sWhere3
                    GROUP BY cat_lotes.id
                ) AS lotes
                GROUP BY id
                ORDER BY lote";
        return $this->db->queryAll($sql);
    }

    public function getDataTables(){
        $response = new stdClass;
        $filters = $this->params();

        $sWhere = "";
        if($filters->tipo_finca != ''){
            $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
        }
        if($filters->id_gerente > 0){
            $sWhere .= " AND id_gerente = $filters->id_gerente";
        }
        if($filters->year > 0){
            $sWhere .= " AND anio = $filters->year";
        }

        $sql = "SELECT semana, anio, finca, h3_3m, h4_3m, h5_3m, ht_3m, hvl_3m, hvl_0s, ht_0s, ht_11s, foliar
                FROM muestras_haciendas_orodelti
                WHERE 1=1 $sWhere
                ORDER BY finca, semana";
        $response->data = $this->db->queryAll($sql);

        return json_encode($response);
    }

    //BEGIN 3 M
        private function getSemanas3M(){
            $filters = $this->params();

            $sWhere = "";
            if($filters->tipo_finca != ''){
                $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
            }
            if($filters->id_gerente > 0){
                $sWhere .= " AND id_gerente = $filters->id_gerente";
            }
            if($filters->year > 0){
                $sWhere .= " AND anio = $filters->year";
            }

            $sql = "SELECT semana
                        FROM muestras_haciendas_orodelti
                        WHERE semana > 0 $sWhere
                        GROUP BY semana
                        ORDER BY semana";
            return $this->db->queryAll($sql);
        }
        private function getFocos3M(){
            $filters = $this->params();

            $sWhere = "";
            if($filters->tipo_finca != ''){
                $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
            }
            if($filters->id_gerente > 0){
                $sWhere .= " AND id_gerente = $filters->id_gerente";
            }
            if($filters->year > 0){
                $sWhere .= " AND anio = $filters->year";
            }

            $sql = "SELECT UPPER(finca) AS id, UPPER(finca) AS label
                        FROM muestras_haciendas_orodelti
                        WHERE semana > 0 $sWhere
                        GROUP BY UPPER(finca)
                        ORDER BY finca";
            return $this->db->queryAllSpecial($sql);
        }

        public function get3M(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $focos = $this->getFocos3M();

            $filters = $this->params();
            $sWhere = "";
            if($filters->tipo_finca != ''){
                $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
            }
            if($filters->year > 0){
                $sWhere .= " AND anio = $filters->year";
            }

            $data_chart = [];
            foreach($semanas as $sem){

                foreach($focos as $foco){
                    $sql = "SELECT AVG(hvl_3m) AS value
                            FROM muestras_haciendas_orodelti
                            WHERE semana > 0 
                                AND semana = {$sem->semana}
                                AND finca = '{$foco}' $sWhere";
                    $data = $this->db->queryRow($sql);


                    $position_foco = array_search($foco, array_keys($focos));
                    if(isset($data->value)){
                        $data_chart[] = ["value" => $data->value, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }

                }
            }

            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line");

            return json_encode($response);
        }

        public function getH4(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $focos = $this->getFocos3M();

            $filters = $this->params();
            $sWhere = "";
            if($filters->tipo_finca != ''){
                $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
            }
            if($filters->year > 0){
                $sWhere .= " AND anio = $filters->year";
            }

            $data_chart_hmvle = [];
            $data_chart_hmvlqm = [];
            
            foreach($semanas as $sem){
                foreach($focos as $foco){
                    $sql = "SELECT 
                                AVG(h4_3m) AS value,
                                semana AS label,
                                1 AS selected,
                                finca AS legend
                            FROM muestras_haciendas_orodelti 
                            WHERE semana > 0 
                                AND semana = {$sem->semana}
                                AND finca = '{$foco}'
                                $sWhere";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco, array_keys($focos));
                    if(isset($data->value)){
                        $data_chart_h4[] = ["value" => $data->value, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_h4[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->h4 = $this->chartInit($data_chart_h4, "vertical" , "" , "line");

            return json_encode($response);
        }

        public function getH3(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $focos = $this->getFocos3M();

            $filters = $this->params();
            $sWhere = "";
            if($filters->tipo_finca != ''){
                $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
            }
            if($filters->year > 0){
                $sWhere .= " AND anio = $filters->year";
            }

            $data_chart_hmvle = [];
            $data_chart_hmvlqm = [];
            foreach($semanas as $sem){

                foreach($focos as $foco){
                    $sql = "SELECT 
                                AVG(h3_3m) AS 'h3',
                                semana AS label,
                                1 AS selected,
                                finca AS legend
                            FROM muestras_haciendas_orodelti
                            WHERE semana > 0 
                                AND semana = {$sem->semana}
                                AND finca = '{$foco}'
                                $sWhere";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco, array_keys($focos));
                    if(isset($data->h3)){
                        $data_chart_h3[] = ["value" => $data->h3, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_h3[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->h3 = $this->chartInit($data_chart_h3, "vertical" , "" , "line");

            return json_encode($response);
        }

        public function getH5(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $focos = $this->getFocos3M();

            $filters = $this->params();
            $sWhere = "";
            if($filters->tipo_finca != ''){
                $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
            }
            if($filters->year > 0){
                $sWhere .= " AND anio = $filters->year";
            }
            foreach($semanas as $sem){

                foreach($focos as $foco){
                    $sql = "SELECT 
                                AVG(h5_3m) AS 'h5',
                                semana AS label,
                                1 AS selected,
                                finca AS legend
                            FROM muestras_haciendas_orodelti
                            WHERE semana > 0 
                                AND semana = {$sem->semana}
                                AND finca = '{$foco}'
                                $sWhere";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco, array_keys($focos));
                    if(isset($data->h5)){
                        $data_chart_h5[] = ["value" => $data->h5, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_h5[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->h5 = $this->chartInit($data_chart_h5, "vertical" , "" , "line");

            return json_encode($response);
        }

        public function getHMVLQM(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $focos = $this->getFocos3M();

            $data_chart_hmvle = [];
            $data_chart_hmvlqm = [];

            $filters = $this->params();
            $sWhere = "";
            if($filters->tipo_finca != ''){
                $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
            }
            if($filters->year > 0){
                $sWhere .= " AND anio = $filters->year";
            }
            foreach($semanas as $sem){

                foreach($focos as $foco){
                    $sql = "SELECT 
                                AVG(ht_3m) AS hmvlqm,
                                semana AS label,
                                1 AS selected,
                                finca AS legend
                            FROM muestras_haciendas_orodelti
                            WHERE semana > 0 #AND anio = YEAR(CURRENT_DATE)
                                AND semana = {$sem->semana}
                                AND finca = '{$foco}'
                                $sWhere";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco, array_keys($focos));
                    if(isset($data->hmvlqm) && $data->hmvlqm > 0){
                        $data_chart_hmvlqm[] = ["value" => $data->hmvlqm, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->hmvlqm = $this->chartInit($data_chart_hmvlqm, "vertical" , "" , "line");

            return json_encode($response);
        }
    //END 3 M

    //BEGIN 0S
        private function getSemanas0S(){
            $filters = $this->params();
            $sWhere = "";
            if($filters->tipo_finca != ''){
                $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
            }
            if($filters->id_gerente > 0){
                $sWhere .= " AND id_gerente = $filters->id_gerente";
            }
            if($filters->year > 0){
                $sWhere .= " AND anio = $filters->year";
            }

            $sql = "SELECT semana
                        FROM muestras_haciendas_orodelti
                        WHERE semana > 0 $sWhere #AND YEAR(fecha) = YEAR(CURRENT_DATE)
                        GROUP BY semana
                        ORDER BY semana";
            return $this->db->queryAll($sql);
        }
        private function getFocos0S(){
            $filters = $this->params();
            $sWhere = "";
            if($filters->tipo_finca != ''){
                $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
            }
            if($filters->id_gerente > 0){
                $sWhere .= " AND id_gerente = $filters->id_gerente";
            }
            if($filters->year > 0){
                $sWhere .= " AND anio = $filters->year";
            }

            $sql = "SELECT UPPER(finca) AS id, UPPER(finca) AS label
                        FROM muestras_haciendas_orodelti
                        WHERE semana > 0 $sWhere #AND YEAR(fecha) = YEAR(CURRENT_DATE)
                        GROUP BY finca
                        ORDER BY finca";
            return $this->db->queryAllSpecial($sql);
        }

        public function getHT0S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $focos = $this->getFocos0S();

            $filters = $this->params();
            $sWhere = "";
            if($filters->tipo_finca != ''){
                $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
            }
            if($filters->year > 0){
                $sWhere .= " AND anio = $filters->year";
            }

            $data_chart_ht0s = [];

            foreach($semanas as $sem){

                foreach($focos as $foco){
                    $sql = "SELECT 
                            AVG(ht_0s) AS value
                        FROM muestras_haciendas_orodelti
                        WHERE semana > 0 
                            AND semana = {$sem->semana}
                            AND finca = '{$foco}' $sWhere";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco,array_keys($focos));
                    if(isset($data->value) && $data->value > 0){
                        $data_chart_ht0s[] = ["value" => $data->value, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_ht0s[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->ht = $this->chartInit($data_chart_ht0s,"vertical","","line");
            return json_encode($response);
        }

        public function getHVLE0S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $focos = $this->getFocos0S();

            $filters = $this->params();
            $sWhere = "";
            if($filters->tipo_finca != ''){
                $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
            }
            if($filters->year > 0){
                $sWhere .= " AND anio = $filters->year";
            }

            $data_chart_hmvle0s = [];

            foreach($semanas as $sem){
                foreach($focos as $foco){
                    $sql = "SELECT 
                            AVG(hvl_0s) AS value
                        FROM muestras_haciendas_orodelti
                        WHERE semana > 0 
                            AND semana = {$sem->semana}
                            AND finca = '{$foco}' $sWhere";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco,array_keys($focos));

                    if(isset($data->value) && $data->value > 0){
                        $data_chart_hmvle0s[] = ["value" => $data->value, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvle0s[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->hmvle = $this->chartInit($data_chart_hmvle0s,"vertical","","line");
            return json_encode($response);
        
        }
    //END 0S

    //BEGIN 11S
        private function getSemanas11S(){
            $filters = $this->params();
            $sWhere = "";
            if($filters->tipo_finca != ''){
                $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
            }
            if($filters->id_gerente > 0){
                $sWhere .= " AND id_gerente = $filters->id_gerente";
            }
            if($filters->year > 0){
                $sWhere .= " AND anio = $filters->year";
            }

            $sql = "SELECT semana
                        FROM muestras_haciendas_orodelti
                        WHERE semana > 0 $sWhere #AND YEAR(fecha) = YEAR(CURRENT_DATE)
                        GROUP BY semana
                        ORDER BY semana";
            return $this->db->queryAll($sql);  
        }  
        private function getFocos11S(){
            $filters = $this->params();
            $sWhere = "";
            if($filters->tipo_finca != ''){
                $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
            }
            if($filters->id_gerente > 0){
                $sWhere .= " AND id_gerente = $filters->id_gerente";
            }
            if($filters->year > 0){
                $sWhere .= " AND anio = $filters->year";
            }

            $sql = "SELECT UPPER(finca) AS id, UPPER(finca) AS label
                        FROM muestras_haciendas_orodelti
                        WHERE semana > 0 $sWhere #AND YEAR(fecha) = YEAR(CURRENT_DATE)
                        GROUP BY finca
                        ORDER BY finca";
            return $this->db->queryAllSpecial($sql);          
        }

        public function getHT11S(){
            $response = new stdClass();

            $semanas = $this->getSemanas11S();
            $focos = $this->getFocos11S();

            $filters = $this->params();
            $sWhere = "";
            if($filters->tipo_finca != ''){
                $sWhere .= " AND tipo_finca = '{$filters->tipo_finca}'";
            }
            if($filters->year > 0){
                $sWhere .= " AND anio = $filters->year";
            }

            $data_chart_ht11s = [];

            foreach($semanas as $sem){

                foreach($focos as $foco){
                    $sql = "SELECT AVG(ht_11s) AS value
                        FROM muestras_haciendas_orodelti
                        WHERE semana > 0 
                            AND semana = {$sem->semana}
                            AND finca = '{$foco}' $sWhere";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco,array_keys($focos));
                    if(isset($data->value) && $data->value > 0){
                        $data_chart_ht11s[] = ["value" => $data->value, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_ht11s[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->ht = $this->chartInit($data_chart_ht11s,"vertical","","line");
            return json_encode($response);
        }
    //END 11S

    private function getSemanasFoliar(){ 
        $filters = $this->params();
        if($filters->id_gerente > 0){
            $sWhere .= " AND id_gerente = $filters->id_gerente";
        }
        if($filters->year > 0){
            $sWhere .= " AND anio = $filters->year";
        }

        $sql = "SELECT semana
                    FROM muestras_haciendas_orodelti
                    WHERE semana > 0 $sWhere #AND YEAR(fecha) = YEAR(CURRENT_DATE)
                    GROUP BY semana
                    ORDER BY semana";
        return $this->db->queryAll($sql);  
    }

    private function getFocosFoliar(){
        $filters = $this->params();
        if($filters->id_gerente > 0){
            $sWhere .= " AND id_gerente = $filters->id_gerente";
        }
        if($filters->year > 0){
            $sWhere .= " AND anio = $filters->year";
        }

        $sql = "SELECT UPPER(finca) AS id, UPPER(finca) AS label
                    FROM muestras_haciendas_orodelti
                    WHERE semana > 0 $sWhere #AND YEAR(fecha) = YEAR(CURRENT_DATE)
                    GROUP BY UPPER(finca)
                    ORDER BY finca";
        return $this->db->queryAllSpecial($sql);          
    }

    public function getEmisionFoliar(){
        $response = new stdClass();
        
        $semanas = $this->getSemanasFoliar();
        $focos = $this->getFocosFoliar();

        $filters = $this->params();
        if($filters->year > 0){
            $sWhere .= " AND anio = $filters->year";
        }

        $data_chart = [];

        foreach($semanas as $sem){
            foreach($focos as $foco){
                $sql = "SELECT AVG(foliar) AS value
                        FROM muestras_haciendas_orodelti
                        WHERE semana > 0 
                            AND semana = {$sem->semana}
                            AND finca = '{$foco}'
                            $sWhere";
                $data = $this->db->queryRow($sql);

                $position_foco = array_search($foco,array_keys($focos));
                if(isset($data->value) && $data->value > 0){
                    $data_chart[] = ["value" => $data->value, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                }else{
                    $data_chart[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                }
            }
        }

        $response->data = $this->chartInit($data_chart,"vertical","","line");
        return json_encode($response);
    }

    public function importar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $xlsx = str_replace('data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,', '', $postdata->upload);
	    $xlsx = str_replace(' ', '+', $xlsx);
        $data = base64_decode($xlsx);
        
        $target_name = "./../excel/inspeccionOrodelti/user_1/data.xlsx";
        file_put_contents($target_name, $data);
        $response = $this->readExcelInspeccion($target_name);

        $move_name = "./../excel/inspeccionOrodelti/completados/data_".date('Ymd_his').".xlsx";
        rename($target_name, $move_name);
        
        return $response;
    }

    private function procesar_xlsx($file_name){
        include '../excel/simplexlsx.class.php';
        $xlsx = new SimpleXLSX($file_name);
        $count = 0;
        $conexion = new M_Conexion;
        $libros = $xlsx->sheetNames();
        $registros = array();
    
        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key > 1){
                    $finca = trim($fila[4]);
                    if(in_array($finca, ["MATEO A", "MATEO B"])){
                        $finca = "MATEO";
                    }
                    if($finca == "MATHIAS EMP 2"){
                        $finca = "MATHIAS 2";
                    }
                    if($finca == "MATHIAS EMP 4"){
                        $finca = "MATHIAS 4";
                    }
                    if($finca == 'LA ENVIDIA'){
                        $finca = 'ENVIDIA';
                    }
                    if($finca == 'THOMAS'){
                        $finca = 'TOMAS';
                    }

                    $h3_3m = $fila[5] == '-' || $fila[5] == '' ? 'NULL' : str_replace("%", "", $fila[5]);
                    $h4_3m = $fila[6] == '-' || $fila[6] == '' ? 'NULL' : str_replace("%", "", $fila[6]);
                    $h5_3m = $fila[7] == '-' || $fila[7] == '' ? 'NULL' : str_replace("%", "", $fila[7]);
                    $ht_3m = $fila[8] == '-' || $fila[8] == '' ? 'NULL' : str_replace("%", "", $fila[8]);
                    $hvl_3m = $fila[9] == '-' || $fila[9] == '' ? 'NULL' : str_replace("%", "", $fila[9]);
                    $hvl_0s = $fila[10] == '-' || $fila[10] == '' ? 'NULL' : str_replace("%", "", $fila[10]);
                    $ht_0s = $fila[11] == '-' || $fila[11] == '' ? 'NULL' : str_replace("%", "", $fila[11]);
                    $ht_11s = $fila[12] == '-' || $fila[12] == '' ? 'NULL' : str_replace("%", "", $fila[12]);
                    $foliar = $fila[13] == '-' || $fila[13] == '' ? 'NULL' : str_replace("%", "", $fila[13]);

                    if($finca != "" && ($h3_3m != 'NULL' || $h4_3m != 'NULL' || $h5_3m != 'NULL' || $ht_3m != 'NULL' || $hvl_3m != 'NULL' || $hvl_0s != 'NULL' || $ht_0s != 'NULL' ||  $ht_11s != 'NULL' || $foliar != 'NULL')){
                        $sql = "INSERT INTO muestras_haciendas_orodelti SET
                                    anio = {$fila[0]},
                                    zona = '{$fila[1]}',
                                    sector = '{$fila[2]}',
                                    semana = {$fila[3]},
                                    finca = '{$finca}',
                                    id_finca = (SELECT id FROM cat_fincas WHERE nombre = '$finca' AND status = 1 LIMIT 1),
                                    id_gerente = (SELECT id_gerente FROM cat_fincas WHERE nombre = '$finca' AND status = 1 LIMIT 1),
                                    h3_3m = '{$h3_3m}',
                                    h4_3m = '{$h4_3m}',
                                    h5_3m = '{$h5_3m}',
                                    ht_3m = '{$ht_3m}',
                                    hvl_3m = '{$hvl_3m}',
                                    hvl_0s = '{$hvl_0s}',
                                    ht_0s = '{$ht_0s}',
                                    ht_11s = '{$ht_11s}',
                                    foliar = '{$foliar}'";
                        $conexion->query($sql);
                    }
                }
            }
        }
    }


    public function readExcelInspeccion($file_name){
        $response = new stdClass;
        include '../excel/simplexlsx.class.php';
        $response->status = 400;
        $response->message = "No ha sido posible guardar, Contacte a soporte";

        $xlsx = new SimpleXLSX($file_name);
        $count = 0;
        $conexion = new M_Conexion;
        $libros = $xlsx->sheetNames();
        $registros = array();
    
        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);

            foreach($rows as $key => $fila){
                if($key > 1){
                    $fecha = $fila[3];
                    if(is_numeric($fecha)){
                        $UNIX_DATE = ($fecha - 25569) * 86400;
                        $fecha = gmdate("Y-m-d", $UNIX_DATE);
                    } else {
                        $fecha = explode('-', $fecha)[2]."-".explode('-', $fecha)[1]."-".explode('-', $fecha)[0];
                    }

                    $finca = $fila[1];
                    if($finca == 'THOMAS'){
                        $finca = 'TOMAS';
                    }
                    if($finca == 'MATIAS 2'){
                        $finca = 'MATHIAS 2';
                    }
                    if($finca == 'MATIAS 4'){
                        $finca = 'MATHIAS 4';
                    }
                    if($finca == 'LA ENVIDIA'){
                        $finca = 'ENVIDIA';
                    }
                    $sql = "INSERT INTO muestras_haciendas_orodelti SET
                                encargado = '{$fila[0]}',
                                finca = '{$finca}',
                                id_finca = (SELECT id FROM cat_fincas WHERE nombre = '{$finca}' AND status = 1),
                                anio = YEAR('{$fecha}'),
                                id_gerente = (SELECT id_gerente FROM cat_fincas WHERE nombre = '{$finca}' AND status = 1 LIMIT 1),
                                semana = getWeek('{$fecha}'),
                                fecha_muestreo = '{$fecha}',
                                lote = '{$fila[4]}',
                                tipo_finca = '{$fila[5]}',
                                ht_3m = '{$fila[6]}',
                                h2_3m = '{$fila[7]}',
                                h2s_3m = '{$fila[8]}',
                                h3_3m = '{$fila[9]}',
                                h3s_3m = '{$fila[10]}',
                                h4_3m = '{$fila[11]}',
                                h4s_3m = '{$fila[12]}',
                                h5_3m = '{$fila[13]}',
                                h5s_3m = '{$fila[14]}',
                                hvl_3m = '{$fila[15]}',
                                hvl_afa_3m = '{$fila[16]}',
                                hvlq_3m = '{$fila[17]}',
                                hvlq_afa_3m = '{$fila[18]}',
                                ht_0s = '{$fila[19]}',
                                hvl_0s = '{$fila[20]}',
                                hvl_afa_0s = '{$fila[21]}',
                                hvlq_0s = '{$fila[22]}',
                                hvlq_afa_0s = '{$fila[23]}',
                                ht_6s = '{$fila[24]}',
                                hvle_6s = '{$fila[25]}',
                                hvle_afa_6s = '{$fila[26]}',
                                hvlq_6s = '{$fila[27]}',
                                hvlq_afa_6s = '{$fila[28]}',
                                ht_11s = '{$fila[29]}',
                                hvle_11s = '{$fila[30]}',
                                hvle_afa_11s = '{$fila[31]}',
                                hvlq_11s = '{$fila[32]}',
                                hvlq_afa_11s = '{$fila[33]}'";
                    $r = $this->db->query($sql);
                    if($r){
                        $response->status = 200;
                        $response->message = "Guardado con éxito";
                    }
                    D($sql);
                }
            }
            /*if(trim($name) == 'FINCAS DOLE'){
                foreach($rows as $key => $fila){
                    if($key > 1){
                        $fecha = $fila[4];
                        if(is_numeric($fecha)){
                            $UNIX_DATE = ($fecha - 25569) * 86400;
                            $fecha = gmdate("Y-m-d", $UNIX_DATE);
                        } else {
                            $fecha = explode('-', $fecha)[2]."-".explode('-', $fecha)[1]."-".explode('-', $fecha)[0];
                        }
                        $sql = "INSERT INTO muestras_haciendas_orodelti SET
                                    encargado = '{$fila[1]}',
                                    finca = '{$fila[2]}',
                                    id_finca = (SELECT id FROM cat_fincas WHERE nombre = '{$fila[2]}' AND status = 1),
                                    id_gerente = (SELECT id_gerente FROM cat_fincas WHERE nombre = '{$fila[2]}' AND status = 1 LIMIT 1),
                                    anio = '{$fila[0]}',
                                    semana = getWeek('{$fecha}'),
                                    fecha_muestreo = '{$fecha}',
                                    lote = '{$fila[5]}',
                                    tipo_finca = '',
                                    ht_3m = '{$fila[6]}',
                                    h2_3m = '{$fila[7]}',
                                    h3_3m = '{$fila[8]}',
                                    h4_3m = '{$fila[9]}',
                                    h5_3m = '{$fila[10]}',
                                    hvl_3m = '{$fila[11]}',
                                    hvlq_3m = '{$fila[12]}',
                                    ht_0s = '{$fila[13]}',
                                    hvl_0s = '{$fila[14]}',
                                    hvlq_0s = '{$fila[15]}',
                                    ht_11s = '{$fila[19]}',
                                    hvle_11s = '{$fila[20]}',
                                    hvlq_11s = '{$fila[21]}'";
                        $this->db->query($sql);
                        D($sql);
                    }
                }
            }*/
        }

        return $response;
    }

    
    //BEGIN COMPARACION 3
        private function getFincas3MComp(){
            $response = [];
            $data = $this->db->queryAll("SELECT YEAR(m.fecha) AS anio, id_hacienda, CONCAT(YEAR(m.fecha), ' ', h.`nombre`) AS label
                FROM muestras_haciendas_3M m
                INNER JOIN cat_haciendas h ON m.id_hacienda = h.`id`
                WHERE m.id_cliente = {$this->session->client} AND m.fecha IS NOT NULL
                GROUP BY id_hacienda, YEAR(m.fecha)");
            foreach($data as $row){
                $response[$row->label] = $row;
            }
            return $response;
        }
        private function getSemanas3MComp(){
            return $this->db->queryAll("SELECT WEEK(fecha) AS semana
                FROM muestras_haciendas_3M m
                WHERE m.id_cliente = {$this->session->client} AND m.fecha IS NOT NULL
                GROUP BY WEEK(fecha)");
        }

        public function get3MComparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            $data_chart = [];
            foreach($semanas as $sem){
                $data_chart[] = ["value" => 6.5, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(hoja_mas_vieja_de_estrias) AS value
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana > 0
                                AND muestras_haciendas_3M.id_hacienda = '{$finca->id_hacienda}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$finca->anio}
                                AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);


                    $position = array_search($finca->label, array_keys($fincas)) + 1;
                    if(isset($data->value)){
                        $data_chart[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }

                }
            }

            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line");

            return json_encode($response);
        }
        public function getH4Comparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            $data_chart_h4 = [];
            foreach($semanas as $sem){
                $data_chart_h4[] = ["value" => 30, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(IF(total_hoja_4 = 100 OR total_hoja_4 = 1, 1, 0)) AS 'h4',
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana > 0
                                AND muestras_haciendas_3M.id_hacienda = '{$finca->id_hacienda}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$finca->anio}
                                AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + 1;
                    if(isset($data->h4)){
                        $data_chart_h4[] = ["value" => $data->h4 * 100, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_h4[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->h4 = $this->chartInit($data_chart_h4, "vertical" , "" , "line");

            return json_encode($response);
        }
        public function getH3Comparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            $data_chart_hmvle = [];
            $data_chart_hmvlqm = [];
            foreach($semanas as $sem){
                $data_chart_h3[] = ["value" => 25, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(IF(total_hoja_3 = 100 OR total_hoja_3 = 1, 1, 0)) AS 'h3',
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana > 0
                                AND muestras_haciendas_3M.id_hacienda = '{$finca->id_hacienda}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$finca->anio}
                                AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + 1;
                    if(isset($data->h3)){
                        $data_chart_h3[] = ["value" => $data->h3 * 100, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_h3[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->h3 = $this->chartInit($data_chart_h3, "vertical" , "" , "line");

            return json_encode($response);
        }
        public function getH5Comparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            foreach($semanas as $sem){
                $data_chart_h5[] = ["value" => 35, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(IF(total_hoja_5 = 100 OR total_hoja_5 = 1, 1, 0)) AS 'h5',
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana > 0
                                AND muestras_haciendas_3M.id_hacienda = '{$finca->id_hacienda}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$finca->anio}
                                AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + 1;
                    if(isset($data->h5)){
                        $data_chart_h5[] = ["value" => $data->h5 * 100, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_h5[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->h5 = $this->chartInit($data_chart_h5, "vertical" , "" , "line");

            return json_encode($response);
        }
        public function getHMVLQMComparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            $data_chart_hmvle = [];
            $data_chart_hmvlqm = [];
            foreach($semanas as $sem){
                $data_chart_hmvlqm[] = ["value" => 11, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(hoja_mas_vieja_de_estrias) AS hmvle,
                                AVG(hoja_mas_vieja_libre_quema_menor) AS hmvlqm,
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana > 0
                                AND muestras_haciendas_3M.id_hacienda = '{$finca->id_hacienda}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$finca->anio}
                                AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + 1;
                    if(isset($data->hmvlqm) && $data->hmvlqm > 0){
                        $data_chart_hmvlqm[] = ["value" => $data->hmvlqm, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_hmvlqm[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->hmvlqm = $this->chartInit($data_chart_hmvlqm, "vertical" , "" , "line");

            return json_encode($response);
        }
    //END COMPARACION 3 M

    //BEGIN COMPARACION 0 SEM
        private function getFincas0SComp(){
            $response = [];
            $data = $this->db->queryAll("SELECT YEAR(m.fecha) AS anio, id_hacienda, CONCAT(YEAR(m.fecha), ' ', h.`nombre`) AS label
                FROM muestras_haciendas m
                INNER JOIN cat_haciendas h ON m.id_hacienda = h.`id`
                WHERE m.id_cliente = {$this->session->client} AND tipo_semana = 0 AND m.fecha IS NOT NULL
                GROUP BY id_hacienda, YEAR(m.fecha)");
            foreach($data as $row){
                $response[$row->label] = $row;
            }
            return $response;
        }
        private function getSemanas0SComp(){
            return $this->db->queryAll("SELECT WEEK(fecha) AS semana
                FROM muestras_haciendas m
                WHERE m.id_cliente = {$this->session->client} AND fecha IS NOT NULL AND tipo_semana = 0
                GROUP BY WEEK(fecha)");
        }

        public function getHT0SComparativo(){
            $response = new stdClass();

            $fincas = $this->getFincas0SComp();
            $semanas = $this->getSemanas0SComp();

            $data_chart_ht0s = [];

            foreach($semanas as $sem){
                $data_chart_ht0s[] = ["value" => 13, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hojas_totales) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana > 0 AND anio = {$finca->anio}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 0
                            AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + 1;
                    if(isset($data->value) && $data->value > 0){
                        $data_chart_ht0s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_ht0s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->ht = $this->chartInit($data_chart_ht0s,"vertical","","line");
            return json_encode($response);
        }

        public function getHMVLE0SComparativo(){
            $response = new stdClass();

            $fincas = $this->getFincas0SComp();
            $semanas = $this->getSemanas0SComp();

            $data_chart_hmvle0s = [];

            foreach($semanas as $sem){
                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hojas_mas_vieja_libre) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana > 0 AND anio = {$finca->anio}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 0
                            AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas));

                    if(isset($data->value) && $data->value > 0){
                        $data_chart_hmvle0s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_hmvle0s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->hmvle = $this->chartInit($data_chart_hmvle0s,"vertical","","line");
            return json_encode($response);
        
        }

        public function getHMVLQM0SComparativo(){
            $response = new stdClass();

            $fincas = $this->getFincas0SComp();
            $semanas = $this->getSemanas0SComp();

            $data_chart_hmvlqm0s = [];

            foreach($semanas as $sem){
                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hoja_mas_vieja_libre_quema_menor) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana > 0 AND anio = {$finca->anio}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 0
                            AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas));

                    if(isset($data->value) && $data->value > 0){
                        $data_chart_hmvlqm0s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_hmvlqm0s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->hmvlqm = $this->chartInit($data_chart_hmvlqm0s,"vertical","","line");
            return json_encode($response);
        }
    //END COMPARACION 0 SEM

    //BEGIN COMPARACION 11 SEM
        private function getFincas11SComp(){
            $response = [];
            $data = $this->db->queryAll("SELECT YEAR(m.fecha) AS anio, id_hacienda, CONCAT(YEAR(m.fecha), ' ', h.`nombre`) AS label
                FROM muestras_haciendas m
                INNER JOIN cat_haciendas h ON m.id_hacienda = h.`id`
                WHERE m.id_cliente = {$this->session->client} AND tipo_semana = 11 AND m.fecha IS NOT NULL
                GROUP BY id_hacienda, YEAR(m.fecha)");
            foreach($data as $row){
                $response[$row->label] = $row;
            }
            return $response;
        }
        private function getSemanas11SComp(){
            return $this->db->queryAll("SELECT WEEK(fecha) AS semana
                FROM muestras_haciendas m
                WHERE m.id_cliente = {$this->session->client} AND fecha IS NOT NULL AND tipo_semana = 11
                GROUP BY WEEK(fecha)");
        }


        public function getHT11SComparativo(){
            $response = new stdClass();

            $semanas = $this->getSemanas11SComp();
            $fincas = $this->getFincas11SComp();

            $data_chart_ht11s = [];

            foreach($semanas as $sem){
                $data_chart_ht11s[] = ["value" => 6.5, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hojas_totales) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana > 0 AND anio = {$finca->anio}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 11
                            AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label ,array_keys($fincas)) + 1;
                    if(isset($data->value) && $data->value > 0){
                        $data_chart_ht11s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_ht11s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->ht = $this->chartInit($data_chart_ht11s,"vertical","","line");
            return json_encode($response);
        }

        public function getHMVLQM11SComparativo(){
            $response = new stdClass();

            $semanas = $this->getSemanas11SComp();
            $fincas = $this->getFincas11SComp();

            $data_chart_hmvlqm11s = [];

            foreach($semanas as $sem){
                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hoja_mas_vieja_libre_quema_menor) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana > 0 AND anio = {$finca->anio}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 11
                            AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas));

                    if(isset($data->value) && $data->value > 0){
                        $data_chart_hmvlqm11s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_hmvlqm11s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->hmvlqm = $this->chartInit($data_chart_hmvlqm11s,"vertical","","line");
            return json_encode($response);
        }
    //END COMPARACION 11 SEM

    private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = []){
		$response = new stdClass;
		$response->chart = [];
		function object_to_array($obj) {
		    if(is_object($obj)) $obj = (array) $obj;
		    if(is_array($obj)) {
		        $new = array();
		        foreach($obj as $key => $val) {
		            $new[$key] = object_to_array($val);
		        }
		    }
		    else $new = $obj;
		    return $new;       
		}

		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["bottom"] = "0%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "20%";
			$response->chart["grid"]["containLabel"] = true;
			$response->chart["plotOptions"]["series"]["connectNulls"] = true;
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "value" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value" , "boundaryGap" => true , "axisLine" => ["onZero" => true]];
			}
			$response->chart["series"] = [];
			$count = 0;
			$positionxAxis = -1;
			$position = -1;
			$colors = [];
			if($type == "line"){
                $response->chart["yAxis"]["min"] = 'dataMin';
				$response->chart["xAxis"]["data"] = [];
			}
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = strtoupper($value->legend);
				$value->label = strtoupper($value->label);
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->positionxAxis)){
					$positionxAxis = $value->positionxAxis;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if($type == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						if(is_numeric($value->label)){
							$value->label = (double)$value->label;
						}
						if(!isset($value->positionxAxis)){
							$response->chart["xAxis"]["data"][] = $value->label;
						}else{
							$value->label = array($value->label);
							if(count($response->chart["xAxis"]["data"]) <= 0){
								while (count($response->chart["xAxis"]["data"]) <= $value->positionxAxis) {
									$response->chart["xAxis"]["data"][] = NULL;
								}
							}
							array_splice($response->chart["xAxis"]["data"] , $value->positionxAxis , 0 , $value->label);
							$response->chart["xAxis"]["data"] = array_filter($response->chart["xAxis"]["data"]);
						}
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"connectNulls" => true,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
                        if(isset($value->z)){
                            $response->chart["series"][$position]["z"] = $value->z;
                        }
					}
                    
					if(is_numeric($value->value)){
						$response->chart["series"][$position]["data"][] = ROUND($value->value,2);
					}else{
						$response->chart["series"][$position]["data"][] = $value->value;
					}

				}else{
					$response->chart["legend"]["data"][] = $value->label;
					// if($count == 0){
						$response->chart["series"][$count] = [
							"name" => $value->label,
							"type" => $type,
							"data" => [(int)$value->value],
							"label" => [
								"normal" => ["show" => true , "position" => "top"],
								"emphasis" => ["show" => true , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					// }
				}
				$count++;
			}
		}

		return $response->chart;
    }
    
}

?>