<?php

class InspeccionOrodeltiApp {
	
	private $bd;
	private $session;
	
	public function __construct() {
        $this->db = new M_Conexion();
        $this->session = Session::getInstance();
    }

    public function clientesDisponibles(){
        $sql = "SELECT id_cliente AS id, nombre
                FROM muestras_haciendas_app_orodelti m
                INNER JOIN cat_clientes ON id_cliente = cat_clientes.`id`
                WHERE m.id_usuario IN ({$this->session->logges->users})
                    AND YEAR(m.fecha) = {$this->session->year}
                GROUP BY id_cliente";
        $data = (object) $this->db->Consultas(2, $sql);
        return json_encode($data);
    }

    private function params(){
        $data = (object)json_decode(file_get_contents("php://input"));
        $params = new stdClass;
        $params->semana = $data->semana;
        $params->luz = $data->luz;
        $params->fecha_inicial = $data->fecha_inicial;
        $params->fecha_final = $data->fecha_final;
        $params->year = $data->year;
        $params->gerente = $data->gerente;
        return $params;
    }

    public function gerentes(){
        $response = new stdClass;
        $response->data = $this->db->queryAll("SELECT id, nombre AS label FROM cat_gerentes WHERE status = 1");
        return json_encode($response);
    }

    public function years(){
        $response = new stdClass;
        $response->selected = $this->db->queryOne("SELECT getYear(MAX(fecha)) AS anio FROM muestras_haciendas_app_orodelti");
        $response->data = $this->db->queryAll("SELECT getYear(fecha) AS id, getYear(fecha) AS label FROM muestras_haciendas_app_orodelti GROUP BY YEAR(fecha)");
        return json_encode($response);
    }

    public function index(){
        $response = new stdClass;
        $filters = $this->params();

        $response->semanas = $this->getSemanas();
        if($filters->semana == null){
            $keys = array_keys($response->semanas);
            $response->last_semana = $filters->semana = $keys[count($keys)-1];
        }
        $response->table_lote = $this->getLotes($filters);

        foreach($response->table_lote as $lote){
            $m3 = $this->db->queryRow("SELECT 
                    ROUND(AVG(IF(total_hoja_3 = 100 OR total_hoja_3 = 1, 1, 0)) * 100, 2) AS h3, 
                    ROUND(AVG(IF(total_hoja_4 = 100 OR total_hoja_4 = 1, 1, 0)) * 100, 2) AS h4, 
                    ROUND(AVG(IF(total_hoja_5 = 100 OR total_hoja_5 = 1, 1, 0)) * 100, 2) AS h5,
                    ROUND(AVG(hoja_mas_vieja_de_estrias)) AS hmvle,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor)) AS hmvlqm
                FROM muestras_hacienda_detalle_3M_orodelti
                WHERE id_lote = '{$lote->id}' AND getWeek(fecha) = '{$filters->semana}'");
            $lote->m3_h3 = $m3->h3;
            $lote->m3_h4 = $m3->h4;
            $lote->m3_h5 = $m3->h5;
            $lote->m3_hmvle = $m3->hmvle;
            $lote->m3_hmvlqm = $m3->hmvlqm;

            $s0 = $this->db->queryRow("SELECT 
                    ROUND(AVG(hojas_totales)) AS ht,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor)) AS hmvlqm,
                    ROUND(AVG(hojas_mas_vieja_libre)) AS hmvle
                FROM muestras_hacienda_detalle_orodelti
                WHERE id_lote = '{$lote->id}' AND tipo_semana = 0 AND getWeek(fecha) = '{$filters->semana}' AND anio = {$this->session->year} ");
            $lote->s0_ht = $s0->ht;
            $lote->s0_hmvlqm = $s0->hmvlqm;
            $lote->s0_hmvle = $s0->hmvle;

            $s11 = $this->db->queryRow("SELECT 
                    ROUND(AVG(hojas_totales)) AS ht,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor)) AS hmvlqm
                FROM muestras_hacienda_detalle_orodelti
                WHERE id_lote = '{$lote->id}' AND tipo_semana = 11 AND getWeek(fecha) = '{$filters->semana}' AND anio = {$this->session->year} ");
            $lote->s11_ht = $s11->ht;
            $lote->s11_hmvlqm = $s11->hmvlqm;
        }

        return json_encode($response);
    }

    public function getAplicaciones(){
        $response = new stdClass;
        $sql = "SELECT fecha, IF(ciclo = '(NULL)', '', ciclo) AS ciclo, IF(producto_1 IS NULL OR producto_1 = '(NULL)', '', producto_1) AS producto_1, IF(producto_2 = '(NULL)', '', producto_2) AS producto_2
                FROM ciclos_aplicacion
                WHERE getYear(fecha) = {$this->session->year}  AND id_finca = '{$this->session->finca}' AND ((producto_1 != '' AND producto_1 != '(NULL)') OR (producto_2 != '' AND producto_2 != '(NULL)'))
                ORDER BY fecha";
        $response->data = $this->db->queryAll($sql);
        return json_encode($response);
    }

    private function getSemanas($filters = []){
        $response = new stdClass;
        $sql = "SELECT id, label FROM(
            SELECT getWeek(fecha) AS id, getWeek(fecha) AS label 
            FROM muestras_haciendas_app_orodelti
            WHERE YEAR(fecha) = {$this->session->year}
                AND AND id_finca = '{$this->session->finca}' 
                AND muestras_haciendas_app_orodelti.id_cliente = '{$this->session->client}' 
                AND muestras_haciendas_app_orodelti.id_usuario IN ('{$this->session->logges->users}')
            GROUP BY getWeek(fecha)
            UNION ALL
            SELECT getWeek(fecha) AS id, getWeek(fecha) AS label 
            FROM muestras_haciendas_3M_app_orodelti
            WHERE YEAR(fecha) = {$this->session->year}
                AND id_finca = '{$this->session->finca}' 
                AND muestras_haciendas_3M_app_orodelti.id_cliente = '{$this->session->client}' 
                AND muestras_haciendas_3M_app_orodelti.id_usuario IN ('{$this->session->logges->users}')
            GROUP BY getWeek(fecha)
        ) AS tbl
        GROUP BY id
        ORDER BY id";
        $response = $this->db->queryAllSpecial($sql);
        return $response;
    }

    private function getLotes($filters = []){
        if($filters->semana >= 0){
            $sWhere .= "AND semana = '{$filters->semana}'";
            $sWhere3 .= "AND semana = '{$filters->semana}'";
        }
        $sql = "SELECT id, UPPER(lote) AS lote
                FROM(
                    SELECT cat_lotes.id, cat_lotes.`nombre` AS lote
                    FROM muestras_haciendas_app_orodelti
                    INNER JOIN muestras_hacienda_detalle_orodelti detalle ON id_Mhacienda = muestras_haciendas_app_orodelti.`id`
                    INNER JOIN cat_lotes ON cat_lotes.id = id_lote
                    WHERE anio = {$this->session->year} 
                        AND AND id_finca = '{$this->session->finca}' 
                        AND muestras_haciendas_app_orodelti.id_cliente = '{$this->session->client}' 
                        AND muestras_haciendas_app_orodelti.id_usuario IN ('{$this->session->logges->users}') $sWhere
                    GROUP BY cat_lotes.id 
                    UNION ALL
                    SELECT cat_lotes.id, cat_lotes.`nombre` AS lote 
                    FROM muestras_haciendas_3M_app_orodelti
                    INNER JOIN muestras_hacienda_detalle_3M_orodelti detalle ON id_Mhacienda = muestras_haciendas_3M_app_orodelti.`id`
                    INNER JOIN cat_lotes ON cat_lotes.id = id_lote
                    WHERE anio = {$this->session->year} 
                        AND id_finca = '{$this->session->finca}' 
                        AND muestras_haciendas_3M_app_orodelti.id_cliente = '{$this->session->client}' 
                        AND muestras_haciendas_3M_app_orodelti.id_usuario IN ('{$this->session->logges->users}') $sWhere3
                    GROUP BY cat_lotes.id
                ) AS lotes
                GROUP BY id
                ORDER BY lote";
        return $this->db->queryAll($sql);
    }

    //BEGIN 3 M
        private function getSemanas3M(){
            $params = $this->params();

            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }

            $sql = "SELECT semana
                        FROM orodelti_plantas_muestras 
                        WHERE semana > 0 AND anio = {$params->year} $sWhere
                        GROUP BY semana
                        ORDER BY semana";
            return $this->db->queryAll($sql);
        }

        private function getFincas(){
            $params = $this->params();

            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id IN({$ids_fincas})";
            }

            $sql = "SELECT id, nombre AS label
                        FROM cat_fincas
                        WHERE status > 0 $sWhere";
            return $this->db->queryAllSpecial($sql);
        }

        public function get3M(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $fincas = $this->getFincas(); 
            $params = $this->params();

            $data_chart = [];
            $max = null;
            $min = null;

            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            foreach($semanas as $sem){
                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                AVG(3m_hvle) AS value
                            FROM orodelti_plantas_muestras
                            WHERE anio = {$params->year}
                                AND id_finca = '{$id_finca}'
                                AND semana = {$sem->semana}
                                $sWhere";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value)){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }

                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line");

            return json_encode($response);
        }

        public function getHVLEAFA(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $fincas = $this->getFincas(); 
            $params = $this->params();

            $data_chart = [];
            $max = null;
            $min = null;

            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            foreach($semanas as $sem){
                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                AVG(3m_hvle_afa) AS value
                            FROM orodelti_plantas_muestras
                            WHERE anio = {$params->year}
                                AND id_finca = '{$id_finca}'
                                AND semana = {$sem->semana}
                                $sWhere";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value)){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }

                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line");

            return json_encode($response);
        }

        public function getHVLQ(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $fincas = $this->getFincas(); 
            $params = $this->params();

            $data_chart = [];
            $max = null;
            $min = null;

            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            foreach($semanas as $sem){
                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                AVG(3m_hvlq) AS value
                            FROM orodelti_plantas_muestras
                            WHERE anio = {$params->year}
                                AND id_finca = '{$id_finca}'
                                AND semana = {$sem->semana}
                                $sWhere";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value)){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }

                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line");

            return json_encode($response);
        }

        public function getHVLQAFA(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $fincas = $this->getFincas(); 
            $params = $this->params();

            $data_chart = [];
            $max = null;
            $min = null;

            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            foreach($semanas as $sem){
                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                AVG(3m_hvlq_afa) AS value
                            FROM orodelti_plantas_muestras
                            WHERE anio = {$params->year}
                                AND id_finca = '{$id_finca}'
                                AND semana = {$sem->semana}
                                $sWhere";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value)){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }

                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line");

            return json_encode($response);
        }

        public function getH4(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $fincas = $this->getFincas();
            $params = $this->params();
            $max = null;
            $min = null;

            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            
            foreach($semanas as $sem){
                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                AVG(IF(incidencia = 100 OR incidencia = 1, 1, 0)) AS 'h4'
                            FROM orodelti_plantas_muestras muestra
                            INNER JOIN orodelti_plantas_muestras_hojas hojas ON id_muestra = muestra.id
                            WHERE anio = {$params->year}
                                AND id_finca = '{$id_finca}' 
                                AND semana = {$sem->semana}
                                AND hojas.num_hoja = 4
                                $sWhere";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->h4)){
                        if($max == null || $max < $data->h4) $max = $data->h4;
                        if($min == null || $min > $data->h4) $min = $data->h4;
                        $data_chart_h4[] = ["value" => $data->h4 * 100, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_h4[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->h4 = $this->chartInit($data_chart_h4, "vertical" , "" , "line");
            return json_encode($response);
        }

        public function getH3(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $fincas = $this->getFincas();
            $params = $this->params();
            $max = null;
            $min = null;

            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            
            foreach($semanas as $sem){
                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                IFNULL(AVG(IF(incidencia = 100 OR incidencia = 1, 1, 0)), 0) AS 'h3'
                            FROM orodelti_plantas_muestras muestra
                            INNER JOIN orodelti_plantas_muestras_hojas hojas ON id_muestra = muestra.id
                            WHERE anio = {$params->year}
                                AND id_finca = '{$id_finca}'
                                AND semana = {$sem->semana}
                                AND num_hoja = 3
                                $sWhere";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->h3)){
                        if($max == null || $max < $data->h3 * 100) $max = $data->h3 * 100;
                        if($min == null || $min > $data->h3 * 100) $min = $data->h3 * 100;
                        $data_chart_h3[] = ["value" => $data->h3 * 100, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_h3[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }   

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->h3 = $this->chartInit($data_chart_h3, "vertical" , "" , "line");

            return json_encode($response);
        }

        public function getH2(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $fincas = $this->getFincas();
            $params = $this->params();
            $max = null;
            $min = null;

            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            
            foreach($semanas as $sem){
                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                IFNULL(AVG(IF(incidencia = 100 OR incidencia = 1, 1, 0)), 0) AS 'h2'
                            FROM orodelti_plantas_muestras muestra
                            INNER JOIN orodelti_plantas_muestras_hojas hojas ON id_muestra = muestra.id
                            WHERE anio = {$params->year}
                                AND id_finca = '{$id_finca}'
                                AND semana = {$sem->semana}
                                AND num_hoja = 2
                                $sWhere";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->h2)){
                        if($max == null || $max < $data->h2 * 100) $max = $data->h2 * 100;
                        if($min == null || $min > $data->h2 * 100) $min = $data->h2 * 100;
                        $data_chart_h3[] = ["value" => $data->h2 * 100, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_h3[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }   

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->h2 = $this->chartInit($data_chart_h3, "vertical" , "" , "line");

            return json_encode($response);
        }

        public function getH5(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $fincas = $this->getFincas();
            $params = $this->params();
            $max = null;
            $min = null;
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }

            foreach($semanas as $sem){
                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                AVG(IF(incidencia = 100 OR incidencia = 1, 1, 0)) AS 'h5'
                            FROM orodelti_plantas_muestras muestra
                            INNER JOIN orodelti_plantas_muestras_hojas hojas ON id_muestra = muestra.id
                            WHERE anio = {$params->year}
                                AND id_finca = '{$id_finca}' 
                                AND semana = {$sem->semana}
                                AND num_hoja = 5
                                {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->h5)){
                        if($max == null || $max < $data->h5) $max = $data->h5;
                        if($min == null || $min > $data->h5) $min = $data->h5;
                        $data_chart_h5[] = ["value" => $data->h5 * 100, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_h5[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->h5 = $this->chartInit($data_chart_h5, "vertical" , "" , "line");

            return json_encode($response);
        }

        public function getHMVLQM(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $fincas = $this->getFincas();
            $max = null;
            $min = null;
            $data_chart_hmvlqm = [];
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            
            foreach($semanas as $sem){                
                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                AVG(3m_hvlq) AS hmvlqm
                            FROM orodelti_plantas_muestras muestra
                            WHERE anio = {$this->session->year}
                                AND id_finca = '{$id_finca}'
                                AND semana = {$sem->semana}
                                $sWhere";
                                
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->hmvlqm) && $data->hmvlqm > 0){
                        if($max == null || $max < $data->hmvlqm) $max = $data->hmvlqm;
                        if($min == null || $min > $data->hmvlqm) $min = $data->hmvlqm;
                        $data_chart_hmvlqm[] = ["value" => $data->hmvlqm, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->hmvlqm = $this->chartInit($data_chart_hmvlqm, "vertical" , "" , "line");
            return json_encode($response);
        }
    //END 3 M

    //BEGIN 0S
        private function getSemanas0S(){
            $params = $this->params();

            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }

            $sql = "SELECT semana
                        FROM orodelti_plantas_muestras muestra
                        WHERE semana > 0 AND anio = {$params->year} $sWhere
                        GROUP BY semana
                        ORDER BY semana";
            return $this->db->queryAll($sql);
        }

        public function getHT0S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $data_chart_ht0s = [];
            $min = null;
            $max = null;
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
           
            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                AVG(0s_ht) AS value
                        FROM orodelti_plantas_muestras muestra
                        WHERE anio = {$params->year}
                            AND id_finca = '{$id_finca}'
                            AND semana = {$sem->semana}
                            {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_ht0s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_ht0s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->ht = $this->chartInit($data_chart_ht0s,"vertical","","line");
            return json_encode($response);
        }

        public function getHTAFA0S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $data_chart_ht0s = [];
            $min = null;
            $max = null;
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
           
            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                AVG(0s_ht_afa) AS value
                        FROM orodelti_plantas_muestras muestra
                        WHERE anio = {$params->year}
                            AND id_finca = '{$id_finca}'
                            AND semana = {$sem->semana}
                            {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_ht0s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_ht0s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->ht = $this->chartInit($data_chart_ht0s,"vertical","","line");
            return json_encode($response);
        }

        public function getHVLE0S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $data_chart_hmvlqm0s = [];

            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }

            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                            AVG(0s_hvle) AS value
                        FROM orodelti_plantas_muestras muestra
                        WHERE anio = {$params->year}
                            AND id_finca = '{$id_finca}'
                            AND semana = {$sem->semana}
                            {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm0s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm0s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->hmvle = $this->chartInit($data_chart_hmvlqm0s,"vertical","","line");
            return json_encode($response);
        }

        public function getHVLQM0S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $data_chart_hmvlqm0s = [];
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            
            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                            AVG(0s_hvlq) AS value
                        FROM orodelti_plantas_muestras muestra
                        WHERE anio = {$params->year}
                            AND id_finca = '{$id_finca}'
                            AND semana = {$sem->semana}
                            {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm0s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm0s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->data_chart = $this->chartInit($data_chart_hmvlqm0s,"vertical","","line");
            
            return json_encode($response); 
        }

        public function getHVLQAFA0S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $data_chart_hmvlqm0s = [];
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            
            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                            AVG(0s_hvlq_afa) AS value
                        FROM orodelti_plantas_muestras muestra
                        WHERE anio = {$params->year}
                            AND id_finca = '{$id_finca}'
                            AND semana = {$sem->semana}
                            {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm0s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm0s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->data_chart = $this->chartInit($data_chart_hmvlqm0s,"vertical","","line");
            
            return json_encode($response); 
        }

        public function getHVLQM50S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $data_chart_hmvlqm0s = [];
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            
            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                            AVG(0s_hvlq_mayor_5) AS value
                        FROM orodelti_plantas_muestras muestra
                        WHERE anio = {$params->year}
                            AND id_finca = '{$id_finca}'
                            AND semana = {$sem->semana}
                            {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm0s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm0s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->data_chart = $this->chartInit($data_chart_hmvlqm0s,"vertical","","line");
            
            return json_encode($response); 
        }
    //END 0S

    //BEGIN 0S
        private function getSemanas6S(){
            $params = $this->params();

            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }

            $sql = "SELECT semana
                        FROM orodelti_plantas_muestras muestra
                        WHERE semana > 0 AND anio = {$params->year} $sWhere
                        GROUP BY semana
                        ORDER BY semana";
            return $this->db->queryAll($sql);
        }

        public function getHT6S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $data_chart_ht0s = [];
            $min = null;
            $max = null;
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
        
            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                AVG(6s_ht) AS value
                        FROM orodelti_plantas_muestras muestra
                        WHERE anio = {$params->year}
                            AND id_finca = '{$id_finca}'
                            AND semana = {$sem->semana}
                            {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_ht0s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_ht0s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->ht = $this->chartInit($data_chart_ht0s,"vertical","","line");
            return json_encode($response);
        }

        public function getHTAFA6S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $data_chart_ht0s = [];
            $min = null;
            $max = null;
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
        
            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                AVG(6s_ht_afa) AS value
                        FROM orodelti_plantas_muestras muestra
                        WHERE anio = {$params->year}
                            AND id_finca = '{$id_finca}'
                            AND semana = {$sem->semana}
                            {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_ht0s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_ht0s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->ht = $this->chartInit($data_chart_ht0s,"vertical","","line");
            return json_encode($response);
        }

        public function getHVLE6S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $data_chart_hmvlqm0s = [];

            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }

            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                            AVG(6s_hvle) AS value
                        FROM orodelti_plantas_muestras muestra
                        WHERE anio = {$params->year}
                            AND id_finca = '{$id_finca}'
                            AND semana = {$sem->semana}
                            {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm0s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm0s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->hmvle = $this->chartInit($data_chart_hmvlqm0s,"vertical","","line");
            return json_encode($response);
        }

        public function getHVLQM6S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $data_chart_hmvlqm0s = [];
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            
            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                            AVG(6s_hvlq) AS value
                        FROM orodelti_plantas_muestras muestra
                        WHERE anio = {$params->year}
                            AND id_finca = '{$id_finca}'
                            AND semana = {$sem->semana}
                            {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm0s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm0s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->data_chart = $this->chartInit($data_chart_hmvlqm0s,"vertical","","line");
            
            return json_encode($response); 
        }

        public function getHVLQAFA6S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $data_chart_hmvlqm0s = [];
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            
            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                            AVG(6s_hvlq_afa) AS value
                        FROM orodelti_plantas_muestras muestra
                        WHERE anio = {$params->year}
                            AND id_finca = '{$id_finca}'
                            AND semana = {$sem->semana}
                            {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm0s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm0s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->data_chart = $this->chartInit($data_chart_hmvlqm0s,"vertical","","line");
            
            return json_encode($response); 
        }

        public function getHVLQM56S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $data_chart_hmvlqm0s = [];
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            
            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                            AVG(6s_hvlq_mayor_5) AS value
                        FROM orodelti_plantas_muestras muestra
                        WHERE anio = {$params->year}
                            AND id_finca = '{$id_finca}'
                            AND semana = {$sem->semana}
                            {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm0s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm0s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->data_chart = $this->chartInit($data_chart_hmvlqm0s,"vertical","","line");
            
            return json_encode($response); 
        }
    //END 0S

    //BEGIN 11S
        private function getSemanas11S(){ 
            $params = $this->params();

            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }

            $sql = "SELECT semana
                        FROM orodelti_plantas_muestras muestra
                        WHERE semana > 0 AND anio = {$params->year} $sWhere
                        GROUP BY semana
                        ORDER BY semana";
            return $this->db->queryAll($sql);
        }

        public function getHT11S(){
            $response = new stdClass();

            $semanas = $this->getSemanas11S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $data_chart_ht11s = [];
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            
            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                            AVG(11s_ht) AS value
                        FROM orodelti_plantas_muestras muestra
                        WHERE semana >= 0 AND anio = {$params->year}
                            AND id_finca = '{$id_finca}' 
                            AND semana = {$sem->semana}
                            {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_ht11s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_ht11s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->ht = $this->chartInit($data_chart_ht11s,"vertical","","line");
            return json_encode($response);
        }

        public function getHTAFA11S(){
            $response = new stdClass();

            $semanas = $this->getSemanas11S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $data_chart_ht11s = [];
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND id_finca IN({$ids_fincas})";
            }
            
            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                            AVG(11s_ht_afa) AS value
                        FROM orodelti_plantas_muestras muestra
                        WHERE semana >= 0 AND anio = {$params->year}
                            AND id_finca = '{$id_finca}' 
                            AND semana = {$sem->semana}
                            {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_ht11s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_ht11s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->data_chart = $this->chartInit($data_chart_ht11s,"vertical","","line");
            return json_encode($response);
        }

        public function getHVLQ11S(){
            $response = new stdClass();

            $semanas = $this->getSemanas11S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $max = null;
            $min = null;
            $data_chart_hmvlqm11s = [];
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND AND id_finca IN({$ids_fincas})";
            }

            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                AVG(11s_hvlq) AS value
                            FROM orodelti_plantas_muestras muestra
                            WHERE semana >= 0 AND anio = {$params->year}
                                AND id_finca = '{$id_finca}' 
                                AND semana = {$sem->semana}
                                {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm11s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm11s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->data_chart = $this->chartInit($data_chart_hmvlqm11s,"vertical","","line");
            return json_encode($response);
        }

        public function getHVLQAFA11S(){
            $response = new stdClass();

            $semanas = $this->getSemanas11S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $max = null;
            $min = null;
            $data_chart_hmvlqm11s = [];
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND AND id_finca IN({$ids_fincas})";
            }

            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                AVG(11s_hvlq_afa) AS value
                            FROM orodelti_plantas_muestras muestra
                            WHERE semana >= 0 AND anio = {$params->year}
                                AND id_finca = '{$id_finca}' 
                                AND semana = {$sem->semana}
                                {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm11s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm11s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->data_chart = $this->chartInit($data_chart_hmvlqm11s,"vertical","","line");
            return json_encode($response);
        }

        public function getHVLE11S(){
            $response = new stdClass();

            $semanas = $this->getSemanas11S();
            $fincas = $this->getFincas();
            $params = $this->params();
            $max = null;
            $min = null;
            $data_chart_hmvlqm11s = [];
            
            $sWhere = "";
            if($params->gerente > 0){
                $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
                $sWhere .= " AND AND id_finca IN({$ids_fincas})";
            }

            foreach($semanas as $sem){

                foreach($fincas as $id_finca => $finca){
                    $sql = "SELECT 
                                AVG(11s_hvle) AS value
                            FROM orodelti_plantas_muestras muestra
                            WHERE semana >= 0 AND anio = {$params->year}
                                AND id_finca = '{$id_finca}' 
                                AND semana = {$sem->semana}
                                {$sWhere}";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($id_finca, array_keys($fincas));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm11s[] = ["value" => $data->value, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm11s[] = ["value" => NULL, "legend" => $finca, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->data_chart = $this->chartInit($data_chart_hmvlqm11s,"vertical","","line");
            return json_encode($response);
        }
    //END 11S

    //BEGIN CLIMA
        private function getSemanasClima(){ 
            $sql = "SELECT semana
                        FROM datos_clima 
                        WHERE semana >= 0 AND YEAR(fecha) = {$this->session->year}
                            AND id_cliente = '{$this->session->client}' 
                            AND id_usuario IN ('{$this->session->logges->users}')
                        GROUP BY semana
                        ORDER BY semana";
            return $this->db->queryAll($sql);
        }

        private function getEstacionesClima(){
            $sql = "SELECT estacion_id AS id, (SELECT GROUP_CONCAT(h.alias SEPARATOR ' | ') FROM estaciones_haciendas INNER JOIN cat_haciendas h ON id_hacienda = h.id WHERE estacion_id = datos_clima.estacion_id) AS label
                    FROM datos_clima 
                    WHERE YEAR(fecha) = {$this->session->year}
                        AND id_cliente = '{$this->session->client}' 
                        AND id_usuario IN ('{$this->session->logges->users}')
                    GROUP BY estacion_id
                    ORDER BY estacion_id";
            return $this->db->queryAllSpecial($sql);
        }

        private function getHorasClima($fecha){
            $sql = "SELECT HOUR(hora) AS id, DATE_FORMAT(CONCAT(CURRENT_DATE,' ', hora), '%h %p') AS label
                    FROM datos_clima 
                    WHERE YEAR(fecha) = {$this->session->year}
                        AND id_cliente = '{$this->session->client}' 
                        AND id_usuario IN ('{$this->session->logges->users}')
                        AND fecha = '{$fecha}'
                    GROUP BY HOUR(hora)
                    ORDER BY HOUR(hora)";
            return $this->db->queryAllSpecial($sql);
        }

        public function getTempMin(){
            $response = new stdClass();
            
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];
            $max = $min = 21;
            foreach($semanas as $sem){
                $data_chart[] = ["value" => 21, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT MIN(temp_minima) AS value
                            FROM datos_clima_resumen
                            WHERE anio = {$this->session->year}
                                AND semana = {$sem->semana}
                                AND estacion_id = '{$estacion}'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones)) + 1;
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = floor(round($min - (($max - $min) * .1), 2));
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = ceil(round($max + (($max - $min) * .1), 2));
            $response->data = $this->chartInit($data_chart,"vertical","","line", ["min" => $response->min, "max" => $response->max]);
            return json_encode($response);
        }

        public function getTempMax(){
            $response = new stdClass();
            
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];
            $max = null;
            $min = null;
            foreach($semanas as $sem){

                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT MAX(temp_maxima) AS value
                            FROM datos_clima_resumen
                            WHERE anio = {$this->session->year}
                                AND temp_maxima > 0
                                AND semana = {$sem->semana}
                                AND estacion_id = '{$estacion}'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = floor(round($min - (($max - $min) * .1), 2));
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = ceil(round($max + (($max - $min) * .1), 2));
            $response->data = $this->chartInit($data_chart,"vertical","","line", ["min" => $response->min, "max" => $response->max]);
            return json_encode($response);
        }

        public function getHumedadMax(){
            $response = new stdClass();
            
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];
            $max = null;
            $min = null;
            foreach($semanas as $sem){

                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT MAX(humedad) AS value
                            FROM datos_clima_resumen
                            WHERE anio = {$this->session->year}
                                AND humedad > 0
                                AND semana = {$sem->semana}
                                AND estacion_id = '{$estacion}'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = floor(round($min - (($max - $min) * .1), 2));
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = ceil(round($max + (($max - $min) * .1), 2));
            $response->data = $this->chartInit($data_chart,"vertical","","line", ["min" => $response->min, "max" => $response->max]);
            return json_encode($response);
        }

        public function getLluvia(){
            $response = new stdClass();
            
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];
            $max = null;
            $min = null;
            foreach($semanas as $sem){
                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT SUM(IFNULL(horas_lluvia, 0)) AS value
                            FROM datos_clima_resumen
                            WHERE anio = {$this->session->year}
                                AND semana = {$sem->semana}
                                AND estacion_id = '{$estacion}'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value)){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = floor(round($min - (($max - $min) * .1), 2));
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = ceil(round($max + (($max - $min) * .1), 2));
            $response->data = $this->chartInit($data_chart,"vertical","","line",["min" => $response->min, "max" => $response->max], true);
            return json_encode($response);
        }

        public function getRadSolar(){
            $response = new stdClass();
            
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];
            $max = null;
            $min = null;
            foreach($semanas as $sem){
                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT value
                            FROM(
                                SELECT SUM(rad_solar) AS value, estacion_id
                                FROM datos_clima_resumen
                                WHERE anio = {$this->session->year}
                                    AND semana = {$sem->semana}
                                    AND estacion_id = '{$estacion}'
                                GROUP BY id_hacienda
                            ) AS tbl
                            GROUP BY estacion_id";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = floor(round($min - (($max - $min) * .1), 2));
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = ceil(round($max + (($max - $min) * .1), 2));
            $response->data = $this->chartInit($data_chart,"vertical","","line", ["min" => $response->min, 'max' => $response->max]);
            return json_encode($response);
        }

        public function getHorasLuz(){
            $response = new stdClass();
            
            $filters = $this->params();
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];
            $max = null;
            $min = null;
            foreach($semanas as $sem){
                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT AVG(value) AS value
                            FROM (
                                SELECT SUM(horas_luz_{$filters->luz}) AS value, estacion_id
                                FROM datos_clima_resumen
                                WHERE anio = {$this->session->year}
                                    AND semana = {$sem->semana}
                                    AND estacion_id = '{$estacion}'
                                GROUP BY id_hacienda
                            ) AS tbl
                            GROUP BY estacion_id";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        if($max == null || $max < $data->value) $max = $data->value;
                        if($min == null || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = floor(round($min - (($max - $min) * .1), 2));
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = ceil(round($max + (($max - $min) * .1), 2));
            $response->data = $this->chartInit($data_chart,"vertical","","line", ["min" => $response->min, "max" => $response->max]);
            return json_encode($response);
        }

        public function getRadSolarHoras(){
            $response = new stdClass();
            
            $filters = $this->params();
            if($filters->fecha_inicial == "" && $filters->fecha_final == ""){
                $filters->fecha_inicial = $this->db->queryRow("SELECT MAX(fecha) AS fecha FROM datos_clima WHERE YEAR(fecha) = {$this->session->year} AND id_cliente = '{$this->session->client}' AND id_usuario IN ('{$this->session->logges->users}') AND rad_solar > 0")->fecha;
                $filters->fecha_final = $filters->fecha_inicial;

                $response->fecha = $filters->fecha_inicial;
            }

            $horas = $this->getHorasClima($filters->fecha_inicial);
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];

            foreach($horas as $hora => $label){

                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT AVG(rad_solar) AS value
                            FROM datos_clima
                            WHERE YEAR(fecha) = {$this->session->year}
                                AND id_cliente = '{$this->session->client}' 
                                AND id_usuario IN ({$this->session->logges->users})
                                AND HOUR(hora) = {$hora}
                                AND estacion_id = '{$estacion}'
                                AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $label, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $label, "position" => $position];
                    }
                }
            }

            $response->data = $this->chartInit($data_chart,"vertical","","line");
            return json_encode($response);
        }
    //END CLIMA

    public function getEmisionFoliar(){
        $params = $this->params();
        $sWhere = "";
        if($params->gerente > 0){
            $ids_fincas = $this->db->queryOne("SELECT GROUP_CONCAT(id SEPARATOR ',') FROM cat_fincas WHERE id_gerente = {$params->gerente}");
            $sWhere .= " AND id_finca IN({$ids_fincas})";
        }

        $sql = "SELECT emision_foliar AS 'value', fincas.nombre AS legend, semanas.semana AS label, 1 selected
                FROM (
                    SELECT semana
                    FROM orodelti_foliar foliar
                    WHERE anio = {$params->year} {$sWhere}
                    GROUP BY semana
                ) AS semanas
                JOIN (
                    SELECT id_finca, fincas.nombre
                    FROM orodelti_foliar foliar
                    INNER JOIN cat_fincas fincas ON foliar.id_finca = fincas.id
                    WHERE anio = {$params->year} {$sWhere}
                    GROUP BY id_finca
                ) AS fincas
                INNER JOIN (
                    SELECT emision_foliar, semana, id_finca
                    FROM orodelti_foliar foliar
                    WHERE anio = {$params->year} {$sWhere}
                ) AS data ON semanas.semana = data.semana AND fincas.id_finca = data.id_finca";
        $data_chart = $this->db->queryAll($sql);

        $fincas = [];
        foreach($data_chart as $row){
            if(!in_array($row->legend, $fincas)) $fincas[] = $row->legend;

            $position = array_search($row->legend, $fincas);
            $row->position = $position;
        }

        $response->data = $this->chartInit($data_chart, "vertical", "", "line");
		return json_encode($response);
    }

    private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = [], $zoom = false){
		$response = new stdClass;
		$response->chart = [];
		function object_to_array($obj) {
		    if(is_object($obj)) $obj = (array) $obj;
		    if(is_array($obj)) {
		        $new = array();
		        foreach($obj as $key => $val) {
		            $new[$key] = object_to_array($val);
		        }
		    }
		    else $new = $obj;
		    return $new;       
		}

		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["bottom"] = ($zoom) ? "90%" : "0%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "20%";
			$response->chart["grid"]["containLabel"] = true;
            $response->chart["plotOptions"]["series"]["connectNulls"] = true;
            if($zoom){
                $response->chart["dataZoom"] = [
                    [
                        "show" => true,
                        "realtime" => true
                    ],
                    [
                        "type" => "inside",
                        "realtime" => true
                    ]
                ];
            }
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "value" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value" , "boundaryGap" => true , "axisLine" => ["onZero" => true]];
			}
			$response->chart["series"] = [];
			$count = 0;
			$positionxAxis = -1;
			$position = -1;
			$colors = [];
			if($type == "line"){
				$response->chart["xAxis"]["data"] = [];
            }

			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = strtoupper($value->legend);
				$value->label = strtoupper($value->label);
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->positionxAxis)){
					$positionxAxis = $value->positionxAxis;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if($type == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						if(is_numeric($value->label)){
							$value->label = (double)$value->label;
						}
						if(!isset($value->positionxAxis)){
							$response->chart["xAxis"]["data"][] = $value->label;
						}else{
							$value->label = array($value->label);
							if(count($response->chart["xAxis"]["data"]) <= 0){
								while (count($response->chart["xAxis"]["data"]) <= $value->positionxAxis) {
									$response->chart["xAxis"]["data"][] = NULL;
								}
							}
							array_splice($response->chart["xAxis"]["data"] , $value->positionxAxis , 0 , $value->label);
							$response->chart["xAxis"]["data"] = array_filter($response->chart["xAxis"]["data"]);
						}
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"connectNulls" => true,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
                        if(isset($value->z)){
                            $response->chart["series"][$position]["z"] = $value->z;
                        }
					}
                    
					if(is_numeric($value->value)){
						$response->chart["series"][$position]["data"][] = ROUND($value->value,2);
					}else{
						$response->chart["series"][$position]["data"][] = $value->value;
					}

				}else{
					$response->chart["legend"]["data"][] = $value->label;
					// if($count == 0){
						$response->chart["series"][$count] = [
							"name" => $value->label,
							"type" => $type,
							"data" => [(int)$value->value],
							"label" => [
								"normal" => ["show" => true , "position" => "top"],
								"emphasis" => ["show" => true , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					// }
				}
				$count++;
            }
            if(count($response->chart["xAxis"]["data"]) < 2){
                $response->chart["yAxis"]["max"] = (isset($minMax["max"])) ? $minMax["max"] : null;
                $response->chart["yAxis"]["min"] = (isset($minMax["min"])) ? $minMax["min"] : 0;
                foreach($response->chart["series"] as $key => $data){
                    #if($response->chart["series"][$key]["name"] != 'UMBRAL'){
                        $response->chart["series"][$key]["type"] = "bar";
                    #}
                }
            }
		}

		return $response->chart;
    }
}

?>