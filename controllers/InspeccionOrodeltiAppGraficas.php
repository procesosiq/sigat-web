<?php

class InspeccionOrodeltiAppGraficas {
	
	private $bd;
	private $session;
	
	public function __construct() {
        $this->db = new M_Conexion();
        $this->session = Session::getInstance();
    }

    private function params(){
        $data = (object)json_decode(file_get_contents("php://input"));
        $params = new stdClass;
        $params->week = (int) $data->week;
        $params->year = (int) $data->year;
        $params->gerente = (int) $data->gerente;
        return $params;
    }

    public function index(){
        $response = new stdClass;
        $params = $this->params();
        $response->years = $this->db->queryAllOne("SELECT anio FROM orodelti_plantas_muestras GROUP BY anio");
        if(!$params->year > 0){
            $response->last_year = $this->db->queryOne("SELECT MAX(anio) FROM orodelti_plantas_muestras");
            $params->year = $response->last_year;
        }
        $response->weeks = $this->db->queryAllOne("SELECT semana FROM orodelti_plantas_muestras WHERE anio = $params->year GROUP BY semana");
        $response->last_week = $this->db->queryOne("SELECT MAX(semana) FROM orodelti_plantas_muestras WHERE anio = $params->year");
        $response->gerentes = $this->db->queryAll("
            SELECT gere.`id`, gere.`nombre`
            FROM `orodelti_plantas_muestras` m
            INNER JOIN cat_fincas fincas ON m.id_finca = fincas.id
            INNER JOIN cat_gerentes gere ON fincas.`id_gerente` = gere.id
            GROUP BY gere.`id`
        ");
        
        return json_encode($response);
    }

    public function data(){
        $response = new stdClass;
        $params = $this->params();
        $response->status = 200;
        $response->data = [];
        $response->graficas = [];

        // Filters
        $sWhere = "";
        if($params->gerente > 0){
            $sWhere .= " AND id_finca IN (SELECT id FROM cat_finca WHERE id_gerente = $params->gerente)";
        }
        if($params->year > 0){
            $sWhere .= " AND anio = {$params->year}";
        }
        if($params->week > 0){
            $sWhere .= " AND semana = {$params->week}";
        }

        // Datatable
            $sql = "SELECT *,
                        ROUND((
                            SELECT SUM(incidencia) 
                            FROM orodelti_plantas_muestras m
                            INNER JOIN `orodelti_plantas_muestras_hojas` ON id_muestra = m.id AND num_hoja = 2
                            WHERE finca = tbl.finca $sWhere
                        ) / m3_ht * 100, 2) AS hoja_2,
                        ROUND((
                            SELECT SUM(incidencia) 
                            FROM orodelti_plantas_muestras m
                            INNER JOIN `orodelti_plantas_muestras_hojas` ON id_muestra = m.id AND num_hoja = 3
                            WHERE finca = tbl.finca $sWhere
                        ) / m3_ht * 100, 2) AS hoja_3,
                        ROUND((
                            SELECT SUM(incidencia) 
                            FROM orodelti_plantas_muestras m
                            INNER JOIN `orodelti_plantas_muestras_hojas` ON id_muestra = m.id AND num_hoja = 4
                            WHERE finca = tbl.finca $sWhere
                        ) / m3_ht * 100, 2) AS hoja_4,
                        ROUND((
                            SELECT SUM(incidencia) 
                            FROM orodelti_plantas_muestras m
                            INNER JOIN `orodelti_plantas_muestras_hojas` ON id_muestra = m.id AND num_hoja = 5
                            WHERE finca = tbl.finca $sWhere
                        ) / m3_ht * 100, 2) AS hoja_5,
                        (SELECT ROUND(emision_foliar, 2) FROM orodelti_foliar WHERE id_finca = tbl.id_finca $sWhere) AS ef
                    FROM (
                        SELECT finca, id_finca,
                            ROUND(AVG(3m_ht), 2) AS m3_ht,
                            ROUND(AVG(3m_hvle), 2) AS m3_hvle,
                            ROUND(AVG(0s_ht), 2) AS s0_ht,
                            ROUND(AVG(0s_hvle), 2) AS s0_hvle,
                            ROUND(AVG(11s_ht), 2) AS s11_ht
                        FROM `orodelti_plantas_muestras`
                        WHERE 1=1 $sWhere
                        GROUP BY finca
                    ) AS tbl";
            $data = $this->db->queryAll($sql);

        // Graficas
            $graficas = [];
            $groups = [
                [
                    "name" => '',
                    "type" => 'bar',
                    'format' => '',
                    'min' => 0
                ]
            ];

        // Grafica - ESTADO EVOLUTIVO
            $sql = "SELECT 
                        finca AS label_x,
                        'HOJA 2' AS 'name',
                        ROUND((
                            SELECT SUM(incidencia) 
                            FROM orodelti_plantas_muestras m
                            INNER JOIN `orodelti_plantas_muestras_hojas` ON id_muestra = m.id AND num_hoja = 2
                            WHERE finca = tbl.finca $sWhere
                        ) / 3m_ht * 100, 2) AS 'value',
                        '0' AS index_y
                    FROM (
                        SELECT finca, 3m_ht
                        FROM `orodelti_plantas_muestras`
                        WHERE 1=1 $sWhere
                        GROUP BY finca
                    ) AS tbl
                    UNION ALL
                    SELECT 
                        finca AS label_x,
                        'HOJA 3' AS 'name',
                        ROUND((
                            SELECT SUM(incidencia) 
                            FROM orodelti_plantas_muestras m
                            INNER JOIN `orodelti_plantas_muestras_hojas` ON id_muestra = m.id AND num_hoja = 3
                            WHERE finca = tbl.finca $sWhere
                        ) / 3m_ht * 100, 2) AS 'value',
                        '0' AS index_y
                    FROM (
                        SELECT finca, 3m_ht
                        FROM `orodelti_plantas_muestras`
                        WHERE 1=1 $sWhere
                        GROUP BY finca
                    ) AS tbl
                    UNION ALL
                    SELECT 
                        finca AS label_x,
                        'HOJA 4' AS 'name',
                        ROUND((
                            SELECT SUM(incidencia) 
                            FROM orodelti_plantas_muestras m
                            INNER JOIN `orodelti_plantas_muestras_hojas` ON id_muestra = m.id AND num_hoja = 4
                            WHERE finca = tbl.finca $sWhere
                        ) / 3m_ht * 100, 2) AS 'value',
                        '0' AS index_y
                    FROM (
                        SELECT finca, 3m_ht
                        FROM `orodelti_plantas_muestras`
                        WHERE 1=1 $sWhere
                        GROUP BY finca
                    ) AS tbl
                    UNION ALL
                    SELECT 
                        finca AS label_x,
                        'HOJA 5' AS 'name',
                        ROUND((
                            SELECT SUM(incidencia) 
                            FROM orodelti_plantas_muestras m
                            INNER JOIN `orodelti_plantas_muestras_hojas` ON id_muestra = m.id AND num_hoja = 5
                            WHERE finca = tbl.finca $sWhere
                        ) / 3m_ht * 100, 2) AS 'value',
                        '0' AS index_y
                    FROM (
                        SELECT finca, 3m_ht
                        FROM `orodelti_plantas_muestras`
                        WHERE 1=1 $sWhere
                        GROUP BY finca
                    ) AS tbl";
            $graficas['ESTADO EVOLUTIVO'] = $this->db->queryAll($sql);
            $graficas['ESTADO EVOLUTIVO'] = $this->grafica_z($graficas['ESTADO EVOLUTIVO'], $groups);

        // Grafica - PLANTAS 3 METROS
            $sql = "SELECT finca AS label_x, AVG(3m_ht) AS 'value', '0' AS index_y, 'HT' AS 'name'
                    FROM `orodelti_plantas_muestras`
                    WHERE 1=1 $sWhere
                    GROUP BY finca
                    UNION ALL
                    SELECT finca AS label_x, AVG(3m_hvle) AS 'value', '0' AS index_y, 'H+VLE' AS 'name'
                    FROM `orodelti_plantas_muestras`
                    WHERE 1=1 $sWhere
                    GROUP BY finca";
            $graficas['PLANTAS 3 METROS'] = $this->db->queryAll($sql);
            $graficas['PLANTAS 3 METROS'] = $this->grafica_z($graficas['PLANTAS 3 METROS'], $groups);

        // Grafica - PLANTAS 0 SEMANAS
            $sql = "SELECT finca AS label_x, AVG(0s_ht) AS 'value', '0' AS index_y, 'HT' AS 'name'
                    FROM `orodelti_plantas_muestras`
                    WHERE 1=1 $sWhere
                    GROUP BY finca
                    UNION ALL
                    SELECT finca AS label_x, AVG(0s_hvle) AS 'value', '0' AS index_y, 'H+VLE' AS 'name'
                    FROM `orodelti_plantas_muestras`
                    WHERE 1=1 $sWhere
                    GROUP BY finca";
            $graficas['PLANTAS 0 SEMANAS'] = $this->db->queryAll($sql);
            $graficas['PLANTAS 0 SEMANAS'] = $this->grafica_z($graficas['PLANTAS 0 SEMANAS'], $groups);

        // Grafica - PLANTAS 11 SEMANAS - HT
            $sql = "SELECT finca AS label_x, AVG(11s_ht) AS 'value', '0' AS index_y, 'HT' AS 'name'
                    FROM `orodelti_plantas_muestras`
                    WHERE 1=1 $sWhere
                    GROUP BY finca
                    UNION ALL
                    SELECT finca AS label_x, AVG(11s_hvle) AS 'value', '0' AS index_y, 'H+VLE' AS 'name'
                    FROM `orodelti_plantas_muestras`
                    WHERE 1=1 $sWhere
                    GROUP BY finca";
            $graficas['PLANTAS 11 SEMANAS'] = $this->db->queryAll($sql);
            $graficas['PLANTAS 11 SEMANAS'] = $this->grafica_z($graficas['PLANTAS 11 SEMANAS'], $groups);

        // Response
        $response->data = $data;
        $response->graficas = $graficas;
        return json_encode($response);
    }

    private function grafica_z($data = [], $group_y = [], $selected = null, $types = []){
		$options = [];
		$options["tooltip"] = [
			"trigger" => 'axis',
			"axisPointer" => [
				"type" => 'cross',
				"crossStyle" => [
					"color" => '#999'
				]
			]
		];
		$options["toolbox"] = [
			"feature" => [
				"dataView" => [
					"show" => true,
					"readOnly" => false
				],
				"magicType" => [
					"show" => true,
					"type" => ['line', 'bar']
				],
				"restore" => [
					"show" => true
				],
				"saveAsImage" => [
					"show" => true
				]
			]
		];
		$options["legend"]["data"] = [];
		$options["legend"]["bottom"] = "0%";
        $options["legend"]["left"] = "center";
        $options["legend"]["selected"] = $selected;
		$options["xAxis"] = [
			[
				"type" => 'category',
				"data" => [],
				"axisPointer" => [
					"type" => 'shadow'
				]
			]
		];
		/*
			[
				type => 'value',
				name => {String},
				min => 0,
				max => 200,
				interval => 5,
				axisLabel => [
					formatter => {value} KG
				]
			]
		*/
		$options["yAxis"] = [];
		/*
			[
				name => {String},
				type => 'line',
				data => [
					{double}, {double}, {double}
				]
			]
		*/
		$options["series"] = [];

		$maxs = [];
		$mins = [];
		$prepare_data = [];
		$_x = [];
		$_names = [];
		$_namess = [];
		foreach($data as $d){
			$d = (object) $d;
			if(!isset($maxs[$d->index_y])) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;
			if($d->value > $maxs[$d->index_y]) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;

			if(!isset($mins[$d->index_y])) if($d->value > 0)
				$mins[$d->index_y] = $d->value;
			if($d->value < $mins[$d->index_y]) if($d->value > 0)
				$mins[$d->index_y] = $d->value;

			if(!in_array($d->label_x, $_x)){
				$_x[] = $d->label_x;
			}
			if(!in_array($d->name, $_namess)){
				$_namess[] = $d->name;
				 
				$n = ["name" => $d->name, "group" => $d->index_y];
				if(isset($d->line)){
					$n["line"] = $d->line;
				}
				$_names[] = $n;
			}
			$prepare_data[$d->label_x][$d->name] = $d->value;
        }

		foreach($group_y as $key => $col){
			$col = (object) $col;
			$options["yAxis"][] = [
				'type' => 'value',
				'name' => $col->name,
				//'max' => ($key == 1) ? $maxs[$key] + ($maxs[$key] - $mins[$key]) * .05 : null,
                //'min' => ($key == 1) ? $mins[$key] - ($maxs[$key] - $mins[$key]) * .05 : null,
                'max' => null,
                'min' => isset($col->min) ? $col->min : 'dataMin',
				'axisLabel' => [
					'formatter' => "{value} $col->format"
				]
			];
		}

		foreach($_x as $row){
			$options["xAxis"][0]["data"][] = $row;
        }

		foreach($_names as $i => $name){
			$name = (object) $name;

			if(!in_array($name->name, $options["legend"]["data"]))
				$options["legend"]["data"][] = $name->name;

			$serie = [
				"name" => $name->name,
				"type" => isset($types[$i]) ? $types[$i] : 'bar',
				"connectNulls" => true,
				"data" => []
			];
			if($name->group > 0)
				$serie["yAxisIndex"] = $name->group;

			if(isset($name->line)){
				$serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
			}

			foreach($_x as $row){
				$val = 0;
				if(isset($prepare_data[$row][$name->name]))
					$val = $prepare_data[$row][$name->name];

				if($val > 0)
					$serie["data"][] = $val;
				else
					$serie["data"][] = null;
			}
			$options["series"][] = $serie;
		}

		return $options;
	}
}

?>