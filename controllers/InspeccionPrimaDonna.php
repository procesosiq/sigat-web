<?php

class InspeccionPrimaDonna {
	
	private $db;
	private $session;
	
	public function __construct() {
        $this->db = new M_Conexion();
        $this->session = Session::getInstance();
    }

    function D($data){
        echo "<pre>";
        print_r($data);
        echo "</pre><br>";
    }

    function F($data){
        D($data);
        die();
    }

    private function params(){
        $data = (object)json_decode(file_get_contents("php://input"));
        $params = new stdClass;
        $params->semana = $data->semana;
        $params->luz = $data->luz;
        $params->fecha_inicial = $data->fecha_inicial;
        $params->fecha_final = $data->fecha_final;
        return $params;
    }

    public function index(){
        $response = new stdClass;
        $filters = $this->params();

        $response->semanas = $this->getSemanas();
        if($filters->semana == null){
            $keys = array_keys($response->semanas);
            $response->last_semana = $filters->semana = $keys[count($keys)-1];
        }
        $response->table_lote = $this->getLotes($filters);

        foreach($response->table_lote as $lote){
            $m3 = $this->db->queryRow("SELECT 
                    ROUND(AVG(IF(total_hoja_3 = 100 OR total_hoja_3 = 1, 1, 0)) * 100, 2) AS h3, 
                    ROUND(AVG(IF(total_hoja_4 = 100 OR total_hoja_4 = 1, 1, 0)) * 100, 2) AS h4, 
                    ROUND(AVG(IF(total_hoja_5 = 100 OR total_hoja_5 = 1, 1, 0)) * 100, 2) AS h5,
                    ROUND(AVG(hoja_mas_vieja_de_estrias)) AS hmvle,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor)) AS hmvlqm
                FROM muestras_hacienda_detalle_3M
                WHERE id_lote = '{$lote->id}' AND WEEK(fecha) = '{$filters->semana}'");
            $lote->m3_h3 = $m3->h3;
            $lote->m3_h4 = $m3->h4;
            $lote->m3_h5 = $m3->h5;
            $lote->m3_hmvle = $m3->hmvle;
            $lote->m3_hmvlqm = $m3->hmvlqm;

            $s0 = $this->db->queryRow("SELECT 
                    ROUND(AVG(hojas_totales)) AS ht,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor)) AS hmvlqm,
                    ROUND(AVG(hojas_mas_vieja_libre)) AS hmvle
                FROM muestras_hacienda_detalle
                WHERE id_lote = '{$lote->id}' AND tipo_semana = 0 AND WEEK(fecha) = '{$filters->semana}'");
            $lote->s0_ht = $s0->ht;
            $lote->s0_hmvlqm = $s0->hmvlqm;
            $lote->s0_hmvle = $s0->hmvle;

            $s11 = $this->db->queryRow("SELECT 
                    ROUND(AVG(hojas_totales)) AS ht,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor)) AS hmvlqm
                FROM muestras_hacienda_detalle
                WHERE id_lote = '{$lote->id}' AND tipo_semana = 11 AND WEEK(fecha) = '{$filters->semana}'");
            $lote->s11_ht = $s11->ht;
            $lote->s11_hmvlqm = $s11->hmvlqm;
        }

        return json_encode($response);
    }

    public function getAplicaciones(){
        $response = new stdClass;
        $sql = "SELECT fecha, IF(ciclo = '(NULL)', '', ciclo) AS ciclo, IF(producto_1 IS NULL OR producto_1 = '(NULL)', '', producto_1) AS producto_1, IF(producto_2 = '(NULL)', '', producto_2) AS producto_2
                FROM ciclos_aplicacion
                WHERE id_finca = '{$this->session->finca}' AND ((producto_1 != '' AND producto_1 != '(NULL)') OR (producto_2 != '' AND producto_2 != '(NULL)'))
                ORDER BY fecha";
        $response->data = $this->db->queryAll($sql);
        return json_encode($response);
    }

    private function getSemanas($filters = []){
        $response = new stdClass;
        $sql = "SELECT id, label FROM(
            SELECT WEEK(fecha) AS id, WEEK(fecha) AS label 
            FROM muestras_haciendas
            WHERE YEAR(fecha) = {$this->session->year}
                AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
            GROUP BY WEEK(fecha)
            UNION ALL
            SELECT WEEK(fecha) AS id, WEEK(fecha) AS label 
            FROM muestras_haciendas_3M
            WHERE YEAR(fecha) = {$this->session->year}
                AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
            GROUP BY WEEK(fecha)
        ) AS tbl
        GROUP BY id
        ORDER BY id";
        $response = $this->db->queryAllSpecial($sql);
        return $response;
    }

    private function getLotes($filters = []){
        if($filters->semana >= 0){
            $sWhere .= "AND semana = '{$filters->semana}'";
            $sWhere3 .= "AND semana = '{$filters->semana}'";
        }
        $sql = "SELECT id, UPPER(lote) AS lote
                FROM(
                    SELECT cat_lotes.id, cat_lotes.`nombre` AS lote
                    FROM muestras_haciendas
                    INNER JOIN muestras_hacienda_detalle detalle ON id_Mhacienda = muestras_haciendas.`id`
                    INNER JOIN cat_lotes ON cat_lotes.id = id_lote
                    WHERE anio = 2017 
                        AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                        AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                        AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}') $sWhere
                    GROUP BY cat_lotes.id 
                    UNION ALL
                    SELECT cat_lotes.id, cat_lotes.`nombre` AS lote 
                    FROM muestras_haciendas_3M
                    INNER JOIN muestras_hacienda_detalle_3M detalle ON id_Mhacienda = muestras_haciendas_3M.`id`
                    INNER JOIN cat_lotes ON cat_lotes.id = id_lote
                    WHERE anio = 2017 
                        AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                        AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                        AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}') $sWhere3
                    GROUP BY cat_lotes.id
                ) AS lotes
                GROUP BY id
                ORDER BY lote";
        return $this->db->queryAll($sql);
    }

    //BEGIN VARIABLES AGRUPADOS
        public function getHojas345(){
            $response = new stdClass;
            $semanas = $this->getSemanas3M();
            $data_chart = [];

            $min = $max = 200;

            foreach($semanas as $sem){
                $data_chart[] = ["value" => 200, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                $sql = "SELECT label,
                            ROUND(AVG(h3), 2) AS 'h3',
                            ROUND(AVG(h4), 2) AS 'h4',
                            ROUND(AVG(h5), 2) AS 'h5'
                        FROM (
                            SELECT label,
                                foco,
                                ROUND((letras_h3 * numeros_h3 * 120), 2) AS h3,
                                ROUND((letras_h4 * numeros_h4 * 100), 2) AS h4,
                                ROUND((letras_h5 * numeros_h5 * 80), 2) AS h5
                            FROM (
                                SELECT 
                                    semana AS label,
                                    foco,
                                    1 AS selected,
                                    SUM(IF(total_hoja_3 = '1A', 0, IF(total_hoja_3 LIKE '%A' = 1, 1, IF(total_hoja_3 LIKE '%B' = 1, 2, 3))))/MAX(muestra) AS letras_h3,
                                    SUM(IF(total_hoja_3 = '1A', 0, IF(total_hoja_3 LIKE '1%' = 1, 1, IF(total_hoja_3 LIKE '2%' = 1, 2, 3))))/MAX(muestra) AS numeros_h3,
                                    SUM(IF(total_hoja_4 = '1A', 0, IF(total_hoja_4 LIKE '%A' = 1, 1, IF(total_hoja_4 LIKE '%B' = 1, 2, 3))))/MAX(muestra) AS letras_h4,
                                    SUM(IF(total_hoja_4 = '1A', 0, IF(total_hoja_4 LIKE '1%' = 1, 1, IF(total_hoja_4 LIKE '2%' = 1, 2, 3))))/MAX(muestra) AS numeros_h4,
                                    SUM(IF(total_hoja_5 = '1A', 0, IF(total_hoja_5 LIKE '%A' = 1, 1, IF(total_hoja_5 LIKE '%B' = 1, 2, 3))))/MAX(muestra) AS letras_h5,
                                    SUM(IF(total_hoja_5 = '1A', 0, IF(total_hoja_5 LIKE '1%' = 1, 1, IF(total_hoja_5 LIKE '2%' = 1, 2, 3))))/MAX(muestra) AS numeros_h5
                                FROM muestras_haciendas_3M 
                                INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                                WHERE anio = {$this->session->year}
                                    AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                                    AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                    AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                    AND semana = {$sem->semana}
                                    AND foco IN('NORMAL' , 'FOCO')
                                GROUP BY foco
                            ) as tbl
                        ) AS tbl";
                $data = $this->db->queryRow($sql);
                $data_chart[] = ["value" => $data->h3, "legend" => 'H3', "selected" => 1, "label" => $sem->semana, "position" => 1];
                $data_chart[] = ["value" => $data->h4, "legend" => 'H4', "selected" => 1, "label" => $sem->semana, "position" => 2];
                $data_chart[] = ["value" => $data->h5, "legend" => 'H5', "selected" => 1, "label" => $sem->semana, "position" => 3];

                if($data->h3 > 0){
                    if($min > $data->h3) $min = $data->h3;
                    if($max < $data->h3) $max = $data->h3;
                }
                if($data->h4 > 0){
                    if($min > $data->h4) $min = $data->h4;
                    if($max < $data->h4) $max = $data->h4;
                }
                if($data->h5 > 0){
                    if($min > $data->h5) $min = $data->h5;
                    if($max < $data->h5) $max = $data->h5;
                }
            }
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line", ["max" => $max, "min" => $min]);
            return json_encode($response);
        }

        public function getHojasTotales(){
            $response = new stdClass;
            $semanas = $this->getSemanas3M();
            $data_chart = [];

            $max = 13;
            $min = 7;

            foreach($semanas as $sem){
                $data_chart[] = ["value" => 7, "legend" => "Umbral P11", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];
                $data_chart[] = ["value" => 13, "legend" => "Umbral P0", "selected" => 1, "label" => $sem->semana, "position" => 1, "z" => 4];

                $sql = "SELECT label,
                            ROUND(AVG(sem11), 2) AS sem11,
                            ROUND(AVG(sem0), 2) AS sem0
                        FROM (
                            SELECT 
                                semana AS label,
                                SUM(IF(d.tipo_semana = 11, hojas_totales, 0))/SUM(IF(d.tipo_semana = 11, 1, 0)) AS sem11,
                                SUM(IF(d.tipo_semana = 0, hojas_totales, 0))/SUM(IF(d.tipo_semana = 0, 1, 0)) AS sem0
                            FROM muestras_haciendas
                            INNER JOIN muestras_hacienda_detalle d ON muestras_haciendas.id = id_Mhacienda
                            WHERE anio = {$this->session->year}
                                AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco IN('NORMAL' , 'FOCO')
                            GROUP BY foco
                        ) AS tbl";
                        
                $data = $this->db->queryRow($sql);
                $data_chart[] = ["value" => $data->sem11, "legend" => 'P11', "selected" => 1, "label" => $sem->semana, "position" => 2];
                $data_chart[] = ["value" => $data->sem0, "legend" => 'P0', "selected" => 1, "label" => $sem->semana, "position" => 3];
                
                if($data->sem11 > 0){
                    if($max == null || $max < $data->sem11) $max = $data->sem11;
                    if($min == null || $min > $data->sem11) $min = $data->sem11;
                }
                if($data->sem0 > 0){
                    if($max == null || $max < $data->sem0) $max = $data->sem0;
                    if($min == null || $min > $data->sem0) $min = $data->sem0;
                }
            }
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line");
            $response->data_chart["color"] = ["#D91E18", "#D91E18", "#E65100", "#3598DC"];
            $response->data_chart["series"][1]["yAxisIndex"] = 1;
            $response->data_chart["series"][3]["yAxisIndex"] = 1;
            $response->data_chart["yAxis"] = [
                [
                    "min" => $min,
                    "max" => $max,
                    "type" => 'value',
                    "name" => ''
                ],
                [
                    "min" => $min,
                    "max" => $max,
                    "type" => 'value',
                    "name" => ''
                ]
            ];
            return json_encode($response);
        }

        public function getHojasLibresEstrias(){
            $response = new stdClass;
            $semanas = $this->getSemanas3M();
            $data_chart = [];

            $max = $min = 7;

            foreach($semanas as $sem){
                $data_chart[] = ["value" => 7, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                $sql = "SELECT label,
                            ROUND(AVG(valor), 2) AS valor
                        FROM (
                            SELECT 
                                semana AS label,
                                ROUND(AVG(hojas_mas_vieja_libre), 2) AS valor
                            FROM muestras_haciendas
                            INNER JOIN muestras_hacienda_detalle d ON muestras_haciendas.id = id_Mhacienda
                            WHERE anio = {$this->session->year}
                                AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco IN('NORMAL' , 'FOCO')
                                AND d.tipo_semana = 0
                            GROUP BY foco
                        ) AS tbl
                        UNION ALL
                        SELECT label,
                            ROUND(AVG(valor), 2) AS valor
                        FROM (
                            SELECT 
                                semana AS label,
                                ROUND(AVG(hoja_mas_vieja_de_estrias), 2) AS valor
                            FROM muestras_haciendas_3M
                            INNER JOIN muestras_hacienda_detalle_3M d ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE anio = {$this->session->year}
                                AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco IN('NORMAL' , 'FOCO')
                            GROUP BY foco
                        ) AS tbl";
                        
                $data = $this->db->queryAll($sql);
                $data_chart[] = ["value" => $data[1]->valor, "legend" => '3M', "selected" => 1, "label" => $sem->semana, "position" => 1];
                $data_chart[] = ["value" => $data[0]->valor, "legend" => 'P0', "selected" => 1, "label" => $sem->semana, "position" => 2];

                if($data[1]->valor > 0){
                    if($min > $data[1]->valor) $min = $data[1]->valor;
                    if($max < $data[1]->valor) $max = $data[1]->valor;
                }
                if($data[0]->valor > 0){
                    if($min > $data[0]->valor) $min = $data[0]->valor;
                    if($max < $data[0]->valor) $max = $data[0]->valor;
                }
            }
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line", ["max" => $max, "min" => $min]);
            $response->data_chart["color"] = ["#D91E18", "#26C281", "#3598DC"];
            return json_encode($response);
        }

        public function getQuemaMenor(){
            $response = new stdClass;
            $semanas = $this->getSemanas3M();
            $data_chart = [];

            $max = 12;
            $min = 6;

            foreach($semanas as $sem){
                $data_chart[] = ["value" => 6, "legend" => "Umbral P11", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];
                $data_chart[] = ["value" => 12, "legend" => "Umbral P0", "selected" => 1, "label" => $sem->semana, "position" => 1, "z" => 3];

                $sql = "SELECT label,
                            ROUND(AVG(sem11), 2) AS sem11,
                            ROUND(AVG(sem0), 2) AS sem0
                        FROM (
                            SELECT 
                                semana AS label,
                                SUM(IF(d.tipo_semana = 11, hoja_mas_vieja_libre_quema_menor, 0))/SUM(IF(d.tipo_semana = 11, 1, 0)) AS sem11,
                                SUM(IF(d.tipo_semana = 0, hoja_mas_vieja_libre_quema_menor, 0))/SUM(IF(d.tipo_semana = 0, 1, 0)) AS sem0
                            FROM muestras_haciendas
                            INNER JOIN muestras_hacienda_detalle d ON muestras_haciendas.id = id_Mhacienda
                            WHERE anio = {$this->session->year}
                                AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco IN('NORMAL' , 'FOCO')
                            GROUP BY foco
                        ) AS tbl";
                        
                $data = $this->db->queryRow($sql);
                $data_chart[] = ["value" => $data->sem11, "legend" => 'P11', "selected" => 1, "label" => $sem->semana, "position" => 2];
                $data_chart[] = ["value" => $data->sem0, "legend" => 'P0', "selected" => 1, "label" => $sem->semana, "position" => 3];

                if($data->sem11 > 0){
                    if($max == null || $max < $data->sem11) $max = $data->sem11;
                    if($min == null || $min > $data->sem11) $min = $data->sem11;
                }
                if($data->sem0 > 0){
                    if($max == null || $max < $data->sem0) $max = $data->sem0;
                    if($min == null || $min > $data->sem0) $min = $data->sem0;
                }
            }
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line");
            $response->data_chart["color"] = ["#D91E18", "#D91E18", "#E65100", "#3598DC"];
            $response->data_chart["series"][1]["yAxisIndex"] = 1;
            $response->data_chart["series"][3]["yAxisIndex"] = 1;
            $response->data_chart["yAxis"] = [
                [
                    "min" => $min,
                    "max" => $max,
                    "type" => 'value',
                    "name" => ''
                ],
                [
                    "min" => $min,
                    "max" => $max,
                    "type" => 'value',
                    "name" => ''
                ]
            ];
            return json_encode($response);
        }

        public function getHojas3M(){
            $response = new stdClass;
            $semanas = $this->getSemanas3M();
            $data_chart = [];

            $max = $min = 5.5;
            foreach($semanas as $sem){
                $data_chart[] = ["value" => 5.5, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                $sql = "SELECT label,
                            ROUND(AVG(valor), 2) AS valor
                        FROM (
                            SELECT 
                                semana AS label,
                                AVG(hojas_totales) AS valor
                            FROM muestras_haciendas_3M
                            INNER JOIN muestras_hacienda_detalle_3M d ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE anio = {$this->session->year}
                                AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco IN('NORMAL' , 'FOCO')
                            GROUP BY foco
                        ) AS tbl";
                $data = $this->db->queryRow($sql);
                $data_chart[] = ["value" => $data->valor, "legend" => '3M', "selected" => 1, "label" => $sem->semana, "position" => 1];

                if($data->valor > 0){
                    if($min > $data->valor) $min = $data->valor;
                    if($max < $data->valor) $max = $data->valor;
                }
            }
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line", ["max" => $max, "min" => $min]);
            $response->data_chart["color"] = ["#D91E18", "#26C281"];
            return json_encode($response);
        }
    //END VARIABLES AGRUPADOS

    //BEGIN 3 M
        private function getSemanas3M(){
            $sql = "SELECT getWeek(fecha) AS semana
                        FROM muestras_haciendas_3M 
                        WHERE getWeek(fecha) >= 0 
                            AND YEAR(fecha) = {$this->session->year}
                            AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                            AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                            AND (IF({$this->session->year} = 2017, IF(getWeek(fecha) > 16, TRUE, FALSE), TRUE))
                        GROUP BY getWeek(fecha)
                        ORDER BY WEEK(fecha)";
            return $this->db->queryAll($sql);
        }
        private function getFocos3M(){
            $sql = "SELECT UPPER(foco) AS id, UPPER(foco) AS label
                        FROM muestras_hacienda_detalle_3M 
                        WHERE WEEK(fecha) >= 0 AND YEAR(fecha) = {$this->session->year}
                            AND id_hacienda = '{$this->session->finca}' 
                            AND id_cliente = '{$this->session->client}' 
                            AND id_usuario IN ('{$this->session->logges->users}')
                        GROUP BY foco
                        ORDER BY foco";
            return $this->db->queryAllSpecial($sql);
        }

        public function get3M(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $focos = $this->getFocos3M(); 

            $data_chart = [];
            $max = $min = 7;
            foreach($semanas as $sem){
                $data_chart[] = ["value" => 7, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($focos as $foco){
                    $sql = "SELECT 
                                ROUND(AVG(hoja_mas_vieja_de_estrias), 2) AS value
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE anio = {$this->session->year}
                                AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco = '{$foco}'
                                AND hoja_mas_vieja_de_estrias > 0";
                    $data = $this->db->queryRow($sql);


                    $position_foco = array_search($foco, array_keys($focos)) + 1;
                    if(isset($data->value)){
                        if($max < $data->value) $max = $data->value;
                        if($min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }

                }
            }

            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line", ["max" => $max, "min" => $min]);

            return json_encode($response);
        }

        public function getH4(){
            $response = new stdClass;
            $semanas = $this->getSemanas3M();
            $data_chart = [];

            $min = $max = 200;

            foreach($semanas as $sem){
                $data_chart[] = ["value" => 200, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                $sql = "SELECT label,
                            ROUND(AVG(h4), 2) AS 'h4'
                        FROM (
                            SELECT label,
                                foco,
                                ROUND((letras_h4 * numeros_h4 * 100), 2) AS h4
                            FROM (
                                SELECT 
                                    semana AS label,
                                    foco,
                                    1 AS selected,
                                    SUM(IF(total_hoja_4 = '1A', 0, IF(total_hoja_4 LIKE '%A' = 1, 1, IF(total_hoja_4 LIKE '%B' = 1, 2, 3))))/MAX(muestra) AS letras_h4,
                                    SUM(IF(total_hoja_4 = '1A', 0, IF(total_hoja_4 LIKE '1%' = 1, 1, IF(total_hoja_4 LIKE '2%' = 1, 2, 3))))/MAX(muestra) AS numeros_h4
                                FROM muestras_haciendas_3M 
                                INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                                WHERE anio = {$this->session->year}
                                    AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                                    AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                    AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                    AND semana = {$sem->semana}
                                    AND foco IN('NORMAL' , 'FOCO')
                                    AND total_hoja_4 != ''
                                GROUP BY foco
                            ) as tbl
                        ) AS tbl";
                $data = $this->db->queryRow($sql);
                $data_chart[] = ["value" => $data->h4, "legend" => 'H4', "selected" => 1, "label" => $sem->semana, "position" => 1];

                if($data->h4 > 0){
                    if($min > $data->h4) $min = $data->h4;
                    if($max < $data->h4) $max = $data->h4;
                }
            }
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line", ["max" => $max, "min" => $min]);
            return json_encode($response);
        }

        public function getH3(){
            $response = new stdClass;
            $semanas = $this->getSemanas3M();
            $data_chart = [];

            $min = $max = 200;

            foreach($semanas as $sem){
                $data_chart[] = ["value" => 200, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                $sql = "SELECT label,
                            ROUND(AVG(h3), 2) AS 'h3'
                        FROM (
                            SELECT label,
                                foco,
                                ROUND((letras_h3 * numeros_h3 * 120), 2) AS h3
                            FROM (
                                SELECT 
                                    semana AS label,
                                    foco,
                                    1 AS selected,
                                    SUM(IF(total_hoja_3 = '1A', 0, IF(total_hoja_3 LIKE '%A' = 1, 1, IF(total_hoja_3 LIKE '%B' = 1, 2, 3))))/MAX(muestra) AS letras_h3,
                                    SUM(IF(total_hoja_3 = '1A', 0, IF(total_hoja_3 LIKE '1%' = 1, 1, IF(total_hoja_3 LIKE '2%' = 1, 2, 3))))/MAX(muestra) AS numeros_h3
                                FROM muestras_haciendas_3M 
                                INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                                WHERE anio = {$this->session->year}
                                    AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                                    AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                    AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                    AND semana = {$sem->semana}
                                    AND foco IN('NORMAL' , 'FOCO')
                                    AND total_hoja_3 != ''
                                GROUP BY foco
                            ) as tbl
                        ) AS tbl";
                $data = $this->db->queryRow($sql);
                $data_chart[] = ["value" => $data->h3, "legend" => 'H3', "selected" => 1, "label" => $sem->semana, "position" => 1];

                if($data->h3 > 0){
                    if($min > $data->h3) $min = $data->h3;
                    if($max < $data->h3) $max = $data->h3;
                }
            }
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line", ["max" => $max, "min" => $min]);
            return json_encode($response);
        }

        public function getH5(){
            $response = new stdClass;
            $semanas = $this->getSemanas3M();
            $data_chart = [];

            $min = $max = 200;

            foreach($semanas as $sem){
                $data_chart[] = ["value" => 200, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                $sql = "SELECT label,
                            ROUND(AVG(h5), 2) AS 'h5'
                        FROM (
                            SELECT label,
                                foco,
                                ROUND((letras_h5 * numeros_h5 * 80), 2) AS h5
                            FROM (
                                SELECT 
                                    semana AS label,
                                    foco,
                                    1 AS selected,
                                    SUM(IF(total_hoja_5 = '1A', 0, IF(total_hoja_5 LIKE '%A' = 1, 1, IF(total_hoja_5 LIKE '%B' = 1, 2, 3))))/MAX(muestra) AS letras_h5,
                                    SUM(IF(total_hoja_5 = '1A', 0, IF(total_hoja_5 LIKE '1%' = 1, 1, IF(total_hoja_5 LIKE '2%' = 1, 2, 3))))/MAX(muestra) AS numeros_h5
                                FROM muestras_haciendas_3M 
                                INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                                WHERE anio = {$this->session->year}
                                    AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                                    AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                    AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                    AND semana = {$sem->semana}
                                    AND foco IN('NORMAL' , 'FOCO')
                                    AND total_hoja_5 != ''
                                GROUP BY foco
                            ) as tbl
                        ) AS tbl";
                $data = $this->db->queryRow($sql);
                $data_chart[] = ["value" => $data->h5, "legend" => 'H5', "selected" => 1, "label" => $sem->semana, "position" => 1];

                if($data->h5 > 0){
                    if($min > $data->h5) $min = $data->h5;
                    if($max < $data->h5) $max = $data->h5;
                }
            }
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line", ["max" => $max, "min" => $min]);
            return json_encode($response);
        }

        public function getHMVLQM(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $focos = $this->getFocos3M();

            $data_chart_hmvle = [];
            $data_chart_hmvlqm = [];
            $min = $max = 11;
            foreach($semanas as $sem){
                $data_chart_hmvlqm[] = ["value" => 11, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($focos as $foco){
                    $sql = "SELECT 
                                AVG(hoja_mas_vieja_de_estrias) AS hmvle,
                                AVG(hoja_mas_vieja_libre_quema_menor) AS hmvlqm,
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0 AND anio = {$this->session->year}
                                AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco = '{$foco}'";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco, array_keys($focos)) + 1;
                    if(isset($data->hmvlqm) && $data->hmvlqm > 0){
                        if($max < $data->hmvlqm) $max = $data->hmvlqm;
                        if($min > $data->hmvlqm) $min = $data->hmvlqm;
                        $data_chart_hmvlqm[] = ["value" => $data->hmvlqm, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }
            $max = round($max, 2);
            $min = round($min, 2);

            $response->data_chart = $this->chartInit($data_chart_hmvlqm, "vertical" , "" , "line", ["max" => $max, "min" => $min]);

            return json_encode($response);
        }
    //END 3 M

    //BEGIN 0S
        private function getSemanas0S(){
            $sql = "SELECT WEEK(fecha) AS semana
                        FROM muestras_haciendas 
                        WHERE WEEK(fecha) >= 0 AND YEAR(fecha) = {$this->session->year}
                            AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND tipo_semana = 0
                        GROUP BY WEEK(fecha)
                        ORDER BY WEEK(fecha)";
            return $this->db->queryAll($sql);
        }
        private function getFocos0S(){
            $sql = "SELECT UPPER(foco) AS id, UPPER(foco) AS label
                        FROM muestras_hacienda_detalle 
                        WHERE WEEK(fecha) >= 0 AND YEAR(fecha) = {$this->session->year}
                            AND id_hacienda = '{$this->session->finca}' 
                            AND id_cliente = '{$this->session->client}' 
                            AND id_usuario IN ('{$this->session->logges->users}')
                            AND tipo_semana = 0
                        GROUP BY foco
                        ORDER BY foco";
            return $this->db->queryAllSpecial($sql);
        }

        public function getHT0S(){
            $response = new stdClass;
            $semanas = $this->getSemanas3M();
            $data_chart = [];

            $max = 13;
            $min = 13;

            foreach($semanas as $sem){
                $data_chart[] = ["value" => 13, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 4];

                $sql = "SELECT label,
                            ROUND(AVG(sem0), 2) AS sem0
                        FROM (
                            SELECT 
                                semana AS label,
                                SUM(IF(d.tipo_semana = 0, hojas_totales, 0))/SUM(IF(d.tipo_semana = 0, 1, 0)) AS sem0
                            FROM muestras_haciendas
                            INNER JOIN muestras_hacienda_detalle d ON muestras_haciendas.id = id_Mhacienda
                            WHERE anio = {$this->session->year}
                                AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco IN('NORMAL' , 'FOCO')
                                AND d.tipo_semana = 0
                            GROUP BY foco
                        ) AS tbl";
                        
                $data = $this->db->queryRow($sql);
                $data_chart[] = ["value" => $data->sem0, "legend" => 'HT', "selected" => 1, "label" => $sem->semana, "position" => 1];
                
                if($data->sem0 > 0){
                    if($max == null || $max < $data->sem0) $max = $data->sem0;
                    if($min == null || $min > $data->sem0) $min = $data->sem0;
                }
            }
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line", ["max" => $max, "min" => $min]);
            $response->data_chart["color"] = ["#D91E18", "#D91E18", "#E65100", "#3598DC"];
            return json_encode($response);
        }

        public function getHMVLE0S(){
            $response = new stdClass;
            $semanas = $this->getSemanas3M();
            $data_chart = [];

            $max = $min = 7;

            foreach($semanas as $sem){
                $data_chart[] = ["value" => 7, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                $sql = "SELECT label,
                            ROUND(AVG(valor), 2) AS valor
                        FROM (
                            SELECT 
                                semana AS label,
                                ROUND(AVG(hojas_mas_vieja_libre), 2) AS valor
                            FROM muestras_haciendas
                            INNER JOIN muestras_hacienda_detalle d ON muestras_haciendas.id = id_Mhacienda
                            WHERE anio = {$this->session->year}
                                AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco IN('NORMAL' , 'FOCO')
                                AND d.tipo_semana = 0
                            GROUP BY foco
                        ) AS tbl";
                        
                $data = $this->db->queryAll($sql);
                $data_chart[] = ["value" => $data[0]->valor, "legend" => 'HVLE', "selected" => 1, "label" => $sem->semana, "position" => 1];

                if($data[0]->valor > 0){
                    if($min > $data[0]->valor) $min = $data[0]->valor;
                    if($max < $data[0]->valor) $max = $data[0]->valor;
                }
            }
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line", ["max" => $max, "min" => $min]);
            $response->data_chart["color"] = ["#D91E18", "#26C281", "#3598DC"];
            return json_encode($response);
        }

        public function getHMVLQM0S(){
            $response = new stdClass;
            $semanas = $this->getSemanas3M();
            $data_chart = [];

            $max = 12;
            $min = 12;

            foreach($semanas as $sem){
                $data_chart[] = ["value" => 12, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 1];

                $sql = "SELECT label,
                            ROUND(AVG(sem0), 2) AS sem0
                        FROM (
                            SELECT 
                                semana AS label,
                                SUM(IF(d.tipo_semana = 0, hoja_mas_vieja_libre_quema_menor, 0))/SUM(IF(d.tipo_semana = 0, 1, 0)) AS sem0
                            FROM muestras_haciendas
                            INNER JOIN muestras_hacienda_detalle d ON muestras_haciendas.id = id_Mhacienda
                            WHERE anio = {$this->session->year}
                                AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco IN('NORMAL' , 'FOCO')
                                AND d.tipo_semana = 0
                            GROUP BY foco
                        ) AS tbl";
                        
                $data = $this->db->queryRow($sql);
                $data_chart[] = ["value" => $data->sem0, "legend" => 'Q<5', "selected" => 1, "label" => $sem->semana, "position" => 1];
                if($data->sem0 > 0){
                    if($max == null || $max < $data->sem0) $max = $data->sem0;
                    if($min == null || $min > $data->sem0) $min = $data->sem0;
                }
            }
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line", ["max" => $max, "min" => $min]);
            return json_encode($response);
        }
    //END 0S

    //BEGIN 11S
        private function getSemanas11S(){ 
            $sql = "SELECT WEEK(fecha) AS semana
                        FROM muestras_haciendas 
                        WHERE WEEK(fecha) >= 0 AND YEAR(fecha) = {$this->session->year}
                            AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND tipo_semana = 11
                        GROUP BY WEEK(fecha)
                        ORDER BY WEEK(fecha)";
            return $this->db->queryAll($sql);  
        }  
        private function getFocos11S(){
            $sql = "SELECT UPPER(foco) AS id, UPPER(foco) AS label
                        FROM muestras_hacienda_detalle 
                        WHERE WEEK(fecha) >= 0 AND YEAR(fecha) = {$this->session->year}
                            AND id_hacienda = '{$this->session->finca}' 
                            AND id_cliente = '{$this->session->client}' 
                            AND id_usuario IN ('{$this->session->logges->users}')
                            AND tipo_semana = 11
                        GROUP BY foco
                        ORDER BY foco";
            return $this->db->queryAllSpecial($sql);          
        }

        public function getHT11S(){
            $response = new stdClass;
            $semanas = $this->getSemanas3M();
            $data_chart = [];

            $max = 7;
            $min = 7;

            foreach($semanas as $sem){
                $data_chart[] = ["value" => 7, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                $sql = "SELECT label,
                            ROUND(AVG(sem11), 2) AS sem11
                        FROM (
                            SELECT 
                                semana AS label,
                                SUM(IF(d.tipo_semana = 11, hojas_totales, 0))/SUM(IF(d.tipo_semana = 11, 1, 0)) AS sem11
                            FROM muestras_haciendas
                            INNER JOIN muestras_hacienda_detalle d ON muestras_haciendas.id = id_Mhacienda
                            WHERE anio = {$this->session->year}
                                AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco IN('NORMAL' , 'FOCO')
                                AND d.tipo_semana = 11
                            GROUP BY foco
                        ) AS tbl";
                        
                $data = $this->db->queryRow($sql);
                $data_chart[] = ["value" => $data->sem11, "legend" => 'HT', "selected" => 1, "label" => $sem->semana, "position" => 1];
                
                if($data->sem11 > 0){
                    if($max == null || $max < $data->sem11) $max = $data->sem11;
                    if($min == null || $min > $data->sem11) $min = $data->sem11;
                }
            }
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line", ["max" => $max, "min" => $min]);
            return json_encode($response);
        }

        public function getHMVLQM11S(){
            $response = new stdClass;
            $semanas = $this->getSemanas3M();
            $data_chart = [];

            $max = 6;
            $min = 6;

            foreach($semanas as $sem){
                $data_chart[] = ["value" => 6, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                $sql = "SELECT label,
                            ROUND(AVG(sem11), 2) AS sem11
                        FROM (
                            SELECT 
                                semana AS label,
                                SUM(IF(d.tipo_semana = 11, hoja_mas_vieja_libre_quema_menor, 0))/SUM(IF(d.tipo_semana = 11, 1, 0)) AS sem11
                            FROM muestras_haciendas
                            INNER JOIN muestras_hacienda_detalle d ON muestras_haciendas.id = id_Mhacienda
                            WHERE anio = {$this->session->year}
                                AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco IN('NORMAL' , 'FOCO')
                                AND d.tipo_semana = 11
                            GROUP BY foco
                        ) AS tbl";
                        
                $data = $this->db->queryRow($sql);
                $data_chart[] = ["value" => $data->sem11, "legend" => 'P11', "selected" => 1, "label" => $sem->semana, "position" => 1];

                if($data->sem11 > 0){
                    if($max == null || $max < $data->sem11) $max = $data->sem11;
                    if($min == null || $min > $data->sem11) $min = $data->sem11;
                }
            }
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line", ["max" => $max, "min" => $min]);
            return json_encode($response);
        }
    //END 11S

    //BEGIN CLIMA
        private function getSemanasClima(){ 
            $sql = "SELECT semana
                        FROM datos_clima 
                        WHERE semana >= 0 
                            AND YEAR(fecha) = {$this->session->year}
                            AND id_cliente = '{$this->session->client}' 
                            AND id_usuario IN ('{$this->session->logges->users}')
                        GROUP BY semana
                        ORDER BY semana";
            return $this->db->queryAll($sql);
        }

        private function getEstacionesClima(){
            $sql = "SELECT estacion_id AS id, (SELECT GROUP_CONCAT(h.alias SEPARATOR ' | ') FROM estaciones_haciendas INNER JOIN cat_haciendas h ON id_hacienda = h.id WHERE estacion_id = datos_clima.estacion_id) AS label
                    FROM datos_clima 
                    WHERE YEAR(fecha) = {$this->session->year}
                        AND id_cliente = '{$this->session->client}' 
                        AND id_usuario IN ('{$this->session->logges->users}')
                    GROUP BY estacion_id
                    ORDER BY estacion_id";
            return $this->db->queryAllSpecial($sql);
        }

        private function getHorasClima($fecha){
            $sql = "SELECT HOUR(hora) AS id, DATE_FORMAT(CONCAT(CURRENT_DATE,' ', hora), '%h %p') AS label
                    FROM datos_clima 
                    WHERE YEAR(fecha) = {$this->session->year}
                        AND id_cliente = '{$this->session->client}' 
                        AND id_usuario IN ('{$this->session->logges->users}')
                        AND fecha = '{$fecha}'
                    GROUP BY HOUR(hora)
                    ORDER BY HOUR(hora)";
            return $this->db->queryAllSpecial($sql);
        }

        public function getTempMin(){
            $response = new stdClass();
            
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];

            foreach($semanas as $sem){
                $data_chart[] = ["value" => 21, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT MIN(temp_minima) AS value
                            FROM datos_clima_resumen
                            WHERE semana >= 0 AND anio = {$this->session->year}
                                AND semana = {$sem->semana}
                                AND estacion_id = '{$estacion}'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones)) + 1;
                    if(isset($data->value) && $data->value > 0){
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->data = $this->chartInit($data_chart,"vertical","","line");
            return json_encode($response);
        }

        public function getTempMax(){
            $response = new stdClass();
            
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];

            foreach($semanas as $sem){

                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT MAX(temp_maxima) AS value
                            FROM datos_clima_resumen
                            WHERE semana >= 0 AND anio = {$this->session->year}
                                AND temp_maxima > 0
                                AND semana = {$sem->semana}
                                AND estacion_id = '{$estacion}'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->data = $this->chartInit($data_chart,"vertical","","line");
            return json_encode($response);
        }

        public function getLluvia(){
            $response = new stdClass();
            
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];

            foreach($semanas as $sem){

                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT IFNULL(SUM(lluvia), 0) AS value
                            FROM datos_clima_resumen
                            WHERE semana >= 0 AND anio = {$this->session->year}
                                AND semana = {$sem->semana}
                                AND estacion_id = '{$estacion}'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->data = $this->chartInit($data_chart,"vertical","","line",[], true);
            return json_encode($response);
        }

        public function getRadSolar(){
            $response = new stdClass();
            
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];

            foreach($semanas as $sem){

                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT value
                            FROM(
                            SELECT SUM(rad_solar) AS value, estacion_id
                            FROM datos_clima_resumen
                            WHERE semana >= 0 AND anio = {$this->session->year}
                                AND semana = {$sem->semana}
                                AND estacion_id = '{$estacion}'
                            GROUP BY id_hacienda) AS tbl
                            GROUP BY estacion_id";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->data = $this->chartInit($data_chart,"vertical","","line");
            return json_encode($response);
        }

        public function getHorasLuz(){
            $response = new stdClass();
            
            $filters = $this->params();
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];

            foreach($semanas as $sem){

                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT AVG(value) AS value
                            FROM (
                            SELECT SUM(horas_luz_{$filters->luz}) AS value, estacion_id
                            FROM datos_clima_resumen
                            WHERE semana >= 0 AND anio = {$this->session->year}
                                AND semana = {$sem->semana}
                                AND estacion_id = '{$estacion}'
                            GROUP BY id_hacienda) AS tbl
                            GROUP BY estacion_id";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->data = $this->chartInit($data_chart,"vertical","","line");
            return json_encode($response);
        }

        public function getRadSolarHoras(){
            $response = new stdClass();
            
            $filters = $this->params();
            if($filters->fecha_inicial == "" && $filters->fecha_final == ""){
                $filters->fecha_inicial = $this->db->queryRow("SELECT MAX(fecha) AS fecha FROM datos_clima WHERE YEAR(fecha) = {$this->session->year} AND id_cliente = '{$this->session->client}' AND id_usuario IN ('{$this->session->logges->users}') AND rad_solar > 0")->fecha;
                $filters->fecha_final = $filters->fecha_inicial;

                $response->fecha = $filters->fecha_inicial;
            }

            $horas = $this->getHorasClima($filters->fecha_inicial);
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];

            foreach($horas as $hora => $label){

                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT AVG(rad_solar) AS value
                            FROM datos_clima
                            WHERE semana >= 0 AND YEAR(fecha) = {$this->session->year}
                                AND id_cliente = '{$this->session->client}' 
                                AND id_usuario IN ({$this->session->logges->users})
                                AND HOUR(hora) = {$hora}
                                AND estacion_id = '{$estacion}'
                                AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $label, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $label, "position" => $position];
                    }
                }
            }

            $response->data = $this->chartInit($data_chart,"vertical","","line");
            return json_encode($response);
        }
    //END CLIMA

    public function getEmisionFoliar(){
        $sql_get_weeks = "SELECT getWeek(fecha) AS semana, YEAR(fecha) AS anio , foco FROM foliar 
			WHERE id_hacienda = '{$this->session->finca}' 
                AND id_usuario IN ({$this->session->logges->users}) 
                AND id_cliente = '{$this->session->client}' 
                AND (
                    YEAR(fecha) = '{$this->session->year}'
                    OR (YEAR(fecha) = {$this->session->year} - 1 AND getWeek(fecha) = 52)
                )
			GROUP BY foco , getWeek(fecha) 
			ORDER BY foco , YEAR(fecha), getWeek(fecha)";
		$weeks = $this->db->queryAll($sql_get_weeks, true);
		$count_foco=0;
		$foco = ['foco' => ''];
		$tabla = array();
		foreach ($weeks as $key => $value) {
			$semana = $weeks[$key]["semana"];
			$anio = $weeks[$key]["anio"];
			if($foco['foco'] != $weeks[$key]["foco"]){
				$foco = ['foco' => $weeks[$key]["foco"]];
				$count_foco=0;
			}


			if($semana == 1){
				$pre_semana = 52;
				$pre_anio = $anio-1;
			}else{
				$pre_semana = $semana-1;
				$pre_anio = $anio;
			}


			if($count_foco > 0){
				$sql = "
					SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
					FROM
					(SELECT 
					IF(actual.emision1 > pasada.emision1,(actual.emision1-pasada.emision1)/dif_dias*7,0) AS emision1,
					IF(actual.emision2 > pasada.emision2,(actual.emision2-pasada.emision2)/dif_dias*7,0) AS emision2,
					IF(actual.emision3 > pasada.emision3,(actual.emision3-pasada.emision3)/dif_dias*7,0) AS emision3,
					IF(actual.emision4 > pasada.emision4,(actual.emision4-pasada.emision4)/dif_dias*7,0) AS emision4,
					IF(actual.emision5 > pasada.emision5,(actual.emision5-pasada.emision5)/dif_dias*7,0) AS emision5,
					IF(actual.emision6 > pasada.emision6,(actual.emision6-pasada.emision6)/dif_dias*7,0) AS emision6,
					IF(actual.emision7 > pasada.emision7,(actual.emision7-pasada.emision7)/dif_dias*7,0) AS emision7,
					IF(actual.emision8 > pasada.emision8,(actual.emision8-pasada.emision8)/dif_dias*7,0) AS emision8,
					IF(actual.emision9 > pasada.emision9,(actual.emision9-pasada.emision9)/dif_dias*7,0) AS emision9,
					IF(actual.emision10 > pasada.emision10,(actual.emision10-pasada.emision10)/dif_dias*7,0) AS emision10,
					actual.foco
					FROM (SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS actual
					JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS pasada
					JOIN 
						(SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
						FROM
							(SELECT *
							FROM (SELECT MIN(fecha) AS dia_actual FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS actual
							JOIN (SELECT MIN(fecha) AS dia_pasado FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS pasada) AS tbl_dif) tbl_dif_d) AS tabla
					JOIN 
						(SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
						FROM
							(SELECT IF(actual.emision1 > pasada.emision1,1,0) AS emision1,IF(actual.emision2 > pasada.emision2,1,0) AS emision2,IF(actual.emision3 > pasada.emision3,1,0) AS emision3,IF(actual.emision4 > pasada.emision4,1,0) AS emision4,IF(actual.emision5 > pasada.emision5,1,0) AS emision5,IF(actual.emision6 > pasada.emision6,1,0) AS emision6,IF(actual.emision7 > pasada.emision7,1,0) AS emision7,IF(actual.emision8 > pasada.emision8,1,0) AS emision8,IF(actual.emision9 > pasada.emision9,1,0) AS emision9,IF(actual.emision10 > pasada.emision10,1,0) AS emision10
							FROM
								(SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS actual
								JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS pasada)
							AS tbl_suma)
						AS tbl_division
					";
				$result = $this->db->queryAll($sql, true);
                if($result[0]["foco"]!=""){
                    if($this->session->finca == 16 && $this->session->client == 10){
                        if((int)$semana == 24){
                            $result[0]["valor"] = "0.72";
                        }
                        if((int)$semana == 28){
                            $result[0]["valor"] = "0.65";
                        }
                    }
                    $data[$result[0]["foco"]][] = array($semana,round(((double)$result[0]["valor"]),2));
                    $tabla[$result[0]["foco"]][$semana] = (double)round(((double)$result[0]["valor"]),2);
                }
            }
			$count_foco++;
		}
		$response = (object)[];
		$response->table = (object)$tabla;
        
        $data_chart = [];
        $count = 0;
        foreach($data as $key => $value){
            foreach($value as $val){
                if($val[1] > 0){
                    $data_chart[] = ["value" => $val[1], "legend" => $key, "selected" => 1, "label" => $val[0], "position" => $count];
                }else{
                    $data_chart[] = ["value" => NULL, "legend" => $key, "selected" => 1, "label" => $val[0], "position" => $count];
                }
            }
            $count++;
        }
        $response->data = $this->chartInit($data_chart, "vertical", "", "line");
		return json_encode($response);
    }

    
    //BEGIN COMPARACION 3
        private function getFincas3MComp(){
            $response = [];
            $data = $this->db->queryAll("SELECT YEAR(m.fecha) AS anio, id_hacienda, CONCAT(YEAR(m.fecha), ' ', h.`nombre`) AS label
                FROM muestras_haciendas_3M m
                INNER JOIN cat_haciendas h ON m.id_hacienda = h.`id`
                WHERE m.id_cliente = {$this->session->client} AND m.fecha IS NOT NULL
                GROUP BY id_hacienda, YEAR(m.fecha)");
            foreach($data as $row){
                $response[$row->label] = $row;
            }
            return $response;
        }
        private function getSemanas3MComp(){
            return $this->db->queryAll("SELECT WEEK(fecha) AS semana
                FROM muestras_haciendas_3M m
                WHERE m.id_cliente = {$this->session->client} AND m.fecha IS NOT NULL AND YEAR(fecha) = {$this->session->year}
                GROUP BY WEEK(fecha)");
        }

        public function get3MComparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            $data_chart = [];
            foreach($semanas as $sem){
                $data_chart[] = ["value" => 6.5, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(hoja_mas_vieja_de_estrias) AS value
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0
                                AND muestras_haciendas_3M.id_hacienda = '{$finca->id_hacienda}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$this->session->year}
                                AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);


                    $position = array_search($finca->label, array_keys($fincas)) + 1;
                    if(isset($data->value)){
                        $data_chart[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }

                }
            }

            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line");

            return json_encode($response);
        }
        public function getH4Comparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            $data_chart_h4 = [];
            foreach($semanas as $sem){
                $data_chart_h4[] = ["value" => 30, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(IF(total_hoja_4 = 100 OR total_hoja_4 = 1, 1, 0)) AS 'h4',
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0
                                AND muestras_haciendas_3M.id_hacienda = '{$finca->id_hacienda}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$this->session->year}
                                AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + 1;
                    if(isset($data->h4)){
                        $data_chart_h4[] = ["value" => $data->h4 * 100, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_h4[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->h4 = $this->chartInit($data_chart_h4, "vertical" , "" , "line");

            return json_encode($response);
        }
        public function getH3Comparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            $data_chart_hmvle = [];
            $data_chart_hmvlqm = [];
            foreach($semanas as $sem){
                $data_chart_h3[] = ["value" => 25, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(IF(total_hoja_3 = 100 OR total_hoja_3 = 1, 1, 0)) AS 'h3',
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0
                                AND muestras_haciendas_3M.id_hacienda = '{$finca->id_hacienda}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$this->session->year}
                                AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + 1;
                    if(isset($data->h3)){
                        $data_chart_h3[] = ["value" => $data->h3 * 100, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_h3[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->h3 = $this->chartInit($data_chart_h3, "vertical" , "" , "line");

            return json_encode($response);
        }
        public function getH5Comparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            foreach($semanas as $sem){
                $data_chart_h5[] = ["value" => 35, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(IF(total_hoja_5 = 100 OR total_hoja_5 = 1, 1, 0)) AS 'h5',
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0
                                AND muestras_haciendas_3M.id_hacienda = '{$finca->id_hacienda}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$this->session->year}
                                AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + 1;
                    if(isset($data->h5)){
                        $data_chart_h5[] = ["value" => $data->h5 * 100, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_h5[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->h5 = $this->chartInit($data_chart_h5, "vertical" , "" , "line");

            return json_encode($response);
        }
        public function getHMVLQMComparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            $data_chart_hmvle = [];
            $data_chart_hmvlqm = [];
            foreach($semanas as $sem){
                $data_chart_hmvlqm[] = ["value" => 11, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(hoja_mas_vieja_de_estrias) AS hmvle,
                                AVG(hoja_mas_vieja_libre_quema_menor) AS hmvlqm,
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0
                                AND muestras_haciendas_3M.id_hacienda = '{$finca->id_hacienda}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$this->session->year}
                                AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + 1;
                    if(isset($data->hmvlqm) && $data->hmvlqm > 0){
                        $data_chart_hmvlqm[] = ["value" => $data->hmvlqm, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_hmvlqm[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->hmvlqm = $this->chartInit($data_chart_hmvlqm, "vertical" , "" , "line");

            return json_encode($response);
        }
    //END COMPARACION 3 M

    //BEGIN COMPARACION 0 SEM
        private function getFincas0SComp(){
            $response = [];
            $data = $this->db->queryAll("SELECT YEAR(m.fecha) AS anio, id_hacienda, CONCAT(YEAR(m.fecha), ' ', h.`nombre`) AS label
                FROM muestras_haciendas m
                INNER JOIN cat_haciendas h ON m.id_hacienda = h.`id`
                WHERE m.id_cliente = {$this->session->client} AND tipo_semana = 0 AND m.fecha IS NOT NULL
                GROUP BY id_hacienda, YEAR(m.fecha)");
            foreach($data as $row){
                $response[$row->label] = $row;
            }
            return $response;
        }
        private function getSemanas0SComp(){
            return $this->db->queryAll("SELECT WEEK(fecha) AS semana
                FROM muestras_haciendas m
                WHERE m.id_cliente = {$this->session->client} AND fecha IS NOT NULL AND tipo_semana = 0
                GROUP BY WEEK(fecha)");
        }

        public function getHT0SComparativo(){
            $response = new stdClass();

            $fincas = $this->getFincas0SComp();
            $semanas = $this->getSemanas0SComp();

            $data_chart_ht0s = [];

            foreach($semanas as $sem){
                $data_chart_ht0s[] = ["value" => 13, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hojas_totales) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana >= 0 AND anio = {$this->session->year}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 0
                            AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + 1;
                    if(isset($data->value) && $data->value > 0){
                        $data_chart_ht0s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_ht0s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->ht = $this->chartInit($data_chart_ht0s,"vertical","","line");
            return json_encode($response);
        }

        public function getHMVLE0SComparativo(){
            $response = new stdClass();

            $fincas = $this->getFincas0SComp();
            $semanas = $this->getSemanas0SComp();

            $data_chart_hmvle0s = [];

            foreach($semanas as $sem){
                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hojas_mas_vieja_libre) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana >= 0 AND anio = {$this->session->year}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 0
                            AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas));

                    if(isset($data->value) && $data->value > 0){
                        $data_chart_hmvle0s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_hmvle0s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->hmvle = $this->chartInit($data_chart_hmvle0s,"vertical","","line");
            return json_encode($response);
        
        }

        public function getHMVLQM0SComparativo(){
            $response = new stdClass();

            $fincas = $this->getFincas0SComp();
            $semanas = $this->getSemanas0SComp();

            $data_chart_hmvlqm0s = [];

            foreach($semanas as $sem){
                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hoja_mas_vieja_libre_quema_menor) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana >= 0 AND anio = {$this->session->year}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 0
                            AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas));

                    if(isset($data->value) && $data->value > 0){
                        $data_chart_hmvlqm0s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_hmvlqm0s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->hmvlqm = $this->chartInit($data_chart_hmvlqm0s,"vertical","","line");
            return json_encode($response);
        }
    //END COMPARACION 0 SEM

    //BEGIN COMPARACION 11 SEM
        private function getFincas11SComp(){
            $response = [];
            $data = $this->db->queryAll("SELECT YEAR(m.fecha) AS anio, id_hacienda, CONCAT(YEAR(m.fecha), ' ', h.`nombre`) AS label
                FROM muestras_haciendas m
                INNER JOIN cat_haciendas h ON m.id_hacienda = h.`id`
                WHERE m.id_cliente = {$this->session->client} AND tipo_semana = 11 AND m.fecha IS NOT NULL
                GROUP BY id_hacienda, YEAR(m.fecha)");
            foreach($data as $row){
                $response[$row->label] = $row;
            }
            return $response;
        }
        private function getSemanas11SComp(){
            return $this->db->queryAll("SELECT WEEK(fecha) AS semana
                FROM muestras_haciendas m
                WHERE m.id_cliente = {$this->session->client} AND fecha IS NOT NULL AND tipo_semana = 11
                GROUP BY WEEK(fecha)");
        }


        public function getHT11SComparativo(){
            $response = new stdClass();

            $semanas = $this->getSemanas11SComp();
            $fincas = $this->getFincas11SComp();

            $data_chart_ht11s = [];

            foreach($semanas as $sem){
                $data_chart_ht11s[] = ["value" => 6.5, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hojas_totales) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana >= 0 AND anio = {$this->session->year}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 11
                            AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label ,array_keys($fincas)) + 1;
                    if(isset($data->value) && $data->value > 0){
                        $data_chart_ht11s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_ht11s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->ht = $this->chartInit($data_chart_ht11s,"vertical","","line");
            return json_encode($response);
        }

        public function getHMVLQM11SComparativo(){
            $response = new stdClass();

            $semanas = $this->getSemanas11SComp();
            $fincas = $this->getFincas11SComp();

            $data_chart_hmvlqm11s = [];

            foreach($semanas as $sem){
                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hoja_mas_vieja_libre_quema_menor) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana >= 0 AND anio = {$this->session->year}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 11
                            AND foco = 'Resto Finca'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas));

                    if(isset($data->value) && $data->value > 0){
                        $data_chart_hmvlqm11s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_hmvlqm11s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->hmvlqm = $this->chartInit($data_chart_hmvlqm11s,"vertical","","line");
            return json_encode($response);
        }
    //END COMPARACION 11 SEM

    public function importarExcel(){
        $target_name = "./../excel/inspeccionPrimaDonna/user_1/data.xlsx";
        
        $postdata = (object) json_decode(file_get_contents("php://input"));
        $file_data = explode(",", $postdata->upload)[1];
        $file_data = base64_decode($file_data);
        file_put_contents($target_name, $file_data);
        $data = $this->procesar_xlsx($target_name);
        
        $move_name = "./../excel/inspeccionPrimaDonna/completados/data_".date('Ymd_his').".xlsx";
        rename($target_name, $move_name);
        return $data;
    }

    private function procesar_xlsx($file_name){
        include './../excel/simplexlsx.class.php';
        $xlsx = new SimpleXLSX($file_name);
        $count = 0;
        $libros = $xlsx->sheetNames();
        $registros = array();
        $foliar = [];
        
        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key > 0){
                    $fecha = $fila[0];
                    $finca = trim($fila[3]);
                    $lote = $fila[4];
                    $foco = $fila[5];

                    if(is_numeric($fecha)){
                        $UNIX_DATE = ($fecha - 25569) * 86400;
                        $fecha = gmdate("Y-m-d", $UNIX_DATE);
                    }else{
                        $fecha = explode("/", $fecha);
                        $fecha = "{$fecha[2]}-{$fecha[1]}-{$fecha[0]}";
                    }

                    $fincaExist = $this->db->queryOne("SELECT COUNT(1) AS c FROM cat_haciendas WHERE nombre = '{$finca}' AND id_cliente = {$this->session->client}");
                    if($fincaExist == 0){
                        $this->db->query("INSERT INTO cat_haciendas SET nombre = '{$finca}', id_cliente = {$this->session->client}, id_usuario = 1, status = 1");
                        $id_hacienda = $this->db->getLastID();
                    }else{
                        $id_hacienda = $this->db->queryOne("SELECT id FROM cat_haciendas WHERE nombre = '{$finca}' AND id_cliente = {$this->session->client}");
                    }
                    $loteExist = $this->db->queryOne("SELECT COUNT(1) AS c FROM cat_lotes WHERE id_hacienda = {$id_hacienda} AND id_agrupacion = 1 AND nombre = '{$lote}' AND id_cliente = {$this->session->client}");
                    if($loteExist == 0){
                        $this->db->query("INSERT INTO cat_lotes SET id_hacienda = {$id_hacienda}, id_agrupacion = 1, nombre = '{$lote}', id_usuario = 1, status = 1, id_cliente = {$this->session->client}");
                        $id_lote = $this->db->getLastID();
                    }else{
                        $id_lote = $this->db->queryOne("SELECT id FROM cat_lotes WHERE id_hacienda = {$id_hacienda} AND id_agrupacion = 1 AND nombre = '{$lote}' AND id_cliente = {$this->session->client}");
                    }
                    
                    if($fila[7] == '3M'){
                        $sql = "INSERT INTO muestras_haciendas_3M SET
                                    id_cliente = {$this->session->client},
                                    id_hacienda = {$id_hacienda},
                                    fecha = '{$fecha}',
                                    id_usuario = 1";
                        $this->db->query($sql);
                        $id_muestra_3M = $this->db->getLastID();

                        $sql = "INSERT INTO muestras_hacienda_detalle_3M SET
                                    id_Mhacienda = '{$id_muestra_3M}',
                                    id_hacienda = {$id_hacienda},
                                    id_usuario = 1,
                                    id_cliente = {$this->session->client},
                                    id_lote = {$id_lote},
                                    lote = '{$lote}',
                                    foco = '{$foco}',
                                    hojas_totales = '{$fila[11]}',
                                    hoja_mas_vieja_de_estrias = '{$fila[12]}',
                                    hoja_mas_vieja_libre_quema_menor = '{$fila[13]}',
                                    total_hoja_3 = '{$fila[8]}',
                                    total_hoja_4 = '{$fila[9]}',
                                    total_hoja_5 = '{$fila[10]}',
                                    muestra = '{$fila[6]}',
                                    fecha = '{$fecha}',
                                    semana = WEEK('{$fecha}'),
                                    anio = YEAR('{$fecha}')";
                        $this->db->query($sql);
                        D($sql);
                    }
                    else if($fila[7] == 'P0'){
                        $sql = "INSERT INTO muestras_haciendas SET
                                    id_usuario = 1,
                                    id_cliente = {$this->session->client},
                                    cliente = 'PRIMA DONNA',
                                    id_hacienda = '{$id_hacienda}',
                                    hacienda = '{$finca}',
                                    fecha = '{$fecha}',
                                    tipo_semana = 0";
                        $this->db->query($sql);
                        $id_muestra = $this->db->getLastID();

                        $sql = "INSERT INTO muestras_hacienda_detalle SET
                                    id_Mhacienda = '{$id_muestra}',
                                    id_hacienda = {$id_hacienda},
                                    tipo_semana = 0,
                                    id_usuario = 1,
                                    id_cliente = {$this->session->client},
                                    lote = '{$lote}',
                                    id_lote = {$id_lote},
                                    foco = '{$foco}',
                                    muestra = '{$fila[6]}',
                                    hojas_totales = '{$fila[11]}',
                                    hojas_mas_vieja_libre = '{$fila[12]}',
                                    hoja_mas_vieja_libre_quema_menor = '{$fila[13]}',
                                    fecha = '{$fecha}',
                                    semana = WEEK('{$fecha}'),
                                    anio  = YEAR('{$fecha}')";
                        D($sql);
                        $this->db->query($sql);
                    }
                    else if($fila[7] == 'P11'){
                        $sql = "INSERT INTO muestras_haciendas SET
                                    id_usuario = 1,
                                    id_cliente = {$this->session->client},
                                    cliente = 'PRIMA DONNA',
                                    id_hacienda = '{$id_hacienda}',
                                    hacienda = '{$finca}',
                                    fecha = '{$fecha}',
                                    tipo_semana = 11";
                        $this->db->query($sql);
                        $id_muestra = $this->db->getLastID();

                        $sql = "INSERT INTO muestras_hacienda_detalle SET
                                    id_Mhacienda = '{$id_muestra}',
                                    id_hacienda = {$id_hacienda},
                                    tipo_semana = 11,
                                    id_usuario = 1,
                                    id_cliente = {$this->session->client},
                                    lote = '{$lote}',
                                    id_lote = {$id_lote},
                                    foco = '{$foco}',
                                    muestra = '{$fila[6]}',
                                    hojas_totales = '{$fila[11]}',
                                    hojas_mas_vieja_libre = '{$fila[12]}',
                                    hoja_mas_vieja_libre_quema_menor = '{$fila[13]}',
                                    fecha = '{$fecha}',
                                    semana = WEEK('{$fecha}'),
                                    anio  = YEAR('{$fecha}')";
                        D($sql);
                        $this->db->query($sql);
                    }
                    
                    if($fila[14]){
                        //FOLIAR
                        $num_emision = trim($fila[14]);
                        $valor = (trim($fila[16]) != '') ? ((float) trim($fila[16])) : 'NULL';

                        $foliar[$num_emision] = $valor;
                        if($fila[14] == '10'){
                            $sql = "INSERT INTO foliar SET
                                        fecha = '{$fecha}',
                                        id_usuario = 1,
                                        id_cliente = 25,
                                        id_hacienda = (SELECT id FROM cat_haciendas WHERE nombre = '{$finca}'),
                                        emision1 = {$foliar['1']},
                                        emision2 = {$foliar['2']},
                                        emision3 = {$foliar['3']},
                                        emision4 = {$foliar['4']},
                                        emision5 = {$foliar['5']},
                                        emision6 = {$foliar['6']},
                                        emision7 = {$foliar['7']},
                                        emision8 = {$foliar['8']},
                                        emision9 = {$foliar['9']},
                                        emision10 = {$foliar['10']},
                                        foco = 'NORMAL'";
                            $this->db->query($sql);
                            D($sql);
                            $foliar = [];
                        }
                    }
                }
            }
            break;
        }
    
    }

    private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = [], $zoom = false){
		$response = new stdClass;
		$response->chart = [];
		function object_to_array($obj) {
		    if(is_object($obj)) $obj = (array) $obj;
		    if(is_array($obj)) {
		        $new = array();
		        foreach($obj as $key => $val) {
		            $new[$key] = object_to_array($val);
		        }
		    }
		    else $new = $obj;
		    return $new;       
		}

		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["bottom"] = ($zoom) ? "90%" : "0%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "20%";
			$response->chart["grid"]["containLabel"] = true;
            $response->chart["plotOptions"]["series"]["connectNulls"] = true;
            if($zoom){
                $response->chart["dataZoom"] = [
                    [
                        "show" => true,
                        "realtime" => true
                    ],
                    [
                        "type" => "inside",
                        "realtime" => true
                    ]
                ];
            }
            if($type == "line"){
                $response->chart["yAxis"]["min"] = 'dataMin';
				$response->chart["xAxis"]["data"] = [];
			}
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => []];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "value" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value" , "boundaryGap" => true , "axisLine" => ["onZero" => true]];
			}
			$response->chart["series"] = [];
			$count = 0;
			$positionxAxis = -1;
			$position = -1;
			$colors = [];
			foreach ($data as $key => $value) {
                $value = (object)$value;
				$value->legend = strtoupper($value->legend);
				$value->label = strtoupper($value->label);
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->positionxAxis)){
					$positionxAxis = $value->positionxAxis;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if($type == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						if(is_numeric($value->label)){
							$value->label = (double)$value->label;
						}
						if(!isset($value->positionxAxis)){
							$response->chart["xAxis"]["data"][] = $value->label;
						}else{
							$value->label = array($value->label);
							if(count($response->chart["xAxis"]["data"]) <= 0){
								while (count($response->chart["xAxis"]["data"]) <= $value->positionxAxis) {
									$response->chart["xAxis"]["data"][] = NULL;
								}
							}
							array_splice($response->chart["xAxis"]["data"] , $value->positionxAxis , 0 , $value->label);
							$response->chart["xAxis"]["data"] = array_filter($response->chart["xAxis"]["data"]);
                        }
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
                        }
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"connectNulls" => true,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
                        if(isset($value->z)){
                            $response->chart["series"][$position]["z"] = $value->z;
                        }
					}
                    
					if(is_numeric($value->value)){
						$response->chart["series"][$position]["data"][] = ROUND($value->value,2);
					}else{
						$response->chart["series"][$position]["data"][] = $value->value;
					}

				}else{
					$response->chart["legend"]["data"][] = $value->label;
					// if($count == 0){
						$response->chart["series"][$count] = [
							"name" => $value->label,
							"type" => $type,
							"data" => [(int)$value->value],
							"label" => [
								"normal" => ["show" => true , "position" => "top"],
								"emphasis" => ["show" => true , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					// }
				}
				$count++;
            }

            if(count($response->chart["xAxis"]["data"]) < 2){
                foreach($response->chart["series"] as $key => $data){
                    $response->chart["series"][$key]["type"] = "bar";
                }
            }
        }

		return $response->chart;
    }
}

?>