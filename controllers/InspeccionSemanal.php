<?php

class InspeccionSemanal {
	
	private $db;
	private $session;
	
	public function __construct() {
        $this->db = new M_Conexion();
        $this->session = Session::getInstance();
    }

    public function last(){
        $response = new stdClass;
        $response->semana = $this->db->queryOne("SELECT MAX(getWeek(fecha)) 
                FROM muestras_haciendas_3M 
                WHERE YEAR(fecha) = {$this->session->year}
                    AND id_hacienda = {$this->session->finca}");
        return json_encode($response);
    }

    public function getSemanasFincas(){
        $sql = "SELECT getWeek(fecha)
                FROM muestras_haciendas_3M
                WHERE YEAR(fecha) = {$this->session->year}
                    AND id_hacienda = {$this->session->finca}
                GROUP BY getWeek(fecha)";

        $data = $this->db->queryAllOne($sql);
        return json_encode($data);
    }

    public function getFincasCliente(){
        $sql = "SELECT id_hacienda AS id, IFNULL(alias, nombre) AS nombre, nombre nombre_real, alias
                FROM muestras_haciendas_3M m
                INNER JOIN cat_haciendas ON id_hacienda = cat_haciendas.id
                WHERE YEAR(m.fecha) = {$this->session->year}
                    AND m.id_cliente = {$this->session->id_client}
                GROUP BY m.id_hacienda
                ORDER BY nombre";
        return $this->db->queryAll($sql);
    }

    public function dataGraficas(){
        $response = new stdClass;

        $data = (object)json_decode(file_get_contents("php://input"));
        $semana = (int) $data->semana;
        $anio = $this->session->year;
        
        $fincas = $this->getFincasCliente();
        $response->fincas = $fincas;
        foreach($fincas as $index => $f){
            #$response->m3_h3[] = ["label" => $f->nombre, "legend" => "UMBRAL", "value" => 25, "selected" => true];
            $response->m3_h3[] = ["label" => $f->nombre, "legend" => "FOCO", "value" => $this->getM3H3($f->id, $semana, $anio, false), "selected" => true];
            $response->m3_h3[] = ["label" => $f->nombre, "legend" => "NORMAL", "value" => $this->getM3H3($f->id, $semana, $anio), "selected" => true];

            #$response->m3_h4[] = ["label" => $f->nombre, "legend" => "UMBRAL", "value" => 30, "selected" => true];
            $response->m3_h4[] = ["label" => $f->nombre, "legend" => "FOCO", "value" => $this->getM3H4($f->id, $semana, $anio, false), "selected" => true];
            $response->m3_h4[] = ["label" => $f->nombre, "legend" => "NORMAL", "value" => $this->getM3H4($f->id, $semana, $anio), "selected" => true];

            #$response->m3_h5[] = ["label" => $f->nombre, "legend" => "UMBRAL", "value" => 35, "selected" => true];
            $response->m3_h5[] = ["label" => $f->nombre, "legend" => "FOCO", "value" => $this->getM3H5($f->id, $semana, $anio, false), "selected" => true];
            $response->m3_h5[] = ["label" => $f->nombre, "legend" => "NORMAL", "value" => $this->getM3H5($f->id, $semana, $anio), "selected" => true];

            #$response->m3_hvle[] = ["label" => $f->nombre, "legend" => "UMBRAL", "value" => 6.5, "selected" => true];
            $response->m3_hvle[] = ["label" => $f->nombre, "legend" => "FOCO", "value" => $this->getM3HVLE($f->id, $semana, $anio, false), "selected" => true];
            $response->m3_hvle[] = ["label" => $f->nombre, "legend" => "NORMAL", "value" => $this->getM3HVLE($f->id, $semana, $anio), "selected" => true];

            #$response->m3_hvlq[] = ["label" => $f->nombre, "legend" => "UMBRAL", "value" => 11, "selected" => true];
            $response->m3_hvlq[] = ["label" => $f->nombre, "legend" => "FOCO", "value" => $this->getM3HVLQ($f->id, $semana, $anio, false), "selected" => true];
            $response->m3_hvlq[] = ["label" => $f->nombre, "legend" => "NORMAL", "value" => $this->getM3HVLQ($f->id, $semana, $anio), "selected" => true];

            #$response->s0_ht[] = ["label" => $f->nombre, "legend" => "UMBRAL", "value" => 13, "selected" => true];
            $response->s0_ht[] = ["label" => $f->nombre, "legend" => "FOCO", "value" => $this->getS0HT($f->id, $semana, $anio, false), "selected" => true];
            $response->s0_ht[] = ["label" => $f->nombre, "legend" => "NORMAL", "value" => $this->getS0HT($f->id, $semana, $anio), "selected" => true];

            #$response->s0_hvle[] = ["label" => $f->nombre, "legend" => "UMBRAL", "value" => 12.5, "selected" => true];
            $response->s0_hvle[] = ["label" => $f->nombre, "legend" => "FOCO", "value" => $this->getS0HVLE($f->id, $semana, $anio, false), "selected" => true];
            $response->s0_hvle[] = ["label" => $f->nombre, "legend" => "NORMAL", "value" => $this->getS0HVLE($f->id, $semana, $anio), "selected" => true];

            #$response->s0_hvlq[] = ["label" => $f->nombre, "legend" => "UMBRAL", "value" => 15, "selected" => true];
            $response->s0_hvlq[] = ["label" => $f->nombre, "legend" => "FOCO", "value" => $this->getS0HVLQ($f->id, $semana, $anio, false), "selected" => true];
            $response->s0_hvlq[] = ["label" => $f->nombre, "legend" => "NORMAL", "value" => $this->getS0HVLQ($f->id, $semana, $anio), "selected" => true];

            #$response->s11_ht[] = ["label" => $f->nombre, "legend" => "UMBRAL", "value" => 6.5, "selected" => true];
            $response->s11_ht[] = ["label" => $f->nombre, "legend" => "FOCO", "value" => $this->getS11HT($f->id, $semana, $anio, false), "selected" => true];
            $response->s11_ht[] = ["label" => $f->nombre, "legend" => "NORMAL", "value" => $this->getS11HT($f->id, $semana, $anio), "selected" => true];

            #$response->s11_hvlq[] = ["label" => $f->nombre, "legend" => "UMBRAL", "value" => 15.5, "selected" => true];
            $response->s11_hvlq[] = ["label" => $f->nombre, "legend" => "FOCO", "value" => $this->getS11HVLQ($f->id, $semana, $anio, false), "selected" => true];
            $response->s11_hvlq[] = ["label" => $f->nombre, "legend" => "NORMAL", "value" => $this->getS11HVLQ($f->id, $semana, $anio), "selected" => true];
        }

        $umbral = $this->db->queryOne("SELECT m3_h3 FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
        if(!$umbral > 0) $umbral = 200;
        $response->m3_h3 = $this->generateSeries($response->m3_h3, $umbral);

        $umbral = $this->db->queryOne("SELECT m3_h4 FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
        if(!$umbral > 0) $umbral = 300;
        $response->m3_h4 = $this->generateSeries($response->m3_h4, $umbral);

        $umbral = $this->db->queryOne("SELECT m3_h5 FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
        if(!$umbral > 0) $umbral = 300;
        $response->m3_h5 = $this->generateSeries($response->m3_h5, $umbral);

        $umbral = $this->db->queryOne("SELECT m3_hvle FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
        if(!$umbral > 0) $umbral = 6.5;
        $response->m3_hvle = $this->generateSeries($response->m3_hvle, $umbral);

        $umbral = $this->db->queryOne("SELECT m3_hvlq FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
        if(!$umbral >= 0) $umbral = 11;
        $response->m3_hvlq = $this->generateSeries($response->m3_hvlq, $umbral);

        $umbral = $this->db->queryOne("SELECT s0_ht FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
        if(!$umbral > 0) $umbral = 13;
        $response->s0_ht = $this->generateSeries($response->s0_ht, $umbral);

        $umbral = $this->db->queryOne("SELECT s0_hvle FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
        if(!$umbral > 0) $umbral = 0;
        $response->s0_hvle = $this->generateSeries($response->s0_hvle, $umbral);

        $umbral = $this->db->queryOne("SELECT s0_hvlq FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
        if(!$umbral > 0) $umbral = 0;
        $response->s0_hvlq = $this->generateSeries($response->s0_hvlq, $umbral);
        
        $umbral = $this->db->queryOne("SELECT s11_ht FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
        if(!$umbral > 0) $umbral = 6.5;
        $response->s11_ht = $this->generateSeries($response->s11_ht, $umbral);

        $umbral = $this->db->queryOne("SELECT s11_hvlq FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
        if(!$umbral > 0) $umbral = 0;
        $response->s11_hvlq = $this->generateSeries($response->s11_hvlq, $umbral);
        return json_encode($response);
    }

    private function getM3H3($finca, $semana, $anio, $resto_finca = true){
        $foco = $resto_finca ? '=' : '!=';
        $area = $this->session->client == 25 ? 'NORMAL' : 'AREA NORMAL';

        if($this->session->client == 25){
            $conteo = " (SELECT count(*) FROM muestras_hacienda_detalle_3M
                        WHERE anio = {$anio}
                            AND id_hacienda = {$finca}
                            AND semana = {$semana}
                            AND foco {$foco} '$area'
                            AND total_hoja_3 != '') ";
                            
            $sql = "SELECT ROUND((letras_h3 * numeros_h3 * 120), 2)
                    FROM (
                        SELECT 
                            SUM(IF(total_hoja_3 = '1A', 0, IF(total_hoja_3 LIKE '%A' = 1, 1, IF(total_hoja_3 LIKE '%B' = 1, 2, 3))))/{$conteo} AS letras_h3,
                            SUM(IF(total_hoja_3 = '1A', 0, IF(total_hoja_3 LIKE '1%' = 1, 1, IF(total_hoja_3 LIKE '2%' = 1, 2, 3))))/{$conteo} AS numeros_h3
                        FROM muestras_hacienda_detalle_3M
                        WHERE anio = {$anio}
                            AND id_hacienda = {$finca}
                            AND semana = {$semana}
                            AND foco {$foco} '$area'
                            AND total_hoja_3 != ''
                    ) AS tbl";
        }else{
            $sql = "SELECT
                        AVG(IF(total_hoja_3 = 100 OR total_hoja_3 = 1, 1, 0)) AS 'h3'
                    FROM muestras_hacienda_detalle_3M
                    WHERE anio = {$anio} 
                        AND semana = {$semana} 
                        AND id_hacienda = {$finca} 
                        AND foco {$foco} '$area'";
        }
        return $this->db->queryOne($sql);
    }
    private function getM3H4($finca, $semana, $anio, $resto_finca = true){
        $foco = $resto_finca ? '=' : '!=';
        $area = $this->session->client == 25 ? 'NORMAL' : 'AREA NORMAL';

        if($this->session->client == 25){
         $conteo = " (SELECT count(*) FROM muestras_hacienda_detalle_3M
                        WHERE anio = {$anio}
                            AND id_hacienda = {$finca}
                            AND semana = {$semana}
                            AND foco {$foco} '$area'
                            AND total_hoja_4 != '') ";
                            
            $sql = "SELECT ROUND((letras_h4 * numeros_h4 * 100), 2)
                    FROM(
                        SELECT 
                            SUM(IF(total_hoja_4 = '1A', 0, IF(total_hoja_4 LIKE '%A' = 1, 1, IF(total_hoja_4 LIKE '%B' = 1, 2, 3))))/{$conteo} AS letras_h4,
                            SUM(IF(total_hoja_4 = '1A', 0, IF(total_hoja_4 LIKE '1%' = 1, 1, IF(total_hoja_4 LIKE '2%' = 1, 2, 3))))/{$conteo} AS numeros_h4
                        FROM muestras_hacienda_detalle_3M
                        WHERE anio = {$anio}
                            AND id_hacienda = {$finca}
                            AND semana = {$semana}
                            AND foco {$foco} '$area'
                            AND total_hoja_4 != ''
                    ) AS tbl";
        }else{
            $sql = "SELECT
                        AVG(IF(total_hoja_4 = 100 OR total_hoja_4 = 1, 1, 0)) AS 'h3'
                    FROM muestras_hacienda_detalle_3M
                    WHERE anio = {$anio} 
                        AND semana = {$semana} 
                        AND id_hacienda = {$finca} 
                        AND foco {$foco} '$area'";
        }
        return $this->db->queryOne($sql);
    }
    private function getM3H5($finca, $semana, $anio, $resto_finca = true){
        $foco = $resto_finca ? '=' : '!=';
        $area = $this->session->client == 25 ? 'NORMAL' : 'AREA NORMAL';

        if($this->session->client == 25){
        $conteo = " (SELECT count(*) FROM muestras_hacienda_detalle_3M
                        WHERE anio = {$anio}
                            AND id_hacienda = {$finca}
                            AND semana = {$semana}
                            AND foco {$foco} '$area'
                            AND total_hoja_5 != '') ";
                            
            $sql = "SELECT ROUND((letras_h5 * numeros_h5 * 80), 2)
                    FROM (
                        SELECT 
                            SUM(IF(total_hoja_5 = '1A', 0, IF(total_hoja_5 LIKE '%A' = 1, 1, IF(total_hoja_5 LIKE '%B' = 1, 2, 3))))/{$conteo} AS letras_h5,
                            SUM(IF(total_hoja_5 = '1A', 0, IF(total_hoja_5 LIKE '1%' = 1, 1, IF(total_hoja_5 LIKE '2%' = 1, 2, 3))))/{$conteo} AS numeros_h5
                        FROM muestras_hacienda_detalle_3M
                        WHERE anio = {$anio}
                            AND id_hacienda = '{$finca}' 
                            AND semana = {$semana}
                            AND foco {$foco} '$area'
                            AND total_hoja_5 != ''
                    ) AS tbl";
        }else{
            $sql = "SELECT
                        AVG(IF(total_hoja_5 = 100 OR total_hoja_5 = 1, 1, 0)) AS 'h3'
                    FROM muestras_hacienda_detalle_3M
                    WHERE anio = {$anio} 
                        AND semana = {$semana} 
                        AND id_hacienda = {$finca} 
                        AND foco {$foco} '$area'";
        }
        return $this->db->queryOne($sql);
    }
    private function getM3HVLE($finca, $semana, $anio, $resto_finca = true){
        $foco = $resto_finca ? '=' : '!=';
        $area = $this->session->client == 25 ? 'NORMAL' : 'AREA NORMAL';
        $sql = "SELECT
                    AVG(hoja_mas_vieja_de_estrias) AS value
                FROM muestras_hacienda_detalle_3M
                WHERE anio = {$anio} AND semana = {$semana} AND id_hacienda = {$finca} AND foco {$foco} '$area'";
        return $this->db->queryOne($sql);
    }
    private function getM3HVLQ($finca, $semana, $anio, $resto_finca = true){
        $foco = $resto_finca ? '=' : '!=';
        $area = $this->session->client == 25 ? 'NORMAL' : 'AREA NORMAL';
        $sql = "SELECT
                    AVG(hoja_mas_vieja_libre_quema_menor) AS value
                FROM muestras_hacienda_detalle_3M
                WHERE anio = {$anio} AND semana = {$semana} AND id_hacienda = {$finca} AND foco {$foco} '$area'";
        return $this->db->queryOne($sql);
    }
    private function getS0HT($finca, $semana, $anio, $resto_finca = true){
        $foco = $resto_finca ? '=' : '!=';
        $area = $this->session->client == 25 ? 'NORMAL' : 'AREA NORMAL';
        $sql = "SELECT
                    AVG(hojas_totales) AS value
                FROM muestras_hacienda_detalle
                WHERE anio = {$anio} AND semana = {$semana} 
                    AND id_hacienda = {$finca} 
                    AND foco {$foco} '$area' 
                    AND tipo_semana = 0";
        return $this->db->queryOne($sql);
    }
    private function getS0HVLE($finca, $semana, $anio, $resto_finca = true){
        $foco = $resto_finca ? '=' : '!=';
        $area = $this->session->client == 25 ? 'NORMAL' : 'AREA NORMAL';
        $sql = "SELECT
                    AVG(hojas_mas_vieja_libre) AS value
                FROM muestras_hacienda_detalle
                WHERE anio = {$anio} AND semana = {$semana} AND id_hacienda = {$finca} AND foco {$foco} '$area' AND tipo_semana = 0";
        return $this->db->queryOne($sql);
    }
    private function getS0HVLQ($finca, $semana, $anio, $resto_finca = true){
        $foco = $resto_finca ? '=' : '!=';
        $area = $this->session->client == 25 ? 'NORMAL' : 'AREA NORMAL';
        $sql = "SELECT
                    AVG(hoja_mas_vieja_libre_quema_menor) AS value
                FROM muestras_hacienda_detalle
                WHERE anio = {$anio} AND semana = {$semana} AND id_hacienda = {$finca} AND foco {$foco} '$area' AND tipo_semana = 0";
        return $this->db->queryOne($sql);
    }
    private function getS11HT($finca, $semana, $anio, $resto_finca = true){
        $foco = $resto_finca ? '=' : '!=';
        $area = $this->session->client == 25 ? 'NORMAL' : 'AREA NORMAL';
        $sql = "SELECT
                    AVG(hojas_totales) AS value
                FROM muestras_hacienda_detalle
                WHERE anio = {$anio} 
                    AND semana = {$semana} 
                    AND id_hacienda = {$finca} 
                    AND foco {$foco} '$area' 
                    AND tipo_semana = 11";
        return $this->db->queryOne($sql);
    }
    private function getS11HVLQ($finca, $semana, $anio, $resto_finca = true){
        $foco = $resto_finca ? '=' : '!=';
        $area = $this->session->client == 25 ? 'NORMAL' : 'AREA NORMAL';
        $sql = "SELECT
                    AVG(hoja_mas_vieja_libre_quema_menor) AS value
                FROM muestras_hacienda_detalle
                WHERE anio = {$anio} 
                    AND semana = {$semana} 
                    AND id_hacienda = {$finca} 
                    AND foco {$foco} '$area' 
                    AND tipo_semana = 11";
        return $this->db->queryOne($sql);
    }

    private function generateSeries($res, $umbral = null){
		$response->data = [];
		$response->legend = [];
        $response->avg = 0;
        $response->umbral = $umbral;
		$count = 0;
        $sum = 0;
        $max = ($umbral != null) ? $umbral : null;
		$series = new stdClass;
		$finca = "";
		$flag_count = 0;
		$markLine = new stdClass;
		$labels = [];

        foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->promedio = (float)$value->value;
            $sum += $value->value;
            
            if(!in_array($value->label, $response->legend)) $response->legend[] = $value->label;

			if(!isset($response->data[$value->legend]->itemStyle)){
				$series = new stdClass;
				$series->name = $value->legend;
				$series->type = "bar";
				$series->connectNulls = true;
				foreach($labels as $lbl){
					$series->data[] = null;
                }

                $value->value = (is_numeric($value->value)) ? round($value->value, 2) : $value->value;
                if($max == null || $max < $value->value) $max = $value->value;
				$series->data[array_search($value->label, $response->legend)] = (is_numeric($value->value)) ? round($value->value ,2) : $value->value;
				$series->itemStyle = (object)[
					"normal" => [
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => true , "position" => "insideTop"]
					]
				];
				$response->data[$value->legend] = $series;
			}else{
                $value->value = (is_numeric($value->value)) ? round($value->value, 2) : $value->value;
                if($max == null || $max < $value->value) $max = $value->value;
				$response->data[$value->legend]->data[array_search($value->label, $response->legend)] = (is_numeric($value->value)) ? round($value->value ,2) : $value->value;
			}
			$count++;
		}

        $response->max = $max;
		$response->avg = ($sum / $count);

        return $response;
	}
    
}

?>