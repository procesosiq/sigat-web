<?php

class Inspeccion {
	
	private $bd;
	private $session;
	
	public function __construct() {
        $this->db = new M_Conexion();
        $this->session = Session::getInstance();
    }

    public function clientesDisponibles(){
        $sql = "SELECT id_cliente AS id, nombre
                FROM muestras_haciendas m
                INNER JOIN cat_clientes ON id_cliente = cat_clientes.`id`
                WHERE m.id_usuario IN ({$this->session->logges->users})
                    AND YEAR(m.fecha) = {$this->session->year}
                GROUP BY id_cliente";
        $data = (object) $this->db->Consultas(2, $sql);
        return json_encode($data);
    }

    private function params(){
        $data = (object)json_decode(file_get_contents("php://input"));
        $params = new stdClass;
        $params->semana = $data->semana;
        $params->luz = $data->luz;
        $params->fecha_inicial = $data->fecha_inicial;
        $params->fecha_final = $data->fecha_final;
        return $params;
    }

    public function index(){
        $response = new stdClass;
        $filters = $this->params();

        $response->semanas = $this->getSemanas();
        if($filters->semana == null){
            $keys = array_keys($response->semanas);
            $response->last_semana = $filters->semana = $keys[count($keys)-1];
        }
        $response->table_lote = $this->getSigatokaPorLoteSemana($filters->semana, $filters);

        $response->table_lote_anio = [];
        foreach($response->semanas as $sem){
            $response->table_lote_anio[$sem] = $this->getSigatokaPorLoteSemana($sem, $filters);
        }

        return json_encode($response);
    }

    private function getSigatokaPorLoteSemana($semana, $filters){
        $response = $this->getLotes($filters);
        foreach($response as $lote){
            $m3 = $this->db->queryRow("SELECT 
                    ROUND(AVG(IF(total_hoja_3 = 100 OR total_hoja_3 = 1, 1, 0)) * 100, 2) AS h3, 
                    ROUND(AVG(IF(total_hoja_4 = 100 OR total_hoja_4 = 1, 1, 0)) * 100, 2) AS h4, 
                    ROUND(AVG(IF(total_hoja_5 = 100 OR total_hoja_5 = 1, 1, 0)) * 100, 2) AS h5,
                    ROUND(AVG(hoja_mas_vieja_de_estrias)) AS hmvle,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor)) AS hmvlqm
                FROM muestras_hacienda_detalle_3M
                WHERE id_lote = '{$lote->id}' AND semana = '{$semana}'
                    AND id_hacienda = '{$this->session->finca}' 
                    AND id_cliente = '{$this->session->client}' 
                    AND id_usuario IN ('{$this->session->logges->users}')");
            $lote->m3_h3 = $m3->h3;
            $lote->m3_h4 = $m3->h4;
            $lote->m3_h5 = $m3->h5;
            $lote->m3_hmvle = $m3->hmvle;
            $lote->m3_hmvlqm = $m3->hmvlqm;

            $s0 = $this->db->queryRow("SELECT 
                    ROUND(AVG(hojas_totales)) AS ht,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor)) AS hmvlqm,
                    ROUND(AVG(hojas_mas_vieja_libre)) AS hmvle
                FROM muestras_hacienda_detalle
                WHERE id_lote = '{$lote->id}' AND tipo_semana = 0 AND semana = '{$semana}' AND anio = {$this->session->year}
                    AND id_hacienda = '{$this->session->finca}' 
                    AND id_cliente = '{$this->session->client}' 
                    AND id_usuario IN ('{$this->session->logges->users}')");
            $lote->s0_ht = $s0->ht;
            $lote->s0_hmvlqm = $s0->hmvlqm;
            $lote->s0_hmvle = $s0->hmvle;

            $s11 = $this->db->queryRow("SELECT 
                    ROUND(AVG(hojas_totales)) AS ht,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor)) AS hmvlqm
                FROM muestras_hacienda_detalle
                WHERE id_lote = '{$lote->id}' AND tipo_semana = 11 AND semana = '{$semana}' AND anio = {$this->session->year}
                    AND id_hacienda = '{$this->session->finca}' 
                    AND id_cliente = '{$this->session->client}' 
                    AND id_usuario IN ('{$this->session->logges->users}')");
            $lote->s11_ht = $s11->ht;
            $lote->s11_hmvlqm = $s11->hmvlqm;
        }

        return $response;
    }

    public function getAplicaciones(){
        $response = new stdClass;
        $sql = "SELECT fecha, IF(ciclo = '(NULL)', '', ciclo) AS ciclo, IF(producto_1 IS NULL OR producto_1 = '(NULL)', '', producto_1) AS producto_1, IF(producto_2 = '(NULL)', '', producto_2) AS producto_2
                FROM ciclos_aplicacion
                WHERE getYear(fecha) = {$this->session->year}  AND id_finca = '{$this->session->finca}' AND ((producto_1 != '' AND producto_1 != '(NULL)') OR (producto_2 != '' AND producto_2 != '(NULL)'))
                ORDER BY fecha";
        $response->data = $this->db->queryAll($sql);
        return json_encode($response);
    }

    private function getSemanas($filters = []){
        $response = new stdClass;
        $sql = "SELECT id, label FROM(
            SELECT getWeek(fecha) AS id, getWeek(fecha) AS label 
            FROM muestras_haciendas
            WHERE YEAR(fecha) = {$this->session->year}
                AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
            GROUP BY getWeek(fecha)
            UNION ALL
            SELECT getWeek(fecha) AS id, getWeek(fecha) AS label 
            FROM muestras_haciendas_3M
            WHERE YEAR(fecha) = {$this->session->year}
                AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
            GROUP BY getWeek(fecha)
        ) AS tbl
        GROUP BY id
        ORDER BY id";
        $response = $this->db->queryAllSpecial($sql);
        return $response;
    }

    private function getLotes($filters = []){
        if($filters->semana >= 0){
            $sWhere .= "AND semana = '{$filters->semana}'";
            $sWhere3 .= "AND semana = '{$filters->semana}'";
        }
        $sql = "SELECT id, UPPER(lote) AS lote, agrupacion
                FROM(
                    SELECT cat_lotes.id, cat_lotes.`nombre` AS lote, agr.nombre AS agrupacion
                    FROM muestras_haciendas
                    INNER JOIN muestras_hacienda_detalle detalle ON id_Mhacienda = muestras_haciendas.`id`
                    INNER JOIN cat_lotes ON cat_lotes.id = id_lote
                    LEFT JOIN cat_agrupaciones agr ON id_agrupacion = agr.id AND agr.status = 1
                    WHERE anio = {$this->session->year} 
                        AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                        AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                        AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}') $sWhere
                    GROUP BY cat_lotes.id 
                    UNION ALL
                    SELECT cat_lotes.id, cat_lotes.`nombre` AS lote, agr.nombre AS agrupacion
                    FROM muestras_haciendas_3M
                    INNER JOIN muestras_hacienda_detalle_3M detalle ON id_Mhacienda = muestras_haciendas_3M.`id`
                    INNER JOIN cat_lotes ON cat_lotes.id = id_lote
                    LEFT JOIN cat_agrupaciones agr ON id_agrupacion = agr.id AND agr.status = 1
                    WHERE anio = {$this->session->year} 
                        AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                        AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                        AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}') $sWhere3
                    GROUP BY cat_lotes.id
                ) AS lotes
                GROUP BY id
                ORDER BY lote";
        return $this->db->queryAll($sql);
    }

    public function getClimaAnio(){
        $sql = "SELECT 
                    semana,
                    MIN(temp_minima) temp_minima,
                    MAX(temp_maxima) temp_maxima,
                    SUM(lluvia) lluvia,
                    MAX(humedad) humedad,
                    SUM(rad_solar) rad_solar
                FROM datos_clima
                WHERE YEAR(fecha) = {$this->session->year}
                    AND id_cliente = '{$this->session->client}' 
                    AND id_usuario IN ('{$this->session->logges->users}')
                GROUP BY semana
                ORDER BY semana";
        $response = $this->db->queryAll($sql);
        return json_encode($response);
    }

    //BEGIN 3 M
        private function getSemanas3M(){
            $sql = "SELECT semana
                        FROM muestras_hacienda_detalle_3M 
                        WHERE semana > 0 AND YEAR(fecha) = {$this->session->year}
                            AND id_hacienda = '{$this->session->finca}' 
                            AND id_cliente = '{$this->session->client}' 
                            AND id_usuario IN ('{$this->session->logges->users}')
                        GROUP BY semana
                        ORDER BY semana";
            return $this->db->queryAll($sql);
        }
        private function getFocos3M(){
            $sql = "SELECT UPPER(foco) AS id, UPPER(foco) AS label
                        FROM muestras_hacienda_detalle_3M 
                        WHERE semana >= 0 AND YEAR(fecha) = {$this->session->year}
                            AND id_hacienda = '{$this->session->finca}' 
                            AND id_cliente = '{$this->session->client}' 
                            AND id_usuario IN ('{$this->session->logges->users}')
                        GROUP BY foco
                        ORDER BY foco";
            return $this->db->queryAllSpecial($sql);
        }

        public function get3M(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $focos = $this->getFocos3M(); 

            $data_chart = [];
            $max = null;    
            $min = null;
            $umbral = $this->db->queryOne("SELECT m3_hvle FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0) {
                $umbral = 6.5;
            }
            $max = $min = $umbral;
            foreach($semanas as $sem){
                if($umbral) $data_chart[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($focos as $foco){
                    $sql = "SELECT 
                                AVG(hoja_mas_vieja_de_estrias) AS value
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0 AND anio = {$this->session->year}
                                AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco = '{$foco}'";
                    $data = $this->db->queryRow($sql);


                    $position_foco = array_search($foco, array_keys($focos)) + ($umbral ? 1: 0);
                    if(isset($data->value)){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }

                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line", ["max" => $response->max, "min" => $response->min]);

            // SET COLORS
            foreach($response->data_chart["series"] as $s){
                $serie = $s["name"];
                $color = "";
                if(strpos($serie, 'UMBRAL') !== false){
                    $color = '#D91E18';
                }else{
                    $color = $this->db->queryOne("SELECT rgb_color FROM clientes_agrupaciones_colores WHERE agrupacion = '{$serie}' AND id_cliente = {$this->session->client} AND year = {$this->session->year} AND id_hacienda = '{$this->session->finca}'");
                }
                $response->data_chart["color"][] = $color;
            }

            return json_encode($response);
        }

        public function getH4(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $focos = $this->getFocos3M();
            $max = null;
            $min = null;
            $umbral = $this->db->queryOne("SELECT m3_h4 FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0) {
                $umbral = 300;
            }
            $max = $min = $umbral;
            foreach($semanas as $sem){
                if($umbral) $data_chart_h4[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($focos as $foco){
                    $sql = "SELECT 
                                AVG(IF(total_hoja_4 = 100 OR total_hoja_4 = 1, 1, 0)) AS 'h4',
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0 AND anio = {$this->session->year}
                                AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco = '{$foco}'";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco, array_keys($focos)) + ($umbral ? 1: 0);
                    if(isset($data->h4)){
                        if(is_null($max) || $max < $data->h4 * 100) $max = $data->h4 * 100;
                        if(is_null($min) || $min > $data->h4 * 100) $min = $data->h4 * 100;
                        $data_chart_h4[] = ["value" => $data->h4 * 100, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_h4[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->h4 = $this->chartInit($data_chart_h4, "vertical" , "" , "line", ["max" => $response->max, "min" => $response->min]);

            // SET COLORS
            foreach($response->h4["series"] as $s){
                $serie = $s["name"];
                $color = "";
                if(strpos($serie, 'UMBRAL') !== false){
                    $color = '#D91E18';
                }else{
                    $color = $this->db->queryOne("SELECT rgb_color FROM clientes_agrupaciones_colores WHERE agrupacion = '{$serie}' AND id_cliente = {$this->session->client} AND year = {$this->session->year} AND id_hacienda = '{$this->session->finca}'");
                }
                $response->h4["color"][] = $color;
            }

            return json_encode($response);
        }

        public function getH3(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $focos = $this->getFocos3M();
            $max = null;
            $min = null;
            $umbral = $this->db->queryOne("SELECT m3_h3 FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0) {
                $umbral = 200;
            }
            $max = $min = $umbral;
            foreach($semanas as $sem){
                if($umbral) $data_chart_h3[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($focos as $foco){
                    $sql = "SELECT 
                                AVG(IF(total_hoja_3 = 100 OR total_hoja_3 = 1, 1, 0)) AS 'h3',
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0 AND anio = {$this->session->year}
                                AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco = '{$foco}'";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco, array_keys($focos)) + ($umbral ? 1: 0);
                    if(isset($data->h3)){
                        if(is_null($max) || $max < $data->h3 * 100) $max = $data->h3 * 100;
                        if(is_null($min) || $min > $data->h3 * 100) $min = $data->h3 * 100;
                        $data_chart_h3[] = ["value" => $data->h3 * 100, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_h3[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->h3 = $this->chartInit($data_chart_h3, "vertical" , "" , "line", ["max" => $response->max, "min" => $response->min]);

            // SET COLORS
            foreach($response->h3["series"] as $s){
                $serie = $s["name"];
                $color = "";
                if(strpos($serie, 'UMBRAL') !== false){
                    $color = '#D91E18';
                }else{
                    $color = $this->db->queryOne("SELECT rgb_color FROM clientes_agrupaciones_colores WHERE agrupacion = '{$serie}' AND id_cliente = {$this->session->client} AND year = {$this->session->year} AND id_hacienda = '{$this->session->finca}'");
                }
                $response->h3["color"][] = $color;
            }

            return json_encode($response);
        }

        public function getH5(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $focos = $this->getFocos3M();
            $max = null;
            $min = null;
            $umbral = $this->db->queryOne("SELECT m3_h5 FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0) {
                $umbral = 300;
            }
            $max = $min = $umbral;
            foreach($semanas as $sem){
                if($umbral) $data_chart_h5[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($focos as $foco){
                    $sql = "SELECT 
                                AVG(IF(total_hoja_5 = 100 OR total_hoja_5 = 1, 1, 0)) AS 'h5',
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0 AND anio = {$this->session->year}
                                AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco = '{$foco}'";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco, array_keys($focos)) + ($umbral ? 1: 0);
                    if(isset($data->h5)){
                        if(is_null($max) || $max < ($data->h5*100)) $max = $data->h5 * 100;
                        if(is_null($min) || $min > ($data->h5*100)) $min = $data->h5 * 100;
                        $data_chart_h5[] = ["value" => $data->h5 * 100, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_h5[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min <= 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->h5 = $this->chartInit($data_chart_h5, "vertical" , "" , "line", ["max" => $response->max, "min" => $response->min]);

            // SET COLORS
            foreach($response->h5["series"] as $s){
                $serie = $s["name"];
                $color = "";
                if(strpos($serie, 'UMBRAL') !== false){
                    $color = '#D91E18';
                }else{
                    $color = $this->db->queryOne("SELECT rgb_color FROM clientes_agrupaciones_colores WHERE agrupacion = '{$serie}' AND id_cliente = {$this->session->client} AND year = {$this->session->year} AND id_hacienda = '{$this->session->finca}'");
                }
                $response->h5["color"][] = $color;
            }

            return json_encode($response);
        }

        public function getHMVLQM(){
            $response = new stdClass;

            $semanas = $this->getSemanas3M();
            $focos = $this->getFocos3M();
            $max = null;
            $min = null;
            $data_chart_hmvlqm = [];
            $umbral = $this->db->queryOne("SELECT m3_hvlq FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral >= 0){
                $umbral = 11;
            }
            $max = $min = $umbral;
            foreach($semanas as $sem){
                if($umbral) $data_chart_hmvlqm[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($focos as $foco){
                    $sql = "SELECT 
                                AVG(hoja_mas_vieja_libre_quema_menor) AS hmvlqm,
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0 AND anio = {$this->session->year}
                                AND muestras_haciendas_3M.id_hacienda = '{$this->session->finca}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND foco = '{$foco}'";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco, array_keys($focos)) + ($umbral ? 1: 0);
                    if(isset($data->hmvlqm) && $data->hmvlqm > 0){
                        if(is_null($max) || $max < $data->hmvlqm) $max = $data->hmvlqm;
                        if(is_null($min) || $min > $data->hmvlqm) $min = $data->hmvlqm;
                        $data_chart_hmvlqm[] = ["value" => $data->hmvlqm, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->hmvlqm = $this->chartInit($data_chart_hmvlqm, "vertical" , "" , "line", ["max" => $response->max, "min" => $response->min]);

            // SET COLORS
            foreach($response->hmvlqm["series"] as $s){
                $serie = $s["name"];
                $color = "";
                if(strpos($serie, 'UMBRAL') !== false){
                    $color = '#D91E18';
                }else{
                    $color = $this->db->queryOne("SELECT rgb_color FROM clientes_agrupaciones_colores WHERE agrupacion = '{$serie}' AND id_cliente = {$this->session->client} AND year = {$this->session->year} AND id_hacienda = '{$this->session->finca}'");
                }
                $response->hmvlqm["color"][] = $color;
            }

            return json_encode($response);
        }
    //END 3 M

    //BEGIN 0S
        private function getSemanas0S(){
            $sql = "SELECT semana
                        FROM muestras_hacienda_detalle 
                        WHERE semana >= 0 AND YEAR(fecha) = {$this->session->year}
                            AND id_hacienda = '{$this->session->finca}' 
                            AND id_cliente = '{$this->session->client}' 
                            AND id_usuario IN ('{$this->session->logges->users}')
                            AND tipo_semana = 0
                        GROUP BY semana
                        ORDER BY semana";
            return $this->db->queryAll($sql);
        }
        private function getFocos0S(){
            $sql = "SELECT UPPER(foco) AS id, UPPER(foco) AS label
                        FROM muestras_hacienda_detalle 
                        WHERE semana >= 0 AND YEAR(fecha) = {$this->session->year}
                            AND id_hacienda = '{$this->session->finca}' 
                            AND id_cliente = '{$this->session->client}' 
                            AND id_usuario IN ('{$this->session->logges->users}')
                            AND tipo_semana = 0
                        GROUP BY foco
                        ORDER BY foco";
            return $this->db->queryAllSpecial($sql);
        }

        public function getHT0S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $focos = $this->getFocos0S();

            $data_chart_ht0s = [];
            $min = null;
            $max = null;
            $umbral = $this->db->queryOne("SELECT s0_ht FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0){
                $umbral = 13;
                $max = $min = $umbral;
            }else{
                $max = $min = $umbral;
            }
            foreach($semanas as $sem){
                if($umbral) $data_chart_ht0s[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($focos as $foco){
                    $sql = "SELECT 
                            AVG(hojas_totales) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana >= 0 AND anio = {$this->session->year}
                            AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND foco = '{$foco}'
                            AND muestras_haciendas.tipo_semana = 0";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco,array_keys($focos)) + ($umbral ? 1: 0);
                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart_ht0s[] = ["value" => $data->value, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_ht0s[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->ht = $this->chartInit($data_chart_ht0s,"vertical","","line", ["min" => $response->min, "max" => $response->max]);

            // SET COLORS
            foreach($response->ht["series"] as $s){
                $serie = $s["name"];
                $color = "";
                if(strpos($serie, 'UMBRAL') !== false){
                    $color = '#D91E18';
                }else{
                    $color = $this->db->queryOne("SELECT rgb_color FROM clientes_agrupaciones_colores WHERE agrupacion = '{$serie}' AND id_cliente = {$this->session->client} AND year = {$this->session->year} AND id_hacienda = '{$this->session->finca}'");
                }
                $response->ht["color"][] = $color;
            }

            return json_encode($response);
        }

        public function getHMVLE0S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $focos = $this->getFocos0S();
            $max = null;
            $min = null;
            $data_chart_hmvlqm0s = [];
            $umbral = $this->db->queryOne("SELECT s0_hvle FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0) {
                $umbral = 0;
            }else{
                $max = $min = $umbral;
            }
            foreach($semanas as $sem){
                if($umbral) $data_chart_hmvlqm0s[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($focos as $foco){
                    $sql = "SELECT 
                            AVG(hojas_mas_vieja_libre) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana >= 0 AND anio = {$this->session->year}
                            AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND foco = '{$foco}'
                            AND muestras_haciendas.tipo_semana = 0";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco,array_keys($focos)) + ($umbral ? 1: 0);
                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm0s[] = ["value" => $data->value, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm0s[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->hmvle = $this->chartInit($data_chart_hmvlqm0s,"vertical","","line", ["max" => $response->max, "min" => $response->min]);

            // SET COLORS
            foreach($response->hmvle["series"] as $s){
                $serie = $s["name"];
                $color = "";
                if(strpos($serie, 'UMBRAL') !== false){
                    $color = '#D91E18';
                }else{
                    $color = $this->db->queryOne("SELECT rgb_color FROM clientes_agrupaciones_colores WHERE agrupacion = '{$serie}' AND id_cliente = {$this->session->client} AND year = {$this->session->year} AND id_hacienda = '{$this->session->finca}'");
                }
                $response->hmvle["color"][] = $color;
            }

            return json_encode($response);
        
        }

        public function getHMVLQM0S(){
            $response = new stdClass();

            $semanas = $this->getSemanas0S();
            $focos = $this->getFocos0S();

            $data_chart_hmvlqm0s = [];
            $umbral = $this->db->queryOne("SELECT s0_hvlq FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0) $umbral = 0;
            else $max = $min = $umbral;
            foreach($semanas as $sem){
                if($umbral) $data_chart_hmvlqm0s[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($focos as $foco){
                    $sql = "SELECT 
                            AVG(hoja_mas_vieja_libre_quema_menor) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana >= 0 AND anio = {$this->session->year}
                            AND muestras_haciendas.id_hacienda = '{$this->session->finca}' 
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND foco = '{$foco}'
                            AND muestras_haciendas.tipo_semana = 0";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco,array_keys($focos)) + ($umbral ? 1: 0);
                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm0s[] = ["value" => $data->value, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm0s[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->hmvlqm = $this->chartInit($data_chart_hmvlqm0s,"vertical","","line", ["max" => $response->max, "min" => $response->min]);

            // SET COLORS
            foreach($response->hmvlqm["series"] as $s){
                $serie = $s["name"];
                $color = "";
                if(strpos($serie, 'UMBRAL') !== false){
                    $color = '#D91E18';
                }else{
                    $color = $this->db->queryOne("SELECT rgb_color FROM clientes_agrupaciones_colores WHERE agrupacion = '{$serie}' AND id_cliente = {$this->session->client} AND year = {$this->session->year} AND id_hacienda = '{$this->session->finca}'");
                }
                $response->hmvlqm["color"][] = $color;
            }
            
            return json_encode($response); 
        }
    //END 0S

    //BEGIN 11S
        private function getSemanas11S(){ 
            $sql = "SELECT semana AS semana
                        FROM muestras_hacienda_detalle
                        WHERE semana >= 0 AND YEAR(fecha) = {$this->session->year}
                            AND id_hacienda = '{$this->session->finca}' 
                            AND id_cliente = '{$this->session->client}' 
                            AND id_usuario IN ('{$this->session->logges->users}')
                            AND tipo_semana = 11
                        GROUP BY semana
                        ORDER BY semana";
            return $this->db->queryAll($sql);  
        }  
        private function getFocos11S(){
            $sql = "SELECT UPPER(foco) AS id, UPPER(foco) AS label
                        FROM muestras_hacienda_detalle 
                        WHERE getWeek(fecha) >= 0 AND YEAR(fecha) = {$this->session->year}
                            AND id_hacienda = '{$this->session->finca}' 
                            AND id_cliente = '{$this->session->client}' 
                            AND id_usuario IN ('{$this->session->logges->users}')
                            AND tipo_semana = 11
                        GROUP BY foco
                        ORDER BY foco";
            return $this->db->queryAllSpecial($sql);          
        }

        public function getHT11S(){
            $response = new stdClass();

            $semanas = $this->getSemanas11S();
            $focos = $this->getFocos11S();
            $max = null;
            $min = null;
            $data_chart_ht11s = [];
            $umbral = $this->db->queryOne("SELECT s11_ht FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0){
                $umbral = 6.5;
            }
            $max = $min = $umbral;
            foreach($semanas as $sem){
                if($umbral) $data_chart_ht11s[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($focos as $foco){
                    $sql = "SELECT 
                            AVG(hojas_totales) AS value
                        FROM muestras_hacienda_detalle
                        WHERE semana >= 0 
                            AND anio = {$this->session->year}
                            AND id_hacienda = '{$this->session->finca}' 
                            AND id_cliente = '{$this->session->client}' 
                            AND id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND foco = '{$foco}'
                            AND tipo_semana = 11";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco,array_keys($focos)) + ($umbral ? 1: 0);
                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart_ht11s[] = ["value" => $data->value, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_ht11s[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->ht = $this->chartInit($data_chart_ht11s,"vertical","","line", ["max" => $response->max, "min" => $response->min]);

            // SET COLORS
            foreach($response->ht["series"] as $s){
                $serie = $s["name"];
                $color = "";
                if(strpos($serie, 'UMBRAL') !== false){
                    $color = '#D91E18';
                }else{
                    $color = $this->db->queryOne("SELECT rgb_color FROM clientes_agrupaciones_colores WHERE agrupacion = '{$serie}' AND id_cliente = {$this->session->client} AND year = {$this->session->year} AND id_hacienda = '{$this->session->finca}'");
                }
                $response->ht["color"][] = $color;
            }

            return json_encode($response);
        }

        public function getHMVLQM11S(){
            $response = new stdClass();

            $semanas = $this->getSemanas11S();
            $focos = $this->getFocos11S();
            $max = null;
            $min = null;
            $data_chart_hmvlqm11s = [];
            $umbral = $this->db->queryOne("SELECT s11_hvlq FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0) $umbral = 0;
            else $max = $min = $umbral;
            foreach($semanas as $sem){
                if($umbral) $data_chart_hmvlqm11s[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($focos as $foco){
                    $sql = "SELECT 
                            AVG(hoja_mas_vieja_libre_quema_menor) AS value
                        FROM muestras_hacienda_detalle
                        WHERE semana >= 0 
                            AND anio = {$this->session->year}
                            AND id_hacienda = '{$this->session->finca}' 
                            AND id_cliente = '{$this->session->client}' 
                            AND id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND foco = '{$foco}'
                            AND tipo_semana = 11";
                    $data = $this->db->queryRow($sql);

                    $position_foco = array_search($foco,array_keys($focos)) + ($umbral ? 1 : 0);
                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm11s[] = ["value" => $data->value, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }else{
                        $data_chart_hmvlqm11s[] = ["value" => NULL, "legend" => $foco, "selected" => 1, "label" => $sem->semana, "position" => $position_foco];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->hmvlqm = $this->chartInit($data_chart_hmvlqm11s,"vertical","","line", ["max" => $response->max, "min" => $response->min]);

            // SET COLORS
            foreach($response->hmvlqm["series"] as $s){
                $serie = $s["name"];
                $color = "";
                if(strpos($serie, 'UMBRAL') !== false){
                    $color = '#D91E18';
                }else{
                    $color = $this->db->queryOne("SELECT rgb_color FROM clientes_agrupaciones_colores WHERE agrupacion = '{$serie}' AND id_cliente = {$this->session->client} AND year = {$this->session->year} AND id_hacienda = '{$this->session->finca}'");
                }
                $response->hmvlqm["color"][] = $color;
            }

            return json_encode($response);
        }
    //END 11S

    //BEGIN CLIMA
        private function getSemanasClima(){ 
            $sql = "SELECT semana
                        FROM datos_clima 
                        WHERE semana >= 0 
                            AND YEAR(fecha) = {$this->session->year}
                            AND id_cliente = '{$this->session->client}' 
                            AND id_usuario IN ('{$this->session->logges->users}')
                        GROUP BY semana
                        ORDER BY semana";
            return $this->db->queryAll($sql);
        }

        private function getEstacionesClima(){
            $sql = "SELECT estacion_id AS id, (SELECT GROUP_CONCAT(h.alias SEPARATOR ' | ') FROM estaciones_haciendas INNER JOIN cat_haciendas h ON id_hacienda = h.id WHERE estacion_id = datos_clima.estacion_id) AS label
                    FROM datos_clima 
                    WHERE YEAR(fecha) = {$this->session->year}
                        AND id_cliente = '{$this->session->client}' 
                        AND id_usuario IN ('{$this->session->logges->users}')
                    GROUP BY estacion_id
                    ORDER BY estacion_id";
            return $this->db->queryAllSpecial($sql);
        }

        private function getHorasClima($fecha){
            $sql = "SELECT HOUR(hora) AS id, DATE_FORMAT(CONCAT(CURRENT_DATE,' ', hora), '%h %p') AS label
                    FROM datos_clima 
                    WHERE YEAR(fecha) = {$this->session->year}
                        AND id_cliente = '{$this->session->client}' 
                        AND id_usuario IN ('{$this->session->logges->users}')
                        AND fecha = '{$fecha}'
                    GROUP BY HOUR(hora)
                    ORDER BY HOUR(hora)";
            return $this->db->queryAllSpecial($sql);
        }

        public function getTempMin(){
            $response = new stdClass();
            
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];
            $max = $min = 21;
            foreach($semanas as $sem){
                $data_chart[] = ["value" => 21, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT MIN(temp_minima) AS value
                            FROM datos_clima_resumen
                            WHERE anio = {$this->session->year}
                                AND semana = {$sem->semana}
                                AND estacion_id = '{$estacion}'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones)) + 1;
                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = floor(round($min - (($max - $min) * .1), 2));
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = ceil(round($max + (($max - $min) * .1), 2));
            $response->data = $this->chartInit($data_chart,"vertical","","line", ["min" => $response->min, "max" => $response->max]);
            return json_encode($response);
        }

        public function getTempMax(){
            $response = new stdClass();
            
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];
            $max = null;
            $min = null;
            foreach($semanas as $sem){

                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT MAX(temp_maxima) AS value
                            FROM datos_clima_resumen
                            WHERE anio = {$this->session->year}
                                AND temp_maxima > 0
                                AND semana = {$sem->semana}
                                AND estacion_id = '{$estacion}'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = floor(round($min - (($max - $min) * .1), 2));
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = ceil(round($max + (($max - $min) * .1), 2));
            $response->data = $this->chartInit($data_chart,"vertical","","line", ["min" => $response->min, "max" => $response->max]);
            return json_encode($response);
        }

        public function getHumedadMax(){
            $response = new stdClass();
            
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];
            $max = null;
            $min = null;
            foreach($semanas as $sem){

                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT MAX(humedad) AS value
                            FROM datos_clima_resumen
                            WHERE anio = {$this->session->year}
                                AND humedad > 0
                                AND semana = {$sem->semana}
                                AND estacion_id = '{$estacion}'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = floor(round($min - (($max - $min) * .1), 2));
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = ceil(round($max + (($max - $min) * .1), 2));
            $response->data = $this->chartInit($data_chart,"vertical","","line", ["min" => $response->min, "max" => $response->max]);
            return json_encode($response);
        }

        public function getLluvia(){
            $response = new stdClass();
            
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];
            $max = null;
            $min = null;
            foreach($semanas as $sem){
                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT SUM(IFNULL(horas_lluvia, 0)) AS value
                            FROM datos_clima_resumen
                            WHERE anio = {$this->session->year}
                                AND semana = {$sem->semana}
                                AND estacion_id = '{$estacion}'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value)){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = floor(round($min - (($max - $min) * .1), 2));
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = ceil(round($max + (($max - $min) * .1), 2));
            $response->data = $this->chartInit($data_chart,"vertical","","line",["min" => $response->min, "max" => $response->max], true);
            return json_encode($response);
        }

        public function getRadSolar(){
            $response = new stdClass();
            
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];
            $max = null;
            $min = null;
            foreach($semanas as $sem){
                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT value
                            FROM(
                                SELECT SUM(rad_solar) AS value, estacion_id
                                FROM datos_clima_resumen
                                WHERE anio = {$this->session->year}
                                    AND semana = {$sem->semana}
                                    AND estacion_id = '{$estacion}'
                                GROUP BY id_hacienda
                            ) AS tbl
                            GROUP BY estacion_id";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = floor(round($min - (($max - $min) * .1), 2));
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = ceil(round($max + (($max - $min) * .1), 2));
            $response->data = $this->chartInit($data_chart,"vertical","","line", ["min" => $response->min, 'max' => $response->max]);
            return json_encode($response);
        }

        public function getHorasLuz(){
            $response = new stdClass();
            
            $filters = $this->params();
            $semanas = $this->getSemanasClima();
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];
            $max = null;
            $min = null;
            foreach($semanas as $sem){
                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT AVG(value) AS value
                            FROM (
                                SELECT SUM(horas_luz_{$filters->luz}) AS value, estacion_id
                                FROM datos_clima_resumen
                                WHERE anio = {$this->session->year}
                                    AND semana = {$sem->semana}
                                    AND estacion_id = '{$estacion}'
                                GROUP BY id_hacienda
                            ) AS tbl
                            GROUP BY estacion_id";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = floor(round($min - (($max - $min) * .1), 2));
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = ceil(round($max + (($max - $min) * .1), 2));
            $response->data = $this->chartInit($data_chart,"vertical","","line", ["min" => $response->min, "max" => $response->max]);
            return json_encode($response);
        }

        public function getRadSolarHoras(){
            $response = new stdClass();
            
            $filters = $this->params();
            if($filters->fecha_inicial == "" && $filters->fecha_final == ""){
                $filters->fecha_inicial = $this->db->queryRow("SELECT MAX(fecha) AS fecha FROM datos_clima WHERE YEAR(fecha) = {$this->session->year} AND id_cliente = '{$this->session->client}' AND id_usuario IN ('{$this->session->logges->users}') AND rad_solar > 0")->fecha;
                $filters->fecha_final = $filters->fecha_inicial;

                $response->fecha = $filters->fecha_inicial;
            }

            $horas = $this->getHorasClima($filters->fecha_inicial);
            $estaciones = $this->getEstacionesClima();

            $data_chart = [];

            foreach($horas as $hora => $label){

                foreach($estaciones as $estacion => $fincas){
                    $sql = "SELECT AVG(rad_solar) AS value
                            FROM datos_clima
                            WHERE YEAR(fecha) = {$this->session->year}
                                AND id_cliente = '{$this->session->client}' 
                                AND id_usuario IN ({$this->session->logges->users})
                                AND HOUR(hora) = {$hora}
                                AND estacion_id = '{$estacion}'
                                AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($estacion, array_keys($estaciones));
                    if(isset($data->value) && $data->value > 0){
                        $data_chart[] = ["value" => $data->value, "legend" => $fincas, "selected" => 1, "label" => $label, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $fincas, "selected" => 1, "label" => $label, "position" => $position];
                    }
                }
            }

            $response->data = $this->chartInit($data_chart,"vertical","","line");
            return json_encode($response);
        }
    //END CLIMA

    public function saveFoliar(){
        $response = new stdClass;
        $data = (object)json_decode(file_get_contents("php://input"));
        $response->status = 400;
        
        if($data->emision > 0 && $data->id_foliar > 0){
            $response->status = 200;
            if(!$data->valor > 0) $val = "NULL";
            else $val = $data->valor;
            $this->db->query("UPDATE foliar SET emision{$data->emision} = $val WHERE id = $data->id_foliar");
        }

        return json_encode($response);
    }

    public function getEmisionFoliar(){
        $sql_get_weeks = "SELECT getWeek(fecha) AS semana, YEAR(fecha) AS anio, foco FROM foliar 
			WHERE id_hacienda = '{$this->session->finca}' 
                AND id_usuario IN ({$this->session->logges->users}) 
                AND id_cliente = '{$this->session->client}' 
                AND (YEAR(fecha) = {$this->session->year} OR (YEAR(fecha) = {$this->session->year}-1 AND getWeek(fecha) = 52))
			GROUP BY foco, getWeek(fecha) 
			ORDER BY foco, YEAR(fecha), getWeek(fecha)";
        $weeks = $this->db->queryAll($sql_get_weeks, true);
		$count_foco=0;
		$foco = ['foco' => ''];
		$tabla = array();
		foreach ($weeks as $key => $value) {
			$semana = $weeks[$key]["semana"];
            $anio = $weeks[$key]["anio"];
            
			if($foco['foco'] != $weeks[$key]["foco"]){
				$foco = ['foco' => $weeks[$key]["foco"]];
				$count_foco=0;
			}

			if($count_foco > 0){
                /*if($semana == 1){
                    $pre_semana = 52;
                    $pre_anio = $anio-1;
                }else{
                    $pre_semana = $semana-1;
                    $pre_anio = $anio;
                }*/
                $pre_semana = $weeks[$key-1]["semana"];
                $pre_anio = $weeks[$key-1]["anio"];
				$sql = "SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
					FROM(
                        SELECT 
                            IF(actual.emision1 > pasada.emision1,(actual.emision1-pasada.emision1)/dif_dias*7,0) AS emision1,
                            IF(actual.emision2 > pasada.emision2,(actual.emision2-pasada.emision2)/dif_dias*7,0) AS emision2,
                            IF(actual.emision3 > pasada.emision3,(actual.emision3-pasada.emision3)/dif_dias*7,0) AS emision3,
                            IF(actual.emision4 > pasada.emision4,(actual.emision4-pasada.emision4)/dif_dias*7,0) AS emision4,
                            IF(actual.emision5 > pasada.emision5,(actual.emision5-pasada.emision5)/dif_dias*7,0) AS emision5,
                            IF(actual.emision6 > pasada.emision6,(actual.emision6-pasada.emision6)/dif_dias*7,0) AS emision6,
                            IF(actual.emision7 > pasada.emision7,(actual.emision7-pasada.emision7)/dif_dias*7,0) AS emision7,
                            IF(actual.emision8 > pasada.emision8,(actual.emision8-pasada.emision8)/dif_dias*7,0) AS emision8,
                            IF(actual.emision9 > pasada.emision9,(actual.emision9-pasada.emision9)/dif_dias*7,0) AS emision9,
                            IF(actual.emision10 > pasada.emision10,(actual.emision10-pasada.emision10)/dif_dias*7,0) AS emision10,
                            actual.foco
                        FROM (
                            SELECT * 
                            FROM foliar 
                            WHERE getWeek(fecha) = $semana 
                                AND YEAR(fecha) = $anio 
                                AND id_hacienda = '{$this->session->finca}' 
                                AND id_usuario IN ({$this->session->logges->users}) 
                                AND id_cliente = '{$this->session->client}' 
                                AND foco = '".$foco["foco"]."'
                        ) AS actual
                        JOIN (
                            SELECT * 
                            FROM foliar 
                            WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." 
                                AND YEAR(fecha) = $pre_anio 
                                AND id_hacienda = '{$this->session->finca}' 
                                AND id_usuario IN ({$this->session->logges->users}) 
                                AND id_cliente = '{$this->session->client}' 
                                AND foco = '".$foco["foco"]."'
                        ) AS pasada
                        JOIN (
                            SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
                            FROM(
                                SELECT *
                                FROM (
                                    SELECT MIN(fecha) AS dia_actual 
                                    FROM foliar 
                                    WHERE getWeek(fecha) = $semana 
                                        AND YEAR(fecha) = $anio 
                                        AND id_hacienda = '{$this->session->finca}' 
                                        AND id_usuario IN ({$this->session->logges->users}) 
                                        AND id_cliente = '{$this->session->client}' 
                                        AND foco = '".$foco["foco"]."' 
                                        GROUP BY id_hacienda
                                ) AS actual
                                JOIN (
                                    SELECT MIN(fecha) AS dia_pasado 
                                    FROM foliar 
                                    WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." 
                                        AND YEAR(fecha) = $pre_anio 
                                        AND id_hacienda = '{$this->session->finca}' 
                                        AND id_usuario IN ({$this->session->logges->users}) 
                                        AND id_cliente = '{$this->session->client}' 
                                        AND foco = '".$foco["foco"]."' 
                                        GROUP BY id_hacienda
                                ) AS pasada
                            ) AS tbl_dif
                        ) tbl_dif_d
                    ) AS tabla
					JOIN (
                        SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
						FROM(
                            SELECT IF(actual.emision1 > pasada.emision1,1,0) AS emision1,
                                IF(actual.emision2 > pasada.emision2,1,0) AS emision2,
                                IF(actual.emision3 > pasada.emision3,1,0) AS emision3,
                                IF(actual.emision4 > pasada.emision4,1,0) AS emision4,
                                IF(actual.emision5 > pasada.emision5,1,0) AS emision5,
                                IF(actual.emision6 > pasada.emision6,1,0) AS emision6,
                                IF(actual.emision7 > pasada.emision7,1,0) AS emision7,
                                IF(actual.emision8 > pasada.emision8,1,0) AS emision8,
                                IF(actual.emision9 > pasada.emision9,1,0) AS emision9,
                                IF(actual.emision10 > pasada.emision10,1,0) AS emision10
							FROM(
                                SELECT * 
                                FROM foliar 
                                WHERE getWeek(fecha) = $semana 
                                    AND YEAR(fecha) = $anio 
                                    AND id_hacienda = '{$this->session->finca}' 
                                    AND id_usuario IN ({$this->session->logges->users}) 
                                    AND id_cliente = '{$this->session->client}' 
                                    AND foco = '".$foco["foco"]."'
                            ) AS actual
							JOIN (
                                SELECT * 
                                FROM foliar 
                                WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." 
                                    AND YEAR(fecha) = $pre_anio 
                                    AND id_hacienda = '{$this->session->finca}' 
                                    AND id_usuario IN ({$this->session->logges->users}) 
                                    AND id_cliente = '{$this->session->client}' 
                                    AND foco = '".$foco["foco"]."'
                            ) AS pasada
                        )AS tbl_suma
                    )AS tbl_division
                    ";
                    
                $result = $this->db->queryAll($sql, true);
                if($result[0]["foco"]!=""){
                    if($this->session->finca == 16 && $this->session->client == 10){
                        if((int)$semana == 24){
                            $result[0]["valor"] = "0.72";
                        }
                        if((int)$semana == 28){
                            $result[0]["valor"] = "0.65";
                        }
                    }
                    $data[$result[0]["foco"]][] = array($semana,round(((double)$result[0]["valor"]),2));
                    $tabla[$result[0]["foco"]][$semana] = (double)round(((double)$result[0]["valor"]),2);
                }
            }
			$count_foco++;
		}
		$response = (object)[];
		$response->table = (object)$tabla;
        
        $data_chart = [];
        $count = 0;
        foreach($data as $key => $value){
            foreach($value as $val){
                if($val[1] > 0){
                    $data_chart[] = ["value" => $val[1], "legend" => $key, "selected" => 1, "label" => $val[0], "position" => $count];
                }else{
                    $data_chart[] = ["value" => NULL, "legend" => $key, "selected" => 1, "label" => $val[0], "position" => $count];
                }
            }
            $count++;
        }

        $response->data_desordenada = $this->chartInit($data_chart, "vertical", "", "line");
        $response->data = $response->data_desordenada;
        sort($response->data['xAxis']['data']);
        /* BEGIN ORDENAR POR FECHA */
        $semanas_order_original = $response->data_desordenada['xAxis']['data'];

        foreach($semanas_order_original as $i_sem_original => $sem_original){
            $i_sem_ordenada = array_search($sem_original, $response->data['xAxis']['data']);

            foreach($response->data_desordenada['series'] as $i_serie => $serie){
                $response->data['series'][$i_serie]['data'][$i_sem_ordenada] = $serie['data'][$i_sem_original];
            }
        }
        /* END ORDENAR POR FECHA */

        $response->table_emision = $this->db->queryAll("SELECT *, getWeek(fecha) AS semana
            FROM foliar
            WHERE id_hacienda = '{$this->session->finca}' 
                AND id_usuario IN ({$this->session->logges->users}) 
                AND id_cliente = '{$this->session->client}' 
                AND (YEAR(fecha) = {$this->session->year} OR (YEAR(fecha) = {$this->session->year}-1 AND getWeek(fecha) = 52))
            GROUP BY foco, getWeek(fecha) 
            ORDER BY foco, YEAR(fecha), getWeek(fecha)");
		return json_encode($response);
    }

    
    //BEGIN COMPARACION 3
        private function getFincas3MComp(){
            $response = [];
            $data = $this->db->queryAll("SELECT YEAR(m.fecha) AS anio, id_hacienda, CONCAT(YEAR(m.fecha), ' ', h.`nombre`) AS label
                FROM muestras_haciendas_3M m
                INNER JOIN cat_haciendas h ON m.id_hacienda = h.`id`
                WHERE m.id_cliente = {$this->session->client} AND m.fecha IS NOT NULL
                GROUP BY id_hacienda, YEAR(m.fecha)");
            foreach($data as $row){
                $response[$row->label] = $row;
            }
            return $response;
        }
        private function getSemanas3MComp(){
            return $this->db->queryAll("SELECT getWeek(fecha) AS semana
                FROM muestras_haciendas_3M m
                WHERE m.id_cliente = {$this->session->client} AND m.fecha IS NOT NULL
                GROUP BY getWeek(fecha)");
        }

        public function get3MComparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            $data_chart = [];
            $umbral = $this->db->queryOne("SELECT m3_hvle FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0) {
                $umbral = 6.5;
            }
            foreach($semanas as $sem){
                if($umbral) $data_chart[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(hoja_mas_vieja_de_estrias) AS value
                            FROM muestras_hacienda_detalle_3M 
                            WHERE semana >= 0
                                AND id_hacienda = '{$finca->id_hacienda}' 
                                AND id_cliente = '{$this->session->client}' 
                                AND id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$finca->anio}
                                #AND foco = 'AREA NORMAL'";
                    $data = $this->db->queryRow($sql);


                    $position = array_search($finca->label, array_keys($fincas)) + ($umbral ? 1: 0);
                    if(isset($data->value)){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }

                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->data_chart = $this->chartInit($data_chart, "vertical" , "" , "line", ["max" => $response->max, "min" => $response->min]);
            return json_encode($response);
        }
        public function getH4Comparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            $data_chart_h4 = [];
            $umbral = $this->db->queryOne("SELECT m3_h4 FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0) {
                $umbral = 300;
            }
            foreach($semanas as $sem){
                if($umbral) $data_chart_h4[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(IF(total_hoja_4 = 100 OR total_hoja_4 = 1, 1, 0)) * 100 AS 'h4',
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0
                                AND muestras_haciendas_3M.id_hacienda = '{$finca->id_hacienda}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$finca->anio}
                                #AND foco = 'AREA NORMAL'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + ($umbral ? 1 : 0);
                    if(isset($data->h4) && $data->h4){
                        if(is_null($max) || $max < $data->h4) $max = $data->h4;
                        if(is_null($min) || $min > $data->h4) $min = $data->h4;
                        $data_chart_h4[] = ["value" => $data->h4, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_h4[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->h4 = $this->chartInit($data_chart_h4, "vertical" , "" , "line", ["max" => $response->max, "min" => $response->min]);
            return json_encode($response);
        }
        public function getH3Comparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            $data_chart_hmvle = [];
            $data_chart_hmvlqm = [];
            $umbral = $this->db->queryOne("SELECT m3_h3 FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0) {
                $umbral = 200;
            }
            foreach($semanas as $sem){
                if($umbral) $data_chart_h3[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(IF(total_hoja_3 = 100 OR total_hoja_3 = 1, 1, 0)) * 100 AS 'h3',
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0
                                AND muestras_haciendas_3M.id_hacienda = '{$finca->id_hacienda}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$finca->anio}
                                #AND foco = 'AREA NORMAL'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + ($umbral ? 1: 0);
                    if(isset($data->h3)){
                        if(is_null($max) || $max < $data->h3) $max = $data->h3;
                        if(is_null($min) || $min > $data->h3) $min = $data->h3;
                        $data_chart_h3[] = ["value" => $data->h3, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_h3[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->h3 = $this->chartInit($data_chart_h3, "vertical" , "" , "line", ["max" => $response->max, "min" => $response->min]);
            return json_encode($response);
        }
        public function getH5Comparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            $umbral = $this->db->queryOne("SELECT m3_h5 FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0) {
                $umbral = 300;
            }
            foreach($semanas as $sem){
                if($umbral) $data_chart_h5[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(IF(total_hoja_5 = 100 OR total_hoja_5 = 1, 1, 0)) * 100 AS 'h5',
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0
                                AND muestras_haciendas_3M.id_hacienda = '{$finca->id_hacienda}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$finca->anio}
                                #AND foco = 'AREA NORMAL'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + ($umbral ? 1 : 0);
                    if(isset($data->h5)){
                        if(is_null($max) || $max < $data->h5) $max = $data->h5;
                        if(is_null($min) || $min > $data->h5) $min = $data->h5;
                        $data_chart_h5[] = ["value" => $data->h5, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_h5[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->h5 = $this->chartInit($data_chart_h5, "vertical" , "" , "line", ["max" => $response->max, "min" => $response->min]);
            return json_encode($response);
        }
        public function getHMVLQMComparativo(){
            $response = new stdClass;

            $fincas = $this->getFincas3MComp();
            $semanas = $this->getSemanas3MComp();

            $data_chart_hmvle = [];
            $data_chart_hmvlqm = [];
            $umbral = $this->db->queryOne("SELECT m3_hvlq FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral >= 0){
                $umbral = 11;
            }
            foreach($semanas as $sem){
                if($umbral) $data_chart_hmvlqm[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                                AVG(hoja_mas_vieja_de_estrias) AS hmvle,
                                AVG(hoja_mas_vieja_libre_quema_menor) AS hmvlqm,
                                semana AS label,
                                1 AS selected,
                                foco AS legend
                            FROM muestras_haciendas_3M 
                            INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                            WHERE semana >= 0
                                AND muestras_haciendas_3M.id_hacienda = '{$finca->id_hacienda}' 
                                AND muestras_haciendas_3M.id_cliente = '{$this->session->client}' 
                                AND muestras_haciendas_3M.id_usuario IN ('{$this->session->logges->users}')
                                AND semana = {$sem->semana}
                                AND anio = {$finca->anio}
                                #AND foco = 'AREA NORMAL'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + ($umbral ? 1: 0);
                    if(isset($data->hmvlqm) && $data->hmvlqm > 0){
                        if(is_null($max) || $max < $data->hmvlqm) $max = $data->hmvlqm;
                        if(is_null($min) || $min > $data->hmvlqm) $min = $data->hmvlqm;
                        $data_chart_hmvlqm[] = ["value" => $data->hmvlqm, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_hmvlqm[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->hmvlqm = $this->chartInit($data_chart_hmvlqm, "vertical" , "" , "line", ["max" => $response->max, "min" => $response->min]);
            return json_encode($response);
        }
    //END COMPARACION 3 M

    //BEGIN COMPARACION 0 SEM
        private function getFincas0SComp(){
            $response = [];
            $data = $this->db->queryAll("SELECT YEAR(m.fecha) AS anio, id_hacienda, CONCAT(YEAR(m.fecha), ' ', h.`nombre`) AS label
                FROM muestras_haciendas m
                INNER JOIN cat_haciendas h ON m.id_hacienda = h.`id`
                WHERE m.id_cliente = {$this->session->client} AND tipo_semana = 0 AND m.fecha IS NOT NULL
                GROUP BY id_hacienda, YEAR(m.fecha)");
            foreach($data as $row){
                $response[$row->label] = $row;
            }
            return $response;
        }
        private function getSemanas0SComp(){
            return $this->db->queryAll("SELECT getWeek(fecha) AS semana
                FROM muestras_haciendas m
                WHERE m.id_cliente = {$this->session->client} AND fecha IS NOT NULL AND tipo_semana = 0
                GROUP BY getWeek(fecha)");
        }

        public function getHT0SComparativo(){
            $response = new stdClass();

            $fincas = $this->getFincas0SComp();
            $semanas = $this->getSemanas0SComp();

            $data_chart_ht0s = [];
            $umbral = $this->db->queryOne("SELECT m0_ht FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0){
                $umbral = 13;
                $max = $min = $umbral;
            }
            foreach($semanas as $sem){
                if($umbral) $data_chart_ht0s[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hojas_totales) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana >= 0 AND anio = {$finca->anio}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 0
                            #AND foco = 'AREA NORMAL'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + ($umbral ? 1 : 0);
                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart_ht0s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_ht0s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->ht = $this->chartInit($data_chart_ht0s,"vertical","","line", ["max" => $response->max, "min" => $response->min]);
            return json_encode($response);
        }

        public function getHMVLE0SComparativo(){
            $response = new stdClass();

            $fincas = $this->getFincas0SComp();
            $semanas = $this->getSemanas0SComp();

            $data_chart_hmvle0s = [];
            $umbral = $this->db->queryOne("SELECT s0_ht FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0) {
                $umbral = 0;
            }
            foreach($semanas as $sem){
                if($umbral) $data_chart_hmvle0s[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hojas_mas_vieja_libre) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana >= 0 AND anio = {$finca->anio}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 0
                            #AND foco = 'AREA NORMAL'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + ($umbral ? 1 : 0);

                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart_hmvle0s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_hmvle0s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->hmvle = $this->chartInit($data_chart_hmvle0s,"vertical","","line", ["max" => $response->max, "min" => $response->min]);
            return json_encode($response);
        
        }

        public function getHMVLQM0SComparativo(){
            $response = new stdClass();

            $fincas = $this->getFincas0SComp();
            $semanas = $this->getSemanas0SComp();

            $data_chart_hmvlqm0s = [];
            $umbral = $this->db->queryOne("SELECT s0_hvlq FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0) $umbral = 0;
            else $max = $min = $umbral;
            foreach($semanas as $sem){
                if($umbral) $data_chart_hmvlqm0s[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hoja_mas_vieja_libre_quema_menor) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana >= 0 AND anio = {$finca->anio}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 0
                            #AND foco = 'AREA NORMAL'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) + ($umbral ? 1 : 0);

                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm0s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_hmvlqm0s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->hmvlqm = $this->chartInit($data_chart_hmvlqm0s,"vertical","","line", ["max" => $response->max, "min" => $response->min]);
            return json_encode($response);
        }
    //END COMPARACION 0 SEM

    //BEGIN COMPARACION 11 SEM
        private function getFincas11SComp(){
            $response = [];
            $data = $this->db->queryAll("SELECT YEAR(m.fecha) AS anio, id_hacienda, CONCAT(YEAR(m.fecha), ' ', h.`nombre`) AS label
                FROM muestras_haciendas m
                INNER JOIN cat_haciendas h ON m.id_hacienda = h.`id`
                WHERE m.id_cliente = {$this->session->client} AND tipo_semana = 11 AND m.fecha IS NOT NULL
                GROUP BY id_hacienda, YEAR(m.fecha)");
            foreach($data as $row){
                $response[$row->label] = $row;
            }
            return $response;
        }
        private function getSemanas11SComp(){
            return $this->db->queryAll("SELECT getWeek(fecha) AS semana
                FROM muestras_haciendas m
                WHERE m.id_cliente = {$this->session->client} AND fecha IS NOT NULL AND tipo_semana = 11
                GROUP BY getWeek(fecha)");
        }

        public function getHT11SComparativo(){
            $response = new stdClass();

            $semanas = $this->getSemanas11SComp();
            $fincas = $this->getFincas11SComp();

            $data_chart_ht11s = [];
            $umbral = $this->db->queryOne("SELECT s11_ht FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0){
                $umbral = 6.5;
            }
            $max = $min = $umbral;
            foreach($semanas as $sem){
                if($umbral) $data_chart_ht11s[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hojas_totales) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana >= 0 AND anio = {$finca->anio}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 11
                            #AND foco = 'AREA NORMAL'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label ,array_keys($fincas)) + ($umbral ? 1 : 0);
                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart_ht11s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_ht11s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->ht = $this->chartInit($data_chart_ht11s,"vertical","","line", ["max" => $response->max, "min" => $response->min]);
            return json_encode($response);
        }

        public function getHMVLQM11SComparativo(){
            $response = new stdClass();

            $semanas = $this->getSemanas11SComp();
            $fincas = $this->getFincas11SComp();

            $data_chart_hmvlqm11s = [];
            $umbral = $this->db->queryOne("SELECT s11_hvlq FROM umbrales_clientes WHERE id_cliente = {$this->session->client}");
            if(!$umbral > 0) $umbral = 0;
            else $max = $min = $umbral;
            foreach($semanas as $sem){
                if($umbral) $data_chart_ht11s[] = ["value" => $umbral, "legend" => "Umbral", "selected" => 1, "label" => $sem->semana, "position" => 0, "z" => 3];

                foreach($fincas as $finca){
                    $sql = "SELECT 
                            AVG(hoja_mas_vieja_libre_quema_menor) AS value
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE semana >= 0 AND anio = {$finca->anio}
                            AND muestras_haciendas.id_hacienda = {$finca->id_hacienda}
                            AND muestras_haciendas.id_cliente = '{$this->session->client}' 
                            AND muestras_haciendas.id_usuario IN ('{$this->session->logges->users}')
                            AND semana = {$sem->semana}
                            AND muestras_haciendas.tipo_semana = 11
                            #AND foco = 'AREA NORMAL'";
                    $data = $this->db->queryRow($sql);

                    $position = array_search($finca->label, array_keys($fincas)) +  ($umbral ? 1 : 0);

                    if(isset($data->value) && $data->value > 0){
                        if(is_null($max) || $max < $data->value) $max = $data->value;
                        if(is_null($min) || $min > $data->value) $min = $data->value;
                        $data_chart_hmvlqm11s[] = ["value" => $data->value, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }else{
                        $data_chart_hmvlqm11s[] = ["value" => NULL, "legend" => $finca->label, "selected" => 1, "label" => $sem->semana, "position" => $position];
                    }
                }
            }

            $response->min = round($min - (($max - $min) * .1), 2);
            $response->min = ($response->min < 0) ? 0 : $response->min;
            $response->max = round($max + (($max - $min) * .1), 2);
            $response->hmvlqm = $this->chartInit($data_chart_hmvlqm11s,"vertical","","line", ["max" => $response->max, "min" => $response->min]);
            return json_encode($response);
        }
    //END COMPARACION 11 SEM

    private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = [], $zoom = false){
		$response = new stdClass;
		$response->chart = [];
		function object_to_array($obj) {
		    if(is_object($obj)) $obj = (array) $obj;
		    if(is_array($obj)) {
		        $new = array();
		        foreach($obj as $key => $val) {
		            $new[$key] = object_to_array($val);
		        }
		    }
		    else $new = $obj;
		    return $new;       
		}

		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["bottom"] = ($zoom) ? "90%" : "0%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "20%";
			$response->chart["grid"]["containLabel"] = true;
            $response->chart["plotOptions"]["series"]["connectNulls"] = true;
            if($zoom){
                $response->chart["dataZoom"] = [
                    [
                        "show" => true,
                        "realtime" => true
                    ],
                    [
                        "type" => "inside",
                        "realtime" => true
                    ]
                ];
            }
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "value" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value" , "boundaryGap" => true , "axisLine" => ["onZero" => true]];
			}
			$response->chart["series"] = [];
			$count = 0;
			$positionxAxis = -1;
			$position = -1;
			$colors = [];
			if($type == "line"){
				$response->chart["xAxis"]["data"] = [];
            }

			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = strtoupper($value->legend);
				$value->label = strtoupper($value->label);
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->positionxAxis)){
					$positionxAxis = $value->positionxAxis;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if($type == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						if(is_numeric($value->label)){
							$value->label = (double)$value->label;
						}
						if(!isset($value->positionxAxis)){
							$response->chart["xAxis"]["data"][] = $value->label;
						}else{
							$value->label = array($value->label);
							if(count($response->chart["xAxis"]["data"]) <= 0){
								while (count($response->chart["xAxis"]["data"]) <= $value->positionxAxis) {
									$response->chart["xAxis"]["data"][] = NULL;
								}
							}
							array_splice($response->chart["xAxis"]["data"] , $value->positionxAxis , 0 , $value->label);
							$response->chart["xAxis"]["data"] = array_filter($response->chart["xAxis"]["data"]);
						}
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"connectNulls" => true,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
                        if(isset($value->z)){
                            $response->chart["series"][$position]["z"] = $value->z;
                        }
					}
                    
					if(is_numeric($value->value)){
						$response->chart["series"][$position]["data"][] = ROUND($value->value,2);
					}else{
						$response->chart["series"][$position]["data"][] = $value->value;
					}

				}else{
					$response->chart["legend"]["data"][] = $value->label;
					// if($count == 0){
						$response->chart["series"][$count] = [
							"name" => $value->label,
							"type" => $type,
							"data" => [(int)$value->value],
							"label" => [
								"normal" => ["show" => true , "position" => "top"],
								"emphasis" => ["show" => true , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					// }
				}
				$count++;
            }
            if(count($response->chart["xAxis"]["data"]) < 2){
                $response->chart["yAxis"]["max"] = (isset($minMax["max"])) ? $minMax["max"] : null;
                $response->chart["yAxis"]["min"] = (isset($minMax["min"])) ? $minMax["min"] : 0;
                foreach($response->chart["series"] as $key => $data){
                    #if($response->chart["series"][$key]["name"] != 'UMBRAL'){
                        $response->chart["series"][$key]["type"] = "bar";
                    #}
                }
            }
		}

		return $response->chart;
    }
    
    public function eliminarClima(){
        $data = $this->db->queryAll("SELECT * FROM datos_clima WHERE id_cliente = 9 AND YEAR(fecha) = 2017 ORDER BY fecha, hora, rad_solar DESC");
        $last_hora = "";
        $last_fecha = "";
        foreach($data as $row){
            if($row->fecha != $last_fecha || $row->hora != $last_hora){
                $last_fecha = $row->fecha;
                $last_hora = $row->hora;
            }else{
                $this->db->query("DELETE FROM datos_clima WHERE id = $row->id");
            }
        }
    }
}

?>