<?php
	
	include 'class.sesion.php';
	include 'conexion.php';
	/**
	* 
	*/
	class Maps {
		
		private $conexion;
		private $session;
	
		public function __construct() {
	        $this->conexion = new M_Conexion();
	        $this->session = Session::getInstance();
	        // $this->session->client = 1;
	        // id_usuario IN ({$this->session->logges->users})
	    }

		public function index(){
			$sql_semana = "SELECT semana 
                            FROM geo_posiciones
							WHERE id_usuario IN ({$this->session->logges->users})
                                AND id_cliente = '{$this->session->client}'
                                AND id_finca =  '{$this->session->finca}'
                                AND YEAR(fecha) = {$this->session->year}
							GROUP BY semana";
			$res = $this->conexion->link->query($sql_semana);
			$semanas = [];
			while($fila = $res->fetch_assoc()){
				$semanas[$fila['semana']] = $fila['semana'];
			}

			return $semanas;
		}

		public function getMarkers(){
			$postdata = (object)json_decode(file_get_contents("php://input"));

			$markers = [];

			if($postdata->sem >= 0){
				$sql_muestras = "SELECT prin.id ,id_geo_posicion as muestra,nombre_muestra, id_cliente , id_finca , semana , prin.lat , 
				prin.lng,muestras.lat as mlat , muestras.lng as mlng 
				FROM geo_posiciones AS prin
				INNER JOIN geo_posiciones_muestras AS muestras ON prin.id = muestras.id_geo_posicion
				WHERE id_usuario IN ({$this->session->logges->users})
                    AND id_cliente = '{$this->session->client}'
                    AND id_finca =  '{$this->session->finca}'
                    AND semana = '{$postdata->sem}'
                    AND YEAR(fecha) = {$this->session->year}
                    -- AND YEAR(fecha) = '{$this->session->current_year}'
				ORDER BY id_geo_posicion";
				// print $sql_muestras;
				$res = $this->conexion->link->query($sql_muestras);
				$muestra_ = "";
				while($fila = $res->fetch_assoc()){
					if($muestra_ != $fila['muestra']){
						$color = $this->randomColor();
						$muestra_ = $fila['muestra'];
					}
					$markers[$fila['muestra']][] = (object)[
						'color' => $color,
						'lat' => $fila['lat'],
						'lng' => $fila['lng'],
						'nombre_muestra' => $fila['nombre_muestra'],
						'mlat' => $fila['mlat'],
						'mlng' => $fila['mlng'],
					];
				}
			}

			return $markers;
		}

		private function randomColor() {
			$str = '#';
			for($i = 0 ; $i < 6 ; $i++) {
				$randNum = rand(0 , 15);
				switch ($randNum) {
					case 10: $randNum = 'A'; break;
					case 11: $randNum = 'B'; break;
					case 12: $randNum = 'C'; break;
					case 13: $randNum = 'D'; break;
					case 14: $randNum = 'E'; break;
					case 15: $randNum = 'F'; break;
				}
				$str .= $randNum;
			}
	    	return $str;
		}
	}


$postdata = (object)json_decode(file_get_contents("php://input"));
if($postdata->opt == "INDEX"){ 
	$retval = new Maps();
	echo json_encode($retval->index());
}elseif($postdata->opt == "MARKERS"){ 
	$retval = new Maps();
	echo json_encode($retval->getMarkers());
}
?>