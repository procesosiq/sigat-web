<?php
	/**
	*  CLASS FROM Fincas
	*/
	class Pilotos 
	{
		private $conexion;
		private $session;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();
		}


		public function params(){
			$data = (object)json_decode(file_get_contents("php://input"));
			$data->id_piloto = (int)$data->id_piloto;
			return $data;
		}

		public function index(){
			$sql = "SELECT piloto.id , 
                        piloto.nombre  ,
                        PLA.nombre as placa , 
                        FUM.nombre as fumigadora , 
					    piloto.status,piloto.id_usuario
					FROM cat_pilotos AS piloto, cat_placas AS PLA, cat_fumigadoras AS FUM
					WHERE piloto.id_placa = PLA.id AND piloto.id_fumigadora = FUM.id
					GROUP BY piloto.id";
			$datos = $this->conexion->queryAll($sql);

			return json_encode($datos);
		}

		public function create(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();
			if($data->id_piloto <= 0 && $data->nombre != ""){
				$sql = "INSERT INTO cat_pilotos SET 
				nombre = '{$data->nombre}' , 
				id_fumigadora = '{$data->fumigadora}' , 
				id_placa = '{$data->placa}' ,
				id_usuario = '{$this->session->logged}'";
				$ids = $this->conexion->Consultas(1,$sql);
        		if($ids > 0){
					$response->success = 200;
					$response->data = $ids;
				}
				$this->changeProntoforms();
			}

			return json_encode($response);
		}

		public function update(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();
			if($data->id_piloto > 0 && $data->nombre != ""){
				$sql = "UPDATE cat_pilotos SET 
					nombre = '{$data->nombre}' , 
					id_fumigadora = '{$data->fumigadora}' , 
					id_placa = '{$data->placa}' 
					WHERE id = {$data->id_piloto}";
				$this->conexion->Consultas(1,$sql);
        		if($data->id_piloto > 0){
					$response->success = 200;
					$response->data = $data->id_piloto;
				}
				$this->changeProntoforms();
			}

			return json_encode($response);
		}

		public function changeStatus(){
			extract($_POST);
			if((int)$id > 0 ){
				$status = ($estado == 'activo') ? 1 : 0;
				$sql = "UPDATE cat_pilotos SET status={$status} WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				// echo $sql;
				$this->conexion->Consultas(1,$sql);
				$this->changeProntoforms();
        		return $id;
			}
		}

		public function show(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();
			if($data->id_piloto > 0){
				$sql = "SELECT 
						id_fumigadora AS fumigadora, 
						id_placa AS placa, 
						nombre AS nombre, 
						id AS id_piloto,
						id AS codigo
					FROM cat_pilotos
					WHERE id = $data->id_piloto";
				$res = $this->conexion->link->query($sql);
				$response->success = 200;
				$response->data = $res->fetch_assoc();
			}
			return json_encode($response);
		}

		public function getPlacas(){
			$data = $this->params();
			$sWhere = "";
			if(isset($data->id_fumigadora)){
				$sWhere = " AND id_fumigadora = '{$data->id_fumigadora}'";
			}
			$placas = [];
			$sql = "SELECT id , nombre FROM cat_placas WHERE status > 0 {$sWhere}";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$placas[$fila['id']] = $fila['nombre'];
			}
			
			return json_encode($placas);
		}

		public function getFumigadoras(){
			$fumigadoras = [];
			$data = $this->params();
			$sql = "SELECT id , nombre FROM cat_fumigadoras WHERE status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$fumigadoras[$fila['id']] = $fila['nombre'];
			}

			return json_encode($fumigadoras);
		}

		public  function saveConfiguracion(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = [];
			$data = $this->params();
			if($data->modo == "fumigadora"){
				$sql = "INSERT INTO cat_fumigadoras SET nombre = '{$data->campo}' , id_usuario = '{$this->session->logged}'";
			}
			if($data->modo == "placa"){
				$sql = "INSERT INTO cat_placas SET nombre = '{$data->campo}' , id_fumigadora = {$data->fumigadora} , id_usuario = '{$this->session->logged}'";
			}

			$ids = (int)$this->conexion->Consultas(1,$sql);

			return json_encode($ids);
		}

		private function changeProntoforms(){
            include ("ProntoformsOrigenes.php");
            $Prontoforms = new ProntoformsOrigenes();
            $Prontoforms->actualizarPilotos();
            $Prontoforms->actualizarPlacas();
        }
	}
?>
