<?php
	/**
	*  CLASS FROM PRODUCTORES
	*/
	class Productores 
	{
		private $conexion;
		private $session;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		public function index(){
			//echo '<script>alert("lala");</script>';
			$sWhere = "";
			$sOrder = " ORDER BY id";
			$DesAsc = "ASC";
			$sOrder .= " {$DesAsc}";
			$sLimit = "";
			if(isset($_POST)){

				/*----------  ORDER BY ----------*/
				
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY id {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY fecha {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY nombre {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY email {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY status {$DesAsc}";
				}
				/*----------  ORDER BY ----------*/

				if(isset($_POST['search_id']) && trim($_POST['search_id']) != ""){
					$sWhere .= " AND id = ".$_POST["search_id"];
				}				
				if((isset($_POST['search_date_from']) && trim($_POST['search_date_from']) != "") && (isset($_POST['search_date_to']) && trim($_POST['search_date_to']) != "")){
					$sWhere .= " AND fecha BETWEEN '".$_POST["search_date_from"]."' AND '".$_POST["search_date_to"]."'";
				}
				if(isset($_POST['search_name']) && trim($_POST['search_name']) != ""){
					$sWhere .= " AND nombre LIKE '%".$_POST['search_name']."%'";
				}
				if(isset($_POST['search_email']) && trim($_POST['search_email']) != ""){
					$sWhere .= " AND email LIKE '%".$_POST['search_email']."%'";
				}
				if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
					$sWhere .= " AND status = ".$_POST["order_status"];
				}

				/*----------  LIMIT  ----------*/
				if(isset($_POST['length']) && $_POST['length'] > 0){
					$sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
				}
			}

			$sql = "SELECT * FROM cat_clientes WHERE id_usuario = '{$this->session->logged}' $sWhere $sOrder $sLimit";
			$res = $this->conexion->link->query($sql);
			$datos = (object)[
				"customActionMessage" => "Error al consultar la informacion",
				"customActionStatus" => "Error",
				"data" => [],
				"draw" => 0,
				"recordsFiltered" => 0,
				"recordsTotal" => 0,
			];
			$count = 1;
			while($fila = $res->fetch_assoc()){
				$fila = (object)$fila;
				$count++;
				$datos->data[] = [
					$count,
					$fila->id,
					$fila->fecha,
					$fila->nombre,
					$fila->email,
					$fila->status,
					$fila->id_usuario,
					'<button class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
				];
			}

			$datos->recordsTotal = count($datos->data);
			$datos->customActionMessage = "Informacion completada con exito";
			$datos->customActionStatus = "OK";

			return json_encode($datos);
		}

		public function create(){
			extract($_POST);
			if($txtnom != "" && $txtemail != ""){
				$sql = "INSERT INTO cat_clientes SET nombre = '{$txtnom}' , email = '{$txtemail}' , fecha = CURRENT_DATE , id_usuario = '{$this->session->logged}'";
				$ids = $this->conexion->Consultas(1,$sql);
        		return $ids;
			}
		}

		public function update(){
			extract($_POST);
			if((int)$id > 0 && $txtnom != "" && $txtemail != ""){
				$sql = "UPDATE cat_clientes SET nombre = '{$txtnom}' , email = '{$txtemail}' , fecha = CURRENT_DATE WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		public function changeStatus(){
			extract($_POST);
			if((int)$id > 0 ){
				$status = ($estado == 'activo') ? 1 : 0;
				$sql = "UPDATE cat_clientes SET status={$status} WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				// echo $sql;
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		private function getUserProductor($id = 0){
			$user_id = 0;
			if($id > 0){
				$sql = "SELECT * FROM cat_clientes WHERE id_usuario = '{$this->session->logged}' AND id = {$id}";
				$res = $this->conexion->link->query($sql);
				if($res->num_rows > 0){
					$fila = $res->fetch_assoc();
					$fila = (object)$fila;
					if($fila->usuario_id > 0){
						$usuario_id = $fila->usuario_id;
						$sql_update = "UPDATE cat_usuarios SET usuario = '{$fila->email}' WHERE id = {$fila->usuario_id}";
						$this->conexion->link->query($sql_update);
					}else{
						$pass = $this->randomPassword();
						$sql_user = "INSERT INTO cat_usuarios SET 
								nombre = '{$fila->nombre}',
								email = '{$fila->email}',
								usuario = '{$fila->email}',
								pass = '{$pass}', 
								fecha_create =CURRENT_TIMESTAMP";
						$ids = $this->conexion->link->query($sql_user);
						$user_id = $this->conexion->link->insert_id;
						$usuario_id = (int)$user_id;
						if((int)$user_id > 0){
							$sql_cliente = "UPDATE cat_clientes SET usuario_id = '{$user_id}' WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
							$this->conexion->link->query($sql_cliente);

							$sql_privileges = "INSERT INTO users_privileges SET
											id_usuario ='{$user_id}',
											tresm = 'Activo',
											cerosem = 'Activo',
											oncesem = 'Activo',
											foliar = 'Activo',
											climas = 'Activo',
											fotos = 'Activo',
											mapas = 'Activo',
											informe = 'Inactivo',
											fincas = 'NONE',
											productores = 'NONE',
											agrupaciones = 'NONE'";
							$this->conexion->link->query($sql_privileges);

							$sql_membresia = "INSERT INTO users_privileges SET
											id_usuario ='{$user_id}',
											id_membresia = '{$this->session->membresia->id_membresia}'";
							$this->conexion->link->query($sql_membresia);
						}
					}
				}
			}

			return $usuario_id;
		}

		private function getTemplete($id_usuario){
			$html = "";
			$sql = "SELECT * FROM cat_usuarios WHERE id = {$id_usuario}";
			$res = $this->conexion->link->query($sql);
			if($res->num_rows > 0){
				$fila = $res->fetch_assoc();
				$fila = (object)$fila;
				$usuario = $fila->usuario;
				$pass= $fila->pass;
				$productor= $fila->nombre;
				ob_start();
				include("../email/index.html");
				$html = ob_get_clean();

			}
			return $html;
		}

		private function randomPassword() {
			$alphabet = 'abcdefghijklmnopqrstuvwxyz1234567890';
			$pass = array(); //remember to declare $pass as an array
			$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
			for ($i = 0; $i < 8; $i++) {
				$n = rand(0, $alphaLength);
				$pass[] = $alphabet[$n];
			}
			return implode($pass); //turn the array into a string
		}

		public function createUser(){
			$response = (object)[
				"success" => 400,
				"data" => []
			];
			if(isset($_POST['id'])){
				$id = (int)$_POST['id'];
				if($id > 0){
					$sql = "SELECT id as id_hacienda , id_cliente as id_productor from cat_haciendas 
							WHERE id_usuario = '{$this->session->logged}' AND id_cliente = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$id_user = $this->getUserProductor($id);
						$sql_delete = "DELETE FROM usuarios_clientes_fincas WHERE 
								id_membresia = '{$this->session->membresia->id_membresia}' AND 
								id_usuario = '{$id_user}'";
						$this->conexion->link->query($sql_delete);
						while($fila = $res->fetch_assoc()){
							$fila = (object)$fila;
							$sql = "INSERT INTO usuarios_clientes_fincas SET
								id_membresia = '{$this->session->membresia->id_membresia}',
								id_usuario = '{$id_user}',
								id_clientes = '{$fila->id_productor}',
								id_finca = '{$fila->id_hacienda}'";
							$this->conexion->link->query($sql);
						}

						$templete = $this->getTemplete($id_user);
						$response->emailSend = $this->sendEmail("javifloresp@gmail.com;arturo.calle@procesos-iq.com" , "Usuario Sigat" ,"",$templete ,"sigat@procesos-iq.com","Sigat Soporte");
					}
					$response->success = 200;
				}
			}
			return json_encode($response);
		}

		private function sendEmail($to, $subject, $body_text, $body_html, $from, $from_name) {
  
		  // Initialize cURL
		  $ch = curl_init();
		  
		  // Set cURL options
		  curl_setopt($ch, CURLOPT_URL, 'https://api.elasticemail.com/mailer/send');
		  curl_setopt($ch, CURLOPT_POST, 1);

		  // Parameter data
		  $data = 'username='.urlencode('javifloresp@gmail.com').
		      '&api_key='.urlencode('7c84da96-fcff-4165-a01a-9bd7d9f05099').
		      '&from='.urlencode($from).
		      '&from_name='.urlencode($from_name).
		      '&to='.urlencode($to).
		      '&subject='.urlencode($subject);

		  if($body_html)  $data .= '&body_html='.urlencode($body_html);
		  if($body_text)  $data .= '&body_text='.urlencode($body_text);
		  
		  // Set parameter data to POST fields
		  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		  // Header data
		      $header = "Content-Type: application/x-www-form-urlencoded\r\n";
		      $header .= "Content-Length: ".strlen($data)."\r\n\r\n";

		  // Set header
		  curl_setopt($ch, CURLOPT_HEADER, $header);
		  
		  // Set to receive server response
		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		  
		  // Set cURL to verify SSL
		  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		  
		  // Set the path to the certificate used by Elastic Mail API
		  // curl_setopt($ch, CURLOPT_CAINFO, getcwd()."/DOWNLOADED_CERTIFICATE.CRT");
		  
		  // Get result
		  $result = curl_exec($ch);
		  
		  // Close cURL
		  curl_close($ch);
		  
		  // print_r($result);
		  // Return the response or NULL on failure
		  return ($result === false) ? NULL : $result;
		  
		  // Alternative error checking return
		  // return ($result === false) ? 'Curl error: ' . curl_error($ch): $result;
		}

		public function edit(){
			$response = (object)[
				"success" => 400,
				"data" => []
			];
			if(isset($_GET['id'])){
				$id = (int)$_GET['id'];
				if($id > 0){
					$sql = "SELECT * FROM cat_clientes WHERE id_usuario = '{$this->session->logged}' AND id = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->data = (object)$res->fetch_assoc();
					}
					$response->success = 200;
				}
			}
			return json_encode($response);
		}
	}
?>
