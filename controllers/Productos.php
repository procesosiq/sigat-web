<?php
	/**
	*  CLASS FROM PRODUCTOS
	*/
	class Productos 
	{
		private $conexion;
		private $session;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		public function index(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();

			$sql = "SELECT 
					IFNULL((SELECT nombre FROM cat_formuladoras WHERE id = id_formuladora),'') AS formuladora, 
					IFNULL((SELECT nombre FROM cat_proveedores WHERE id = id_proveedor),'') AS proveedor, 
					IFNULL((SELECT nombre FROM cat_tipo_productos WHERE id = id_tipo_producto),'') AS tipo_producto, 
					nombre_comercial AS nombreComercial,
					ingrediente_activo AS ingrediente_activo,
					CONCAT(frac, IF(frac_2 != 'N/A' AND frac_2 != '', CONCAT(', ', frac_2), '')) AS frac,
					categoria,
					accion AS action,
					id AS id_producto,
					id AS codigo
				FROM products
				WHERE status != 'Archivado'";
			$res = $this->conexion->link->query($sql);
			$datos = [];
			while($fila = $res->fetch_assoc()){
				$fila = (object)$fila;
				$fila->codigo = (int)$fila->codigo;
				$datos[] = $fila;
			}

			return json_encode($datos);
		}

		public function params(){
			$data = (object)json_decode(file_get_contents("php://input"));
			$data->id_producto = (int)$data->id_producto;
			if(isset($data->precio)){
				$data->precio = (double)$data->precio;
			}
			return $data;
		}

		public function create(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();
			if($data->nombreComercial != ""){
				$dosis_2_3 = "";
				$data->dosis_2 = $data->dosis_2 > 0 ? $data->dosis_2 : 'NULL';
				$data->dosis_3 = $data->dosis_3 > 0 ? $data->dosis_3 : 'NULL';
				$data->dosis_4 = $data->dosis_4 > 0 ? $data->dosis_4 : 'NULL';
				$data->dosis_5 = $data->dosis_5 > 0 ? $data->dosis_5 : 'NULL';
				$data->dosis_6 = $data->dosis_6 > 0 ? $data->dosis_6 : 'NULL';
				$data->dosis_7 = $data->dosis_7 > 0 ? $data->dosis_7 : 'NULL';
				$data->dosis_8 = $data->dosis_8 > 0 ? $data->dosis_8 : 'NULL';
				$data->dosis_9 = $data->dosis_9 > 0 ? $data->dosis_9 : 'NULL';
				$data->dosis_10 = $data->dosis_10 > 0 ? $data->dosis_10 : 'NULL';
				
				$dosis_2_3 .= "dosis_2 = {$data->dosis_2},";
				$dosis_2_3 .= "dosis_3 = {$data->dosis_3},";
				$dosis_2_3 .= "dosis_4 = {$data->dosis_4},";
				$dosis_2_3 .= "dosis_5 = {$data->dosis_5},";
				$dosis_2_3 .= "dosis_6 = {$data->dosis_6},";
				$dosis_2_3 .= "dosis_7 = {$data->dosis_7},";
				$dosis_2_3 .= "dosis_8 = {$data->dosis_8},";
				$dosis_2_3 .= "dosis_9 = {$data->dosis_9},";
				$dosis_2_3 .= "dosis_10 = {$data->dosis_10},";
				
				$sql = "INSERT INTO `products` SET
						id_formuladora = '{$data->formuladora}',
						id_proveedor = '{$data->proveedor}',
						id_tipo_producto = '{$data->tipo_producto}',
						nombre_comercial = '{$data->nombreComercial}',
						ingrediente_activo = '{$data->ingrediente_activo}',
						frac = '{$data->frac}',
						frac_2 = '{$data->frac_2}',
                        categoria = '{$data->categoria}',
						dosis = '{$data->dosis}',
						{$dosis_2_3}
						accion = '{$data->action}'";
				$ids = (int)$this->conexion->Consultas(1,$sql);

				if($ids > 0){
					$response->success = 200;
					$response->data = $ids;
					$this->changeProntoforms($data->tipo_producto);
				}
			}

			return json_encode($response);
		}

		public function update(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();
			if($data->id_producto > 0){
				$dosis_2_3 = "";
				$data->dosis_2 = $data->dosis_2 > 0 ? $data->dosis_2 : 'NULL';
				$data->dosis_3 = $data->dosis_3 > 0 ? $data->dosis_3 : 'NULL';
				$data->dosis_4 = $data->dosis_4 > 0 ? $data->dosis_4 : 'NULL';
				$data->dosis_5 = $data->dosis_5 > 0 ? $data->dosis_5 : 'NULL';
				$data->dosis_6 = $data->dosis_6 > 0 ? $data->dosis_6 : 'NULL';
				$data->dosis_7 = $data->dosis_7 > 0 ? $data->dosis_7 : 'NULL';
				$data->dosis_8 = $data->dosis_8 > 0 ? $data->dosis_8 : 'NULL';
				$data->dosis_9 = $data->dosis_9 > 0 ? $data->dosis_9 : 'NULL';
				$data->dosis_10 = $data->dosis_10 > 0 ? $data->dosis_10 : 'NULL';
				
				$dosis_2_3 .= "dosis_2 = {$data->dosis_2},";
				$dosis_2_3 .= "dosis_3 = {$data->dosis_3},";
				$dosis_2_3 .= "dosis_4 = {$data->dosis_4},";
				$dosis_2_3 .= "dosis_5 = {$data->dosis_5},";
				$dosis_2_3 .= "dosis_6 = {$data->dosis_6},";
				$dosis_2_3 .= "dosis_7 = {$data->dosis_7},";
				$dosis_2_3 .= "dosis_8 = {$data->dosis_8},";
				$dosis_2_3 .= "dosis_9 = {$data->dosis_9},";
				$dosis_2_3 .= "dosis_10 = {$data->dosis_10},";
				
				$sql = "UPDATE `products` SET
						id_formuladora = '{$data->formuladora}',
						id_proveedor = '{$data->proveedor}',
						id_tipo_producto = '{$data->tipo_producto}',
						nombre_comercial = '{$data->nombreComercial}',
						ingrediente_activo = '{$data->ingrediente_activo}',
						frac = '{$data->frac}',
						frac_2 = '{$data->frac_2}',
                        categoria = '{$data->categoria}',
						dosis = '{$data->dosis}',
						{$dosis_2_3}
						accion = '{$data->action}'
						WHERE id = $data->id_producto";
                $this->conexion->Consultas(1,$sql);
				if($data->id_producto > 0){
					$response->success = 200;
                    $response->data = $data->id_producto;
                    
                    $sql = "UPDATE ciclos_aplicacion_hist_productos SET id_proveedor = '{$data->proveedor}', id_tipo_producto = '{$data->tipo_producto}' WHERE id_producto = '{$data->id_producto}'";
                    $this->conexion->Consultas(1,$sql);

                    $sql = "UPDATE ciclos_aplicacion_detalle SET id_proveedor = '{$data->proveedor}' WHERE id_producto = '{$data->id_producto}'";
                    $this->conexion->Consultas(1,$sql);

					$this->changeProntoforms($data->tipo_producto);
				}
			}
			return json_encode($response);
		}

		public function delete(){
			$response = new stdClass;
			$response->status = 400;
			$data = $this->params();

			if($data->id_producto > 0){
				$sql = "SELECT id_tipo_producto FROM products WHERE id = $data->id_producto";
				$tipo = $this->conexion->queryOne($sql);

				$sql = "UPDATE products SET status = 'Archivado' WHERE id = $data->id_producto";
				$this->conexion->query($sql);

				$this->changeProntoforms($tipo);
				$response->status = 200;
			}

			return json_encode($response);
		}

		public function show(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();
			if($data->id_producto > 0){
				$sql = "SELECT 
						id_formuladora AS formuladora, 
						id_proveedor AS proveedor, 
						id_tipo_producto AS tipo_producto, 
						nombre_comercial AS nombreComercial,
						ingrediente_activo AS ingrediente_activo,
                        categoria,
						frac AS frac,
						frac_2,
						accion AS action,
						id AS id_producto,
						id AS codigo,
						dosis,
						dosis_2,
						dosis_3,
						dosis_4,
						dosis_5,
						dosis_6,
						dosis_7,
						dosis_8,
						dosis_9,
						dosis_10
					FROM products
					WHERE id = $data->id_producto";
				$res = $this->conexion->link->query($sql);
				$response->success = 200;
				$response->data = $res->fetch_assoc();
			}
			return json_encode($response);
		}

		public function getPrecios(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = [];
			$data = $this->params();
			if($data->id_producto > 0){
				$sql = "SELECT id , precio , fecha FROM products_price WHERE id_producto = $data->id_producto ORDER BY fecha DESC";
				$res = $this->conexion->link->query($sql);
				while($fila = $res->fetch_assoc()){
					$fila = (object)$fila;
					$fila->precio = (double)$fila->precio;
					$fila->id = (int)$fila->id;
					$response->data[] = $fila;
				}
			}

			return json_encode($response);
		}

		public function savePrecios(){
			$response = new stdClass;
			$data = $this->params();
			if($data->id_producto > 0 && $data->precio >= 0){
				$fecha = "CURRENT_DATE";
				if($data->fecha != ""){
					$fecha = "'{$data->fecha}'";
				}	
				$sql = "INSERT INTO products_price SET id_producto = {$data->id_producto} , precio = '{$data->precio}' ,  fecha = $fecha";
				$res = $this->conexion->link->query($sql);

				$fecha_delante = $this->conexion->queryOne("SELECT MIN(fecha) FROM `products_price` WHERE fecha > {$fecha} AND id_producto = {$data->id_producto}");
				$sWhere = "";
				if($fecha_delante){
					$sWhere = " AND fecha_real <= '{$fecha_delante}'";
				}

				$sql = "UPDATE `ciclos_aplicacion_hist_productos` p
						INNER JOIN  `ciclos_aplicacion_hist` h ON h.id = p.`id_ciclo_aplicacion` AND p.`id_producto` = {$data->id_producto}
						SET
							precio = {$data->precio},
							total = {$data->precio} * cantidad
						WHERE fecha_real >= {$fecha} $sWhere";
				$this->conexion->link->query($sql);

				$sql = "UPDATE ciclos_aplicacion_hist h
						SET costo_total = oper + (SELECT SUM(total) FROM ciclos_aplicacion_hist_productos p WHERE h.id = p.`id_ciclo_aplicacion`)
						WHERE fecha_real >= {$fecha} $sWhere
							AND EXISTS (SELECT * FROM ciclos_aplicacion_hist_productos p WHERE h.id = p.`id_ciclo_aplicacion` AND p.`id_producto` = {$data->id_producto})";
				$this->conexion->link->query($sql);

				return $this->getPrecios();
			}
			return ["error" => 500];
		}

		public function changeStatus(){
			extract($_POST);
			if((int)$id > 0 ){
				$status = ($estado == 'activo') ? 1 : 0;
				$sql = "UPDATE cat_gerentes SET status={$status} WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				// echo $sql;
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		public function getFormuladoras(){
			$formuladoras = [];
			$sql = "SELECT id , UPPER(nombre) AS nombre FROM cat_formuladoras WHERE status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$formuladoras[$fila['id']] = $fila['nombre'];
			}

			return json_encode($formuladoras);
		}

        public function getProveedores(){
			$proveedores = [];
			$sql = "SELECT id , UPPER(nombre) as nombre FROM cat_proveedores WHERE status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$proveedores[$fila['id']] = $fila['nombre'];
			}

			return json_encode($proveedores);
		}

        public function getTipoProductos(){
			$tipo_productos = [];
			$sql = "SELECT id , nombre FROM cat_tipo_productos WHERE status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$tipo_productos[$fila['id']] = $fila['nombre'];
			}

			return json_encode($tipo_productos);
		}

		public function getFrac(){
			$frac = [];
			$sql = "SELECT nombre AS id , nombre AS label FROM cat_frac";
			$frac = $this->conexion->queryAllSpecial($sql);

			return json_encode($frac);
        }
        
        public function getCategorias(){
            $data = [];
			$sql = "SELECT nombre AS id , nombre AS label FROM cat_categorias_productos";
			$data = $this->conexion->queryAllSpecial($sql);

			return json_encode($data);
        }

		public function getAccion(){
			$accion = [];
			$sql = "SELECT accion AS id , accion AS nombre FROM products WHERE accion != '' GROUP BY accion";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$accion[$fila['id']] = $fila['nombre'];
			}

			return json_encode($accion);
		}

		private function getFincas($id_gerente){
			$selected = "";
			if($id_gerente){
				$selected = ", IF(id_gerente = '{$id_gerente}' , 'selected' , '') AS selected";
			}
			$cliente = [];
			$sql = "SELECT cat_fincas.id , cat_fincas.nombre  , 
					IF(id_gerente > 0 OR id_gerente != '' , 'Fincas con Gerentes' , 'Fincas sin Gerentes') AS grupo
					,id_gerente  {$selected}
					FROM cat_fincas 
					LEFT JOIN cat_gerentes ON cat_fincas.id_gerente = cat_gerentes.id
					WHERE cat_fincas.status > 0 
					ORDER BY id_gerente , nombre";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$cliente[] = (object)$fila;
			}

			return $cliente;
		}

		public  function saveConfiguracion(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = [];
			$data = $this->params();
			if($data->modo == "formuladora"){
				$sql = "INSERT INTO cat_formuladoras SET nombre = '{$data->campo}'";
			}
			if($data->modo == "proveedor"){
				$sql = "INSERT INTO cat_proveedores SET nombre = '{$data->campo}'";
			}
			if($data->modo == "tipo_producto"){
				$sql = "INSERT INTO cat_tipo_productos SET nombre = '{$data->campo}'";
			}

			$ids = (int)$this->conexion->Consultas(1,$sql);
			
			if($data->modo == "proveedor"){
				$this->changeProntoformsProv();
			}

			return json_encode($ids);
		}

		private function changeProntoformsProv(){
			include ("ProntoformsOrigenes.php");
			$Prontoforms = new ProntoformsOrigenes();
			$Prontoforms->actualizarProveedores();
		}

		private function changeProntoforms($tipo_productos){
            include ("ProntoformsOrigenes.php");
            $Prontoforms = new ProntoformsOrigenes();
			$Prontoforms->actualizarProductosNuevoFormulario();
			$Prontoforms->actualizarProveedores();
        }
	}
?>
