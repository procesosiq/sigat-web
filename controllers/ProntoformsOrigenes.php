<?php
	/**
	*  CLASS FROM Prontoforms Origenes de datos para orodelti
	*  PRONTOFORMS formspance : ORODELTI
	*/
	class ProntoformsOrigenes 
	{
		private $db;
		private $session;
		
		public function __construct()
		{
			$this->db = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		// $urlOrigin : nombre de la funcion en este controller para obtener la data
		// $nameFile : nombre para nombrar el archivo json (al final del proceso de borra el archivo)
		private function actualizar($urlOrigin ,$nameFile, $idFormSpace){
			$path_file = "/home/procesosiq/public_html/sigat/controllers/".$nameFile.".json";
            $content = file_get_contents("http://sigat.procesos-iq.com/controllers/index.php?accion=ProntoformsOrigenes.".$urlOrigin);
            file_put_contents($path_file, $content);
			
            #ejecutar curl
            $url_upload = "https://api.prontoforms.com/api/[version]/formspaces/".$idFormSpace."/upload.json";
            $url_upload = str_replace("[version]", "1", $url_upload);

            $user = "a2141374005";
            $pass = "81BRvyjToCm2HJISaUKzMBhHgUJ90M89TOQUo1ivf8NHp3K7H5k0yzZuUfBBf/le";
            
            $cmd = 'curl -v -k -u user:password -X PUT --upload-file '.$path_file.' '.$url_upload;
            $cmd = str_replace("user", $user, $cmd);
            $cmd = str_replace("password", $pass, $cmd);
			// echo "<br>".$cmd;
            exec($cmd, $out);
			// echo json_encode($out);
            unlink($path_file);
		}

		public function actualizarProductosNuevoFormulario(){
			$this->actualizar("getProductos", "productos", "192774006/sources/165706002");
			$this->actualizar("getDosis", "dosis", "192774006/sources/165706003");
		}
        
        public function actualizarPlacas(){
			// placas
			// formspace : 192774006/sources/165120020
			$this->actualizar("getPlacasFumigadoras", "placas", "192774006/sources/165120020");
		}

		public function actualizarSectores(){
			// sectores
			// formspace : 192774006/sources/164680076
			$this->actualizar("getSectores", "sectores", "192774006/sources/164680076");
		}

		public function actualizarProductos(){
			// productos
			// formspance : 192774003/sources/164561107
			$this->actualizar("getProductos", "productos", "192774003/sources/164561107");

			// formulario 2
			$this->actualizar("getProductos", "productos", "192774006/sources/165706002");
		}

		public function actualizarBioestimulantes(){
			// bioestimulantes
			// formspance : 192774006/sources/164681087
			$this->actualizar("getBioestimulantes", "bioestimulantes", "192774006/sources/164681087");
			// dosis_bioestimulantes
			// formspace : 192774006/sources/164681095
			$this->actualizar("getDosisBioestimulantes", "dosis_bioestimulantes", "192774006/sources/164681095");
		}

		public function actualizarEmulsificantes(){
			// emulsificantes
			// formspance : 192774006/sources/164681089
			$this->actualizar("getEmulsificantes", "emulsificantes", "192774006/sources/164681089");
			// dosis emulsificantes
			// formspace : 192774006/sources/164681094
			$this->actualizar("getDosisEmulsificantes", "dosis_emulsificantes", "192774006/sources/164707004");
			// $this->actualizar("getDosisEmulsificantes", "dosis_emulsificantes", "192774006/sources/164681094");
		}

		public function actualizarInsecticidas(){
			// insecticidas
			// formspance : 192774006/sources/164681088
			$this->actualizar("getInsecticidas", "insecticidas", "192774006/sources/164681088");
			// dosis inseciticidas
			// formspace : 192774006/sources/164681093
			$this->actualizar("getDosisInsecticidas", "dosis_insecticidas", "192774006/sources/164681093");
		}

		public function actualizarFincas(){
			// fincas
			// formspance : 192774006/sources/164561130
            $this->actualizar("getFincas", "fincas", "192774006/sources/164561130");
            $this->actualizar("getFincas", "fincas", "193232001/sources/165376010");
			$this->actualizar("getFincas", "fincas", "193232001/sources/165376002");
			$this->actualizarSectores();
		}

		public function actualizarAceites(){
			// aceites
			// formspance : 192774006/sources/164681085
			$this->actualizar("getAceites", "aceites", "192774006/sources/164681085");
			// dosis aceites
			// formspace : 192774006/sources/164681092
			// $this->actualizar("getDosisAceites", "dosis_aceites", "192774006/sources/164681092");
			$this->actualizar("getDosisAceites", "dosis_aceites", "192774006/sources/164707003");
		}

		public function actualizarFumigadoras(){
			// fumigadoras
			// formspance : 192774006/sources/164560106
			$this->actualizar("getFumigadoras", "fumigadoras", "192774006/sources/164560106");
		}

		public function actualizarPilotos(){
			// pilotos
			// formspance : 192774006/sources/164560105
			$this->actualizar("getPilotos", "pilotos", "192774006/sources/164560105");
		}

		public function actualizarFungicidas(){
			// fungicidas
			// formspance : 192774006/sources/164560109
			$this->actualizar("getFungicidas", "fungicidas", "192774006/sources/164560109");
			// dosis fungicidas
			// formspace : 192774006/sources/164684037
			$this->actualizar("getDosisFungicidas", "dosis_fungicidas", "192774006/sources/164684037");
		}

		public function actualizarFoliares(){
			// foliares
			// formspance : 192774006/sources/164560108
			$this->actualizar("getFoliares", "foliares", "192774006/sources/164560108");
			// dosis_foliares
			// formspace : 192774006/sources/164681096
			$this->actualizar("getDosisFoliares", "dosis_foliares", "192774006/sources/164681096");
        }
        
        public function actualizarControlErwinia(){
            $this->actualizar("getControlErwinia", "erwinia", "192774006/sources/165331031");
			$this->actualizar("getDosisControlErwinia", "dosis_erwinia", "192774006/sources/165331032");
        }

        public function actualizarOtroSigatoka(){
            $this->actualizar("getOtroSigatoka", "sigatoka", "192774006/sources/165331034");
			$this->actualizar("getDosisOtroSigatoka", "dosis_sigatoka", "192774006/sources/165331035");
        }

		public function actualizarProveedores(){
			// formspance : 192774006/sources/164560110
			$this->actualizar("getProveedores", "proveedores", "192774006/sources/164560110");

			$this->actualizar("getProveedoresFoliares", "prov_foliares", "192774006/sources/164680075");
			$this->actualizar("getProveedoresFungicidas", "prov_fungicidas", "192774006/sources/164680074");
			$this->actualizar("getProveedoresAceites", "prov_aceites", "192774006/sources/164680073");
			$this->actualizar("getProveedoresInsecticidas", "prov_insecticidas", "192774006/sources/164680072");
			$this->actualizar("getProveedoresEmulsificantes", "prov_emulsificantes", "192774006/sources/164680071");
            $this->actualizar("getProveedoresBioestimulantes", "prov_bio", "192774006/sources/164680070");
            $this->actualizar("getProveedoresControlErwinia", "prov_erwinia", "192774006/sources/165331030");
            $this->actualizar("getProveedoresOtroSigtatoka", "prov_sigatoka", "192774006/sources/165331033");
		}
		
		public function actualizarMotivos(){
			$this->actualizar("getMotivosAtraso", "motivos", "192774006/sources/164680034");
		}

        public function actualizarPreciosFumigadoras(){
            $this->actualizar("getPrecioOperacion", "precios", "192774006/sources/165163125");
        }
        
        public function getPrecioOperacion(){
            $sql = "SELECT id, id_fumigadora, id_finca, galones, precio, CONCAT('FUM',id_fumigadora, '-FIN', id_finca) AS fumigadora_finca FROM cat_fumigadoras_prices GROUP BY id_fumigadora, id_finca, galones, precio";
            $data = $this->db->queryAll($sql);
            return json_encode($data);
        }

		public function getProveedores(){
			$sql = "SELECT * FROM cat_proveedores";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}

		public function getProductos(){
			$sql = "SELECT p.id, id_proveedor, tipos.nombre as tipo_producto, nombre_comercial
					FROM products p 
					INNER JOIN cat_tipo_productos tipos ON id_tipo_producto = tipos.id 
					WHERE p.status != 'Archivado'";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}

		public function getFincas(){
			$sql = "SELECT id, nombre, id_gerente FROM cat_fincas WHERE status = 1";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}

		public function getSectores(){
			$sql = "SELECT id_finca, sector, hec_fumigacion as hectareas
					FROM fincas_sectores
					GROUP BY id_finca, sector
					ORDER BY sector";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}

		public function getPilotos(){
			$sql = "SELECT id, id_fumigadora, id_placa, id_usuario, nombre FROM cat_pilotos WHERE status = 1";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}

		public function getFumigadoras(){
			$sql = "SELECT id, id_usuario, nombre FROM cat_fumigadoras WHERE status = 1 AND id != 4";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}

		public function getPlacas(){
			$sql = "SELECT id, id_usuario, nombre, id_fumigadora FROM cat_fumigadoras WHERE status = 1";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
        }
        
        public function getPlacasFumigadoras(){
            $sql = "SELECT id, nombre as placa, id_fumigadora FROM cat_placas WHERE status = 1";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
        }

		public function getFungicidas(){
			$sql = "SELECT id AS id , nombre_comercial AS nombre, id_proveedor, dosis, dosis_2, dosis_3 FROM products WHERE id_tipo_producto = 4";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getProveedoresFungicidas(){
			$sql = "SELECT cat_proveedores.id, cat_proveedores.`nombre`
					FROM cat_proveedores
					INNER JOIN products ON cat_proveedores.id = products.id_proveedor
					WHERE id_tipo_producto = 4
					GROUP BY cat_proveedores.`id`";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getDosisFungicidas(){
			$sql = "SELECT * 
					FROM(
						SELECT id AS id_producto, dosis AS dosis, '1' AS num_dosis, '0' as id_aceite FROM products WHERE id_tipo_producto = 4 AND status != 'Archivado'
						UNION
						SELECT id AS id_producto, dosis_2 AS dosis, '2' AS num_dosis, '0' as id_aceite FROM products WHERE id_tipo_producto = 4 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_3 AS dosis, '3' AS num_dosis, '0' as id_aceite FROM products WHERE id_tipo_producto = 4 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_4 AS dosis, '4' AS num_dosis, '0' as id_aceite FROM products WHERE id_tipo_producto = 4 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_5 AS dosis, '5' AS num_dosis, '0' as id_aceite FROM products WHERE id_tipo_producto = 4 AND status != 'Archivado'
					) AS tbl
					WHERE dosis  IS NOT NUll ORDER BY num_dosis";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}

		public function getFoliares(){
			$accion = [];
			$sql = "SELECT id AS id , nombre_comercial AS nombre, id_proveedor, dosis, dosis_2, dosis_3 FROM products WHERE tipo_producto = 'Foliar' OR id_tipo_producto = 3 AND status != 'Archivado'";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getProveedoresFoliares(){
			$sql = "SELECT cat_proveedores.id, cat_proveedores.`nombre`
					FROM cat_proveedores
					INNER JOIN products ON cat_proveedores.id = products.id_proveedor
					WHERE id_tipo_producto = 3
					GROUP BY cat_proveedores.`id`";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getDosisFoliares(){
			$sql = "SELECT * 
					FROM(
						SELECT id AS id_producto, dosis AS dosis, '1' AS num_dosis FROM products WHERE id_tipo_producto = 3 AND status != 'Archivado'
						UNION
						SELECT id AS id_producto, dosis_2 AS dosis, '2' AS num_dosis FROM products WHERE id_tipo_producto = 3 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_3 AS dosis, '3' AS num_dosis FROM products WHERE id_tipo_producto = 3 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_4 AS dosis, '4' AS num_dosis FROM products WHERE id_tipo_producto = 3 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_5 AS dosis, '5' AS num_dosis FROM products WHERE id_tipo_producto = 3 AND status != 'Archivado'
					) AS tbl
					WHERE dosis  IS NOT NUll ORDER BY num_dosis";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}

		public function getGerentes(){
			$sql = "SELECT id, id_usuario, nombre, hectarea, email FROM cat_gerentes WHERE status = 1";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}

		public function getBioestimulantes(){
			$sql = "SELECT id AS id , nombre_comercial AS nombre, id_proveedor, dosis, dosis_2, dosis_3 FROM products WHERE id_tipo_producto = 1 AND status != 'Archivado'";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getProveedoresBioestimulantes(){
			$sql = "SELECT cat_proveedores.id, cat_proveedores.`nombre`
					FROM cat_proveedores
					INNER JOIN products ON cat_proveedores.id = products.id_proveedor
					WHERE id_tipo_producto = 1
					GROUP BY cat_proveedores.`id`";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getDosisBioestimulantes(){
			$sql = "SELECT * 
					FROM(
						SELECT id AS id_producto, dosis AS dosis, '1' AS num_dosis FROM products WHERE id_tipo_producto = 1 AND status != 'Archivado'
						UNION
						SELECT id AS id_producto, dosis_2 AS dosis, '2' AS num_dosis FROM products WHERE id_tipo_producto = 1 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_3 AS dosis, '3' AS num_dosis FROM products WHERE id_tipo_producto = 1 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_4 AS dosis, '4' AS num_dosis FROM products WHERE id_tipo_producto = 1 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_5 AS dosis, '5' AS num_dosis FROM products WHERE id_tipo_producto = 1 AND status != 'Archivado'
					) AS tbl
					WHERE dosis  IS NOT NUll ORDER BY num_dosis";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}

		public function getEmulsificantes(){
			$sql = "SELECT id AS id , nombre_comercial AS nombre, id_proveedor, dosis, dosis_2, dosis_3 FROM products WHERE id_tipo_producto = 2 AND status != 'Archivado'";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getProveedoresEmulsificantes(){
			$sql = "SELECT cat_proveedores.id, cat_proveedores.`nombre`
					FROM cat_proveedores
					INNER JOIN products ON cat_proveedores.id = products.id_proveedor
					WHERE id_tipo_producto = 2
					GROUP BY cat_proveedores.`id`";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getDosisEmulsificantes(){
			$sql = "SELECT * 
					FROM(
						SELECT id AS id_producto, dosis AS dosis, '1' AS num_dosis FROM products WHERE id_tipo_producto = 2 AND status != 'Archivado'
						UNION
						SELECT id AS id_producto, dosis_2 AS dosis, '2' AS num_dosis FROM products WHERE id_tipo_producto = 2 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_3 AS dosis, '3' AS num_dosis FROM products WHERE id_tipo_producto = 2 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_4 AS dosis, '4' AS num_dosis FROM products WHERE id_tipo_producto = 2 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_5 AS dosis, '5' AS num_dosis FROM products WHERE id_tipo_producto = 2 AND status != 'Archivado'
					) AS tbl
					WHERE dosis  IS NOT NUll ORDER BY num_dosis";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}

		public function getInsecticidas(){
			$sql = "SELECT id AS id , nombre_comercial AS nombre, id_proveedor, dosis, dosis_2, dosis_3 FROM products WHERE id_tipo_producto = 5 AND status != 'Archivado'";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getProveedoresInsecticidas(){
			$sql = "SELECT cat_proveedores.id, cat_proveedores.`nombre`
					FROM cat_proveedores
					INNER JOIN products ON cat_proveedores.id = products.id_proveedor
					WHERE id_tipo_producto = 5
					GROUP BY cat_proveedores.`id`";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getDosisInsecticidas(){
			$sql = "SELECT * 
					FROM(
						SELECT id AS id_producto, dosis AS dosis, '1' AS num_dosis FROM products WHERE id_tipo_producto = 5 AND status != 'Archivado'
						UNION
						SELECT id AS id_producto, dosis_2 AS dosis, '2' AS num_dosis FROM products WHERE id_tipo_producto = 5 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_3 AS dosis, '3' AS num_dosis FROM products WHERE id_tipo_producto = 5 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_4 AS dosis, '4' AS num_dosis FROM products WHERE id_tipo_producto = 5 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_5 AS dosis, '5' AS num_dosis FROM products WHERE id_tipo_producto = 5 AND status != 'Archivado'
					) AS tbl
					WHERE dosis  IS NOT NUll ORDER BY num_dosis";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}

		public function getAceites(){
			$sql = "SELECT id AS id , nombre_comercial AS nombre, id_proveedor, dosis, dosis_2, dosis_3 FROM products WHERE id_tipo_producto = 6 AND status != 'Archivado'";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getProveedoresAceites(){
			$sql = "SELECT cat_proveedores.id, cat_proveedores.`nombre`
					FROM cat_proveedores
					INNER JOIN products ON cat_proveedores.id = products.id_proveedor
					WHERE id_tipo_producto = 6
					GROUP BY cat_proveedores.`id`";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getDosisAceites(){
			$sql = "SELECT * 
					FROM(
						SELECT id AS id_producto, dosis AS dosis, '1' AS num_dosis FROM products WHERE id_tipo_producto = 6 AND status != 'Archivado'
						UNION
						SELECT id AS id_producto, dosis_2 AS dosis, '2' AS num_dosis FROM products WHERE id_tipo_producto = 6 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_3 AS dosis, '3' AS num_dosis FROM products WHERE id_tipo_producto = 6 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_4 AS dosis, '4' AS num_dosis FROM products WHERE id_tipo_producto = 6 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_5 AS dosis, '5' AS num_dosis FROM products WHERE id_tipo_producto = 6 AND status != 'Archivado'
					) AS tbl
					WHERE dosis  IS NOT NUll ORDER BY num_dosis";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
        }

        public function getControlErwinia(){
			$sql = "SELECT id AS id , nombre_comercial AS nombre, id_proveedor, dosis, dosis_2, dosis_3 FROM products WHERE id_tipo_producto = 8 AND status != 'Archivado'";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getProveedoresControlErwinia(){
			$sql = "SELECT cat_proveedores.id, cat_proveedores.`nombre`
					FROM cat_proveedores
					INNER JOIN products ON cat_proveedores.id = products.id_proveedor
					WHERE id_tipo_producto = 8
					GROUP BY cat_proveedores.`id`";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getDosisControlErwinia(){
			$sql = "SELECT * 
					FROM(
						SELECT id AS id_producto, dosis AS dosis, '1' AS num_dosis FROM products WHERE id_tipo_producto = 8 AND status != 'Archivado'
						UNION
						SELECT id AS id_producto, dosis_2 AS dosis, '2' AS num_dosis FROM products WHERE id_tipo_producto = 8 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_3 AS dosis, '3' AS num_dosis FROM products WHERE id_tipo_producto = 8 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_4 AS dosis, '4' AS num_dosis FROM products WHERE id_tipo_producto = 8 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_5 AS dosis, '5' AS num_dosis FROM products WHERE id_tipo_producto = 8 AND status != 'Archivado'
					) AS tbl
					WHERE dosis  IS NOT NUll ORDER BY num_dosis";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
        }

        public function getOtroSigatoka(){
			$sql = "SELECT id AS id , nombre_comercial AS nombre, id_proveedor, dosis, dosis_2, dosis_3 FROM products WHERE id_tipo_producto = 9 AND status != 'Archivado'";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getProveedoresOtroSigtatoka(){
			$sql = "SELECT cat_proveedores.id, cat_proveedores.`nombre`
					FROM cat_proveedores
					INNER JOIN products ON cat_proveedores.id = products.id_proveedor
					WHERE id_tipo_producto = 9
					GROUP BY cat_proveedores.`id`";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}
		public function getDosisOtroSigatoka(){
			$sql = "SELECT * 
					FROM(
						SELECT id AS id_producto, dosis AS dosis, '1' AS num_dosis FROM products WHERE id_tipo_producto = 9 AND status != 'Archivado'
						UNION
						SELECT id AS id_producto, dosis_2 AS dosis, '2' AS num_dosis FROM products WHERE id_tipo_producto = 9 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_3 AS dosis, '3' AS num_dosis FROM products WHERE id_tipo_producto = 9 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_4 AS dosis, '4' AS num_dosis FROM products WHERE id_tipo_producto = 9 AND status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_5 AS dosis, '5' AS num_dosis FROM products WHERE id_tipo_producto = 9 AND status != 'Archivado'
					) AS tbl
					WHERE dosis  IS NOT NUll ORDER BY num_dosis";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
        }

        public function getMotivosAtraso(){
            $sql = "SELECT * FROM cat_motivo_atraso WHERE status = 'Activo'";
            echo json_encode($this->db->queryAll($sql));
        }

		public function historicoDosis(){
			$sql = "SELECT * FROM (
						SELECT nombre_comercial, ciclos_aplicacion_historico.dosis_9
						FROM products
						LEFT JOIN ciclos_aplicacion_historico ON ciclos_aplicacion_historico.`foliar_2` = UPPER(nombre_comercial)
						WHERE ciclos_aplicacion_historico.dosis_9 IS NOT NULL AND ciclos_aplicacion_historico.dosis_9 > 0
						GROUP BY nombre_comercial, ciclos_aplicacion_historico.dosis_9
						) AS tbl
					GROUP BY nombre_comercial, dosis_9";

			$sql = "SELECT 'Zn 700' as nombre_comercial, dosis_9 FROM ciclos_aplicacion_historico WHERE foliar_2 = 'ZINC 700' AND dosis_9 > 0 GROUP BY dosis_9";
			$data = $this->db->queryAll($sql);
			$producs = [];
			foreach($data as $proc){
				$producs[$proc->nombre_comercial][] = $proc->dosis_9;
			}
			foreach($producs as $key => $proc){
				$sql = "UPDATE products SET ";
				for($x = 1; $x <= count($proc); $x++){
					if($x == 1){
						$sql .= " dosis = ";	
					}else{
						$sql .= " dosis_{$x} = ";
					}
					$sql .= "'{$proc[$x-1]}',";
				}
				$sql = trim($sql, ",");
				$sql .= " WHERE UPPER(nombre_comercial) = UPPER('{$key}')";
				echo $sql."<br>";
				#$this->db->query($sql);
			}
		}

		public function getDosis(){
			$sql = "SELECT * 
					FROM(
						SELECT id AS id_producto, dosis AS dosis, '1' AS num_dosis FROM products WHERE status != 'Archivado'
						UNION
						SELECT id AS id_producto, dosis_2 AS dosis, '2' AS num_dosis FROM products WHERE status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_3 AS dosis, '3' AS num_dosis FROM products WHERE status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_4 AS dosis, '4' AS num_dosis FROM products WHERE status != 'Archivado'
						UNION 
						SELECT id AS id_producto, dosis_5 AS dosis, '5' AS num_dosis FROM products WHERE  status != 'Archivado'
					) AS tbl
					WHERE dosis  IS NOT NUll ORDER BY num_dosis";
			$data = $this->db->queryAll($sql);
			return json_encode($data);
		}

		private function DD($data){
			echo "<pre>";
			D($data);
			echo "</pre>";
		}
	}
?>
