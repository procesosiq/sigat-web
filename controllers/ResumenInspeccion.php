<?php

class ResumenInspeccion 
{
	private $db;
	private $session;
	
	public function __construct()
	{
		$this->db = new M_Conexion();
		$this->postdata = (object)json_decode(file_get_contents("php://input"));
	}

	public function last(){
		$response = new stdClass;

		$response->anio = $this->db->queryOne("SELECT MAX(anio) FROM muestras_haciendas_orodelti");
		$response->semana = $this->db->queryOne("SELECT MAX(semana) FROM muestras_haciendas_orodelti WHERE anio = $response->anio AND id_finca IS NOT NULL AND id_gerente IS NOT NULL");
		$response->anios = $this->db->queryAllOne("SELECT anio FROM muestras_haciendas_orodelti WHERE anio > 0 AND id_finca IS NOT NULL AND id_gerente IS NOT NULL GROUP BY anio");

		return json_encode($response);
	}

	public function semanas(){
		$response = new stdClass;
		$response->semanas = $this->db->queryAllOne("SELECT semana FROM muestras_haciendas_orodelti WHERE anio = {$this->postdata->anio} AND id_finca IS NOT NULL AND id_gerente IS NOT NULL GROUP BY semana");
		return json_encode($response);
	}

	public function variables(){
		$response = new stdClass;
		$response->gerentes = $this->db->queryAll("SELECT id_gerente as id, cat_gerentes.nombre FROM muestras_haciendas_orodelti INNER JOIN cat_gerentes ON cat_gerentes.id = id_gerente WHERE anio = {$this->postdata->anio} AND semana = {$this->postdata->semana} AND id_gerente IS NOT NULL GROUP BY id_gerente");
		$response->fincas = $this->db->queryAll("SELECT id_finca as id, cat_fincas.nombre FROM muestras_haciendas_orodelti INNER JOIN cat_fincas ON cat_fincas.id = id_finca WHERE anio = {$this->postdata->anio} AND semana = {$this->postdata->semana} AND id_finca IS NOT NULL GROUP BY id_finca");
		return json_encode($response);
	}

	public function index(){
		$response = new stdClass;

		$sWhere = "";
		if($this->postdata->finca > 0){
			$sWhere .= " AND id_finca = {$this->postdata->finca}";
		}

		$sql = "SELECT 
					lote,
					GROUP_CONCAT(DISTINCT tipo_finca SEPARATOR ', ') tipo_finca,
					AVG(ht_3m) ht_3m,
					AVG(h2_3m) h2_3m,
					AVG(h2s_3m) h2s_3m,
					AVG(h3_3m) h3_3m,
					AVG(h3s_3m) h3s_3m,
					AVG(h4_3m) h4_3m,
					AVG(h4s_3m) h4s_3m,
					AVG(hvl_3m) hvl_3m,
					AVG(hvl_afa_3m) hvl_afa_3m,
					AVG(hvlq_3m) hvlq_3m,
					AVG(hvlq_afa_3m) hvlq_afa_3m,

					AVG(ht_0s) ht_0s,
					AVG(hvl_0s) hvl_0s,
					AVG(hvl_afa_0s) hvl_afa_0s,
					AVG(hvlq_0s) hvlq_0s,
					AVG(hvlq_afa_0s) hvlq_afa_0s,

					AVG(ht_6s) ht_6s,
					AVG(hvle_6s) hvl_6s,
					AVG(hvle_afa_6s) hvl_afa_6s,
					AVG(hvlq_6s) hvlq_6s,
					AVG(hvlq_afa_6s) hvlq_afa_6s,

					AVG(ht_11s) ht_11s,
					AVG(hvle_11s) hvl_11s,
					AVG(hvle_afa_11s) hvl_afa_11s,
					AVG(hvlq_11s) hvlq_11s,
					AVG(hvlq_afa_11s) hvlq_afa_11s

				FROM muestras_haciendas_orodelti
				WHERE anio = {$this->postdata->anio} AND semana = {$this->postdata->semana} $sWhere
				GROUP BY lote
				ORDER BY lote+0";
		$response->data = $this->db->queryAll($sql);

		return json_encode($response);
	}

}

?>