<?php
	/**
	*  CLASS FROM PRODUCTOS
	*/
	class RevisionTecnico
	{
		private $db;
		private $session;
		
		public function __construct(){
			$this->db = new M_Conexion();
            $this->session = Session::getInstance();
            $available = $this->db->queryRow("SELECT GROUP_CONCAT(CONCAT(\"'\", f.nombre, \"'\") SEPARATOR ',') AS fincas, GROUP_CONCAT(f.id SEPARATOR ',') AS ids
                FROM cat_usuarios u
                INNER JOIN users_fincas_privileges ON id_user = u.id
                INNER JOIN cat_fincas f ON id_finca = f.`id`
                WHERE u.tipo = 'CICLOS' AND u.id = {$this->session->id}");
            $this->available_fincas = $available->fincas;
            $this->available_id_fincas = $available->ids;
        }
        
        public function listado(){
            $response = new stdClass;
            $response->data = $this->db->queryAll("SELECT 
                    reporte.id,
                    num_ciclo as ciclo, 
                    fecha_programada, 
                    fecha_real, 
                    finca, 
                    hectareas_fumigacion as ha, 
                    programa,
                    GROUP_CONCAT(DISTINCT sector SEPARATOR ',') AS sectores,
                    REPLACE(REPLACE(json, '/ciclos/old/', ''), '.json', '') as json
                FROM cycle_application_reporte reporte 
                INNER JOIN cycle_application_reporte_sectores sectores ON id_cycle_application_reporte = reporte.id
                GROUP BY reporte.id
                ORDER BY fecha_real DESC");
            return json_encode($response);
        }
		
		private function getFilters(){
			$data = (object)json_decode(file_get_contents("php://input"));
			$response = new stdClass;
            $response->id = (int) $data->id;
            $response->data = (object) $data->data;
			return $response;
        }

		public function index(){
			$response = new stdClass;
            $filters = $this->getFilters();
            
            $response->data = $this->db->queryRow("SELECT * FROM cycle_application_reporte WHERE id = $filters->id");
            
            // ACEITE
            $response->data->id_prov_aceite = $this->db->queryOne("SELECT id_proveedor FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = $filters->id AND id_tipo_producto = 6");
            $response->data->id_aceite = $this->db->queryOne("SELECT id_producto FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = $filters->id AND id_tipo_producto = 6");
            $response->data->aceite_dosis = (float) $this->db->queryOne("SELECT dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = $filters->id AND id_tipo_producto = 6");

            $response->data->sectores = $this->db->queryAll("SELECT id_sector, sector, hectareas FROM cycle_application_reporte_sectores WHERE id_cycle_application_reporte = $filters->id");
            $response->data->total_hectareas = $response->data->hectareas_fumigacion;

            //fungicidas, insecticidas
            $response->data->fungicidas = $this->db->queryAll("SELECT id AS id_reporte_producto, id_producto, id_proveedor, dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = $filters->id AND id_tipo_producto = 4");
            $response->data->insecticidas = $this->db->queryAll("SELECT id AS id_reporte_producto, id_producto, id_proveedor, dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = $filters->id AND id_tipo_producto = 5");
            $response->data->emulsificantes = $this->db->queryAll("SELECT id AS id_reporte_producto, id_producto, id_proveedor, dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = $filters->id AND id_tipo_producto = 2");
            $response->data->foliares = $this->db->queryAll("SELECT id AS id_reporte_producto, id_producto, id_proveedor, dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = $filters->id AND id_tipo_producto = 3");
            $response->data->sigatokas = $this->db->queryAll("SELECT id AS id_reporte_producto, id_producto, id_proveedor, dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = $filters->id AND id_tipo_producto = 9");
            $response->data->erwinias = $this->db->queryAll("SELECT id AS id_reporte_producto, id_producto, id_proveedor, dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = $filters->id AND id_tipo_producto = 8");
			return json_encode($response);
        }
        
        public function productos(){
            $response = new stdClass;
            
            $response->motivos = $this->db->queryAll("SELECT * FROM cat_motivo_atraso WHERE status = 'Activo'");
            $response->placas = $this->db->queryAll("SELECT id, nombre as placa, id_fumigadora FROM cat_placas WHERE status = 1");
            $response->pilotos = $this->db->queryAll("SELECT id, id_fumigadora, id_placa, id_usuario, nombre FROM cat_pilotos WHERE status = 1");
            $response->sectores = $this->db->queryAll("SELECT id, id_finca, sector, hec_fumigacion as hectareas FROM fincas_sectores GROUP BY id_finca, sector ORDER BY sector");
            $response->fincas = $this->db->queryAll("SELECT id, nombre FROM cat_fincas WHERE status > 0");
            $response->fumigadoras = [
                "fumigadoras" => $this->db->queryAll("SELECT id, nombre FROM cat_fumigadoras WHERE status = 1"),
                "precios" => $this->db->queryAll("SELECT id_fumigadora, id_finca, precio FROM cat_fumigadoras_prices"),
                "galones" => $this->db->queryAll("SELECT id_fumigadora, galones, precio, id_finca FROM cat_fumigadoras_prices")
            ];
            $response->bioestimulantes = [
                "proveedores" => $this->db->queryAllSpecial("SELECT id_proveedor AS id, prov.nombre AS label FROM products INNER JOIN cat_proveedores prov ON id_proveedor = prov.id WHERE id_tipo_producto = 1 GROUP BY id_proveedor"),
                "productos" => $this->db->queryAll("SELECT id, nombre_comercial, id_proveedor FROM products WHERE id_tipo_producto = 1"),
                "dosis" => $this->getDosis(1)
            ];
            $response->fungicidas = [
                "proveedores" => $this->db->queryAllSpecial("SELECT id_proveedor AS id, prov.nombre AS label FROM products INNER JOIN cat_proveedores prov ON id_proveedor = prov.id WHERE id_tipo_producto = 4 GROUP BY id_proveedor"),
                "productos" => $this->db->queryAll("SELECT id, nombre_comercial, id_proveedor FROM products WHERE id_tipo_producto = 4"),
                "dosis" => $this->getDosis(4)
            ];
            $response->aceites = [
                "proveedores" => $this->db->queryAllSpecial("SELECT id_proveedor AS id, prov.nombre AS label FROM products INNER JOIN cat_proveedores prov ON id_proveedor = prov.id WHERE id_tipo_producto = 6 GROUP BY id_proveedor"),
                "productos" => $this->db->queryAll("SELECT id, nombre_comercial, id_proveedor FROM products WHERE id_tipo_producto = 6"),
                "dosis" => $this->getDosis(6)
            ];
            $response->emulsificantes = [
                "proveedores" => $this->db->queryAllSpecial("SELECT id_proveedor AS id, prov.nombre AS label FROM products INNER JOIN cat_proveedores prov ON id_proveedor = prov.id WHERE id_tipo_producto = 2 GROUP BY id_proveedor"),
                "productos" => $this->db->queryAll("SELECT id, nombre_comercial, id_proveedor FROM products WHERE id_tipo_producto = 2"),
                "dosis" => $this->getDosis(2)
            ];
            $response->insecticidas = [
                "proveedores" => $this->db->queryAllSpecial("SELECT id_proveedor AS id, prov.nombre AS label FROM products INNER JOIN cat_proveedores prov ON id_proveedor = prov.id WHERE id_tipo_producto = 5 GROUP BY id_proveedor"),
                "productos" => $this->db->queryAll("SELECT id, nombre_comercial, id_proveedor FROM products WHERE id_tipo_producto = 5"),
                "dosis" => $this->getDosis(5)
            ];
            $response->foliares = [
                "proveedores" => $this->db->queryAllSpecial("SELECT id_proveedor AS id, prov.nombre AS label FROM products INNER JOIN cat_proveedores prov ON id_proveedor = prov.id WHERE id_tipo_producto = 3 GROUP BY id_proveedor"),
                "productos" => $this->db->queryAll("SELECT id, nombre_comercial, id_proveedor FROM products WHERE id_tipo_producto = 3"),
                "dosis" => $this->getDosis(3)
            ];
            $response->erwinias = [
                "proveedores" => $this->db->queryAllSpecial("SELECT id_proveedor AS id, prov.nombre AS label FROM products INNER JOIN cat_proveedores prov ON id_proveedor = prov.id WHERE id_tipo_producto = 8 GROUP BY id_proveedor"),
                "productos" => $this->db->queryAll("SELECT id, nombre_comercial, id_proveedor FROM products WHERE id_tipo_producto = 8"),
                "dosis" => $this->getDosis(8)
            ];
            $response->sigatokas = [
                "proveedores" => $this->db->queryAllSpecial("SELECT id_proveedor AS id, prov.nombre AS label FROM products INNER JOIN cat_proveedores prov ON id_proveedor = prov.id WHERE id_tipo_producto = 9 GROUP BY id_proveedor"),
                "productos" => $this->db->queryAll("SELECT id, nombre_comercial, id_proveedor FROM products WHERE id_tipo_producto = 9"),
                "dosis" => $this->getDosis(9)
            ];
            return json_encode($response);
        }

        public function save(){
            $postdata = (object)json_decode(file_get_contents("php://input"));
            $postdata = $postdata->data;
            
            if(
                empty($postdata->fecha_programada) ||
                empty($postdata->fecha_real) || 
                !$postdata->id_finca > 0 ||
                empty($postdata->programa) ||
                empty($postdata->tipo_ciclo) ||
                !$postdata->precio_operacion > 0 ||
                !$postdata->total_hectareas > 0
            ) return json_encode(false);

            $total_ha = 0;
            foreach($postdata->sectores as $sec){
                if($sec->hectareas > 0){
                    $total_ha += $sec->hectareas;
                }
            }

            $id_ciclo_hist = $this->db->queryOne("SELECT id FROM ciclos_aplicacion_hist WHERE id_cycle_application = $postdata->id");
            $sql = "DELETE FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = $id_ciclo_hist";
            $this->db->query($sql);

            $sql = "UPDATE cycle_application_reporte
                    SET 
                        fecha_programada = '{$postdata->fecha_programada}',
                        fecha_real = '{$postdata->fecha_real}',
                        hora = '{$postdata->hora}',
                        id_finca = {$postdata->id_finca},
                        finca = (SELECT nombre FROM cat_fincas WHERE id = $postdata->id_finca),
                        hectareas_fumigacion = $total_ha,
                        programa = '{$postdata->programa}',
                        tipo_ciclo = '{$postdata->tipo_ciclo}',
                        num_ciclo = '{$postdata->num_ciclo}',
                        id_fumigadora = {$postdata->id_fumigadora},
                        fumigadora = (SELECT nombre FROM cat_fumigadoras WHERE id = $postdata->id_fumigadora),
                        galones = '{$postdata->galones}',
                        dosis_agua = '{$postdata->dosis_agua}',
                        precio_operacion = '{$postdata->precio_operacion}',
                        id_piloto = '{$postdata->id_piloto}',
                        piloto = (SELECT nombre FROM cat_pilotos WHERE id = '{$postdata->id_piloto}'),
                        id_placa = '{$postdata->id_placa}',
                        placa_avion = (SELECT nombre FROM cat_placas WHERE id = '{$postdata->id_placa}'),
                        hora_salida = '{$postdata->hora_salida}',
                        hora_llegada = '{$postdata->hora_llegada}',
                        motivo = '{$postdata->motivo}'
                    WHERE id = $postdata->id";
            $this->db->query($sql);

            $costo_total = 0;
            foreach($postdata->fungicidas as $prod){
                if($prod->id_producto > 0 && $prod->dosis > 0){
                    $precio = (float) $this->db->queryOne("SELECT precio FROM products_price WHERE id_producto = $prod->id_producto ORDER BY fecha DESC LIMIT 1");
                    $cantidad = (float) $prod->dosis * $postdata->hectareas_fumigacion;

                    if(isset($prod->id_reporte_producto) && $prod->id_reporte_producto > 0){
                        $sql = "UPDATE cycle_application_reporte_productos
                                SET
                                    id_producto = $prod->id_producto,
                                    producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                    dosis = $prod->dosis,
                                    id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                    id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                    precio = $precio,
                                    cantidad = $cantidad,
                                    ha = $postdata->hectareas_fumigacion,
                                    total = $cantidad * $precio
                                WHERE id = $prod->id_reporte_producto";
                    }else{
                        $sql = "INSERT INTO cycle_application_reporte_productos
                                SET
                                    id_ciclo_aplicacion = $postdata->id,
                                    id_producto = $prod->id_producto,
                                    producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                    dosis = $prod->dosis,
                                    id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                    id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                    precio = $precio,
                                    cantidad = $cantidad,
                                    ha = $postdata->hectareas_fumigacion,
                                    total = $cantidad * $precio";
                    }
                    $this->db->query($sql);

                    $sql = "INSERT INTO ciclos_aplicacion_hist_productos SET
                                id_ciclo_aplicacion =  $id_ciclo_hist,
                                id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                id_producto = $prod->id_producto,
                                producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                precio = $precio,
                                cantidad = $cantidad,
                                ha = $postdata->hectareas_fumigacion,
                                total = $cantidad * $precio";
                    $costo_total += ($cantidad * $precio);
                    $this->db->query($sql);
                }
            }
            foreach($postdata->emulsificantes as $prod){
                if($prod->id_producto > 0 && $prod->dosis > 0){
                    $precio = (float) $this->db->queryOne("SELECT precio FROM products_price WHERE id_producto = $prod->id_producto ORDER BY fecha DESC LIMIT 1");
                    $cantidad = (float) $prod->dosis * $postdata->hectareas_fumigacion;

                    if(isset($prod->id_reporte_producto) && $prod->id_reporte_producto > 0){
                        $sql = "UPDATE cycle_application_reporte_productos
                                SET
                                    id_producto = $prod->id_producto,
                                    producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                    dosis = $prod->dosis,
                                    id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                    id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                    precio = $precio,
                                    cantidad = $cantidad,
                                    ha = $postdata->hectareas_fumigacion,
                                    total = $cantidad * $precio
                                WHERE id = $prod->id_reporte_producto";
                    }else{
                        $sql = "INSERT INTO cycle_application_reporte_productos
                                SET
                                    id_ciclo_aplicacion = $postdata->id,
                                    id_producto = $prod->id_producto,
                                    producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                    dosis = $prod->dosis,
                                    id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                    id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                    precio = $precio,
                                    cantidad = $cantidad,
                                    ha = $postdata->hectareas_fumigacion,
                                    total = $cantidad * $precio";
                    }
                    $this->db->query($sql);
                    $sql = "INSERT INTO ciclos_aplicacion_hist_productos SET
                                id_ciclo_aplicacion =  $id_ciclo_hist,
                                id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                id_producto = $prod->id_producto,
                                producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                precio = $precio,
                                cantidad = $cantidad,
                                ha = $postdata->hectareas_fumigacion,
                                total = $cantidad * $precio";
                    $costo_total += ($cantidad * $precio);
                    $this->db->query($sql);
                }
            }
            foreach($postdata->erwinias as $prod){
                if($prod->id_producto > 0 && $prod->dosis > 0){
                    $precio = (float) $this->db->queryOne("SELECT precio FROM products_price WHERE id_producto = $prod->id_producto ORDER BY fecha DESC LIMIT 1");
                    $cantidad = (float) $prod->dosis * $postdata->hectareas_fumigacion;

                    if(isset($prod->id_reporte_producto) && $prod->id_reporte_producto > 0){
                        $sql = "UPDATE cycle_application_reporte_productos
                                SET
                                    id_producto = $prod->id_producto,
                                    producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                    dosis = $prod->dosis,
                                    id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                    id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                    precio = $precio,
                                    cantidad = $cantidad,
                                    ha = $postdata->hectareas_fumigacion,
                                    total = $cantidad * $precio
                                WHERE id = $prod->id_reporte_producto";
                    }else{
                        $sql = "INSERT INTO cycle_application_reporte_productos
                                SET
                                    id_ciclo_aplicacion = $postdata->id,
                                    id_producto = $prod->id_producto,
                                    producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                    dosis = $prod->dosis,
                                    id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                    id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                    precio = $precio,
                                    cantidad = $cantidad,
                                    ha = $postdata->hectareas_fumigacion,
                                    total = $cantidad * $precio";
                    }
                    $this->db->query($sql);
                    $sql = "INSERT INTO ciclos_aplicacion_hist_productos SET
                                id_ciclo_aplicacion =  $id_ciclo_hist,
                                id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                id_producto = $prod->id_producto,
                                producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                precio = $precio,
                                cantidad = $cantidad,
                                ha = $postdata->hectareas_fumigacion,
                                dosis = $prod->dosis,
                                total = $cantidad * $precio";
                    $costo_total += ($cantidad * $precio);
                    $this->db->query($sql);
                }
            }
            foreach($postdata->foliares as $prod){
                if($prod->id_producto > 0 && $prod->dosis > 0){
                    $precio = (float) $this->db->queryOne("SELECT precio FROM products_price WHERE id_producto = $prod->id_producto ORDER BY fecha DESC LIMIT 1");
                    $cantidad = (float) $prod->dosis * $postdata->hectareas_fumigacion;

                    if(isset($prod->id_reporte_producto) && $prod->id_reporte_producto > 0){
                        $sql = "UPDATE cycle_application_reporte_productos
                                SET
                                    id_producto = $prod->id_producto,
                                    producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                    dosis = $prod->dosis,
                                    id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                    id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                    precio = $precio,
                                    cantidad = $cantidad,
                                    ha = $postdata->hectareas_fumigacion,
                                    total = $cantidad * $precio
                                WHERE id = $prod->id_reporte_producto";
                    }else{
                        $sql = "INSERT INTO cycle_application_reporte_productos
                                SET
                                    id_ciclo_aplicacion = $postdata->id,
                                    id_producto = $prod->id_producto,
                                    producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                    dosis = $prod->dosis,
                                    id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                    id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                    precio = $precio,
                                    cantidad = $cantidad,
                                    ha = $postdata->hectareas_fumigacion,
                                    total = $cantidad * $precio";
                    }
                    $this->db->query($sql);
                    $sql = "INSERT INTO ciclos_aplicacion_hist_productos SET
                                id_ciclo_aplicacion =  $id_ciclo_hist,
                                id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                id_producto = $prod->id_producto,
                                producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                precio = $precio,
                                cantidad = $cantidad,
                                ha = $postdata->hectareas_fumigacion,
                                total = $cantidad * $precio";
                    $costo_total += ($cantidad * $precio);
                    $this->db->query($sql);
                }
            }
            foreach($postdata->insecticidas as $prod){
                if($prod->id_producto > 0 && $prod->dosis > 0){
                    $precio = (float) $this->db->queryOne("SELECT precio FROM products_price WHERE id_producto = $prod->id_producto ORDER BY fecha DESC LIMIT 1");
                    $cantidad = (float) $prod->dosis * $postdata->hectareas_fumigacion;

                    if(isset($prod->id_reporte_producto) && $prod->id_reporte_producto > 0){
                        $sql = "UPDATE cycle_application_reporte_productos
                                SET
                                    id_producto = $prod->id_producto,
                                    producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                    dosis = $prod->dosis,
                                    id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                    id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                    precio = $precio,
                                    cantidad = $cantidad,
                                    ha = $postdata->hectareas_fumigacion,
                                    total = $cantidad * $precio
                                WHERE id = $prod->id_reporte_producto";
                    }else{
                        $sql = "INSERT INTO cycle_application_reporte_productos
                                SET
                                    id_ciclo_aplicacion = $postdata->id,
                                    id_producto = $prod->id_producto,
                                    producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                    dosis = $prod->dosis,
                                    id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                    id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                    precio = $precio,
                                    cantidad = $cantidad,
                                    ha = $postdata->hectareas_fumigacion,
                                    total = $cantidad * $precio";
                    }
                    $this->db->query($sql);
                    $sql = "INSERT INTO ciclos_aplicacion_hist_productos SET
                                id_ciclo_aplicacion =  $id_ciclo_hist,
                                id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                id_producto = $prod->id_producto,
                                producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                precio = $precio,
                                cantidad = $cantidad,
                                ha = $postdata->hectareas_fumigacion,
                                total = $cantidad * $precio";
                    $costo_total += ($cantidad * $precio);
                    $this->db->query($sql);
                }
            }
            foreach($postdata->sigatokas as $prod){
                if($prod->id_producto > 0 && $prod->dosis > 0){
                    $precio = (float) $this->db->queryOne("SELECT precio FROM products_price WHERE id_producto = $prod->id_producto ORDER BY fecha DESC LIMIT 1");
                    $cantidad = (float) $prod->dosis * $postdata->hectareas_fumigacion;

                    if(isset($prod->id_reporte_producto) && $prod->id_reporte_producto > 0){
                        $sql = "UPDATE cycle_application_reporte_productos
                                SET
                                    id_producto = $prod->id_producto,
                                    producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                    dosis = $prod->dosis,
                                    id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                    id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                    precio = $precio,
                                    cantidad = $cantidad,
                                    ha = $postdata->hectareas_fumigacion,
                                    total = $cantidad * $precio
                                WHERE id = $prod->id_reporte_producto";
                    }else{
                        $sql = "INSERT INTO cycle_application_reporte_productos
                                SET
                                    id_ciclo_aplicacion = $postdata->id,
                                    id_producto = $prod->id_producto,
                                    producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                    dosis = $prod->dosis,
                                    id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                    id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                    precio = $precio,
                                    cantidad = $cantidad,
                                    ha = $postdata->hectareas_fumigacion,
                                    total = $cantidad * $precio";
                    }
                    $this->db->query($sql);
                    $sql = "INSERT INTO ciclos_aplicacion_hist_productos SET
                                id_ciclo_aplicacion =  $id_ciclo_hist,
                                id_proveedor = (SELECT id_proveedor FROM products WHERE id = $prod->id_producto),
                                id_tipo_producto = (SELECT id_tipo_producto FROM products WHERE id = $prod->id_producto),
                                id_producto = $prod->id_producto,
                                producto = (SELECT nombre_comercial FROM products WHERE id = $prod->id_producto),
                                precio = $precio,
                                cantidad = $cantidad,
                                ha = $postdata->hectareas_fumigacion,
                                total = $cantidad * $precio";
                    $costo_total += ($cantidad * $precio);
                    $this->db->query($sql);
                }
            }

            $sql = "UPDATE ciclos_aplicacion_hist SET
                        hora = '{$postdata->hora}',
                        fecha_prog = '{$postdata->fecha_programada}',
                        fecha_real = '{$postdata->fecha_real}',
                        id_finca = {$postdata->id_finca},
                        finca = (SELECT nombre FROM cat_fincas WHERE id = $postdata->id_finca),
                        hectareas_fumigacion = $total_ha,
                        programa = '{$postdata->programa}',
                        tipo_ciclo = '{$postdata->tipo_ciclo}',
                        num_ciclo = '{$postdata->num_ciclo}',
                        id_fumigadora = {$postdata->id_fumigadora},
                        fumigadora = (SELECT nombre FROM cat_fumigadoras WHERE id = $postdata->id_fumigadora),
                        dosis_agua = '{$postdata->dosis_agua}',
                        motivo = '{$postdata->motivo}',
                        semana = getWeek('{$postdata->fecha_real}'),
                        anio = getYear('{$postdata->fecha_real}'),
                        ha_oper = {$postdata->precio_operacion},
                        oper = ROUND($postdata->precio_operacion * $total_ha, 2),
                        costo_total = ROUND($costo_total, 2)
                    WHERE id_cycle_application = $postdata->id";
            $this->db->query($sql);

            return json_encode(true);
        }

        private function getDosis($id_tipo_producto){
            $dosis = [];
            for($x = 1; $x <= 10; $x++){
                if($x == 1){
                    $dosis[] = "SELECT id AS id_producto, dosis, {$x} AS num_dosis
                                FROM products 
                                WHERE id_tipo_producto = {$id_tipo_producto} AND dosis IS NOT NULL";
                }else{
                    $dosis[] = "SELECT id AS id_producto, dosis_{$x} AS dosis, {$x} AS num_dosis
                                FROM products 
                                WHERE id_tipo_producto = {$id_tipo_producto} AND dosis_{$x} IS NOT NULL";
                }
            }
            $dosis = implode(" UNION ALL ", $dosis);
            $data = $this->db->queryAll("SELECT * FROM ($dosis) AS tbl ORDER BY num_dosis");
            foreach($data as $row){
                $row->id_producto = (int) $row->id_producto;
            }
            return $data;
        }

        public function delete(){
            $postdata = (object)json_decode(file_get_contents("php://input"));

            if($postdata->json != ''){
                $sql = "SELECT id FROM cycle_application_reporte WHERE id = '{$postdata->id}'";
                $ids = $this->db->queryAllOne($sql);

                foreach($ids as $id){
                    $sql = "SELECT id FROM ciclos_aplicacion_hist WHERE id_cycle_application = '{$id}'";
                    $ids_web = $this->db->queryAllOne($sql);

                    foreach($ids_web as $id_web){
                        $sql = "DELETE FROM ciclos_aplicacion_hist_productos WHERE id_ciclo_aplicacion = $id_web";
                        $this->db->query($sql);
                    }

                    $sql = "DELETE FROM ciclos_aplicacion_hist WHERE id_cycle_application = $id";
                    $this->db->query($sql);

                    $sql = "DELETE FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = $id";
                    $this->db->query($sql);

                    $sql = "DELETE FROM cycle_application_reporte WHERE id = $id";
                    $this->db->query($sql);

                    $id_form = $this->db->queryOne("SELECT id FROM cycle_application_reporte WHERE id = '{$postdata->id}'");
                    $sql = "DELETE FROM cycle_application_reporte WHERE id = $id_form";
                    $this->db->query($sql);
                    /*$sql = "DELETE FROM cycle_application_reporte_productos_formularios WHERE id_cycle_application_reporte = $id_form";
                    $this->db->query($sql);*/
                }

                return ["status" => 200];
            }
            return ["status" => 404];
        }
    }
?>
