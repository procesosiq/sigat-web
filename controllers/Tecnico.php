<?php
	/**
	*  CLASS FROM PRODUCTOS
	*/
	class Tecnico 
	{
		private $db;
		private $session;
		
		public function __construct()
		{
			$this->db = new M_Conexion();
            $this->session = Session::getInstance();
            $available = $this->db->queryRow("SELECT GROUP_CONCAT(CONCAT(\"'\", f.nombre, \"'\") SEPARATOR ',') AS fincas, GROUP_CONCAT(f.id SEPARATOR ',') AS ids
                FROM cat_usuarios u
                INNER JOIN users_fincas_privileges ON id_user = u.id
                INNER JOIN cat_fincas f ON id_finca = f.`id`
                WHERE u.tipo = 'CICLOS' AND u.id = {$this->session->id}");
            $this->available_fincas = $available->fincas;
            $this->available_id_fincas = $available->ids;
		}
		
		private function getFilters(){
			$data = (object)json_decode(file_get_contents("php://input"));
			$response = new stdClass;
            $response->semana = (int)$data->sem;
            $response->semana2 = (int)$data->sem2;
			return $response;
		}


		/*public function index(){
			$data = (object)json_decode(file_get_contents("php://input"));
			$response = new stdClass;
			$filters = $this->getFilters();
            $sWhere = "";
            $sWhere2 = "";
			if($filters->semana > 0){
                $sWhere .= " AND semana >= '{$filters->semana}'";
                $sWhere2 .= " AND WEEK(c.fecha) >= {$filters->semana}";
            }
            if($filters->semana2 > 0){
                $sWhere .= " AND semana <= '{$filters->semana2}'";
                $sWhere2 .= " AND WEEK(c.fecha) <= {$filters->semana2}";
            }

            $sql = "SELECT * FROM resumen_ciclo WHERE finca IN ({$this->available_fincas}) {$sWhere} ORDER BY id DESC";
			$response->table1 = $this->db->queryAll($sql);

			$sql = "SELECT r.* FROM resumen_ciclo_2 r INNER JOIN cycle_application c ON c.id = id_json WHERE finca IN ({$this->available_fincas}) {$sWhere2} ORDER BY r.id DESC";
            $response->table2 = $this->db->queryAll($sql);
            
            $sql = "SELECT c.id, f.`nombre` AS finca, c.sector, hectareas_fumigacion AS ha, c.fecha, 
                        fungicida, dosis AS 'DOSIS 1', fungicida_2 AS 'FUNGICIDA 2', dosis_2 AS 'DOSIS 2', 
                        bioestimulante_1 AS 'BIOESTIMULANTE', dosis_bioestimulante_1 AS 'DOSIS 3', 
                        emulsificante_1 AS 'COADYUVANTE', dosis_emulsificante_1 AS 'DOSIS 4',
                        aceite, dosis_aceite AS 'DOSIS 5', agua, dosis_agua AS 'DOSIS 6', fumigadora, p.`nombre` AS piloto, 
                        placa_avion AS 'PLACA', hora_salida AS 'HORA SALIDA', hora_llegada AS 'HORA LLEGADA', motivo AS 'MOTIVO ATRASO', 
                        notificacion, tipo_ciclo
                FROM cycle_application c 
                INNER JOIN cat_fincas f ON id_hacienda = f.`id`
                INNER JOIN cat_pilotos p ON id_piloto = p.`id`
                WHERE id_hacienda IN ({$this->available_id_fincas}) {$sWhere2}";
            $response->table_to_export = $this->db->queryAll($sql);

			$response->semanas = $this->db->queryAllSpecial("SELECT semana AS id , semana AS label FROM resumen_ciclo WHERE semana > 0 GROUP BY semana");
			$response->ciclos = $this->db->queryAllSpecial("SELECT ciclo AS id , ciclo AS label FROM resumen_ciclo WHERE ciclo > 0 GROUP BY ciclo");

			return json_encode($response);
        }*/
        
        public function index(){
            $data = (object)json_decode(file_get_contents("php://input"));
			$response = new stdClass;
			$filters = $this->getFilters();
            $sWhere = "";
            $sWhere2 = "";
			if($filters->semana > 0){
                $sWhere .= " AND semana >= '{$filters->semana}'";
                $sWhere2 .= " AND WEEK(c.fecha) >= {$filters->semana}";
            }
            if($filters->semana2 > 0){
                $sWhere .= " AND semana <= '{$filters->semana2}'";
                $sWhere2 .= " AND WEEK(c.fecha) <= {$filters->semana2}";
            }

            $sql = "SELECT 
                        reporte.id, 
                        finca, 
                        sector, 
                        hectareas AS ha, 
                        fecha_real,
                        hora_salida,
                        hora_llegada,
                        motivo AS atraso,
                        (SELECT producto FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1) AS fungicidad_1,
                        (SELECT dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1) AS dosis_1,
                        (SELECT producto FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1,1) AS fungicidad_2,
                        (SELECT dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1,1) AS dosis_2,
                        (SELECT producto FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 1 ORDER BY id LIMIT 1) AS bioestimulantes,
                        (SELECT dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 1 ORDER BY id LIMIT 1) AS dosis_3,
                        (SELECT producto FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1) AS coadyuvante,
                        (SELECT dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1) AS dosis_4,
                        (SELECT producto FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 6 ORDER BY id LIMIT 1) AS aceite,
                        (SELECT dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 6 ORDER BY id LIMIT 1) AS dosis_5,
                        'AGUA' AS agua,
                        dosis_agua AS dosis_6
                    FROM `cycle_application_reporte` reporte
                    INNER JOIN `cycle_application_reporte_sectores` sectores ON reporte.id = sectores.`id_cycle_application_reporte`
                    WHERE finca IN ({$this->available_fincas}) $sWhere
                    ORDER BY id DESC";
            $response->table1 = $this->db->queryAll($sql);

            $sql = "SELECT 
                        reporte.id,
                        reporte.finca,
                        reporte.fumigadora,
                        reporte.piloto,
                        reporte.placa_avion as placa,
                        reporte.hora_salida,
                        reporte.hora_llegada,
                        reporte.motivo as motivo_atraso,
                        reporte.notificacion as notificada,
                        sectores.sector as sector_aplicado,
                        reporte.observaciones
                    FROM `cycle_application_reporte` reporte
                    INNER JOIN `cycle_application_reporte_sectores` sectores ON reporte.id = sectores.`id_cycle_application_reporte`
                    WHERE finca IN ({$this->available_fincas}) $sWhere
                    ORDER BY id DESC";
            $response->table2 = $this->db->queryAll($sql);

            $sql = "SELECT 
                        reporte.id, 
                        reporte.finca, 
                        sectores.sector, 
                        reporte.hectareas_fumigacion AS ha, 
                        reporte.fecha_real as fecha, 
                        (SELECT producto FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1) AS fungicida,
                        (SELECT dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1) AS 'DOSIS 1',
                        (SELECT producto FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1,1) AS fungicida_2,
                        (SELECT dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 4 ORDER BY id LIMIT 1,1) AS 'DOSIS 2',
                        (SELECT producto FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 1 ORDER BY id LIMIT 1) AS 'BIOESTIMULANTE',
                        (SELECT dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 1 ORDER BY id LIMIT 1) AS 'DOSIS 3',
                        (SELECT producto FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1) AS 'COADYUVANTE',
                        (SELECT dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 2 ORDER BY id LIMIT 1) AS 'DOSIS 4',
                        (SELECT producto FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 6 ORDER BY id LIMIT 1) AS aceite,
                        (SELECT dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 6 ORDER BY id LIMIT 1) AS 'DOSIS 5',
                        'AGUA' AS agua,
                        (SELECT dosis FROM cycle_application_reporte_productos WHERE id_cycle_application_reporte = reporte.id AND id_tipo_producto = 6 ORDER BY id LIMIT 1) AS 'DOSIS 6',
                        reporte.fumigadora, 
                        reporte.piloto, 
                        reporte.placa_avion AS 'PLACA', 
                        reporte.hora_salida AS 'HORA SALIDA', 
                        reporte.hora_llegada AS 'HORA LLEGADA', 
                        reporte.motivo AS 'MOTIVO ATRASO', 
                        reporte.notificacion, 
                        reporte.tipo_ciclo
                    FROM `cycle_application_reporte` reporte
                    INNER JOIN `cycle_application_reporte_sectores` sectores ON reporte.id = sectores.`id_cycle_application_reporte`
                    WHERE finca IN ({$this->available_fincas}) $sWhere
                    ORDER BY id DESC";
            $response->table_to_export = $this->db->queryAll($sql);

            $response->semanas = $this->db->queryAllSpecial("SELECT semana AS id , semana AS label FROM cycle_application_reporte WHERE semana > 0 GROUP BY semana");
			$response->ciclos = $this->db->queryAllSpecial("SELECT num_ciclo AS id , num_ciclo AS label FROM cycle_application_reporte WHERE num_ciclo > 0 GROUP BY num_ciclo");
            
            return json_encode($response);
        }
	}
?>
