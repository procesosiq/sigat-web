<?php
	/**
	*  CLASS FROM PRODUCTOS
	*/
	class TecnicoDemo
	{
		private $db;
		private $session;
		
		public function __construct()
		{
			$this->db = new M_Conexion();
            $this->session = Session::getInstance();
            $available = $this->db->queryRow("SELECT GROUP_CONCAT(CONCAT(\"'\", f.nombre, \"'\") SEPARATOR ',') AS fincas, GROUP_CONCAT(f.id SEPARATOR ',') AS ids
                FROM cat_usuarios u
                INNER JOIN users_fincas_privileges ON id_user = u.id
                INNER JOIN cat_fincas f ON id_finca = f.`id`
                WHERE u.tipo = 'CICLOS' AND u.id = {$this->session->id}");
            $this->available_fincas = $available->fincas;
            $this->available_id_fincas = $available->ids;
		}
		
		private function getFilters(){
			$data = (object)json_decode(file_get_contents("php://input"));
			$response = new stdClass;
            $response->semana = (int)$data->sem;
            $response->semana2 = (int)$data->sem2;
			return $response;
		}


		public function index(){
			$data = (object)json_decode(file_get_contents("php://input"));
			$response = new stdClass;
			$filters = $this->getFilters();
            $sWhere = "";
            $sWhere2 = "";
			if($filters->semana > 0){
                $sWhere .= " AND semana >= '{$filters->semana}'";
                $sWhere2 .= " AND WEEK(c.fecha) >= {$filters->semana}";
            }
            if($filters->semana2 > 0){
                $sWhere .= " AND semana <= '{$filters->semana2}'";
                $sWhere2 .= " AND WEEK(c.fecha) <= {$filters->semana2}";
            }

            $sql = "SELECT *, (SELECT alias_2 FROM cat_fincas WHERE nombre = finca AND status = 1) AS finca FROM resumen_ciclo WHERE finca IN ({$this->available_fincas}) {$sWhere} ORDER BY id DESC";
			$response->table1 = $this->db->queryAll($sql);

			$sql = "SELECT r.*, (SELECT alias_2 FROM cat_fincas WHERE nombre = finca AND status = 1) AS finca FROM resumen_ciclo_2 r INNER JOIN cycle_application c ON c.id = id_json WHERE finca IN ({$this->available_fincas}) {$sWhere2} ORDER BY r.id DESC";
            $response->table2 = $this->db->queryAll($sql);
            
            $sql = "SELECT c.id, (SELECT alias_2 FROM cat_fincas WHERE nombre = f.nombre AND status = 1) AS finca, c.sector, hectareas_fumigacion AS ha, c.fecha, 
                        fungicida, dosis AS 'DOSIS 1', fungicida_2 AS 'FUNGICIDA 2', dosis_2 AS 'DOSIS 2', 
                        bioestimulante_1 AS 'BIOESTIMULANTE', dosis_bioestimulante_1 AS 'DOSIS 3', 
                        emulsificante_1 AS 'COADYUVANTE', dosis_emulsificante_1 AS 'DOSIS 4',
                        aceite, dosis_aceite AS 'DOSIS 5', agua, dosis_agua AS 'DOSIS 6', fumigadora, p.`nombre` AS piloto, 
                        placa_avion AS 'PLACA', hora_salida AS 'HORA SALIDA', hora_llegada AS 'HORA LLEGADA', motivo AS 'MOTIVO ATRASO', 
                        notificacion, tipo_ciclo
                FROM cycle_application c 
                INNER JOIN cat_fincas f ON id_hacienda = f.`id`
                INNER JOIN cat_pilotos p ON id_piloto = p.`id`
                WHERE id_hacienda IN ({$this->available_id_fincas}) {$sWhere2}";
            $response->table_to_export = $this->db->queryAll($sql);

			$response->semanas = $this->db->queryAllSpecial("SELECT semana AS id , semana AS label FROM resumen_ciclo WHERE semana > 0 GROUP BY semana");
			$response->ciclos = $this->db->queryAllSpecial("SELECT ciclo AS id , ciclo AS label FROM resumen_ciclo WHERE ciclo > 0 GROUP BY ciclo");

			return json_encode($response);
		}
	}
?>
