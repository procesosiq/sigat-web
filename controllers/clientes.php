<?php

class clientes { 
     
    private $conexion;
    
    public function __construct(){
        $this->conexion = new M_Conexion;
    }
    
    public function index(){

        $sWhere = "";
        $sOrder = " ORDER BY nombre";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        // print_r($_POST);
        if(isset($_POST)){

                /*----------  ORDER BY ----------*/
                
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY cat_clientes.id {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY cat_clientes.fecha {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY cat_clientes.nombre {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY id_tipcli {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY (SELECT COUNT(*) FROM cat_sucursales WHERE id_cliente=cat_clientes.id) {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY COUNT(cat_equipos.id) {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY cat_clientes.status {$DesAsc}";
                }
                /*----------  ORDER BY ----------*/

                if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                    $sWhere .= " AND cat_clientes.id = ".$_POST["order_id"];
                }               
                if((isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != "") && (isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != "")){
                    $sWhere .= " AND cat_clientes.fecha BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
                }
                if(isset($_POST['order_customer_name']) && trim($_POST['order_customer_name']) != ""){
                    $sWhere .= " AND nombre LIKE '%".$_POST['order_customer_name']."%'";
                }
                if(isset($_POST['order_ship_to']) && trim($_POST['order_ship_to']) != ""){
                    $sWhere .= " AND (SELECT nombre FROM cat_clientesTipo WHERE id=id_tipcli)  LIKE '%".$_POST['order_ship_to']."%'";
                }
                if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                    $sWhere .= " AND cat_clientes.status = ".$_POST["order_status"];
                }

                /*----------  LIMIT  ----------*/
                if(isset($_POST['length']) && $_POST['length'] > 0){
                    $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
                }
            }

        $sql = "SELECT cat_clientes.id,DATE_FORMAT(cat_clientes.fecha,'%d/%m/%Y') AS fecha,nombre,
        (SELECT nombre FROM cat_clientesTipo WHERE id=id_tipcli) AS tipo_cliente,
        (SELECT COUNT(*) FROM cat_sucursales WHERE id_cliente=cat_clientes.id) AS totSucursal,sucursales,
        COUNT(cat_equipos.id) AS totEquipos,IF(cat_clientes.STATUS=1,'Activo','Inactivo') AS edocli
        FROM cat_clientes
        LEFT JOIN cat_equipos ON cat_clientes.id = id_cliente
        WHERE 1=1 $sWhere 
        GROUP BY cat_clientes.id
        $sOrder $sLimit";
        // print $sql;
        $res = $this->conexion->link->query($sql);
        /*$datos = (object) array(
            "customActionMessage" => "Error al consultar la informacion",
            "customActionStatus" => "Error",
            "data" => [],
            "draw" => 0,
            "recordsFiltered" => 0,
            "recordsTotal" => 0,
        );*/
        while($fila = $res->fetch_assoc()){
            $fila = (object)$fila;
            
            $botonn="";
            if($fila->sucursales=='SI'){
                $botonn='<button id="sucu" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-plus"></i> Sucursales</button>';
            }
            else{
                $botonn='<button id="equi" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-plus"></i> Equipos</button>';
            }
            
            $datos->data[] = array (
                '<input type="checkbox" name="id[]" value="'.$fila->id.'">',
                $fila->id,
                $fila->fecha,
                $fila->nombre,
                $fila->tipo_cliente,
                $fila->totSucursal,
                $fila->totEquipos,
                ($fila->edocli == 'Inactivo') ? '<button id="status" class="btn btn-sm red-thunderbird">'.$fila->edocli.'</button>' : '<button class="btn btn-sm green-jungle" id="status">'.$fila->edocli.'</button>',
                '<button id="edit" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-plus"></i> Editar</button>',
                $botonn
            );
        }

        $datos->recordsTotal = count($datos->data);
        #$datos->customActionMessage = "Informacion completada con exito";
        $datos->customActionStatus = "OK";

        return json_encode($datos);
    }
    
    public function AddCliente(){
        $datos = (object)$_POST;
        
        $fec1 = $datos->txtfec;
		$f1 = substr($fec1,6,4)."-".substr($fec1,0,2)."-".substr($fec1,3,2);
        
        $sql="INSERT INTO cat_clientes SET nombre='$datos->txtnom',email='$datos->txtemail',id_tipcli='$datos->s_tipocli',
        sucursales='$datos->s_sucursales',razon_social='$datos->txtrazon',ruc='$datos->txtruc',direccion='$datos->txtdircli',
        telefono='$datos->txttel',ciudad='$datos->txtciudad',fecha=CURRENT_DATE , telefono_contacto = '$datos->txttel_contacto',
        nombre_contacto = '$datos->txtnom_contacto',cargo='$datos->txtCargo'";
        $ids = $this->conexion->Consultas(1,$sql);
        echo $ids;
    }
    
    public function UpdateCliente(){
        $datos = (object)$_POST;
        
        $sql="UPDATE cat_clientes SET nombre='$datos->txtnom',email='$datos->txtemail',id_tipcli='$datos->s_tipocli',
        sucursales='$datos->s_sucursales',razon_social='$datos->txtrazon',ruc='$datos->txtruc',direccion='$datos->txtdircli',
        telefono='$datos->txttel',ciudad='$datos->txtciudad',telefono_contacto = '$datos->txttel_contacto',
        nombre_contacto = '$datos->txtnom_contacto',cargo='$datos->txtCargo' WHERE id='$datos->idcli'";
        $this->conexion->Consultas(1,$sql);
        echo true;
    }

    public function ChangeStatus(){
        $datos = (object)$_POST;
        
        $sql="UPDATE cat_clientes SET status=IF(status , 0 , 1) WHERE id='$datos->idcli'";
        $this->conexion->Consultas(1,$sql);
        echo true;
    }

    public function addContact(){
        $datos = (object)$_POST;
        $sql="INSERT INTO cat_clientes_contactos SET id_cliente='$data->id' , nombre='$datos->nombre',correo='$datos->correo',cargo='$datos->cargo',
        telefono='$datos->telefono',area='$datos->area'";
        $ids = $this->conexion->Consultas(1,$sql);

        return json_encode($this->getContact($data->id));
    }

    public function getContact($id){
        $datos = [];
        if($id > 0){
            $sql="SELECT * FROM cat_clientes_contactos WHERE id_cliente = $id";
            $res = $this->conexion->link->query($sql);
            while($fila = $res->fetch_assoc()){
                $datos[] = (object) $fila;
            }
        }
        return $datos;
    }
    public function GetAreas(){
        $sql="SELECT * FROM cat_areas WHERE status = 1;";
        $res = $this->conexion->link->query($sql);
        while($fila = $res->fetch_assoc()){
            $datos[] = (object) $fila;
        }
        return $datos;
    }
    
    public function GetCliente($id){
        $sql = "SELECT nombre,email,DATE_FORMAT(cat_clientes.fecha,'%m/%d/%Y') AS fecha,id_tipcli,sucursales,
        razon_social,ruc,direccion,telefono,cargo,ciudad, COUNT(cat_equipos.id) AS equipos,nombre_contacto,telefono_contacto
        FROM cat_clientes
        LEFT JOIN cat_equipos ON cat_clientes.id = id_cliente
        WHERE cat_clientes.id='$id'
        GROUP BY id_cliente";
        $res = $this->conexion->link->query($sql);
        $datosCli = array();
        if($fila = $res->fetch_assoc()){
             $datosCli = (object)$fila;
        }
        return $datosCli;
    }
}
?>