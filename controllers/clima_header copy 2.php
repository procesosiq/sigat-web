<?php

if($page != "index"){
	include 'conexion.php';
}
class Datos_TOP{
	
	private $conexion;
	private $session;
	
	public function __construct() {
        $this->conexion = new M_Conexion();
        $this->session = Session::getInstance();
    }

	private function ClimaTop(){
		$sql="SELECT YEAR(fecha) AS anoo,WEEK(fecha) AS numsemana,
		MIN(temp_minima) AS Tmin,MAX(temp_maxima) AS Tmax,
		SUM(lluvia) AS Tlluvia,AVG(rad_solar) AS rad_solar
		FROM datos_clima 
		WHERE YEAR(fecha) 
		AND id_hacienda = '{$this->session->finca}' 
		AND id_usuario = '{$this->session->logged}' 
		AND id_cliente = '{$this->session->client}'
		GROUP BY numsemana
		ORDER BY anoo DESC,numsemana DESC LIMIT 1";
		$res = $this->conexion->link->query($sql);
		$datos = array();
		while($fila = $res->fetch_assoc()){
			$datos[] = $fila;
		}
		return $datos;
	}

	public function Clima_Top(){ 
		$datos = array();
		$datos = $this->ClimaTop();
		return $datos;
	}
}

?>