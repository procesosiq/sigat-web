<?php
include 'conexion.php';
class Graficas_M{
	
	private $conexion;
	
	public function __construct() {
        $this->conexion = new M_Conexion();
    }
	
	public function Grafica_clima($tipo){ 
		$datos = array();
		$datos = $this->conexion->ConsultaClimas($tipo);
		return json_encode($datos);
	}
}

$postdata = (object)json_decode(file_get_contents("php://input"));
if($postdata->opt == "TEMMIN"){ 
	$retval = new Graficas_M();
	echo $retval->Grafica_clima("TEM_MIN");
}
else if($postdata->opt == "TEMMAX"){ 
	$retval = new Graficas_M();
	echo $retval->Grafica_clima("TEM_MAX");
}
else if($postdata->opt == "LLUVIA"){ 
	$retval = new Graficas_M();
	echo $retval->Grafica_clima("LLUVIA");
}
else if($postdata->opt == "RADSOLAR"){ 
	$retval = new Graficas_M();
	echo $retval->Grafica_clima("RAD_SOLAR");
}
else if($postdata->opt == "DIASSOL"){ 
	$retval = new Graficas_M();
	echo $retval->Grafica_clima("DIAS_SOL");
}
else if($postdata->opt == "HUMMIN"){ 
	$retval = new Graficas_M();
	echo $retval->Grafica_clima("HUM_MIN");
}
else if($postdata->opt == "HUMMAX"){ 
	$retval = new Graficas_M();
	echo $retval->Grafica_clima("HUM_MAX");
}
?>