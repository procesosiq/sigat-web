<?php
include 'class.sesion.php';
include 'conexion.php';
class Graficas_M{
	
	private $conexion;
	private $link;
	
	public function __construct() {
        $this->conexion = new M_Conexion();
        $this->link = $this->conexion->link;
        $this->session = Session::getInstance();
    }
	
	public function Grafica_clima($tipo){ 
		$datos = array();
		$datos = $this->ConsultaClimas($tipo);
		return json_encode($datos);
	}

	public function Grafica_climaToken($token, $tipo){ 
		$datos = array();
		$datos = $this->ConsultaClimasToken($token, $tipo);
		return json_encode($datos);
	}

	public function ConsultaClimasToken($token,$tipo){
		$sql_get_info = "SELECT * FROM informe WHERE token = '{$token}'";
    	$result = $this->conexion->Consultas(2, $sql_get_info);
    	$id_usuario = $result[0]["id_usuario"];
    	$id_cliente = $result[0]["id_cliente"];
    	$id_finca = $result[0]["id_hacienda"];
			$sql = "SELECT YEAR(fecha) AS anoo 
				FROM datos_clima 
				WHERE
				YEAR(fecha) > 0 
				AND id_hacienda = '{$id_finca}' 
				AND id_usuario = '{$id_usuario}' 
				AND id_cliente = '{$id_cliente}'
				GROUP BY YEAR(fecha) ORDER BY anoo";
			$res = $this->link->query($sql);
			$datos = array();
			$min = [];
			$max = [];
			while($fila = $res->fetch_assoc()){
				
				if($tipo=='TEM_MIN'){ #TEMPERATURA MINIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MIN(temp_minima) AS Tmin
					FROM datos_clima 
					WHERE YEAR(fecha)='{$fila['anoo']}'
					AND id_hacienda = '{$id_finca}' 
					AND id_usuario = '{$id_usuario}' 
					AND id_cliente = '{$id_cliente}'
					AND temp_minima > 0
					GROUP BY numsemana
					ORDER BY numsemana";
					$datos["0"][] = [0 , 20.5];
					$datos["0"][] = [52 , 20.5];
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Tmin"]);
						$min[] = $fila2["Tmin"];
						$max[] = $fila2["Tmin"];
					}
				}
				else if($tipo=='TEM_MAX'){ #TEMPERATURA MAXIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MAX(temp_maxima) AS Tmax
					FROM datos_clima 
					WHERE YEAR(fecha)='{$fila['anoo']}'
					AND id_hacienda = '{$id_finca}' 
					AND id_usuario = '{$id_usuario}' 
					AND id_cliente = '{$id_cliente}'
					AND temp_maxima > 0
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Tmax"]);
						$min[] = $fila2["Tmax"];
						$max[] = $fila2["Tmax"];

					}
				}
				else if($tipo=='LLUVIA'){ #DATOS DE LLUVIA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					SUM(lluvia) AS totlluvia
					FROM datos_clima 
					WHERE YEAR(fecha)='{$fila['anoo']}'
					AND id_hacienda = '{$id_finca}' 
					AND id_usuario = '{$id_usuario}' 
					AND id_cliente = '{$id_cliente}'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["totlluvia"]);
						$min[] = $fila2["totlluvia"];
						$max[] = $fila2["totlluvia"];

					}
				}
				else if($tipo=='RAD_SOLAR'){ #RADIACION SOLAR
					$sql2="SELECT WEEK(fecha) AS numsemana,(SUM(rad_solar)/COUNT(*)) AS promedio
					FROM datos_clima 
					WHERE YEAR(fecha)='{$fila['anoo']}'
					AND id_hacienda = '{$id_finca}' 
					AND id_usuario = '{$id_usuario}' 
					AND id_cliente = '{$id_cliente}'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["promedio"]);
						$min[] = $fila2["promedio"];
						$max[] = $fila2["promedio"];

					}
				}
				else if($tipo=='DIAS_SOL'){ #RADIACION SOLAR
					$sql2="SELECT WEEK(fecha) AS numsemana,(SUM(dias_sol)/COUNT(*)) AS promedio
					FROM datos_clima 
					WHERE YEAR(fecha)='{$fila['anoo']}'
					AND id_hacienda = '{$id_finca}' 
					AND id_usuario = '{$id_usuario}' 
					AND id_cliente = '{$id_cliente}'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["promedio"]);
						$min[] = $fila2["promedio"];
						$max[] = $fila2["promedio"];

					}
				}
				if($tipo=='HUM_MIN'){ #HUMEDAD MINIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MIN(humedad) AS Hmin
					FROM datos_clima 
					WHERE YEAR(fecha)='{$fila['anoo']}'
					AND id_hacienda = '{$id_finca}' 
					AND id_usuario = '{$id_usuario}' 
					AND id_cliente = '{$id_cliente}'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Hmin"]);
						$min[] = $fila2["Hmin"];
						$max[] = $fila2["Hmin"];

					}
				}
				else if($tipo=='HUM_MAX'){ #HUMEDAD MAXIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MAX(humedad) AS Hmax
					FROM datos_clima 
					WHERE YEAR(fecha)='{$fila['anoo']}'
					AND id_hacienda = '{$id_finca}' 
					AND id_usuario = '{$id_usuario}' 
					AND id_cliente = '{$id_cliente}'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Hmax"]);
						$min[] = $fila2["Hmax"];
						$max[] = $fila2["Hmax"];

					}
				}
			}
			$response = (object)[];
			$response->datos = (array)$datos;
			$response->max = max($max);
			$response->min = is_null(min($min))?0:min($min);

			return (array)$response;
	} 
	
	public function ConsultaClimas($tipo){
			$sql = "SELECT YEAR(fecha) AS anoo 
				FROM datos_clima 
				WHERE
				YEAR(fecha) > 0 
				AND id_hacienda = '{$this->session->finca}' 
				AND id_usuario = '{$this->session->logged}' 
				AND id_cliente = '{$this->session->client}'
				GROUP BY YEAR(fecha) ORDER BY anoo";
			$res = $this->link->query($sql);
			$datos = array();
			$min = [];
			$max = [];
			while($fila = $res->fetch_assoc()){
				
				if($tipo=='TEM_MIN'){ #TEMPERATURA MINIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MIN(temp_minima) AS Tmin
					FROM datos_clima 
					WHERE YEAR(fecha)='{$fila['anoo']}'
					AND id_hacienda = '{$this->session->finca}' 
					AND id_usuario = '{$this->session->logged}' 
					AND id_cliente = '{$this->session->client}'
					AND temp_minima > 0
					GROUP BY numsemana
					ORDER BY numsemana";
					$datos["0"][] = [0 , 20.5];
					$datos["0"][] = [52 , 20.5];
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Tmin"]);
						$min[] = $fila2["Tmin"];
						$max[] = $fila2["Tmin"];
					}
				}
				else if($tipo=='TEM_MAX'){ #TEMPERATURA MAXIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MAX(temp_maxima) AS Tmax
					FROM datos_clima 
					WHERE YEAR(fecha)='{$fila['anoo']}'
					AND id_hacienda = '{$this->session->finca}' 
					AND id_usuario = '{$this->session->logged}' 
					AND id_cliente = '{$this->session->client}'
					AND temp_maxima > 0
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Tmax"]);
						$min[] = $fila2["Tmax"];
						$max[] = $fila2["Tmax"];
					}
				}
				else if($tipo=='LLUVIA'){ #DATOS DE LLUVIA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					SUM(lluvia) AS totlluvia
					FROM datos_clima 
					WHERE YEAR(fecha)='{$fila['anoo']}'
					AND id_hacienda = '{$this->session->finca}' 
					AND id_usuario = '{$this->session->logged}' 
					AND id_cliente = '{$this->session->client}'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["totlluvia"]);
						$min[] = $fila2["totlluvia"];
						$max[] = $fila2["totlluvia"];
					}
				}
				else if($tipo=='RAD_SOLAR'){ #RADIACION SOLAR
					$sql2="SELECT WEEK(fecha) AS numsemana,(SUM(rad_solar)/COUNT(*)) AS promedio
					FROM datos_clima 
					WHERE YEAR(fecha)='{$fila['anoo']}'
					AND id_hacienda = '{$this->session->finca}' 
					AND id_usuario = '{$this->session->logged}' 
					AND id_cliente = '{$this->session->client}'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["promedio"]);
						$min[] = $fila2["promedio"];
						$max[] = $fila2["promedio"];
					}
				}
				else if($tipo=='DIAS_SOL'){ #RADIACION SOLAR
					$sql2="SELECT WEEK(fecha) AS numsemana,(SUM(dias_sol)/COUNT(*)) AS promedio
					FROM datos_clima 
					WHERE YEAR(fecha)='{$fila['anoo']}'
					AND id_hacienda = '{$this->session->finca}' 
					AND id_usuario = '{$this->session->logged}' 
					AND id_cliente = '{$this->session->client}'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["promedio"]);
						$min[] = $fila2["promedio"];
						$max[] = $fila2["promedio"];
					}
				}
				if($tipo=='HUM_MIN'){ #HUMEDAD MINIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MIN(humedad) AS Hmin
					FROM datos_clima 
					WHERE YEAR(fecha)='{$fila['anoo']}'
					AND id_hacienda = '{$this->session->finca}' 
					AND id_usuario = '{$this->session->logged}' 
					AND id_cliente = '{$this->session->client}'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Hmin"]);
						$min[] = $fila2["Hmin"];
						$max[] = $fila2["Hmin"];
					}
				}
				else if($tipo=='HUM_MAX'){ #HUMEDAD MAXIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MAX(humedad) AS Hmax
					FROM datos_clima 
					WHERE YEAR(fecha)='{$fila['anoo']}'
					AND id_hacienda = '{$this->session->finca}' 
					AND id_usuario = '{$this->session->logged}' 
					AND id_cliente = '{$this->session->client}'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Hmax"]);
						$min[] = $fila2["Hmax"];
						$max[] = $fila2["Hmax"];
					}
				}
			}

			$response = (object)[];
			$response->datos = (array)$datos;
			$response->max = max($max);
			$response->min = is_null(min($min))?0:min($min);

			return (array)$response;
	} 
}

$postdata = (object)json_decode(file_get_contents("php://input"));
if($postdata->opt == "TEMMIN"){ 
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_M();
			echo $retval->Grafica_climaToken($postdata->token, "TEM_MIN");
		}	
	}else{
		$retval = new Graficas_M();
		echo $retval->Grafica_clima("TEM_MIN");
	}
}
else if($postdata->opt == "TEMMAX"){ 
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_M();
			echo $retval->Grafica_climaToken($postdata->token, "TEM_MAX");
		}	
	}else{
		$retval = new Graficas_M();
		echo $retval->Grafica_clima("TEM_MAX");
	}
}
else if($postdata->opt == "LLUVIA"){
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_M();
			echo $retval->Grafica_climaToken($postdata->token, "LLUVIA");
		}	
	}else{
		$retval = new Graficas_M();
		echo $retval->Grafica_clima("LLUVIA");
	}
}
else if($postdata->opt == "RADSOLAR"){ 
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_M();
			echo $retval->Grafica_climaToken($postdata->token, "RAD_SOLAR");
		}	
	}else{
		$retval = new Graficas_M();
		echo $retval->Grafica_clima("RAD_SOLAR");
	}
}
else if($postdata->opt == "DIASSOL"){ 
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_M();
			echo $retval->Grafica_climaToken($postdata->token, "DIAS_SOL");
		}	
	}else{
		$retval = new Graficas_M();
		echo $retval->Grafica_clima("DIAS_SOL");
	}
}
else if($postdata->opt == "HUMMIN"){ 
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_M();
			echo $retval->Grafica_climaToken($postdata->token, "HUM_MIN");
		}	
	}else{
		$retval = new Graficas_M();
		echo $retval->Grafica_clima("HUM_MIN");
	}
}
else if($postdata->opt == "HUMMAX"){ 
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_M();
			echo $retval->Grafica_climaToken($postdata->token, "HUM_MAX");
		}	
	}else{
		$retval = new Graficas_M();
		echo $retval->Grafica_clima("HUM_MAX");
	}
}
?>