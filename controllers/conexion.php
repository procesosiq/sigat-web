<?php

// include 'class.sesion.php';

#global 	$session = Session::getInstance();

class M_Conexion { 
	private $host="localhost";
    private $db_user="auditoriasbonita";
    private $db_pass="u[V(fTIUbcVb";
    private $db_name="sigat";
    public $link; 

    public $insert = 1;
	public $selects = 2;

    public function __construct($db = ""){
        if($db != "")
            $this->db_name = trim($db);

        $this->link = new mysqli($this->host, $this->db_user, $this->db_pass, $this->db_name);
        if ( $this->link->connect_errno ) 
        { 
            echo "Fallo al conectar a MySQL: ". $this->link->connect_error; 
            return;
        }
        $this->link->set_charset("utf8");
    }

    public function query($sql){
    	if($sql != ""){
    		$this->link->query($sql);
    	}
    }

	public function getLastID(){
		return $this->link->insert_id;
	}

	public function queryAll($sql = "", $convertArray = false){
		$response = [];
		if($sql != ""){
			if($res = $this->link->query($sql)){
                while($row = $res->fetch_assoc()){
                    if($convertArray)
                        $response[] = (array) $row;
                    else
                        $response[] = (object) $row;
                }
            }else{
                print_r([
                    "sql" => $sql,
                    "error" => $this->link->error
                ]);
            }
		}
		return $response;
    }
    
    public function queryAllOne($sql = "", $convertArray = false){
		$response = [];
		if($sql != ""){
			if($res = $this->link->query($sql)){
                while($row = $res->fetch_row()){
                    if($convertArray)
                        $response[] = $row[0];
                    else
                        $response[] = $row[0];
                }
            }else{
                print_r([
                    "sql" => $sql,
                    "error" => $this->link->error
                ]);
            }
		}
		return $response;
	}

	public function queryRow($sql = "", $convertArray = false){
		$response = new stdClass;
		if($sql != ""){
			$res = $this->link->query($sql);
			while($row = $res->fetch_assoc()){
				if($convertArray)
					$response = (array) $row;
				else
					$response = (object) $row;
				break;
			}
		}
		return $response;
	}

	public function queryAllSpecial($sql){
		$response = [];
		if($sql != ""){
			if($res = $this->link->query($sql)){
                while($row = $res->fetch_assoc()){
                    $row = (object) $row;
                    if(isset($row->id) && isset($row->label))
                        $response[$row->id] = $row->label;
                }
            }else{
                print_r([
                    "sql" => $sql,
                    "error" => $this->link->error
                ]);
            }
		}
		return $response;
    }
    
    public function queryOne($sql){
        $response = null;
        if($sql != ""){
            if($res = $this->link->query($sql)){
                if($row = $res->fetch_assoc()){
                    return $row[array_keys($row)[0]];
                }
            }else{
                print_r([
                    "sql" => $sql,
                    "error" => $this->link->error
                ]);
            }
        }
        return $response;
    }
	
	public function Consultas($tipo,$sql){
		if($tipo == 1){
			if($this->link->query($sql)){
				$ids=$this->link->insert_id;
				return $ids;
			}else{
				print_r($this->link->error);
			}
		}
		else if($tipo == 2){
			$res = $this->link->query($sql);
			$datos = array();
			while($fila = $res->fetch_assoc()){
				$datos[] = $fila;
			}
			return $datos;
		}
	}
	
	public function ClimaTop(){
		$sql="SELECT YEAR(fecha) AS anoo,WEEK(fecha) AS numsemana,
		MIN(temp_minima) AS Tmin,MAX(temp_maxima) AS Tmax,
		SUM(lluvia) AS Tlluvia,AVG(rad_solar) AS rad_solar
		FROM datos_clima 
		WHERE YEAR(fecha)
		GROUP BY numsemana
		ORDER BY anoo DESC,numsemana DESC LIMIT 1";
		$res = $this->link->query($sql);
		$datos = array();
		while($fila = $res->fetch_assoc()){
			$datos[] = $fila;
		}
		return $datos;
	}

	public function ConsultasTresMetros($tipo){
		$sql = "SELECT YEAR(fecha) AS anoo FROM muestras_haciendas_3M GROUP BY YEAR(fecha) ORDER BY anoo";
			$res = $this->link->query($sql);
			$datos = array();
			while($fila = $res->fetch_assoc()){
				
				if($tipo=='HVMLE_HVLE'){ 
					$sql2="
					SELECT 
					WEEK(fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(total_hoja_3) AS hojaViejaLibreEstrias

					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(fecha) = '".$fila['anoo']."' 
					GROUP BY numsemana 
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreEstrias"]);
					}
				}
			}
	}
				

	public function ConsultasCeroSemanas($tipo,$tipo_semana , $id_cliente){
		$sql = "SELECT YEAR(prin.fecha) AS anoo , foco 
					FROM muestras_hacienda_detalle as det
					INNER JOIN muestras_haciendas as prin on det.id_Mhacienda = prin.id
				WHERE YEAR(prin.fecha) > 0 
				AND prin.id_usuario = '{$this->session->logged}' 
				AND prin.id_cliente =  '{$this->session->client}'
				GROUP BY foco ,YEAR(fecha) 
				ORDER BY anoo";
			$res = $this->link->query($sql);
			$datos = array();
			while($fila = $res->fetch_assoc()){
				
				if($tipo=='HMVLDQMEN5%'){ 
					$sql2="
					SELECT 
					WEEK(fecha) AS numsemana,
					COUNT(*) AS numreg,
					foco,
					AVG(hoja_mas_vieja_libre_quema_menor) AS hojaViejaLibreQuemaMenor
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
					WHERE YEAR(fecha) = '".$fila['anoo']."' AND tipo_semana = $tipo_semana
					AND muestras_haciendas.id_usuario = '{$this->session->logged}' AND muestras_haciendas.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY foco,numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMenor"]);
					}
				}
				if($tipo=="HMVLDE"){
					$sql2="
					SELECT 
					WEEK(fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(hojas_mas_vieja_libre) AS hojaViejaLibreEstrias,
					foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(fecha) = '".$fila['anoo']."' AND tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario = '{$this->session->logged}' AND muestras_haciendas.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY foco,numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreEstrias"]);
					}
				}
				if($tipo=="LIB_DE_CIRUG"){
					$sql2="
					SELECT 
					WEEK(fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(libre_cirugias) AS hojaLibreCirugias,foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(fecha) = '".$fila['anoo']."' AND tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario = '{$this->session->logged}' AND muestras_haciendas.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY foco,numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaLibreCirugias"]);
					}
				}
				if($tipo=="HMVLDQMAY5"){
					$sql2="
					SELECT 
					WEEK(fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(hoja_mas_vieja_libre_quema_mayor) AS hojaViejaLibreQuemaMayor,
					foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(fecha) = '".$fila['anoo']."' AND tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario = '{$this->session->logged}' AND muestras_haciendas.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY foco,numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMayor"]);
					}
				}
				if($tipo=="HOJTOT"){
					$sql2="
					SELECT 
					WEEK(fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(hojas_totales) AS hojasTotales,foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(fecha) = '".$fila['anoo']."' AND tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario = '{$this->session->logged}' AND muestras_haciendas.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY foco,numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojasTotales"]);
					}
				}
			}
			return $datos;
	}
	
	public function ConsultaClimas($tipo){
			$sql = "SELECT YEAR(fecha) AS anoo FROM datos_clima GROUP BY YEAR(fecha) ORDER BY anoo";
			$res = $this->link->query($sql);
			$datos = array();
			while($fila = $res->fetch_assoc()){
				
				if($tipo=='TEM_MIN'){ #TEMPERATURA MINIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MIN(temp_minima) AS Tmin
					FROM datos_clima 
					WHERE YEAR(fecha)='".$fila['anoo']."'
					GROUP BY numsemana
					ORDER BY numsemana";
					$datos["umbral"][] = [0 , 20.5];
					$datos["umbral"][] = [52 , 20.5];
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Tmin"]);
					}
				}
				else if($tipo=='TEM_MAX'){ #TEMPERATURA MAXIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MAX(temp_maxima) AS Tmax
					FROM datos_clima 
					WHERE YEAR(fecha)='".$fila['anoo']."'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Tmax"]);
					}
				}
				else if($tipo=='LLUVIA'){ #DATOS DE LLUVIA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					SUM(lluvia) AS totlluvia
					FROM datos_clima 
					WHERE YEAR(fecha)='".$fila['anoo']."'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["totlluvia"]);
					}
				}
				else if($tipo=='RAD_SOLAR'){ #RADIACION SOLAR
					$sql2="SELECT WEEK(fecha) AS numsemana,(SUM(rad_solar)/COUNT(*)) AS promedio
					FROM datos_clima 
					WHERE YEAR(fecha)='".$fila['anoo']."'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["promedio"]);
					}
				}
				else if($tipo=='DIAS_SOL'){ #RADIACION SOLAR
					$sql2="SELECT WEEK(fecha) AS numsemana,(SUM(dias_sol)/COUNT(*)) AS promedio
					FROM datos_clima 
					WHERE YEAR(fecha)='".$fila['anoo']."'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["promedio"]);
					}
				}
				if($tipo=='HUM_MIN'){ #HUMEDAD MINIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MIN(humedad) AS Hmin
					FROM datos_clima 
					WHERE YEAR(fecha)='".$fila['anoo']."'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Hmin"]);
					}
				}
				else if($tipo=='HUM_MAX'){ #HUMEDAD MAXIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MAX(humedad) AS Hmax
					FROM datos_clima 
					WHERE YEAR(fecha)='".$fila['anoo']."'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Hmax"]);
					}
				}
			}
			return $datos;
	} 
	
}

?>