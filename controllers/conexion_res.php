<?php

#include 'class.sesion.php';

#global 	$session = Session::getInstance();

class M_Conexion { 
	private $host="localhost";
    private $db_user="auditoriasbonita";
    private $db_pass="u[V(fTIUbcVb";
    private $db_name="sigat";
    protected $link; 

    public $insert = 1;
	public $selects = 2;

    public function __construct(){ 
        $this->link = new mysqli($this->host, $this->db_user, $this->db_pass, $this->db_name);

        if ( $this->link->connect_errno ) 
        { 
            echo "Fallo al conectar a MySQL: ". $this->link->connect_error; 
            return;
        }
    } 
	
	public function Consultas($tipo,$sql){
		if($tipo == 1){
			if($this->link->query($sql)){
				$ids=$this->link->insert_id;
				return $ids;
			}else{
				print_r($this->link->error);
			}
		}
		else if($tipo == 2){
			$res = $this->link->query($sql);
			$datos = array();
			while($fila = $res->fetch_assoc()){
				$datos[] = $fila;
			}
			return $datos;
		}
	}
	
	public function ClimaTop(){
		$sql="SELECT YEAR(fecha) AS anoo,WEEK(fecha) AS numsemana,
		MIN(temp_minima) AS Tmin,MAX(temp_maxima) AS Tmax,
		SUM(lluvia) AS Tlluvia,'0' AS xxx
		FROM datos_clima 
		WHERE YEAR(fecha)
		GROUP BY numsemana
		ORDER BY anoo DESC,numsemana DESC LIMIT 1";
		$res = $this->link->query($sql);
		$datos = array();
		while($fila = $res->fetch_assoc()){
			$datos[] = $fila;
		}
		return $datos;
	}

	public function ConsultasTresMetros($tipo){
		$sql = "SELECT YEAR(fecha) AS anoo FROM muestras_haciendas_3M GROUP BY YEAR(fecha) ORDER BY anoo";
			$res = $this->link->query($sql);
			$datos = array();
			while($fila = $res->fetch_assoc()){
				
				if($tipo=='HVMLE_HVLE'){ 
					$sql2="
					SELECT 
					WEEK(fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(total_hoja_3) AS hojaViejaLibreEstrias

					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(fecha) = '".$fila['anoo']."' 
					GROUP BY numsemana 
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreEstrias"]);
					}
				}
			}
	}
				

	public function ConsultasCeroSemanas($tipo,$tipo_semana){
		$sql = "SELECT YEAR(fecha) AS anoo FROM muestras_haciendas GROUP BY YEAR(fecha) ORDER BY anoo";
			$res = $this->link->query($sql);
			$datos = array();
			while($fila = $res->fetch_assoc()){
				
				if($tipo=='HMVLDQMEN5%'){ 
					$sql2="
					SELECT 
					WEEK(fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(hoja_mas_vieja_libre_quema_menor) AS hojaViejaLibreQuemaMenor

					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(fecha) = '".$fila['anoo']."' AND tipo_semana = $tipo_semana
					GROUP BY numsemana 
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMenor"]);
					}
				}
				if($tipo=="HMVLDE"){
					$sql2="
					SELECT 
					WEEK(fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(hojas_mas_vieja_libre) AS hojaViejaLibreEstrias

					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(fecha) = '".$fila['anoo']."' AND tipo_semana = $tipo_semana 
					GROUP BY numsemana 
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreEstrias"]);
					}
				}
				if($tipo=="LIB_DE_CIRUG"){
					$sql2="
					SELECT 
					WEEK(fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(libre_cirugias) AS hojaLibreCirugias

					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(fecha) = '".$fila['anoo']."' AND tipo_semana = $tipo_semana 
					GROUP BY numsemana 
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["hojaLibreCirugias"]);
					}
				}
				if($tipo=="HMVLDQMAY5"){
					$sql2="
					SELECT 
					WEEK(fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(hoja_mas_vieja_libre_quema_mayor) AS hojaViejaLibreQuemaMayor

					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(fecha) = '".$fila['anoo']."' AND tipo_semana = $tipo_semana 
					GROUP BY numsemana 
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMayor"]);
					}
				}
				if($tipo=="HOJTOT"){
					$sql2="
					SELECT 
					WEEK(fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(hojas_totales) AS hojasTotales

					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(fecha) = '".$fila['anoo']."' AND tipo_semana = $tipo_semana 
					GROUP BY numsemana 
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["hojasTotales"]);
					}
				}
			}
			return $datos;
	}
	
	public function ConsultaClimas($tipo){
			$sql = "SELECT YEAR(fecha) AS anoo FROM datos_clima GROUP BY YEAR(fecha) ORDER BY anoo";
			$res = $this->link->query($sql);
			$datos = array();
			while($fila = $res->fetch_assoc()){
				
				if($tipo=='TEM_MIN'){ #TEMPERATURA MINIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MIN(temp_minima) AS Tmin
					FROM datos_clima 
					WHERE YEAR(fecha)='".$fila['anoo']."'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Tmin"]);
					}
				}
				else if($tipo=='TEM_MAX'){ #TEMPERATURA MAXIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MAX(temp_maxima) AS Tmax
					FROM datos_clima 
					WHERE YEAR(fecha)='".$fila['anoo']."'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Tmax"]);
					}
				}
				else if($tipo=='LLUVIA'){ #DATOS DE LLUVIA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					SUM(lluvia) AS totlluvia
					FROM datos_clima 
					WHERE YEAR(fecha)='".$fila['anoo']."'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["totlluvia"]);
					}
				}
				else if($tipo=='RAD_SOLAR'){ #RADIACION SOLAR
					$sql2="SELECT WEEK(fecha) AS numsemana,(SUM(rad_solar)/COUNT(*)) AS promedio
					FROM datos_clima 
					WHERE YEAR(fecha)='".$fila['anoo']."'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["promedio"]);
					}
				}
				else if($tipo=='DIAS_SOL'){ #RADIACION SOLAR
					$sql2="SELECT WEEK(fecha) AS numsemana,(SUM(dias_sol)/COUNT(*)) AS promedio
					FROM datos_clima 
					WHERE YEAR(fecha)='".$fila['anoo']."'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["promedio"]);
					}
				}
				if($tipo=='HUM_MIN'){ #HUMEDAD MINIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MIN(humedad) AS Hmin
					FROM datos_clima 
					WHERE YEAR(fecha)='".$fila['anoo']."'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Hmin"]);
					}
				}
				else if($tipo=='HUM_MAX'){ #HUMEDAD MAXIMA
					$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
					MAX(humedad) AS Hmax
					FROM datos_clima 
					WHERE YEAR(fecha)='".$fila['anoo']."'
					GROUP BY numsemana
					ORDER BY numsemana";
					$res2 = $this->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Hmax"]);
					}
				}
			}
			return $datos;
	} 
	
}

?>