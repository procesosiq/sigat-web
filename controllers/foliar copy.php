<?php
include 'class.sesion.php';
include 'conexion.php';
class Graficas_S_C{
	
	private $conexion;
	private $session;
	
	public function __construct() {
        $this->conexion = new M_Conexion();
        $this->session = Session::getInstance();
        // print_r($this->session);
        // $this->session->client = 1;
        // $this->session->finca = 8;
    }
	
	public function ConsultasGrafica(){

		$sql_get_weeks = "SELECT WEEK(fecha) AS semana,foco FROM foliar WHERE 
		id_usuario = '{$this->session->logged}' AND 
		id_cliente = '{$this->session->client}' AND 
		id_hacienda = '{$this->session->finca}' AND 
		YEAR(fecha) = YEAR(CURRENT_TIMESTAMP) GROUP BY WEEK(fecha),foco";
		// print_r($sql_get_weeks);
		$weeks = $this->conexion->Consultas(2, $sql_get_weeks);
		$ol_sem = "";
		$semFoco[] = "";
		$post_semFoco[] = "";
		$contador[] = 0;
		foreach ($weeks as $key => $value) {
			$contador[$value["foco"]]++;
			$semFoco[$value["foco"]] = $value["semana"];
			$sem = $semFoco[$value["foco"]];
			$foco = $value["foco"];
			// if($key == 0 || $value["id"] <= 41){
			// 	$sql_emision_0 = "SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/10 AS hoj_sem FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'";
			// 	// echo $sql_emision_0;
			// 	$resul['Emision Foliar'][] = [$sem ,$this->conexion->Consultas(2, $sql_emision_0)[0]["hoj_sem"]];
			// }
			// else 
			if($contador[$value["foco"]] == 1){
				$post_semFoco[$value["foco"]] = $value["semana"];
				$post_sem = $post_semFoco[$value["foco"]];
				$contador[$value["foco"]]++;
			}
			else if($contador[$value["foco"]] > 1){
				$contador[$value["foco"]]++;
				// echo $sem." Semana Ahora <br>";
				// echo $post_sem." Semana Antes <br>";
				// $post_sem = $sem - 1;

				$sql_emision_1[$value["foco"]] = "
						SELECT ((
							(SELECT (emision1) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')
								-
							(SELECT (emision1) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')
						)
						/
						(SELECT 
							(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')
								-
							(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}' AND foco = '{$foco}')
						)*7)/2
						AS hoj_sem";
				// print_r($sql_emision_1[$value["foco"]]);

				$sql_emision_2[$value["foco"]] = "SELECT(((SELECT (emision2) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision2) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";

				$sql_emision_3[$value["foco"]] = "SELECT(((SELECT (emision3) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision3) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";
				$sql_emision_4[$value["foco"]] = "SELECT(((SELECT (emision4) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision4) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";
				$sql_emision_5[$value["foco"]] = "SELECT(((SELECT (emision5) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision5) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";
				$sql_emision_6[$value["foco"]] = "SELECT(((SELECT (emision6) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision6) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";
				$sql_emision_7[$value["foco"]] = "SELECT(((SELECT (emision7) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision7) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";
				$sql_emision_8[$value["foco"]] = "SELECT(((SELECT (emision8) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision8) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";
				$sql_emision_9[$value["foco"]] = "SELECT(((SELECT (emision9) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision9) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";
				$sql_emision_10[$value["foco"]] = "SELECT(((SELECT (emision10) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision10) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";

				$emision_1 = $this->conexion->Consultas(2, $sql_emision_1[$value["foco"]])[0]["hoj_sem"];
				// echo ($emision_1)."<br>";
				$emision_2 = $this->conexion->Consultas(2, $sql_emision_2[$value["foco"]])[0]["hoj_sem"];
				$emision_3 = $this->conexion->Consultas(2, $sql_emision_3[$value["foco"]])[0]["hoj_sem"];
				$emision_4 = $this->conexion->Consultas(2, $sql_emision_4[$value["foco"]])[0]["hoj_sem"];
				$emision_5 = $this->conexion->Consultas(2, $sql_emision_5[$value["foco"]])[0]["hoj_sem"];
				$emision_6 = $this->conexion->Consultas(2, $sql_emision_6[$value["foco"]])[0]["hoj_sem"];
				$emision_7 = $this->conexion->Consultas(2, $sql_emision_7[$value["foco"]])[0]["hoj_sem"];
				$emision_8 = $this->conexion->Consultas(2, $sql_emision_8[$value["foco"]])[0]["hoj_sem"];
				$emision_9 = $this->conexion->Consultas(2, $sql_emision_9[$value["foco"]])[0]["hoj_sem"];
				$emision_10 = $this->conexion->Consultas(2, $sql_emision_10[$value["foco"]])[0]["hoj_sem"];
				$post_semFoco[$value["foco"]] = $sem;
				$resul[$value["foco"]][] = [$sem,($emision_1 + $emision_2 + $emision_3 + $emision_4 + $emision_5 + $emision_6 + $emision_7 + $emision_8 + $emision_9 + $emision_10)/10];

			}
		}
		return $resul;
	}
	public function Grafica(){
		$resul = array();
		$resul = $this->ConsultasGrafica();
		return json_encode($resul);
	}
}

$postdata = (object)json_decode(file_get_contents("php://input"));
if($postdata->opt == "GRAFICA"){ 
	$retval = new Graficas_S_C();
	echo $retval->Grafica();
}
?>