<?php
include 'class.sesion.php';
include 'conexion.php';
class Graficas_S_C{
	
	private $conexion;
	private $session;
	
	public function __construct() {
        $this->conexion = new M_Conexion();
        $this->session = Session::getInstance();
    }
	private function FoliarTokenJavi($token){
		$sql_get_info = "SELECT * FROM informe WHERE token = '{$token}'";
    	$result = $this->conexion->Consultas(2, $sql_get_info);
    	$id_usuario = $result[0]["id_usuario"];
    	$id_cliente = $result[0]["id_cliente"];
    	$id_finca = $result[0]["id_hacienda"];
    	$ids_membresia = $result[0]["ids_membresia"];
		$sql_get_weeks = "SELECT getWeek(fecha) AS semana, YEAR(fecha) AS anio FROM foliar 
		WHERE id_hacienda = '{$id_finca}' AND id_usuario IN ({$ids_membresia}) AND id_cliente = '{$id_cliente}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
		GROUP BY getWeek(fecha) ORDER BY YEAR(fecha), getWeek(fecha)";
		$weeks = $this->conexion->Consultas(2, $sql_get_weeks);
		$sql_get_focos = "SELECT foco FROM foliar WHERE id_hacienda = '{$id_finca}' AND id_usuario IN ({$ids_membresia}) AND id_cliente = '{$id_cliente}' AND YEAR(fecha) = YEAR(CURRENT_DATE)  GROUP BY foco";
		$focos = $this->conexion->Consultas(2, $sql_get_focos);
		foreach ($weeks as $key => $value) {
			$semana = $weeks[$key]["semana"];
			$anio = $weeks[$key]["anio"];


			if($semana == 1){
				$pre_semana = 52;
				$pre_anio = $anio-1;
			}else{
				$pre_semana = $semana-1;
				$pre_anio = $anio;
			}

			$count_foco=0;

            if(!(sizeof($weeks) == 1 && $semana > 1)) {
                foreach ($focos as $key2 => $foco) {
                    if($key > 0){
				$sql = "
					SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
					FROM
					(SELECT 
					IF(actual.emision1 > pasada.emision1,(actual.emision1-pasada.emision1)/dif_dias*7,0) AS emision1,
					IF(actual.emision2 > pasada.emision2,(actual.emision2-pasada.emision2)/dif_dias*7,0) AS emision2,
					IF(actual.emision3 > pasada.emision3,(actual.emision3-pasada.emision3)/dif_dias*7,0) AS emision3,
					IF(actual.emision4 > pasada.emision4,(actual.emision4-pasada.emision4)/dif_dias*7,0) AS emision4,
					IF(actual.emision5 > pasada.emision5,(actual.emision5-pasada.emision5)/dif_dias*7,0) AS emision5,
					IF(actual.emision6 > pasada.emision6,(actual.emision6-pasada.emision6)/dif_dias*7,0) AS emision6,
					IF(actual.emision7 > pasada.emision7,(actual.emision7-pasada.emision7)/dif_dias*7,0) AS emision7,
					IF(actual.emision8 > pasada.emision8,(actual.emision8-pasada.emision8)/dif_dias*7,0) AS emision8,
					IF(actual.emision9 > pasada.emision9,(actual.emision9-pasada.emision9)/dif_dias*7,0) AS emision9,
					IF(actual.emision10 > pasada.emision10,(actual.emision10-pasada.emision10)/dif_dias*7,0) AS emision10,
					actual.foco
					FROM (SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario IN ({$ids_membresia}) AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS actual
					JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario IN ({$ids_membresia}) AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS pasada
					JOIN 
						(SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
						FROM
							(SELECT *
							FROM (SELECT MIN(fecha) AS dia_actual FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario IN ({$ids_membresia}) AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS actual
							JOIN (SELECT MIN(fecha) AS dia_pasado FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario IN ({$ids_membresia}) AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS pasada) AS tbl_dif) tbl_dif_d) AS tabla
					JOIN 
						(SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
						FROM
							(SELECT IF(actual.emision1 > pasada.emision1,1,0) AS emision1,IF(actual.emision2 > pasada.emision2,1,0) AS emision2,IF(actual.emision3 > pasada.emision3,1,0) AS emision3,IF(actual.emision4 > pasada.emision4,1,0) AS emision4,IF(actual.emision5 > pasada.emision5,1,0) AS emision5,IF(actual.emision6 > pasada.emision6,1,0) AS emision6,IF(actual.emision7 > pasada.emision7,1,0) AS emision7,IF(actual.emision8 > pasada.emision8,1,0) AS emision8,IF(actual.emision9 > pasada.emision9,1,0) AS emision9,IF(actual.emision10 > pasada.emision10,1,0) AS emision10
							FROM
								(SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario IN ({$ids_membresia}) AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS actual
								JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario IN ({$ids_membresia}) AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS pasada)
							AS tbl_suma)
						AS tbl_division limit 20
					";

				$result = $this->conexion->Consultas(2, $sql);

				if($result[0]["foco"]!=""){
					if($id_finca == 16 && $id_cliente == 10){
						if((int)$semana == 24){
							$result[0]["valor"] = "0.72";
						}
						if((int)$semana == 28){
							$result[0]["valor"] = "0.65";
						}
					}
                    $data[$result[0]["foco"]][] = array($semana,round(((double)$result[0]["valor"]),2));
                    $valores[] = round(((double)$result[0]["valor"]),2);
				}
				}
                }
            } else {
                foreach ($focos as $key2 => $foco) {
                    if($key > 0){
				$sql = "
					SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
					FROM
					(SELECT 
					IF(actual.emision1/dif_dias*7,0) AS emision1,
					IF(actual.emision2/dif_dias*7,0) AS emision2,
					IF(actual.emision3/dif_dias*7,0) AS emision3,
					IF(actual.emision4/dif_dias*7,0) AS emision4,
					IF(actual.emision5/dif_dias*7,0) AS emision5,
					IF(actual.emision6/dif_dias*7,0) AS emision6,
					IF(actual.emision7/dif_dias*7,0) AS emision7,
					IF(actual.emision8/dif_dias*7,0) AS emision8,
					IF(actual.emision9/dif_dias*7,0) AS emision9,
					IF(actual.emision10/dif_dias*7,0) AS emision10,
					actual.foco
					FROM (SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario IN ({$ids_membresia}) AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS actual
					JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario IN ({$ids_membresia}) AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS pasada
					JOIN 
						(SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
						FROM
							(SELECT *
							FROM (SELECT MIN(fecha) AS dia_actual FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario IN ({$ids_membresia}) AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS actual
							JOIN (SELECT MIN(fecha) AS dia_pasado FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario IN ({$ids_membresia}) AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS pasada) AS tbl_dif) tbl_dif_d) AS tabla
					JOIN 
						(SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
						FROM
							(SELECT IF(actual.emision1,1,0) AS emision1,IF(actual.emision2,1,0) AS emision2,IF(actual.emision3,1,0) AS emision3,IF(actual.emision4,1,0) AS emision4,IF(actual.emision5,1,0) AS emision5,IF(actual.emision6,1,0) AS emision6,IF(actual.emision7,1,0) AS emision7,IF(actual.emision8,1,0) AS emision8,IF(actual.emision9,1,0) AS emision9,IF(actual.emision10,1,0) AS emision10
							FROM
								(SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario IN ({$ids_membresia}) AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS actual
								JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario IN ({$ids_membresia}) AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS pasada)
							AS tbl_suma)
						AS tbl_division limit 20
					";

				#echo "semana = $semana<br>";

				$result = $this->conexion->Consultas(2, $sql);

				#echo "SEMANA $semana = ".json_encode($result)."<br>";
				if($result[0]["foco"]!=""){
					if($id_finca == 16 && $id_cliente == 10){
						if((int)$semana == 24){
							$result[0]["valor"] = "0.72";
						}
						if((int)$semana == 28){
							$result[0]["valor"] = "0.65";
						}
					}
                    $data[$result[0]["foco"]][] = array($semana,round(((double)$result[0]["valor"]),2));
                    $valores[] = round(((double)$result[0]["valor"]),2);
				}
				}
                }

            }
		}

        if(sizeof($data["Resto Finca"]) == 1) {
            $indice = $data["Resto Finca"][0][0];
            if($indice > 1) {
                array_push($data["Resto Finca"], [$indice - 1, 0] );
            }
        }
		return ["data" => $data, "min" => min($valores), "max" => max($valores) ];
	}

    private function get_foliar_division($arr_pasado, $arr_actual) {
        $emision1 = ($arr_actual->emision1 > $arr_pasado->emision1 ? 1:0);
        $emision2 = ($arr_actual->emision2 > $arr_pasado->emision2 ? 1:0);
        $emision3 = ($arr_actual->emision3 > $arr_pasado->emision3 ? 1:0);
        $emision4 = ($arr_actual->emision4 > $arr_pasado->emision4 ? 1:0);
        $emision5 = ($arr_actual->emision5 > $arr_pasado->emision5 ? 1:0);
        $emision6 = ($arr_actual->emision6 > $arr_pasado->emision6 ? 1:0);
        $emision7 = ($arr_actual->emision7 > $arr_pasado->emision7 ? 1:0);
        $emision8 = ($arr_actual->emision8 > $arr_pasado->emision8 ? 1:0);
        $emision9 = ($arr_actual->emision9 > $arr_pasado->emision9 ? 1:0);
        $emision10 = ($arr_actual->emision10 > $arr_pasado->emision10 ? 1:0);

        return ($emision1 + $emision2 + $emision3 + $emision4 + $emision5 + $emision6 + $emision7 + $emision8 + $emision9 + $emision10);
    }

    private function get_foliar_total($arr_pasado, $arr_actual, $division, $diff) {
        $emision1 = ($arr_actual->emision1 > $arr_pasado->emision1 ? ($arr_actual->emision1 - $arr_pasado->emision1)/$diff*7:0); // 
        $emision2 = ($arr_actual->emision2 > $arr_pasado->emision2 ? ($arr_actual->emision2 - $arr_pasado->emision2)/$diff*7:0);
        $emision3 = ($arr_actual->emision3 > $arr_pasado->emision3 ? ($arr_actual->emision3 - $arr_pasado->emision3)/$diff*7:0);
        $emision4 = ($arr_actual->emision4 > $arr_pasado->emision4 ? ($arr_actual->emision4 - $arr_pasado->emision4)/$diff*7:0);
        $emision5 = ($arr_actual->emision5 > $arr_pasado->emision5 ? ($arr_actual->emision5 - $arr_pasado->emision5)/$diff*7:0);
        $emision6 = ($arr_actual->emision6 > $arr_pasado->emision6 ? ($arr_actual->emision6 - $arr_pasado->emision6)/$diff*7:0);
        $emision7 = ($arr_actual->emision7 > $arr_pasado->emision7 ? ($arr_actual->emision7 - $arr_pasado->emision7)/$diff*7:0);
        $emision8 = ($arr_actual->emision8 > $arr_pasado->emision8 ? ($arr_actual->emision8 - $arr_pasado->emision8)/$diff*7:0);
        $emision9 = ($arr_actual->emision9 > $arr_pasado->emision9 ? ($arr_actual->emision9 - $arr_pasado->emision9)/$diff*7:0);
        $emision10 = ($arr_actual->emision10 > $arr_pasado->emision10 ? ($arr_actual->emision10 - $arr_pasado->emision10)/$diff*7:0);

        return ($emision1 + $emision2 + $emision3 + $emision4 + $emision5 + $emision6 + $emision7 + $emision8 + $emision9 + $emision10)/$division;
    }

    private function foliarNew2() {
        $sql = " select year(fecha) as anio, getWeek(fecha) as semana,
                emision1, emision2, emision3, emision4, emision5, emision6, emision7, 
                emision8, emision9, emision10, foco
                from foliar s where id_hacienda = '{$this->session->finca}' 
                AND id_usuario IN ({$this->session->logges->users})  
                AND id_cliente = '{$this->session->client}'  
                AND (YEAR(fecha) = {$this->session->year} OR (YEAR(fecha) = {$this->session->year}-1 AND getWeek(fecha) = 52))
                group by year(fecha), getWeek(fecha), foco
                order by fecha asc;";

        $result = $this->db->queryAll($sql);

        $anterior_row  = array();
        $acum = array();
		$count_foco=0;
		$foco = ['foco' => ''];
		$tabla = array();

        foreach($result as $row){
            $actual_semana = $row->semana;
            $actual_anio = $row->anio;

			if($foco['foco'] != $row->foco) {
				$foco = ['foco' => $row->foco];
				$count_foco=0;
			}
            if(!empty($anterior_row)) {
                $sql_dia_pasado = "SELECT MIN(fecha) AS dia_pasado 
                                    FROM foliar 
                                    WHERE getWeek(fecha) = ".$anterior_row->semana." 
                                        AND YEAR(fecha) = '{$anterior_row->anio}'
                                        AND id_hacienda = '{$this->session->finca}' 
                                        AND id_usuario IN ({$this->session->logges->users}) 
                                        AND id_cliente = '{$this->session->client}' 
                                        AND foco = '".$foco['foco']."' 
                                        GROUP BY id_hacienda";

                $sql_dia_actual = "SELECT MIN(fecha) AS dia_actual 
                                    FROM foliar 
                                    WHERE getWeek(fecha) = ".$actual_semana." 
                                        AND YEAR(fecha) = '{$actual_anio}'
                                        AND id_hacienda = '{$this->session->finca}' 
                                        AND id_usuario IN ({$this->session->logges->users}) 
                                        AND id_cliente = '{$this->session->client}' 
                                        AND foco = '".$foco."' 
                                        GROUP BY id_hacienda";

                $dia_pasado = $this->db->queryOne($sql_dia_pasado);
                $dia_actual = $this->db->queryOne($sql_dia_actual);

                // emisiones
                $div_valor = $this->get_foliar_division($anterior_row, $row);

                $division = ($div_valor != 0 ? $div_valor: 1);
                $diff = date_diff(new DateTime($dia_pasado), new DateTime($dia_actual), true);
                $total = $this->get_foliar_total($anterior_row, $row, $division, $diff->days);

                $count_foco+=1;

                $data[$foco][]  = array($actual_semana,round(((double)$total),2));
                $tabla[$foco][$actual_semana]  = (double)round(((double)$total),2);
            }
            $anterior_row = clone $row;
        }
    }

	private function FoliarNew(){
		$sql_get_weeks = "SELECT getWeek(fecha) AS semana, YEAR(fecha) AS anio , foco FROM foliar 
			WHERE id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND YEAR(fecha) = '{$this->session->current_year}'
			GROUP BY foco , getWeek(fecha) 
			ORDER BY foco ,YEAR(fecha), getWeek(fecha)";
		$weeks = $this->conexion->Consultas(2, $sql_get_weeks);
		// print_r($weeks);
		// $sql_get_focos = "SELECT foco FROM foliar WHERE id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' GROUP BY foco";
		// $focos = $this->conexion->Consultas(2, $sql_get_focos);
		$count_foco=0;
		$foco = ['foco' => ''];
		$tabla = array();
		foreach ($weeks as $key => $value) {
			$semana = $weeks[$key]["semana"];
			$anio = $weeks[$key]["anio"];
			if($foco['foco'] != $weeks[$key]["foco"]){
				$foco = ['foco' => $weeks[$key]["foco"]];
				$count_foco=0;
			}


			if($semana == 1){
				$pre_semana = 52;
				$pre_anio = $anio-1;
			}else{
				$pre_semana = $semana-1;
				$pre_anio = $anio;
			}


			// foreach ($focos as $key2 => $foco) {
				if($count_foco > 0){
				$sql = "
					SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
					FROM
					(SELECT 
					IF(actual.emision1 > pasada.emision1,(actual.emision1-pasada.emision1)/dif_dias*7,0) AS emision1,
					IF(actual.emision2 > pasada.emision2,(actual.emision2-pasada.emision2)/dif_dias*7,0) AS emision2,
					IF(actual.emision3 > pasada.emision3,(actual.emision3-pasada.emision3)/dif_dias*7,0) AS emision3,
					IF(actual.emision4 > pasada.emision4,(actual.emision4-pasada.emision4)/dif_dias*7,0) AS emision4,
					IF(actual.emision5 > pasada.emision5,(actual.emision5-pasada.emision5)/dif_dias*7,0) AS emision5,
					IF(actual.emision6 > pasada.emision6,(actual.emision6-pasada.emision6)/dif_dias*7,0) AS emision6,
					IF(actual.emision7 > pasada.emision7,(actual.emision7-pasada.emision7)/dif_dias*7,0) AS emision7,
					IF(actual.emision8 > pasada.emision8,(actual.emision8-pasada.emision8)/dif_dias*7,0) AS emision8,
					IF(actual.emision9 > pasada.emision9,(actual.emision9-pasada.emision9)/dif_dias*7,0) AS emision9,
					IF(actual.emision10 > pasada.emision10,(actual.emision10-pasada.emision10)/dif_dias*7,0) AS emision10,
					actual.foco
					FROM (SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS actual
					JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS pasada
					JOIN 
						(SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
						FROM
							(SELECT *
							FROM (SELECT MIN(fecha) AS dia_actual FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS actual
							JOIN (SELECT MIN(fecha) AS dia_pasado FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS pasada) AS tbl_dif) tbl_dif_d) AS tabla
					JOIN 
						(SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
						FROM
							(SELECT IF(actual.emision1 > pasada.emision1,1,0) AS emision1,IF(actual.emision2 > pasada.emision2,1,0) AS emision2,IF(actual.emision3 > pasada.emision3,1,0) AS emision3,IF(actual.emision4 > pasada.emision4,1,0) AS emision4,IF(actual.emision5 > pasada.emision5,1,0) AS emision5,IF(actual.emision6 > pasada.emision6,1,0) AS emision6,IF(actual.emision7 > pasada.emision7,1,0) AS emision7,IF(actual.emision8 > pasada.emision8,1,0) AS emision8,IF(actual.emision9 > pasada.emision9,1,0) AS emision9,IF(actual.emision10 > pasada.emision10,1,0) AS emision10
							FROM
								(SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS actual
								JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS pasada)
							AS tbl_suma)
						AS tbl_division
					";

				// echo "semana = $semana<br>";
				#echo $sql."<br><br>";

				$result = $this->conexion->Consultas(2, $sql);

				#echo "SEMANA $semana = ".json_encode($result)."<br>";
					if($result[0]["foco"]!=""){
						if($this->session->finca == 16 && $this->session->client == 10){
							if((int)$semana == 24){
								$result[0]["valor"] = "0.72";
							}
							if((int)$semana == 28){
								$result[0]["valor"] = "0.65";
							}
						}
						$data[$result[0]["foco"]][] = array($semana,round(((double)$result[0]["valor"]),2));
						$tabla[$result[0]["foco"]][$semana] = (double)round(((double)$result[0]["valor"]),2);
					}
				}
			$count_foco++;
			// }
		}
		$response = (object)[];
		$response->table = (object)$tabla;
		$response->data = (object)$data;
		return $response;
	}

	private function Foliar(){
		$sql_get_weeks = "SELECT getWeek(fecha) AS semana, YEAR(fecha) AS anio FROM foliar WHERE id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' GROUP BY getWeek(fecha) ORDER BY YEAR(fecha), getWeek(fecha)";
		$weeks = $this->conexion->Consultas(2, $sql_get_weeks);
		$sql_get_focos = "SELECT foco FROM foliar WHERE id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' GROUP BY foco";
		$focos = $this->conexion->Consultas(2, $sql_get_focos);
		foreach ($weeks as $key => $value) {
			$semana = $weeks[$key]["semana"];
			$anio = $weeks[$key]["anio"];

			if($semana == 1){
				$pre_semana = 52;
				$pre_anio = $anio-1;
			}else{
				$pre_semana = $semana-1;
				$pre_anio = $anio;
			}

			$count_foco=0;

			foreach ($focos as $key2 => $foco) {
				if($key > 0){
				$sql = "
					SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
					FROM
					(SELECT 
					IF(actual.emision1 > pasada.emision1,(actual.emision1-pasada.emision1)/dif_dias*7,0) AS emision1,
					IF(actual.emision2 > pasada.emision2,(actual.emision2-pasada.emision2)/dif_dias*7,0) AS emision2,
					IF(actual.emision3 > pasada.emision3,(actual.emision3-pasada.emision3)/dif_dias*7,0) AS emision3,
					IF(actual.emision4 > pasada.emision4,(actual.emision4-pasada.emision4)/dif_dias*7,0) AS emision4,
					IF(actual.emision5 > pasada.emision5,(actual.emision5-pasada.emision5)/dif_dias*7,0) AS emision5,
					IF(actual.emision6 > pasada.emision6,(actual.emision6-pasada.emision6)/dif_dias*7,0) AS emision6,
					IF(actual.emision7 > pasada.emision7,(actual.emision7-pasada.emision7)/dif_dias*7,0) AS emision7,
					IF(actual.emision8 > pasada.emision8,(actual.emision8-pasada.emision8)/dif_dias*7,0) AS emision8,
					IF(actual.emision9 > pasada.emision9,(actual.emision9-pasada.emision9)/dif_dias*7,0) AS emision9,
					IF(actual.emision10 > pasada.emision10,(actual.emision10-pasada.emision10)/dif_dias*7,0) AS emision10,
					actual.foco
					FROM (SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS actual
					JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS pasada
					JOIN 
						(SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
						FROM
							(SELECT *
							FROM (SELECT MIN(fecha) AS dia_actual FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS actual
							JOIN (SELECT MIN(fecha) AS dia_pasado FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS pasada) AS tbl_dif) tbl_dif_d) AS tabla
					JOIN 
						(SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
						FROM
							(SELECT IF(actual.emision1 > pasada.emision1,1,0) AS emision1,IF(actual.emision2 > pasada.emision2,1,0) AS emision2,IF(actual.emision3 > pasada.emision3,1,0) AS emision3,IF(actual.emision4 > pasada.emision4,1,0) AS emision4,IF(actual.emision5 > pasada.emision5,1,0) AS emision5,IF(actual.emision6 > pasada.emision6,1,0) AS emision6,IF(actual.emision7 > pasada.emision7,1,0) AS emision7,IF(actual.emision8 > pasada.emision8,1,0) AS emision8,IF(actual.emision9 > pasada.emision9,1,0) AS emision9,IF(actual.emision10 > pasada.emision10,1,0) AS emision10
							FROM
								(SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS actual
								JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS pasada)
							AS tbl_suma)
						AS tbl_division
					";

				#echo "semana = $semana<br>";
				#echo $sql."<br><br>";

				$result = $this->conexion->Consultas(2, $sql);

				#echo "SEMANA $semana = ".json_encode($result)."<br>";
				if($result[0]["foco"]!=""){
					$data[$result[0]["foco"]][] = array($semana,round(((double)$result[0]["valor"]),2));
				}
				}
			}
		}
		return $data;
	}

	private function FoliarToken2($token){
		$sql_get_info = "SELECT * FROM informe WHERE token = '{$token}'";
    	$result = $this->conexion->Consultas(2, $sql_get_info);
    	$id_usuario = $result[0]["id_usuario"];
    	$id_cliente = $result[0]["id_cliente"];
    	$id_finca = $result[0]["id_hacienda"];

		$sql_get_weeks = "SELECT getWeek(fecha) AS semana, YEAR(fecha) AS anio FROM foliar WHERE id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' GROUP BY getWeek(fecha) ORDER BY YEAR(fecha), getWeek(fecha)";
		$weeks = $this->conexion->Consultas(2, $sql_get_weeks);
		$sql_get_focos = "SELECT foco FROM foliar WHERE id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' GROUP BY foco";
		$focos = $this->conexion->Consultas(2, $sql_get_focos);
		foreach ($weeks as $key => $value) {
			if($key>0){
				$semana = $weeks[$key]["semana"];
				$anio = $weeks[$key]["anio"];

				if($semana == 1){
					$pre_semana = 52;
					$pre_anio = $anio-1;
				}else{
					$pre_semana = $semana-1;
					$pre_anio = $anio;
				}

				$count_foco=0;

				foreach ($focos as $key2 => $foco) {
					$sql = "
						SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
						FROM
						(SELECT 
						IF(actual.emision1 > pasada.emision1,(actual.emision1-pasada.emision1)/dif_dias*7,0) AS emision1,
						IF(actual.emision2 > pasada.emision2,(actual.emision2-pasada.emision2)/dif_dias*7,0) AS emision2,
						IF(actual.emision3 > pasada.emision3,(actual.emision3-pasada.emision3)/dif_dias*7,0) AS emision3,
						IF(actual.emision4 > pasada.emision4,(actual.emision4-pasada.emision4)/dif_dias*7,0) AS emision4,
						IF(actual.emision5 > pasada.emision5,(actual.emision5-pasada.emision5)/dif_dias*7,0) AS emision5,
						IF(actual.emision6 > pasada.emision6,(actual.emision6-pasada.emision6)/dif_dias*7,0) AS emision6,
						IF(actual.emision7 > pasada.emision7,(actual.emision7-pasada.emision7)/dif_dias*7,0) AS emision7,
						IF(actual.emision8 > pasada.emision8,(actual.emision8-pasada.emision8)/dif_dias*7,0) AS emision8,
						IF(actual.emision9 > pasada.emision9,(actual.emision9-pasada.emision9)/dif_dias*7,0) AS emision9,
						IF(actual.emision10 > pasada.emision10,(actual.emision10-pasada.emision10)/dif_dias*7,0) AS emision10,
						actual.foco
						FROM (SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS actual
						JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS pasada
						JOIN 
							(SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
							FROM
								(SELECT *
								FROM (SELECT MIN(fecha) AS dia_actual FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS actual
								JOIN (SELECT MIN(fecha) AS dia_pasado FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS pasada) AS tbl_dif) tbl_dif_d) AS tabla
						JOIN 
							(SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
							FROM
								(SELECT IF(actual.emision1 > pasada.emision1,1,0) AS emision1,IF(actual.emision2 > pasada.emision2,1,0) AS emision2,IF(actual.emision3 > pasada.emision3,1,0) AS emision3,IF(actual.emision4 > pasada.emision4,1,0) AS emision4,IF(actual.emision5 > pasada.emision5,1,0) AS emision5,IF(actual.emision6 > pasada.emision6,1,0) AS emision6,IF(actual.emision7 > pasada.emision7,1,0) AS emision7,IF(actual.emision8 > pasada.emision8,1,0) AS emision8,IF(actual.emision9 > pasada.emision9,1,0) AS emision9,IF(actual.emision10 > pasada.emision10,1,0) AS emision10
								FROM
									(SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS actual
									JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS pasada)
								AS tbl_suma)
							AS tbl_division
						";

					#echo "semana = $semana<br>";
					#echo $sql."<br><br>";

					$result = $this->conexion->Consultas(2, $sql);
					#echo "SEMANA $semana = ".json_encode($result)."<br>";
					if($result[0]["foco"]!=""){
						$count_foco++;
						if(isset($data["foco"][$key]))
							$data["foco"][$key] = array($semana+1, $data["foco"][$key][1]+round(((double)$result[0]["valor"]),2));
						else
							$data["foco"][$key] = array($semana+1, round(((double)$result[0]["valor"]),2));
						#$data[$result[0]["foco"]][] = $result[0]["valor"];
					}
					/*if($key2+1 == count($focos)){
						$acum["foco"][$key] /= $count_foco;
						$data["foco"][] = array($semana, $acum["foco"][$key]);
					}*/
				}
				if($count_foco > 1)
					$data["foco"][$key][1] = $data["foco"][$key][1]/$count_foco;
			}else{
				$datos["foco"][] = array("1", 0);
			}
		}
		foreach ($data["foco"] as $key => $value) {
			$datos["foco"][] = $value;
		}
		return $datos;
	}

	private function Foliar2(){
		$sql_get_weeks = "SELECT getWeek(fecha) AS semana, YEAR(fecha) AS anio FROM foliar WHERE id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' GROUP BY getWeek(fecha) ORDER BY YEAR(fecha), getWeek(fecha)";
		$weeks = $this->conexion->Consultas(2, $sql_get_weeks);
		$sql_get_focos = "SELECT foco FROM foliar WHERE id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' GROUP BY foco";
		$focos = $this->conexion->Consultas(2, $sql_get_focos);
		foreach ($weeks as $key => $value) {
			if($key>0){
				$semana = $weeks[$key]["semana"];
				$anio = $weeks[$key]["anio"];

				if($semana == 1){
					$pre_semana = 52;
					$pre_anio = $anio-1;
				}else{
					$pre_semana = $semana-1;
					$pre_anio = $anio;
				}

				$count_foco=0;

				foreach ($focos as $key2 => $foco) {
					if($key > 0){
					$sql = "
						SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
						FROM
						(SELECT 
						IF(actual.emision1 > pasada.emision1,(actual.emision1-pasada.emision1)/dif_dias*7,0) AS emision1,
						IF(actual.emision2 > pasada.emision2,(actual.emision2-pasada.emision2)/dif_dias*7,0) AS emision2,
						IF(actual.emision3 > pasada.emision3,(actual.emision3-pasada.emision3)/dif_dias*7,0) AS emision3,
						IF(actual.emision4 > pasada.emision4,(actual.emision4-pasada.emision4)/dif_dias*7,0) AS emision4,
						IF(actual.emision5 > pasada.emision5,(actual.emision5-pasada.emision5)/dif_dias*7,0) AS emision5,
						IF(actual.emision6 > pasada.emision6,(actual.emision6-pasada.emision6)/dif_dias*7,0) AS emision6,
						IF(actual.emision7 > pasada.emision7,(actual.emision7-pasada.emision7)/dif_dias*7,0) AS emision7,
						IF(actual.emision8 > pasada.emision8,(actual.emision8-pasada.emision8)/dif_dias*7,0) AS emision8,
						IF(actual.emision9 > pasada.emision9,(actual.emision9-pasada.emision9)/dif_dias*7,0) AS emision9,
						IF(actual.emision10 > pasada.emision10,(actual.emision10-pasada.emision10)/dif_dias*7,0) AS emision10,
						actual.foco
						FROM (SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS actual
						JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS pasada
						JOIN 
							(SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
							FROM
								(SELECT *
								FROM (SELECT MIN(fecha) AS dia_actual FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS actual
								JOIN (SELECT MIN(fecha) AS dia_pasado FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS pasada) AS tbl_dif) tbl_dif_d) AS tabla
						JOIN 
							(SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
							FROM
								(SELECT IF(actual.emision1 > pasada.emision1,1,0) AS emision1,IF(actual.emision2 > pasada.emision2,1,0) AS emision2,IF(actual.emision3 > pasada.emision3,1,0) AS emision3,IF(actual.emision4 > pasada.emision4,1,0) AS emision4,IF(actual.emision5 > pasada.emision5,1,0) AS emision5,IF(actual.emision6 > pasada.emision6,1,0) AS emision6,IF(actual.emision7 > pasada.emision7,1,0) AS emision7,IF(actual.emision8 > pasada.emision8,1,0) AS emision8,IF(actual.emision9 > pasada.emision9,1,0) AS emision9,IF(actual.emision10 > pasada.emision10,1,0) AS emision10
								FROM
									(SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS actual
									JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS pasada)
								AS tbl_suma)
							AS tbl_division
						";

					#echo "semana = $semana<br>";
					#echo $sql."<br><br>";

					$result = $this->conexion->Consultas(2, $sql);
					#echo "SEMANA $semana = ".json_encode($result)."<br>";
					if($result[0]["foco"]!=""){
						$count_foco++;
						if(isset($data["foco"][$key]))
							$data["foco"][$key] = array($semana+1, $data["foco"][$key][1]+round(((double)$result[0]["valor"]),2));
						else
							$data["foco"][$key] = array($semana+1, round(((double)$result[0]["valor"]),2));
						#$data[$result[0]["foco"]][] = $result[0]["valor"];
					}
					/*if($key2+1 == count($focos)){
						$acum["foco"][$key] /= $count_foco;
						$data["foco"][] = array($semana, $acum["foco"][$key]);
					}*/
					}
				}
				if($count_foco > 1)
					$data["foco"][$key][1] = $data["foco"][$key][1]/$count_foco;
			}else{
				$datos["foco"][] = array("1", 0);
			}
		}
		foreach ($data["foco"] as $key => $value) {
			$datos["foco"][] = $value;
		}
		return $datos;
	}

	public function Grafica(){
		$resul = array();
		// $resul = $this->Foliar();
		$resul = $this->FoliarNew();
		return json_encode($resul);
	}
	
	public function GraficaToken($token){
		$resul = array();
		$resul = $this->FoliarTokenJavi($token);
		return json_encode($resul);
	}	
}

$postdata = (object)json_decode(file_get_contents("php://input"));
if(!isset($postdata->opt)) $postdata = (object) $_POST;

if($postdata->opt == "GRAFICA"){ 
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_C();
			echo $retval->GraficaToken($postdata->token);
		}	
	}else{
		$retval = new Graficas_S_C();
		echo $retval->Grafica();
	}
}
?>
