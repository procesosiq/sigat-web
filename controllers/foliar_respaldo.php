<?php
include 'class.sesion.php';
include 'conexion.php';
class Graficas_S_C{
	
	private $conexion;
	private $session;
	
	public function __construct() {
        $this->conexion = new M_Conexion();
        $this->session = Session::getInstance();
        // print_r($this->session);
        // $this->session->client = 1;
        // $this->session->finca = 8;
    }
	
	public function ConsultasGrafica(){

		$sql_get_weeks = "SELECT WEEK(fecha) AS semana,foco FROM foliar WHERE 
		id_usuario = '{$this->session->logged}' AND 
		id_cliente = '{$this->session->client}' AND 
		id_hacienda = '{$this->session->finca}' AND 
		YEAR(fecha) = YEAR(CURRENT_TIMESTAMP) GROUP BY WEEK(fecha),foco";
		// print_r($sql_get_weeks);
		$weeks = $this->conexion->Consultas(2, $sql_get_weeks);
		$ol_sem = "";
		$semFoco[] = "";
		$post_semFoco[] = "";
		$contador[] = 0;
		foreach ($weeks as $key => $value) {
			$contador[$value["foco"]]++;
			$semFoco[$value["foco"]] = $value["semana"];
			$sem = $semFoco[$value["foco"]];
			$foco = $value["foco"];
			// if($key == 0 || $value["id"] <= 41){
			// 	$sql_emision_0 = "SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/10 AS hoj_sem FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'";
			// 	// echo $sql_emision_0;
			// 	$resul['Emision Foliar'][] = [$sem ,$this->conexion->Consultas(2, $sql_emision_0)[0]["hoj_sem"]];
			// }
			// else 
			if($contador[$value["foco"]] == 1){
				$post_semFoco[$value["foco"]] = $value["semana"];
				$post_sem = $post_semFoco[$value["foco"]];
				$contador[$value["foco"]]++;
			}
			else if($contador[$value["foco"]] > 1){
				$contador[$value["foco"]]++;
				// echo $sem." Semana Ahora <br>";
				// echo $post_sem." Semana Antes <br>";
				// $post_sem = $sem - 1;

				$sql_emision_1[$value["foco"]] = "
						SELECT ((
							(SELECT (emision1) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')
								-
							(SELECT (emision1) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')
						)
						/
						(SELECT 
							(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')
								-
							(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}' AND foco = '{$foco}')
						)*7)/2
						AS hoj_sem";
				// print_r($sql_emision_1[$value["foco"]]);

				$sql_emision_2[$value["foco"]] = "SELECT(((SELECT (emision2) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision2) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";

				$sql_emision_3[$value["foco"]] = "SELECT(((SELECT (emision3) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision3) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";
				$sql_emision_4[$value["foco"]] = "SELECT(((SELECT (emision4) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision4) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";
				$sql_emision_5[$value["foco"]] = "SELECT(((SELECT (emision5) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision5) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";
				$sql_emision_6[$value["foco"]] = "SELECT(((SELECT (emision6) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision6) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";
				$sql_emision_7[$value["foco"]] = "SELECT(((SELECT (emision7) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision7) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";
				$sql_emision_8[$value["foco"]] = "SELECT(((SELECT (emision8) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision8) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";
				$sql_emision_9[$value["foco"]] = "SELECT(((SELECT (emision9) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision9) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";
				$sql_emision_10[$value["foco"]] = "SELECT(((SELECT (emision10) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT (emision10) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}' AND foco = '{$foco}'))*7) AS hoj_sem";

				$emision_1 = $this->conexion->Consultas(2, $sql_emision_1[$value["foco"]])[0]["hoj_sem"];
				// echo ($emision_1)."<br>";
				$emision_2 = $this->conexion->Consultas(2, $sql_emision_2[$value["foco"]])[0]["hoj_sem"];
				$emision_3 = $this->conexion->Consultas(2, $sql_emision_3[$value["foco"]])[0]["hoj_sem"];
				$emision_4 = $this->conexion->Consultas(2, $sql_emision_4[$value["foco"]])[0]["hoj_sem"];
				$emision_5 = $this->conexion->Consultas(2, $sql_emision_5[$value["foco"]])[0]["hoj_sem"];
				$emision_6 = $this->conexion->Consultas(2, $sql_emision_6[$value["foco"]])[0]["hoj_sem"];
				$emision_7 = $this->conexion->Consultas(2, $sql_emision_7[$value["foco"]])[0]["hoj_sem"];
				$emision_8 = $this->conexion->Consultas(2, $sql_emision_8[$value["foco"]])[0]["hoj_sem"];
				$emision_9 = $this->conexion->Consultas(2, $sql_emision_9[$value["foco"]])[0]["hoj_sem"];
				$emision_10 = $this->conexion->Consultas(2, $sql_emision_10[$value["foco"]])[0]["hoj_sem"];
				$post_semFoco[$value["foco"]] = $sem;
				$resul[$value["foco"]][] = [$sem,($emision_1 + $emision_2 + $emision_3 + $emision_4 + $emision_5 + $emision_6 + $emision_7 + $emision_8 + $emision_9 + $emision_10)/10];

			}
		}
		return $resul;
	}

	private function FoliarTokenJavi($token){
		$sql_get_info = "SELECT * FROM informe WHERE token = '{$token}'";
    	$result = $this->conexion->Consultas(2, $sql_get_info);
    	$id_usuario = $result[0]["id_usuario"];
    	$id_cliente = $result[0]["id_cliente"];
    	$id_finca = $result[0]["id_hacienda"];
		$sql_get_weeks = "SELECT WEEK(fecha) AS semana, YEAR(fecha) AS anio FROM foliar WHERE id_hacienda = '{$id_finca}' AND id_usuario = {$id_usuario} AND id_cliente = '{$id_cliente}' GROUP BY WEEK(fecha) ORDER BY YEAR(fecha), WEEK(fecha)";
		$weeks = $this->conexion->Consultas(2, $sql_get_weeks);
		$sql_get_focos = "SELECT foco FROM foliar WHERE id_hacienda = '{$id_finca}' AND id_usuario = {$id_usuario} AND id_cliente = '{$id_cliente}' GROUP BY foco";
		$focos = $this->conexion->Consultas(2, $sql_get_focos);
		foreach ($weeks as $key => $value) {
			$semana = $weeks[$key]["semana"];
			$anio = $weeks[$key]["anio"];

			if($semana == 1){
				$pre_semana = 52;
				$pre_anio = $anio-1;
			}else{
				$pre_semana = $semana-1;
				$pre_anio = $anio;
			}

			$count_foco=0;

			foreach ($focos as $key2 => $foco) {
				if($key > 0){
				$sql = "
					SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
					FROM
					(SELECT 
					IF(actual.emision1 > pasada.emision1,(actual.emision1-pasada.emision1)/dif_dias*7,0) AS emision1,
					IF(actual.emision2 > pasada.emision2,(actual.emision2-pasada.emision2)/dif_dias*7,0) AS emision2,
					IF(actual.emision3 > pasada.emision3,(actual.emision3-pasada.emision3)/dif_dias*7,0) AS emision3,
					IF(actual.emision4 > pasada.emision4,(actual.emision4-pasada.emision4)/dif_dias*7,0) AS emision4,
					IF(actual.emision5 > pasada.emision5,(actual.emision5-pasada.emision5)/dif_dias*7,0) AS emision5,
					IF(actual.emision6 > pasada.emision6,(actual.emision6-pasada.emision6)/dif_dias*7,0) AS emision6,
					IF(actual.emision7 > pasada.emision7,(actual.emision7-pasada.emision7)/dif_dias*7,0) AS emision7,
					IF(actual.emision8 > pasada.emision8,(actual.emision8-pasada.emision8)/dif_dias*7,0) AS emision8,
					IF(actual.emision9 > pasada.emision9,(actual.emision9-pasada.emision9)/dif_dias*7,0) AS emision9,
					IF(actual.emision10 > pasada.emision10,(actual.emision10-pasada.emision10)/dif_dias*7,0) AS emision10,
					actual.foco
					FROM (SELECT * FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario = {$id_usuario} AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS actual
					JOIN (SELECT * FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario = {$id_usuario} AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS pasada
					JOIN 
						(SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
						FROM
							(SELECT *
							FROM (SELECT MIN(fecha) AS dia_actual FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario = {$id_usuario} AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS actual
							JOIN (SELECT MIN(fecha) AS dia_pasado FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario = {$id_usuario} AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS pasada) AS tbl_dif) tbl_dif_d) AS tabla
					JOIN 
						(SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
						FROM
							(SELECT IF(actual.emision1 > pasada.emision1,1,0) AS emision1,IF(actual.emision2 > pasada.emision2,1,0) AS emision2,IF(actual.emision3 > pasada.emision3,1,0) AS emision3,IF(actual.emision4 > pasada.emision4,1,0) AS emision4,IF(actual.emision5 > pasada.emision5,1,0) AS emision5,IF(actual.emision6 > pasada.emision6,1,0) AS emision6,IF(actual.emision7 > pasada.emision7,1,0) AS emision7,IF(actual.emision8 > pasada.emision8,1,0) AS emision8,IF(actual.emision9 > pasada.emision9,1,0) AS emision9,IF(actual.emision10 > pasada.emision10,1,0) AS emision10
							FROM
								(SELECT * FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario = {$id_usuario} AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS actual
								JOIN (SELECT * FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario = {$id_usuario} AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS pasada)
							AS tbl_suma)
						AS tbl_division
					";

				#echo "semana = $semana<br>";
				#echo $sql."<br><br>";

				$result = $this->conexion->Consultas(2, $sql);

				#echo "SEMANA $semana = ".json_encode($result)."<br>";
				if($result[0]["foco"]!=""){
					$data[$result[0]["foco"]][] = array($semana,round(((double)$result[0]["valor"]),2));
				}
				}
			}
		}
		return $data;
	}
	// private function FoliarToken($token){
	// 	$sql_get_info = "SELECT * FROM informe WHERE token = '{$token}'";
 //    	$result = $this->conexion->Consultas(2, $sql_get_info);
 //    	$id_usuario = $result[0]["id_usuario"];
 //    	$id_cliente = $result[0]["id_cliente"];
 //    	$id_finca = $result[0]["id_hacienda"];

	// 	$sql_get_weeks = "SELECT WEEK(fecha) AS semana, YEAR(fecha) AS anio FROM foliar WHERE id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' GROUP BY WEEK(fecha) ORDER BY YEAR(fecha), WEEK(fecha)";
	// 	$weeks = $this->conexion->Consultas(2, $sql_get_weeks);
	// 	$sql_get_focos = "SELECT foco FROM foliar WHERE id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' GROUP BY foco";
	// 	$focos = $this->conexion->Consultas(2, $sql_get_focos);
	// 	foreach ($weeks as $key => $value) {
	// 		$semana = $weeks[$key]["semana"];
	// 		$anio = $weeks[$key]["anio"];

	// 		if($semana == 1){
	// 			$pre_semana = 52;
	// 			$pre_anio = $anio-1;
	// 		}else{
	// 			$pre_semana = $semana-1;
	// 			$pre_anio = $anio;
	// 		}

	// 		$count_foco=0;

	// 		foreach ($focos as $key2 => $foco) {
	// 			$sql = "
	// 				SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
	// 				FROM
	// 				(SELECT 
	// 				IF(actual.emision1 > pasada.emision1,(actual.emision1-pasada.emision1)/dif_dias*7,0) AS emision1,
	// 				IF(actual.emision2 > pasada.emision2,(actual.emision2-pasada.emision2)/dif_dias*7,0) AS emision2,
	// 				IF(actual.emision3 > pasada.emision3,(actual.emision3-pasada.emision3)/dif_dias*7,0) AS emision3,
	// 				IF(actual.emision4 > pasada.emision4,(actual.emision4-pasada.emision4)/dif_dias*7,0) AS emision4,
	// 				IF(actual.emision5 > pasada.emision5,(actual.emision5-pasada.emision5)/dif_dias*7,0) AS emision5,
	// 				IF(actual.emision6 > pasada.emision6,(actual.emision6-pasada.emision6)/dif_dias*7,0) AS emision6,
	// 				IF(actual.emision7 > pasada.emision7,(actual.emision7-pasada.emision7)/dif_dias*7,0) AS emision7,
	// 				IF(actual.emision8 > pasada.emision8,(actual.emision8-pasada.emision8)/dif_dias*7,0) AS emision8,
	// 				IF(actual.emision9 > pasada.emision9,(actual.emision9-pasada.emision9)/dif_dias*7,0) AS emision9,
	// 				IF(actual.emision10 > pasada.emision10,(actual.emision10-pasada.emision10)/dif_dias*7,0) AS emision10,
	// 				actual.foco
	// 				FROM (SELECT * FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS actual
	// 				JOIN (SELECT * FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS pasada
	// 				JOIN 
	// 					(SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
	// 					FROM
	// 						(SELECT *
	// 						FROM (SELECT MIN(fecha) AS dia_actual FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS actual
	// 						JOIN (SELECT MIN(fecha) AS dia_pasado FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS pasada) AS tbl_dif) tbl_dif_d) AS tabla
	// 				JOIN 
	// 					(SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
	// 					FROM
	// 						(SELECT IF(actual.emision1 > pasada.emision1,1,0) AS emision1,IF(actual.emision2 > pasada.emision2,1,0) AS emision2,IF(actual.emision3 > pasada.emision3,1,0) AS emision3,IF(actual.emision4 > pasada.emision4,1,0) AS emision4,IF(actual.emision5 > pasada.emision5,1,0) AS emision5,IF(actual.emision6 > pasada.emision6,1,0) AS emision6,IF(actual.emision7 > pasada.emision7,1,0) AS emision7,IF(actual.emision8 > pasada.emision8,1,0) AS emision8,IF(actual.emision9 > pasada.emision9,1,0) AS emision9,IF(actual.emision10 > pasada.emision10,1,0) AS emision10
	// 						FROM
	// 							(SELECT * FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS actual
	// 							JOIN (SELECT * FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS pasada)
	// 						AS tbl_suma)
	// 					AS tbl_division
	// 				";

	// 			echo "semana = $semana<br>";
	// 			echo $sql."<br><br>";

	// 			$result = $this->conexion->Consultas(2, $sql);
	// 			#echo "SEMANA $semana = ".json_encode($result)."<br>";
	// 			if($result[0]["foco"]!=""){
	// 				$data[$result[0]["foco"]][] = array($semana,round(((double)$result[0]["valor"]),2));
	// 			}
	// 		}
	// 	}
	// 	return $data;
	// }

	private function Foliar(){
		$sql_get_weeks = "SELECT WEEK(fecha) AS semana, YEAR(fecha) AS anio FROM foliar WHERE id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' GROUP BY WEEK(fecha) ORDER BY YEAR(fecha), WEEK(fecha)";
		print $sql_get_weeks;
		$weeks = $this->conexion->Consultas(2, $sql_get_weeks);
		$sql_get_focos = "SELECT foco FROM foliar WHERE id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' GROUP BY foco";
		$focos = $this->conexion->Consultas(2, $sql_get_focos);
		foreach ($weeks as $key => $value) {
			$semana = $weeks[$key]["semana"];
			$anio = $weeks[$key]["anio"];

			if($semana == 1){
				$pre_semana = 52;
				$pre_anio = $anio-1;
			}else{
				$pre_semana = $semana-1;
				$pre_anio = $anio;
			}

			$count_foco=0;

			foreach ($focos as $key2 => $foco) {
				if($key > 0){
				$sql = "
					SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
					FROM
					(SELECT 
					IF(actual.emision1 > pasada.emision1,(actual.emision1-pasada.emision1)/dif_dias*7,0) AS emision1,
					IF(actual.emision2 > pasada.emision2,(actual.emision2-pasada.emision2)/dif_dias*7,0) AS emision2,
					IF(actual.emision3 > pasada.emision3,(actual.emision3-pasada.emision3)/dif_dias*7,0) AS emision3,
					IF(actual.emision4 > pasada.emision4,(actual.emision4-pasada.emision4)/dif_dias*7,0) AS emision4,
					IF(actual.emision5 > pasada.emision5,(actual.emision5-pasada.emision5)/dif_dias*7,0) AS emision5,
					IF(actual.emision6 > pasada.emision6,(actual.emision6-pasada.emision6)/dif_dias*7,0) AS emision6,
					IF(actual.emision7 > pasada.emision7,(actual.emision7-pasada.emision7)/dif_dias*7,0) AS emision7,
					IF(actual.emision8 > pasada.emision8,(actual.emision8-pasada.emision8)/dif_dias*7,0) AS emision8,
					IF(actual.emision9 > pasada.emision9,(actual.emision9-pasada.emision9)/dif_dias*7,0) AS emision9,
					IF(actual.emision10 > pasada.emision10,(actual.emision10-pasada.emision10)/dif_dias*7,0) AS emision10,
					actual.foco
					FROM (SELECT * FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS actual
					JOIN (SELECT * FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS pasada
					JOIN 
						(SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
						FROM
							(SELECT *
							FROM (SELECT MIN(fecha) AS dia_actual FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS actual
							JOIN (SELECT MIN(fecha) AS dia_pasado FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS pasada) AS tbl_dif) tbl_dif_d) AS tabla
					JOIN 
						(SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
						FROM
							(SELECT IF(actual.emision1 > pasada.emision1,1,0) AS emision1,IF(actual.emision2 > pasada.emision2,1,0) AS emision2,IF(actual.emision3 > pasada.emision3,1,0) AS emision3,IF(actual.emision4 > pasada.emision4,1,0) AS emision4,IF(actual.emision5 > pasada.emision5,1,0) AS emision5,IF(actual.emision6 > pasada.emision6,1,0) AS emision6,IF(actual.emision7 > pasada.emision7,1,0) AS emision7,IF(actual.emision8 > pasada.emision8,1,0) AS emision8,IF(actual.emision9 > pasada.emision9,1,0) AS emision9,IF(actual.emision10 > pasada.emision10,1,0) AS emision10
							FROM
								(SELECT * FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS actual
								JOIN (SELECT * FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS pasada)
							AS tbl_suma)
						AS tbl_division
					";

				#echo "semana = $semana<br>";
				#echo $sql."<br><br>";

				$result = $this->conexion->Consultas(2, $sql);

				#echo "SEMANA $semana = ".json_encode($result)."<br>";
				if($result[0]["foco"]!=""){
					$data[$result[0]["foco"]][] = array($semana,round(((double)$result[0]["valor"]),2));
				}
				}
			}
		}
		return $data;
	}

	private function FoliarToken2($token){
		$sql_get_info = "SELECT * FROM informe WHERE token = '{$token}'";
    	$result = $this->conexion->Consultas(2, $sql_get_info);
    	$id_usuario = $result[0]["id_usuario"];
    	$id_cliente = $result[0]["id_cliente"];
    	$id_finca = $result[0]["id_hacienda"];

		$sql_get_weeks = "SELECT WEEK(fecha) AS semana, YEAR(fecha) AS anio FROM foliar WHERE id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' GROUP BY WEEK(fecha) ORDER BY YEAR(fecha), WEEK(fecha)";
		$weeks = $this->conexion->Consultas(2, $sql_get_weeks);
		$sql_get_focos = "SELECT foco FROM foliar WHERE id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' GROUP BY foco";
		$focos = $this->conexion->Consultas(2, $sql_get_focos);
		foreach ($weeks as $key => $value) {
			if($key>0){
				$semana = $weeks[$key]["semana"];
				$anio = $weeks[$key]["anio"];

				if($semana == 1){
					$pre_semana = 52;
					$pre_anio = $anio-1;
				}else{
					$pre_semana = $semana-1;
					$pre_anio = $anio;
				}

				$count_foco=0;

				foreach ($focos as $key2 => $foco) {
					$sql = "
						SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
						FROM
						(SELECT 
						IF(actual.emision1 > pasada.emision1,(actual.emision1-pasada.emision1)/dif_dias*7,0) AS emision1,
						IF(actual.emision2 > pasada.emision2,(actual.emision2-pasada.emision2)/dif_dias*7,0) AS emision2,
						IF(actual.emision3 > pasada.emision3,(actual.emision3-pasada.emision3)/dif_dias*7,0) AS emision3,
						IF(actual.emision4 > pasada.emision4,(actual.emision4-pasada.emision4)/dif_dias*7,0) AS emision4,
						IF(actual.emision5 > pasada.emision5,(actual.emision5-pasada.emision5)/dif_dias*7,0) AS emision5,
						IF(actual.emision6 > pasada.emision6,(actual.emision6-pasada.emision6)/dif_dias*7,0) AS emision6,
						IF(actual.emision7 > pasada.emision7,(actual.emision7-pasada.emision7)/dif_dias*7,0) AS emision7,
						IF(actual.emision8 > pasada.emision8,(actual.emision8-pasada.emision8)/dif_dias*7,0) AS emision8,
						IF(actual.emision9 > pasada.emision9,(actual.emision9-pasada.emision9)/dif_dias*7,0) AS emision9,
						IF(actual.emision10 > pasada.emision10,(actual.emision10-pasada.emision10)/dif_dias*7,0) AS emision10,
						actual.foco
						FROM (SELECT * FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS actual
						JOIN (SELECT * FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS pasada
						JOIN 
							(SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
							FROM
								(SELECT *
								FROM (SELECT MIN(fecha) AS dia_actual FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS actual
								JOIN (SELECT MIN(fecha) AS dia_pasado FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS pasada) AS tbl_dif) tbl_dif_d) AS tabla
						JOIN 
							(SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
							FROM
								(SELECT IF(actual.emision1 > pasada.emision1,1,0) AS emision1,IF(actual.emision2 > pasada.emision2,1,0) AS emision2,IF(actual.emision3 > pasada.emision3,1,0) AS emision3,IF(actual.emision4 > pasada.emision4,1,0) AS emision4,IF(actual.emision5 > pasada.emision5,1,0) AS emision5,IF(actual.emision6 > pasada.emision6,1,0) AS emision6,IF(actual.emision7 > pasada.emision7,1,0) AS emision7,IF(actual.emision8 > pasada.emision8,1,0) AS emision8,IF(actual.emision9 > pasada.emision9,1,0) AS emision9,IF(actual.emision10 > pasada.emision10,1,0) AS emision10
								FROM
									(SELECT * FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS actual
									JOIN (SELECT * FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$id_finca}' AND id_usuario = '{$id_usuario}' AND id_cliente = '{$id_cliente}' AND foco = '".$foco["foco"]."') AS pasada)
								AS tbl_suma)
							AS tbl_division
						";

					#echo "semana = $semana<br>";
					#echo $sql."<br><br>";

					$result = $this->conexion->Consultas(2, $sql);
					#echo "SEMANA $semana = ".json_encode($result)."<br>";
					if($result[0]["foco"]!=""){
						$count_foco++;
						if(isset($data["foco"][$key]))
							$data["foco"][$key] = array($semana+1, $data["foco"][$key][1]+round(((double)$result[0]["valor"]),2));
						else
							$data["foco"][$key] = array($semana+1, round(((double)$result[0]["valor"]),2));
						#$data[$result[0]["foco"]][] = $result[0]["valor"];
					}
					/*if($key2+1 == count($focos)){
						$acum["foco"][$key] /= $count_foco;
						$data["foco"][] = array($semana, $acum["foco"][$key]);
					}*/
				}
				if($count_foco > 1)
					$data["foco"][$key][1] = $data["foco"][$key][1]/$count_foco;
			}else{
				$datos["foco"][] = array("1", 0);
			}
		}
		foreach ($data["foco"] as $key => $value) {
			$datos["foco"][] = $value;
		}
		return $datos;
	}

	private function Foliar2(){
		$sql_get_weeks = "SELECT WEEK(fecha) AS semana, YEAR(fecha) AS anio FROM foliar WHERE id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' GROUP BY WEEK(fecha) ORDER BY YEAR(fecha), WEEK(fecha)";
		$weeks = $this->conexion->Consultas(2, $sql_get_weeks);
		$sql_get_focos = "SELECT foco FROM foliar WHERE id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' GROUP BY foco";
		$focos = $this->conexion->Consultas(2, $sql_get_focos);
		foreach ($weeks as $key => $value) {
			if($key>0){
				$semana = $weeks[$key]["semana"];
				$anio = $weeks[$key]["anio"];

				if($semana == 1){
					$pre_semana = 52;
					$pre_anio = $anio-1;
				}else{
					$pre_semana = $semana-1;
					$pre_anio = $anio;
				}

				$count_foco=0;

				foreach ($focos as $key2 => $foco) {
					if($key > 0){
					$sql = "
						SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
						FROM
						(SELECT 
						IF(actual.emision1 > pasada.emision1,(actual.emision1-pasada.emision1)/dif_dias*7,0) AS emision1,
						IF(actual.emision2 > pasada.emision2,(actual.emision2-pasada.emision2)/dif_dias*7,0) AS emision2,
						IF(actual.emision3 > pasada.emision3,(actual.emision3-pasada.emision3)/dif_dias*7,0) AS emision3,
						IF(actual.emision4 > pasada.emision4,(actual.emision4-pasada.emision4)/dif_dias*7,0) AS emision4,
						IF(actual.emision5 > pasada.emision5,(actual.emision5-pasada.emision5)/dif_dias*7,0) AS emision5,
						IF(actual.emision6 > pasada.emision6,(actual.emision6-pasada.emision6)/dif_dias*7,0) AS emision6,
						IF(actual.emision7 > pasada.emision7,(actual.emision7-pasada.emision7)/dif_dias*7,0) AS emision7,
						IF(actual.emision8 > pasada.emision8,(actual.emision8-pasada.emision8)/dif_dias*7,0) AS emision8,
						IF(actual.emision9 > pasada.emision9,(actual.emision9-pasada.emision9)/dif_dias*7,0) AS emision9,
						IF(actual.emision10 > pasada.emision10,(actual.emision10-pasada.emision10)/dif_dias*7,0) AS emision10,
						actual.foco
						FROM (SELECT * FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS actual
						JOIN (SELECT * FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS pasada
						JOIN 
							(SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
							FROM
								(SELECT *
								FROM (SELECT MIN(fecha) AS dia_actual FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS actual
								JOIN (SELECT MIN(fecha) AS dia_pasado FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS pasada) AS tbl_dif) tbl_dif_d) AS tabla
						JOIN 
							(SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
							FROM
								(SELECT IF(actual.emision1 > pasada.emision1,1,0) AS emision1,IF(actual.emision2 > pasada.emision2,1,0) AS emision2,IF(actual.emision3 > pasada.emision3,1,0) AS emision3,IF(actual.emision4 > pasada.emision4,1,0) AS emision4,IF(actual.emision5 > pasada.emision5,1,0) AS emision5,IF(actual.emision6 > pasada.emision6,1,0) AS emision6,IF(actual.emision7 > pasada.emision7,1,0) AS emision7,IF(actual.emision8 > pasada.emision8,1,0) AS emision8,IF(actual.emision9 > pasada.emision9,1,0) AS emision9,IF(actual.emision10 > pasada.emision10,1,0) AS emision10
								FROM
									(SELECT * FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS actual
									JOIN (SELECT * FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]["semana"]." AND YEAR(fecha) = $pre_anio AND id_hacienda = '{$this->session->finca}' AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND foco = '".$foco["foco"]."') AS pasada)
								AS tbl_suma)
							AS tbl_division
						";

					#echo "semana = $semana<br>";
					#echo $sql."<br><br>";

					$result = $this->conexion->Consultas(2, $sql);
					#echo "SEMANA $semana = ".json_encode($result)."<br>";
					if($result[0]["foco"]!=""){
						$count_foco++;
						if(isset($data["foco"][$key]))
							$data["foco"][$key] = array($semana+1, $data["foco"][$key][1]+round(((double)$result[0]["valor"]),2));
						else
							$data["foco"][$key] = array($semana+1, round(((double)$result[0]["valor"]),2));
						#$data[$result[0]["foco"]][] = $result[0]["valor"];
					}
					/*if($key2+1 == count($focos)){
						$acum["foco"][$key] /= $count_foco;
						$data["foco"][] = array($semana, $acum["foco"][$key]);
					}*/
					}
				}
				if($count_foco > 1)
					$data["foco"][$key][1] = $data["foco"][$key][1]/$count_foco;
			}else{
				$datos["foco"][] = array("1", 0);
			}
		}
		foreach ($data["foco"] as $key => $value) {
			$datos["foco"][] = $value;
		}
		return $datos;
	}

	/*private function FoliarToken($token){
		$sql_get_info = "SELECT * FROM informe WHERE token = '{$token}'";
    	$result = $this->conexion->Consultas(2, $sql_get_info);
    	$id_usuario = $result[0]["id_usuario"];
    	$id_cliente = $result[0]["id_cliente"];
    	$id_finca = $result[0]["id_hacienda"];
		$sql = "SELECT 
				currentFoliar.iterator,
				currentFoliar.id,
				WEEK(currentFoliar.fecha) AS semana,
				currentFoliar.foco,
				currentFoliar.fecha,
				@post_emision1 := 
				(
					SELECT emision1 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision1
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$id_usuario}' 
								AND id_cliente = '{$id_cliente}' 
								AND id_hacienda = '{$id_finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision1,
				@post_emision2 := 
				(
					SELECT emision2
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision2
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$id_usuario}' 
								AND id_cliente = '{$id_cliente}' 
								AND id_hacienda = '{$id_finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision2,
				@post_emision3 := 
				(
					SELECT emision3 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision3
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$id_usuario}' 
								AND id_cliente = '{$id_cliente}' 
								AND id_hacienda = '{$id_finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision3,
				@post_emision4 := 
				(
					SELECT emision4 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision4
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$id_usuario}' 
								AND id_cliente = '{$id_cliente}' 
								AND id_hacienda = '{$id_finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision4,
				@post_emision5 := 
				(
					SELECT emision5 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision5
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$id_usuario}' 
								AND id_cliente = '{$id_cliente}' 
								AND id_hacienda = '{$id_finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision5,
				@post_emision6 := 
				(
					SELECT emision6 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision6
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$id_usuario}' 
								AND id_cliente = '{$id_cliente}' 
								AND id_hacienda = '{$id_finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision6,
				@post_emision7 := 
				(
					SELECT emision7 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision7
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$id_usuario}' 
								AND id_cliente = '{$id_cliente}' 
								AND id_hacienda = '{$id_finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision7,
				@post_emision8 := 
				(
					SELECT emision8 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision8
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$id_usuario}' 
								AND id_cliente = '{$id_cliente}' 
								AND id_hacienda = '{$id_finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision8,
				@post_emision9 := 
				(
					SELECT emision9 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision9
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$id_usuario}' 
								AND id_cliente = '{$id_cliente}' 
								AND id_hacienda = '{$id_finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision9,
				@post_emision10 := 
				(
					SELECT emision10 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision10
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$id_usuario}' 
								AND id_cliente = '{$id_cliente}' 
								AND id_hacienda = '{$id_finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision10
				,if(@post_emision1 > currentFoliar.emision1 , -1 , currentFoliar.emision1) AS rlEmision1
				,if(@post_emision2 > currentFoliar.emision2 , -1 , currentFoliar.emision2) AS rlEmision2
				,if(@post_emision3 > currentFoliar.emision3 , -1 , currentFoliar.emision3) AS rlEmision3
				,if(@post_emision4 > currentFoliar.emision4 , -1 , currentFoliar.emision4) AS rlEmision4
				,if(@post_emision5 > currentFoliar.emision5 , -1 , currentFoliar.emision5) AS rlEmision5
				,if(@post_emision6 > currentFoliar.emision6 , -1 , currentFoliar.emision6) AS rlEmision6
				,if(@post_emision7 > currentFoliar.emision7 , -1 , currentFoliar.emision7) AS rlEmision7
				,if(@post_emision8 > currentFoliar.emision8 , -1 , currentFoliar.emision8) AS rlEmision8
				,if(@post_emision9 > currentFoliar.emision9 , -1 , currentFoliar.emision9) AS rlEmision9
				,if(@post_emision10 > currentFoliar.emision10 , -1 , currentFoliar.emision10) AS rlEmision10
				,if(DATEDIFF(currentFoliar.fecha ,
					(
					SELECT fecha 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha 
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$id_usuario}' 
								AND id_cliente = '{$id_cliente}' 
								AND id_hacienda = '{$id_finca}'
								ORDER BY foco,YEAR(fecha),WEEK(fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY foco,YEAR(fecha),WEEK(fecha)
					)
				) > 0 , DATEDIFF(currentFoliar.fecha ,
					(
					SELECT fecha 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha 
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$id_usuario}' 
								AND id_cliente = '{$id_cliente}' 
								AND id_hacienda = '{$id_finca}'
								ORDER BY foco,YEAR(fecha),WEEK(fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY foco,YEAR(fecha),WEEK(fecha)
					)
				), 0) as DiasDif,
				currentFoliar.id_usuario
				FROM 
				(
					SELECT @i:=@i+1 AS iterator, 
							id ,
							id_usuario,
							id_cliente,
							id_hacienda,
							WEEK(fecha) AS semana,
							foco,
							emision1,
							emision2,
							emision3,
							emision4,
							emision5,
							emision6,
							emision7,
							emision8,
							emision9,
							emision10 , 
							fecha
				 			FROM foliar ,(SELECT @i:=0) AS foo 
				 			WHERE 
				 			id_usuario = '{$id_usuario}' 
								AND id_cliente = '{$id_cliente}' 
								AND id_hacienda = '{$id_finca}'
					ORDER BY foco,YEAR(fecha),semana
				) as currentFoliar
				WHERE 
				currentFoliar.id_usuario = '{$id_usuario}' 
				AND currentFoliar.id_cliente = '{$id_cliente}' 
				AND currentFoliar.id_hacienda = '{$id_finca}'
				GROUP BY WEEK(currentFoliar.fecha),currentFoliar.foco
				ORDER BY foco,YEAR(fecha),semana";
		$weeks = $this->conexion->Consultas(2, $sql);
		$SubFoco = [];
		$TotFoco = [];
		$contador = [];
		$limitEmision = 10;
		foreach ($weeks as $key => $value) {
			$contador[$value["foco"]]++;
			if($contador[$value["foco"]] > 1){
				$SubFoco[$value["foco"]]["avg"] = 0;
				$SubFoco[$value["foco"]]["sum"] = 0;
				for ($i=1; $i <= $limitEmision; $i++) { 
					if($value["rlEmision{$i}"] >= 0 && $value["DiasDif"] > 0){
						$SubFoco[$value["foco"]]["sum"] += (($value["rlEmision{$i}"] - $value["@post_emision{i}"]) / $value["DiasDif"]);
						$SubFoco[$value["foco"]]["avg"]++;
					}
				}
				$TotFoco[$value["foco"]][] = [$value["semana"],($SubFoco[$value["foco"]]["sum"]/$SubFoco[$value["foco"]]["avg"])];
			}
		}
		return $TotFoco;
	}

	private function Foliar(){
		$sql = "SELECT 
				currentFoliar.iterator,
				currentFoliar.id,
				WEEK(currentFoliar.fecha) AS semana,
				currentFoliar.foco,
				currentFoliar.fecha,
				@post_emision1 := 
				(
					SELECT emision1 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision1
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$this->session->logged}' 
								AND id_cliente = '{$this->session->client}' 
								AND id_hacienda = '{$this->session->finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision1,
				@post_emision2 := 
				(
					SELECT emision2
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision2
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$this->session->logged}' 
								AND id_cliente = '{$this->session->client}' 
								AND id_hacienda = '{$this->session->finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision2,
				@post_emision3 := 
				(
					SELECT emision3 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision3
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$this->session->logged}' 
								AND id_cliente = '{$this->session->client}' 
								AND id_hacienda = '{$this->session->finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision3,
				@post_emision4 := 
				(
					SELECT emision4 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision4
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$this->session->logged}' 
								AND id_cliente = '{$this->session->client}' 
								AND id_hacienda = '{$this->session->finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision4,
				@post_emision5 := 
				(
					SELECT emision5 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision5
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$this->session->logged}' 
								AND id_cliente = '{$this->session->client}' 
								AND id_hacienda = '{$this->session->finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision5,
				@post_emision6 := 
				(
					SELECT emision6 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision6
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$this->session->logged}' 
								AND id_cliente = '{$this->session->client}' 
								AND id_hacienda = '{$this->session->finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision6,
				@post_emision7 := 
				(
					SELECT emision7 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision7
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$this->session->logged}' 
								AND id_cliente = '{$this->session->client}' 
								AND id_hacienda = '{$this->session->finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision7,
				@post_emision8 := 
				(
					SELECT emision8 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision8
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$this->session->logged}' 
								AND id_cliente = '{$this->session->client}' 
								AND id_hacienda = '{$this->session->finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision8,
				@post_emision9 := 
				(
					SELECT emision9 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision9
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$this->session->logged}' 
								AND id_cliente = '{$this->session->client}' 
								AND id_hacienda = '{$this->session->finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision9,
				@post_emision10 := 
				(
					SELECT emision10 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha ,emision10
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$this->session->logged}' 
								AND id_cliente = '{$this->session->client}' 
								AND id_hacienda = '{$this->session->finca}'
								ORDER BY foco,YEAR(foliar.fecha),WEEK(foliar.fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY preFoliar.foco,YEAR(preFoliar.fecha),WEEK(preFoliar.fecha)
				) as post_emision10
				,if(@post_emision1 > currentFoliar.emision1 , -1 , currentFoliar.emision1) AS rlEmision1
				,if(@post_emision2 > currentFoliar.emision2 , -1 , currentFoliar.emision2) AS rlEmision2
				,if(@post_emision3 > currentFoliar.emision3 , -1 , currentFoliar.emision3) AS rlEmision3
				,if(@post_emision4 > currentFoliar.emision4 , -1 , currentFoliar.emision4) AS rlEmision4
				,if(@post_emision5 > currentFoliar.emision5 , -1 , currentFoliar.emision5) AS rlEmision5
				,if(@post_emision6 > currentFoliar.emision6 , -1 , currentFoliar.emision6) AS rlEmision6
				,if(@post_emision7 > currentFoliar.emision7 , -1 , currentFoliar.emision7) AS rlEmision7
				,if(@post_emision8 > currentFoliar.emision8 , -1 , currentFoliar.emision8) AS rlEmision8
				,if(@post_emision9 > currentFoliar.emision9 , -1 , currentFoliar.emision9) AS rlEmision9
				,if(@post_emision10 > currentFoliar.emision10 , -1 , currentFoliar.emision10) AS rlEmision10
				,if(DATEDIFF(currentFoliar.fecha ,
					(
					SELECT fecha 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha 
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$this->session->logged}' 
								AND id_cliente = '{$this->session->client}' 
								AND id_hacienda = '{$this->session->finca}'
								ORDER BY foco,YEAR(fecha),WEEK(fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY foco,YEAR(fecha),WEEK(fecha)
					)
				) > 0 , DATEDIFF(currentFoliar.fecha ,
					(
					SELECT fecha 
						FROM 
							(
								SELECT @x:=@x+1 AS iterator,foco , fecha 
								FROM foliar ,(SELECT @x:=0) AS foo 
								WHERE 
								id_usuario = '{$this->session->logged}' 
								AND id_cliente = '{$this->session->client}' 
								AND id_hacienda = '{$this->session->finca}'
								ORDER BY foco,YEAR(fecha),WEEK(fecha)
							) as preFoliar
					WHERE 
					preFoliar.iterator = currentFoliar.iterator - 1
					ORDER BY foco,YEAR(fecha),WEEK(fecha)
					)
				), 0) as DiasDif,
				currentFoliar.id_usuario
				FROM 
				(
					SELECT @i:=@i+1 AS iterator, 
							id ,
							id_usuario,
							id_cliente,
							id_hacienda,
							WEEK(fecha) AS semana,
							foco,
							emision1,
							emision2,
							emision3,
							emision4,
							emision5,
							emision6,
							emision7,
							emision8,
							emision9,
							emision10 , 
							fecha
				 			FROM foliar ,(SELECT @i:=0) AS foo 
				 			WHERE 
				 			id_usuario = '{$this->session->logged}' 
				 			AND id_cliente = '{$this->session->client}'
				 			 AND id_hacienda = '{$this->session->finca}'
					ORDER BY foco,YEAR(fecha),semana
				) as currentFoliar
				WHERE 
				currentFoliar.id_usuario = '{$this->session->logged}' 
				AND currentFoliar.id_cliente = '{$this->session->client}' 
				AND currentFoliar.id_hacienda = '{$this->session->finca}'
				GROUP BY WEEK(currentFoliar.fecha),currentFoliar.foco
				ORDER BY foco,YEAR(fecha),semana";
		$weeks = $this->conexion->Consultas(2, $sql);
		$SubFoco = [];
		$TotFoco = [];
		$contador = [];
		$limitEmision = 10;
		foreach ($weeks as $key => $value) {
			$contador[$value["foco"]]++;
			if($contador[$value["foco"]] > 1){
				$SubFoco[$value["foco"]]["avg"] = 0;
				$SubFoco[$value["foco"]]["sum"] = 0;
				if($value["rlEmision1"] > 0 && $value["DiasDif"] > 0){
					// $SubFoco[$value["foco"]]["sum"] = (((($value["rlEmision1"] - $value["post_emision1"]) / $value["DiasDif"]))*7);
					$SubFoco[$value["foco"]]["sum"] += (($value["rlEmision1"]-$value["post_emision1"])/$value["DiasDif"])*7;
					$SubFoco[$value["foco"]]["avg"]++;
				}
				if($value["rlEmision2"] > 0 && $value["DiasDif"] > 0){
					$SubFoco[$value["foco"]]["sum"] += (7*((($value["rlEmision2"] - $value["post_emision2"]) / $value["DiasDif"])));
					$SubFoco[$value["foco"]]["avg"]++;
				}if($value["rlEmision2"] > 0 && $value["DiasDif"] > 0){
					$SubFoco[$value["foco"]]["sum"] += (7*((($value["rlEmision2"] - $value["post_emision2"]) / $value["DiasDif"])));
					$SubFoco[$value["foco"]]["avg"]++;
				}if($value["rlEmision3"] > 0 && $value["DiasDif"] > 0){
					$SubFoco[$value["foco"]]["sum"] += (7*((($value["rlEmision3"] - $value["post_emision3"]) / $value["DiasDif"])));
					$SubFoco[$value["foco"]]["avg"]++;
				}if($value["rlEmision4"] > 0 && $value["DiasDif"] > 0){
					$SubFoco[$value["foco"]]["sum"] += (7*((($value["rlEmision4"] - $value["post_emision4"]) / $value["DiasDif"])));
					$SubFoco[$value["foco"]]["avg"]++;
				}if($value["rlEmision5"] > 0 && $value["DiasDif"] > 0){
					$SubFoco[$value["foco"]]["sum"] += (7*((($value["rlEmision5"] - $value["post_emision5"]) / $value["DiasDif"])));
					$SubFoco[$value["foco"]]["avg"]++;
				}if($value["rlEmision6"] > 0 && $value["DiasDif"] > 0){
					$SubFoco[$value["foco"]]["sum"] += (7*((($value["rlEmision6"] - $value["post_emision6"]) / $value["DiasDif"])));
					$SubFoco[$value["foco"]]["avg"]++;
				}if($value["rlEmision8"] > 0 && $value["DiasDif"] > 0){
					$SubFoco[$value["foco"]]["sum"] += (7*((($value["rlEmision8"] - $value["post_emision8"]) / $value["DiasDif"])));
					$SubFoco[$value["foco"]]["avg"]++;
				}if($value["rlEmision9"] > 0 && $value["DiasDif"] > 0){
					$SubFoco[$value["foco"]]["sum"] += (7*((($value["rlEmision9"] - $value["post_emision9"]) / $value["DiasDif"])));
					$SubFoco[$value["foco"]]["avg"]++;
				}if($value["rlEmision10"] > 0 && $value["DiasDif"] > 0){
					$SubFoco[$value["foco"]]["sum"] += (7*((($value["rlEmision10"] - $value["post_emision10"]) / $value["DiasDif"])));
					$SubFoco[$value["foco"]]["avg"]++;
				}
				// for ($i=1; $i <= $limitEmision; $i++) { 
				// 	if($value["rlEmision{$i}"] >= 0 && $value["DiasDif"] > 0){
				// 		/// SUM(((fomular Semana actual - semana anterior ) / diferencia de dias ) * 7)/ellos
				// 		$SubFoco[$value["foco"]]["sum"] += (7*((($value["rlEmision{$i}"] - $value["post_emision{$i}"]) / $value["DiasDif"])));
				// 	}
				// }
				// print $SubFoco[$value["foco"]]["avg"];
				$TotFoco[$value["foco"]][] = [$value["semana"],($SubFoco[$value["foco"]]["sum"]/$SubFoco[$value["foco"]]["avg"])];
				// $TotFoco[$value["foco"]][] = [$value["semana"],($SubFoco[$value["foco"]]["sum"]/$SubFoco[$value["foco"]]["avg"])];
				// print $value["rlEmision1"]."<br>";
				// print $value["post_emision1"]."<br>";
				// print $emision1."<br>";
			}
		}
		return $TotFoco;
	}*/

	public function Grafica(){
		$resul = array();
		$resul = $this->Foliar();
		return json_encode($resul);
	}
	
	public function GraficaToken($token){
		$resul = array();
		$resul = $this->FoliarTokenJavi($token);
		return json_encode($resul);
	}	
}
$postdata = (object)json_decode(file_get_contents("php://input"));
if($postdata->opt == "GRAFICA"){ 
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_C();
			echo $retval->GraficaToken($postdata->token);
		}	
	}else{
		$retval = new Graficas_S_C();
		echo $retval->Grafica();
	}
}
?>