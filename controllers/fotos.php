<?php

	/**
	*  CLASS FROM Fincas
	*/
	class fotos 
	{
		private $conexion;
		private $session;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		public function getPhotos(){
			// $sWhere = "";
			// $sOrder = " ORDER BY id";
			// $DesAsc = "ASC";
			// $sOrder .= " {$DesAsc}";
			// if(isset($_POST)){

			// 	/*----------  ORDER BY ----------*/
				
			// 	if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
			// 		$DesAsc = $_POST['order'][0]['dir'];
			// 		$sOrder = " ORDER BY id {$DesAsc}";
			// 	}
			// 	if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
			// 		$DesAsc = $_POST['order'][0]['dir'];
			// 		$sOrder = " ORDER BY (SELECT nombre from cat_haciendas WHERE id = cat_lotes.id_cliente) {$DesAsc}";
			// 	}
			// 	if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
			// 		$DesAsc = $_POST['order'][0]['dir'];
			// 		$sOrder = " ORDER BY nombre {$DesAsc}";
			// 	}
			// 	if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
			// 		$DesAsc = $_POST['order'][0]['dir'];
			// 		$sOrder = " ORDER BY email {$DesAsc}";
			// 	}
			// 	if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
			// 		$DesAsc = $_POST['order'][0]['dir'];
			// 		$sOrder = " ORDER BY status {$DesAsc}";
			// 	}
			// 	/*----------  ORDER BY ----------*/

			// 	if(isset($_POST['search_id']) && trim($_POST['search_id']) != ""){
			// 		$sWhere .= " AND id = ".$_POST["search_id"];
			// 	}				
			// 	if((isset($_POST['search_finca']) && trim($_POST['search_finca']) != "")){
			// 		$sWhere .= " AND (SELECT nombre from cat_haciendas WHERE id = cat_lotes.id_cliente) LIKE '%".$_POST['search_finca']."%'";
			// 	}
			// 	if(isset($_POST['search_name']) && trim($_POST['search_name']) != ""){
			// 		$sWhere .= " AND nombre LIKE '%".$_POST['search_name']."%'";
			// 	}
			// 	if(isset($_POST['search_email']) && trim($_POST['search_email']) != ""){
			// 		$sWhere .= " AND email LIKE '%".$_POST['search_email']."%'";
			// 	}
			// 	if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
			// 		$sWhere .= " AND status = ".$_POST["order_status"];
			// 	}
			// }

			$sql = "SELECT malezas , evi_malezas , deshoje ,evi_deshoje ,
							deshojefito,evi_deshojefito ,enfunde,evi_enfunde,
							drenaje , evi_drenaje, plaga , evi_plaga , otrasobs , evi_otrasobs ,getWeek(fecha) AS Semana
					FROM foliar
					WHERE id_usuario IN ({$this->session->logges->users}) AND id_cliente = '{$this->session->client}' 
					AND YEAR(fecha) = '{$this->session->year}' AND id_hacienda = '{$this->session->finca}'
					ORDER BY fecha DESC";
					// echo $sql;
			$fotos = $this->conexion->Consultas(2, $sql);
			$datos = (object)[
					'fotos' => [],
					'semana' => []
			];
			$sem = [];
			foreach ($fotos as $key => $value) {
				$fila = (object)$fila;
				if(!is_null($value['malezas']) && $value['malezas'] != "" && $value['malezas'] != ''){
					$value['evi_malezas'] = trim($value['evi_malezas']);
					$datos->fotos['malezas'][] = [
						'semana' => $value["Semana"],
						'type' => 'Malezas',
                        'description' => $value['malezas'],
                        'fotos' => explode('|', $value['evi_malezas'])
						/*'fotos' => is_array($value['evi_malezas'])
									? $value['evi_malezas'] 
									: ($value['evi_malezas'] != "" && $value['evi_malezas'] != '(NULL)') 
										? explode("_", $value['evi_malezas'])[0]."_Evidencia Malezas_1.".explode(".", $value['evi_malezas'])[1]
										: [""]*/
					];
					$datos->semana[] = $value["Semana"];
				}
				if(!is_null($value['deshoje']) && $value['deshoje'] != "" && $value['deshoje'] != ''){
					$value['evi_deshoje'] = trim($value['evi_deshoje']);
					$datos->fotos['deshoje'][] = [
						'semana' => $value["Semana"],
						'type' => 'Deshoje',
                        'description' => $value['deshoje'],
                        'fotos' => explode('|', $value['evi_deshoje'])
						/*'fotos' => is_array($value['evi_deshoje'])
										? $value['evi_deshoje'] 
										: ($value['evi_deshoje'] != "" && $value['evi_deshoje'] != "(NULL)") 
											? explode("_", $value['evi_deshoje'])[0]."_Evidencia Deshoje_1.".explode(".", $value['evi_deshoje'])[1]
											: [""]*/
					];
					$datos->semana[] = $value["Semana"];
				}
				if(!is_null($value['deshojefito']) && $value['deshojefito'] != "" && $value['deshojefito'] != ''){
					$value['evi_deshojefito'] = trim($value['evi_deshojefito']);
					$datos->fotos['deshojefito'][] = [
						'semana' => $value["Semana"],
						'type' => 'Deshojefito',
                        'description' => $value['deshojefito'],
                        'fotos' => explode('|', $value['evi_deshojefito'])
						/*'fotos' => is_array($value['evi_deshojefito'])
										? $value['evi_deshojefito'] 
										: ($value['evi_deshojefito'] != "" && $value['evi_deshojefito'] != "(NULL)") 
											? (file_exists(explode("_", $value['evi_deshojefito'])[0]."_Desh Fito_1.".explode(".", $value['evi_deshojefito'])[1]))
												? explode("_", $value['evi_deshojefito'])[0]."_Desh Fito_1.".explode(".", $value['evi_deshojefito'])[1]
												: explode("_", $value['evi_deshojefito'])[0]."_EDF_1.".explode(".", $value['evi_deshojefito'])[1]
											: [""]*/
					];
					$datos->semana[] = $value["Semana"];
				}
				if(!is_null($value['enfunde']) && $value['enfunde'] != "" && $value['enfunde'] != ''){
					$value['evi_enfunde'] = trim($value['evi_enfunde']);
					$datos->fotos['enfunde'][] = [
						'semana' => $value["Semana"],
						'type' => 'Enfunde',
                        'description' => $value['enfunde'],
                        'fotos' => explode('|', $value['evi_enfunde'])
						/*'fotos' => is_array($value['evi_enfunde'])
										? $value['evi_enfunde'] 
										: ($value['evi_enfunde'] != "" && $value['evi_enfunde'] != "(NULL)") 
											? explode("_", $value['evi_enfunde'])[0]."_Evidencia Enfunde_1.".explode(".", $value['evi_enfunde'])[1]
											: [""]*/
					];
					$datos->semana[] = $value["Semana"];
				}
				if(!is_null($value['drenaje']) && $value['drenaje'] != "" && $value['drenaje'] != ''){
					$value['evi_drenaje'] = trim($value['evi_drenaje']);
					$datos->fotos['drenaje'][] = [
						'semana' => $value["Semana"],
						'type' => 'Drenaje',
                        'description' => $value['drenaje'],
                        'fotos' => explode('|', $value['evi_drenaje'])
						/*'fotos' => is_array($value['evi_drenaje'])
										? $value['evi_drenaje'] 
										: ($value['evi_drenaje'] != "" && $value['evi_drenaje'] != "(NULL)") 
											? explode("_", $value['evi_drenaje'])[0]."_Evidencia Drenajes_1.".explode(".", $value['evi_drenaje'])[1]
											: [""]*/
					];
					$datos->semana[] = $value["Semana"];
				}
				if(!is_null($value['plaga']) && $value['plaga'] != "" && $value['plaga'] != ''){
					$value['evi_plaga'] = trim($value['evi_plaga']);
					$datos->fotos['plaga'][] = [
						'semana' => $value["Semana"],
						'type' => 'Plaga',
						'description' => $value['plaga'],
						'fotos' => is_array($value['evi_plaga'])
										? $value['evi_plaga'] 
										: ($value['evi_plaga'] != "" && $value['evi_plaga'] != "(NULL)") 
											? explode("_", $value['evi_plaga'])[0]."_EPf_1.".explode(".", $value['evi_plaga'])[1]
											: [""]
					];
					$datos->semana[] = $value["Semana"];
				}
				if(!is_null($value['otrasobs']) && $value['otrasobs'] != "" && $value['otrasobs'] != ''){
					$value['evi_otrasobs'] = trim($value['evi_otrasobs']);
					$datos->fotos['otrasobs'][] = [
						'semana' => $value["Semana"],
						'type' => 'Obs',
						'description' => $value['otrasobs'],
						'fotos' => is_array($value['evi_otrasobs'])
										? $value['evi_otrasobs'] 
										: ($value['evi_otrasobs'] != "" && $value['evi_otrasobs'] != "(NULL)") 
											? (file_exists(explode("_", $value['evi_otrasobs'])[0]."_Evidencia otras lab_1.".explode(".", $value['evi_otrasobs'])[1])) 
												? explode("_", $value['evi_otrasobs'])[0]."_Evidencia otras lab_1.".explode(".", $value['evi_otrasobs'])[1]
												: explode("_", $value['evi_otrasobs'])[0]."_Eo_1.".explode(".", $value['evi_otrasobs'])[1]
											: [""]
					];
					$datos->semana[] = $value["Semana"];
				}
			}

			return $datos;
		}

	}
?>
