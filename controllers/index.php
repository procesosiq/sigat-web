<?php
    /*----------  MICRO FRAMEWORK V1  ----------*/
    
    function D($data){
        header('Content-type: text/html');
        print_r($data);
    }

    function F($data){
        D($data);
        die();
    }
	
	include_once 'class.sesion.php';
	$session = Session::getInstance();
	$session = (object)$session;
 //    // echo $session->nombre;

	if(!isset($_REQUEST['accion'])){
		$_REQUEST = (array)json_decode(file_get_contents("php://input"));
	}
	
	if(!isset($_REQUEST['accion'])){
		die("Peticion desconocida");
	}

 	/* EXTRACT REQUEST */
 	$options = $_REQUEST['accion'];
	$controller = (object)[
		"controller" => "",
		"method" => "index"
	];
	
	if(strpos($options , ".")){
		$option = explode(".",$options);
		$controller->controller = $option[0];
		$controller->method = $option[1];
	}else{
		$controller->controller = $options;
	}

	if($controller->controller != "ProntoformsOrigenes"){
		if(!isset($session->logged)){
			die("Acceso no Autorizado");
		}
	}
	/* EXTRACT REQUEST */

	$controlName = $controller->controller;
	$path = './'.$controlName.'.php';
	include 'conexion.php';
	include($path);

	$objectController = new $controlName();
    // print_r($objectController);
    header('Content-Type: application/json');
	echo $objectController->{$controller->method}();
?>