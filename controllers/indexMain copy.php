<?php

// include 'class.sesion.php';
// include 'conexion.php';
/**
* 
*/
class indexMain
{
	
	private $conexion;
	private $session;
	
	public function __construct() {
		$this->conexion = new M_Conexion();
        $this->session = Session::getInstance();
    }


	public function getDetailAccountCliente(){
		$sql_client = "SELECT id,nombre FROM cat_clientes WHERE id_usuario = '{$this->session->logged}' AND status > 0";
		$cliente = (object)[];
		$cliente = (object)$this->conexion->Consultas(2, $sql_client);

		return $cliente;
	}

	public function saveFinca($id_hacienda){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$id_hacienda = $postdata->id_hacienda;
		$this->session->finca = $id_hacienda;
	}

	public function getFincas(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$id_cliente = (int)$postdata->id_client;
		$hacienda = (object)[];
		if($id_cliente > 0){
			$this->session->client = $id_cliente;
			$sql_hacienda = "SELECT id,nombre FROM cat_haciendas WHERE id_usuario = '{$this->session->logged}' 
			AND id_cliente = '{$id_cliente}' AND status > 0 ORDER BY id_cliente";
			$hacienda = (object)$this->conexion->Consultas(2, $sql_hacienda);
			// print_r($hacienda);
		}

		return json_encode($hacienda);
	}
}

// $postdata = (object)json_decode(file_get_contents("php://input"));
// if($postdata->id_client > 0){
// 	$data = new indexMain();
// 	echo $data->getFincas($postdata->id_client);
// }

// if($postdata->id_hacienda > 0){
// 	$data = new indexMain();
// 	echo $data->saveFinca($postdata->id_hacienda);
// }
?>