    <?php

// include 'class.sesion.php';
// include 'conexion.php';
/**
* 
*/
class indexMain
{
	
	private $conexion;
	private $session;
	
	public function __construct() {
		$this->conexion = new M_Conexion();
        $this->session = Session::getInstance();
    }

	public function getDetailAccountCliente(){
		$sWhere = "";
		if(isset($this->session->privileges->access) && $this->session->privileges->access == 1){
			$sWhere = "";
		}else{
			if(isset($this->session->access_fincas->access_special) && $this->session->access_fincas->access_special > 0){
				$ids = implode(",", $this->session->access_fincas->id_clientes);
				$sWhere = "  AND id IN ({$ids})";
			}else{
				$sWhere = " AND id = '{$this->session->client}'";
			}
		}
		$sql_client = "SELECT id,nombre FROM cat_clientes 
		WHERE id_usuario IN ({$this->session->logges->users}) AND status > 0 $sWhere ";
		$cliente = (object)[];
		$cliente = (object)$this->conexion->Consultas(2, $sql_client);

		return $cliente;
    }
    
    public function getClientesDiponibles(){
		$sWhere = "";

		if(isset($this->session->privileges->access) && $this->session->privileges->access == 1){
			$sWhere = "";
		}else{
			if(isset($this->session->access_fincas->access_special) && $this->session->access_fincas->access_special > 0){
				$ids = implode(",", $this->session->access_fincas->id_clientes);
				$sWhere = "  AND cat_clientes.id IN ({$ids})";
			}else{
				$sWhere = " AND cat_clientes.id = '{$this->session->client}'";
			}
		}
        
        $sql = "SELECT id_cliente AS id, nombre
                FROM muestras_haciendas m
                INNER JOIN cat_clientes ON id_cliente = cat_clientes.`id`
                WHERE m.id_usuario IN ({$this->session->logges->users})
                    AND YEAR(m.fecha) = {$this->session->year}
                    $sWhere
                    OR ({$this->session->isAdmin} = 1 AND id_cliente = 25)
				GROUP BY id_cliente";

        $data = (object) $this->conexion->Consultas(2, $sql);
        return $data;
    }

	public function saveFinca($id_hacienda){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$id_hacienda = $postdata->id_hacienda;

		$sql_hacienda = "SELECT Finca.id,Finca.nombre,Productor.nombre as Productor  
						FROM cat_haciendas as Finca
						INNER JOIN cat_clientes AS Productor ON Finca.`id_cliente` = Productor.`id`
						WHERE 
							Finca.id_usuario IN ({$this->session->logges->users})
                            AND Finca.id_cliente = '{$this->session->client}' 
							AND Finca.id = '{$id_hacienda}'";
        $hacienda = (object)$this->conexion->Consultas(2, $sql_hacienda)[0];
		$this->session->finca = $hacienda->id;
		$this->session->client_name = $hacienda->Productor;
		$this->session->finca_name = $hacienda->nombre;

		$path = "/informe";
        if($this->session->logged == 25){
			$path = "/inspeccionSemanal";
		}
		if($this->session->logged == 29){
			$path = "/listCiclos";
		}

		return json_encode(["path" => $path ]);
	}

	public function getFincas(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$id_cliente = (int)$postdata->id_client;
		$hacienda = (object)[];
		if($id_cliente > 0){
			$this->session->client = $id_cliente;
			$sWhere = "";
			if(isset($this->session->access_fincas->access_special) && $this->session->access_fincas->access_special > 0){
				$ids = implode(",", $this->session->access_fincas->id_finca);
				$sWhere = "  AND id IN ({$ids})";
			}
			/*$sql_hacienda = "SELECT id,nombre 
                            FROM cat_haciendas 
                            WHERE id_usuario IN ({$this->session->logges->users})
			                    AND id_cliente = '{$id_cliente}' 
                                AND status > 0 $sWhere  
                            ORDER BY id_cliente";*/
            $sql_hacienda = "SELECT id, nombre
                            FROM cat_haciendas
                            WHERE id_usuario IN ({$this->session->logges->users})
                                AND id_cliente = {$id_cliente}
                                AND IF({$id_cliente} = 11,		
                                    IF({$this->session->year} = 2017 AND STATUS = 0, 
                                        TRUE, 
                                        IF({$this->session->year} != 2017 AND STATUS > 0, 
                                            TRUE,
                                            FALSE
                                        )
                                    ),
                                    IF(STATUS > 0, TRUE, FALSE)
                                )
                            ORDER BY id_cliente";
            $hacienda = (object)$this->conexion->Consultas(2, $sql_hacienda);
            $this->session->fincas_disponibles_cliente_seleccionado = $hacienda;
            $this->session->id_client = $id_cliente;
		}
		return json_encode($hacienda);
    }
    
    public function getFincasDisponibles(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$id_cliente = (int)$postdata->id_client;
		$hacienda = (object)[];
		if($id_cliente > 0){
			$sWhere = "";
			if(isset($this->session->access_fincas->access_special) && $this->session->access_fincas->access_special > 0){
				$ids = implode(",", $this->session->access_fincas->id_finca);
				$sWhere = "  AND cat_haciendas.id IN ({$ids})";
			}
            $sql_hacienda = "SELECT id_hacienda AS id, nombre
                            FROM muestras_haciendas m
                            INNER JOIN cat_haciendas ON id_hacienda = cat_haciendas.id
                            WHERE m.id_usuario IN ({$this->session->logges->users})
                                AND m.id_cliente = {$id_cliente}
                                AND YEAR(m.fecha) = {$this->session->year} $sWhere
                                OR ({$id_cliente} = 25 AND m.id_cliente = 25)
                            GROUP BY id_hacienda
                            ORDER BY nombre";

			$hacienda = (object)$this->conexion->Consultas(2, $sql_hacienda);			
            $this->session->id_client = $id_cliente;
            $this->session->client = $id_cliente;
            $this->session->fincas_disponibles_cliente_seleccionado = $hacienda;
        }
		return json_encode($hacienda);
	}

	public function getFincasPOST(){
		$postdata = (object)$_POST;
		$id_cliente = (int)$postdata->id_client;
		$hacienda = (object)[];
		if($id_cliente > 0){
			$this->session->client = $id_cliente;
			$sql_hacienda = "SELECT id,nombre FROM cat_haciendas WHERE id_usuario IN ({$this->session->logges->users})
			AND id_cliente = '{$id_cliente}' AND status > 0 ORDER BY id_cliente";
			$hacienda = (object)$this->conexion->Consultas(2, $sql_hacienda);
			// print_r($hacienda);
		}

		return json_encode($hacienda);
	}

	public function setYear(){
		$postdata = (object)$_POST;
		$year = (int)$postdata->year;
		if($year > 0){
			$this->session->current_year = $year;
		}

		return 1;
    }
    
    public function saveYear(){
        $postdata = (object)$_POST;
		$year = (int)$postdata->year;
		if($year > 0){
			$this->session->year = $year;
		}
		return 1;
    }
}

// $postdata = (object)json_decode(file_get_contents("php://input"));
// if($postdata->id_client > 0){
// 	$data = new indexMain();
// 	echo $data->getFincas($postdata->id_client);
// }

// if($postdata->id_hacienda > 0){
// 	$data = new indexMain();
// 	echo $data->saveFinca($postdata->id_hacienda);
// }
?>