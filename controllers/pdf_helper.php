<?php

function pdf($html, $filename, $stream) 
{
    require_once("dompdf/dompdf_config.inc.php");
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $dompdf->render();
    
    if ($stream) {
        $dompdf->stream($filename.".pdf");
    } else {
        return $dompdf->output();
    }   
}

if(isset($_GET["html"])){
    $html = $_GET["html"];
}

#$postdata = (object)json_decode(file_get_contents("php://input"));
pdf($html, "mi_pdf", true); 

?>