<?php
include 'class.sesion.php';
include 'conexion.php';

class Graficas_S_C{
	
	private $conexion;
	private $session;
	
	public function __construct() {
        $this->conexion = new M_Conexion();
        $this->session = Session::getInstance();
        // $this->session->client = 1;
    }

    public function ConsultasCeroSemanasToken($token, $tipo, $tipo_semana){
    	$sql_get_info = "SELECT * FROM informe WHERE token = '{$token}'";
        $result = $this->conexion->Consultas(2, $sql_get_info);
        $count = 0;
    	$id_usuario = $result[0]["id_usuario"];
    	$id_cliente = $result[0]["id_cliente"];
    	$id_finca = $result[0]["id_hacienda"];
        $ids_membresia = $result[0]["ids_membresia"];
        $year = $result[0]["year_selected"];
        $semana = $result[0]["semana_selected"];
        
		$sql = "SELECT YEAR(prin.fecha) AS anoo , foco 
				FROM muestras_hacienda_detalle as det
				INNER JOIN muestras_haciendas as prin on det.id_Mhacienda = prin.id
				WHERE YEAR(prin.fecha) > 0 
                    AND YEAR(prin.fecha) = {$year}
                    AND getWeek(prin.fecha) >= 0
                    AND getWeek(prin.fecha) <= {$semana}
				    AND det.id_usuario IN ({$ids_membresia})
				    AND det.id_cliente =  '{$id_cliente}'
				    AND det.id_hacienda =  '{$id_finca}'
				GROUP BY foco, YEAR(fecha) 
				ORDER BY anoo";
				
        $res = $this->conexion->link->query($sql);
        $datos = array();
        $min = [];
        $max = [];

        while($fila = $res->fetch_assoc()){
            if($tipo=='HMVLDQMEN5%'){
                $sql2="SELECT 
                            getWeek(muestras_haciendas.fecha) AS numsemana,
                            COUNT(*) AS numreg,
                            foco,
                            AVG(IF(hoja_mas_vieja_libre_quema_menor>0,hoja_mas_vieja_libre_quema_menor,NULL)) AS hojaViejaLibreQuemaMenor
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE YEAR(muestras_haciendas.fecha) = {$year}
                            AND getWeek(muestras_haciendas.fecha) <= {$semana}
                            AND foco = '{$fila['foco']}'
                            AND muestras_haciendas.tipo_semana = $tipo_semana
                            AND muestras_haciendas.id_usuario IN ({$ids_membresia})
                            AND muestras_haciendas.id_cliente = '{$id_cliente}' 
                            AND muestras_hacienda_detalle.id_hacienda = '{$id_finca}'
                        GROUP BY foco,numsemana 
                        ORDER BY numsemana, foco";
                $res2 = $this->conexion->link->query($sql2);
                while($fila2 = $res2->fetch_assoc()){
                    $count += 1;
                    $datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMenor"]);
                    $min[] = $fila2["hojaViejaLibreQuemaMenor"];
                    $max[] = $fila2["hojaViejaLibreQuemaMenor"];
                }
                if($count == 1 && $datos[$fila["foco"]][0][0] > 1) {
                    array_push($datos[$fila["foco"]], [$datos[$fila["foco"]][0][0] -1,0]);
                }
            }
            else if($tipo=="HMVLDE"){
                $sql2 = "SELECT 
                            getWeek(muestras_haciendas.fecha) AS numsemana,
                            COUNT(*) AS numreg,
                            AVG(IF(hojas_mas_vieja_libre>0,hojas_mas_vieja_libre,NULL)) AS hojaViejaLibreEstrias,
                            foco
                        FROM muestras_haciendas 
                        INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                        WHERE YEAR(muestras_haciendas.fecha) = $year
                            AND semana <= $semana
                            AND foco = '{$fila['foco']}'
                            AND muestras_haciendas.tipo_semana = $tipo_semana 
                            AND muestras_haciendas.id_usuario IN ({$ids_membresia})
                            AND muestras_haciendas.id_cliente = '{$id_cliente}' 
                            AND muestras_hacienda_detalle.id_hacienda = '{$id_finca}'
                        GROUP BY foco, numsemana 
                        ORDER BY numsemana, foco";
                $res2 = $this->conexion->link->query($sql2);
                while($fila2 = $res2->fetch_assoc()){
                    $count+=1;
                    $datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreEstrias"]);
                    $min[] = $fila2["hojaViejaLibreEstrias"];
                    $max[] = $fila2["hojaViejaLibreEstrias"];
                }
                if($count == 1 && $datos[$fila["foco"]][0][0] > 1) {
                    array_push($datos[$fila["foco"]], [$datos[$fila["foco"]][0][0] -1,0]);
                }
            }
            else if($tipo=="LIB_DE_CIRUG"){
                $sql2="SELECT 
                    getWeek(muestras_haciendas.fecha) AS numsemana,
                    COUNT(*) AS numreg,
                    AVG(IF(libre_cirugias>0,libre_cirugias,NULL)) AS hojaLibreCirugias,foco
                FROM muestras_haciendas 
                INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
                    AND muestras_haciendas.id_usuario IN ({$ids_membresia})
                    AND muestras_haciendas.id_cliente = '{$id_cliente}' 
                    AND muestras_hacienda_detalle.id_hacienda = '{$id_finca}'
                GROUP BY foco,numsemana 
                ORDER BY numsemana,foco ";
                $res2 = $this->conexion->link->query($sql2);
                while($fila2 = $res2->fetch_assoc()){
                    $count+=1;
                    $datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaLibreCirugias"]);
                    $min[] = $fila2["hojaLibreCirugias"];
                    $max[] = $fila2["hojaLibreCirugias"];
                }
                if($count == 1 && $datos[$fila["foco"]][0][0] > 1) {
                    array_push($datos[$fila["foco"]], [$datos[$fila["foco"]][0][0] -1,0]);
                }
            }
            else if($tipo=="HMVLDQMAY5"){
                $sql2="SELECT 
                    getWeek(muestras_haciendas.fecha) AS numsemana,
                    COUNT(*) AS numreg,
                    AVG(IF(hoja_mas_vieja_libre_quema_mayor>0,hoja_mas_vieja_libre_quema_mayor,NULL)) AS hojaViejaLibreQuemaMayor,
                    foco
                FROM muestras_haciendas 
                INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
                    AND muestras_haciendas.id_usuario IN ({$ids_membresia})
                    AND muestras_haciendas.id_cliente = '{$id_cliente}' 
                    AND muestras_hacienda_detalle.id_hacienda = '{$id_finca}'
                GROUP BY foco,numsemana 
                ORDER BY numsemana,foco ";
                $res2 = $this->conexion->link->query($sql2);
                while($fila2 = $res2->fetch_assoc()){
                    $count+=1;
                    $datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMayor"]);
                    $max[] = $fila2["hojaViejaLibreQuemaMayor"];
                    $min[] = $fila2["hojaViejaLibreQuemaMayor"];
                }
                if($count == 1 && $datos[$fila["foco"]][0][0] > 1) {
                    array_push($datos[$fila["foco"]], [$datos[$fila["foco"]][0][0] -1,0]);
                }
            }
            else if($tipo=="HOJTOT"){
                $sql2="SELECT 
                    getWeek(muestras_haciendas.fecha) AS numsemana,
                    COUNT(*) AS numreg,
                    AVG(IF(hojas_totales>0,hojas_totales,NULL)) AS hojasTotales,foco
                FROM muestras_haciendas 
                INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
                AND muestras_haciendas.id_usuario IN ({$ids_membresia})
                AND muestras_haciendas.id_cliente = '{$id_cliente}' 
                AND muestras_hacienda_detalle.id_hacienda = '{$id_finca}'
                GROUP BY foco,numsemana 
                ORDER BY numsemana,foco ";
                
                $res2 = $this->conexion->link->query($sql2);
                $min[] = 13;
                $max[] = 13;
                while($fila2 = $res2->fetch_assoc()){
                    $count+=1;
                    $datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojasTotales"]);
                    $sem[] = (int) $fila2["numsemana"];
                    $max[] = $fila2["hojasTotales"];
                    $min[] = $fila2["hojasTotales"];
                }

                if($count == 1 && $datos[$fila["foco"]][0][0] > 1) {
                    array_push($datos[$fila["foco"]], [$datos[$fila["foco"]][0][0] -1,0]);
                }
                $datos["0"][] = [min($sem), 13];
                $datos["0"][] = [max($sem), 13];
            }
        }

        $response = (object)[];
        $response->datos = (array)$datos;
        $response->max = max($max);
        $response->min = is_null(min($min))?0:min($min);

        return (array)$response;
	}
	
	public function ConsultasCeroSemanas($tipo,$tipo_semana){
		$sWhere = $this->getPreferencias($tipo);
		if($sWhere != ""){
			$Label = explode(",", $sWhere);
		}
		$labels = [
			'HMVLDQMEN5%' => 'hoja_vieja_menor_5',
			'HMVLDE' => 'libre_estrias',
			'LIB_DE_CIRUG' => 'libre_cirugia',
			'HMVLDQMAY5' => 'hoja_vieja_mayor_5',
			'HOJTOT' => 'hojas_total'
		];

		$sql = "SELECT YEAR(prin.fecha) AS anoo , foco 
					FROM muestras_hacienda_detalle as det
					INNER JOIN muestras_haciendas as prin on det.id_Mhacienda = prin.id
				WHERE YEAR(prin.fecha) > 0 AND YEAR(prin.fecha) = '{$this->session->current_year}'
				AND det.id_usuario IN ({$this->session->logges->users})
				AND det.id_cliente =  '{$this->session->client}'
				AND det.id_hacienda =  '{$this->session->finca}'
				GROUP BY foco ,YEAR(fecha) 
				ORDER BY foco";
				// echo $sql;
				// return;
			$res = $this->conexion->link->query($sql);
			$datos = array();
			$tabla = array();
			$min = [];
			$max = [];
			$index = 0;
			$foco = "";
			while($fila = $res->fetch_assoc()){
				if($foco != $fila["foco"]){
					$foco = $fila["foco"];
					$index++;
				}
				if($tipo=='HMVLDQMEN5%'){ 
					$sql2="SELECT 
					    getWeek(muestras_haciendas.fecha) AS numsemana,
					    COUNT(*) AS numreg,
					    foco,
					    AVG(IF(hoja_mas_vieja_libre_quema_menor>0,hoja_mas_vieja_libre_quema_menor,NULL)) AS hojaViejaLibreQuemaMenor
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND muestras_haciendas.tipo_semana = $tipo_semana
					    AND muestras_haciendas.id_usuario IN ({$this->session->logges->users})
					    AND muestras_haciendas.id_cliente = '{$this->session->client}' 
					    AND muestras_hacienda_detalle.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["0"]["Umbral"][] = [0 , 0];
					$datos["0"]["Umbral"][] = [52 , 0];
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
                        $count+=1;
						$tabla[$fila["foco"]][$fila2["numsemana"]] = (double)$fila2["hojaViejaLibreQuemaMenor"];
						// $datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMenor"]);
						$datos[$index][trim($fila["foco"])][] = array($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMenor"]);
						$min[] = $fila2["hojaViejaLibreQuemaMenor"];
						$max[] = $fila2["hojaViejaLibreQuemaMenor"];
					}
				}
				if($tipo=="HMVLDE"){
					$sql2="SELECT 
					    getWeek(muestras_haciendas.fecha) AS numsemana,
					    COUNT(*) AS numreg,
					    AVG(IF(hojas_mas_vieja_libre>0,hojas_mas_vieja_libre,NULL)) AS hojaViejaLibreEstrias,
					    foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
					    AND muestras_haciendas.id_usuario IN ({$this->session->logges->users})
					    AND muestras_haciendas.id_cliente = '{$this->session->client}' 
					    AND muestras_hacienda_detalle.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["0"]["Umbral"][] = [0 , 0];
					$datos["0"]["Umbral"][] = [52 , 0];
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
                        $count+=1;
						$tabla[$fila["foco"]][$fila2["numsemana"]] = (double)$fila2["hojaViejaLibreEstrias"];
						// $datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreEstrias"]);
						$datos[$index][trim($fila["foco"])][] = array($fila2["numsemana"],$fila2["hojaViejaLibreEstrias"]);
						$min[] = $fila2["hojaViejaLibreEstrias"];
						$max[] = $fila2["hojaViejaLibreEstrias"];
					}
				}
				if($tipo=="LIB_DE_CIRUG"){
					$sql2="SELECT 
					    getWeek(muestras_haciendas.fecha) AS numsemana,
					    COUNT(*) AS numreg,
					    AVG(IF(libre_cirugias>0,libre_cirugias,NULL)) AS hojaLibreCirugias,foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                    WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' 
                        AND foco = '".$fila['foco']."' 
                        AND muestras_haciendas.tipo_semana = $tipo_semana 
					    AND muestras_haciendas.id_usuario IN ({$this->session->logges->users})
					    AND muestras_haciendas.id_cliente = '{$this->session->client}' 
					    AND muestras_hacienda_detalle.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["0"]["Umbral"][] = [0 , 0];
					$datos["0"]["Umbral"][] = [52 , 0];
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
                        $count+=1;
						$tabla[$fila["foco"]][$fila2["numsemana"]] = (double)$fila2["hojaLibreCirugias"];
						// $datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaLibreCirugias"]);
						$datos[$index][trim($fila["foco"])][] = array($fila2["numsemana"],$fila2["hojaLibreCirugias"]);
						$max[] = $fila2["hojaLibreCirugias"];
						$min[] = $fila2["hojaLibreCirugias"];
					}
				}
				if($tipo=="HMVLDQMAY5"){
					$sql2="SELECT 
					    getWeek(muestras_haciendas.fecha) AS numsemana,
					    COUNT(*) AS numreg,
					    AVG(IF(hoja_mas_vieja_libre_quema_mayor>0,hoja_mas_vieja_libre_quema_mayor,NULL)) AS hojaViejaLibreQuemaMayor,
					    foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                    WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' 
                        AND foco = '".$fila['foco']."' 
                        AND muestras_haciendas.tipo_semana = $tipo_semana 
					    AND muestras_haciendas.id_usuario IN ({$this->session->logges->users})
					    AND muestras_haciendas.id_cliente = '{$this->session->client}' 
					    AND muestras_hacienda_detalle.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["0"]["Umbral"][] = [0 , 0];
					$datos["0"]["Umbral"][] = [52 , 0];
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
                        $count+=1;
						$tabla[$fila["foco"]][$fila2["numsemana"]] = (double)$fila2["hojaViejaLibreQuemaMayor"];
						// $datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMayor"]);
						$datos[$index][trim($fila["foco"])][] = array($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMayor"]);
						$max[] = $fila2["hojaViejaLibreQuemaMayor"];
						$min[] = $fila2["hojaViejaLibreQuemaMayor"];
					}
				}
				if($tipo=="HOJTOT"){
					$sql2="SELECT 
					    getWeek(muestras_haciendas.fecha) AS numsemana,
					    COUNT(*) AS numreg,
					    AVG(IF(hojas_totales>0,hojas_totales,NULL)) AS hojasTotales,foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                    WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' 
                        AND foco = '".$fila['foco']."' 
                        AND muestras_haciendas.tipo_semana = $tipo_semana 
					    AND muestras_haciendas.id_usuario IN ({$this->session->logges->users})
					    AND muestras_haciendas.id_cliente = '{$this->session->client}' 
					    AND muestras_hacienda_detalle.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["0"]["Umbral"][] = [0 , 13];
					$datos["0"]["Umbral"][] = [52 , 13];
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
                        $count+=1;
						$tabla[$fila["foco"]][$fila2["numsemana"]] = (double)$fila2["hojasTotales"];
						// $datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojasTotales"]);
						$datos[$index][trim($fila["foco"])][] = array($fila2["numsemana"],$fila2["hojasTotales"]);
						$max[] = $fila2["hojasTotales"];
						$min[] = $fila2["hojasTotales"];
					}
				}

				if(is_array($Label) && !in_array($fila["foco"], $Label)){
					$notLoad[$labels[$tipo]][] = $fila["foco"];
				}
			}

			$response = new stdClass;
			$response->table = (object)$tabla;
			$response->datos = (array)$datos;
			$response->max = max($max);
			$response->min = is_null(min($min))?0:min($min);
			$response->validate = $notLoad;

			return (array)$response;
	}

	public function getPreferencias($tipo , $id_finca = 0 , $id_usuario = 0){
		$labels = [
			'HMVLDQMEN5%' => 'hoja_vieja_menor_5',
			'HMVLDE' => 'libre_estrias',
			'LIB_DE_CIRUG' => 'libre_cirugia',
			'HMVLDQMAY5' => 'hoja_vieja_mayor_5',
			'HOJTOT' => 'hojas_total'
		];
		$column = $labels[$tipo];
		$label = "";
		if($id_finca > 0){
			$id_hacienda = $id_finca;
		}else{
			$id_hacienda = $this->session->finca;
		}

		if($id_usuario > 0){
			$user_id = $id_usuario;
		}else{
			$user_id = $this->session->logged;
		}
		$consulta = "SELECT {$column} FROM usuario_preferencia_0_semanas WHERE id_usuario = '{$user_id}' 
		AND id_hacienda = '{$id_hacienda}'";
		// print $consulta;
		$result = $this->conexion->Consultas(2, $consulta);
		$filas = count($result);
		if($filas > 0){
			$focos = $result[0];
			$label = $focos[$column];
		}

		return $label;
	}

	public function saveLabels(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$type = $postdata->type;
		if($type != "" && count($postdata->labels) > 0){
			$data = implode(",", array_unique($postdata->labels));
			$consulta = "SELECT * FROM usuario_preferencia_0_semanas WHERE id_usuario = '{$this->session->logged}' 
			AND id_hacienda = '{$this->session->finca}' ";
			$result = $this->conexion->Consultas(2, $consulta);
			$filas = count($result);
			if($filas > 0){
				$sql = "UPDATE usuario_preferencia_0_semanas SET {$type} = '{$data}' WHERE id_usuario = '{$this->session->logged}'
				AND id_hacienda = '{$this->session->finca}'";
			}else{
				$sql = "INSERT INTO usuario_preferencia_0_semanas SET id_usuario = '{$this->session->logged}' ,
				id_hacienda = '{$this->session->finca}'  , {$type} = '{$data}'";
			}
			// print($sql);
			$this->conexion->link->query($sql);
		}
		return json_encode(["success" => "200"]);
	}

	public function Grafica_semana($tipo,$tipo_semana){
		$datos = array();
		$datos = $this->ConsultasCeroSemanas($tipo,$tipo_semana);
		return json_encode($datos);
	}
	public function Grafica_semanaToken($token,$tipo,$tipo_semana){
		$datos = array();
		$datos = $this->ConsultasCeroSemanasToken($token,$tipo,$tipo_semana);
		return json_encode($datos);
	}
}

$postdata = (object)json_decode(file_get_contents("php://input"));
if(!isset($postdata->opt)) $postdata = (object) $_POST;

if($postdata->opt == "HMVLDQMEN5"){ #HOJA MAS VIEJA LIBRE DE QUEMA MENOR A 5%
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_C;
			echo $retval->Grafica_semanaToken($postdata->token, "HMVLDQMEN5%", 0);
		}	
	}else{
		$retval = new Graficas_S_C;
		echo $retval->Grafica_semana("HMVLDQMEN5%",0);
	}
}
else if($postdata->opt == "HMVLDE"){  #hoja mas vieja libre de estias
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_C;
			echo $retval->Grafica_semanaToken($postdata->token, "HMVLDE", 0);
		}	
	}else{
		$retval = new Graficas_S_C;
		echo $retval->Grafica_semana("HMVLDE",0);
	}
}
else if($postdata->opt == "LIB_DE_CIRUG"){  #hojas libres de cirugias 
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_C;
			echo $retval->Grafica_semanaToken($postdata->token, "LIB_DE_CIRUG", 0);
		}	
	}else{
		$retval = new Graficas_S_C;
		echo $retval->Grafica_semana("LIB_DE_CIRUG",0);
	}
}
else if($postdata->opt == "HMVLDQMAY5"){  #HOJA MAS VIEJA LIBRE DE QUEMA MAYOR A 5%
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_C;
			echo $retval->Grafica_semanaToken($postdata->token, "HMVLDQMAY5", 0);
		}	
	}else{
		$retval = new Graficas_S_C;
		echo $retval->Grafica_semana("HMVLDQMAY5",0);
	}
}
else if($postdata->opt == "HOJTOT"){  #HOJAS TOTALES
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_C;
			echo $retval->Grafica_semanaToken($postdata->token, "HOJTOT", 0);
		}	
	}else{
		$retval = new Graficas_S_C;
		echo $retval->Grafica_semana("HOJTOT",0);
	}
}else if($postdata->opt = "SAVE_LABELS"){
	if(isset($postdata->type)){
		$retval = new Graficas_S_C;
		echo $retval->saveLabels();
	}
}
?>
