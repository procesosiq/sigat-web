<?php

include 'class.sesion.php';
include 'conexion.php';

class Graficas_S_O{
	
	private $conexion;
	private $session;
	
	public function __construct() {
        $this->conexion = new M_Conexion();
		$this->session = Session::getInstance();
        // $this->session->client = 1;
    }

    private function ConsultasCeroSemanasToken($token, $tipo, $tipo_semana){
    	$sql_get_info = "SELECT * FROM informe WHERE token = '{$token}'";
    	$result = $this->conexion->Consultas(2, $sql_get_info);
    	$id_usuario = $result[0]["id_usuario"];
    	$id_cliente = $result[0]["id_cliente"];
    	$id_finca = $result[0]["id_hacienda"];
    	$ids_membresia = $result[0]["ids_membresia"];
    	// print_r($this->db2_conn_error()xion);
		$sql = "SELECT YEAR(prin.fecha) AS anoo , foco 
					FROM muestras_hacienda_detalle as det
					INNER JOIN muestras_haciendas as prin on det.id_Mhacienda = prin.id
				WHERE YEAR(prin.fecha) > 0 
				AND det.id_usuario IN ({$ids_membresia})
				AND det.id_cliente =  '{$id_cliente}'
				AND det.id_hacienda =  '{$id_finca}'
				AND prin.tipo_semana = '{$tipo_semana}'
				GROUP BY foco ,YEAR(fecha) 
				ORDER BY anoo";
				// echo $sql;
				// return;
			$res = $this->conexion->link->query($sql);
			$datos = array();
			$min = [];
			$max = [];
			while($fila = $res->fetch_assoc()){
				
				if($tipo=='HMVLDQMEN5%'){ 
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					foco,
					AVG(IF(hoja_mas_vieja_libre_quema_menor>0,hoja_mas_vieja_libre_quema_menor,NULL)) AS hojaViejaLibreQuemaMenor
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND muestras_haciendas.tipo_semana = $tipo_semana
					AND muestras_haciendas.id_usuario IN ({$ids_membresia})
					AND muestras_haciendas.id_cliente = '{$id_cliente}' 
					AND muestras_haciendas.id_hacienda = '{$id_finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[trim($fila["foco"])][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMenor"]);
						$min[] = $fila2["hojaViejaLibreQuemaMenor"];
						$max[] = $fila2["hojaViejaLibreQuemaMenor"];
					}
				}
				if($tipo=="HMVLDE"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(hojas_mas_vieja_libre>0,hojas_mas_vieja_libre,NULL)) AS hojaViejaLibreEstrias,
					foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario IN ({$ids_membresia})
					AND muestras_haciendas.id_cliente = '{$id_cliente}' 
					AND muestras_haciendas.id_hacienda = '{$id_finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[trim($fila["foco"])][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreEstrias"]);
						$min[] = $fila2["hojaViejaLibreEstrias"];
						$max[] = $fila2["hojaViejaLibreEstrias"];
					}
				}
				if($tipo=="LIB_DE_CIRUG"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(libre_cirugias>0,libre_cirugias,NULL)) AS hojaLibreCirugias,foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario IN ({$ids_membresia})
					AND muestras_haciendas.id_cliente = '{$id_cliente}' 
					AND muestras_haciendas.id_hacienda = '{$id_finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[trim($fila["foco"])][] = array ($fila2["numsemana"],$fila2["hojaLibreCirugias"]);
						$min[] = $fila2["hojaLibreCirugias"];
						$max[] = $fila2["hojaLibreCirugias"];
					}
				}
				if($tipo=="HMVLDQMAY5"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(hoja_mas_vieja_libre_quema_mayor>0,hoja_mas_vieja_libre_quema_mayor,NULL)) AS hojaViejaLibreQuemaMayor,
					foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario IN ({$ids_membresia})
					AND muestras_haciendas.id_cliente = '{$id_cliente}' 
					AND muestras_haciendas.id_hacienda = '{$id_finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[trim($fila["foco"])][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMayor"]);
						$min[] = $fila2["hojaViejaLibreQuemaMayor"];
						$max[] = $fila2["hojaViejaLibreQuemaMayor"];
					}
				}
				if($tipo=="HOJTOT"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(hojas_totales>0,hojas_totales,NULL)) AS hojasTotales,foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario IN ({$ids_membresia})
					AND muestras_haciendas.id_cliente = '{$id_cliente}' 
					AND muestras_haciendas.id_hacienda = '{$id_finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					$datos["0"][] = [0 , 7];
					$datos["0"][] = [26 , 7];
					while($fila2 = $res2->fetch_assoc()){
						$datos[trim($fila["foco"])][] = array ($fila2["numsemana"],$fila2["hojasTotales"]);
						$min[] = $fila2["hojasTotales"];
						$max[] = $fila2["hojasTotales"];
					}
				}
			}
			$response = (object)[];
			$response->datos = (array)$datos;
			$response->max = max($max);
			$response->min = is_null(min($min))?0:min($min);

			return (array)$response;
    }

	public function ConsultasCeroSemanas($tipo,$tipo_semana){
		// print_r($this->db2_conn_error()xion);
		$sql = "SELECT YEAR(prin.fecha) AS anoo , foco 
					FROM muestras_hacienda_detalle as det
					INNER JOIN muestras_haciendas as prin on det.id_Mhacienda = prin.id
				WHERE YEAR(prin.fecha) > 0 
				AND det.id_usuario IN ({$this->session->logges->users})
				AND det.id_cliente =  '{$this->session->client}'
				AND det.id_hacienda =  '{$this->session->finca}'
				AND prin.tipo_semana = '{$tipo_semana}'
				GROUP BY foco ,YEAR(fecha) 
				ORDER BY anoo";
				// echo $sql;
				// return;
			$res = $this->conexion->link->query($sql);
			$datos = array();
			while($fila = $res->fetch_assoc()){
				
				if($tipo=='HMVLDQMEN5%'){ 
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					foco,
					AVG(IF(hoja_mas_vieja_libre_quema_menor>0,hoja_mas_vieja_libre_quema_menor,NULL)) AS hojaViejaLibreQuemaMenor
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND muestras_haciendas.tipo_semana = $tipo_semana
					AND muestras_haciendas.id_usuario IN ({$this->session->logges->users})
					AND muestras_haciendas.id_cliente = '{$this->session->client}' 
					AND muestras_haciendas.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[trim($fila["foco"])][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMenor"]);
						$min[] = $fila2["hojaViejaLibreQuemaMenor"];
						$max[] = $fila2["hojaViejaLibreQuemaMenor"];
					}
				}
				if($tipo=="HMVLDE"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(hojas_mas_vieja_libre>0,hojas_mas_vieja_libre,NULL)) AS hojaViejaLibreEstrias,
					foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario IN ({$this->session->logges->users})
					AND muestras_haciendas.id_cliente = '{$this->session->client}' 
					AND muestras_haciendas.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[trim($fila["foco"])][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreEstrias"]);
						$min[] = $fila2["hojaViejaLibreEstrias"];
						$max[] = $fila2["hojaViejaLibreEstrias"];
					}
				}
				if($tipo=="LIB_DE_CIRUG"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(libre_cirugias>0,libre_cirugias,NULL)) AS hojaLibreCirugias,foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario IN ({$this->session->logges->users})
					AND muestras_haciendas.id_cliente = '{$this->session->client}' 
					AND muestras_haciendas.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[trim($fila["foco"])][] = array ($fila2["numsemana"],$fila2["hojaLibreCirugias"]);
						$min[] = $fila2["hojaLibreCirugias"];
						$max[] = $fila2["hojaLibreCirugias"];
					}
				}
				if($tipo=="HMVLDQMAY5"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(hoja_mas_vieja_libre_quema_mayor>0,hoja_mas_vieja_libre_quema_mayor,NULL)) AS hojaViejaLibreQuemaMayor,
					foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario IN ({$this->session->logges->users})
					AND muestras_haciendas.id_cliente = '{$this->session->client}' 
					AND muestras_haciendas.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[trim($fila["foco"])][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMayor"]);
						$min[] = $fila2["hojaViejaLibreQuemaMayor"];
						$max[] = $fila2["hojaViejaLibreQuemaMayor"];
					}
				}
				if($tipo=="HOJTOT"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(IF(hojas_totales>0,hojas_totales,NULL)) AS hojasTotales,foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario IN ({$this->session->logges->users})
					AND muestras_haciendas.id_cliente = '{$this->session->client}' 
					AND muestras_haciendas.id_hacienda = '{$this->session->finca}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					$datos["0"][] = [0 , 7];
					$datos["0"][] = [26 , 7];
					while($fila2 = $res2->fetch_assoc()){
						$datos[trim($fila["foco"])][] = array ($fila2["numsemana"],$fila2["hojasTotales"]);
						$min[] = $fila2["hojasTotales"];
						$max[] = $fila2["hojasTotales"];
					}
				}
			}
			$response = (object)[];
			$response->datos = (array)$datos;
			$response->max = max($max);
			$response->min = (is_null(min($min))  || min($min) > 7)?0:min($min);

			return (array)$response;
	}
	
	public function Grafica_semana($tipo,$tipo_semana){
		$datos = array();
		$datos = $this->ConsultasCeroSemanas($tipo,$tipo_semana);
		return json_encode($datos);
	}

	public function Grafica_semanaToken($token, $tipo,$tipo_semana){
		$datos = array();
		$datos = $this->ConsultasCeroSemanasToken($token, $tipo,$tipo_semana);
		return json_encode($datos);
	}	
}

$postdata = (object)json_decode(file_get_contents("php://input"));

if($postdata->opt == "HMVLDQMEN5"){ #HOJA MAS VIEJA LIBRE DE QUEMA MENOR A 5%
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_O;
			echo $retval->Grafica_semanaToken($postdata->token, "HMVLDQMEN5%", 11);
		}	
	}else{
		$retval = new Graficas_S_O;
		echo $retval->Grafica_semana("HMVLDQMEN5%",11);
	}
}
else if($postdata->opt == "LIB_DE_CIRUG"){  #hojas libres de cirugias 
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_O;
			echo $retval->Grafica_semanaToken($postdata->token, "LIB_DE_CIRUG", 11);
		}	
	}else{
		$retval = new Graficas_S_O;
		echo $retval->Grafica_semana("LIB_DE_CIRUG",11);
	}
}
else if($postdata->opt == "HMVLDQMAY5"){  #HOJA MAS VIEJA LIBRE DE QUEMA MAYOR A 5%
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_O;
			echo $retval->Grafica_semanaToken($postdata->token, "HMVLDQMAY5", 11);
		}	
	}else{
		$retval = new Graficas_S_O;
		echo $retval->Grafica_semana("HMVLDQMAY5",11);
	}
}
else if($postdata->opt == "HOJTOT"){  #HOJAS TOTALES
	if(isset($postdata->token)){
		if($postdata->token != ""){
			$retval = new Graficas_S_O;
			echo $retval->Grafica_semanaToken($postdata->token, "HOJTOT", 11);
		}	
	}else{
		$retval = new Graficas_S_O;
		echo $retval->Grafica_semana("HOJTOT",11);
	}
}
?>