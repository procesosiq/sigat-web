<?php

include 'class.sesion.php';
include 'conexion.php';

class Graficas_S_O{
	
	private $conexion;
	private $session;
	
	public function __construct() {
        $this->conexion = new M_Conexion();
		$this->session = Session::getInstance();
        $this->session->client = 1;
    }


	public function ConsultasCeroSemanas($tipo,$tipo_semana){
		// print_r($this->db2_conn_error()xion);
		$sql = "SELECT YEAR(prin.fecha) AS anoo , foco 
					FROM muestras_hacienda_detalle as det
					INNER JOIN muestras_haciendas as prin on det.id_Mhacienda = prin.id
				WHERE YEAR(prin.fecha) > 0 
				AND prin.id_usuario = '{$this->session->logged}' 
				AND prin.id_cliente =  '{$this->session->client}'
				GROUP BY foco ,YEAR(fecha) 
				ORDER BY anoo";
				// echo $sql;
				// return;
			$res = $this->conexion->link->query($sql);
			$datos = array();
			while($fila = $res->fetch_assoc()){
				
				if($tipo=='HMVLDQMEN5%'){ 
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					foco,
					AVG(hoja_mas_vieja_libre_quema_menor) AS hojaViejaLibreQuemaMenor
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND muestras_haciendas.tipo_semana = $tipo_semana
					AND muestras_haciendas.id_usuario = '{$this->session->logged}' AND muestras_haciendas.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMenor"]);
					}
				}
				if($tipo=="HMVLDE"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(hojas_mas_vieja_libre) AS hojaViejaLibreEstrias,
					foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario = '{$this->session->logged}' AND muestras_haciendas.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreEstrias"]);
					}
				}
				if($tipo=="LIB_DE_CIRUG"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(libre_cirugias) AS hojaLibreCirugias,foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario = '{$this->session->logged}' AND muestras_haciendas.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaLibreCirugias"]);
					}
				}
				if($tipo=="HMVLDQMAY5"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(hoja_mas_vieja_libre_quema_mayor) AS hojaViejaLibreQuemaMayor,
					foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario = '{$this->session->logged}' AND muestras_haciendas.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMayor"]);
					}
				}
				if($tipo=="HOJTOT"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(hojas_totales) AS hojasTotales,foco
					FROM muestras_haciendas 
					INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' AND  muestras_haciendas.tipo_semana = $tipo_semana 
					AND muestras_haciendas.id_usuario = '{$this->session->logged}' AND muestras_haciendas.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					$datos["umbral"][] = [0 , 7];
					$datos["umbral"][] = [26 , 7];
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojasTotales"]);
					}
				}
			}
			return $datos;
	}
	
	public function Grafica_semana($tipo,$tipo_semana){
		$datos = array();
		$datos = $this->ConsultasCeroSemanas($tipo,$tipo_semana);
		return json_encode($datos);
	}
}

$postdata = (object)json_decode(file_get_contents("php://input"));
if($postdata->opt == "HMVLDQMEN5"){ #HOJA MAS VIEJA LIBRE DE QUEMA MENOR A 5%
	$retval = new Graficas_S_O;
	echo $retval->Grafica_semana("HMVLDQMEN5%",11);
}
else if($postdata->opt == "LIB_DE_CIRUG"){  #hojas libres de cirugias 
	$retval = new Graficas_S_O;
	echo $retval->Grafica_semana("LIB_DE_CIRUG",11);
}
else if($postdata->opt == "HMVLDQMAY5"){  #HOJA MAS VIEJA LIBRE DE QUEMA MAYOR A 5%
	$retval = new Graficas_S_O;
	echo $retval->Grafica_semana("HMVLDQMAY5",11);
}
else if($postdata->opt == "HOJTOT"){  #HOJAS TOTALES
	$retval = new Graficas_S_O;
	echo $retval->Grafica_semana("HOJTOT",11);
}
?>