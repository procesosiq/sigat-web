<?php

class Datos_Ciclos{
	
	private $bd;
	private $session;
	
	public function __construct() {
        $this->bd = new M_Conexion();
        $this->session = Session::getInstance();
    }

	private function Indicadores(){
        $filters = $this->getFilters();
        $sWhere = $response->fincas;        
        if($filters->year  > 0){
			$sWhere .= " AND YEAR(fecha_real) = $filters->year";
		}
		$response = new stdClass;
        $response->tags = [];
        // 11/05/2017 - TAG: CICLOS PROMEDIO
        $sql = "SELECT AVG(fila) AS ciclos_promedio FROM (
                SELECT finca ,  COUNT(1) AS fila FROM ciclos_aplicacion_historico
                WHERE 1 = 1 {$sWhere}
                GROUP BY finca) AS ciclos_promedio";
        $ciclos_promedio = $this->bd->queryRow($sql);

        $response->tags[] = (object)[
            "tittle" => "Ciclos Promedio",
            "subtittle" => "",
            "valor" => (double)round($ciclos_promedio->ciclos_promedio,2),
            "promedio" => (double)round($ciclos_promedio->ciclos_promedio,2),
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: CICLOS PROMEDIO
        // 11/05/2017 - TAG: MAYOR CICLO
        $sql = "SELECT finca  , fecha_real,  COUNT(1) AS max_finca FROM ciclos_aplicacion_historico
                WHERE 1 = 1 {$sWhere}
                GROUP BY finca
                ORDER BY COUNT(1) ASC, fecha_real DESC
                LIMIT 1";
        $ciclos_mayor = $this->bd->queryRow($sql);
        $response->tags[] = (object)[
            "tittle" => $ciclos_mayor->finca.". Mayor Ciclos",
            "subtittle" => "",
            "valor" => (double)round($ciclos_mayor->max_finca,2),
            "promedio" => (double)round($ciclos_mayor->max_finca,2),
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: MAYOR CICLO
        // 11/05/2017 - TAG: MENOR CICLO
        $sql = "SELECT finca  , fecha_real,  COUNT(1) AS min_finca FROM ciclos_aplicacion_historico
                WHERE 1 = 1 {$sWhere}
                GROUP BY finca
                ORDER BY COUNT(1) DESC , fecha_real ASC
                LIMIT 1";
        $ciclos_menor = $this->bd->queryRow($sql);
        $response->tags[] = (object)[
            "tittle" => $ciclos_menor->finca.". Menor Ciclos",
            "subtittle" => "",
            "valor" => (double)round($ciclos_menor->min_finca,2),
            "promedio" => (double)round($ciclos_menor->min_finca,2),
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: MENOR CICLO
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO
        $sql = "SELECT AVG(costo_ha) ha_ciclos FROM ciclos_aplicacion_historico WHERE 1 = 1 {$sWhere}";
        $ha_ciclos = $this->bd->queryRow($sql);
        $response->tags[] = (object)[
            "tittle" => "/Ha Ciclos Promedio",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos->ha_ciclos,2),
            "promedio" => (double)round($ha_ciclos->ha_ciclos,2),
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO
                // 11/05/2017 - TAG: HA CICLOS PROMEDIO MAX
        $sql = "SELECT finca , costo_ha FROM ciclos_aplicacion_historico
        WHERE 1 = 1 {$sWhere}
        ORDER BY costo_ha DESC LIMIT 1";
        $ha_ciclos_max = $this->bd->queryRow($sql);
        $response->tags[] = (object)[
            "tittle" => $ha_ciclos_max->finca.". Mayor $/Ha Ciclos",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos_max->costo_ha,2),
            "promedio" => (double)round($ha_ciclos_max->costo_ha,2),
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO MAX
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO MIN
        $sql = "SELECT finca , costo_ha FROM ciclos_aplicacion_historico
        WHERE 1 = 1 {$sWhere}
        ORDER BY costo_ha ASC LIMIT 1";
        $ha_ciclos_min = $this->bd->queryRow($sql);
        $response->tags[] = (object)[
            "tittle" => $ha_ciclos_min->finca.". Menor $/Ha Ciclos",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos_min->costo_ha,2),
            "promedio" => (double)round($ha_ciclos_min->costo_ha,2),
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO MIN
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO
        $sql = "SELECT AVG(costo_ha)  AS costo_ha
                FROM (
                SELECT finca , SUM(costo_ha) AS costo_ha FROM ciclos_aplicacion_historico
                WHERE 1 = 1 {$sWhere}
                GROUP BY finca) AS costo_ha_anio";
        $ha_ciclos_anio = $this->bd->queryRow($sql);
         $response->tags[] = (object)[
            "tittle" => "$/Ha año promedio",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos_anio->costo_ha,2),
            "promedio" => (double)round($ha_ciclos_anio->costo_ha,2),
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MAX
        $sql = "SELECT finca , costo_ha AS ha_ciclos_max
                FROM (
                SELECT finca , SUM(costo_ha) AS costo_ha FROM ciclos_aplicacion_historico
                WHERE 1 = 1 {$sWhere}
                GROUP BY finca) AS costo_ha_anio
                ORDER BY costo_ha DESC
                LIMIT 1";
        $ha_ciclos_anio_max = $this->bd->queryRow($sql);
        $response->tags[] = (object)[
            "tittle" => $ha_ciclos_anio_max->finca.". Mayor $/Ha año",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos_anio_max->ha_ciclos_max,2),
            "promedio" => (double)round($ha_ciclos_anio_max->ha_ciclos_max,2),
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MAX
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MIN
        $sql = "SELECT finca , costo_ha AS ha_ciclos_min
                FROM (
                SELECT finca , SUM(costo_ha) AS costo_ha FROM ciclos_aplicacion_historico
                WHERE 1 = 1 {$sWhere}
                GROUP BY finca) AS costo_ha_anio
                ORDER BY costo_ha ASC
                LIMIT 1";
        $ha_ciclos_anio_min = $this->bd->queryRow($sql);
        $response->tags[] = (object)[
            "tittle" => $ha_ciclos_anio_min->finca.". Menor $/Ha año",
            "subtittle" => "",
            "valor" => (double)round($ha_ciclos_anio_min->ha_ciclos_min,2),
            "promedio" => (double)round($ha_ciclos_anio_min->ha_ciclos_min,2),
            "cssClass" => "blue-steel"
        ];
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MIN

        return $response;
	}

    private function getFilters(){
        $data = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->gerente = (int)$data->params->gerente;
        $response->year = (double)$data->params->year;
		$response->fincas = "";

		if($response->gerente > 0){
			$response->fincas = "AND finca IN(SELECT nombre FROM cat_fincas WHERE id_gerente = $response->gerente)";
		}
        return $response;
    }

	public function getIndicadores(){ 
		$response = new stdClass;
		$response = $this->Indicadores();
		return json_encode($response);
	}
}

?>