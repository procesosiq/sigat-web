<?php

if($page != "index"){
	include 'conexion.php';
}
class Datos_Resumen {
	
	private $bd;
	private $session;
	
	public function __construct() {
        $this->bd = new M_Conexion();
        $this->session = Session::getInstance();
    }

	private function Indicadores(){
		$response = new stdClass;

         // 12/05/2017 - TAG: GERENTES
        $sql = "SELECT id AS id , nombre AS label FROM cat_gerentes WHERE status = 1";
        $response->gerentes = $this->bd->queryAll($sql);
        // 12/05/2017 - TAG: GERENTES

         // 12/05/2017 - TAG: FINCAS
        $sql = "SELECT finca AS id , finca AS label FROM ciclos_aplicacion_historico GROUP BY finca";
        $response->fincas = $this->bd->queryAll($sql);
        // 12/05/2017 - TAG: FINCAS

        // 12/05/2017 - TAG: TABLA DE CICLOS APLICADOS
        $sql = "SELECT finca ,  COUNT(1) AS fila FROM ciclos_aplicacion_historico GROUP BY finca ORDER BY COUNT(1) DESC";
        $response->ciclos_aplicados = $this->bd->queryAll($sql);
        // 12/05/2017 - TAG: TABLA DE CICLOS APLICADOS
        
        // 12/05/2017 - TAG: TABLA DE CICLOS APLICADOS AÑO
        $sql = "SELECT finca , AVG(costo_ha) AS fila FROM ciclos_aplicacion_historico GROUP BY finca ORDER BY costo_ha DESC";
        $response->ciclos_aplicados_anio = $this->bd->queryAll($sql);
        // 12/05/2017 - TAG: TABLA DE CICLOS APLICADOS AÑO

        // 12/05/2017 - TAG: TABLA DE CICLOS APLICADOS AÑO
        $sql = "SELECT finca , SUM(costo_ha) AS fila FROM ciclos_aplicacion_historico GROUP BY finca ORDER BY costo_ha DESC";
        $response->ciclos_aplicados_sum_anio = $this->bd->queryAll($sql);
        // 12/05/2017 - TAG: TABLA DE CICLOS APLICADOS AÑO

        // 12/05/2017 - TAG: GRAFICA CICLOS APLICADOS
        $sql = "SELECT finca AS label , YEAR(fecha_real) AS legend ,COUNT(1) AS value , IF(YEAR(fecha_real) > 2013 ,true,false) AS selected
                FROM ciclos_aplicacion_historico
                GROUP BY YEAR(fecha_real) , finca
                ORDER BY fecha_real ASC  , finca";
        $data = $this->bd->queryAll($sql);
        $response->ciclos_aplicados_chart =(object)$this->chartInit($data,"vertical","Ciclos Aplicados","line");
        // 12/05/2017 - TAG: GRAFICA CICLOS APLICADOS

        // 12/05/2017 - TAG: GRAFICA CICLOS APLICADOS AÑOS SUM
        $sql = "SELECT finca AS label , YEAR(fecha_real) AS legend ,SUM(costo_ha) AS value , IF(YEAR(fecha_real) > 2013 ,true,false) AS selected
                FROM ciclos_aplicacion_historico
                GROUP BY YEAR(fecha_real) , finca
                ORDER BY fecha_real ASC  , finca";
        $data = $this->bd->queryAll($sql);
        $response->ciclos_aplicados_sum_chart =(object)$this->chartInit($data,"vertical","HECTAREA/AÑO","line");
        // 12/05/2017 - TAG: GRAFICA CICLOS APLICADOS AÑOS SUM

         // 12/05/2017 - TAG: GRAFICA CICLOS APLICADOS AÑOS AVG
        $sql = "SELECT finca AS label , YEAR(fecha_real) AS legend ,AVG(costo_ha) AS value , IF(YEAR(fecha_real) > 2013 ,true,false) AS selected
                FROM ciclos_aplicacion_historico
                GROUP BY YEAR(fecha_real) , finca
                ORDER BY fecha_real ASC  , finca";
        $data = $this->bd->queryAll($sql);
        $response->ciclos_aplicados_avg_chart =(object)$this->chartInit($data,"vertical","HECTAREA/CICLO","bar");
        // 12/05/2017 - TAG: GRAFICA CICLOS APLICADOS AÑOS AVG


        // 12/05/2017 - TAG: CICLOS ATRASADOS
        $sql = "SELECT ciclo FROM ciclos_aplicacion_historico GROUP BY ciclo";
        $response->ciclos = $this->bd->queryAll($sql);
        // 12/05/2017 - TAG: CICLOS ATRASADOS


        // 12/05/2017 - TAG: CICLOS ATRASADOS DATA
        $sql = "SELECT finca , ciclo , atraso FROM ciclos_aplicacion_historico ORDER BY finca , ciclo";
        $data = $this->bd->queryAll($sql);
        $response->data_ciclos = [];
        $bgColor = "";
        $atraso = 0;
        foreach ($data as $key => $value) {
            $atraso = (int)$value->atraso;
            $response->data_ciclos[$value->finca]["finca"] = $value->finca;
            $response->data_ciclos[$value->finca]["ciclos"][$value->ciclo] = (int)$atraso;
            if($atraso < 0){
                $bgColor = "bg-yellow-gold bg-font-yellow-gold";
            }else if($atraso == 0){
                $bgColor = "bg-green-haze bg-font-green-haze";
            }else{
                $bgColor = "bg-red-thunderbird bg-font-red-thunderbird";
            }
            $response->data_ciclos[$value->finca]["className"][$value->ciclo] = $bgColor;
        }
        // 12/05/2017 - TAG: CICLOS ATRASADOS DATA

        // 12/05/2017 - TAG: MOTIVO
        $sql = "SELECT motivo AS label , COUNT(motivo) AS value FROM ciclos_aplicacion_historico GROUP BY motivo";
        $data = $this->bd->queryAll($sql);
        $response->motivo = $this->pie($data , ['0%', '50%'] , "");
        // 12/05/2017 - TAG: MOTIVO

        return $response;
	}

	public function getIndicadores(){ 
		$response = new stdClass;
		$response = $this->Indicadores();
		return $response;
	}

    private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = []){
		$response = new stdClass;
		$response->chart = [];
		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["orient"] = "vertical";
			$response->chart["legend"]["bottom"] = "1%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "15%";
			$response->chart["grid"]["containLabel"] = true;
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""] , "axisLabel" => ["rotate" => 60] , "margin" => 2];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value"];
			}
			$response->chart["series"] = [];
			$count = 0;
			$position = -1;
			$colors = [];
			if($type == "line"){
				$response->chart["xAxis"]["data"] = [];
			}
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = ucwords(strtolower($value->legend));
				$value->label = ucwords(strtolower($value->label));
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if("line" == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						$response->chart["xAxis"]["data"][] = $value->label;
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					}

					$response->chart["series"][$position]["data"][] = ROUND($value->value,2);

				}
                // else{
				// 	$response->chart["legend"]["data"][] = $value->label;
				// 	// if($count == 0){
				// 		$response->chart["series"][$count] = [
				// 			"name" => $value->label,
				// 			"type" => $type,
				// 			"data" => [(int)$value->value],
				// 			"label" => [
				// 				"normal" => ["show" => true , "position" => "top"],
				// 				"emphasis" => ["show" => true , "position" => "top"],
				// 			],
				// 			"itemStyle" => $colors
				// 		];
				// 	// }
				// }
				// $count++;
			}
		}

		return $response->chart;
	}
	private function pie($data = [] , $radius = ['50%', '70%'] , $name = "CATEGORIAS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['45%', '40%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = $this->session->sloganCompany;
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["magicType"]["show"] = false;
			$response->pie["toolbox"]["feature"]["magicType"]["type"] = ['pie'];
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["x"] = "center";
			$response->pie["legend"]["y"] = "bottom";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];
			$colors = [];
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->label = ucwords(strtolower($value->label));
				$response->pie["legend"]["data"][] = $value->label;
				if($version === 3){
					if(isset($value->color)){
							$colors = [
								"normal" => ["color" => $value->color],
							];
					}
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value , 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							],
							"itemStyle" => $colors
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value];
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value,
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
							"emphasis" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}
}

?>