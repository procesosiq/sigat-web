<?php
include 'class.sesion.php';
include 'conexion.php';

class Graficas_S_C{
	
	private $conexion;
	private $session;
	
	public function __construct() {
        $this->conexion = new M_Conexion();
        $this->session = Session::getInstance();
    }
	
	public function ConsultasCeroSemanas($tipo,$tipo_semana){
		// print_r($this->db2_conn_error()xion);
		$sql = "SELECT YEAR(prin.fecha) AS anoo , foco 
				FROM muestras_hacienda_detalle_3M as det
				INNER JOIN muestras_haciendas_3M as prin on det.id_Mhacienda = prin.id
				WHERE YEAR(prin.fecha) > 0 
				AND prin.id_usuario = '{$this->session->logged}' 
				AND prin.id_cliente =  '{$this->session->client}'
				GROUP BY foco ,YEAR(fecha) 
				ORDER BY anoo";
				// echo $sql;
				// return;
			$res = $this->conexion->link->query($sql);
			$datos = array();
			while($fila = $res->fetch_assoc()){
				
				if($tipo=='HMVLDQMEN5'){ 
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					foco,
					AVG(hoja_mas_vieja_libre_quema_menor) AS hojaViejaLibreQuemaMenor
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."'
					AND muestras_haciendas_3M.id_usuario = '{$this->session->logged}' AND muestras_haciendas_3M.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["umbral"][] = [0 , 11];
					$datos["umbral"][] = [26 , 11];
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMenor"]);
					}
				}
				if($tipo=="HMVLDE"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(hoja_mas_vieja_de_estrias) AS hojaViejaLibreEstrias,
					foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario = '{$this->session->logged}' AND muestras_haciendas_3M.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["umbral"][] = [0 , 6.5];
					$datos["umbral"][] = [26 , 6.5];
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreEstrias"]);
					}
				}
				if($tipo=="HOJA4"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(total_hoja_4) AS hojaLibreCirugias,foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario = '{$this->session->logged}' AND muestras_haciendas_3M.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaLibreCirugias"]);
					}
				}
				if($tipo=="HOJA3"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(total_hoja_3) AS hojaViejaLibreQuemaMayor,
					foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario = '{$this->session->logged}' AND muestras_haciendas_3M.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMayor"]);
					}
				}
				if($tipo=="HOJA5"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(total_hoja_5) AS hojaViejaLibreQuemaMayor,
					foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario = '{$this->session->logged}' AND muestras_haciendas_3M.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojaViejaLibreQuemaMayor"]);
					}
				}
				if($tipo=="HOJTOT"){
					$sql2="
					SELECT 
					WEEK(muestras_haciendas_3M.fecha) AS numsemana,
					COUNT(*) AS numreg,
					AVG(hojas_totales) AS hojasTotales,foco
					FROM muestras_haciendas_3M 
					INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda

					WHERE YEAR(muestras_haciendas_3M.fecha) = '".$fila['anoo']."' AND foco = '".$fila['foco']."' 
					AND muestras_haciendas_3M.id_usuario = '{$this->session->logged}' AND muestras_haciendas_3M.id_cliente = '{$this->session->client}'
					GROUP BY foco,numsemana 
					ORDER BY numsemana,foco ";
					$datos["umbral"][] = [0 , 11];
					$datos["umbral"][] = [26 , 11];
					$res2 = $this->conexion->link->query($sql2);
					while($fila2 = $res2->fetch_assoc()){
						$datos[$fila["foco"]][] = array ($fila2["numsemana"],$fila2["hojasTotales"]);
					}
				}
			}
			return $datos;
	}

	public function Grafica_semana($tipo,$tipo_semana){
		$datos = array();
		$datos = $this->ConsultasCeroSemanas($tipo,$tipo_semana);
		return json_encode($datos);
	}
}

$postdata = (object)json_decode(file_get_contents("php://input"));
$retval = new Graficas_S_C;

if($postdata->opt == "HMVLDQMEN5"){ #HOJA MAS VIEJA LIBRE DE QUEMA MENOR A 5%
	echo $retval->Grafica_semana("HMVLDQMEN5",0);
}
else if($postdata->opt == "HMVLDE"){  #hoja mas vieja libre de estias
	echo $retval->Grafica_semana("HMVLDE",0);
}
else if($postdata->opt == "HOJA4"){  #hojas libres de cirugias 
	echo $retval->Grafica_semana("HOJA4",0);
}
else if($postdata->opt == "HOJA3"){  #HOJA MAS VIEJA LIBRE DE QUEMA MAYOR A 5%
	echo $retval->Grafica_semana("HOJA3",0);
}
else if($postdata->opt == "HOJA5"){  #HOJA MAS VIEJA LIBRE DE QUEMA MAYOR A 5%
	echo $retval->Grafica_semana("HOJA5",0);
}
else if($postdata->opt == "HOJTOT"){  #HOJAS TOTALES
	echo $retval->Grafica_semana("HOJTOT",0);
}
?>