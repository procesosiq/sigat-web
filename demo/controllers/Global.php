<?php
/**
* 
*/
class Global 
{
	private $session;
	
	function __construct(){
		$this->session = Session::getInstance();
	}	

	private function redirect($url = "login.php"){
		echo "Entro";
		// header('Location: http://sigat.procesos-iq.com/'.$url);
	}

	public function is_logged() {
		print_r($this->session);
		return isset($this->session->logged);
	}

	public function if_not_logged_redirect() {
		if (!$this->is_logged()) {
			$this->redirect("login.php");
		}
	}

	public function if_logged_redirect() {
		if ($this->is_logged()) {
			$this->redirect("index.php");
		}
	}
}
?>