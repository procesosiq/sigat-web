<?php

class Session {

	private $uid  = "";
	private $ssid = "";
	protected $session = array();

	/**
	 * @var Object instance
	 */
	private static $instance = array();


	public function __construct($sessionName=null, $sessionId=null){
		if (!is_null($sessionId)) {
			$this->uid = md5($sessionId);
			session_write_close();
			$this->ssid = session_id($session_id);
			session_start();
			$this->session = &$_SESSION['SIGAT'];
		}else{
			if (!$this->session_started()) { session_start(); }
			$this->session = &$_SESSION['SIGAT'];
		}

		if (!isset($_SESSION['SIGAT'])) {
			$_SESSION['SIGAT'] = array();
			$this->session = &$_SESSION['SIGAT'];
		}


		if (!is_null($sessionName)) {
			$this->session[$this->uid.$sessionName] = session_name($sessionName);
		}
	}

	/**
	 * Singleton pattern http://en.wikipedia.org/wiki/Singleton_pattern
	 * @return object Class Instance
	 */
	public static function getInstance()
	{
		if (!self::$instance instanceof self)
			self::$instance = new self;

		return self::$instance;
	}

	/**
	 * Validate if the session has been started
	 *
	 * @return boolean
	 */
	private function session_started()
	{
		if(isset($_SESSION))
			return true;

		return false;
	}

	public function kill() {
		$_SESSION = [];
		$this->session = [];

		session_destroy();
		$this->redirect();
	}

	private function redirect($url = "login.php"){
		// echo "Entro";
		header('Location: http://sigat.procesos-iq.com/'.$url);
	}


	function __set($name, $value = '')
	{
		$this->session[$name] = $value;

		return true;
	}

	function __get($name) {
		if (isset($this->session[$name])) {
			return $this->session[$name];
		}

		return NULL;
	}

	function __isset($name) {
		return isset($this->session[$name]);
	}

	function __unset($name) {
		if (isset($this->session[$name])) {
			unset($this->session[$name]);
		}

		return false;
	}

}

?>