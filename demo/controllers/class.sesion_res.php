<?php

class sesiones{
	
	public function __construct(){
        session_start();
    }
	
	public function borrarsesion(){
		session_destroy();
	}
	
	public function setS($nombre,$valor){
		$_SESSION [$nombre] = $valor;
	}
	
	public function getS($nombre) {
		if (isset ( $_SESSION [$nombre] )) {
			return $_SESSION [$nombre];
		} else {
			return false;
		}
	}
	public function borrar_variable($nombre) {
		unset ( $_SESSION [$nombre] );
	}
}

?>