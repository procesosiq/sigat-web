<?php
include 'conexion.php';
include 'class.sesion.php';
class Graficas_M{
	
	private $conexion;
	
	public function __construct() {
        $this->conexion = new M_Conexion();
        $this->session = Session::getInstance();
    }
	
	public function Grafica_clima($tipo){ 
		$datos = array();
		$datos = ConsultaClimas($tipo);
		return json_encode($datos);
	}

	public function ConsultaClimas($tipo){
		$sql = "SELECT YEAR(fecha) AS anoo FROM datos_clima GROUP BY YEAR(fecha) ORDER BY anoo";
		$res = $this->link->query($sql);
		$datos = array();
		while($fila = $res->fetch_assoc()){
			
			if($tipo=='TEM_MIN'){ #TEMPERATURA MINIMA
				$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
				MIN(temp_minima) AS Tmin
				FROM datos_clima 
				WHERE YEAR(fecha)='".$fila['anoo']."'
				GROUP BY numsemana
				ORDER BY numsemana";
				$datos["umbral"][] = [0 , 20.5];
				$datos["umbral"][] = [52 , 20.5];
				$res2 = $this->link->query($sql2);
				while($fila2 = $res2->fetch_assoc()){
					$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Tmin"]);
				}
			}
			else if($tipo=='TEM_MAX'){ #TEMPERATURA MAXIMA
				$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
				MAX(temp_maxima) AS Tmax
				FROM datos_clima 
				WHERE YEAR(fecha)='".$fila['anoo']."'
				GROUP BY numsemana
				ORDER BY numsemana";
				$res2 = $this->link->query($sql2);
				while($fila2 = $res2->fetch_assoc()){
					$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Tmax"]);
				}
			}
			else if($tipo=='LLUVIA'){ #DATOS DE LLUVIA
				$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
				SUM(lluvia) AS totlluvia
				FROM datos_clima 
				WHERE YEAR(fecha)='".$fila['anoo']."'
				GROUP BY numsemana
				ORDER BY numsemana";
				$res2 = $this->link->query($sql2);
				while($fila2 = $res2->fetch_assoc()){
					$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["totlluvia"]);
				}
			}
			else if($tipo=='RAD_SOLAR'){ #RADIACION SOLAR
				$sql2="SELECT WEEK(fecha) AS numsemana,(SUM(rad_solar)/COUNT(*)) AS promedio
				FROM datos_clima 
				WHERE YEAR(fecha)='".$fila['anoo']."'
				GROUP BY numsemana
				ORDER BY numsemana";
				$res2 = $this->link->query($sql2);
				while($fila2 = $res2->fetch_assoc()){
					$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["promedio"]);
				}
			}
			else if($tipo=='DIAS_SOL'){ #RADIACION SOLAR
				$sql2="SELECT WEEK(fecha) AS numsemana,(SUM(dias_sol)/COUNT(*)) AS promedio
				FROM datos_clima 
				WHERE YEAR(fecha)='".$fila['anoo']."'
				GROUP BY numsemana
				ORDER BY numsemana";
				$res2 = $this->link->query($sql2);
				while($fila2 = $res2->fetch_assoc()){
					$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["promedio"]);
				}
			}
			if($tipo=='HUM_MIN'){ #HUMEDAD MINIMA
				$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
				MIN(humedad) AS Hmin
				FROM datos_clima 
				WHERE YEAR(fecha)='".$fila['anoo']."'
				GROUP BY numsemana
				ORDER BY numsemana";
				$res2 = $this->link->query($sql2);
				while($fila2 = $res2->fetch_assoc()){
					$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Hmin"]);
				}
			}
			else if($tipo=='HUM_MAX'){ #HUMEDAD MAXIMA
				$sql2="SELECT WEEK(fecha) AS numsemana,COUNT(*) AS numreg,
				MAX(humedad) AS Hmax
				FROM datos_clima 
				WHERE YEAR(fecha)='".$fila['anoo']."'
				GROUP BY numsemana
				ORDER BY numsemana";
				$res2 = $this->link->query($sql2);
				while($fila2 = $res2->fetch_assoc()){
					$datos[$fila["anoo"]][] = array ($fila2["numsemana"],$fila2["Hmax"]);
				}
			}
		}
		return $datos;
	} 
}

$postdata = (object)json_decode(file_get_contents("php://input"));
if($postdata->opt == "TEMMIN"){ 
	$retval = new Graficas_M();
	echo $retval->Grafica_clima("TEM_MIN");
}
else if($postdata->opt == "TEMMAX"){ 
	$retval = new Graficas_M();
	echo $retval->Grafica_clima("TEM_MAX");
}
else if($postdata->opt == "LLUVIA"){ 
	$retval = new Graficas_M();
	echo $retval->Grafica_clima("LLUVIA");
}
else if($postdata->opt == "RADSOLAR"){ 
	$retval = new Graficas_M();
	echo $retval->Grafica_clima("RAD_SOLAR");
}
else if($postdata->opt == "DIASSOL"){ 
	$retval = new Graficas_M();
	echo $retval->Grafica_clima("DIAS_SOL");
}
else if($postdata->opt == "HUMMIN"){ 
	$retval = new Graficas_M();
	echo $retval->Grafica_clima("HUM_MIN");
}
else if($postdata->opt == "HUMMAX"){ 
	$retval = new Graficas_M();
	echo $retval->Grafica_clima("HUM_MAX");
}
?>