<?php
include 'class.sesion.php';
include 'conexion.php';
class Graficas_S_C{
	
	private $conexion;
	private $session;
	
	public function __construct() {
        $this->conexion = new M_Conexion();
        $this->session = Session::getInstance();
        $this->session->client = 1;
        $this->session->finca = 8;
    }
	
	public function ConsultasGrafica(){

		$sql_get_weeks = "SELECT WEEK(fecha) AS semana FROM foliar WHERE 
		id_usuario = '{$this->session->logged}' AND 
		id_cliente = '{$this->session->client}' AND 
		id_hacienda = '{$this->session->finca}' AND 
		YEAR(fecha) = YEAR(CURRENT_TIMESTAMP) GROUP BY WEEK(fecha)";
		$weeks = $this->conexion->Consultas(2, $sql_get_weeks);
		foreach ($weeks as $key => $value) {
			$sem = $value["semana"];
			if($key == 0 || $value["id"] <= 41){
				$sql_emision_0 = "SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/10 AS hoj_sem FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'";
				// echo $sql_emision_0;
				$resul['Emision Foliar'][] = [$sem ,$this->conexion->Consultas(2, $sql_emision_0)[0]["hoj_sem"]];
			}
			else if($key > 0){

				$post_sem = $sem - 1;

				$sql_emision_1 = "
						SELECT(
							(SELECT emision1 FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')
								-
							(SELECT emision1 FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')
						)
						/
						(SELECT 
							(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')
								-
							(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')
						)*7
						AS hoj_sem";

				$sql_emision_2 = "SELECT((SELECT emision2 FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT emision2 FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = 11 AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))*7 AS hoj_sem";
				$sql_emision_3 = "SELECT((SELECT emision3 FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT emision3 FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = 11 AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))*7 AS hoj_sem";
				$sql_emision_4 = "SELECT((SELECT emision4 FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT emision4 FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = 11 AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))*7 AS hoj_sem";
				$sql_emision_5 = "SELECT((SELECT emision5 FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT emision5 FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = 11 AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))*7 AS hoj_sem";
				$sql_emision_6 = "SELECT((SELECT emision6 FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT emision6 FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = 11 AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))*7 AS hoj_sem";
				$sql_emision_7 = "SELECT((SELECT emision7 FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT emision7 FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = 11 AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))*7 AS hoj_sem";
				$sql_emision_8 = "SELECT((SELECT emision8 FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT emision8 FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = 11 AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))*7 AS hoj_sem";
				$sql_emision_9 = "SELECT((SELECT emision9 FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT emision9 FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = 11 AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))*7 AS hoj_sem";
				$sql_emision_10 = "SELECT((SELECT emision10 FROM foliar WHERE WEEK(fecha) = $post_sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT emision10 FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))/(SELECT (SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = 11 AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}')-(SELECT DAY(MIN(fecha)) FROM foliar WHERE WEEK(fecha) = $sem AND id_usuario = '{$this->session->logged}' AND id_cliente = '{$this->session->client}' AND id_hacienda = '{$this->session->finca}'))*7 AS hoj_sem";

				$emision_1 = $this->conexion->Consultas(2, $sql_emision_1)[0]["hoj_sem"];
				$emision_2 = $this->conexion->Consultas(2, $sql_emision_2)[0]["hoj_sem"];
				$emision_3 = $this->conexion->Consultas(2, $sql_emision_3)[0]["hoj_sem"];
				$emision_4 = $this->conexion->Consultas(2, $sql_emision_4)[0]["hoj_sem"];
				$emision_5 = $this->conexion->Consultas(2, $sql_emision_5)[0]["hoj_sem"];
				$emision_6 = $this->conexion->Consultas(2, $sql_emision_6)[0]["hoj_sem"];
				$emision_7 = $this->conexion->Consultas(2, $sql_emision_7)[0]["hoj_sem"];
				$emision_8 = $this->conexion->Consultas(2, $sql_emision_8)[0]["hoj_sem"];
				$emision_9 = $this->conexion->Consultas(2, $sql_emision_9)[0]["hoj_sem"];
				$emision_10 = $this->conexion->Consultas(2, $sql_emision_10)[0]["hoj_sem"];

				$resul['Emision Foliar'][] = [$sem,($emision_1 + $emision_2 + $emision_3 + $emision_4 + $emision_5 + $emision_6 + $emision_7 + $emision_8 + $emision_9 + $emision_10)/10];
			}
		}
		return $resul;
	}
	public function Grafica(){
		$resul = array();
		$resul = $this->ConsultasGrafica();
		return json_encode($resul);
	}
}

$postdata = (object)json_decode(file_get_contents("php://input"));
if($postdata->opt == "GRAFICA"){ 
	$retval = new Graficas_S_C;
	echo $retval->Grafica("GRAFICA");
}
?>