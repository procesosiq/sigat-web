<?php

// include 'class.sesion.php';

#global     $session = Session::getInstance();

class M_Conexion { 
    private $host="localhost";
    private $db_user="auditoriasbonita";
    private $db_pass="u[V(fTIUbcVb";
    private $db_name="sigat";
    public $link; 

    public $insert = 1;
    public $selects = 2;

    private $select;
    private $from;
    private $where = array();
    private $group_by = array();
    private $order_by = array();

    public function __construct(){ 
        $this->link = new mysqli($this->host, $this->db_user, $this->db_pass, $this->db_name);
        if ( $this->link->connect_errno ) 
        { 
            echo "Fallo al conectar a MySQL: ". $this->link->connect_error; 
            return;
        }
    }

    public function group_by($grupos){
        foreach ($grupos as $key => $value) {
            $order_by[$key] = $value;
        }
    }

    public function order_by($ordenes){
        foreach ($ordenes as $key => $value) {
            $order_by[$key] = $value; 
        }
    }

    public function where($condiciones){
        foreach ($condiciones as $key => $value) {
            $where[$key] = $value;
        }
    }

    public function get($tabla){
        $from = $tabla;

        if(isset($select)){
            $sql = "SELECT ".$select;
        }else{
            $sql = "SELECT *";
        }

        $sql .= " FROM {$from}";
        if(count($where)>0){
            $count = 0;
            $sql .= " WHERE ";
            foreach ($where as $key => $value) {
                if($count > 0 && $count + 1 < count($where))
                    $sql .= ",";
                $sql .= " {$key} = {$value} ";
                $count++;
            }
        }
        if(count($order_by)>0){
            $sql .= " ORDER BY";
            foreach ($order_by as $key => $value) {
                $sql .= " {$key} = {$value}";
            }
        }
        if(count($group_by)>0){
            $sql .= " GROUP BY";
            foreach ($group_by as $key => $value) {
                $sql .= " {$key} = {$value}";
            }
        }
        return $this->link->query($sql);
    }
    
    public function Consultas($tipo,$sql){
        if($tipo == 1){
            if($this->link->query($sql)){
                $ids=$this->link->insert_id;
                return $ids;
            }else{
                print_r($this->link->error);
            }
        }
        else if($tipo == 2){
            $res = $this->link->query($sql);
            $datos = array();
            while($fila = $res->fetch_assoc()){
                $datos[] = $fila;
            }
            return $datos;
        }
    }
    
    public function ClimaTop(){
        $sql="SELECT YEAR(fecha) AS anoo,WEEK(fecha) AS numsemana,
        MIN(temp_minima) AS Tmin,MAX(temp_maxima) AS Tmax,
        SUM(lluvia) AS Tlluvia,AVG(rad_solar) AS rad_solar
        FROM datos_clima 
        WHERE YEAR(fecha)
        GROUP BY numsemana
        ORDER BY anoo DESC,numsemana DESC LIMIT 1";
        $res = $this->link->query($sql);
        $datos = array();
        while($fila = $res->fetch_assoc()){
            $datos[] = $fila;
        }
        return $datos;
    }
}

?>