<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

include '../controllers/conexion.php';

/** Directorio de los JSON */
$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        $pos1 = strpos($object->getPath(), "nuevo/emision_foliar/json_");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        process_json($json, $object->getFileName());
                        move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        move_image($object->getPathName(), $object->getFileName());
                        break;

                    default:
                        
                        break;
                }
            }
        }
    }
}

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/procesado/json/$nameFile")){
        echo 'error';
    }
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/procesado/image/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename){
    $conexion = new M_Conexion;

    $identifier = $json['identifier'];
    $version = $json['version']. '';;
    $zone = $json['zone']. '';;
    $referenceNumber = $json['referenceNumber']. '';;
    $state = $json['state']. '';;
    $deviceSubmitDate = $json['deviceSubmitDate'];
    $deviceSubmitDateDate = $deviceSubmitDate['time']. '';;
    $deviceSubmitDateZone = $deviceSubmitDate['zone']. '';;
    $fecha_file = str_replace("-", "", explode("T", $deviceSubmitDateDate)[0]);

    $shiftedDeviceSubmitDate = $json['shiftedDeviceSubmitDate'].'';;
    $serverReceiveDate = $json['serverReceiveDate'].'';;
    $form = $json['form'];
    $form_identifier = $form['identifier'].'';;
    $form_versionIdentifier = $form['versionIdentifier'].'';;
    $form_name = $form['name'].'';;
    $form_version = $form['version'].'';;
    $form_formSpaceIdentifier = $form['formSpaceIdentifier'].'';;
    $form_formSpaceName = $form['formSpaceName'].'';;

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';;
    $user_username = $user['username'].'';;
    $user_displayName = $user['displayName'].'';;

    $geoStamp = $json['geoStamp'];
    $geoStamp_success = $geoStamp['success'].'';;
    $geoStamp_captureTimestamp = $geoStamp['captureTimestamp'];
    $geoStamp_captureTimestamp_provided = $geoStamp_captureTimestamp['provided'];
    $geoStamp_captureTimestamp_provided_time = $geoStamp_captureTimestamp_provided['time'].'';;
    $geoStamp_captureTimestamp_provided_zone = $geoStamp_captureTimestamp_provided['zone'].'';;
    $geoStamp_captureTimestamp_shifted = $geoStamp_captureTimestamp['shifted'].'';;
    $geoStamp_errorMessage = $geoStamp['errorMessage'].'';;
    $geoStamp_source = $geoStamp['source'].'';;
    $geoStamp_coordinates = $geoStamp['coordinates'];
    $geoStamp_coordinates_latitude = $geoStamp_coordinates['latitude'].'';;
    $geoStamp_coordinates_longitude = $geoStamp_coordinates['longitude'].'';;
    $geoStamp_coordinates_altitude = $geoStamp_coordinates['altitude'].'';;
    $geoStamp_address = $geoStamp['address'].'';;
    $pages = $json['pages'];

    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = $page_data['name'];
        $answers = $page_data["answers"];

        foreach($answers as $x => $answer){

            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];

            $labelita = limpiar(trim(strtolower($label)));
            if($pagina_nombre == "ENCABEZADO"){
                if($question == "CLIENTE:"){
                    $cliente = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($question == "CLIENTE: - id"){
                    $id_cliente = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($question == "FINCA:"){
                    $finca = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($question == "FINCA: - id"){
                    $id_finca = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($question == "SUPERVISOR:"){
                    $supervisor = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($question == "FECHA:"){
                    $fecha = (isset($values[0]) ? "'".$values[0]."'" : '(NULL)');
                }
            }

            if($pagina_nombre == "EMISION FOLIAR"){
                if($question == "Planta 1"){
                    $emision[0] = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($question == "Planta 2"){
                    $emision[1] = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($question == "Planta 3"){
                    $emision[2] = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($question == "Planta 4"){
                    $emision[3] = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($question == "Planta 5"){
                    $emision[4] = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($question == "Planta6"){
                    $emision[5] = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($question == "Planta7"){
                    $emision[6] = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($question == "Planta8"){
                    $emision[7] = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($question == "Planta9"){
                    $emision[8] = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($question == "Planta 10"){
                    $emision[9] = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
            }
            if($pagina_nombre == "LABORES AGRÍCOLAS"){
                if($question == "CONTROL MALEZAS:"){
                    $malezas = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($label == "EvidenciaMALEZAS"){
                    foreach ($values as $value) {
                        $evi_malezas[] = $value["filename"];
                    }
                }
                if($question == "DESHOJE:"){
                    $deshoje = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($label == "EvidenciaDESHOJE"){
                    foreach ($values as $value) {
                        $evi_deshoje[] = $value["filename"];
                    }
                }
                if($question == "DESHOJE FITO:"){
                    $deshojefito = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($label == "EvidenciaDESHOJEFITO"){
                    foreach ($values as $value) {
                        $evi_deshojefito[] = $value["filename"];
                    }
                }
                if($question == "ENFUNDE (SALVADO DE HOJAS):"){
                    $enfunde = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($label == "EvidenciaENFUNDE"){
                    foreach ($values as $value) {
                        $evi_enfunde[] = $value["filename"];
                    }
                }
                if($question == "DRENAJES:"){
                    $drenaje = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($label == "EvidenciaDRENAJE"){
                    foreach ($values as $value) {
                        $evi_drenaje[] = $value["filename"];
                    }
                }
                if($question == "PLAGAS FOLIARES:"){
                    $plaga = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($label == "EvidenciaPLAGA"){
                    foreach ($values as $value) {
                        $evi_plaga[] = $value["filename"];
                    }
                }
                if($question == "OTRAS OBSERVACIONES:"){
                    $otrasobs = "'".(isset($values[0]) ? $values[0] : '(NULL)')."'";
                }
                if($label == "EvidenciaOTRASOBS"){
                    foreach ($values as $value) {
                        $evi_otrasobs[] = $value["filename"];
                    }
                }
            }
        }
    }

    $sql_get_usuario = "SELECT * FROM cat_usuarios WHERE idProntoforms = '$user_username'";
    $id_usuario = $conexion->Consultas(2, $sql_get_usuario);
    $id_usuario = $id_usuario[0]["id"];

    $consulta_principal = 
    "INSERT INTO foliar(supervisor,fecha,id_usuario,id_cliente,id_hacienda,emision1,emision2,emision3,emision4,emision5,emision6,emision7,emision8,emision9,emision10, malezas, evi_malezas, deshoje, evi_deshoje, deshojefito, evi_deshojefito, enfunde, evi_enfunde, drenaje, evi_drenaje, plaga, evi_plaga, otrasobs, evi_otrasobs) 
    VALUES(".$supervisor
        .",".$fecha
        .",".$id_usuario
        .",".$id_cliente
        .",".$id_finca
        .",".$emision[0]
        .",".$emision[1]
        .",".$emision[2]
        .",".$emision[3]
        .",".$emision[4]
        .",".$emision[5]
        .",".$emision[6]
        .",".$emision[7]
        .",".$emision[8]
        .",".$emision[9]
        .",".$malezas
        .",".(isset($evi_malezas)?"'".json_encode($evi_malezas)."'":'(NULL)')
        .",".$deshoje
        .",".(isset($evi_deshoje)?"'".json_encode($evi_deshoje)."'":'(NULL)')
        .",".$deshojefito
        .",".(isset($evi_deshojefito)?"'".json_encode($evi_deshojefito)."'":'(NULL)')
        .",".$enfunde
        .",".(isset($evi_enfunde)?"'".json_encode($evi_enfunde)."'":'(NULL)')
        .",".$drenaje
        .",".(isset($evi_drenaje)?"'".json_encode($evi_drenaje)."'":'(NULL)')
        .",".$plaga
        .",".(isset($evi_plaga)?"'".json_encode($evi_plaga)."'":'(NULL)')
        .",".$otrasobs
        .",".(isset($evi_otrasobs)?"'".json_encode($evi_otrasobs)."'":'(NULL)')
        .");";
    print_r($consulta_principal);
    $id_principal = $conexion->Consultas(1, $consulta_principal);
}

function limpiar($String){
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
     
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);
    return $String;
}

?>