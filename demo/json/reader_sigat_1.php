<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

include '../controllers/conexion.php';

/** Directorio de los JSON */
$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        $pos1 = strpos($object->getPath(), "nuevo/semanas0/json_");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        process_json($json, $object->getFileName());
                        move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        move_image($object->getPathName(), $object->getFileName());
                        break;

                    default:
                        
                        break;
                }
            }
        }
    }
}

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/procesado/json/$nameFile")){
        echo 'error';
    }
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/procesado/image/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename){
    $conexion = new M_Conexion;

    $identifier = $json['identifier'];
    $version = $json['version']. '';;
    $zone = $json['zone']. '';;
    $referenceNumber = $json['referenceNumber']. '';;
    $state = $json['state']. '';;
    $deviceSubmitDate = $json['deviceSubmitDate'];
    $deviceSubmitDateDate = $deviceSubmitDate['time']. '';;
    $deviceSubmitDateZone = $deviceSubmitDate['zone']. '';;
    $fecha_file = str_replace("-", "", explode("T", $deviceSubmitDateDate)[0]);

    $shiftedDeviceSubmitDate = $json['shiftedDeviceSubmitDate'].'';;
    $serverReceiveDate = $json['serverReceiveDate'].'';;
    $form = $json['form'];
    $form_identifier = $form['identifier'].'';;
    $form_versionIdentifier = $form['versionIdentifier'].'';;
    $form_name = $form['name'].'';;
    $form_version = $form['version'].'';;
    $form_formSpaceIdentifier = $form['formSpaceIdentifier'].'';;
    $form_formSpaceName = $form['formSpaceName'].'';;

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';;
    $user_username = $user['username'].'';;
    $user_displayName = $user['displayName'].'';;

    $geoStamp = $json['geoStamp'];
    $geoStamp_success = $geoStamp['success'].'';;
    $geoStamp_captureTimestamp = $geoStamp['captureTimestamp'];
    $geoStamp_captureTimestamp_provided = $geoStamp_captureTimestamp['provided'];
    $geoStamp_captureTimestamp_provided_time = $geoStamp_captureTimestamp_provided['time'].'';;
    $geoStamp_captureTimestamp_provided_zone = $geoStamp_captureTimestamp_provided['zone'].'';;
    $geoStamp_captureTimestamp_shifted = $geoStamp_captureTimestamp['shifted'].'';;
    $geoStamp_errorMessage = $geoStamp['errorMessage'].'';;
    $geoStamp_source = $geoStamp['source'].'';;
    $geoStamp_coordinates = $geoStamp['coordinates'];
    $geoStamp_coordinates_latitude = $geoStamp_coordinates['latitude'].'';;
    $geoStamp_coordinates_longitude = $geoStamp_coordinates['longitude'].'';;
    $geoStamp_coordinates_altitude = $geoStamp_coordinates['altitude'].'';;
    $geoStamp_address = $geoStamp['address'].'';;
    $pages = $json['pages'];

    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = $page_data['name'];
        $answers = $page_data["answers"];

        foreach($answers as $x => $answer){

            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];

            $labelita = limpiar(trim(strtolower($label)));
            if($pagina_nombre == "ENCABEZADO"){
                if($question == "CLIENTE"){
                    $cliente = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "CLIENTE - id"){
                    $id_cliente = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "FINCA"){
                    $finca = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "FINCA - id"){
                    $id_finca = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "SUPERVISOR"){
                    $supervisor = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "FECHA"){
                    $fecha = (isset($values[0]["provided"]["time"]) ? "'".str_replace("T", " ", $values[0]['provided']['time'])."'" : '(NULL)');
                }
            }

            if(strpos($pagina_nombre, "MUESTRA ") !== false){
                if($question == "LOTE"){
                    $lote[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "FOCO"){
                    $foco[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "HOJAS TOTALES"){
                    $hojas_totales[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                if($question == "HOJA MÁS VIEJA LIBRE DE ESTRÍAS"){
                    $hojas_mas_vieja_libre[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                if($question == "HOJA MÁS VIEJA LIBRE DE QUEMA < 5%"){
                    $hoja_mas_vieja_libre_quema_menor[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                if($question == "HOJA MÁS VIEJA LIBRE DE QUEMA > 5%"){
                    $hoja_mas_vieja_libre_quema_mayor[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                if($question == "LIBRE DE CIRUGÍAS"){
                    $libre_cirugias[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "OBSERVACIONES"){
                    $observaciones_m[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "NOTA DE VOZ"){
                    $nota_voz[] = "'".(isset($values[0]["filename"]) ? $values[0]["filename"] : '(NULL)')."'";
                }
                if($question == "EVIDENCIA"){
                    foreach ($values as $y => $val) {
                        $evidencia[$x][] = (isset($values[$y]["filename"]) ? $values[$y]["filename"] : '');
                    }
                }
                if($question == "HORA"){
                    $hora[] = "'".(isset($values[0]["provided"]["time"]) ? $values[0]["provided"]["time"] : '(NULL)')."'";
                }
                if($question == "Muestra HT"){
                    $muestra_ht[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Muestra H+VLE"){
                    $muestra_hvle[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Muestra H+VLQ<5%"){
                    $muestra_hvlq_menor[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Muestra H+VLQ>5%"){
                    $muestra_hvlq_mayor[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Muestra LC"){
                    $muestra_lc[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
            }
            if($pagina_nombre == "OBSERVACIONES GENERALES"){
                if($question == "OBSERVACIONES"){
                    $observaciones = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
            }
            if($pagina_nombre == "RESULTADOS"){
                if($question == "Cálculo Hojas Totales"){
                    $r_calculo_hojas_totales = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Total Muestras Hojas Totales"){
                    $r_total_muestras_hojas_totales = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Hojas Totales (HT)"){
                    $r_hojas_totales_ht = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Cálculo H+VLE"){
                    $r_calculo_hvle = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Total Muestras H+VLE"){
                    $r_total_muestras_hvle = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Hoja más vieja libre de estrías (H+VLE)"){
                    $r_hoja_mas_vieja_libre_estrias = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Cálculo H+VLQ<5%"){
                    $r_calculo_hvlq_menor = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Total Muestras H+VLQ<5%"){
                    $r_total_muestras_hvlq_menor = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Hoja más vieja libre de quema <5% (H+VLQ<5%)"){
                    $r_hoja_mas_vieja_libre_de_quema_menor = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Cálculo H+VLQ>5%"){
                    $r_calculo_hvlq_mayor = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Total Muestras H+VLQ>5%"){
                    $r_total_muestras_hvlq_mayor = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Hoja más vieja libre de quema >5% (H+VLQ>5%)"){
                    $r_hoja_mas_vieja_libre_de_quema_mayor = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Cálculo LC"){
                    $r_calculo_lc = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Total Muestras LC"){
                    $r_total_muestras_lc = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Libre de Cirugía"){
                    $r_libre_cirugia = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
            }
        }
    }

    $sql_get_usuario = "SELECT * FROM cat_usuarios WHERE idProntoforms = '$user_username'";
    $id_usuario = $conexion->Consultas(2, $sql_get_usuario);
    $id_usuario = $id_usuario[0]["id"];

    $consulta_principal = "INSERT INTO muestras_haciendas(cliente,id_cliente,id_hacienda,id_usuario,fecha,notas,hacienda,r_CalHT,r_TMHT,r_tot_HT,r_cal_HVLE,r_TMHV,r_Hmasvldeia_HV,r_tot_cal_HVLQ5,r_TMHV5_men,r_HHV_men,r_cal_HVLQ5,r_TMHV5_may,r_HHV_may,r_cal_LC,r_tot_mue_LC,r_TotalLC, tipo_semana) 
    VALUES($cliente,$id_cliente,$id_finca,$id_usuario,$fecha,$observaciones,$finca,$r_calculo_hojas_totales,$r_total_muestras_hojas_totales,$r_hojas_totales_ht,$r_calculo_hvle,$r_total_muestras_hvle,$r_hoja_mas_vieja_libre_estrias,$r_calculo_hvlq_menor,$r_total_muestras_hvlq_menor,$r_hoja_mas_vieja_libre_de_quema_menor,$r_calculo_hvlq_mayor,$r_total_muestras_hvlq_mayor,$r_hoja_mas_vieja_libre_de_quema_mayor,$r_calculo_lc,$r_total_muestras_lc,$r_libre_cirugia, 0);";

    $id_principal = $conexion->Consultas(1, $consulta_principal);

    foreach ($lote as $key => $value) {
        if($hojas_totales[$key] != '' && $hojas_mas_vieja_libre[$key] != '' && $hoja_mas_vieja_libre_quema_menor[$key] != '' && $hoja_mas_vieja_libre_quema_mayor[$key] != '' && $libre_cirugias[$key] != ''){
            $sub_consulta = "INSERT INTO muestras_hacienda_detalle(id_Mhacienda,lote,foco,hojas_totales,hojas_mas_vieja_libre,hoja_mas_vieja_libre_quema_menor,hoja_mas_vieja_libre_quema_mayor,libre_cirugias,observaciones,nota_voz,evidencia,hora,muestra_ht,muestra_hvle,muestra_hvlq_menor,muestra_hvlq_mayor,muestra_lc,id_usuario,id_hacienda,fecha,tipo_semana) 
            VALUES("
                .$id_principal
                .",".$lote[$key]
                .",".$foco[$key]
                .",".$hojas_totales[$key]
                .",".$hojas_mas_vieja_libre[$key]
                .",".$hoja_mas_vieja_libre_quema_menor[$key]
                .",".$hoja_mas_vieja_libre_quema_mayor[$key]
                .",".$libre_cirugias[$key]
                .",".$observaciones_m[$key]
                .",".$nota_voz[$key]
                .",".(isset($evidencia[$key])?"'".json_encode($evidencia[$key])."'":'(NULL)')
                .",".$hora[$key]
                .",".$muestra_ht[$key]
                .",".$muestra_hvle[$key]
                .",".$muestra_hvlq_menor[$key]
                .",".$muestra_hvlq_mayor[$key]
                .",".$muestra_lc[$key]
                .",".$id_usuario
                .",".$id_finca
                .",".$fecha
                .", 0);";

            $conexion->Consultas(1, $sub_consulta);
        }
    }
}

function limpiar($String){
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
     
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);
    return $String;
}

?>