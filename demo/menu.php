<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item start <?php if($page == 'index'){ ?> active open <? } ?>">
                            <a href="index.php" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Inicio</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="heading">
                            <h3 class="uppercase">Menu</h3>
                        </li>
                        <li class="nav-item <?php if($page == 'clima'){ ?> active <? } ?>">
                            <a href="/clima" class="nav-link nav-toggle">
                                <i class="icon-puzzle"></i>
                                <span class="title">Clima</span>
                                <!--span class="arrow"></span-->
                            </a>
						</li>
                        <li class="nav-item <?php if($page == 'planta3m'){ ?> active <? } ?>">
                            <a href="planta3m" class="nav-link nav-toggle">
                                <i class="icon-settings"></i>
                                <span class="title">Plantas 3m</span>
                                <!--span class="arrow"></span-->
                            </a>
						</li>
                        <li class="nav-item <?php if($page == 'planta0sem'){ ?> active <? } ?>">
                            <a href="planta0sem" class="nav-link nav-toggle">
                                <i class="icon-settings"></i>
                                <span class="title">Plantas 0 Sem</span>
                                <!--span class="arrow"></span-->
                            </a>
						</li>
                        <li class="nav-item <?php if($page == 'planta11sem'){ ?> active <? } ?>">
                            <a href="planta11sem" class="nav-link nav-toggle">
                                <i class="icon-settings"></i>
                                <span class="title">Plantas 11 Sem</span>
                                <!--span class="arrow"></span-->
                            </a>
						</li>
                        <li class="nav-item <?php if($page == 'foliar'){ ?> active <? } ?>">
                            <a href="foliar" class="nav-link nav-toggle">
                                <i class="icon-layers"></i>
                                <span class="title">Emision foliar</span>
                                <!--span class="arrow"></span-->
                            </a>
						</li>
                        <li class="nav-item <?php if($page == 'fotos'){ ?> active <? } ?>">
                            <a href="fotos" class="nav-link nav-toggle">
                                <i class="icon-wallet"></i>
                                <span class="title">Fotos</span>
                                <!--span class="arrow"></span-->
                            </a>
						</li>
                        <li class="nav-item <?php if($page == 'mapas'){ ?> active <? } ?>">
                            <a href="mapas" class="nav-link nav-toggle">
                                <i class="icon-pointer"></i>
                                <span class="title">Mapas</span>
                                <!--span class="arrow"></span-->
                            </a>
						</li>
                        <li class="nav-item <?php if($page == 'informe'){ ?> active <? } ?>">
                            <a href="informe" class="nav-link nav-toggle">
                                <i class="icon-briefcase"></i>
                                <span class="title">Enviar informe</span>
                                <!--span class="arrow"></span-->
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-puzzle"></i>
                                <span class="title">Catalogos</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <span class="title">Clientes</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item ">
                                            <a href="clientList" class="nav-link "> Listado </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="newClient" class="nav-link "> Nuevo Cliente </a>
                                        </li>
                                    </ul>
                                </li>
                               <li class="nav-item  ">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <span class="title">Fincas</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item ">
                                            <a href="fincaList" class="nav-link "> Listado </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="newFinca" class="nav-link "> Nueva Finca </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item  ">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <span class="title">Lotes</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item ">
                                            <a href="LotesList" class="nav-link "> Listado </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="newLote" class="nav-link "> Nuevo Lote </a>
                                        </li>
                                    </ul>
                                </li>
                               <li class="nav-item  ">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <span class="title">Agrupaciones</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item ">
                                            <a href="agrapucacionList" class="nav-link "> Listado </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="newAgrupacion" class="nav-link "> Nueva Agrupacion </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->