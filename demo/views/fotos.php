<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="../assets/global/plugins/cubeportfolio/css/cubeportfolio.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Portafolio
                                <small>Fotos</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <div class="page-toolbar">
                            <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green" data-placement="top" data-original-title="Change dashboard date range">
                                <i class="icon-calendar"></i>&nbsp;
                                <span class="thin uppercase hidden-xs"></span>&nbsp;
                                <i class="fa fa-angle-down"></i>
                            </div>
                            <!-- BEGIN THEME PANEL -->
                            <!-- END THEME PANEL -->
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Portafolio</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <!-- BEGIN DASHBOARD STATS 1-->
                        <?php include("clima_tags.php");?>
                    <!-- BEGIN INTERACTIVE CHART PORTLET-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE BASE CONTENT -->
                            <div class="portfolio-content portfolio-3">
                                <div class="clearfix">
                                    <div id="js-filters-lightbox-gallery1" class="cbp-l-filters-dropdown cbp-l-filters-dropdown-floated">
                                        <div class="cbp-l-filters-dropdownWrap border-grey-salsa">
                                            <div class="cbp-l-filters-dropdownHeader uppercase">Ordenar Galeria</div>
                                            <div class="cbp-l-filters-dropdownList">
                                                <div data-filter="*" class="cbp-filter-item-active cbp-filter-item uppercase"> Todos (
                                                    <div class="cbp-filter-counter"></div> fotos) </div>
                                                <div data-filter=".identity" class="cbp-filter-item uppercase"> Labor 1 (
                                                    <div class="cbp-filter-counter"></div> fotos) </div>
                                                <div data-filter=".web-design" class="cbp-filter-item uppercase"> Labor 2 (
                                                    <div class="cbp-filter-counter"></div> fotos) </div>
                                                <div data-filter=".print" class="cbp-filter-item uppercase"> Labor 3 (
                                                    <div class="cbp-filter-counter"></div> fotos) </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="js-filters-lightbox-gallery2" class="cbp-l-filters-button cbp-l-filters-left">
                                        <div data-filter="*" class="cbp-filter-item-active cbp-filter-item btn blue btn-outline uppercase">Todos</div>
                                        <div data-filter=".graphic" class="cbp-filter-item btn blue btn-outline uppercase">Labor 1</div>
                                        <div data-filter=".logos" class="cbp-filter-item btn blue btn-outline uppercase">Labor 2</div>
                                        <div data-filter=".motion" class="cbp-filter-item btn blue btn-outline uppercase">Labor 3</div>
                                    </div>
                                </div>
                                <div id="js-grid-lightbox-gallery" class="cbp">
                                    <div class="cbp-item web-design graphic print motion">
                                        <a href="../assets/global/plugins/cubeportfolio/ajax/project3.html" class="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/01.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">World Clock Widget</div>
                                                        <div class="cbp-l-caption-desc">by Paul Flavius Nechita</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="cbp-item web-design logos identity graphic">
                                        <a href="../assets/global/plugins/cubeportfolio/ajax/project4.html" class="cbp-caption cbp-singlePageInline" data-title="Bolt UI<br>by Tiberiu Neamu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/1.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">Bolt UI</div>
                                                        <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="cbp-item graphic print identity">
                                        <a href="../assets/global/plugins/cubeportfolio/ajax/project3.html" class="cbp-caption cbp-singlePageInline" data-title="WhereTO App<br>by Tiberiu Neamu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/02.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">WhereTO App</div>
                                                        <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="cbp-item web-design motion logos">
                                        <a href="../assets/global/plugins/cubeportfolio/ajax/project4.html" class="cbp-caption cbp-singlePageInline" data-title="iDevices<br>by Tiberiu Neamu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/2.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">iDevices</div>
                                                        <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="cbp-item identity graphic print">
                                        <a href="../assets/global/plugins/cubeportfolio/ajax/project3.html" class="cbp-caption cbp-singlePageInline" data-title="Seemple* Music for iPad<br>by Tiberiu Neamu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/03.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">Seemple* Music for iPad</div>
                                                        <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="cbp-item motion print logos web-design">
                                        <a href="../assets/global/plugins/cubeportfolio/ajax/project4.html" class="cbp-caption cbp-singlePageInline" data-title="Remind~Me Widget<br>by Tiberiu Neamu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/3.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">Remind~Me Widget</div>
                                                        <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="cbp-item graphic logos">
                                        <a href="../assets/global/plugins/cubeportfolio/ajax/project3.html" class="cbp-caption cbp-singlePageInline" data-title="Workout Buddy<br>by Tiberiu Neamu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/04.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">Workout Buddy</div>
                                                        <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="cbp-item identity print logos motion">
                                        <a href="../assets/global/plugins/cubeportfolio/ajax/project4.html" class="cbp-caption cbp-singlePageInline" data-title="Digital Menu<br>by Cosmin Capitanu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/4.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">Digital Menu</div>
                                                        <div class="cbp-l-caption-desc">by Cosmin Capitanu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="cbp-item identity motion web-design">
                                        <a href="../assets/global/plugins/cubeportfolio/ajax/project3.html" class="cbp-caption cbp-singlePageInline" data-title="Holiday Selector<br>by Cosmin Capitanu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="../assets/global/img/portfolio/600x600/05.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">Holiday Selector</div>
                                                        <div class="cbp-l-caption-desc">by Cosmin Capitanu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div id="js-loadMore-lightbox-gallery" class="cbp-l-loadMore-button">
                                    <a href="../assets/global/plugins/cubeportfolio/ajax/loadMore3.html" class="cbp-l-loadMore-link btn grey-mint btn-outline" rel="nofollow">
                                        <span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
                                        <span class="cbp-l-loadMore-loadingText">LOADING...</span>
                                        <span class="cbp-l-loadMore-noMoreLoading">NO MORE WORKS</span>
                                    </a>
                                </div>
                            </div>
                            <!-- END PAGE BASE CONTENT -->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->