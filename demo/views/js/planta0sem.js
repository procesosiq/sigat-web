
            // chart2("temp_min");
            // chart2("precp");
            // chart2("temp_max");
            // chart2("radiacion");
            // chart2("humedad");



app.controller('plantas0Sem', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){
    
    $scope.init = function(){
        $scope.getData();
        $interval($scope.getData, 30000);
    }

    $scope.random = [];
    
    // $scope.getRandomData = function() {
    //     if ($scope.random.length > 0) $scope.random = $scope.random.slice(1);
    //     // do a random walk
    //     while ($scope.random.length < totalPoints) {
    //         var prev = $scope.random.length > 0 ? $scope.random[$scope.random.length - 1] : 50;
    //         var y = prev + Math.random() * 10 - 5;
    //         if (y < 0) y = 0;
    //         if (y > 100) y = 100;
    //         $scope.random.push(y);
    //     }
    //     // zip the generated y values with the x values
    //     var res = [];
    //     for (var i = 0; i < $scope.random.length; ++i) {
    //         res.push([i, $scope.random[i]]);
    //     }

    //     return res;
    // }

    $scope.getData = function(){
        var data = {
            opt : 'HMVLDQMEN5'
        }
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycMenor5 , data);
        
        data = {};
        data = {
            opt : "HMVLDE"
        }
        
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycLibreStrias , data);

        data = {};
        data = {
            opt : "LIB_DE_CIRUG"
        }
        
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycLibreCirug , data);


        data = {};
        data = {
            opt : "HMVLDQMAY5"
        }
        
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycMayor5 , data);

        data = {};
        data = {
            opt : "HOJTOT"
        }
        
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycHojasTotales , data);

    }

    $scope.printGraphycMenor5 = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hoja_vieja_menor_5");
            console.log("hoja_vieja_menor_5")
        }
    }

    $scope.printGraphycLibreStrias = function(r , b){
        b();
        if(r){
            $scope.printData(r , "libre_estrias");
            console.log("libre_estrias")
        }
    }

    $scope.printGraphycLibreCirug = function(r , b){
        b();
        if(r){
            $scope.printData(r , "libre_cirugia");
            console.log("libre_cirugia")
        }
    }

    $scope.printGraphycMayor5 = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hoja_vieja_mayor_5");
            console.log("hoja_vieja_mayor_5")
        }
    }

    $scope.printGraphycHojasTotales = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hojas_total");
            console.log("hojas_total")
        }
    }

    $scope.printData = function(r , id){
        if(r){  
            var data = [];
            var id = id;
            for(var info in r){
                data.push({
                    data : r[info],
                    label: info,
                    lines: {
                        lineWidth: 1,
                    },
                    shadowSize: 0
                })
            }
            console.log(id);
            var plot = $.plot($('#'+id), data, {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.05
                                }, {
                                    opacity: 0.01
                                }]
                            }
                        },
                        points: {
                            show: true,
                            radius: 3,
                            lineWidth: 1
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderColor: "#eee",
                        borderWidth: 1
                    },
                    colors: ["#d12610", "#37b7f3", "#52e136"],
                    xaxis: {
                        ticks: 11,
                        tickDecimals: 0,
                        tickColor: "#eee",
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 0,
                        tickColor: "#eee",
                    }
                });


                function showTooltip(x, y, contents) {
                    $('<div id="tooltip">' + contents + '</div>').css({
                        position: 'absolute',
                        display: 'none',
                        top: y + 5,
                        left: x + 15,
                        border: '1px solid #333',
                        padding: '4px',
                        color: '#fff',
                        'border-radius': '3px',
                        'background-color': '#333',
                        opacity: 0.80
                    }).appendTo("body").fadeIn(200);
                }

                var previousPoint = null;
                $('#'+id).bind("plothover", function(event, pos, item) {
                    $("#x").text(pos.x.toFixed(0));
                    $("#y").text(pos.y.toFixed(2));

                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            showTooltip(item.pageX, item.pageY,"Año " + item.series.label + " Semana " + x + " Valor " + y);
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                
                });
        }   
    }
}]);
