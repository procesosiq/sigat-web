<?php
    include "../controllers/class.sesion.php";
    $session = Session::getInstance();
    $id_usuario = $_POST["usuario"];
    $root_path =  realpath('./climas');
    $target_dir = $root_path."/user_".$id_usuario."/";

if( is_dir($target_dir) === false ){
    mkdir($target_dir, 0777, true);
}
    $target_file = $target_dir.basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    $ruta = realpath('./climas/user_'.$id_usuario.'/');

    echo $target_file;

    $files = glob("{$ruta}/*"); // get all file names
    foreach($files as $file){ // iterate files
        if(is_file($file))
            unlink($file); // delete file
    }
    
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "<div style='display:none;'>";
        include 'reader_xlsx_climas.php';
        echo "</div>";
        echo "<a href='http://sigat.procesos-iq.com/importarInformacion'>Volver</a>";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
?>
