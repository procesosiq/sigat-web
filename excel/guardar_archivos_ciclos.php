<?php
    include "../controllers/class.sesion.php";
    $session = Session::getInstance();
    $target_dir = "ciclos/user_".$_POST["usuario"]."/";
    $target_file = $target_dir.basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "<div style='display:block;'>";
        include 'reader_xlsx_ciclos.php';
        echo "</div>";
        echo "<a href='http://sigat.procesos-iq.com/importarInformacionCiclos'>Volver</a>";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
?>