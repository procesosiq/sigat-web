import MySQLdb
import csv     # imports the csv module
import sys      # imports the sys module

db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user="auditoriasbonita",         # your username
                     passwd="u[V(fTIUbcVb",  # your password
                     db="sigat")        # name of the data base
f = open(sys.argv[1], 'rb') # opens the csv file
start = sys.argv[2]
count = 0
fila = []
date = ""
fecha = ""
exe = db.cursor()
try:
	reader = csv.reader(f) # creates the reader object
	for row in reader:  # iterates the rows of the file in orders
		count+=1
		if count > 1:
			fila=','.join(row).split(";")
			if fila[0] != "":
				date = fila[0].split("/")
				fecha = date[2] + "-" + date[1] + "-" + date[0]
				exe.execute("""INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,humedad,rad_solar,dias_sol,fecha,id_hacienda , id_usuario , id_cliente) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""" , (fila[4], fila[3], fila[17], fila[28], fila[19],fila[20],fecha,14 , 1 , 9))
				db.commit()
finally:
	f.close()      # closing
db.close()
