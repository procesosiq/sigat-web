// var parseXlsx = require('excel');
var csv = require('csv-to-json');
var mysql = require('mysql');
var fs = require('fs');
var path = process.argv[2]; /// PARAMETRO DESDE CONSOLA

var classBD = {
	mysql : {},
	fs : {},
	init : function(config){
        if(config && (Array.isArray(config) || config instanceof Object)){
            Object.assign(this.configAgency , config);
        }
        // console.log(this.configAgency);s
        this.fs = fs;
        this.mysql = mysql;
        this.config = JSON.parse(this.fs.readFileSync('config.json', 'utf8'));
        this.pool = this.mysql.createPool(this.config);
        console.log(this.pool);
    },
	multiInsert : function(sql , callback){
        this.pool.getConnection(function(err,connection){
            if (err) console.log(err);
            else
            connection.query(sql, function(err,result) {
                if (err) throw err;
                // console.log('The solution is: ', result.insertId);
                connection.release();
                if(callback) callback(result , connection);
            });
        });
    },
    insert : function(sql , callback){
        this.pool.getConnection(function(err,connection){
            if (err) console.log(err);
            else
            connection.query(sql, function(err,result) {
                if (err) throw err+sql;
                // console.log('The solution is: ', result.insertId);
                connection.release();
                if(callback) callback(result.insertId , connection);
            });
        });
    },
    select : function(sql , callback){
        this.pool.getConnection(function(err,connection){
            if (err) console.log(err);
            else
            connection.query(sql, function(err,result) {
                // console.log(result);
                if (err) throw err;
                // console.log('The solution is: ', result.insertId);
                connection.release();
                if(callback) callback(result , connection);
            });
        });
    },
    update : function(sql , callback){
        this.pool.getConnection(function(err,connection){
            if (err) console.log(err);
            else
            connection.query(sql, function(err,result) {
                if (err) throw err;
                // console.log('The solution is: ', result.insertId);
                connection.release();
                if(callback) callback(result.changedRows , connection);
            });
        });
    },
    delete : function(sql , callback){
        if (err) console.log(err);
        else
        this.pool.getConnection(function(err,connection){
            if (err) throw err;
            connection.query(sql, function(err,result) {
                if (err) throw err;
                // console.log('The solution is: ', result.insertId);
                connection.release();
                if(callback) callback(result.affectedRows , connection);
            });
        });
    }
}

// parseXlsx(path, function(err, data) {
//   if(err) throw err;
//   	console.log(data);
//   	for (var i = 0; i <= data.length; i++) {
//   		if(i > 4){
//   			Reader(data[i]);
//   		}
//   	}

// });


// function Reader(data){
// 	console.log(data);
// }

var obj = {
    filename: path
};

csv.parse(obj, callback);

var callback = function(err, json) {
    console.log(json);
};

// classBD.init();
// classBD.select("SELECT * FROM cat_usuarios" , function(data , connection){
// 	console.log(data);
// 	connection.release();
// })
/**

	TODO:
	- node [ Abre consola de NODE.JS ]
	- node filename.js [ EJECUTAR UN ARCHIVO DE NODE.JS ]
	- node filename.js  parametro n [ EJECUTA UN ARCHIVO DE NODE.JS MANDADO {parametro} COMO PARAMETRO ]

 */
