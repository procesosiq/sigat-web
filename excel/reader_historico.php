<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

include '../controllers/conexion.php';
include 'simplexlsx.class.php';

$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName()){
        $pos1 = strpos($object->getPathName(), "CLIMA HISTORICO.xlsx");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('xlsx' == $ext){
                switch ($ext){

                    case 'xlsx':
                        $excel = trim(file_get_contents($object->getPathName()));
                        procesar_xlsx($excel, $object->getFileName());
                        break;

                    default:
                        break;
                }
            }
        }
    }
}


function eliminar_archivo($ruta){
    unlink($ruta);
}

function procesar_xlsx($excel, $file_name){
    $xlsx = new SimpleXLSX($file_name);
    $conexion = new M_Conexion;

    foreach ($xlsx->rows() as $key => $fila) {
        if($key > 1){
            #echo json_encode($fila)."<br>";
            foreach($fila  as $key2 => $celda){
                if($key2 == 0){
                    $registros[$key]["semana"] = $celda;
                }
                if($key2 == 1){
                    $registros[$key]["97"]["tem_min"] = $celda;
                }
                if($key2 == 2){
                    $registros[$key]["2009"]["tem_min"] = $celda;
                }
                if($key2 == 3){
                    $registros[$key]["2010"]["tem_min"] = $celda;
                }
                if($key2 == 4){
                    $registros[$key]["2011"]["tem_min"] = $celda;
                }
                if($key2 == 5){
                    $registros[$key]["2012"]["tem_min"] = $celda;
                }
                if($key2 == 6){
                    $registros[$key]["2013"]["tem_min"] = $celda;
                }
                if($key2 == 7){
                    $registros[$key]["2014"]["tem_min"] = $celda;
                }
                if($key2 == 8){
                    $registros[$key]["2015"]["tem_min"] = $celda;
                }
                #espacio 9
                #espacio sem 10
                if($key2 == 11){
                    $registros[$key]["97"]["tem_max"] = $celda;
                }
                if($key2 == 12){
                    $registros[$key]["2009"]["tem_max"] = $celda;
                }
                if($key2 == 13){
                    $registros[$key]["2010"]["tem_max"] = $celda;
                }
                if($key2 == 14){
                    $registros[$key]["2011"]["tem_max"] = $celda;
                }
                if($key2 == 15){
                    $registros[$key]["2012"]["tem_max"] = $celda;
                }
                if($key2 == 16){
                    $registros[$key]["2013"]["tem_max"] = $celda;
                }
                if($key2 == 17){
                    $registros[$key]["2014"]["tem_max"] = $celda;
                }
                if($key2 == 18){
                    $registros[$key]["2015"]["tem_max"] = $celda;
                }
                #espacio 19
                #espacio 20 sem
                if($key2 == 21){
                    $registros[$key]["97"]["lluvia"] = $celda;
                }
                if($key2 == 22){
                    $registros[$key]["2009"]["lluvia"] = $celda;
                }
                if($key2 == 23){
                    $registros[$key]["2010"]["lluvia"] = $celda;
                }
                if($key2 == 24){
                    $registros[$key]["2011"]["lluvia"] = $celda;
                }
                if($key2 == 25){
                    $registros[$key]["2012"]["lluvia"] = $celda;
                }
                if($key2 == 26){
                    $registros[$key]["2013"]["lluvia"] = $celda;
                }
                if($key2 == 27){
                    $registros[$key]["2014"]["lluvia"] = $celda;
                }
                if($key2 == 28){
                    $registros[$key]["2015"]["lluvia"] = $celda;
                }
            }
        }
    }

    foreach ($registros as $key => $value) {
        if(isset($value["semana"]) && ($value["97"]["tem_max"] != "" || $value["97"]["tem_min"] != "" || $value["97"]["lluvia"] != "")){
            $sql_insert_1 = "
                INSERT INTO datos_clima
                (
                    temp_maxima,
                    temp_minima,
                    lluvia,
                    fecha,
                    id_hacienda,
                    id_usuario,
                    id_cliente
                )
                VALUES(
                    ".(($value["97"]["tem_max"]!=""&&$value["97"]["tem_max"]!="---")?$value["97"]["tem_max"]:"'(NULL)'").",
                    ".(($value["97"]["tem_min"]!=""&&$value["97"]["tem_min"]!="---")?$value["97"]["tem_min"]:"'(NULL)'").",
                    ".(($value["97"]["lluvia"]!=""&&$value["97"]["lluvia"]!="---")?$value["97"]["lluvia"]:"'(NULL)'").",
                    STR_TO_DATE('1997".$value["semana"]." Monday', '%X%V %W'),
                    18,
                    1,
                    10
                );
            ";
            $sql_insert_2 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["97"]["tem_max"]!=""&&$value["97"]["tem_max"]!="---")?$value["97"]["tem_max"]:"'(NULL)'").",".(($value["97"]["tem_min"]!=""&&$value["97"]["tem_min"]!="---")?$value["97"]["tem_min"]:"'(NULL)'").",".(($value["97"]["lluvia"]!=""&&$value["97"]["lluvia"]!="---")?$value["97"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('1997".$value["semana"]." Monday', '%X%V %W'),17,1,11);";
            #$sql_insert_3 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["97"]["tem_max"]!=""&&$value["97"]["tem_max"]!="---")?$value["97"]["tem_max"]:"'(NULL)'").",".(($value["97"]["tem_min"]!=""&&$value["97"]["tem_min"]!="---")?$value["97"]["tem_min"]:"'(NULL)'").",".(($value["97"]["lluvia"]!=""&&$value["97"]["lluvia"]!="---")?$value["97"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('1997".$value["semana"]." Monday', '%X%V %W'),16,1,10);";
            #$sql_insert_4 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["97"]["tem_max"]!=""&&$value["97"]["tem_max"]!="---")?$value["97"]["tem_max"]:"'(NULL)'").",".(($value["97"]["tem_min"]!=""&&$value["97"]["tem_min"]!="---")?$value["97"]["tem_min"]:"'(NULL)'").",".(($value["97"]["lluvia"]!=""&&$value["97"]["lluvia"]!="---")?$value["97"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('1997".$value["semana"]." Monday', '%X%V %W'),20,1,10);";
            #$sql_insert_5 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["97"]["tem_max"]!=""&&$value["97"]["tem_max"]!="---")?$value["97"]["tem_max"]:"'(NULL)'").",".(($value["97"]["tem_min"]!=""&&$value["97"]["tem_min"]!="---")?$value["97"]["tem_min"]:"'(NULL)'").",".(($value["97"]["lluvia"]!=""&&$value["97"]["lluvia"]!="---")?$value["97"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('1997".$value["semana"]." Monday', '%X%V %W'),15,1,10);";
            #$sql_insert_6 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["97"]["tem_max"]!=""&&$value["97"]["tem_max"]!="---")?$value["97"]["tem_max"]:"'(NULL)'").",".(($value["97"]["tem_min"]!=""&&$value["97"]["tem_min"]!="---")?$value["97"]["tem_min"]:"'(NULL)'").",".(($value["97"]["lluvia"]!=""&&$value["97"]["lluvia"]!="---")?$value["97"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('1997".$value["semana"]." Monday', '%X%V %W'),14,1,9);";

            #$conexion->Consultas(1, $sql_insert_1);
            $conexion->Consultas(1, $sql_insert_2);
            #$conexion->Consultas(1, $sql_insert_3);
            #$conexion->Consultas(1, $sql_insert_4);
            #$conexion->Consultas(1, $sql_insert_5);
            #$conexion->Consultas(1, $sql_insert_6);
            #echo $sql_insert_1."<br>";
            echo $sql_insert_2."<br>";
            /*echo $sql_insert_3."<br>";
            echo $sql_insert_4."<br>";
            echo $sql_insert_5."<br>";
            echo $sql_insert_6."<br>";*/
        }
        if(isset($value["semana"]) && ($value["2009"]["tem_max"] != "" || $value["2009"]["tem_min"] != "" || $value["2009"]["lluvia"] != "")){
            $sql_insert_1 = "
                INSERT INTO datos_clima
                (
                    temp_maxima,
                    temp_minima,
                    lluvia,
                    fecha,
                    id_hacienda,
                    id_usuario,
                    id_cliente
                )
                VALUES(
                    ".(($value["2009"]["tem_max"]!=""&&$value["2009"]["tem_max"]!="---")?$value["2009"]["tem_max"]:"'(NULL)'").",
                    ".(($value["2009"]["tem_min"]!=""&&$value["2009"]["tem_min"]!="---")?$value["2009"]["tem_min"]:"'(NULL)'").",
                    ".(($value["2009"]["lluvia"]!=""&&$value["2009"]["lluvia"]!="---")?$value["2009"]["lluvia"]:"'(NULL)'").",
                    STR_TO_DATE('2009".$value["semana"]." Monday', '%X%V %W'),
                    18,
                    1,
                    10
                );
            ";
            #$sql_insert_2 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2009"]["tem_max"]!=""&&$value["2009"]["tem_max"]!="---")?$value["2009"]["tem_max"]:"'(NULL)'").",".(($value["2009"]["tem_min"]!=""&&$value["2009"]["tem_min"]!="---")?$value["2009"]["tem_min"]:"'(NULL)'").",".(($value["2009"]["lluvia"]!=""&&$value["2009"]["lluvia"]!="---")?$value["2009"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2009".$value["semana"]." Monday', '%X%V %W'),17,1,11);";
            #$sql_insert_3 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2009"]["tem_max"]!=""&&$value["2009"]["tem_max"]!="---")?$value["2009"]["tem_max"]:"'(NULL)'").",".(($value["2009"]["tem_min"]!=""&&$value["2009"]["tem_min"]!="---")?$value["2009"]["tem_min"]:"'(NULL)'").",".(($value["2009"]["lluvia"]!=""&&$value["2009"]["lluvia"]!="---")?$value["2009"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2009".$value["semana"]." Monday', '%X%V %W'),16,1,10);";
            #$sql_insert_4 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2009"]["tem_max"]!=""&&$value["2009"]["tem_max"]!="---")?$value["2009"]["tem_max"]:"'(NULL)'").",".(($value["2009"]["tem_min"]!=""&&$value["2009"]["tem_min"]!="---")?$value["2009"]["tem_min"]:"'(NULL)'").",".(($value["2009"]["lluvia"]!=""&&$value["2009"]["lluvia"]!="---")?$value["2009"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2009".$value["semana"]." Monday', '%X%V %W'),20,1,10);";
            #$sql_insert_5 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2009"]["tem_max"]!=""&&$value["2009"]["tem_max"]!="---")?$value["2009"]["tem_max"]:"'(NULL)'").",".(($value["2009"]["tem_min"]!=""&&$value["2009"]["tem_min"]!="---")?$value["2009"]["tem_min"]:"'(NULL)'").",".(($value["2009"]["lluvia"]!=""&&$value["2009"]["lluvia"]!="---")?$value["2009"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2009".$value["semana"]." Monday', '%X%V %W'),15,1,10);";
            #$sql_insert_6 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2009"]["tem_max"]!=""&&$value["2009"]["tem_max"]!="---")?$value["2009"]["tem_max"]:"'(NULL)'").",".(($value["2009"]["tem_min"]!=""&&$value["2009"]["tem_min"]!="---")?$value["2009"]["tem_min"]:"'(NULL)'").",".(($value["2009"]["lluvia"]!=""&&$value["2009"]["lluvia"]!="---")?$value["2009"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2009".$value["semana"]." Monday', '%X%V %W'),14,1,9);";

            #$conexion->Consultas(1, $sql_insert_1);
            #$conexion->Consultas(1, $sql_insert_2);
            #$conexion->Consultas(1, $sql_insert_3);
            #$conexion->Consultas(1, $sql_insert_4);
            #$conexion->Consultas(1, $sql_insert_5);
            #$conexion->Consultas(1, $sql_insert_6);
            /*echo $sql_insert_1."<br>";
            echo $sql_insert_2."<br>";
            echo $sql_insert_3."<br>";
            echo $sql_insert_4."<br>";
            echo $sql_insert_5."<br>";
            echo $sql_insert_6."<br>";*/
        }
        if(isset($value["semana"]) && ($value["2010"]["tem_max"] != "" || $value["2010"]["tem_min"] != "" || $value["2010"]["lluvia"] != "")){
            $sql_insert_1 = "
                INSERT INTO datos_clima
                (
                    temp_maxima,
                    temp_minima,
                    lluvia,
                    fecha,
                    id_hacienda,
                    id_usuario,
                    id_cliente
                )
                VALUES(
                    ".(($value["2010"]["tem_max"]!=""&&$value["2010"]["tem_max"]!="---")?$value["2010"]["tem_max"]:"'(NULL)'").",
                    ".(($value["2010"]["tem_min"]!=""&&$value["2010"]["tem_min"]!="---")?$value["2010"]["tem_min"]:"'(NULL)'").",
                    ".(($value["2010"]["lluvia"]!=""&&$value["2010"]["lluvia"]!="---")?$value["2010"]["lluvia"]:"'(NULL)'").",
                    STR_TO_DATE('2010".$value["semana"]." Monday', '%X%V %W'),
                    18,
                    1,
                    10
                );
            ";
            #$sql_insert_2 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2010"]["tem_max"]!=""&&$value["2010"]["tem_max"]!="---")?$value["2010"]["tem_max"]:"'(NULL)'").",".(($value["2010"]["tem_min"]!=""&&$value["2010"]["tem_min"]!="---")?$value["2010"]["tem_min"]:"'(NULL)'").",".(($value["2010"]["lluvia"]!=""&&$value["2010"]["lluvia"]!="---")?$value["2010"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2010".$value["semana"]." Monday', '%X%V %W'),17,1,11);";
            #$sql_insert_3 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2010"]["tem_max"]!=""&&$value["2010"]["tem_max"]!="---")?$value["2010"]["tem_max"]:"'(NULL)'").",".(($value["2010"]["tem_min"]!=""&&$value["2010"]["tem_min"]!="---")?$value["2010"]["tem_min"]:"'(NULL)'").",".(($value["2010"]["lluvia"]!=""&&$value["2010"]["lluvia"]!="---")?$value["2010"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2010".$value["semana"]." Monday', '%X%V %W'),16,1,10);";
            #$sql_insert_4 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2010"]["tem_max"]!=""&&$value["2010"]["tem_max"]!="---")?$value["2010"]["tem_max"]:"'(NULL)'").",".(($value["2010"]["tem_min"]!=""&&$value["2010"]["tem_min"]!="---")?$value["2010"]["tem_min"]:"'(NULL)'").",".(($value["2010"]["lluvia"]!=""&&$value["2010"]["lluvia"]!="---")?$value["2010"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2010".$value["semana"]." Monday', '%X%V %W'),20,1,10);";
            #$sql_insert_5 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2010"]["tem_max"]!=""&&$value["2010"]["tem_max"]!="---")?$value["2010"]["tem_max"]:"'(NULL)'").",".(($value["2010"]["tem_min"]!=""&&$value["2010"]["tem_min"]!="---")?$value["2010"]["tem_min"]:"'(NULL)'").",".(($value["2010"]["lluvia"]!=""&&$value["2010"]["lluvia"]!="---")?$value["2010"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2010".$value["semana"]." Monday', '%X%V %W'),15,1,10);";
            #$sql_insert_6 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2010"]["tem_max"]!=""&&$value["2010"]["tem_max"]!="---")?$value["2010"]["tem_max"]:"'(NULL)'").",".(($value["2010"]["tem_min"]!=""&&$value["2010"]["tem_min"]!="---")?$value["2010"]["tem_min"]:"'(NULL)'").",".(($value["2010"]["lluvia"]!=""&&$value["2010"]["lluvia"]!="---")?$value["2010"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2010".$value["semana"]." Monday', '%X%V %W'),14,1,9);";

            #$conexion->Consultas(1, $sql_insert_1);
            #$conexion->Consultas(1, $sql_insert_2);
            #$conexion->Consultas(1, $sql_insert_3);
            #$conexion->Consultas(1, $sql_insert_4);
            #$conexion->Consultas(1, $sql_insert_5);
            #$conexion->Consultas(1, $sql_insert_6);
            /*echo $sql_insert_1."<br>";
            echo $sql_insert_2."<br>";
            echo $sql_insert_3."<br>";
            echo $sql_insert_4."<br>";
            echo $sql_insert_5."<br>";
            echo $sql_insert_6."<br>";*/
        }
        if(isset($value["semana"]) && ($value["2011"]["tem_max"] != "" || $value["2011"]["tem_min"] != "" || $value["2011"]["lluvia"] != "")){
            $sql_insert_1 = "
                INSERT INTO datos_clima
                (
                    temp_maxima,
                    temp_minima,
                    lluvia,
                    fecha,
                    id_hacienda,
                    id_usuario,
                    id_cliente
                )
                VALUES(
                    ".(($value["2011"]["tem_max"]!=""&&$value["2011"]["tem_max"]!="---")?$value["2011"]["tem_max"]:"'(NULL)'").",
                    ".(($value["2011"]["tem_min"]!=""&&$value["2011"]["tem_min"]!="---")?$value["2011"]["tem_min"]:"'(NULL)'").",
                    ".(($value["2011"]["lluvia"]!=""&&$value["2011"]["lluvia"]!="---")?$value["2011"]["lluvia"]:"'(NULL)'").",
                    STR_TO_DATE('2011".$value["semana"]." Monday', '%X%V %W'),
                    18,
                    1,
                    10
                );
            ";
            #$sql_insert_2 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2011"]["tem_max"]!=""&&$value["2011"]["tem_max"]!="---")?$value["2011"]["tem_max"]:"'(NULL)'").",".(($value["2011"]["tem_min"]!=""&&$value["2011"]["tem_min"]!="---")?$value["2011"]["tem_min"]:"'(NULL)'").",".(($value["2011"]["lluvia"]!=""&&$value["2011"]["lluvia"]!="---")?$value["2011"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2011".$value["semana"]." Monday', '%X%V %W'),17,1,11);";
            #$sql_insert_3 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2011"]["tem_max"]!=""&&$value["2011"]["tem_max"]!="---")?$value["2011"]["tem_max"]:"'(NULL)'").",".(($value["2011"]["tem_min"]!=""&&$value["2011"]["tem_min"]!="---")?$value["2011"]["tem_min"]:"'(NULL)'").",".(($value["2011"]["lluvia"]!=""&&$value["2011"]["lluvia"]!="---")?$value["2011"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2011".$value["semana"]." Monday', '%X%V %W'),16,1,10);";
            #$sql_insert_4 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2011"]["tem_max"]!=""&&$value["2011"]["tem_max"]!="---")?$value["2011"]["tem_max"]:"'(NULL)'").",".(($value["2011"]["tem_min"]!=""&&$value["2011"]["tem_min"]!="---")?$value["2011"]["tem_min"]:"'(NULL)'").",".(($value["2011"]["lluvia"]!=""&&$value["2011"]["lluvia"]!="---")?$value["2011"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2011".$value["semana"]." Monday', '%X%V %W'),20,1,10);";
            #$sql_insert_5 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2011"]["tem_max"]!=""&&$value["2011"]["tem_max"]!="---")?$value["2011"]["tem_max"]:"'(NULL)'").",".(($value["2011"]["tem_min"]!=""&&$value["2011"]["tem_min"]!="---")?$value["2011"]["tem_min"]:"'(NULL)'").",".(($value["2011"]["lluvia"]!=""&&$value["2011"]["lluvia"]!="---")?$value["2011"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2011".$value["semana"]." Monday', '%X%V %W'),15,1,10);";
            #$sql_insert_6 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2011"]["tem_max"]!=""&&$value["2011"]["tem_max"]!="---")?$value["2011"]["tem_max"]:"'(NULL)'").",".(($value["2011"]["tem_min"]!=""&&$value["2011"]["tem_min"]!="---")?$value["2011"]["tem_min"]:"'(NULL)'").",".(($value["2011"]["lluvia"]!=""&&$value["2011"]["lluvia"]!="---")?$value["2011"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2011".$value["semana"]." Monday', '%X%V %W'),14,1,9);";

            #$conexion->Consultas(1, $sql_insert_1);
            #$conexion->Consultas(1, $sql_insert_2);
            #$conexion->Consultas(1, $sql_insert_3);
            #$conexion->Consultas(1, $sql_insert_4);
            #$conexion->Consultas(1, $sql_insert_5);
            #$conexion->Consultas(1, $sql_insert_6);
            /*echo $sql_insert_1."<br>";
            echo $sql_insert_2."<br>";
            echo $sql_insert_3."<br>";
            echo $sql_insert_4."<br>";
            echo $sql_insert_5."<br>";
            echo $sql_insert_6."<br>";*/
        }
        if(isset($value["semana"]) && ($value["2012"]["tem_max"] != "" || $value["2012"]["tem_min"] != "" || $value["2012"]["lluvia"] != "")){
            $sql_insert_1 = "
                INSERT INTO datos_clima
                (
                    temp_maxima,
                    temp_minima,
                    lluvia,
                    fecha,
                    id_hacienda,
                    id_usuario,
                    id_cliente
                )
                VALUES(
                    ".(($value["2012"]["tem_max"]!=""&&$value["2012"]["tem_max"]!="---")?$value["2012"]["tem_max"]:"'(NULL)'").",
                    ".(($value["2012"]["tem_min"]!=""&&$value["2012"]["tem_min"]!="---")?$value["2012"]["tem_min"]:"'(NULL)'").",
                    ".(($value["2012"]["lluvia"]!=""&&$value["2012"]["lluvia"]!="---")?$value["2012"]["lluvia"]:"'(NULL)'").",
                    STR_TO_DATE('2012".$value["semana"]." Monday', '%X%V %W'),
                    18,
                    1,
                    10
                );
            ";
            #$sql_insert_2 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2012"]["tem_max"]!=""&&$value["2012"]["tem_max"]!="---")?$value["2012"]["tem_max"]:"'(NULL)'").",".(($value["2012"]["tem_min"]!=""&&$value["2012"]["tem_min"]!="---")?$value["2012"]["tem_min"]:"'(NULL)'").",".(($value["2012"]["lluvia"]!=""&&$value["2012"]["lluvia"]!="---")?$value["2012"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2012".$value["semana"]." Monday', '%X%V %W'),17,1,11);";
            #$sql_insert_3 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2012"]["tem_max"]!=""&&$value["2012"]["tem_max"]!="---")?$value["2012"]["tem_max"]:"'(NULL)'").",".(($value["2012"]["tem_min"]!=""&&$value["2012"]["tem_min"]!="---")?$value["2012"]["tem_min"]:"'(NULL)'").",".(($value["2012"]["lluvia"]!=""&&$value["2012"]["lluvia"]!="---")?$value["2012"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2012".$value["semana"]." Monday', '%X%V %W'),16,1,10);";
            #$sql_insert_4 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2012"]["tem_max"]!=""&&$value["2012"]["tem_max"]!="---")?$value["2012"]["tem_max"]:"'(NULL)'").",".(($value["2012"]["tem_min"]!=""&&$value["2012"]["tem_min"]!="---")?$value["2012"]["tem_min"]:"'(NULL)'").",".(($value["2012"]["lluvia"]!=""&&$value["2012"]["lluvia"]!="---")?$value["2012"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2012".$value["semana"]." Monday', '%X%V %W'),20,1,10);";
            #$sql_insert_5 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2012"]["tem_max"]!=""&&$value["2012"]["tem_max"]!="---")?$value["2012"]["tem_max"]:"'(NULL)'").",".(($value["2012"]["tem_min"]!=""&&$value["2012"]["tem_min"]!="---")?$value["2012"]["tem_min"]:"'(NULL)'").",".(($value["2012"]["lluvia"]!=""&&$value["2012"]["lluvia"]!="---")?$value["2012"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2012".$value["semana"]." Monday', '%X%V %W'),15,1,10);";
            #$sql_insert_6 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2012"]["tem_max"]!=""&&$value["2012"]["tem_max"]!="---")?$value["2012"]["tem_max"]:"'(NULL)'").",".(($value["2012"]["tem_min"]!=""&&$value["2012"]["tem_min"]!="---")?$value["2012"]["tem_min"]:"'(NULL)'").",".(($value["2012"]["lluvia"]!=""&&$value["2012"]["lluvia"]!="---")?$value["2012"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2012".$value["semana"]." Monday', '%X%V %W'),14,1,9);";

            #$conexion->Consultas(1, $sql_insert_1);
            #$conexion->Consultas(1, $sql_insert_2);
            #$conexion->Consultas(1, $sql_insert_3);
            #$conexion->Consultas(1, $sql_insert_4);
            #$conexion->Consultas(1, $sql_insert_5);
            #$conexion->Consultas(1, $sql_insert_6);
            /*echo $sql_insert_1."<br>";
            echo $sql_insert_2."<br>";
            echo $sql_insert_3."<br>";
            echo $sql_insert_4."<br>";
            echo $sql_insert_5."<br>";
            echo $sql_insert_6."<br>";*/
        }
        if(isset($value["semana"]) && ($value["2013"]["tem_max"] != "" || $value["2013"]["tem_min"] != "" || $value["2013"]["lluvia"] != "")){
            $sql_insert_1 = "
                INSERT INTO datos_clima
                (
                    temp_maxima,
                    temp_minima,
                    lluvia,
                    fecha,
                    id_hacienda,
                    id_usuario,
                    id_cliente
                )
                VALUES(
                    ".(($value["2013"]["tem_max"]!=""&&$value["2013"]["tem_max"]!="---")?$value["2013"]["tem_max"]:"'(NULL)'").",
                    ".(($value["2013"]["tem_min"]!=""&&$value["2013"]["tem_min"]!="---")?$value["2013"]["tem_min"]:"'(NULL)'").",
                    ".(($value["2013"]["lluvia"]!=""&&$value["2013"]["lluvia"]!="---")?$value["2013"]["lluvia"]:"'(NULL)'").",
                    STR_TO_DATE('2013".$value["semana"]." Monday', '%X%V %W'),
                    18,
                    1,
                    10
                );
            ";
            #$sql_insert_2 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2013"]["tem_max"]!=""&&$value["2013"]["tem_max"]!="---")?$value["2013"]["tem_max"]:"'(NULL)'").",".(($value["2013"]["tem_min"]!=""&&$value["2013"]["tem_min"]!="---")?$value["2013"]["tem_min"]:"'(NULL)'").",".(($value["2013"]["lluvia"]!=""&&$value["2013"]["lluvia"]!="---")?$value["2013"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2013".$value["semana"]." Monday', '%X%V %W'),17,1,11);";
            #$sql_insert_3 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2013"]["tem_max"]!=""&&$value["2013"]["tem_max"]!="---")?$value["2013"]["tem_max"]:"'(NULL)'").",".(($value["2013"]["tem_min"]!=""&&$value["2013"]["tem_min"]!="---")?$value["2013"]["tem_min"]:"'(NULL)'").",".(($value["2013"]["lluvia"]!=""&&$value["2013"]["lluvia"]!="---")?$value["2013"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2013".$value["semana"]." Monday', '%X%V %W'),16,1,10);";
            #$sql_insert_4 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2013"]["tem_max"]!=""&&$value["2013"]["tem_max"]!="---")?$value["2013"]["tem_max"]:"'(NULL)'").",".(($value["2013"]["tem_min"]!=""&&$value["2013"]["tem_min"]!="---")?$value["2013"]["tem_min"]:"'(NULL)'").",".(($value["2013"]["lluvia"]!=""&&$value["2013"]["lluvia"]!="---")?$value["2013"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2013".$value["semana"]." Monday', '%X%V %W'),20,1,10);";
            #$sql_insert_5 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2013"]["tem_max"]!=""&&$value["2013"]["tem_max"]!="---")?$value["2013"]["tem_max"]:"'(NULL)'").",".(($value["2013"]["tem_min"]!=""&&$value["2013"]["tem_min"]!="---")?$value["2013"]["tem_min"]:"'(NULL)'").",".(($value["2013"]["lluvia"]!=""&&$value["2013"]["lluvia"]!="---")?$value["2013"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2013".$value["semana"]." Monday', '%X%V %W'),15,1,10);";
            #$sql_insert_6 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2013"]["tem_max"]!=""&&$value["2013"]["tem_max"]!="---")?$value["2013"]["tem_max"]:"'(NULL)'").",".(($value["2013"]["tem_min"]!=""&&$value["2013"]["tem_min"]!="---")?$value["2013"]["tem_min"]:"'(NULL)'").",".(($value["2013"]["lluvia"]!=""&&$value["2013"]["lluvia"]!="---")?$value["2013"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2013".$value["semana"]." Monday', '%X%V %W'),14,1,9);";

            #$conexion->Consultas(1, $sql_insert_1);
            #$conexion->Consultas(1, $sql_insert_2);
            #$conexion->Consultas(1, $sql_insert_3);
            #$conexion->Consultas(1, $sql_insert_4);
            #$conexion->Consultas(1, $sql_insert_5);
            #$conexion->Consultas(1, $sql_insert_6);
            /*echo $sql_insert_1."<br>";
            echo $sql_insert_2."<br>";
            echo $sql_insert_3."<br>";
            echo $sql_insert_4."<br>";
            echo $sql_insert_5."<br>";
            echo $sql_insert_6."<br>";*/
        }
        if(isset($value["semana"]) && ($value["2014"]["tem_max"] != "" || $value["2014"]["tem_min"] != "" || $value["2014"]["lluvia"] != "")){
            $sql_insert_1 = "
                INSERT INTO datos_clima
                (
                    temp_maxima,
                    temp_minima,
                    lluvia,
                    fecha,
                    id_hacienda,
                    id_usuario,
                    id_cliente
                )
                VALUES(
                    ".(($value["2014"]["tem_max"]!=""&&$value["2014"]["tem_max"]!="---")?$value["2014"]["tem_max"]:"'(NULL)'").",
                    ".(($value["2014"]["tem_min"]!=""&&$value["2014"]["tem_min"]!="---")?$value["2014"]["tem_min"]:"'(NULL)'").",
                    ".(($value["2014"]["lluvia"]!=""&&$value["2014"]["lluvia"]!="---")?$value["2014"]["lluvia"]:"'(NULL)'").",
                    STR_TO_DATE('2014".$value["semana"]." Monday', '%X%V %W'),
                    18,
                    1,
                    10
                );
            ";
            #$sql_insert_2 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2014"]["tem_max"]!=""&&$value["2014"]["tem_max"]!="---")?$value["2014"]["tem_max"]:"'(NULL)'").",".(($value["2014"]["tem_min"]!=""&&$value["2014"]["tem_min"]!="---")?$value["2014"]["tem_min"]:"'(NULL)'").",".(($value["2014"]["lluvia"]!=""&&$value["2014"]["lluvia"]!="---")?$value["2014"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2014".$value["semana"]." Monday', '%X%V %W'),17,1,11);";
            #$sql_insert_3 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2014"]["tem_max"]!=""&&$value["2014"]["tem_max"]!="---")?$value["2014"]["tem_max"]:"'(NULL)'").",".(($value["2014"]["tem_min"]!=""&&$value["2014"]["tem_min"]!="---")?$value["2014"]["tem_min"]:"'(NULL)'").",".(($value["2014"]["lluvia"]!=""&&$value["2014"]["lluvia"]!="---")?$value["2014"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2014".$value["semana"]." Monday', '%X%V %W'),16,1,10);";
            #$sql_insert_4 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2014"]["tem_max"]!=""&&$value["2014"]["tem_max"]!="---")?$value["2014"]["tem_max"]:"'(NULL)'").",".(($value["2014"]["tem_min"]!=""&&$value["2014"]["tem_min"]!="---")?$value["2014"]["tem_min"]:"'(NULL)'").",".(($value["2014"]["lluvia"]!=""&&$value["2014"]["lluvia"]!="---")?$value["2014"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2014".$value["semana"]." Monday', '%X%V %W'),20,1,10);";
            #$sql_insert_5 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2014"]["tem_max"]!=""&&$value["2014"]["tem_max"]!="---")?$value["2014"]["tem_max"]:"'(NULL)'").",".(($value["2014"]["tem_min"]!=""&&$value["2014"]["tem_min"]!="---")?$value["2014"]["tem_min"]:"'(NULL)'").",".(($value["2014"]["lluvia"]!=""&&$value["2014"]["lluvia"]!="---")?$value["2014"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2014".$value["semana"]." Monday', '%X%V %W'),15,1,10);";
            #$sql_insert_6 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2014"]["tem_max"]!=""&&$value["2014"]["tem_max"]!="---")?$value["2014"]["tem_max"]:"'(NULL)'").",".(($value["2014"]["tem_min"]!=""&&$value["2014"]["tem_min"]!="---")?$value["2014"]["tem_min"]:"'(NULL)'").",".(($value["2014"]["lluvia"]!=""&&$value["2014"]["lluvia"]!="---")?$value["2014"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2014".$value["semana"]." Monday', '%X%V %W'),14,1,9);";

            #$conexion->Consultas(1, $sql_insert_1);
            #$conexion->Consultas(1, $sql_insert_2);
            #$conexion->Consultas(1, $sql_insert_3);
            #$conexion->Consultas(1, $sql_insert_4);
            #$conexion->Consultas(1, $sql_insert_5);
            #$conexion->Consultas(1, $sql_insert_6);
            /*echo $sql_insert_1."<br>";
            echo $sql_insert_2."<br>";
            echo $sql_insert_3."<br>";
            echo $sql_insert_4."<br>";
            echo $sql_insert_5."<br>";
            echo $sql_insert_6."<br>";*/
        }
        if(isset($value["semana"]) && ($value["2015"]["tem_max"] != "" || $value["2015"]["tem_min"] != "" || $value["2015"]["lluvia"] != "")){
            $sql_insert_1 = "
                INSERT INTO datos_clima
                (
                    temp_maxima,
                    temp_minima,
                    lluvia,
                    fecha,
                    id_hacienda,
                    id_usuario,
                    id_cliente
                )
                VALUES(
                    ".(($value["2015"]["tem_max"]!=""&&$value["2015"]["tem_max"]!="---")?$value["2015"]["tem_max"]:"'(NULL)'").",
                    ".(($value["2015"]["tem_min"]!=""&&$value["2015"]["tem_min"]!="---")?$value["2015"]["tem_min"]:"'(NULL)'").",
                    ".(($value["2015"]["lluvia"]!=""&&$value["2015"]["lluvia"]!="---")?$value["2015"]["lluvia"]:"'(NULL)'").",
                    STR_TO_DATE('2015".$value["semana"]." Monday', '%X%V %W'),
                    18,
                    1,
                    10
                );
            ";
            #$sql_insert_2 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2015"]["tem_max"]!=""&&$value["2015"]["tem_max"]!="---")?$value["2015"]["tem_max"]:"'(NULL)'").",".(($value["2015"]["tem_min"]!=""&&$value["2015"]["tem_min"]!="---")?$value["2015"]["tem_min"]:"'(NULL)'").",".(($value["2015"]["lluvia"]!=""&&$value["2015"]["lluvia"]!="---")?$value["2015"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2015".$value["semana"]." Monday', '%X%V %W'),17,1,11);";
            #$sql_insert_3 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2015"]["tem_max"]!=""&&$value["2015"]["tem_max"]!="---")?$value["2015"]["tem_max"]:"'(NULL)'").",".(($value["2015"]["tem_min"]!=""&&$value["2015"]["tem_min"]!="---")?$value["2015"]["tem_min"]:"'(NULL)'").",".(($value["2015"]["lluvia"]!=""&&$value["2015"]["lluvia"]!="---")?$value["2015"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2015".$value["semana"]." Monday', '%X%V %W'),16,1,10);";
            #$sql_insert_4 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2015"]["tem_max"]!=""&&$value["2015"]["tem_max"]!="---")?$value["2015"]["tem_max"]:"'(NULL)'").",".(($value["2015"]["tem_min"]!=""&&$value["2015"]["tem_min"]!="---")?$value["2015"]["tem_min"]:"'(NULL)'").",".(($value["2015"]["lluvia"]!=""&&$value["2015"]["lluvia"]!="---")?$value["2015"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2015".$value["semana"]." Monday', '%X%V %W'),20,1,10);";
            #$sql_insert_5 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2015"]["tem_max"]!=""&&$value["2015"]["tem_max"]!="---")?$value["2015"]["tem_max"]:"'(NULL)'").",".(($value["2015"]["tem_min"]!=""&&$value["2015"]["tem_min"]!="---")?$value["2015"]["tem_min"]:"'(NULL)'").",".(($value["2015"]["lluvia"]!=""&&$value["2015"]["lluvia"]!="---")?$value["2015"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2015".$value["semana"]." Monday', '%X%V %W'),15,1,10);";
            #$sql_insert_6 = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,fecha,id_hacienda,id_usuario,id_cliente)VALUES(".(($value["2015"]["tem_max"]!=""&&$value["2015"]["tem_max"]!="---")?$value["2015"]["tem_max"]:"'(NULL)'").",".(($value["2015"]["tem_min"]!=""&&$value["2015"]["tem_min"]!="---")?$value["2015"]["tem_min"]:"'(NULL)'").",".(($value["2015"]["lluvia"]!=""&&$value["2015"]["lluvia"]!="---")?$value["2015"]["lluvia"]:"'(NULL)'").",STR_TO_DATE('2015".$value["semana"]." Monday', '%X%V %W'),14,1,9);";

            #$conexion->Consultas(1, $sql_insert_1);
            #$conexion->Consultas(1, $sql_insert_2);
            #$conexion->Consultas(1, $sql_insert_3);
            #$conexion->Consultas(1, $sql_insert_4);
            #$conexion->Consultas(1, $sql_insert_5);
            #$conexion->Consultas(1, $sql_insert_6);
            /*echo $sql_insert_1."<br>";
            echo $sql_insert_2."<br>";
            echo $sql_insert_3."<br>";
            echo $sql_insert_4."<br>";
            echo $sql_insert_5."<br>";
            echo $sql_insert_6."<br>";*/
        }
    }
}

?>