<?php
// header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
/*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/

ini_set('max_execution_time', 300);
ini_set('memory_limit', '-1');

/*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/

error_reporting(E_ALL);


include '../controllers/conexion.php';
include 'simplexlsx.class.php';

$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName()){
        $pos1 = strpos($object->getPathName(), "ciclos/");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('xlsx' == $ext){
                switch ($ext){

                    case 'xlsx':
                        try {
                            $ruta = pathinfo($object->getPathName(), PATHINFO_DIRNAME);
                            $carpetas = explode("/", $ruta);
                            $num_dir = count($carpetas)-1;
                            $id_usuario = after('user_', $carpetas[$num_dir]);
                            $filename = "ciclos/user_".$id_usuario."/".pathinfo($object->getPathName(), PATHINFO_BASENAME);
                            readParciales($filename, $id_usuario);
                            #eliminar_archivo($filename);
                        } catch (Exception $e) {
                            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                        }
                        break;

                    default:
                        break;
                }
            }
        }
    }
}

function eliminar_archivo($ruta){
    unlink($ruta);
}

function procesar_xlsx($file_name, $id_usuario){
    $xlsx = new SimpleXLSX($file_name);
    $count = 0;
    $conexion = new M_Conexion;
    $clientes = $xlsx->sheetNames();
    $registros = array();
    foreach ($clientes as $x => $name) {
        $rows = $xlsx->rows($x);
        $names = explode(";", $name);

        $finca = trim($names[0]);
        $tipo = trim((isset($names[1])?$names[1]:''));
        $anio = trim((isset($names[2])?$names[2]:''));

        foreach ($rows as $key => $fila) {
            if($key > 0 && isset($fila[0]) && trim($fila[0]) != "" && $tipo != ""){

                $data = new stdClass;
                $data->finca = $finca;
                $data->tipo = $tipo;

                $data->fecha = $fila[0];
                if(is_numeric($data->fecha)){
                    $UNIX_DATE = ($data->fecha - 25569) * 86400;
                    $data->fecha = gmdate("Y-m-d H:i:s", $UNIX_DATE);
                }

                $data->week = $fila[1];
                $data->ciclo = $fila[2];
                $data->nombre_comercial1 = $fila[4];
                $data->ingrediente_activo1 = $fila[5];
                $data->dosis1 = $fila[6];
                $data->nombre_comercial2 = "";
                $data->ingrediente_activo2 = "";
                $data->dosis2 = "";
                $data->emul = "";
                $data->aceite = "";
                $data->vol = "";

                if((isset($fila[7]) && $fila[7] != "") || (isset($fila[8]) && $fila[8] != "")){
                    $data->nombre_comercial2 = trim($fila[7]);
                    $data->ingrediente_activo2 = trim($fila[8]);
                    $data->dosis2 = trim($fila[9]);
                }
                if(isset($fila[10]) && $fila[10] != ""){
                    $data->emul = $fila[10];
                }
                if(isset($fila[11]) && $fila[11] != ""){
                    $data->aceite = $fila[11];
                }
                if(isset($fila[12]) && $fila[12] != ""){
                    $data->vol = $fila[12];
                }
                $registros[] = $data;
            }
        }
    }

    $sqls = array();
    
    foreach($registros as $reg){
        D(json_encode($reg));
        $id_ciclo = $conexion->Consultas(1, "INSERT INTO ciclos_de_app SET id_usuario = '{$id_usuario}'");
        $id_finca = $conexion->Consultas(2, "SELECT * FROM cat_haciendas WHERE nombre = '{$reg->finca}'")[0]["id"];

        #id_producto y num_dosis
        $product1 = regProduct(array("nombre_comercial" => $reg->nombre_comercial1, "ingrediente_activo" => $reg->ingrediente_activo1, "dosis"=>$reg->dosis1), $id_usuario, 1, $id_ciclo);

        #id_producto y num_dosis
        if($reg->nombre_comercial2 != "" || $reg->ingrediente_activo2 != "")
            $product2 = regProduct(array("nombre_comercial" => $reg->nombre_comercial2, "ingrediente_activo" => $reg->ingrediente_activo2, "dosis"=>$reg->dosis2), $id_usuario, 2, $id_ciclo);
        else
            $product2 = null;

        #id_emulsificante
        if($reg->emul != "")
            $emulsificante = regDosis($reg->emul, $id_usuario);
        else
            $emulsificante = null;

        #id_aceite
        if($reg->aceite != "")
            $aceite = regDosis($reg->aceite, $id_usuario);
        else
            $aceite = null;

        $sql = "UPDATE ciclos_de_app SET 
            id_usuario = '{$id_usuario}',
            fecha = '{$reg->fecha}',
            ciclo = '{$reg->ciclo}',
            id_producto_1 = '{$product1->id}',
            dosis_num_1 = '{$product1->num_dosis}',
            id_producto_2 = '".(($product2 == null)?"":"{$product2->id}")."',
            dosis_num_2 = '".(($product2 == null)?"":"{$product2->num_dosis}")."',
            id_emulsificante = '{$emulsificante}',
            id_aceite = '{$aceite}',
            dosis_num_emulsificante = 1,
            dosis_num_aceite = 1,
            id_finca = '{$id_finca}',
            tipo = '".ucwords(strtolower($reg->tipo))."'

            WHERE id = {$id_ciclo}
        ";

        $registros[] = $sql;
        $conexion->Consultas(1, $sql);
        D($sql);
    }
    //echo "Termino\n<a href='http://sigat.procesos-iq.com/importarInformacionCiclos'>Regresar</a>";
}

function readParciales($file_name, $id_usuario){
    $xlsx = new SimpleXLSX($file_name);
    $count = 0;
    $conexion = new M_Conexion;
    $clientes = $xlsx->sheetNames();
    $registros = array();
    foreach ($clientes as $x => $name) {
        $rows = $xlsx->rows($x);
        foreach ($rows as $key => $fila) {
            $data = new stdClass;
            $data->finca = trim($fila[1]);
            $data->ciclo = trim($fila[2]);
            $data->ha = trim($fila[3]);
            if(is_numeric($fila[4])){
                $UNIX_DATE = ($fila[4] - 25569) * 86400;
                $data->fecha_prog = gmdate("Y-m-d H:i:s", $UNIX_DATE);
            }else{
                $data->fecha_prog = '';
            }
            if(is_numeric($fila[5])){
                $UNIX_DATE = ($fila[5] - 25569) * 86400;
                $data->fecha_real = gmdate("Y-m-d H:i:s", $UNIX_DATE);
            }else{
                $data->fecha_real = '';
            }
            $data->sem = trim($fila[6]);
            $data->atraso = trim($fila[7]);
            $data->motivo = trim($fila[8]);

            $data->fungicida_1 = trim($fila[9]);
            $data->dosis_1 = trim($fila[10]);
            $data->cantidad_1 = trim($fila[11]);
            $data->precio_1 = trim($fila[12]);
            $data->ha_1 = trim($fila[13]);
            $data->total_1 = trim($fila[14]);

            $data->fungicida_2 = trim($fila[15]);
            $data->dosis_2 = trim($fila[16]);
            $data->cantidad_2 = trim($fila[17]);
            $data->precio_2 = trim($fila[18]);
            $data->ha_2 = trim($fila[19]);
            $data->total_2 = trim($fila[20]);

            $data->bioestimulante_1 = trim($fila[21]);
            $data->dosis_3 = trim($fila[22]);
            $data->cantidad_3 = trim($fila[23]);
            $data->precio_3 = trim($fila[24]);
            $data->ha_3 = trim($fila[25]);
            $data->total_3 = trim($fila[26]);

            $data->bioestimulante_2 = trim($fila[27]);
            $data->dosis_4 = trim($fila[28]);
            $data->cantidad_4 = trim($fila[29]);
            $data->precio_4 = trim($fila[30]);
            $data->ha_4 = trim($fila[31]);
            $data->total_4 = trim($fila[32]);

            $data->coadyuvante_1 = trim($fila[33]);
            $data->dosis_5 = trim($fila[34]);
            $data->cantidad_5 = trim($fila[35]);
            $data->precio_5 = trim($fila[36]);
            $data->ha_5 = trim($fila[37]);
            $data->total_5 = trim($fila[38]);

            $data->aceite = trim($fila[45]);
            $data->dosis_7 = trim($fila[46]);
            $data->cantidad_7 = trim($fila[47]);
            $data->precio_7 = trim($fila[48]);
            $data->ha_7 = trim($fila[49]);
            $data->total_7 = trim($fila[50]);

            $data->foliar_1 = trim($fila[51]);
            $data->dosis_8 = trim($fila[52]);
            $data->cantidad_8 = trim($fila[53]);
            $data->precio_8 = trim($fila[54]);
            $data->ha_8 = trim($fila[55]);
            $data->total_8 = trim($fila[56]);

            $data->foliar_2 = trim($fila[57]);
            $data->dosis_9 = trim($fila[58]);
            $data->cantidad_9 = trim($fila[59]);
            $data->precio_9 = trim($fila[60]);
            $data->ha_9 = trim($fila[61]);
            $data->total_9 = trim($fila[62]);

            $data->foliar_3 = trim($fila[63]);
            $data->dosis_10 = trim($fila[64]);
            $data->cantidad_10 = trim($fila[65]);
            $data->precio_10 = trim($fila[66]);
            $data->ha_10 = trim($fila[67]);
            $data->total_10 = trim($fila[68]);

            $data->agua = trim($fila[75]);
            $data->dosis_12 = trim($fila[76]);
            $data->cantidad_12 = trim($fila[77]);
            $data->ha_oper = trim($fila[78]);
            $data->oper = trim($fila[79]);
            $data->costo_total = trim($fila[80]);
            $data->costo_ha = trim($fila[81]);

            $sql = "INSERT INTO ciclos_aplicacion_historico SET
                        finca = '{$data->finca}',
                        ciclo = '{$data->ciclo}',
                        ha = '{$data->ha}',
                        fecha_prog = DATE('{$data->fecha_prog}'),
                        fecha_real = DATE('{$data->fecha_real}'),
                        sem = '{$data->sem}',
                        atraso = '{$data->atraso}',
                        motivo = '{$data->motivo}',

                        fungicida_1 = '{$data->fungicida_1}',
                        dosis_1 = '{$data->dosis_1}',
                        cantidad_1 = '{$data->cantidad_1}',
                        precio_1 = '{$data->precio_1}',
                        ha_1 = '{$data->ha_1}',
                        total_1 = '{$data->total_1}',

                        fungicida_2 = '{$data->fungicida_2}',
                        dosis_2 = '{$data->dosis_2}',
                        cantidad_2 = '{$data->cantidad_2}',
                        precio_2 = '{$data->precio_2}',
                        ha_2 = '{$data->ha_2}',
                        total_2 = '{$data->total_2}',

                        bioestimulantes_1 = '{$data->bioestimulante_1}',
                        dosis_3 = '{$data->dosis_3}',
                        cantidad_3 = '{$data->cantidad_3}',
                        precio_3 = '{$data->precio_3}',
                        ha_3 = '{$data->ha_3}',
                        total_3 = '{$data->total_3}',
                        
                        bioestimulantes_2 = '{$data->bioestimulante_2}',
                        dosis_4 = '{$data->dosis_4}',
                        cantidad_4 = '{$data->cantidad_4}',
                        precio_4 = '{$data->precio_4}',
                        ha_4 = '{$data->ha_4}',
                        total_4 = '{$data->total_4}',
                        
                        coadyuvante_1 = '{$data->coadyuvante_1}',
                        dosis_5 = '{$data->dosis_5}',
                        cantidad_5 = '{$data->cantidad_5}',
                        precio_5 = '{$data->precio_5}',
                        ha_5 = '{$data->ha_5}',
                        total_5 = '{$data->total_5}',
                        
                        aceite = '{$data->aceite}',
                        dosis_7 = '{$data->dosis_7}',
                        cantidad_7 = '{$data->cantidad_7}',
                        precio_7 = '{$data->precio_7}',
                        ha_7 = '{$data->ha_7}',
                        total_7 = '{$data->total_7}',
                        
                        foliar_1 = '{$data->foliar_1}',
                        dosis_8 = '{$data->dosis_8}',
                        cantidad_8 = '{$data->cantidad_8}',
                        precio_8 = '{$data->precio_8}',
                        ha_8 = '{$data->ha_8}',
                        total_8 = '{$data->total_8}',
                        
                        foliar_2 = '{$data->foliar_2}',
                        dosis_9 = '{$data->dosis_9}',
                        cantidad_9 = '{$data->cantidad_9}',
                        precio_9 = '{$data->precio_9}',
                        ha_9 = '{$data->ha_9}',
                        total_9 = '{$data->total_9}',
                        
                        foliar_3 = '{$data->foliar_3}',
                        dosis_10 = '{$data->dosis_10}',
                        cantidad_10 = '{$data->cantidad_10}',
                        precio_10 = '{$data->precio_10}',
                        ha_10 = '{$data->ha_10}',
                        total_10 = '{$data->total_10}',
                        
                        agua = '{$data->agua}',
                        dosis_12 = '{$data->dosis_12}',
                        cantidad_12 = '{$data->cantidad_12}',
                        ha_oper = '{$data->ha_oper}',
                        oper = '{$data->oper}',
                        costo_total = '{$data->costo_total}',
                        costo_ha = '{$data->costo_ha}',
                        years = 2017,
                        tipo_aplicacion = 'PARCIAL',
                        programa = 'Sigatoka',
                        tipo_ciclo = 'Parcial'";
            $conexion->query($sql);
        }
    }
}

function regDosis($dosis, $id_usuario){
    $conexion = new M_Conexion;
    $dosis = explode("-", $dosis);
    $dosis = $dosis[0];
    $doss = "";
    foreach(explode(" ", $dosis) as $part){
        $doss .= "{$part}";
    }
    $doss = str_replace(",", ".", $doss);

    $sql = "INSERT INTO productos SET dosis = '{$doss}', id_usuario = '$id_usuario'";
    $id_producto = $conexion->Consultas(1, $sql);
    D($sql);
    return $id_producto;
}

function regProduct($product, $id_usuario, $num, $id_ciclo){
    $conexion = new M_Conexion;

    if(trim($product["nombre_comercial"]) != ""){
        $res = $conexion->Consultas(2, "SELECT * FROM productos WHERE nombre_comercial = '{$product["nombre_comercial"]}'");
        $dosis = explode("-", $product["dosis"]);
        $dosis = $dosis[0];
        $doss = "";
        foreach(explode(" ", $dosis) as $part){
            $doss .= "{$part}";
        }
        $doss = str_replace(",", ".", $doss);

        if(count($res) > 0){
            $id_producto = $res[0]["id"];
            switch($doss){
                case "{$res[0]["dosis"]}":
                    $num_dosis = 1;
                    break;
                case "{$res[0]["dosis2"]}":
                    $num_dosis = 2;
                    break;
                case "{$res[0]["dosis3"]}":
                    $num_dosis = 3;
                    break;
                default:
                    $num_dosis = 1;
                    break;
            }
        }else{
            $sql = "INSERT INTO productos SET nombre_comercial = '{$product["nombre_comercial"]}', ingrediente_activo = '".$product["ingrediente_activo"]."', dosis = '{$doss}', id_usuario = '$id_usuario'";
            $id_producto = $conexion->Consultas(1, $sql);
            $num_dosis = 1;

            $sql = "INSERT INTO ".(($num==1)?'ciclo_producto1':'ciclo_producto2')." SET id_ciclo = '{$id_ciclo}', nombre_comun = '{$product["nombre_comercial"]}', nombre_molecula = '{$product["ingrediente_activo"]}', dosis = '{$doss}'";

            $conexion->Consultas(1, $sql);
            D($sql);
        }
    }else if(trim($product["ingrediente_activo"]) != ""){
        $res = $conexion->Consultas(2, "SELECT * FROM productos WHERE ingrediente_activo = '{$product["ingrediente_activo"]}'");
        $dosis = explode("-", $product["dosis"]);
        $dosis = $dosis[0];
        $doss = "";
        foreach(explode(" ", $dosis) as $part){
            $doss .= "{$part}";
        }
        $doss = str_replace(",", ".", $doss);

        if(count($res) == 0){
            $sql = "INSERT INTO productos SET ingrediente_activo = '".$product["ingrediente_activo"]."', dosis = '{$doss}', id_usuario = '$id_usuario'";
            $id_producto = $conexion->Consultas(1, $sql);
            $num_dosis = 1;

            $sql = "INSERT INTO ".(($num==1)?'ciclo_producto1':'ciclo_producto2')." SET id_ciclo = '{$id_ciclo}', nombre_molecula = '{$product["ingrediente_activo"]}', dosis = '{$doss}'";
            
            $conexion->Consultas(1, $sql);
            D($sql);
        }else{
            $id_producto = $res[0]["id"];
            switch($doss){
                case "{$res[0]["dosis"]}":
                    $num_dosis = 1;
                    break;
                case "{$res[0]["dosis2"]}":
                    $num_dosis = 2;
                    break;
                case "{$res[0]["dosis3"]}":
                    $num_dosis = 3;
                    break;
                default:
                    $num_dosis = 1;
                    break;
            }
        }
    }
    return (object) array("id" => $id_producto, "num_dosis" => $num_dosis);
}

function D($data){
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function after ($this, $inthat)
    {
        if (!is_bool(strpos($inthat, $this)))
        return substr($inthat, strpos($inthat,$this)+strlen($this));
    };

?>