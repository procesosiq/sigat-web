<?php
// header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
/*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/

ini_set('max_execution_time', 300);
ini_set('memory_limit', '-1');

/*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/

error_reporting(E_ALL);

include '../controllers/conexion.php';
include 'simplexlsx.class.php';

$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName()){
        $pos1 = strpos($object->getPathName(), "climasOrodelti/");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('xlsx' == $ext){
                switch ($ext){

                    case 'xlsx':
                        try {
                            $ruta = pathinfo($object->getPathName(), PATHINFO_DIRNAME);
                            $carpetas = explode("/", $ruta);
                            $num_dir = count($carpetas)-1;
                            $id_usuario = after ('user_', $carpetas[$num_dir]);

                            $filename = "climasOrodelti/user_1/".pathinfo($object->getPathName(), PATHINFO_BASENAME);
                            procesar_xlsx($filename, $id_usuario);
                            #eliminar_archivo($filename);
                        } catch (Exception $e) {
                            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                        }
                        break;

                    default:
                        break;
                }
            }
        }
    }
}

function eliminar_archivo($ruta){
    unlink($ruta);
}

function procesar_xlsx($file_name, $id_usuario){
    $xlsx = new SimpleXLSX($file_name);
    $conexion = new M_Conexion;
    $count = 0;
    $clientes = $xlsx->sheetNames();
    // print_r($cliente);
    // for($x = 1; $x <= $xlsx->sheetsCount(); $x++){
    D($clientes);
    $registro = [];
    foreach ($clientes as $x => $name) {
        $rows = $xlsx->rows($x);
        foreach ($rows as $key => $fila) {
            if($key > 1){
                foreach($fila  as $key2 => $celda){
                    if($key2 == 0){
                        $ts = ($celda - 25569)*86400;
                        $registro[$key+$count]["fecha"] = date('Y-m-d', $ts);
                    }
                    if($key2 == 1){
                        $ts = ($celda - 25569)*86400;
                        $registro[$key+$count]["hora"] = date('H:m:s', $ts);
                    }
                    if($key2 == 3){
                        $registro[$key+$count]["tem_max"] = (isset($celda)?$celda:"");
                    }
                    if($key2 == 4){
                        $registro[$key+$count]["tem_min"] = (isset($celda)?$celda:"");
                    }
                    if($key2 == 17){
                        $registro[$key+$count]["lluvia"] = (isset($celda)?$celda:"");
                    }
                    if($key2 == 19){
                        $registro[$key+$count]["rad_solar"] = (isset($celda)?$celda:"");
                    }
                    if($key2 == 20){
                        $registro[$key+$count]["dias_sol"] = (isset($celda)?$celda:"");
                    }
                    if($key2 == 5){
                        $registro[$key+$count]["humedad"] = (isset($celda)?$celda:"");
                    }
                }
                #$cliente = trim($xlsx->sheetName($x));
                #$sql_hacienda = "SELECT id, id_cliente FROM cat_haciendas WHERE nombre = '".$cliente."'";
                #$result_hacienda = $conexion->Consultas(2, $sql_hacienda);

                #if(count($result_hacienda) > 0){
                    #$registro[$key+$count]["id_finca"] = $result_hacienda[0]["id"];
                    #$registro[$key+$count]["id_cliente"] = $result_hacienda[0]["id_cliente"];
                #}
                $registro[$key+$count]["id_finca"] = 78;
                $registro[$key+$count]["id_cliente"] = 29;
            }
        }
        $count = count($registro);
    }

    $insertados = 0;
    foreach ($registro as $key => $value) {
        if($insertados > 0){
            $horas_luz = $conexion->queryRow("SELECT ROUND((TIME_TO_SEC('{$value["hora"]}')-TIME_TO_SEC(hora)) / 60 / 60, 2) AS luz 
                            FROM datos_clima
                            WHERE id_hacienda = {$value["id_finca"]} AND fecha = '{$value["fecha"]}' AND hora < '{$value["hora"]}'
                            ORDER BY hora DESC
                            LIMIT 1")->luz;
        }
        if(!$horas_luz) $horas_luz = 0;

        if($value["fecha"] != "" && isset($value["fecha"]) && ($value["tem_max"] != "" || $value["tem_min"] != "" || $value["lluvia"] != "" || $value["rad_solar"] != "" || $value["dias_sol"] != "" || $value["humedad"] != "")){
            $insertados++;
            $sql_insert = "
                INSERT INTO datos_clima
                (
                    temp_maxima,
                    temp_minima,
                    lluvia,
                    humedad,
                    rad_solar,
                    dias_sol,
                    fecha,
                    hora,
                    id_hacienda,
                    semana,
                    horas_luz,
                    id_cliente,
                    id_usuario
                )
                VALUES(
                    ".(($value["tem_max"]!=""&&$value["tem_max"]!="---")?$value["tem_max"]:"NULL").",
                    ".(($value["tem_min"]!=""&&$value["tem_min"]!="---")?$value["tem_min"]:"NULL").",
                    ".(($value["lluvia"]!=""&&$value["lluvia"]!="---")?$value["lluvia"]:"NULL").",
                    ".(($value["humedad"]!=""&&$value["humedad"]!="---")?$value["humedad"]:"NULL").",
                    ".(($value["rad_solar"]!=""&&$value["rad_solar"]!="---")?$value["rad_solar"]:"NULL").",
                    ".(($value["dias_sol"]!=""&&$value["dias_sol"]!="---")?$value["dias_sol"]:"NULL").",
                    '".$value["fecha"]."',
                    '".$value["hora"]."',
                    ".$value['id_finca'].",
                    WEEK('{$value["fecha"]}'),
                    {$horas_luz},
                    {$value['id_cliente']},
                    1
                );
            ";
            D($sql_insert);
            $conexion->Consultas(1, $sql_insert);
        }
    }
}

function D($val){
    echo "<pre>";
    print_r($val);
    echo "</pre>";
}

function after ($this, $inthat)
    {
        if (!is_bool(strpos($inthat, $this)))
        return substr($inthat, strpos($inthat,$this)+strlen($this));
    };

?>