<?php
// header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
/*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/

ini_set('max_execution_time', 300);
ini_set('memory_limit', '-1');

/*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/

error_reporting(E_ALL);


include '../controllers/conexion.php';
include 'simplexlsx.class.php';

$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName()){
        $pos1 = strpos($object->getPathName(), "cuadro_costos/");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('xlsx' == $ext){
                switch ($ext){
                    case 'xlsx':
                        try {
                            $ruta = pathinfo($object->getPathName(), PATHINFO_DIRNAME);
                            $carpetas = explode("/", $ruta);
                            $num_dir = count($carpetas)-1;
                            $filename = "cuadro_costos/".pathinfo($object->getPathName(), PATHINFO_BASENAME);
                            procesar_xlsx($filename);
                            #eliminar_archivo($filename);
                        } catch (Exception $e) {
                            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                        }
                        break;

                    default:
                        break;
                }
            }
        }
    }
}

function eliminar_archivo($ruta){
    unlink($ruta);
}

function procesar_xlsx($file_name){
    $xlsx = new SimpleXLSX($file_name);
    $conexion = new M_Conexion;
    $hojas = $xlsx->sheetNames();
    $registros = array();

    $posicion_ciclos = 0;
    $posicion_foliares = 0;
    $posicion_parciales = 0;

    $filas_ciclos = [];
    $filas_foliares = [];
    $filas_parciales = [];
    $no_repetidos = [];
    foreach ($hojas as $x => $name) {
        $rows = $xlsx->rows($x);
        #D("- ".$name);
        foreach ($rows as $key => $fila) {
            if(trim($fila[0]) == 'CONTROL DE COSTOS ORODELTI S.A'){
                $posicion_ciclos = $key;
                $LLAVE = 'CICLOS';
            }else if(trim($fila[0]) == 'FOLIARES'){
                $posicion_foliares = $key;
                $LLAVE = 'FOLIARES';
            }else if(trim($fila[0]) == 'PARCIALES'){
                $posicion_parciales = $key;
                $LLAVE = 'PARCIALES';
            }else if($fila[0] != "" && $fila[0] != null){
                if($LLAVE == 'CICLOS' && $key > $posicion_ciclos+2){
                    if(is_numeric($fila[5])){
                        $UNIX_DATE = ($fila[5] - 25569) * 86400;
                        $fecha_programada = gmdate("Y-m-d", $UNIX_DATE);
                    }

                    if(is_numeric($fila[6])){
                        $UNIX_DATE = ($fila[6] - 25569) * 86400;
                        $fecha_real = gmdate("Y-m-d", $UNIX_DATE);
                    }

                    $filas_ciclos[] = [
                        "ciclo" => $fila[0],
                        "frec" => $fila[1],
                        "programa" => ($fila[2] == 'Sigatoka-foliar') 
                                            ? 'Sigatoka Foliar' 
                                            : ($fila[2] == 'sigatoka-plagas') 
                                                ? 'Sigatoka Plagas' 
                                                : ($fila[2] == 'sigatpka')
                                                    ? 'Sigatoka'
                                                    : $fila[2],
                        "semana" => $fila[3],
                        "finca" => $fila[4],
                        "programada" => $fecha_programada,
                        "real" => $fecha_real,
                        "has" => $fila[7],
                        "dias" => $fila[8],
                        "motivo" => $fila[9],
                        "productos" => [
                            "1" => [
                                "nombre" => strtoupper($fila[10]),
                                "dosis" => $fila[18],
                                "cantidad" => $fila[26],
                                "precio" => $fila[34],
                                "total" => $fila[42]
                            ],
                            "2" => [
                                "nombre" => strtoupper($fila[11]),
                                "dosis" => $fila[19],
                                "cantidad" => $fila[27],
                                "precio" => $fila[35],
                                "total" => $fila[43]
                            ],
                            "3" => [
                                "nombre" => strtoupper($fila[12]),
                                "dosis" => $fila[20],
                                "cantidad" => $fila[28],
                                "precio" => $fila[36],
                                "total" => $fila[44]
                            ],
                            "4" => [
                                "nombre" => strtoupper($fila[13]),
                                "dosis" => $fila[21],
                                "cantidad" => $fila[29],
                                "precio" => $fila[37],
                                "total" => $fila[45]
                            ],
                            "5" => [
                                "nombre" => strtoupper($fila[14]),
                                "dosis" => $fila[22],
                                "cantidad" => $fila[30],
                                "precio" => $fila[38],
                                "total" => $fila[46]
                            ],
                            "6" => [
                                "nombre" => strtoupper($fila[15]),
                                "dosis" => $fila[23],
                                "cantidad" => $fila[31],
                                "precio" => $fila[39],
                                "total" => $fila[47]
                            ],
                            "7" => [
                                "nombre" => strtoupper($fila[16]),
                                "dosis" => $fila[24],
                                "cantidad" => $fila[32],
                                "precio" => $fila[40],
                                "total" => $fila[48]
                            ],
                            "8" => [
                                "nombre" => strtoupper($fila[17]),
                                "dosis" => $fila[25],
                                "cantidad" => $fila[33],
                                "precio" => $fila[41],
                                "total" => $fila[49]
                            ]
                        ],
                        "costo_operacion" => $fila[50],
                        "total_costo_operacion" => $fila[51],
                        "costo_total" => $fila[52],
                        "costo_has_neta" => $fila[53],
                        "observaciones" => $fila[54]
                    ];
                }else if($LLAVE == 'FOLIARES' && $key > $posicion_foliares+2){
                    if(is_numeric($fila[5])){
                        $UNIX_DATE = ($fila[5] - 25569) * 86400;
                        $fecha_programada = gmdate("Y-m-d", $UNIX_DATE);
                    }

                    if(is_numeric($fila[6])){
                        $UNIX_DATE = ($fila[6] - 25569) * 86400;
                        $fecha_real = gmdate("Y-m-d", $UNIX_DATE);
                    }

                    $filas_foliares[] = [
                        "ciclo" => $fila[0],
                        "frec" => $fila[1],
                        "programa" => ($fila[2] == 'Sigatoka-foliar') 
                                            ? 'Sigatoka Foliar' 
                                            : ($fila[2] == 'sigatoka-plagas') 
                                                ? 'Sigatoka Plagas' 
                                                : ($fila[2] == 'sigatpka')
                                                    ? 'Sigatoka'
                                                    : $fila[2],
                        "semana" => $fila[3],
                        "finca" => $fila[4],
                        "programada" => $fecha_programada,
                        "real" => $fecha_real,
                        "has" => $fila[7],
                        "dias" => $fila[8],
                        "motivo" => $fila[9],
                        "productos" => [
                            "1" => [
                                "nombre" => strtoupper($fila[10]),
                                "dosis" => $fila[18],
                                "cantidad" => $fila[26],
                                "precio" => $fila[34],
                                "total" => $fila[42]
                            ],
                            "2" => [
                                "nombre" => strtoupper($fila[11]),
                                "dosis" => $fila[19],
                                "cantidad" => $fila[27],
                                "precio" => $fila[35],
                                "total" => $fila[43]
                            ],
                            "3" => [
                                "nombre" => strtoupper($fila[12]),
                                "dosis" => $fila[20],
                                "cantidad" => $fila[28],
                                "precio" => $fila[36],
                                "total" => $fila[44]
                            ],
                            "4" => [
                                "nombre" => strtoupper($fila[13]),
                                "dosis" => $fila[21],
                                "cantidad" => $fila[29],
                                "precio" => $fila[37],
                                "total" => $fila[45]
                            ],
                            "5" => [
                                "nombre" => strtoupper($fila[14]),
                                "dosis" => $fila[22],
                                "cantidad" => $fila[30],
                                "precio" => $fila[38],
                                "total" => $fila[46]
                            ],
                            "6" => [
                                "nombre" => strtoupper($fila[15]),
                                "dosis" => $fila[23],
                                "cantidad" => $fila[31],
                                "precio" => $fila[39],
                                "total" => $fila[47]
                            ],
                            "7" => [
                                "nombre" => strtoupper($fila[16]),
                                "dosis" => $fila[24],
                                "cantidad" => $fila[32],
                                "precio" => $fila[40],
                                "total" => $fila[48]
                            ],
                            "8" => [
                                "nombre" => strtoupper($fila[17]),
                                "dosis" => $fila[25],
                                "cantidad" => $fila[33],
                                "precio" => $fila[41],
                                "total" => $fila[49]
                            ]
                        ],
                        "costo_operacion" => $fila[50],
                        "total_costo_operacion" => $fila[51],
                        "costo_total" => $fila[52],
                        "costo_has_neta" => $fila[53],
                        "observaciones" => $fila[54]
                    ];

                    //$filas_foliares[] = $fila;
                }else if($LLAVE == 'PARCIALES' && $key > $posicion_parciales+2){
                    if(is_numeric($fila[5])){
                        $UNIX_DATE = ($fila[5] - 25569) * 86400;
                        $fecha_programada = gmdate("Y-m-d", $UNIX_DATE);
                    }

                    if(is_numeric($fila[6])){
                        $UNIX_DATE = ($fila[6] - 25569) * 86400;
                        $fecha_real = gmdate("Y-m-d", $UNIX_DATE);
                    }
                    $filas_parciales[] = [
                        "ciclo" => $fila[0],
                        "frec" => $fila[1],
                        "programa" => ($fila[2] == 'Sigatoka-foliar') 
                                            ? 'Sigatoka Foliar' 
                                            : ($fila[2] == 'sigatoka-plagas') 
                                                ? 'Sigatoka Plagas' 
                                                : ($fila[2] == 'sigatpka')
                                                    ? 'Sigatoka'
                                                    : $fila[2],
                        "semana" => $fila[3],
                        "finca" => $fila[4],
                        "programada" => $fecha_programada,
                        "real" => $fecha_real,
                        "has" => $fila[7],
                        "dias" => $fila[8],
                        "motivo" => $fila[9],
                        "productos" => [
                            "1" => [
                                "nombre" => strtoupper($fila[10]),
                                "dosis" => $fila[18],
                                "cantidad" => $fila[26],
                                "precio" => $fila[34],
                                "total" => $fila[42]
                            ],
                            "2" => [
                                "nombre" => strtoupper($fila[11]),
                                "dosis" => $fila[19],
                                "cantidad" => $fila[27],
                                "precio" => $fila[35],
                                "total" => $fila[43]
                            ],
                            "3" => [
                                "nombre" => strtoupper($fila[12]),
                                "dosis" => $fila[20],
                                "cantidad" => $fila[28],
                                "precio" => $fila[36],
                                "total" => $fila[44]
                            ],
                            "4" => [
                                "nombre" => strtoupper($fila[13]),
                                "dosis" => $fila[21],
                                "cantidad" => $fila[29],
                                "precio" => $fila[37],
                                "total" => $fila[45]
                            ],
                            "5" => [
                                "nombre" => strtoupper($fila[14]),
                                "dosis" => $fila[22],
                                "cantidad" => $fila[30],
                                "precio" => $fila[38],
                                "total" => $fila[46]
                            ],
                            "6" => [
                                "nombre" => strtoupper($fila[15]),
                                "dosis" => $fila[23],
                                "cantidad" => $fila[31],
                                "precio" => $fila[39],
                                "total" => $fila[47]
                            ],
                            "7" => [
                                "nombre" => strtoupper($fila[16]),
                                "dosis" => $fila[24],
                                "cantidad" => $fila[32],
                                "precio" => $fila[40],
                                "total" => $fila[48]
                            ],
                            "8" => [
                                "nombre" => strtoupper($fila[17]),
                                "dosis" => $fila[25],
                                "cantidad" => $fila[33],
                                "precio" => $fila[41],
                                "total" => $fila[49]
                            ]
                        ],
                        "costo_operacion" => $fila[50],
                        "total_costo_operacion" => $fila[51],
                        "costo_total" => $fila[52],
                        "costo_has_neta" => $fila[53],
                        "observaciones" => $fila[54]
                    ];

                    //$filas_parciales[] = $fila;
                }
            }
        }
    }

    $fumipalma = [9.7, 10.2, 10.7, 11.9, 9.4]; //9
    $aifa = [11.86, 12.38, 12.96, 13.56, 9.5]; //10
    $otro = [11.5, 11.75, 11.6]; //17
    $tipos_productos = $conexion->queryAllSpecial("SELECT id, nombre AS label FROM cat_tipo_productos");
    $dif_precios = [];
    $dif_prod = [];
    
    $programa = 'Sigatoka';
    $tipo_ciclo = 'PARCIAL';
    foreach($filas_parciales as $row){
        if($tipo_ciclo == 'PARCIAL'){
            $row['ciclo'] = round($row['ciclo'], 1);
        }
        
        $exist = $conexion->queryOne("SELECT COUNT(1) FROM ciclos_aplicacion_hist WHERE finca = '{$row['finca']}' AND fecha_real = '{$row['real']}' AND (programa = '{$row['programa']}' OR programa = '{$programa}') AND tipo_ciclo = '{$tipo_ciclo}' AND hectareas_fumigacion = '{$row['has']}'");
        if(!$exist){
            $dosis_agua = 0;
            foreach($row['productos'] as $prod){
                if($prod['nombre'] == 'AGUA'){
                    $dosis_agua = (float) $prod['dosis'];
                }
            }

            if(in_array($row['costo_operacion'], $fumipalma)){
                $id_fumigadora = 2;
            }
            else if(in_array($row['costo_operacion'], $aifa)){
                $id_fumigadora = 1;
            }
            else{
                $id_fumigadora = 4;
            }
            if($row['motivo'] == '-' || $row['motivo'] == '') $row['motivo'] = 'Normal';
            if($row['motivo'] == 'Cond+operac' || $row['motivo'] == 'Condic + operac') $row['motivo'] = 'Cond-Oper';
            if($row['motivo'] == 'condic') $row['motivo'] = 'Condiciones';
            if($row['motivo'] == 'Condic +proc') $row['motivo'] = 'Cond-Proc';
            if($row['motivo'] == 'Proc+condic') $row['motivo'] = 'Cond-Proc';
            if($row['motivo'] == 'Proc+operac') $row['motivo'] = 'Oper-Proc';
            if($row['motivo'] == 'Cond+proceso') $row['motivo'] = 'Cond-Proc';

            
            $sql = "INSERT INTO ciclos_aplicacion_hist SET 
            num_ciclo = '{$row['ciclo']}',
            fecha_prog = '{$row['programada']}',
            fecha_real = '{$row['real']}',
            id_finca = (SELECT id FROM cat_fincas WHERE nombre = '{$row['finca']}' AND status = 1),
            finca = UPPER('{$row['finca']}'),
            hectareas_fumigacion = '{$row['has']}',
            atraso = '{$row['dias']}',
            motivo = '{$row['motivo']}',
            semana = getWeek('{$row['real']}'),
            anio = YEAR('{$row['real']}'),
            -- programa = UPPER('{$row['programa']}'),
            programa = '{$programa}',
            ha_oper = '{$row['costo_operacion']}',
            oper = '{$row['total_costo_operacion']}',
            costo_total = '{$row['costo_total']}',
            costo_ha = '{$row['costo_has_neta']}',
            dosis_agua = $dosis_agua,
            tipo_ciclo = '{$tipo_ciclo}',
            id_fumigadora = {$id_fumigadora}";
            D($sql);
            #$conexion->query($sql);
            $id_main = $conexion->getLastID();

            foreach($row['productos'] as $prod){
                if($prod['nombre'] != 'AGUA' && $prod['nombre'] != ''){
                    if($prod['nombre'] == 'BIOEMUGLOP') $prod['nombre'] = 'BIOEMUGLOB';
                    if($prod['nombre'] == 'COMETGOLD') $prod['nombre'] = 'COMET GOLD';
                    if($prod['nombre'] == 'LUNA T.' || $prod['nombre'] == 'LUNA T') $prod['nombre'] = 'LUNA TRANQUILITY';
                    if($prod['nombre'] == 'PTA88') $prod['nombre'] = 'PTA 88';
                    if($prod['nombre'] == 'RUBRICT') $prod['nombre'] = 'RUBRIC';
                    if($prod['nombre'] == 'EMULAD') $prod['nombre'] = 'EMULAD SE';
                    if($prod['nombre'] == 'MERITROJO') $prod['nombre'] = 'MERIT ROJO';
                    if($prod['nombre'] == 'BIEOMUGLOP') $prod['nombre'] = 'BIOEMUGLOB';
                    if($prod['nombre'] == 'SURGER') $prod['nombre'] = 'SURGE';
                    if($prod['nombre'] == 'RUBIRCT') $prod['nombre'] = 'RUBRIC';
                    if($prod['nombre'] == 'BIOFOL CA' || $prod['nombre'] == 'BIOFOLCA') $prod['nombre'] = 'BIOFOL';
                    

                    $id_producto = (int) $conexion->queryOne("SELECT id FROM products WHERE nombre_comercial = '{$prod['nombre']}'");
                    $id_tipo_producto = (int) $conexion->queryOne("SELECT id_tipo_producto FROM products WHERE nombre_comercial = '{$prod['nombre']}'");
                    $id_proveedor = (int) $conexion->queryOne("SELECT id_proveedor FROM products WHERE nombre_comercial = '{$prod['nombre']}'");
                    if($id_proveedor > 0)
                        $proveedor = $conexion->queryOne("SELECT nombre FROM cat_proveedores WHERE id = {$id_proveedor}");
                    else $proveedor = '';

                    if($prod['nombre'] == 'ACEITE'){
                        if(in_array($row['costo_operacion'], $fumipalma)){
                            $prod['nombre'] = 'BANOLE';
                            $proveedor = 'FUMIPALMA';
                            $id_proveedor = 9;
                            $id_tipo_producto = 6;
                            $id_producto = 49;
                        }
                        else if(in_array($row['costo_operacion'], $aifa)){
                            $prod['nombre'] = 'BANOLE';
                            $proveedor = 'AIFA';
                            $id_proveedor = 10;
                            $id_tipo_producto = 6;
                            $id_producto = 73;
                        }
                        else{
                            $prod['nombre'] = 'BANOLE';
                            $proveedor = 'OTRO';
                            $id_proveedor = 17;
                            $id_tipo_producto = 6;
                            $id_producto = 81;
                        }
                    }
                    
                    $sql = "INSERT INTO ciclos_aplicacion_hist_productos SET
                        id_ciclo_aplicacion = $id_main,
                        id_tipo_producto = $id_tipo_producto,
                        id_proveedor = {$id_proveedor},
                        id_producto = {$id_producto},
                        producto = '{$prod['nombre']}',
                        proveedor = '{$proveedor}',
                        cantidad = {$prod['cantidad']},
                        precio = {$prod['precio']},
                        dosis = {$prod['dosis']},
                        total = {$prod['cantidad']} * {$prod['precio']}";
                    #$conexion->query($sql);
                    D($sql);

                    $sql = "INSERT INTO ciclos_aplicacion_detalle SET
                        id_producto = $id_producto,
                        id_proveedor = $id_proveedor,
                        finca = '{$row['finca']}',
                        ciclo = '{$row['ciclo']}',
                        fecha = '{$row['real']}',
                        dosis = '{$prod['dosis']}',
                        precio = '{$prod['precio']}',
                        cantidad = '{$prod['cantidad']}',
                        ha = '{$row['has']}',
                        total = {$prod['precio']} * {$prod['cantidad']},
                        -- programa = UPPER('{$row['programa']}'),
                        programa = '{$programa}',
                        tipo_ciclo = '{$tipo_ciclo}'";
                    #$conexion->query($sql);
                    D($sql);
                }
            }
        }

        if($exist){
            $ciclo = $conexion->queryRow("SELECT * FROM ciclos_aplicacion_hist WHERE finca = '{$row['finca']}' AND fecha_real = '{$row['real']}' AND (programa = '{$row['programa']}' OR programa = '{$programa}')");
            $row['costo_total'] = round($row['costo_total'], 2);
            if($row['costo_total'] != $ciclo->costo_total){
                $diff = $row['costo_total'] - $ciclo->costo_total;
                D("{$row['finca']} {$row['ciclo']} {$row['costo_total']}/{$ciclo->costo_total} diff {$diff}");
                #d($ciclo);
            }
        }
    }
}

function D($data){
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

?>