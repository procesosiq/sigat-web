<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

include '../controllers/conexion.php';
include 'simplexlsx.class.php';

$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    $pos1 = strpos($object->getPath(), "fincas/user_");

    if($pos1 !== false){
        $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
        if('xlsx' == $ext){
            switch ($ext){
                case 'xlsx':
                    $ruta = pathinfo($object->getPathName(), PATHINFO_DIRNAME);
                    $carpetas = explode("/", $ruta);
                    $num_dir = count($carpetas)-1;
                    $id_usuario = after ('user_', $carpetas[$num_dir]);
                    $filename = "fincas/user_".$id_usuario."/".pathinfo($object->getPathName(), PATHINFO_BASENAME);
                    procesar_xlsx2($filename, $id_usuario);
                    #eliminar_archivo($filename);
                    break;

                default:
                    break;
            }
        }
    }
}


function eliminar_archivo($ruta){
    unlink($ruta);
}

function procesar_xlsx($file_name, $id_usuario){
    $xlsx = new SimpleXLSX($file_name);
    $conexion = new M_Conexion;

    $registro_tres_metros = 0;
    $registro_cero_semanas = 0;
    $registro_once_semanas = 0;

    $sheets = $xlsx->sheetsCount();

    for ($x = 1; $x <= $sheets; $x++) {
        foreach ($xlsx->rows($x) as $key => $fila) {
            foreach($fila  as $key2 => $celda){
                if($key == 1){
                    $cabezales[] = $celda;
                }
                else if($key > 2){
                    if($key2 >= 0 && $key2 <= 9){ #3metros
                        #if($celda != ""){
                            if($key2 == 0){
                                $tres_metros[$registro_tres_metros]["semana"] = $celda;
                            }
                            if($key2 == 1){
                                $tres_metros[$registro_tres_metros]["foco"] = ucwords(strtolower_utf8($celda));
                            }
                            if($key2 == 2){
                                $tres_metros[$registro_tres_metros]["lote"] = strtolower_utf8($celda);
                                #echo $xlsx->sheetName($x)."-".$celda."<br>";
                            }
                            if($key2 == 3){
                                $tres_metros[$registro_tres_metros]["muestra"] = $celda;
                            }
                            if($key2 == 4){
                                $celda = trim($celda);
                                $tres_metros[$registro_tres_metros]["hoja3"] = ((!empty($celda))?$celda:0);
                            }
                            if($key2 == 5){
                                $celda = trim($celda);
                                $tres_metros[$registro_tres_metros]["hoja4"] = ((!empty($celda))?$celda:0);
                            }
                            if($key2 == 6){
                                $celda = trim($celda);
                                $tres_metros[$registro_tres_metros]["hoja5"] = ((!empty($celda))?$celda:0);
                            }
                            if($key2 == 7){
                                $celda = trim($celda);
                                $tres_metros[$registro_tres_metros]["hojas_totales"] = ((!empty($celda))?$celda:-1);;
                            }
                            if($key2 == 8){
                                $celda = trim($celda);
                                $tres_metros[$registro_tres_metros]["hvle"] = ((!empty($celda))?$celda:-1);
                            }
                            if($key2 == 9){
                                $celda = trim($celda);
                                $tres_metros[$registro_tres_metros]["hvlq_menor"] = ((!empty($celda))?$celda:-1);

                                $tres_metros[$registro_tres_metros]["cliente"] = $xlsx->sheetName($x);

                                $sql_hacienda = "SELECT id, id_cliente FROM cat_haciendas WHERE nombre LIKE '%".$tres_metros[$registro_tres_metros]["cliente"]."%'";
                                $result_hacienda = $conexion->Consultas(2, $sql_hacienda);

                                if(count($result_hacienda) > 0){
                                    $tres_metros[$registro_tres_metros]["id_finca"] = $result_hacienda[0]["id"];
                                    $tres_metros[$registro_tres_metros]["id_cliente"] = $result_hacienda[0]["id_cliente"];
                                }

                                $registro_tres_metros++;
                            }
                        #}
                    }
                    #espacio 10
                    if($key2 > 10 && $key2 <= 19){ #0semanas
                        #if($celda != ""){
                            if($key2 == 11){
                                $cero_semanas[$registro_cero_semanas]["semana"] = $celda;
                            }
                            if($key2 == 12){
                                $cero_semanas[$registro_cero_semanas]["foco"] = ucwords(strtolower_utf8($celda));
                            }
                            if($key2 == 13){
                                $cero_semanas[$registro_cero_semanas]["lote"] = strtolower_utf8($celda);
                            }
                            if($key2 == 14){
                                $cero_semanas[$registro_cero_semanas]["muestra"] = $celda;
                            }
                            if($key2 == 15){
                                $cero_semanas[$registro_cero_semanas]["hojas_totales"] = $celda;
                            }
                            if($key2 == 16){
                                $cero_semanas[$registro_cero_semanas]["hvle"] = $celda;
                            }
                            if($key2 == 17){
                                $cero_semanas[$registro_cero_semanas]["hvlq_menor"] = $celda;
                            }
                            if($key2 == 18){
                                $cero_semanas[$registro_cero_semanas]["hvlq_mayor"] = $celda;
                            }
                            if($key2 == 19){
                                $cero_semanas[$registro_cero_semanas]["lc"] = $celda;

                                $cero_semanas[$registro_cero_semanas]["cliente"] = $xlsx->sheetName($x);

                                $sql_hacienda = "SELECT id, id_cliente FROM cat_haciendas WHERE nombre LIKE '%".$cero_semanas[$registro_cero_semanas]["cliente"]."%'";
                                $result_hacienda = $conexion->Consultas(2, $sql_hacienda);

                                if(count($result_hacienda) > 0){
                                    $cero_semanas[$registro_cero_semanas]["id_finca"] = $result_hacienda[0]["id"];
                                    $cero_semanas[$registro_cero_semanas]["id_cliente"] = $result_hacienda[0]["id_cliente"];
                                }

                                $registro_cero_semanas++;
                            }
                        #}
                    }
                    #espacio 20
                    if($key2 > 20){ #once semanas
                        if($celda != ""){
                            if($key2 == 21){
                                $once_semanas[$registro_once_semanas]["semana"] = $celda;
                            }
                            if($key2 == 22){
                                $once_semanas[$registro_once_semanas]["foco"] = ucwords(strtolower_utf8($celda));
                            }
                            if($key2 == 23){
                                $once_semanas[$registro_once_semanas]["lote"] = strtolower_utf8($celda);
                            }
                            if($key2 == 24){
                                $once_semanas[$registro_once_semanas]["muestra"] = $celda;
                            }
                            if($key2 == 25){
                                $once_semanas[$registro_once_semanas]["hojas_totales"] = $celda;
                            }
                            if($key2 == 26){
                                $once_semanas[$registro_once_semanas]["hvlq_menor"] = $celda;
                            }
                            if($key2 == 27){
                                $once_semanas[$registro_once_semanas]["hvlq_mayor"] = $celda;
                            }
                            if($key2 == 28){
                                $once_semanas[$registro_once_semanas]["lc"] = $celda;

                                $once_semanas[$registro_once_semanas]["cliente"] = $xlsx->sheetName($x);

                                $sql_hacienda = "SELECT id, id_cliente FROM cat_haciendas WHERE nombre LIKE '%".$once_semanas[$registro_once_semanas]["cliente"]."%'";
                                $result_hacienda = $conexion->Consultas(2, $sql_hacienda);

                                if(count($result_hacienda) > 0){
                                    $once_semanas[$registro_once_semanas]["id_finca"] = $result_hacienda[0]["id"];
                                    $once_semanas[$registro_once_semanas]["id_cliente"] = $result_hacienda[0]["id_cliente"];
                                }

                                $registro_once_semanas++;
                            }
                        }
                    }
                }
            }
        }
    }

    $sem = 0;
    foreach($tres_metros as $registro){

        /*----------------INFO PRINCIPAL----------------*/
    
        if($registro["semana"]!="" && isset($registro["semana"])){
            if($sem != $registro["semana"]){
                $sql_principal = "INSERT INTO muestras_haciendas_3M
                    (
                        id_usuario,
                        id_cliente,
                        id_hacienda,
                        fecha
                    )
                     VALUES
                    (
                        {$id_usuario},
                        '".(isset($registro["id_cliente"]) ? $registro["id_cliente"] : "")."',
                        '".(isset($registro["id_finca"]) ? $registro["id_finca"] : "")."',
                        STR_TO_DATE('2016".$registro["semana"]." Monday', '%X%V %W')
                    )
                ";

                #echo $sql_principal."<br>";
                $sem = $registro["semana"];

                $id_principal = $conexion->Consultas(1, $sql_principal);
            }
        }

        /*--------------AGRUPACIONES--------------*/
        
        $sql_focos = "SELECT id FROM cat_agrupaciones WHERE nombre LIKE '%".$registro["foco"]."%' AND id_cliente = ".$registro["id_cliente"]." AND id_usuario = {$id_usuario} AND id_hacienda = ".$registro["id_finca"].";";
        $result_focos = $conexion->Consultas(2, $sql_focos);

        if(count($result_focos) == 0)
            $registro["id_foco"] = $conexion->Consultas(1, "INSERT INTO cat_agrupaciones(nombre, id_hacienda, id_cliente, id_usuario) VALUES('".$registro["foco"]."', ".$registro["id_finca"].",".$registro["id_cliente"].",'{$id_usuario}');");
        else
            $registro["id_foco"] = $result_focos[0]["id"];

        /*----------------LOTES------------------*/
        
        $sql_lote = "SELECT id FROM cat_lotes WHERE nombre LIKE '%".$registro["lote"]."%' AND id_hacienda = ".$registro["id_finca"].";";
        $result_lote = $conexion->Consultas(2, $sql_lote);

        if(count($result_lote)>0){
            $registro["id_lote"] = $result_lote[0]["id"];
        }else{
            $sql_insert_lote = "INSERT INTO cat_lotes
                (
                    id_hacienda,
                    id_agrupacion,
                    nombre,
                    id_usuario,
                    id_cliente
                )
                VALUES(
                    ".$registro["id_finca"].",
                    ".$registro["id_foco"].",
                    '".$registro["lote"]."',
                    {$id_usuario},
                    ".$registro["id_cliente"]."
                );";

            echo $sql_insert_lote."<br><br>";
            $registro["id_lote"] = $conexion->Consultas(1, $sql_insert_lote);
        }
    

        /*---------------INFO DETALLE-----------------*/
        
        if($registro["semana"] != "" && ($registro["hojas_totales"]!=""||$registro["hvle"]!=""||$registro["hvlq_menor"]!=""||$registro["hoja3"]!=0||$registro["hoja4"]!=0||$registro["hoja5"]!=0)){
            $sql = "INSERT INTO muestras_hacienda_detalle_3M
            (
                id_usuario,
                id_hacienda,
                id_cliente,
                id_Mhacienda,
                id_lote,
                lote,
                foco,
                hojas_totales,
                hoja_mas_vieja_de_estrias,
                hoja_mas_vieja_libre_quema_menor,
                total_hoja_3,
                total_hoja_4,
                total_hoja_5,
                fecha
            )
             VALUES
            (
                {$id_usuario},
                '".(isset($registro["id_finca"]) ? $registro["id_finca"] : "")."',
                '".(isset($registro["id_cliente"]) ? $registro["id_cliente"] : "")."',
                '".$id_principal."',
                ".$registro["id_lote"].",
                '".(isset($registro["lote"]) ? $registro["lote"] : "")."',
                '".(isset($registro["foco"]) ? $registro["foco"] : "")."',
                '".(isset($registro["hojas_totales"]) ? $registro["hojas_totales"] : "")."',
                '".(isset($registro["hvle"]) ? $registro["hvle"] : "")."',
                '".(isset($registro["hvlq_menor"]) ? $registro["hvlq_menor"] : "")."',
                ".(isset($registro["hoja3"]) ? $registro["hoja3"] : '-1').",
                ".(isset($registro["hoja4"]) ? $registro["hoja4"] : '-1').",
                ".(isset($registro["hoja5"]) ? $registro["hoja5"] : '-1').",
                STR_TO_DATE('2016".$registro["semana"]." Monday', '%X%V %W')
            );";

            echo $sql."<br><br>";
            $conexion->Consultas(1, $sql);
        }
    }
    $sem = 0;
    foreach($cero_semanas as $registro){

        /*------------INFO PRINCIPAL-------------*/
        if($registro["semana"] != "" && isset($registro["semana"])){
            if($sem != $registro["semana"]){
                $sql_principal = "INSERT INTO muestras_haciendas
                    (
                        id_usuario,
                        id_cliente,
                        id_hacienda,
                        tipo_semana,
                        fecha
                    )
                     VALUES
                    (
                        {$id_usuario},
                        '".(isset($registro["id_cliente"]) ? $registro["id_cliente"] : "")."',
                        '".(isset($registro["id_finca"]) ? $registro["id_finca"] : "")."',
                        0,
                        STR_TO_DATE('2016".$registro["semana"]." Monday', '%X%V %W')
                    )
                ";

                $sem = $registro["semana"];

                $id_principal = $conexion->Consultas(1, $sql_principal);
            }
        }

        /*--------------AGRUPACIONES--------------*/
        
        $sql_focos = "SELECT id FROM cat_agrupaciones WHERE nombre LIKE '%".$registro["foco"]."%' AND id_cliente = ".$registro["id_cliente"]." AND id_usuario = {$id_usuario} AND id_hacienda = ".$registro["id_finca"].";";
        $result_focos = $conexion->Consultas(2, $sql_focos);

        if(count($result_focos) == 0)
            $registro["id_foco"] = $conexion->Consultas(1, "INSERT INTO cat_agrupaciones(nombre, id_hacienda, id_cliente, id_usuario) VALUES('".$registro["foco"]."', ".$registro["id_finca"].",".$registro["id_cliente"].",'{$id_usuario}');");
        else
            $registro["id_foco"] = $result_focos[0]["id"];

        /*----------------LOTES------------------*/
        
        $sql_lote = "SELECT id FROM cat_lotes WHERE nombre LIKE '%".$registro["lote"]."%' AND id_hacienda = ".$registro["id_finca"].";";
        $result_lote = $conexion->Consultas(2, $sql_lote);

        if(count($result_lote)>0){
            $registro["id_lote"] = $result_lote[0]["id"];
        }else{
            $sql_insert_lote = "INSERT INTO cat_lotes
                (
                    id_hacienda,
                    id_agrupacion,
                    nombre,
                    id_usuario,
                    id_cliente
                )
                VALUES(
                    ".$registro["id_finca"].",
                    ".$registro["id_foco"].",
                    '".$registro["lote"]."',
                    {$id_usuario},
                    ".$registro["id_cliente"]."
                );";

            #echo $sql_insert_lote."<br><br>";
            $registro["id_lote"] = $conexion->Consultas(1, $sql_insert_lote);
        }
    

        /*-----------------INFO DETALLE---------------*/
        
        if($registro["hojas_totales"]!=""||$registro["hvle"]!=""||$registro["hvlq_menor"]!=""||$registro["hvlq_mayor"]!=""||$registro["lc"]!=""){
            $sql = "INSERT INTO muestras_hacienda_detalle
            (
                id_usuario,
                id_hacienda,
                id_cliente,
                id_Mhacienda,
                id_lote,
                lote,
                foco,
                hojas_totales,
                hojas_mas_vieja_libre,
                hoja_mas_vieja_libre_quema_menor,
                hoja_mas_vieja_libre_quema_mayor,
                libre_cirugias,
                tipo_semana,
                fecha
            )
             VALUES
            (
                $id_usuario,
                '".(isset($registro["id_finca"]) ? $registro["id_finca"] : "")."',
                '".(isset($registro["id_cliente"]) ? $registro["id_cliente"] : "")."',
                '".$id_principal."',
                ".$registro["id_lote"].",
                '".(isset($registro["lote"]) ? $registro["lote"] : "")."',
                '".(isset($registro["foco"]) ? $registro["foco"] : "")."',
                '".(isset($registro["hojas_totales"]) ? $registro["hojas_totales"] : "")."',
                '".(isset($registro["hvle"]) ? $registro["hvle"] : "")."',
                '".(isset($registro["hvlq_menor"]) ? $registro["hvlq_menor"] : "")."',
                '".(isset($registro["hvlq_mayor"]) ? $registro["hvlq_mayor"] : "")."',
                '".(isset($registro["lc"]) ? $registro["lc"] : "")."',
                0,
                STR_TO_DATE('2016".$registro["semana"]." Monday', '%X%V %W')
            );";
            
            echo $sql."<br><br>";
            $conexion->Consultas(1, $sql);
        }
    }

    $sem = 0;
    foreach($once_semanas as $registro){
        /*------------INFO PRINCIPAL-------------*/
        
        if($registro["semana"] != "" && isset($registro["semana"])){
            if($registro["semana"] != $sem){
                $sql_principal = "INSERT INTO muestras_haciendas
                    (
                        id_usuario,
                        id_cliente,
                        id_hacienda,
                        tipo_semana,
                        fecha
                    )
                     VALUES
                    (
                        {$id_usuario},
                        '".(isset($registro["id_cliente"]) ? $registro["id_cliente"] : "")."',
                        '".(isset($registro["id_finca"]) ? $registro["id_finca"] : "")."',
                        11,
                        STR_TO_DATE('2016".$registro["semana"]." Monday', '%X%V %W')
                    )
                ";

                $sem = $registro["semana"];

                $id_principal = $conexion->Consultas(1, $sql_principal);
                #echo $sql_principal;
            }
        }

        /*--------------AGRUPACIONES--------------*/
        
        $sql_focos = "SELECT id FROM cat_agrupaciones WHERE nombre LIKE '%".$registro["foco"]."%' AND id_cliente = ".$registro["id_cliente"]." AND id_usuario = {$id_usuario} AND id_hacienda = ".$registro["id_finca"].";";
        $result_focos = $conexion->Consultas(2, $sql_focos);

        if(count($result_focos) == 0)
            $registro["id_foco"] = $conexion->Consultas(1, "INSERT INTO cat_agrupaciones(nombre, id_hacienda, id_cliente, id_usuario) VALUES('".$registro["foco"]."', ".$registro["id_finca"].",".$registro["id_cliente"].",'{$id_usuario}');");
        else
            $registro["id_foco"] = $result_focos[0]["id"];

        /*----------------LOTES------------------*/
        
        $sql_lote = "SELECT id FROM cat_lotes WHERE nombre LIKE '%".$registro["lote"]."%' AND id_hacienda = ".$registro["id_finca"].";";
        $result_lote = $conexion->Consultas(2, $sql_lote);

        if(count($result_lote)>0){
            $registro["id_lote"] = $result_lote[0]["id"];
        }else{
            $sql_insert_lote = "INSERT INTO cat_lotes
                (
                    id_hacienda,
                    id_agrupacion,
                    nombre,
                    id_usuario,
                    id_cliente
                )
                VALUES(
                    ".$registro["id_finca"].",
                    ".$registro["id_foco"].",
                    '".$registro["lote"]."',
                    {$id_usuario},
                    ".$registro["id_cliente"]."
                );";

            #echo $sql_insert_lote."<br><br>";
            $registro["id_lote"] = $conexion->Consultas(1, $sql_insert_lote);
        }
    

        /*------------INFO DETALLE------------*/
        
        if($registro["hojas_totales"]!=""||$registro["hvle"]!=""||$registro["hvlq_menor"]!=""||$registro["lc"]!=""){
            $sql = "INSERT INTO muestras_hacienda_detalle
            (
                id_usuario,
                id_hacienda,
                id_cliente,
                id_Mhacienda,
                id_lote,
                lote,
                foco,
                hojas_totales,
                hoja_mas_vieja_libre_quema_menor,
                hoja_mas_vieja_libre_quema_mayor,
                libre_cirugias,
                tipo_semana,
                fecha
            )
             VALUES
            (
                $id_usuario,
                '".(isset($registro["id_finca"]) ? $registro["id_finca"] : "")."',
                '".(isset($registro["id_cliente"]) ? $registro["id_cliente"] : "")."',
                '".$id_principal."',
                ".$registro["id_lote"].",
                '".(isset($registro["lote"]) ? $registro["lote"] : "")."',
                '".(isset($registro["foco"]) ? $registro["foco"] : "")."',
                '".(isset($registro["hojas_totales"]) ? $registro["hojas_totales"] : "")."',
                '".(isset($registro["hvlq_menor"]) ? $registro["hvlq_menor"] : "")."',
                '".(isset($registro["hvlq_mayor"]) ? $registro["hvlq_mayor"] : "")."',
                '".(isset($registro["lc"]) ? $registro["lc"] : "")."',
                11,
                STR_TO_DATE('2016".$registro["semana"]." Monday', '%X%V %W')
            );";
            
            echo $sql."<br><br>";
            $conexion->Consultas(1, $sql);
        }
    }
}

function procesar_xlsx2($file_name, $id_usuario){
    $xlsx = new SimpleXLSX($file_name);
    $conexion = new M_Conexion;

    $registro_tres_metros = 0;
    $registro_cero_semanas = 0;
    $registro_once_semanas = 0;

    $sheets = $xlsx->sheetsCount();

    for ($x = 1; $x <= $sheets; $x++) {
        foreach ($xlsx->rows($x) as $key => $fila) {
            if($key > 0){
                #muestras 3m
                $sql = "INSERT INTO muestras_haciendas_3M SET 
                            id_cliente = $fila[0],
                            id_hacienda = $fila[1],
                            fecha_insert = CURRENT_TIMESTAMP,
                            fecha = STR_TO_DATE('2015{$fila[2]} Monday', '%X%V %W'),
                            id_usuario = 1";

                D($sql);
                $id_principal = $conexion->Consultas(1, $sql);

                $sql = "INSERT INTO muestras_hacienda_detalle_3M SET
                            id_Mhacienda = $id_principal,
                            id_hacienda = $fila[1],
                            id_usuario = 1,
                            id_cliente = $fila[0],
                            hojas_totales = '".round($fila[6], 1)."',
                            hoja_mas_vieja_de_estrias = '".round($fila[7], 1)."',
                            hoja_mas_vieja_libre_quema_menor = '".round($fila[8], 1)."',
                            total_hoja_3 = '".round($fila[3], 1)."',
                            total_hoja_4 = '".round($fila[4], 1)."',
                            total_hoja_5 = '".round($fila[5], 1)."',
                            fecha = STR_TO_DATE('2015{$fila[2]} Monday', '%X%V %W')";
                D($sql);
                $conexion->Consultas(1, $sql);

                $sql = "INSERT INTO muestras_haciendas SET 
                            id_cliente = $fila[0],
                            id_hacienda = $fila[1],
                            fecha = STR_TO_DATE('2015{$fila[2]} Monday', '%X%V %W'),
                            id_usuario = 1,
                            tipo_semana = 0";

                D($sql);
                $id_principal_2 = $conexion->Consultas(1, $sql);

                $sql = "INSERT INTO muestras_hacienda_detalle SET 
                            id_Mhacienda = $id_principal_2,
                            id_hacienda = $fila[1],
                            id_usuario = 1,
                            id_cliente = $fila[0],
                            hojas_totales = '".round($fila[9], 1)."',
                            hojas_mas_vieja_libre = '".round($fila[10], 1)."',
                            hoja_mas_vieja_libre_quema_menor = '".round($fila[11], 1)."',
                            hoja_mas_vieja_libre_quema_mayor = '".round($fila[12], 1)."',
                            libre_cirugias = '".round($fila[13], 1)."',
                            tipo_semana = 0,
                            fecha = STR_TO_DATE('2015{$fila[2]} Monday', '%X%V %W')";
                D($sql);
                $conexion->Consultas(1, $sql);

                $sql = "INSERT INTO muestras_haciendas SET 
                            id_cliente = 11,
                            id_hacienda = $fila[1],
                            fecha = STR_TO_DATE('2015{$fila[2]} Monday', '%X%V %W'),
                            id_usuario = 1,
                            tipo_semana = 11";

                D($sql);
                $id_principal_2 = $conexion->Consultas(1, $sql);

                $sql = "INSERT INTO muestras_hacienda_detalle SET 
                            id_Mhacienda = $id_principal_2,
                            id_hacienda = $fila[1],
                            id_usuario = 1,
                            id_cliente = $fila[0],
                            hojas_totales = '".round($fila[14], 1)."',
                            hojas_mas_vieja_libre = '".round($fila[15], 1)."',
                            hoja_mas_vieja_libre_quema_menor = '".round($fila[16], 1)."',
                            hoja_mas_vieja_libre_quema_mayor = '".round($fila[17], 1)."',
                            libre_cirugias = '".round($fila[18], 1)."',
                            tipo_semana = 11,
                            fecha = STR_TO_DATE('2015{$fila[2]} Monday', '%X%V %W')";
                D($sql);
                $conexion->Consultas(1, $sql);
            }
        }
    }
}

function after ($this, $inthat)
    {
        if (!is_bool(strpos($inthat, $this)))
        return substr($inthat, strpos($inthat,$this)+strlen($this));
    };

function strtolower_utf8($string){ 
  $convert_to = array( 
    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", 
    "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", 
    "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж", 
    "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", 
    "ь", "э", "ю", "я" 
  ); 
  $convert_from = array( 
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", 
    "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", 
    "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", 
    "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ", 
    "Ь", "Э", "Ю", "Я" 
  ); 

  return str_replace($convert_from, $convert_to, $string); 
}

function D($data){
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}
?>