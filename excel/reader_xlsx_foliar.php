<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

include '../controllers/conexion.php';
include 'simplexlsx.class.php';

$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    $pos1 = strpos($object->getPath(), "foliar/user_");

    if($pos1 !== false){
        $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
        if('xlsx' == $ext){
            switch ($ext){
                case 'xlsx':
                    $ruta = pathinfo($object->getPathName(), PATHINFO_DIRNAME);
                    $carpetas = explode("/", $ruta);
                    $num_dir = count($carpetas)-1;
                    $id_usuario = after ('user_', $carpetas[$num_dir]);
                    $filename = "foliar/user_".$id_usuario."/".pathinfo($object->getPathName(), PATHINFO_BASENAME);
                    procesar_xlsx($filename, $id_usuario);
                    eliminar_archivo($filename);
                    break;

                default:
                    break;
            }
        }
    }
}

function eliminar_archivo($ruta){
    unlink($ruta);
}

function procesar_xlsx($file_name, $id_usuario){
    $xlsx = new SimpleXLSX($file_name);
    $conexion = new M_Conexion;

    $registro = 1;
    $muestra = 1;
    $sheets = $xlsx->sheetsCount();
    for($x = 1; $x <= $sheets; $x++){
        foreach ($xlsx->rows($x) as $key => $fila) {
            foreach($fila  as $key2 => $celda){
                if($key == 0){
                    $cabezales[] = $celda;
                }
                else{
                    if($key2 == 0){
                        $datos[$registro]["anio"] = $celda;
                    }
                    else if($key2 == 1){
                        $ts = ($celda - 25569)*86400;
                        $datos[$registro]["fecha"] = date('Y-m-d', $ts);
                    }
                    else if($key2 == 2){
                        $datos[$registro]["mes"] = $celda;
                    }
                    else if($key2 == 3){
                        $datos[$registro]["sem"] = $celda;
                    }
                    else if($key2 == 4){
                        $datos[$registro]["per"] = $celda;
                    }
                    else if($key2 == 5){
                        $cliente = trim($celda);
                        if($cliente == "MARCEL LANIADO")
                            $datos[$registro]["id_cliente"] = 9;
                        if($cliente == "MANUEL MARUN")
                            $datos[$registro]["id_cliente"] = 10;
                        if($cliente == "REISET")
                            $datos[$registro]["id_cliente"] = 11;
                        if($cliente == "CASTRO")
                            $datos[$registro]["id_cliente"] = 12;
                    }
                    else if($key2 == 6){
                        $datos[$registro]["productor"] = $celda;
                    }
                    else if($key2 == 7){
                        $finca = trim($celda);
                        if($finca == "NUEVA PUBENZA")
                            $datos[$registro]["id_finca"] = 14;
                        if($finca == "SAN JOSE 2")
                            $datos[$registro]["id_finca"] = 15;
                        if($finca == "SAN JOSE 1")
                            $datos[$registro]["id_finca"] = 16;
                        if($finca == "GUARUMAL")
                            $datos[$registro]["id_finca"] = 17;
                        if($finca == "OCHOA")
                            $datos[$registro]["id_finca"] = 18;
                    }
                    else if($key2 == 8){
                        if($celda == ""){
                            if($muestra == 10)
                                $muestra = 1;
                            else
                                $muestra++;
                        }
                        else if($celda == 1)
                            $muestra = 1;
                        else if($celda == 2)
                            $muestra = 2;
                        else if($celda == 3)
                            $muestra = 3;
                        else if($celda == 4)
                            $muestra = 4;
                        else if($celda == 5)
                            $muestra = 5;
                        else if($celda == 6)
                            $muestra = 6;
                        else if($celda == 7)
                            $muestra = 7;
                        else if($celda == 8)
                            $muestra = 8;
                        else if($celda == 9)
                            $muestra = 9;
                        else if($celda == 10){
                            $muestra = 10;
                        }
                    }
                    else if($key2 == 9){
                        $datos[$registro]["lote"] = $celda;
                    }
                    else if($key2 == 10){
                        $datos[$registro]["foco"] = $celda;
                    }
                    else if($key2 == 11){
                        $datos[$registro]["edad"] = $celda;
                    }
                    else if($key2 == 12){
                        if($muestra == 1)
                            $datos[$registro]["ef1"] = (($celda!="")?$celda:-1);
                        if($muestra == 2)
                            $datos[$registro]["ef2"] = (($celda!="")?$celda:-1);
                        if($muestra == 3)
                            $datos[$registro]["ef3"] = (($celda!="")?$celda:-1);
                        if($muestra == 4)
                            $datos[$registro]["ef4"] = (($celda!="")?$celda:-1);
                        if($muestra == 5)
                            $datos[$registro]["ef5"] = (($celda!="")?$celda:-1);
                        if($muestra == 6)
                            $datos[$registro]["ef6"] = (($celda!="")?$celda:-1);
                        if($muestra == 7)
                            $datos[$registro]["ef7"] = (($celda!="")?$celda:-1);
                        if($muestra == 8)
                            $datos[$registro]["ef8"] = (($celda!="")?$celda:-1);
                        if($muestra == 9)
                            $datos[$registro]["ef9"] = (($celda!="")?$celda:-1);
                        if($muestra == 10){
                            $datos[$registro]["ef10"] = (($celda!="")?$celda:-1);
                        }
                    }
                    else if($key2 == 13){
                        $datos[$registro]["fecha_ef"] = $celda;
                    }
                    else if($key2 == 14){
                        $datos[$registro]["ef_2"] = $celda;
                    }
                    else if($key2 == 15){
                        $datos[$registro]["dif_dias_ef"] = $celda;
                    }
                    else if($key2 == 16){
                        $datos[$registro]["efac_afpa"] = $celda;
                    }
                    else if($key2 == 17){
                        $datos[$registro]["ef_real"] = $celda;
                        if($muestra == 10)
                            $registro++;
                    }
                }
            }
        }
    }


    foreach($datos as $registro){

        $sql = "INSERT INTO foliar
        (
            fecha,
            id_usuario,
            id_cliente,
            id_hacienda,
            foco,
            emision1,
            emision2,
            emision3,
            emision4,
            emision5,
            emision6,
            emision7,
            emision8,
            emision9,
            emision10
        )
         VALUES
        (
            '".$registro["fecha"]."',
            {$id_usuario},
            '".(isset($registro["id_cliente"]) ?  $registro["id_cliente"] : "")."',
            '".(isset($registro["id_finca"]) ? $registro["id_finca"] : "")."',
            '".(isset($registro["foco"]) ? $registro["foco"] : "")."',
            '".(isset($registro["ef1"]) ? $registro["ef1"] : -1)."',
            '".(isset($registro["ef2"]) ? $registro["ef2"] : -1)."',
            '".(isset($registro["ef3"]) ? $registro["ef3"] : -1)."',
            '".(isset($registro["ef4"]) ? $registro["ef4"] : -1)."',
            '".(isset($registro["ef5"]) ? $registro["ef5"] : -1)."',
            '".(isset($registro["ef6"]) ? $registro["ef6"] : -1)."',
            '".(isset($registro["ef7"]) ? $registro["ef7"] : -1)."',
            '".(isset($registro["ef8"]) ? $registro["ef8"] : -1)."',
            '".(isset($registro["ef9"]) ? $registro["ef9"] : -1)."',
            '".(isset($registro["ef10"]) ? $registro["ef10"] : -1)."'
        );";

        #$conexion->Consultas(1, $sql);
        echo $sql."<br><br>";
    }

}

function after ($this, $inthat)
    {
        if (!is_bool(strpos($inthat, $this)))
        return substr($inthat, strpos($inthat,$this)+strlen($this));
    };

?>