<?php
// header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
/*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/

ini_set('max_execution_time', 300);
ini_set('memory_limit', '-1');

/*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/

error_reporting(E_ALL);


include '../controllers/conexion.php';
include 'simplexlsx.class.php';

$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName()){
        $pos1 = strpos($object->getPathName(), "fumigacion/");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('xlsx' == $ext){
                switch ($ext){

                    case 'xlsx':
                        try {
                            $ruta = pathinfo($object->getPathName(), PATHINFO_DIRNAME);
                            $carpetas = explode("/", $ruta);
                            $num_dir = count($carpetas)-1;
                            $id_usuario = after('user_', $carpetas[$num_dir]);
                            $filename = "fumigacion/user_".$id_usuario."/".pathinfo($object->getPathName(), PATHINFO_BASENAME);
                            procesar_xlsx($filename, $id_usuario);
                            #eliminar_archivo($filename);
                        } catch (Exception $e) {
                            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                        }
                        break;

                    default:
                        break;
                }
            }
        }
    }
}

function eliminar_archivo($ruta){
    unlink($ruta);
}

function procesar_xlsx($file_name, $id_usuario){
    $xlsx = new SimpleXLSX($file_name);
    $count = 0;
    $conexion = new M_Conexion;
    $clientes = $xlsx->sheetNames();
    $registros = array();
    foreach ($clientes as $x => $name) {
        $rows = $xlsx->rows($x);
        D("{$name} -----");
        foreach ($rows as $key => $fila) {
            if($key > 3){
                $finca = $fila[2];

                if(is_numeric($fila[4])){
                    $UNIX_DATE = ($fila[4] - 25569) * 86400;
                    $fecha_programada = gmdate("Y-m-d", $UNIX_DATE);
                }else $fecha_programada = '';

                if(is_numeric($fila[5])){
                    $UNIX_DATE = ($fila[5] - 25569) * 86400;
                    $fecha_real = gmdate("Y-m-d", $UNIX_DATE);
                }else $fecha_real = '';

                if(is_numeric($fila[11])){
                    $UNIX_DATE = ($fila[11] - 25569) * 86400;
                    $hora_salida = gmdate("H:m:s", $UNIX_DATE);
                }else $hora_salida = '';

                if(is_numeric($fila[12])){
                    $UNIX_DATE = ($fila[12] - 25569) * 86400;
                    $hora_llegada = gmdate("H:m:s", $UNIX_DATE);
                }else $hora_llegada = '';

                $data = $conexion->queryAll("SELECT * FROM ciclos_aplicacion_historico WHERE finca = '{$finca}' AND fecha_real = '{$fecha_real}' AND id_json = 0");
                if(count($data) == 0){
                    #D("Fila {$key} No existe {$finca} : {$fecha_real}");
                }else if(count($data) == 1){
                    $data = $data[0];
                    $row = $conexion->queryAll("SELECT * FROM resumen_ciclo_2 WHERE id_historico = $data->id");
                    if(count($row) == 0){
                        $sql = "INSERT INTO resumen_ciclo_2 SET id_historico = $data->id, finca = '{$data->finca}', piloto = '{$fila[9]}', placa = '{$fila[10]}', hora_salida = '{$hora_salida}', hora_llegada = '{$hora_llegada}', motivo_atraso = '{$fila[7]}', notificada = '{$fila[3]}', sector_aplicado = '{$fila[43]}', observaciones = '{$fila[44]}'";
                        D($sql);
                        $conexion->query($sql);
                    }
                    #D("Fila {$key} si existe {$finca} : {$fecha_real}");
                    #$row = $conexion->queryAll("SELECT * FROM resumen_ciclo_2");
                }else{
                    #D("Fila {$key} tiene mas de 1 registro");
                    #D($data);
                }
            }
        }
    }
}

function D($data){
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function after ($this, $inthat)
    {
        if (!is_bool(strpos($inthat, $this)))
        return substr($inthat, strpos($inthat,$this)+strlen($this));
    };

?>