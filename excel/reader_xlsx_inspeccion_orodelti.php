<?php
// header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
/*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/

ini_set('max_execution_time', 300);
ini_set('memory_limit', '-1');

/*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/

error_reporting(E_ALL);


include '../controllers/conexion.php';
include 'simplexlsx.class.php';

$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName()){
        $pos1 = strpos($object->getPathName(), "inspeccionOrodelti/");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('xlsx' == $ext){
                switch ($ext){

                    case 'xlsx':
                        try {
                            $ruta = pathinfo($object->getPathName(), PATHINFO_DIRNAME);
                            $carpetas = explode("/", $ruta);
                            $num_dir = count($carpetas)-1;
                            $id_usuario = after('user_', $carpetas[$num_dir]);
                            $filename = "inspeccionOrodelti/user_".$id_usuario."/".pathinfo($object->getPathName(), PATHINFO_BASENAME);
                            procesar_xlsx($filename, $id_usuario);
                            #eliminar_archivo($filename);
                        } catch (Exception $e) {
                            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                        }
                        break;

                    default:
                        break;
                }
            }
        }
    }
}

function eliminar_archivo($ruta){
    unlink($ruta);
}

function procesar_xlsx($file_name, $id_usuario){
    $xlsx = new SimpleXLSX($file_name);
    $count = 0;
    $conexion = new M_Conexion;
    $libros = $xlsx->sheetNames();
    $registros = array();

    foreach ($libros as $x => $name) {
        $rows = $xlsx->rows($x);
        foreach($rows as $key => $fila){
            if($key > 1){
                /*$sql = "INSERT INTO muestras_haciendas_orodelti SET
                            anio = {$fila[0]},
                            zona = '{$fila[1]}',
                            sector = '{$fila[2]}',
                            semana = {$fila[3]},
                            finca = '{$fila[4]}',
                            h3_3m = '{$fila[5]}',
                            h4_3m = '{$fila[6]}',
                            h5_3m = '{$fila[7]}',
                            ht_3m = '{$fila[8]}',
                            hvl_3m = '{$fila[9]}',
                            hvl_0s = '{$fila[10]}',
                            ht_0s = '{$fila[11]}',
                            ht_11s = '{$fila[12]}',
                            foliar = '{$fila[13]}'";
                $conexion->query($sql);*/
                $finca = $fila[1];
                $fecha = $fila[3];
                if(is_numeric($fecha)){
                    $UNIX_DATE = ($fecha - 25569) * 86400;
                    $fecha = gmdate("Y-m-d", $UNIX_DATE);
                }

                $sql = "INSERT INTO orodelti_plantas_muestras SET
                            id_finca = (SELECT id FROM cat_fincas WHERE nombre = '{$finca}' AND status = 1),
                            finca = '{$finca}',
                            
                            lote = '{$fila[4]}',
                            fecha = '{$fecha}',
                            3m_ht = '{$fila[6]}',
                            3m_hvle = '{$fila[15]}',
                            3m_hvle_afa = '{$fila[16]}',
                            3m_hvlq = '{$fila[17]}',
                            3m_hvlq_afa = '{$fila[18]}',

                            0s_ht = '{$fila[19]}',
                            0s_ht_afa = '{$fila[20]}',
                            0s_hvle = '{$fila[21]}',                            
                            0s_hvlq = '{$fila[22]}',
                            0s_hvlq_afa = '{$fila[23]}',
                            
                            6s_ht = '{$fila[24]}',
                            6s_ht_afa = '{$fila[25]}',
                            6s_hvle = '{$fila[26]}',
                            6s_hvlq = '{$fila[27]}',
                            6s_hvlq_afa = '{$fila[28]}',
                            
                            11s_ht = '{$fila[29]}',
                            11s_ht_afa = '{$fila[30]}',
                            11s_hvle = '{$fila[31]}',
                            11s_hvlq = '{$fila[32]}',
                            11s_hvlq_afa = '{$fila[33]}'
                            ";
                D($sql);
                $conexion->query($sql);
                $id_main = $conexion->getLastID();

                for($x = 2; $x <= 5; $x++){
                    $incidencia = $fila[7 + (($x-2) * 2)];
                    $severidad = $fila[8 + (($x-2) * 2)];

                    $sql = "INSERT INTO orodelti_plantas_muestras_hojas SET
                        id_muestra = $id_main,
                        num_hoja = $x,
                        incidencia = '{$incidencia}',
                        severidad = '{$severidad}'";
                    D($sql);
                    $conexion->query($sql);
                }
            }
        }
        break;
    }

}

function D($data){
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function F($data){
    D($data);
    die();
}

function after ($this, $inthat)
    {
        if (!is_bool(strpos($inthat, $this)))
        return substr($inthat, strpos($inthat,$this)+strlen($this));
    };

?>