<?php

include './controllers/conexion.php';

function get_clientes($data){
	$conexion = new M_Conexion();	
	$datos = array();
	$sql="SELECT id,CONCAT_WS(' ',nombre,apellidos) AS nombre FROM cat_clientes WHERE STATUS=1 ORDER BY nombre";
	$datos = $conexion->Consultas(2, $sql);
	return json_encode($datos);
}


function get_haciendas($data){
	$conexion = new M_Conexion();	
	$datos = array();
	$sql="SELECT id,nombre,id_cliente FROM cat_haciendas WHERE STATUS=1 ORDER BY nombre";
	$datos = $conexion->Consultas(2, $sql);
	return json_encode($datos);
}

function get_lotes($data){
	$conexion = new M_Conexion();	
	$datos = array();
	$sql="SELECT id,nombre,nombre,id_hacienda,id_agrupacion,id_jefecampo FROM cat_lotes ORDER BY nombre";
	$datos = $conexion->Consultas(2, $sql);
	return json_encode($datos);
}

$postdata = (object)$_REQUEST;
if($postdata->mod == 'CLI'){
	$retval = get_clientes($postdata);
	echo $retval;
}
else if($postdata->mod == 'HAC'){
	$retval = get_haciendas($postdata);
	echo $retval;
}
else if($postdata->mod == 'LOT'){
	$retval = get_lotes($postdata);
	echo $retval;
}
else if($postdata->mod == 'NOE'){
	$retval = Grafica_Tmin();
	echo $retval;
}

function Grafica_Tmin(){
	$conexion = new M_Conexion();	
	$datos = array();
	$datos = $conexion->ClimaTop();
	print_r($datos);
}

?>