<?php

include './controllers/conexion.php';

function get_clientes($data){
	$conexion = new M_Conexion();	
	$datos = array();
	$id_usuario = 1;
	if(isset($data->usuario)){
		$id_usuario = $data->usuario;
	}
	$sql="SELECT id,CONCAT_WS(' ',nombre,apellidos) AS nombre 
            FROM cat_clientes 
	        WHERE STATUS = 1 AND id_usuario = '{$id_usuario}' AND id not IN(25, 26, 30)
            ORDER BY nombre";
	$datos = $conexion->Consultas(2, $sql);
	return json_encode($datos);
}


function get_haciendas($data){
	$conexion = new M_Conexion();	
	$datos = array();
	$id_usuario = 1;
	if(isset($data->usuario)){
		$id_usuario = $data->usuario;
	}
	$sql = "SELECT id, nombre, id_cliente 
            FROM cat_haciendas 
	        WHERE STATUS = 1  AND id_usuario = '{$id_usuario}' AND id_cliente not IN(25, 26, 30)
	        ORDER BY nombre";
    $datos = $conexion->queryAll($sql);
    
    foreach($datos as $row){
        $foliar = get_foliar($row->id);
        $row->emision1 = round($foliar->emision1, 2);
        $row->emision2 = round($foliar->emision2, 2);
        $row->emision3 = round($foliar->emision3, 2);
        $row->emision4 = round($foliar->emision4, 2);
        $row->emision5 = round($foliar->emision5, 2);
        $row->emision6 = round($foliar->emision6, 2);
        $row->emision7 = round($foliar->emision7, 2);
        $row->emision8 = round($foliar->emision8, 2);
        $row->emision9 = round($foliar->emision9, 2);
        $row->emision10 = round($foliar->emision10, 2);
    }

	return json_encode($datos);
}

function get_lotes($data){
	$conexion = new M_Conexion();	
	$datos = array();
	$id_usuario = 1;
	if(isset($data->usuario)){
		$id_usuario = $data->usuario;
	}
	$sql="SELECT Lote.id, 
                Lote.nombre AS nombre,
                agrup.nombre AS agrupacion,
                Lote.id_hacienda,
                id_agrupacion,
                id_jefecampo,
                CONCAT(Lote.id_hacienda, '-', id_agrupacion) AS hac_agrup
        FROM cat_lotes as Lote
		INNER JOIN cat_agrupaciones AS agrup ON Lote.id_agrupacion = agrup.id
		WHERE Lote.status > 0 AND Lote.id_usuario = '{$id_usuario}'
		ORDER BY Lote.position DESC ,Lote.id_hacienda ,Lote.nombre";
	$datos = $conexion->Consultas(2, $sql);
	return json_encode($datos);
}

function get_foliar($id_finca){
	$conexion = new M_Conexion();	
	
	$sql = "SELECT emision1, emision2, emision3, emision4, emision5, emision6, emision7, emision8, emision9, emision10
            FROM (
                SELECT id
                FROM foliar
                WHERE id_hacienda = {$id_finca}
                ORDER BY fecha DESC
            ) AS tbl
            INNER JOIN foliar ON foliar.id = tbl.id";
	$datos = $conexion->queryRow($sql);
	return $datos;
}

$postdata = (object)$_REQUEST;
if($postdata->mod == 'CLI'){
	$retval = get_clientes($postdata);
	echo $retval;
}
else if($postdata->mod == 'HAC'){
	$retval = get_haciendas($postdata);
	echo $retval;
}
else if($postdata->mod == 'LOT'){
	$retval = get_lotes($postdata);
	echo $retval;
}
else if($postdata->mod == 'NOE'){
	$retval = Grafica_Tmin();
	echo $retval;
}else if($postdata->mod == 'FOLIAR'){
    $retval = get_foliar();
	echo $retval;
}

function Grafica_Tmin(){
	$conexion = new M_Conexion();	
	$datos = array();
	$datos = $conexion->ClimaTop();
	print_r($datos);
}

?>