<?php

function pdf($content, $filename, $stream) 
{
    require_once("./dompdf/dompdf_config.inc.php");
    /*require_once('./html2pdf/html2pdf.class.php');

    $html2pdf = new HTML2PDF('P','A4','fr');
    $html2pdf->WriteHTML($content);
    $pdf = $html2pdf->Output('mipdf.pdf');

    file_put_contents("./mipdf.pdf", file_get_contents($pdf));*/

    $dompdf = new DOMPDF();
    // $html =file_get_contents("http://sigat.procesos-iq.com/printPDF.php?page=informe_pdf");
    $dompdf->load_html($html);
    $dompdf->render();

    $pdf = $dompdf->output();
    $ruta = './mipdf.pdf';
    file_put_contents($ruta, file_get_contents($pdf));
    
    if ($stream) {
        $file_to_save = $ruta;
        //save the pdf file on the server
        file_put_contents($file_to_save, $pdf); 
        //print the pdf file to the screen for saving
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="file.pdf"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file_to_save));
        header('Accept-Ranges: bytes');
        readfile($file_to_save);
        //$dompdf->stream($file_to_save);
    } else {
        return $dompdf->output();
    }
}

$postdata = (object) json_decode(file_get_contents("php://input"));

$html = $postdata->html;

#print_r($html);
pdf($html, "mipdf", true);

?>