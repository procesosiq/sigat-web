<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    include './controllers/class.sesion.php';
    include './controllers/conexion.php';
    $session = Session::getInstance();
    $session = (object)$session;
    // echo $session->nombre;
    if(!isset($session->logged) || (int)$session->logged <= 0){
        header('Location: http://sigat.procesos-iq.com/login.php');
    }
    // print_r($session);

    // print_r($_GET);
    if(isset($_GET["page"])){
        if($_GET["page"] != ""){
            $page = $_GET["page"];
            if ($page != "logout" || $page != "index" || $page != ""){
                include 'loader.php';
            }
        }else{
            $page = "404";
        }
    }else{
        $page = "index"; 
        include 'loader.php';
    }

    if(isset($_GET["page"])){
        if($_GET["page"] == "logout"){
            Session::getInstance()->kill();
            header('Location: http://sigat.procesos-iq.com/login.php');
        }
    }

    $cdn = "http://cdn.procesos-iq.com/";
    //$html = basename($_SERVER['PHP_SELF']);
    // print_r($page);
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Sigat | Dashboard</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=$cdn?>global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?=$cdn?>global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?=$cdn?>global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        
        <link href="<?=$cdn?>layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <!-- <link href="<?=$cdn?>layouts/layout4/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color" /> -->
        <link href="<?=$cdn?>layouts/layout4/css/themes/sigat.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?=$cdn?>layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <style>
        ul.page-sidebar-menu > li > .active {
            background: #5b9bd1 !important;
        }

        .portlet.box.yellow{
            border: 1px solid #178513 !important;
        }
    
        .portlet.box.yellow>.portlet-title, .portlet.yellow, .portlet>.portlet-body.yellow {
            background-color: #178513 !important;
        }

        .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu .sub-menu > li:hover > a > i, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu .sub-menu > li.open > a > i, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu .sub-menu > li.active > a > i, .page-sidebar .page-sidebar-menu .sub-menu > li:hover > a > i, .page-sidebar .page-sidebar-menu .sub-menu > li.open > a > i, .page-sidebar .page-sidebar-menu .sub-menu > li.active > a > i {
            color: rgb(255, 255, 255) !important;
        }

        .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu .sub-menu > li:hover > a, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu .sub-menu > li.open > a, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu .sub-menu > li.active > a, .page-sidebar .page-sidebar-menu .sub-menu > li:hover > a, .page-sidebar .page-sidebar-menu .sub-menu > li.open > a, .page-sidebar .page-sidebar-menu .sub-menu > li.active > a{
            background: #178513 !important;
        }
    </style>
    <body class="page-container-bg-solid page-header-fixed  page-sidebar-closed-hide-logo">
        <?php include ("./header.php"); ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php include ("./menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
            <?php include ("./views/{$page}.php"); ?>
            </div>
            <!-- END CONTENT -->
            <?php
                if($page == "index"){
                    include ("./views/sidebar.php");
                }
            ?>
        </div>
        <!-- END CONTAINER -->
        <?php include("./footer.php");?>
        <!--[if lt IE 9]>
<script src="<?=$cdn?>global/plugins/respond.min.js"></script>
<script src="<?=$cdn?>global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?=$cdn?>global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=$cdn?>global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <!-- <script src="<?=$cdn?>global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script> -->
        <!-- END PAGE LEVEL PLUGINS -->
        <?php
            $file_js_php = "./views/{$page}.js.php";
            if (file_exists($file_js_php)){
                include ($file_js_php);
            }

        ?>
        <script src="http://cdn.rawgit.com/MrRio/jsPDF/master/dist/jspdf.min.js"></script>
        <script src="http://cdn.rawgit.com/niklasvh/html2canvas/0.5.0-alpha2/dist/html2canvas.min.js"></script>
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=$cdn?>global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=$cdn?>pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js"></script> -->
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?=$cdn?>layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!--<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular.min.js"></script>-->
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
        <script src="../views/js/main.js" type="text/javascript"></script>
        <script src="js/directives.js" type="text/javascript"></script>
        <script src="js/echarts.min.js" type="text/javascript"></script>
        <script src="js/charts.js" type="text/javascript"></script>
        <!-- Add ReactJS -->
        <script src="//unpkg.com/react@15/dist/react.min.js"></script>
        <script src="//unpkg.com/react-dom@15/dist/react-dom.min.js"></script>
        <script src="componentes/gallery.js?<?=rand()?>" type="text/javascript"></script>
        <script src="componentes/charts.js?<?=rand()?>" type="text/javascript"></script>
        <script src="componentes/table.js?<?=rand()?>" type="text/javascript"></script>
        <script src="componentes/tags.js?<?=rand()?>" type="text/javascript"></script>
        <!-- Add ReactJS -->
        <script>
            <?php
                $file_js = "./views/js/{$page}.js";
                if (file_exists($file_js)){
                    include ($file_js);
                }
            ?>
        </script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>