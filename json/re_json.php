<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

include '../controllers/conexion.php';

/** Directorio de los JSON */
$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        $pos1 = strpos($object->getPath(), "re-procesar");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        // print "Hola";
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        process_json($json, $object->getFileName());
                        // move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        // move_image($object->getPathName(), $object->getFileName());
                        break;

                    default:
                        
                        break;
                }
            }
        }
    }
}

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/procesado//json/$nameFile")){
        echo 'error';
    }
    /*if(!rename($file, __DIR__."/nuevo/semanas3/$nameFile")){
        echo 'error';
    }*/
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/procesado/image/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename){
    $conexion = new M_Conexion;

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';;
    $user_username = $user['username'].'';;
    $referenceNumber = $json["referenceNumber"];
    $pages = $json['pages'];

    $geoStamp = $json["geoStamp"];
    $coordinates = $geoStamp["coordinates"];
    $latitude = $coordinates["latitude"];
    $longitude = $coordinates["longitude"];

    $foliar = 0;
    $tres_metros = false;
    $cero_semanas = false;
    $once_semanas = false; 

    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = trim($page_data['name']);
        $answers = $page_data["answers"];

        foreach($answers as $x => $answer){

            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];

            $labelita = limpiar(trim(strtolower($label)));
            if($pagina_nombre == "ENCABEZADO"){
                if($question == "SUPERVISOR"){
                    $supervisor = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                } 
            }

            if(isset($answer["question"])){
                if($answer["question"] == "CLIENTE"){
                    $cliente = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '(NULL)')."'";
                }
                if($answer["question"] == "CLIENTE - id"){
                    $id_cliente = "'".(isset($answer["values"][0]) ? trim($answer["values"][0]) : '(NULL)')."'";
                }
                if($answer["question"] == "FINCA"){
                    $finca = "'".(isset($answer["values"][0]) ? trim($answer["values"][0]) : '(NULL)')."'";
                }
                if($answer["question"] == "FINCA - id"){
                    $id_finca = "'".(isset($answer["values"][0]) ? trim($answer["values"][0]) : '(NULL)')."'";
                }
                if($answer["question"] == "FECHA"){
                    $fecha = "'".(isset($values[0]["provided"]["time"]) ? str_replace("T", " ", substr($values[0]['provided']['time'],0,-6)) : '(NULL)')."'";
                }
            }

            #if($pagina_nombre == "CICLOS DE APLICACI\u00c3\u201cN"){
                if($answer["question"] == "Ciclo"){
                    $ciclo = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '(NULL)')."'";
                }
                if($answer["question"] == "Producto 1"){
                    $producto_1 = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '(NULL)')."'";
                }
                if($answer["question"] == "Producto 2"){
                    $producto_2 = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '(NULL)')."'";
                }
                if(strpos($answer["question"], "Fecha de aplicaci") !== false){
                    $fecha_aplicacion = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '(NULL)')."'";
                }
            #}

            #if($pagina_nombre == "EMISIÓN FOLIAR"){
                
                if($question == "Planta 1"){
                    $foliar_emision_1[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Planta 2"){
                    $foliar_emision_2[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";   
                }
                if($question == "Planta 3"){
                    $foliar_emision_3[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Planta 4"){
                    $foliar_emision_4[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Planta 5"){
                    $foliar_emision_5[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Planta 6"){
                    $foliar_emision_6[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Planta 7"){
                    $foliar_emision_7[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Planta 8"){
                    $foliar_emision_8[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Planta 9"){
                    $foliar_emision_9[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Planta 10"){
                    $foliar_emision_10[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "FOCO"){
                    $foliar_foco = (isset($values[0]) ? "'".trim($values[0])."'" : null);
                }
            #}

            #if($pagina_nombre = "LABORES AGR\u00cdCOLAS"){
                if($question == "Malezas"){
                    $malezas[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Evidencia Malezas"){
                    $evi_malezas[] = "'".(isset($values[0]["filename"]) ? "json/procesado/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '(NULL)')."'";
                }
                if($question == "Deshoje"){
                    $deshoje[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Evidencia Deshoje"){
                    $evi_deshoje[] = "'".(isset($values[0]["filename"]) ? "json/procesado/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '(NULL)')."'";
                }
                if($question == "Deshoje Fito"){
                    $deshoje_fito[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Evidencia Deshoje Fito"){
                    $evi_deshoje_fito[] = "'".(isset($values[0]["filename"]) ? "json/procesado/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '(NULL)')."'";
                }
                if($question == "Enfunde (Salvado de hojas)"){
                    $enfunde[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Evidencia Enfunde"){
                    $evi_enfunde[] = "'".(isset($values[0]["filename"]) ? "json/procesado/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '(NULL)')."'";
                }
                if($question == "Drenajes"){
                    $drenajes[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Evidencia Drenajes"){
                    $evi_drenajes[] = "'".(isset($values[0]["filename"]) ? "json/procesado/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '(NULL)')."'";
                }
                if($question == "Plagas foliares"){
                    $plaga[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Evidencia Plagas foliares"){
                    $evi_plaga[] = "'".(isset($values[0]["filename"]) ? "json/procesado/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '(NULL)')."'";
                }
                if($question == "Observaciones"){
                    $otrasobs[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Evidencia observaciones"){
                    $evi_otrasobs[] = "'".(isset($values[0]["filename"]) ? "json/procesado/image/".$referenceNumber."_".$values[0]["identifier"].str_replace("image/", ".", $values[0]["contentType"]) : '(NULL)')."'";
                }
            #}

            #if(strpos($pagina_nombre, "MUESTRA") !== false){
                if($question == "GEOLOCALIZACIÓN"){
                    if(isset($values[0])){
                        if($values[0]["coordinates"] != null){
                            $geo_posiciones[] = array("muestra"=>$pagina_nombre, "lat"=>$values[0]["coordinates"]["latitude"], "lng"=>$values[0]["coordinates"]["longitude"]);
                        }
                    }
                }
                if($question == "HORA DE MUESTRA"){
                    $hora_muestra[] = "'".(isset($values[0]["provided"]["time"]) ? $values[0]["provided"]["time"] : '(NULL)')."'";
                }
                if($question == "PLANTAS DE 3 METROS"){
                    $plantas_jovenes_3_metros[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "LOTE"){
                    $lote[] = (isset($values[0]) ? "'".ucwords(strtolower_utf8(trim($values[0])))."'" : '');
                }
                if($question == "LOTE - agrupacion"){
                    $focos[] = (isset($values[0]) ? "'".ucwords(strtolower_utf8(trim($values[0])))."'" : '');
                }
                if($question == "LOTE - id_agrupacion"){
                    $id_foco[] = (isset($values[0]) ? "'".ucwords(strtolower_utf8(trim($values[0])))."'" : '');
                }
                if($question == "LOTE - id"){
                    $id_lotes[] = (isset($values[0]) ? "'".ucwords(strtolower_utf8(trim($values[0])))."'" : '');
                }
                if(strpos($question, "INCIDENCIA HOJA") !== false){
                    $incidencia_hoja[$key][] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Sin incidencia"){
                    $sin_incidencia[$key][] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Leve"){
                    $leve[$key][] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Alta"){
                    $alta[$key][] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Hoja 3"){
                    $total_hoja_3[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                if($question == "Hoja 4"){
                    $total_hoja_4[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                if($question == "Hoja 5"){
                    $total_hoja_5[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                
                /* ----------------3METROS--------------- */
                if(strpos($question, "PLANTAS DE 3 METROS") !== false){
                    $tres_metros = true;
                }
                if($tres_metros){
                    if($question == "H+VLE"){
                        $tres_metros_hvle[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if($question == "H+VLQ<5%"){
                        $tres_metros_hvlq_menor[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($label, "HT") !== false){
                        $tres_metros_ht[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                        $tres_metros = false;
                    }
                }

                /* ----------------0 SEMANAS--------------- */
                if(strpos(strtoupper($question), "PLANTAS 0 SEMANAS") !== false){
                    $cero_semanas = true;
                }
                if($cero_semanas){
                    if(strpos($question, "HT") !== false){
                        $cero_semanas_ht[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($question, "H+VLE") !== false){
                        $cero_semanas_hvle[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($question, "H+VLQ<5%") !== false){
                        $cero_semanas_hvlq_menor[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($question, "H+VLQ>5%") !== false){
                        $cero_semanas_hvlq_mayor[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($question, "LC") !== false || strpos($label, "LC") !== false){
                        $cero_semanas_lc[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                        $cero_semanas = false;
                    }
                }

                /* ---------------11 SEMANAS--------------- */
                if(strpos(strtoupper($question), "PLANTAS 11 SEMANAS") !== null){
                    $once_semanas = true;
                }
                if($once_semanas){
                    if(strpos($question, "HT") !== false){
                        $once_semanas_ht[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($question, "H+VLQ<5%") !== false){
                        $once_semanas_hvlq_menor[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($question, "H+VLQ>5%") !== false){
                        $once_semanas_hvlq_mayor[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($question, "LC") !== false || strpos($label, "LC") !== false){
                        $once_semanas_lc[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                        $once_semanas = false;
                    }
                }
            #}

            if($pagina_nombre == "OBSERVACIONES GENERALES"){
                if($question == "OBSERVACIONES"){
                    $observaciones = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
            }
        }
    }

    /*------------------ID USUARIO--------------------*/

    $sql_get_usuario = "SELECT * FROM cat_usuarios WHERE idProntoforms = '$user_username'";
    $id_usuario = $conexion->Consultas(2, $sql_get_usuario);
    $id_usuario = $id_usuario[0]["id"];

    /* APLICACION */

    // $sql_aplicacion = "INSERT INTO ciclos_aplicacion(ciclo,producto_1,producto_2,fecha,id_usuario,id_finca) VALUES ($ciclo,$producto_1,$producto_2,$fecha,id_usuario,$id_finca);";
    // echo $sql_aplicacion."<br><br>";
    // $conexion->Consultas(1, $sql_aplicacion);

    /*------------------GEOPOSICION-------------------*/

    // $sql_geo_posicion = "INSERT INTO geo_posiciones (id_usuario, id_cliente, id_finca, fecha, semana, lat, lng)
    // VALUES($id_usuario,$id_cliente,$id_finca,$fecha,WEEK($fecha),$latitude,$longitude);";

    // echo $sql_geo_posicion."<br><br>";
    // $id_geoposicion = 42;#$conexion->Consultas(1, $sql_geo_posicion);

    // foreach ($geo_posiciones as $key => $value) {
    //     $sql_geo_posicion_sub = "INSERT INTO geo_posiciones_muestras(id_geo_posicion, nombre_muestra, lat, lng) 
    //     VALUES($id_geoposicion,
    //      '".$value["muestra"]."',
    //      ".$value["lat"].",
    //      ".$value["lng"].")";
        
    //     $conexion->Consultas(1, $sql_geo_posicion_sub);
    // }

    /*----------------TRES METROS---------------------*/

    $consulta_principal_tres_metros = "INSERT INTO muestras_haciendas_3M(cliente,id_cliente,id_usuario,id_hacienda,fecha) 
    VALUES($cliente,$id_cliente,$id_usuario,$id_finca,$fecha);";
    print $consulta_principal_tres_metros."<br><br>";
    // $id_principal = $conexion->Consultas($conexion->insert, $consulta_principal_tres_metros);
    $hojas_3 = '';
    $hojas_4 = '';
    $hojas_5 = '';
    foreach ($lote as $key => $value) {
        if($tres_metros_ht[$key] != '' && $lote[$key] != ''){
            /*$get_foco = "SELECT cat_lotes.id AS id_lote, cat_agrupaciones.nombre AS foco FROM cat_lotes INNER JOIN cat_agrupaciones ON cat_lotes.id_agrupacion = cat_agrupaciones.id WHERE cat_lotes.nombre = ".$lote[$key]." AND cat_lotes.id_hacienda = $id_finca;";
            $result = $conexion->Consultas(2, $get_foco);
            
            if(count($result)>0){
                $id_lote = $result[0]["id_lote"];
                $foco = $result[0]["foco"];
            }else{
                echo "3 mt ".$get_foco."<br>";
                $id_lote = 9999;
                $foco = "Desconocido";
            }*/

            if($total_hoja_3[$key] != ''){
                $hojas_3 = $total_hoja_3[$key];
            }else{
                $hojas_3 = "''";
            }

            if($total_hoja_4[$key] != ''){
                $hojas_4 = $total_hoja_4[$key];
            }else{
                $hojas_4 = "''";
            }

            if($total_hoja_5[$key] != ''){
                $hojas_5 = $total_hoja_5[$key];
            }else{
                $hojas_5 = "''";
            }

            $sub_consulta = "INSERT INTO muestras_hacienda_detalle_3M(id_Mhacienda,id_hacienda,lote,plantas_jovenes_3_met,hojas_totales,hoja_mas_vieja_de_estrias,hoja_mas_vieja_libre_quema_menor,total_hoja_3,total_hoja_4,total_hoja_5,id_lote,foco,id_cliente,fecha,id_usuario,json) 
            VALUES(".$id_principal
                .",".$id_finca
                .",".$lote[$key]
                .",".$plantas_jovenes_3_metros[$key]
                .",".$tres_metros_ht[$key]
                .",".$tres_metros_hvle[$key]
                .",".$tres_metros_hvlq_menor[$key]
                .",".$hojas_3
                .",".$hojas_4
                .",".$hojas_5
                .",".$id_lotes[$key]
                .",".$focos[$key]
                .",".$id_cliente
                .",".$fecha
                .",".(($focos[$key] != "Desconocido")?$id_usuario:10).",
                '$filename');";
            echo $sub_consulta."<br><br>";
            // $conexion->Consultas($conexion->insert, $sub_consulta);
        }
    }

    /*----------------CERO SEMANAS-----------------*/

    // $consulta_principal_cero_semanas = "INSERT INTO muestras_haciendas(cliente,id_cliente,id_usuario,id_hacienda,fecha, tipo_semana) 
    // VALUES($cliente,$id_cliente,$id_usuario,$id_finca,$fecha, 0);";

    // $id_principal = $conexion->Consultas($conexion->insert, $consulta_principal_cero_semanas);

    // foreach ($lote as $key => $value) {
    //     if($lote[$key] != '' && ($cero_semanas_ht[$key] != '' || $cero_semanas_hvle[$key] != '' || $cero_semanas_hvlq_menor[$key] != '' || $cero_semanas_hvlq_mayor[$key] != '' || $cero_semanas_lc[$key] != '')){
    //         $get_foco = "SELECT cat_lotes.id AS id_lote, cat_agrupaciones.nombre AS foco FROM cat_lotes INNER JOIN cat_agrupaciones ON cat_lotes.id_agrupacion = cat_agrupaciones.id WHERE cat_lotes.nombre = ".$lote[$key]." AND cat_lotes.id_hacienda = $id_finca;";
    //         $result = $conexion->Consultas(2, $get_foco);

    //         if(count($result)>0){
    //             $id_lote = $result[0]["id_lote"];
    //             $foco = $result[0]["foco"];
    //         }else{
    //             echo "sem 0 ".$get_foco."<br>";
    //             $id_lote = 9999;
    //             $foco = "Desconocido";
    //         }

    //         $sub_consulta = "INSERT INTO muestras_hacienda_detalle(id_Mhacienda,id_hacienda,lote,hojas_totales,hojas_mas_vieja_libre,hoja_mas_vieja_libre_quema_menor,hoja_mas_vieja_libre_quema_mayor,libre_cirugias,id_lote,foco,id_cliente,fecha,tipo_semana,id_usuario,json) 
    //         VALUES(".$id_principal
    //             .",".$id_finca
    //             .",".$lote[$key]
    //             .",".$cero_semanas_ht[$key]
    //             .",".$cero_semanas_hvle[$key]
    //             .",".$cero_semanas_hvlq_menor[$key]
    //             .",".$cero_semanas_hvlq_mayor[$key]
    //             .",".((isset($cero_semanas_lc[$key]) && $cero_semanas_lc[$key] != "")?$cero_semanas_lc[$key]:"''")
    //             .",".$id_lotes[$key]
    //             .",".$focos[$key]
    //             .",".$id_cliente
    //             .",".$fecha
    //             .",0"
    //             .",".(($focos[$key] != "Desconocido")?$id_usuario:10).",
    //             '$filename');";
    //         echo $sub_consulta."<br><br>";
    //         $conexion->Consultas($conexion->insert, $sub_consulta);
    //     }
    // }

    /*----------------ONCE SEMANAS-----------------*/

    // $consulta_principal_once_semanas = "INSERT INTO muestras_haciendas(cliente,id_cliente,id_usuario,id_hacienda,fecha, tipo_semana) 
    // VALUES($cliente,$id_cliente,$id_usuario,$id_finca,$fecha, 11);";

    // $id_principal = $conexion->Consultas($conexion->insert, $consulta_principal_once_semanas);

    // foreach ($lote as $key => $value) {
    //     if($lote[$key] != '' && ($once_semanas_ht[$key] != '' || $once_semanas_hvlq_menor[$key] != '' || $once_semanas_hvlq_mayor[$key] != '' || $once_semanas_lc[$key] != '')){
    //         /*$get_foco = "SELECT cat_lotes.id AS id_lote, cat_agrupaciones.nombre AS foco FROM cat_lotes INNER JOIN cat_agrupaciones ON cat_lotes.id_agrupacion = cat_agrupaciones.id WHERE cat_lotes.nombre = ".$lote[$key]." AND cat_lotes.id_hacienda = $id_finca;";
    //         $result = $conexion->Consultas(2, $get_foco);

    //         if(count($result)>0){
    //             $id_lote = $result[0]["id_lote"];
    //             $foco = $result[0]["foco"];
    //         }else{
    //             echo "sem 11 ".$get_foco."<br>";
    //             $id_lote = 9999;
    //             $foco = "Desconocido";
    //         }*/

    //     #echo $sub_consulta;
    //         $sub_consulta = "INSERT INTO muestras_hacienda_detalle(id_Mhacienda,id_hacienda,lote,hojas_totales,hoja_mas_vieja_libre_quema_menor,hoja_mas_vieja_libre_quema_mayor,libre_cirugias,id_lote,foco,id_cliente,fecha,tipo_semana,id_usuario, json) 
    //         VALUES(".$id_principal
    //             .",".$id_finca
    //             .",".$lote[$key]
    //             .",".$once_semanas_ht[$key]
    //             .",".$once_semanas_hvlq_menor[$key]
    //             .",".$once_semanas_hvlq_mayor[$key]
    //             .",".((isset($once_semanas_lc[$key]) && $once_semanas_lc[$key] != "")?$once_semanas_lc[$key]:"''")
    //             .",".$id_lotes[$key]
    //             .",".$focos[$key]
    //             .",".$id_cliente
    //             .",".$fecha
    //             .", 11"
    //             .",".(($focos[$key] != "Desconocido")?$id_usuario:10).",
    //             '$filename');";

    //         echo $sub_consulta."<br><br>";
    //         $conexion->Consultas($conexion->insert, $sub_consulta);
    //     }
    // }

    /*----------------FOLIAR------------------*/

    
    // $consulta_principal_foliar = "INSERT INTO foliar(supervisor,id_cliente,id_usuario,id_hacienda,fecha, emision1,emision2,emision3,emision4,emision5,emision6,emision7,emision8,emision9,emision10,malezas,evi_malezas,deshoje,evi_deshoje,deshojefito,evi_deshojefito,enfunde,evi_enfunde,drenaje,evi_drenaje,plaga,evi_plaga,otrasobs,evi_otrasobs, foco,json) 
    // VALUES(
    //     $supervisor,
    //     $id_cliente,
    //     $id_usuario,
    //     $id_finca,
    //     $fecha,
    //     ".$foliar_emision_1[0].",
    //     ".$foliar_emision_2[0].",
    //     ".$foliar_emision_3[0].",
    //     ".$foliar_emision_4[0].",
    //     ".$foliar_emision_5[0].",
    //     ".$foliar_emision_6[0].",
    //     ".$foliar_emision_7[0].",
    //     ".$foliar_emision_8[0].",
    //     ".$foliar_emision_9[0].",
    //     ".$foliar_emision_10[0].",
    //     ".(isset($malezas[0])?$malezas[0]:'').",
    //     ".(isset($evi_malezas[0])?$evi_malezas[0]:'').",
    //     ".(isset($deshoje[0])?$deshoje[0]:'').",
    //     ".(isset($evi_deshoje[0])?$evi_deshoje[0]:'').",
    //     ".(isset($deshoje[0])?$deshoje_fito[0]:'').",
    //     ".(isset($evi_deshoje[0])?$evi_deshoje_fito[0]:'').",
    //     ".(isset($enfunde[0])?$enfunde[0]:'').",
    //     ".(isset($evi_enfunde[0])?$evi_enfunde[0]:'').",
    //     ".(isset($drenajes[0])?$drenajes[0]:'').",
    //     ".(isset($evi_drenajes[0])?$evi_drenajes[0]:'').",
    //     ".(isset($plaga[0])?$plaga[0]:'').",
    //     ".(isset($evi_plaga[0])?$evi_plaga[0]:'').",
    //     ".(isset($otrasobs[0])?$otrasobs[0]:'').",
    //     ".(isset($evi_otrasobs[0])?$evi_otrasobs[0]:'').",
    //     ".(($id_finca == 18)?"'Total Fca.'":"'Resto Finca'").",
    //     '$filename'
    //     );";

    // echo $consulta_principal_foliar."<br><br>";

    // $conexion->Consultas(1, $consulta_principal_foliar);

    // /*GET OTRO FOCO*/
    // $sql_get_otro_foco = "SELECT foco FROM foliar WHERE id_hacienda = $id_finca AND foco != 'Resto Finca' GROUP BY foco";
    // $otro_foco = $conexion->Consultas(2,$sql_get_otro_foco);
    // $consulta_principal_foliar = "";
    // #if($foliar_foco != null){
    // if(isset($foliar_emision_1[1]) || isset($foliar_emision_2[1]) || isset($foliar_emision_3[1]) || isset($foliar_emision_4[1]) || isset($foliar_emision_5[1]) || isset($foliar_emision_6[1]) || isset($foliar_emision_7[1]) || isset($foliar_emision_8[1]) || isset($foliar_emision_9[1]) || isset($foliar_emision_10[1]) && $id_finca != 18){
    //     $consulta_principal_foliar = "INSERT INTO foliar(supervisor,id_cliente,id_usuario,id_hacienda,fecha, emision1,emision2,emision3,emision4,emision5,emision6,emision7,emision8,emision9,emision10,malezas,evi_malezas,deshoje,evi_deshoje,deshojefito,evi_deshojefito,enfunde,evi_enfunde,drenaje,evi_drenaje,plaga,evi_plaga,otrasobs,evi_otrasobs,foco,json) 
    //     VALUES(
    //         $supervisor,
    //         $id_cliente,
    //         $id_usuario,
    //         $id_finca,
    //         $fecha,
    //         ".(isset($foliar_emision_1[1])?$foliar_emision_1[1]:'').",
    //         ".(isset($foliar_emision_2[1])?$foliar_emision_2[1]:'').",
    //         ".$foliar_emision_3[1].",
    //         ".$foliar_emision_4[1].",
    //         ".$foliar_emision_5[1].",
    //         ".$foliar_emision_6[1].",
    //         ".$foliar_emision_7[1].",
    //         ".$foliar_emision_8[1].",
    //         ".$foliar_emision_9[1].",
    //         ".$foliar_emision_10[1].",
    //         ".(isset($malezas[1])?$malezas[1]:"'(NULL)'").",
    //         ".(isset($evi_malezas[1])?$evi_malezas[1]:"'(NULL)'").",
    //         ".(isset($deshoje[1])?$deshoje[1]:"'(NULL)'").",
    //         ".(isset($evi_deshoje[1])?$evi_deshoje[1]:"'(NULL)'").",
    //         ".(isset($deshoje[1])?$deshoje_fito[1]:"'(NULL)'").",
    //         ".(isset($evi_deshoje[1])?$evi_deshoje_fito[1]:"'(NULL)'").",
    //         ".(isset($enfunde[1])?$enfunde[1]:"'(NULL)'").",
    //         ".(isset($evi_enfunde[1])?$evi_enfunde[1]:"'(NULL)'").",
    //         ".(isset($drenajes[1])?$drenajes[1]:"'(NULL)'").",
    //         ".(isset($evi_drenajes[1])?$evi_drenajes[1]:"'(NULL)'").",
    //         ".(isset($plaga[1])?$plaga[1]:"'(NULL)'").",
    //         ".(isset($evi_plaga[1])?$evi_plaga[1]:"'(NULL)'").",
    //         ".(isset($otrasobs[1])?$otrasobs[1]:"'(NULL)'").",
    //         ".(isset($evi_otrasobs[1])?$evi_otrasobs[1]:"'(NULL)'").",
    //         ".(isset($otro_foco[0]["foco"])?"'".$otro_foco[0]["foco"]."'":"'(NULL)'").",
    //         '$filename'
    //         );";

    //     echo $consulta_principal_foliar."<br><br>";
    //     $conexion->Consultas(1, $consulta_principal_foliar);
    // }
}

function limpiar($String){
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
     
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);
    return $String;
}

function strtolower_utf8($string){ 
  $convert_to = array( 
    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", 
    "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", 
    "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж", 
    "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", 
    "ь", "э", "ю", "я" 
  ); 
  $convert_from = array( 
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", 
    "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", 
    "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", 
    "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ", 
    "Ь", "Э", "Ю", "Я" 
  ); 

  return str_replace($convert_from, $convert_to, $string); 
}

?>