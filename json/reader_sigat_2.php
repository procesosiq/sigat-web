<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

include '../controllers/conexion.php';

/** Directorio de los JSON */
$path = realpath('./');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        $pos1 = strpos($object->getPath(), "nuevo/semanas3/json_");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        process_json($json, $object->getFileName());
                        move_json($object->getPathName(), $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        move_image($object->getPathName(), $object->getFileName());
                        break;

                    default:
                        
                        break;
                }
            }
        }
    }
}

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/procesado/json/$nameFile")){
        echo 'error';
    }
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/procesado/image/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename){
    $conexion = new M_Conexion;

    $identifier = $json['identifier'];
    $version = $json['version']. '';;
    $zone = $json['zone']. '';;
    $referenceNumber = $json['referenceNumber']. '';;
    $state = $json['state']. '';;
    $deviceSubmitDate = $json['deviceSubmitDate'];
    $deviceSubmitDateDate = $deviceSubmitDate['time']. '';;
    $deviceSubmitDateZone = $deviceSubmitDate['zone']. '';;
    #$fecha_file = str_replace("-", "", explode("T", $deviceSubmitDateDate)[0]);

    $shiftedDeviceSubmitDate = $json['shiftedDeviceSubmitDate'].'';;
    $serverReceiveDate = $json['serverReceiveDate'].'';;
    $form = $json['form'];
    $form_identifier = $form['identifier'].'';;
    $form_versionIdentifier = $form['versionIdentifier'].'';;
    $form_name = $form['name'].'';;
    $form_version = $form['version'].'';;
    $form_formSpaceIdentifier = $form['formSpaceIdentifier'].'';;
    $form_formSpaceName = $form['formSpaceName'].'';;

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';;
    $user_username = $user['username'].'';;
    $user_displayName = $user['displayName'].'';;

    $geoStamp = $json['geoStamp'];
    $geoStamp_success = $geoStamp['success'].'';;
    $geoStamp_captureTimestamp = $geoStamp['captureTimestamp'];
    $geoStamp_captureTimestamp_provided = $geoStamp_captureTimestamp['provided'];
    $geoStamp_captureTimestamp_provided_time = $geoStamp_captureTimestamp_provided['time'].'';;
    $geoStamp_captureTimestamp_provided_zone = $geoStamp_captureTimestamp_provided['zone'].'';;
    $geoStamp_captureTimestamp_shifted = $geoStamp_captureTimestamp['shifted'].'';;
    $geoStamp_errorMessage = $geoStamp['errorMessage'].'';;
    $geoStamp_source = $geoStamp['source'].'';;
    $geoStamp_coordinates = $geoStamp['coordinates'];
    $geoStamp_coordinates_latitude = $geoStamp_coordinates['latitude'].'';;
    $geoStamp_coordinates_longitude = $geoStamp_coordinates['longitude'].'';;
    $geoStamp_coordinates_altitude = $geoStamp_coordinates['altitude'].'';;
    $geoStamp_address = $geoStamp['address'].'';;
    $pages = $json['pages'];

    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = $page_data['name'];
        $answers = $page_data["answers"];

        foreach($answers as $x => $answer){

            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];

            $labelita = limpiar(trim(strtolower($label)));
            if($pagina_nombre == "ENCABEZADO"){
                if($question == "CLIENTE"){
                    $cliente = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "CLIENTE - id"){
                    $id_cliente = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "FINCA"){
                    $finca = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "FINCA - id"){
                    $id_finca = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "SUPERVISOR"){
                    $supervisor = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "FECHA"){
                    $fecha = "'".(isset($values[0]["provided"]["time"]) ? str_replace("T", " ", $values[0]['provided']['time']) : '(NULL)')."'";
                }
            }

            if(strpos($pagina_nombre, "MUESTRA ") !== false){
                if($question == "HORA DE MUESTRA"){
                    $hora_muestra[] = "'".(isset($values[0]["provided"]["time"]) ? $values[0]["provided"]["time"] : '(NULL)')."'";
                }
                if($question == "PLANTAS JÓVENES 3 METROS"){
                    $plantas_jovenes_3_metros[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "LOTE"){
                    $lote[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "FOCO"){
                    $foco[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if(strpos($question, "INCIDENCIA HOJA") !== false){
                    $incidencia_hoja[$key][] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Sin incidencia"){
                    $sin_incidencia[$key][] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Leve"){
                    $leve[$key][] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Alta"){
                    $alta[$key][] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Total Hoja 3"){
                    $total_hoja_3[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                if($question == "Total Hoja 4"){
                    $total_hoja_4[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                if($question == "Total Hoja 5"){
                    $total_hoja_5[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                if($question == "EVALUACIÓN"){
                    $evaluacion[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "HOJAS TOTALES"){
                    $hojas_totales[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                if($question == "HOJA MÁS VIEJA LIBRE DE ESTRÍAS"){
                    $hoja_mas_vieja_de_estrias[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                if($question == "HOJA MÁS VIEJA LIBRE DE QUEMA < 5%"){
                    $hoja_mas_vieja_libre_quema_menor[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                if($question == "OBSERVACIONES"){
                    $observaciones_l[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "NOTA DE VOZ"){
                    $nota_voz[] = "'".(isset($values[0]["filename"]) ? $values[0]["filename"] : '(NULL)')."'";
                }
                if($question == "EVIDENCIA"){
                    foreach ($values as $y => $val) {
                        $evidencia[$x][] = (isset($values[$y]["filename"]) ? $values[$y]["filename"] : '');
                    }
                }
                if($question == "Muestra HT"){
                    $muestra_ht[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Muestra H+VLE"){
                    $muestra_hvle[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Muestra H+VLQ<5%"){
                    $muestra_hvlq_menor[] =  "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }

                /* ----------------3METROS--------------- */
                if($question == "H+VLE"){
                    $tres_metros_hvle[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "H+VLQ<5%"){
                    $tres_metros_hvlq_menor[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if(strpos($label, "HT3M") !== false){
                    $tres_metros_ht[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }

                /* ----------------0 SEMANAS--------------- */
                if(strpos($label, "HT0SEM") !== false)
                    $cero_semanas_ht[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if(strpos($label, "HMÁSVLDEÍA0SEM") !== false)
                    $cero_semanas_hvle[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if(strpos($label, "HMÁSVLDQ<5%0SEM") !== false){
                    $cero_semanas_hvlq_menor[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if(strpos($label, "HMÁSVLDQ>5%0SEM") !== false){
                    $cero_semanas_hvlq_mayor[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if(strpos($label, "LDCÍA") !== false){
                    $cero_semanas_lc[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }

                /* ---------------11 SEMANAS--------------- */
                if(strpos($label, "HT11SEM") !== false){
                    $once_semanas_ht[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if(strpos($label, "HMÁSVLDQ<5%11SEM") !== false){
                    $once_semanas_hvlq_menor[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if(strpos($label, "HMÁSVLDQ>5%11SEM") !== false){
                    $once_semanas_hvlq_mayor[] = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if()
            }

            if($pagina_nombre == "OBSERVACIONES GENERALES"){
                if($question == "OBSERVACIONES"){
                    $observaciones = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
            }
            if($pagina_nombre == "RESULTADOS"){
                if($question == "PLANTAS JÓVENES (3 METROS)"){
                    $r_plantas_jovenes_3_metros = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Evaluación Total"){
                    $r_evaluacion_total = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Cálculo Hojas Totales"){
                    $r_calculo_hojas_totales = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Total Muestras Hojas Totales"){
                    $r_total_muestras_hojas_totales = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Hojas Totales (HT)"){
                    $r_hojas_totales_ht = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Cálculo H+VLE"){
                    $r_calculo_hvle =  "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Total Muestras H+VLE"){
                    $r_total_muestras_hvle = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Hoja más vieja libre de estrías (H+VLE)"){
                    $r_hoja_mas_vieja_libre_estrias = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Cálculo H+VLQ<5%"){
                    $r_calculo_hvlq_menor = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
                if($question == "Hoja más vieja libre de quema menor al 5% (H+VLQ<5%)"){
                    $r_hoja_mas_vieja_libre_de_quema_menor = "'".(isset($values[0]) ? trim($values[0]) : '(NULL)')."'";
                }
            }
        }
    }

    $sql_get_usuario = "SELECT * FROM cat_usuarios WHERE idProntoforms = '$user_username'";
    $id_usuario = $conexion->Consultas(2, $sql_get_usuario);
    $id_usuario = $id_usuario[0]["id"];

    $consulta_principal = "INSERT INTO muestras_haciendas_3M(cliente,id_cliente,id_usuario,id_hacienda,fecha,notas,r_plantas_jovenes_3_metros, r_evaluacion_total, r_calculo_hojas_totales,r_total_muestras_hojas_totales,r_hojas_totales_ht,r_calculo_hvle,r_total_muestras_hvle,r_hoja_mas_vieja_lib_estrias,r_calculo_hvlq_menor,r_hoja_mas_vieja_lib_de_quema_men) 
    VALUES($cliente,$id_cliente,$id_usuario,$id_finca,$fecha,$observaciones,$r_plantas_jovenes_3_metros,$r_evaluacion_total,$r_calculo_hojas_totales,$r_total_muestras_hojas_totales,$r_hojas_totales_ht,$r_calculo_hvle,$r_total_muestras_hvle,$r_hoja_mas_vieja_libre_estrias,$r_calculo_hvlq_menor,$r_hoja_mas_vieja_libre_de_quema_menor);";

    $id_principal = $conexion->Consultas($conexion->insert, $consulta_principal);

    foreach ($lote as $key => $value) {
        if($hoja_mas_vieja_de_estrias[$key] != '' && $total_hoja_3[$key] != '' && $total_hoja_4[$key] != '' && $total_hoja_5[$key] != '' && $hoja_mas_vieja_libre_quema_menor[$key] != '' && $hojas_totales[$key] != ''){
            $sub_consulta = "INSERT INTO muestras_hacienda_detalle_3M(id_Mhacienda,id_hacienda,lote,plantas_jovenes_3_met,foco,evaluacion,hojas_totales,hoja_mas_vieja_de_estrias,hoja_mas_vieja_libre_quema_menor,observaciones_l,nota_voz,evidencia,muestra_ht,muestra_hvle,muestra_hvlq_menor,total_hoja_3,total_hoja_4,total_hoja_5,id_usuario) 
            VALUES(".$id_principal
                .",".$id_finca
                .",".$lote[$key]
                .",".$plantas_jovenes_3_metros[$key]
                .",".$foco[$key]
                .",".$evaluacion[$key]
                .",".$hojas_totales[$key]
                .",".$hoja_mas_vieja_de_estrias[$key]
                .",".$hoja_mas_vieja_libre_quema_menor[$key]
                .",".$observaciones_l[$key]
                .",".$nota_voz[$key]
                .",".(isset($evidencia[$key])?"'".json_encode($evidencia[$key])."'":'(NULL)')
                .",".$muestra_ht[$key]
                .",".$muestra_hvle[$key]
                .",".$muestra_hvlq_menor[$key]
                .",".$total_hoja_3[$key]
                .",".$total_hoja_4[$key]
                .",".$total_hoja_5[$key]
                .",".$id_usuario.");";

            $conexion->Consultas($conexion->insert, $sub_consulta);
        }
    }
}

function limpiar($String){
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
     
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);
    return $String;
}

?>