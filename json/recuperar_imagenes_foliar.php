<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

include '../controllers/conexion.php';
$db = new M_Conexion;

$rows = $db->queryAll("SELECT id, json FROM foliar WHERE fecha >= '2016-04-29' AND procesado_imagenes = 0");
foreach($rows as $row){
    $json = file_get_contents("http://sigat.procesos-iq.com/json/procesado/json/{$row->json}");
    $json = json_decode($json, true);

    $pages = $json["pages"];
    $preguntas = $pages[2]["answers"];

    $images = [
        "malezas" => [],
        "deshoje" => [],
        "deshojefito" => [],
        "enfunde" => [],
        "drenajes" => [],
        "plagas" => [],
        "obs" => []
    ];
    $count_images = 0;
    foreach($preguntas as $pregunta){
        if(strpos($pregunta['question'], 'Evidencia') !== false){
            if(isset($pregunta['values'][0])){
                switch($pregunta['question']){
                    case 'Evidencia Malezas':
                        foreach($pregunta['values'] as $foto){
                            $count_images++;
                            $images["malezas"][] = "json/procesado/image/{$json['referenceNumber']}_{$foto['filename']}";
                        }
                        break;
                    case 'Evidencia Deshoje':
                        foreach($pregunta['values'] as $foto){
                            $count_images++;
                            $images["deshoje"][] = "json/procesado/image/{$json['referenceNumber']}_{$foto['filename']}";
                        }
                        break;
                    case 'Evidencia Deshoje Fito':
                        foreach($pregunta['values'] as $foto){
                            $count_images++;
                            $images["deshojefito"][] = "json/procesado/image/{$json['referenceNumber']}_{$foto['filename']}";
                        }
                        break;
                    case 'Evidencia Enfunde':
                        foreach($pregunta['values'] as $foto){
                            $count_images++;
                            $images["enfunde"][] = "json/procesado/image/{$json['referenceNumber']}_{$foto['filename']}";
                        }
                        break;
                    case 'Evidencia Drenajes':
                        foreach($pregunta['values'] as $foto){
                            $count_images++;
                            $images["drenajes"][] = "json/procesado/image/{$json['referenceNumber']}_{$foto['filename']}";
                        }
                        break;
                    case 'Evidencia Plagas foliares':
                        foreach($pregunta['values'] as $foto){
                            $count_images++;
                            $images["plagas"][] = "json/procesado/image/{$json['referenceNumber']}_{$foto['filename']}";
                        }
                        break;
                    case 'Evidencia observaciones':
                        foreach($pregunta['values'] as $foto){
                            $count_images++;
                            $images["obs"][] = "json/procesado/image/{$json['referenceNumber']}_{$foto['filename']}";
                        }
                        break;
                }
            }
        }
    }
    if($count_images > 0){
        $add = "";
        if(count($images["malezas"]) > 0){
            $add .= ", evi_malezas = '".implode("|", $images["malezas"])."'";
        }
        if(count($images["deshoje"]) > 0){
            $add .= ", evi_deshoje = '".implode("|", $images["deshoje"])."'";
        }
        if(count($images["deshojefito"]) > 0){
            $add .= ", evi_deshojefito = '".implode("|", $images["deshojefito"])."'";
        }
        if(count($images["enfunde"]) > 0){
            $add .= ", evi_enfunde = '".implode("|", $images["enfunde"])."'";
        }
        if(count($images["drenajes"]) > 0){
            $add .= ", evi_drenaje = '".implode("|", $images["drenajes"])."'";
        }
        if(count($images["plagas"]) > 0){
            $add .= ", evi_plaga = '".implode("|", $images["plagas"])."'";
        }
        if(count($images["obs"]) > 0){
            $add .= ", evi_otrasobs = '".implode("|", $images["obs"])."'";
        }
        $sql = "UPDATE foliar SET  
                    procesado_imagenes = 1 {$add}
                WHERE id = $row->id";
        $db->query($sql);
    }
}

function D($value){
    echo "<pre>";
    print_r($value);
    echo "</pre><br>";
}

?>