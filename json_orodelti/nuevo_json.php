<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

include '../controllers/conexion.php';

/** Directorio de los JSON */
$path = realpath('./new');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName()){
        $pos1 = strpos($object->getPath(), "json_orodelti/new");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            if('json' == $ext || 'jpeg' == $ext || 'jpg' == $ext || 'png' == $ext){
                switch ($ext) {
                    case 'json':
                        /* READ AND MOVE JSON */
                        $json = json_decode(trim(file_get_contents($object->getPathName())), true);
                        #process_json($json, $object->getFileName());
                        move_json($object->getPathName(), $object->getFileName());
                        process_json_nuevo($json, $object->getFileName());
                        break;

                    case 'jpeg':
                    case 'jpg':
                    case 'png':
                        /* MOVE JSON */
                        move_image($object->getPathName(), $object->getFileName());
                        break;

                    default:
                        
                        break;
                }
            }
        }
    }
}

function move_json($file, $nameFile){
    echo $file;
    echo $nameFile;
    // die();
    if(!rename($file, __DIR__."/procesado/json/$nameFile")){
        echo 'error';
    }
    /*if(!rename($file, __DIR__."/nuevo/semanas3/$nameFile")){
        echo 'error';
    }*/
}

function move_image($file, $nameFile){
    // die();
    if(!rename($file, __DIR__."/procesado/image/$nameFile")){
        echo 'error';
    }
}

function process_json($json, $filename){
    $conexion = new M_Conexion;

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';;
    $user_username = str_replace(array("ñ", "Ñ"), array("n", "N"), $user['username'].'');
    $referenceNumber = $json["referenceNumber"];
    $pages = $json['pages'];

    $geoStamp = $json["geoStamp"];
    $coordinates = $geoStamp["coordinates"];
    $latitude = $coordinates["latitude"];
    $longitude = $coordinates["longitude"];

    $foliar = 0;
    $tres_metros = false;
    $cero_semanas = false;
    $once_semanas = false; 
    $geo_posiciones = [];
    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $pagina_nombre = trim($page_data['name']);
        $answers = $page_data["answers"];

        foreach($answers as $x => $answer){

            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = $answer['question'];
            $values   = $answer['values'];

            $labelita = limpiar(trim(strtolower($label)));
            if($pagina_nombre == "ENCABEZADO"){
                if($question == "SUPERVISOR"){
                    $supervisor = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                } 
            }

            if(isset($answer["question"])){
                if($answer["question"] == "CLIENTE"){
                    $cliente = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                }
                if($answer["question"] == "CLIENTE - id"){
                    $id_cliente = "'".(isset($answer["values"][0]) ? trim($answer["values"][0]) : '')."'";
                }
                if($answer["question"] == "FINCA"){
                    $finca = "'".(isset($answer["values"][0]) ? trim($answer["values"][0]) : '')."'";
                }
                if($answer["question"] == "ID FINCA"){
                    $id_finca = "'".(isset($answer["values"][0]) ? trim($answer["values"][0]) : '')."'";
                }
                if($answer["question"] == "FECHA"){
                    $fecha = "'".(isset($values[0]["provided"]["time"]) ? str_replace("T", " ", substr($values[0]['provided']['time'],0,-6)) : '')."'";
                }
            }

            #if($pagina_nombre == "CICLOS DE APLICACI\u00c3\u201cN"){
                if($answer["question"] == "Ciclo"){
                    $ciclo = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                }
                if($answer["question"] == "Producto 1"){
                    $producto_1 = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                }
                if($answer["question"] == "Producto 2"){
                    $producto_2 = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                }
                if(strpos($answer["question"], "Fecha de aplicaci") !== false){
                    $fecha_aplicacion = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                }
            #}

            #if($pagina_nombre == "EMISIÓN FOLIAR"){
                
                if($question == "Planta 1"){
                    $foliar_emision_1[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Planta 2"){
                    $foliar_emision_2[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";   
                }
                if($question == "Planta 3"){
                    $foliar_emision_3[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Planta 4"){
                    $foliar_emision_4[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Planta 5"){
                    $foliar_emision_5[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Planta 6"){
                    $foliar_emision_6[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Planta 7"){
                    $foliar_emision_7[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Planta 8"){
                    $foliar_emision_8[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Planta 9"){
                    $foliar_emision_9[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Planta 10"){
                    $foliar_emision_10[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "FOCO"){
                    $foliar_foco = (isset($values[0]) ? "'".trim($values[0])."'" : null);
                }
            #}

            #if($pagina_nombre = "LABORES AGR\u00cdCOLAS"){
                if($question == "Malezas"){
                    $malezas[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Evidencia Malezas"){
                    if(count($values) > 0){
                        $evi_malezas[] = "";
                        foreach($values as $img){
                            $evi_malezas[count($evi_malezas)-1] .= "json_orodelti/procesado/image/".$referenceNumber."_".$img["filename"]."|";
                        }
                        $evi_malezas[count($evi_malezas)-1] = substr($evi_malezas[count($evi_malezas)-1], 0, -1);
                    }
                    #$evi_malezas[] = "'".(isset($values[0]["filename"]) ? "json_orodelti/procesado/image/".$referenceNumber."_".$values[0]["filename"] : '')."'";
                }
                if($question == "Deshoje"){
                    $deshoje[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Evidencia Deshoje"){
                    if(count($values) > 0){
                        $evi_deshoje[] = "";
                        foreach($values as $img){
                            $evi_deshoje[count($evi_deshoje)-1] .= "json_orodelti/procesado/image/".$referenceNumber."_".$img["filename"]."|";
                        }
                        $evi_deshoje[count($evi_deshoje)-1] = substr($evi_deshoje[count($evi_deshoje)-1], 0, -1);
                    }
                    #$evi_deshoje[] = "'".(isset($values[0]["filename"]) ? "json_orodelti/procesado/image/".$referenceNumber."_".$values[0]["filename"] : '')."'";
                }
                if($question == "Deshoje Fito"){
                    $deshoje_fito[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Evidencia Deshoje Fito"){
                    if(count($values) > 0){
                        $evi_deshoje_fito[] = "";
                        foreach($values as $img){
                            $evi_deshoje_fito[count($evi_deshoje_fito)-1] .= "json_orodelti/procesado/image/".$referenceNumber."_".$img["filename"]."|";
                        }
                        $evi_deshoje_fito[count($evi_deshoje_fito)-1] = substr($evi_deshoje_fito[count($evi_deshoje_fito)-1], 0, -1);
                    }
                    #$evi_deshoje_fito[] = "'".(isset($values[0]["filename"]) ? "json_orodelti/procesado/image/".$referenceNumber."_".$values[0]["filename"] : '')."'";
                }
                if($question == "Enfunde (Salvado de hojas)"){
                    $enfunde[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Evidencia Enfunde"){
                    if(count($values) > 0){
                        $evi_enfunde[] = "";
                        foreach($values as $img){
                            $evi_enfunde[count($evi_enfunde)-1] .= "json_orodelti/procesado/image/".$referenceNumber."_".$img["filename"]."|";
                        }
                        $evi_enfunde[count($evi_enfunde)-1] = substr($evi_enfunde[count($evi_enfunde)-1], 0, -1);
                    }
                    #$evi_enfunde[] = "'".(isset($values[0]["filename"]) ? "json_orodelti/procesado/image/".$referenceNumber."_".$values[0]["filename"] : '')."'";
                }
                if($question == "Drenajes"){
                    $drenajes[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Evidencia Drenajes"){
                    if(count($values) > 0){
                        $evi_drenajes[] = "";
                        foreach($values as $img){
                            $evi_drenajes[count($evi_drenajes)-1] .= "json_orodelti/procesado/image/".$referenceNumber."_".$img["filename"]."|";
                        }
                        $evi_drenajes[count($evi_drenajes)-1] = substr($evi_drenajes[count($evi_drenajes)-1], 0, -1);
                    }
                    #$evi_drenajes[] = "'".(isset($values[0]["filename"]) ? "json_orodelti/procesado/image/".$referenceNumber."_".$values[0]["filename"] : '')."'";
                }
                if($question == "Plagas foliares"){
                    $plaga[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Evidencia Plagas foliares"){
                    if(count($values) > 0){
                        $evi_plaga[] = "";
                        foreach($values as $img){
                            $evi_plaga[count($evi_plaga)-1] .= "json_orodelti/procesado/image/".$referenceNumber."_".$img["filename"]."|";
                        }
                        $evi_plaga[count($evi_plaga)-1] = substr($evi_plaga[count($evi_plaga)-1], 0, -1);
                    }
                    #$evi_plaga[] = "'".(isset($values[0]["filename"]) ? "json_orodelti/procesado/image/".$referenceNumber."_".$values[0]["filename"] : '')."'";
                }
                if($question == "Observaciones"){
                    $otrasobs[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Evidencia observaciones"){
                    if(count($values) > 0){
                        $evi_otrasobs[] = "";
                        foreach($values as $img){
                            $evi_otrasobs[count($evi_otrasobs)-1] .= "json_orodelti/procesado/image/".$referenceNumber."_".$img["filename"]."|";
                        }
                        $evi_otrasobs[count($evi_otrasobs)-1] = substr($evi_otrasobs[count($evi_otrasobs)-1], 0, -1);
                    }
                    #$evi_otrasobs[] = "'".(isset($values[0]["filename"]) ? "json_orodelti/procesado/image/".$referenceNumber."_".$values[0]["filename"] : '')."'";
                }
            #}

            #if(strpos($pagina_nombre, "MUESTRA") !== false){
                if($question == "GEOLOCALIZACIÓN"){
                    if(isset($values[0])){
                        if($values[0]["coordinates"] != null){
                            $geo_posiciones[] = array("muestra"=>$pagina_nombre, "lat"=>$values[0]["coordinates"]["latitude"], "lng"=>$values[0]["coordinates"]["longitude"]);
                        }
                    }
                }
                if($question == "HORA DE MUESTRA"){
                    $hora_muestra[] = "'".(isset($values[0]["provided"]["time"]) ? $values[0]["provided"]["time"] : '')."'";
                }
                if($question == "PLANTAS DE 3 METROS"){
                    $plantas_jovenes_3_metros[] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "LOTE"){
                    $lote[] = (isset($values[0]) ? "'".ucwords(strtolower_utf8(trim($values[0])))."'" : '');
                }
                if($question == "LOTE - agrupacion"){
                    $focos[] = (isset($values[0]) ? "'".ucwords(strtolower_utf8(trim($values[0])))."'" : '');
                }
                if($question == "LOTE - id_agrupacion"){
                    $id_foco[] = (isset($values[0]) ? "'".ucwords(strtolower_utf8(trim($values[0])))."'" : '');
                }
                if($question == "LOTE - id"){
                    $id_lotes[] = (isset($values[0]) ? "'".ucwords(strtolower_utf8(trim($values[0])))."'" : '');
                }
                if(strpos($question, "INCIDENCIA HOJA") !== false){
                    $incidencia_hoja[$key][] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Sin incidencia"){
                    $sin_incidencia[$key][] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Leve"){
                    $leve[$key][] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Alta"){
                    $alta[$key][] = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
                if($question == "Hoja 3"){
                    $total_hoja_3[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                if($question == "Hoja 4"){
                    $total_hoja_4[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                if($question == "Hoja 5"){
                    $total_hoja_5[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                }
                
                /* ----------------3METROS--------------- */
                if(strpos($question, "PLANTAS DE 3 METROS") !== false){
                    $tres_metros = true;
                }
                if($tres_metros){
                    if($question == "H+VLE"){
                        $tres_metros_hvle[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if($question == "H+VLQ<5%"){
                        $tres_metros_hvlq_menor[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($label, "HT") !== false){
                        $tres_metros_ht[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                        $tres_metros = false;
                    }
                }

                /* ----------------0 SEMANAS--------------- */
                if(strpos(strtoupper($question), "PLANTAS 0 SEMANAS") !== false){
                    $cero_semanas = true;
                }
                if($cero_semanas){
                    if(strpos($question, "HT") !== false){
                        $cero_semanas_ht[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($question, "H+VLE") !== false){
                        $cero_semanas_hvle[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($question, "H+VLQ<5%") !== false){
                        $cero_semanas_hvlq_menor[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($question, "H+VLQ>5%") !== false){
                        $cero_semanas_hvlq_mayor[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($question, "LC") !== false || strpos($label, "LC") !== false){
                        $cero_semanas_lc[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                        $cero_semanas = false;
                    }
                }

                /* ---------------11 SEMANAS--------------- */
                if(strpos(strtoupper($question), "PLANTAS 11 SEMANAS") !== null){
                    $once_semanas = true;
                }
                if($once_semanas){
                    if(strpos($question, "HT") !== false){
                        $once_semanas_ht[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($question, "H+VLQ<5%") !== false){
                        $once_semanas_hvlq_menor[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($question, "H+VLQ>5%") !== false){
                        $once_semanas_hvlq_mayor[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                    }
                    if(strpos($question, "LC") !== false || strpos($label, "LC") !== false){
                        $once_semanas_lc[] = (isset($values[0]) ? "'".trim($values[0])."'" : '');
                        $once_semanas = false;
                    }
                }
            #}

            if($pagina_nombre == "OBSERVACIONES GENERALES"){
                if($question == "OBSERVACIONES"){
                    $observaciones = "'".(isset($values[0]) ? trim($values[0]) : '')."'";
                }
            }
        }
    }

    //------------------ID USUARIO--------------------*/

    $id_usuario = 1;
    $id_cliente = 0;
    $cliente = 0;
    
    // APLICACION 
    $sql_aplicacion = "INSERT INTO ciclos_aplicacion_orodelti (ciclo,producto_1,producto_2,fecha,id_usuario,id_finca) VALUES ($ciclo,$producto_1,$producto_2,$fecha,id_usuario,$id_finca);";
    echo $sql_aplicacion."<br><br>";
    #$conexion->Consultas(1, $sql_aplicacion);

    //------------------GEOPOSICION-------------------
    if(count($geo_posiciones) > 0){
        $cont = $conexion->queryRow("SELECT COUNT(1) AS contar FROM geo_posiciones_orodelti WHERE json = '{$filename}'")->contar;
        if($cont == 0){
            $latitude = is_numeric($latitude) ? $latitude : 'NULL';
            $longitude = is_numeric($longitude) ? $longitude : 'NULL';
            $sql_geo_posicion = "INSERT INTO geo_posiciones_orodelti (id_usuario, id_cliente, id_finca, fecha, semana, lat, lng, json)
                                    VALUES($id_usuario, $id_cliente,$id_finca,$fecha,getWeek($fecha), $latitude,$longitude, '{$filename}');";
    
            D($sql_geo_posicion);
            $id_geoposicion = 1;#$conexion->Consultas(1, $sql_geo_posicion);
    
            foreach ($geo_posiciones as $key => $value) {
                $sql_geo_posicion_sub = "INSERT INTO geo_posiciones_muestras_orodelti (id_geo_posicion, nombre_muestra, lat, lng) 
                VALUES($id_geoposicion,
                '".$value["muestra"]."',
                ".$value["lat"].",
                ".$value["lng"].")";
                
                D($sql_geo_posicion_sub);
                $conexion->Consultas(1, $sql_geo_posicion_sub);
            }
        }
    }

    //----------------TRES METROS---------------------

    $consulta_principal_tres_metros = "INSERT INTO muestras_haciendas_3M_app_orodelti(cliente,id_cliente,id_usuario,id_hacienda,fecha) 
    VALUES($cliente,$id_cliente,$id_usuario,$id_finca,$fecha);";

    $id_principal = $conexion->Consultas($conexion->insert, $consulta_principal_tres_metros);

    $hojas_3 = '';
    $hojas_4 = '';
    $hojas_5 = '';
    foreach ($lote as $key => $value) {
        if($tres_metros_ht[$key] != '' && $lote[$key] != ''){

            if($total_hoja_3[$key] != ''){
                $hojas_3 = $total_hoja_3[$key];
            }else{
                $hojas_3 = "''";
            }

            if($total_hoja_4[$key] != ''){
                $hojas_4 = $total_hoja_4[$key];
            }else{
                $hojas_4 = "''";
            }

            if($total_hoja_5[$key] != ''){
                $hojas_5 = $total_hoja_5[$key];
            }else{
                $hojas_5 = "''";
            }

            $sub_consulta = "INSERT INTO muestras_hacienda_detalle_3M_orodelti(id_Mhacienda,id_hacienda,lote,plantas_jovenes_3_met,hojas_totales,hoja_mas_vieja_de_estrias,hoja_mas_vieja_libre_quema_menor,total_hoja_3,total_hoja_4,total_hoja_5,id_lote,foco,id_cliente,fecha,id_usuario,json) 
            VALUES(".$id_principal
                .",".$id_finca
                .",".$lote[$key]
                .",".$plantas_jovenes_3_metros[$key]
                .",".$tres_metros_ht[$key]
                .",".$tres_metros_hvle[$key]
                .",".$tres_metros_hvlq_menor[$key]
                .",".$hojas_3
                .",".$hojas_4
                .",".$hojas_5
                .",".$id_lotes[$key]
                .",".$focos[$key]
                .",".$id_cliente
                .",".$fecha
                .",".(($focos[$key] != "Desconocido")?$id_usuario:10).",
                '$filename');";
            echo $sub_consulta."<br><br>";
            $conexion->Consultas($conexion->insert, $sub_consulta);
        }
    }

    //----------------CERO SEMANAS-----------------

    $consulta_principal_cero_semanas = "INSERT INTO muestras_haciendas_app_orodelti(cliente,id_cliente,id_usuario,id_hacienda,fecha, tipo_semana, usuario) 
    VALUES($cliente,$id_cliente,$id_usuario,$id_finca,$fecha, 0, '$user_username');";

    $id_principal = $conexion->Consultas($conexion->insert, $consulta_principal_cero_semanas);

    foreach ($lote as $key => $value) {
        if($lote[$key] != '' && ($cero_semanas_ht[$key] != '' || $cero_semanas_hvle[$key] != '' || $cero_semanas_hvlq_menor[$key] != '' || $cero_semanas_hvlq_mayor[$key] != '' || $cero_semanas_lc[$key] != '')){

            $sub_consulta = "INSERT INTO muestras_hacienda_detalle_orodelti(id_Mhacienda,id_hacienda,lote,hojas_totales,hojas_mas_vieja_libre,hoja_mas_vieja_libre_quema_menor,hoja_mas_vieja_libre_quema_mayor,libre_cirugias,id_lote,foco,id_cliente,fecha,tipo_semana,id_usuario,json, semana, anio) 
            VALUES(".$id_principal
                .",".$id_finca
                .",".$lote[$key]
                .",".$cero_semanas_ht[$key]
                .",".$cero_semanas_hvle[$key]
                .",".$cero_semanas_hvlq_menor[$key]
                .",".$cero_semanas_hvlq_mayor[$key]
                .",".((isset($cero_semanas_lc[$key]) && $cero_semanas_lc[$key] != "")?$cero_semanas_lc[$key]:"''")
                .",".$id_lotes[$key]
                .",".$focos[$key]
                .",".$id_cliente
                .",".$fecha
                .",0"
                .",".(($focos[$key] != "Desconocido")?$id_usuario:10).",
                '$filename',
                getWeek('{$fecha}'),
                getYear('{$fecha}')
            );";
            
            D($sub_consulta);
            $conexion->Consultas($conexion->insert, $sub_consulta);
        }
    }

    //----------------ONCE SEMANAS-----------------

    $consulta_principal_once_semanas = "INSERT INTO muestras_haciendas_app_orodelti(cliente,id_cliente,id_usuario,id_hacienda,fecha, tipo_semana, usuario) 
    VALUES($cliente,$id_cliente,$id_usuario,$id_finca,$fecha, 11, '$user_username');";

    $id_principal = $conexion->Consultas($conexion->insert, $consulta_principal_once_semanas);

    foreach ($lote as $key => $value) {
        if($lote[$key] != '' && ($once_semanas_ht[$key] != '' || $once_semanas_hvlq_menor[$key] != '' || $once_semanas_hvlq_mayor[$key] != '' || $once_semanas_lc[$key] != '')){

            $sub_consulta = "INSERT INTO muestras_hacienda_detalle_orodelti(id_Mhacienda,id_hacienda,lote,hojas_totales,hoja_mas_vieja_libre_quema_menor,hoja_mas_vieja_libre_quema_mayor,libre_cirugias,id_lote,foco,id_cliente,fecha,tipo_semana,id_usuario, json, semana, anio) 
            VALUES(".$id_principal
                .",".$id_finca
                .",".$lote[$key]
                .",".$once_semanas_ht[$key]
                .",".$once_semanas_hvlq_menor[$key]
                .",".$once_semanas_hvlq_mayor[$key]
                .",".((isset($once_semanas_lc[$key]) && $once_semanas_lc[$key] != "")?$once_semanas_lc[$key]:"''")
                .",".$id_lotes[$key]
                .",".$focos[$key]
                .",".$id_cliente
                .",".$fecha
                .", 11"
                .",".(($focos[$key] != "Desconocido")?$id_usuario:10).",
                '$filename',
                getWeek('{$fecha}'),
                getYear('{$fecha}')
            );";

            D($sub_consulta);
            $conexion->Consultas($conexion->insert, $sub_consulta);
        }
    }

    //----------------FOLIAR------------------

    
    $consulta_principal_foliar = "INSERT INTO foliar_orodelti(supervisor,id_cliente,id_usuario,id_hacienda,fecha, planta_1,planta_2,planta_3,planta_4,planta_5,planta_6,planta_7,planta_8,planta_9,planta_10,malezas,evi_malezas,deshoje,evi_deshoje,deshojefito,evi_deshojefito,enfunde,evi_enfunde,drenaje,evi_drenaje,plaga,evi_plaga,otrasobs,evi_otrasobs, foco,json) 
    VALUES(
        $supervisor,
        $id_cliente,
        $id_usuario,
        $id_finca,
        $fecha,
        ".$foliar_emision_1[0].",
        ".$foliar_emision_2[0].",
        ".$foliar_emision_3[0].",
        ".$foliar_emision_4[0].",
        ".$foliar_emision_5[0].",
        ".$foliar_emision_6[0].",
        ".$foliar_emision_7[0].",
        ".$foliar_emision_8[0].",
        ".$foliar_emision_9[0].",
        ".$foliar_emision_10[0].",
        ".(isset($malezas[0])?$malezas[0]:'').",
        '".(isset($evi_malezas[0])?$evi_malezas[0]:'')."',
        ".(isset($deshoje[0])?$deshoje[0]:'').",
        '".(isset($evi_deshoje[0])?$evi_deshoje[0]:'')."',
        ".(isset($deshoje[0])?$deshoje_fito[0]:'').",
        '".(isset($evi_deshoje_fito[0])?$evi_deshoje_fito[0]:'')."',
        ".(isset($enfunde[0])?$enfunde[0]:'').",
        '".(isset($evi_enfunde[0])?$evi_enfunde[0]:'')."',
        ".(isset($drenajes[0])?$drenajes[0]:'').",
        '".(isset($evi_drenajes[0])?$evi_drenajes[0]:'')."',
        ".(isset($plaga[0])?$plaga[0]:'').",
        '".(isset($evi_plaga[0])?$evi_plaga[0]:'')."',
        ".(isset($otrasobs[0])?$otrasobs[0]:'').",
        '".(isset($evi_otrasobs[0])?$evi_otrasobs[0]:'')."',
        ".(($id_finca == 18)?"'Total Fca.'":"'Resto Finca'").",
        '$filename'
        );";

    D($consulta_principal_foliar);
    $conexion->Consultas(1, $consulta_principal_foliar);

    // GET OTRO FOCO
    $sql_get_otro_foco = "SELECT foco FROM foliar_orodelti WHERE id_hacienda = $id_finca AND foco != 'Resto Finca' GROUP BY foco";
    $otro_foco = $conexion->Consultas(2, $sql_get_otro_foco);
    $consulta_principal_foliar = "";
    #if($foliar_foco != null){
    if(isset($foliar_emision_1[1]) || isset($foliar_emision_2[1]) || isset($foliar_emision_3[1]) || isset($foliar_emision_4[1]) || isset($foliar_emision_5[1]) || isset($foliar_emision_6[1]) || isset($foliar_emision_7[1]) || isset($foliar_emision_8[1]) || isset($foliar_emision_9[1]) || isset($foliar_emision_10[1]) && $id_finca != 18){
        $consulta_principal_foliar = "INSERT INTO foliar_orodelti(supervisor,id_cliente,id_usuario,id_hacienda,fecha, planta_1,planta_2,planta_3,planta_4,planta_5,planta_6,planta_7,planta_8,planta_9,planta_10,malezas,evi_malezas,deshoje,evi_deshoje,deshojefito,evi_deshojefito,enfunde,evi_enfunde,drenaje,evi_drenaje,plaga,evi_plaga,otrasobs,evi_otrasobs,foco,json) 
        VALUES(
            $supervisor,
            $id_cliente,
            $id_usuario,
            $id_finca,
            $fecha,
            ".(isset($foliar_emision_1[1])?$foliar_emision_1[1]:'').",
            ".(isset($foliar_emision_2[1])?$foliar_emision_2[1]:'').",
            ".$foliar_emision_3[1].",
            ".$foliar_emision_4[1].",
            ".$foliar_emision_5[1].",
            ".$foliar_emision_6[1].",
            ".$foliar_emision_7[1].",
            ".$foliar_emision_8[1].",
            ".$foliar_emision_9[1].",
            ".$foliar_emision_10[1].",
            ".(isset($malezas[1])?$malezas[1]:"''").",
            ".(isset($evi_malezas[1])?$evi_malezas[1]:"''").",
            ".(isset($deshoje[1])?$deshoje[1]:"''").",
            ".(isset($evi_deshoje[1])?$evi_deshoje[1]:"''").",
            ".(isset($deshoje[1])?$deshoje_fito[1]:"''").",
            ".(isset($evi_deshoje[1])?$evi_deshoje_fito[1]:"''").",
            ".(isset($enfunde[1])?$enfunde[1]:"''").",
            ".(isset($evi_enfunde[1])?$evi_enfunde[1]:"''").",
            ".(isset($drenajes[1])?$drenajes[1]:"''").",
            ".(isset($evi_drenajes[1])?$evi_drenajes[1]:"''").",
            ".(isset($plaga[1])?$plaga[1]:"''").",
            ".(isset($evi_plaga[1])?$evi_plaga[1]:"''").",
            ".(isset($otrasobs[1])?$otrasobs[1]:"''").",
            ".(isset($evi_otrasobs[1])?$evi_otrasobs[1]:"''").",
            ".(isset($otro_foco[0]["foco"])?"'".$otro_foco[0]["foco"]."'":"''").",
            '$filename'
            );";

        D($consulta_principal_foliar); 
        $conexion->Consultas(1, $consulta_principal_foliar);
    }

}

function process_json_nuevo($json, $filename){
    $db = new M_Conexion;

    $user = $json['user'];
    $user_identifier = $user['identifier'].'';;
    $user_username = $user['username'].'';;
    $referenceNumber = $json["referenceNumber"];
    $pages = $json['pages'];
    $form = $json["form"]["name"];
    $pagina_nombre = "";
    $objectMuestras = new stdClass;
    $objectMuestras->json = $filename;
    $objectMuestras->indentifier = $user_identifier;
    $objectMuestras->referenceNumber = $referenceNumber;
    $objectMuestras->auditor = $user["username"];
    $objectMuestras->auditorName = $user["displayName"];
    $contador = 0;

    $qqq = ["HT", "HT AFA", "H+VLE", "H+VLE AFA", "H+VLQ", "H+VLQ AFA", "LC", "H+VLQ>5%"];
    foreach($pages as $key => $page_data){
        $pagina = $key+1;
        $sql_muestra_causas[] = [];
        if($pagina_nombre != limpiar(trim(strtoupper($page_data['name'])))){
            $pagina_nombre = limpiar(trim(strtoupper($page_data['name'])));
            $contador++;
        }
        $answers = $page_data["answers"];
        foreach($answers as $x => $answer){
            $label    = $answer['label'];
            $dataType = $answer['dataType'];
            $question = trim($answer['question']);
            $values   = $answer['values'];
            $labelita = limpiar(trim(strtolower($label)));

            if(!isset($objectMuestras->{$pagina_nombre}[$question]) && !in_array($question, $qqq)){
                $objectMuestras->{$pagina_nombre}[$question] = getValueFromDataType($dataType, $values, $referenceNumber);
            } else {
                if(in_array($question, $qqq)){
                    $edad = '3M';
                    if(isset($objectMuestras->{$pagina_nombre}['PLANTAS 0 SEMANAS'])){
                        $edad = 'S0';
                    }
                    if(isset($objectMuestras->{$pagina_nombre}['PLANTAS 6 SEMANAS'])){
                        $edad = 'S6';
                    }
                    if(isset($objectMuestras->{$pagina_nombre}['PLANTAS 11 SEMANAS'])){
                        $edad = 'S11';
                    }

                    $objectMuestras->{$pagina_nombre}["{$edad} {$question}"] = getValueFromDataType($dataType, $values, $referenceNumber);
                }else{
                    $c = 1;
                    foreach($objectMuestras->{$pagina_nombre} as $q => $val){
                        if(strpos($question, $q) !== false){
                            $c++;
                        }
                    }
                    $objectMuestras->{$pagina_nombre}["{$question} {$c}"] = getValueFromDataType($dataType, $values, $referenceNumber);
                }
            }
        }
    }

    #D($objectMuestras);

    $fecha = $objectMuestras->{"ENCABEZADO"}['FECHA'];
    $finca = $objectMuestras->{"ENCABEZADO"}['FINCA'];
    $id_finca = $objectMuestras->{"ENCABEZADO"}['ID FINCA'];
    $supervisor = $objectMuestras->{"ENCABEZADO"}['SUPERVISOR'];

    $lat = 'NULL';
    $lng = 'NULL';
    if($objectMuestras->{"ENCABEZADO"}['GEOLOCALIZACIÓN'] != ''){
        $_parts = explode(",", $objectMuestras->{"ENCABEZADO"}['GEOLOCALIZACIÓN']);
        if(count($_parts) == 2){
            $lat = $_parts[0];
            $lng = $_parts[1];
        }

        if(empty($lat) || empty($lng)){
            $lat = 'NULL';
            $lng = 'NULL';
        }
    }

    // FOLIAR
    if(isset($objectMuestras->{"EMISION FOLIAR"})){
        $foliar = $objectMuestras->{"EMISION FOLIAR"};
        $e1 = $foliar['Planta 1'] > 0 ? $foliar['Planta 1'] : 'NULL';
        $e2 = $foliar['Planta 2'] > 0 ? $foliar['Planta 2'] : 'NULL';
        $e3 = $foliar['Planta 3'] > 0 ? $foliar['Planta 3'] : 'NULL';
        $e4 = $foliar['Planta 4'] > 0 ? $foliar['Planta 4'] : 'NULL';
        $e5 = $foliar['Planta 5'] > 0 ? $foliar['Planta 5'] : 'NULL';
        $e6 = $foliar['Planta 6'] > 0 ? $foliar['Planta 6'] : 'NULL';
        $e7 = $foliar['Planta 7'] > 0 ? $foliar['Planta 7'] : 'NULL';
        $e8 = $foliar['Planta 8'] > 0 ? $foliar['Planta 8'] : 'NULL';
        $e9 = $foliar['Planta 9'] > 0 ? $foliar['Planta 9'] : 'NULL';
        $e10 = $foliar['Planta 10'] > 0 ? $foliar['Planta 10'] : 'NULL';

        $ef = getEmisionFoliar($id_finca, "getYear('{$fecha}')", $db);
        if(count($ef) > 0){
            $ef = $ef[count($ef)-1];
            $ef = $ef['value'];
        }else{
            $ef = "NULL";
        }

        $sql = "INSERT INTO orodelti_foliar SET
                    id_finca = {$id_finca},
                    fecha = DATE('{$fecha}'),
                    semana = getWeek('{$fecha}'),
                    anio = getYear('{$fecha}'),
                    planta_1 = $e1,
                    planta_2 = $e2,
                    planta_3 = $e3,
                    planta_4 = $e4,
                    planta_5 = $e5,
                    planta_6 = $e6,
                    planta_7 = $e7,
                    planta_8 = $e8,
                    planta_9 = $e9,
                    planta_10 = $e10,
                    emision_foliar = $ef,
                    supervisor = '{$supervisor}',
                    lat = $lat,
                    lng = $lng,
                    json = '{$filename}'
                ";
        D($sql);
        $db->query($sql);
    }

    // EVIDENCIAS
    if(isset($objectMuestras->{"LABORES AGRICOLAS"})){
        $evis = [];
        foreach($objectMuestras->{"LABORES AGRICOLAS"} as $question => $value){
            if(strpos($question, "Evidencia") === false){
                if($value != ''){
                    $evis[$question]["comentarios"] = $value;
                }
            }else{
                $categoria = str_replace("Evidencia", "", $question);
                $evis[$question]["imagenes"] = $value;
            }
        }
        D($evis);
    }

    for($x = 1; $x <= 40; $x++){
        if(isset($objectMuestras->{"MUESTRA {$x}"}) && $objectMuestras->{"MUESTRA {$x}"}['AGREGAR OTRA MUESTRA?'] == 'SI'){
            $muestra = $objectMuestras->{"MUESTRA {$x}"};
            $lote = $muestra['LOTE'];
            $hora_muestra = $muestra['HORA DE MUESTRA'];

            $m3_ht = $muestra['3M HT'];
            $m3_hvle = $muestra['3M H+VLE'];
            $m3_hvle_afa = $muestra['3M H+VLE AFA'];
            $m3_hvlq = $muestra['3M H+VLQ'];
            $m3_hvlq_afa = $muestra['3M H+VLQ AFA'];

            $s0_ht = $muestra['S0 HT'] > 0 ? $muestra['S0 HT'] : 'NULL';
            $s0_ht_afa = $muestra['S0 HT AFA'] > 0 ? $muestra['S0 HT AFA'] : 'NULL';
            $s0_hvle = $muestra['S0 H+VLE'] > 0 ? $muestra['S0 H+VLQ'] : 'NULL';
            $s0_hvle_afa = $muestra['S0 H+VLE AFA'] > 0 ? $muestra['S0 H+VLE AFA'] : 'NULL';
            $s0_hvlq = $muestra['S0 H+VLQ'] > 0 ? $muestra['S0 H+VLQ'] : 'NULL';
            $s0_hvlq_afa = $muestra['S0 H+VLQ AFA'] > 0 ? $muestra['S0 H+VLQ AFA'] : 'NULL';
            $s0_hvlq_mayor_5 = $muestra['S0 H+VLQ>5%'] > 0 ? $muestra['S0 H+VLQ>%5'] : 'NULL';
            $s0_lc = $muestra['S0 LC'] > 0 ? $muestra['S0 LC'] : 'NULL';

            $s6_ht = $muestra['S6 HT'] > 0 ? $muestra['S6 HT'] : 'NULL';
            $s6_ht_afa = $muestra['S6 HT AFA'] > 0 ? $muestra['S6 HT AFA'] : 'NULL';
            $s6_hvle = $muestra['S6 H+VLE'] > 0 ? $muestra['S6 H+VLE'] : 'NULL';
            $s6_hvle_afa = $muestra['S6 H+VLE AFA'] > 0 ? $muestra['S6 H+VLE'] : 'NULL';
            $s6_hvlq = $muestra['S6 H+VLQ'] > 0 ? $muestra['S6 H+VLQ'] : 'NULL';
            $s6_hvlq_afa = $muestra['S6 H+VLQ AFA'] > 0 ? $muestra['S6 H+VLE AFA'] : 'NULL';
            $s6_hvlq_mayor_5 = $muestra['S6 H+VLQ>5%'] > 0 ? $muestra['S6 H+VLEQ>5%'] : 'NULL';
            $s6_lc = $muestra['S6 LC'] > 0 ? $muestra['S6 LC'] : 'NULL';
            
            $s11_ht = $muestra['S11 HT'] > 0 ? $muestra['S11 HT'] : 'NULL';
            $s11_ht_afa = $muestra['S11 HT AFA'] > 0 ? $muestra['S11 HT AFA'] : 'NULL';
            $s11_hvle = $muestra['S11 H+VLE'] > 0 ? $muestra['S11 H+VLE'] : 'NULL';
            $s11_hvle_afa = $muestra['S11 H+VLE AFA'] > 0 ? $muestra['S11 H+VLE AFA'] : 'NULL';
            $s11_hvlq = $muestra['S11 H+VLQ'] > 0 ? $muestra['S11 H+VLQ'] : 'NULL';
            $s11_hvlq_afa = $muestra['S11 H+VLQ AFA'] > 0 ? $muestra['S11 H+VLQ AFA'] : 'NULL';
            $s11_lc = $muestra['S11 LC'] > 0 ? $muestra['S11 LC'] : 'NULL';

            $latlng = explode(",", $muestra['GEOLOCALIZACIÓN']);
            $lat = trim($latlng[0]);
            $lng = trim($latlng[1]);

            if(empty($lat) || empty($lng)){
                $lat = 'NULL';
                $lng = 'NULL';
            }

            $sql = "INSERT INTO orodelti_plantas_muestras SET
                        id_finca = {$id_finca},
                        finca = '{$finca}',
                        lote = '{$lote}',
                        fecha = DATE('{$fecha}'),
                        semana = getWeek('{$fecha}'),
                        anio = getYear('{$fecha}'),
                        numero_muestra = {$x},
                        hora_muestra = '{$hora_muestra}',
                        3m_ht = {$m3_ht},
                        3m_hvle = {$m3_hvle},
                        3m_hvle_afa = {$m3_hvle_afa},
                        3m_hvlq = {$m3_hvlq},
                        3m_hvlq_afa = {$m3_hvlq_afa},
                        0s_ht = {$s0_ht},
                        0s_ht_afa = {$s0_ht_afa},
                        0s_hvle = {$s0_hvle},
                        0s_hvle_afa = {$s0_hvle_afa},
                        0s_hvlq = {$s0_hvlq},
                        0s_hvlq_afa = {$s0_hvlq_afa},
                        0s_hvlq_mayor_5 = {$s0_hvlq_mayor_5},
                        0s_lc = {$s0_lc},
                        6s_ht = {$s6_ht},
                        6s_ht_afa = {$s6_ht_afa},
                        6s_hvle = {$s6_hvle},
                        6s_hvle_afa = {$s6_hvle_afa},
                        6s_hvlq = {$s6_hvlq},
                        6s_hvlq_afa = {$s6_hvlq_afa},
                        6s_hvlq_mayor_5 = {$s6_hvlq_mayor_5},
                        6s_lc = {$s6_lc},
                        11s_ht = {$s11_ht},
                        11s_ht_afa = {$s11_ht_afa},
                        11s_hvle = {$s11_hvle},
                        11s_hvle_afa = {$s11_hvle_afa},
                        11s_hvlq = {$s11_hvlq},
                        11s_hvlq_afa = {$s11_hvlq_afa},
                        11s_lc = {$s11_lc},
                        lat = {$lat},
                        lng = {$lng},
                        json = '{$filename}'";
            D($sql);
            $db->query($sql);
            $id_main = $db->getLastID();

            for($y = 2; $y <= 5; $y++){
                if($muestra["HOJA {$y}. Incidencia"] > 0 || $muestra["HOJA {$y}. Severidad"] > 0){
                    $sql = "INSERT INTO orodelti_plantas_muestras_hojas SET
                                id_muestra = $id_main,
                                num_hoja = $y,
                                incidencia = '{$muestra["HOJA {$y}. Incidencia"]}',
                                severidad = '{$muestra["HOJA {$y}. Severidad"]}'";
                    D($sql);
                    $db->query($sql);
                }
            }
        }else{
            break;
        }
    }
}

function getEmisionFoliar($id_finca, $year, $db){
    $sql_get_weeks = "SELECT semana, anio, id_finca AS foco
        FROM orodelti_foliar
        WHERE id_finca = '{$id_finca}'
            AND (anio = {$year} OR (anio = {$year}-1 AND semana = 52))
        GROUP BY semana
        ORDER BY anio, semana";
    
    $weeks = $db->queryAll($sql_get_weeks, true);
    
    $count_foco=0;
    $foco = ['foco' => ''];
    $tabla = array();
    $data = [];
    foreach ($weeks as $key => $value) {
        $semana = $weeks[$key]["semana"];
        $anio = $weeks[$key]["anio"];
        
        if($foco['foco'] != $weeks[$key]["foco"]){
            $foco = ['foco' => $weeks[$key]["foco"]];
            $count_foco=0;
        }

        if($count_foco > 0){
            $pre_semana = $weeks[$key-1]["semana"];
            $pre_anio = $weeks[$key-1]["anio"];
            $sql = "SELECT (planta_1+planta_2+planta_3+planta_4+planta_5+planta_6+planta_7+planta_8+planta_9+planta_10)/division AS valor, foco
                FROM(
                    SELECT 
                        IF(actual.planta_1 > pasada.planta_1,(actual.planta_1-pasada.planta_1)/dif_dias*7,0) AS planta_1,
                        IF(actual.planta_2 > pasada.planta_2,(actual.planta_2-pasada.planta_2)/dif_dias*7,0) AS planta_2,
                        IF(actual.planta_3 > pasada.planta_3,(actual.planta_3-pasada.planta_3)/dif_dias*7,0) AS planta_3,
                        IF(actual.planta_4 > pasada.planta_4,(actual.planta_4-pasada.planta_4)/dif_dias*7,0) AS planta_4,
                        IF(actual.planta_5 > pasada.planta_5,(actual.planta_5-pasada.planta_5)/dif_dias*7,0) AS planta_5,
                        IF(actual.planta_6 > pasada.planta_6,(actual.planta_6-pasada.planta_6)/dif_dias*7,0) AS planta_6,
                        IF(actual.planta_7 > pasada.planta_7,(actual.planta_7-pasada.planta_7)/dif_dias*7,0) AS planta_7,
                        IF(actual.planta_8 > pasada.planta_8,(actual.planta_8-pasada.planta_8)/dif_dias*7,0) AS planta_8,
                        IF(actual.planta_9 > pasada.planta_9,(actual.planta_9-pasada.planta_9)/dif_dias*7,0) AS planta_9,
                        IF(actual.planta_10 > pasada.planta_10,(actual.planta_10-pasada.planta_10)/dif_dias*7,0) AS planta_10,
                        actual.id_finca AS foco
                    FROM (
                        SELECT * 
                        FROM orodelti_foliar 
                        WHERE semana = $semana 
                            AND anio = $anio 
                            AND id_finca = '{$id_finca}'
                    ) AS actual
                    JOIN (
                        SELECT * 
                        FROM orodelti_foliar 
                        WHERE semana = ".$weeks[$key-1]["semana"]." 
                            AND anio = $pre_anio 
                            AND id_finca = '{$id_finca}'
                    ) AS pasada
                    JOIN (
                        SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
                        FROM(
                            SELECT *
                            FROM (
                                SELECT MIN(fecha) AS dia_actual 
                                FROM orodelti_foliar 
                                WHERE semana = $semana 
                                    AND anio = $anio 
                                    AND id_finca = '{$id_finca}' 
                                    GROUP BY id_finca
                            ) AS actual
                            JOIN (
                                SELECT MIN(fecha) AS dia_pasado 
                                FROM orodelti_foliar 
                                WHERE semana = ".$weeks[$key-1]["semana"]." 
                                    AND anio = $pre_anio 
                                    AND id_finca = '{$id_finca}'
                                    GROUP BY id_finca
                            ) AS pasada
                        ) AS tbl_dif
                    ) tbl_dif_d
                ) AS tabla
                JOIN (
                    SELECT (planta_1+planta_2+planta_3+planta_4+planta_5+planta_6+planta_7+planta_8+planta_9+planta_10) AS division 
                    FROM(
                        SELECT IF(actual.planta_1 > pasada.planta_1,1,0) AS planta_1,
                            IF(actual.planta_2 > pasada.planta_2,1,0) AS planta_2,
                            IF(actual.planta_3 > pasada.planta_3,1,0) AS planta_3,
                            IF(actual.planta_4 > pasada.planta_4,1,0) AS planta_4,
                            IF(actual.planta_5 > pasada.planta_5,1,0) AS planta_5,
                            IF(actual.planta_6 > pasada.planta_6,1,0) AS planta_6,
                            IF(actual.planta_7 > pasada.planta_7,1,0) AS planta_7,
                            IF(actual.planta_8 > pasada.planta_8,1,0) AS planta_8,
                            IF(actual.planta_9 > pasada.planta_9,1,0) AS planta_9,
                            IF(actual.planta_10 > pasada.planta_10,1,0) AS planta_10
                        FROM(
                            SELECT * 
                            FROM orodelti_foliar 
                            WHERE semana = $semana 
                                AND anio = $anio 
                                AND id_finca = '{$id_finca}'
                        ) AS actual
                        JOIN (
                            SELECT * 
                            FROM orodelti_foliar
                            WHERE semana = ".$weeks[$key-1]["semana"]." 
                                AND anio = $pre_anio 
                                AND id_finca = '{$id_finca}'
                        ) AS pasada
                    )AS tbl_suma
                )AS tbl_division
                ";
                
            $result = $db->queryAll($sql, true);
            if($result[0]["foco"] != ""){
                $data[$result[0]["foco"]][] = array($semana,round(((double)$result[0]["valor"]),2));
                $tabla[$result[0]["foco"]][$semana] = (double)round(((double)$result[0]["valor"]),2);
            }
        }
        $count_foco++;
    }
    
    $data_chart = [];
    $count = 0;
    D($data);
    foreach($data as $key => $value){
        foreach($value as $val){
            if($val[1] > 0){
                $data_chart[] = ["value" => $val[1], "legend" => $key, "selected" => 1, "label" => $val[0], "position" => $count];
            }else{
                $data_chart[] = ["value" => NULL, "legend" => $key, "selected" => 1, "label" => $val[0], "position" => $count];
            }
        }
        $count++;
    }
    return $data_chart;
}

function getValueFromDataType($dataType, $values, $referenceNumber){
    switch ($dataType) {
        case 'EmailAddress': return '';
        case 'Timestamp':
            return getValueFromTimestamp($values);
        case 'GeoLocation':
            return getValueFromGeoLocation($values);
        case 'Information':
        case 'FreeText':
        case 'Date':
            return getValueFromFreeText($values);
        case 'Integer':
            return getValueFromInteger($values);
        case 'Signature':
            return getValueFromSignature($values, $referenceNumber);
        case 'Decimal':
            return getValueFromDecimal($values);
        case 'Time':
            return getValueFromTime($values);
        case 'Image':
            return getValueFromImage($values, $referenceNumber);
        default:
            D("No has agregado el tipo: ".$dataType);
            return "";
    }
}

function getValueFromTimestamp($values){
    if(isset($values[0])){
        $parts = $values[0]["provided"]["time"];
        $parts = explode("T", $parts);
        $date = $parts[0];
        $time = substr($parts[1], 0, 8);
        return "{$date} {$time}";
    }
    return "";
}

function getValueFromGeoLocation($values){
    return isset($values[0])
        ? $values[0]["coordinates"]["latitude"].",".$values[0]["coordinates"]["longitude"]
        : "";
}

function getValueFromFreeText($values){
    return isset($values[0]) ? trim(addslashes($values[0])) : "";
}

function getValueFromInteger($values){
    return (int) isset($values[0]) ? trim(addslashes($values[0])) : "";
}

function getValueFromDecimal($values){
    return (float) isset($values[0]) ? trim(addslashes($values[0])) : "";
}

function getValueFromTime($values){
    return isset($values[0])
            ? substr($values[0]["provided"]["time"], 0, 8)
            : "";
}

function getValueFromSignature($values, $referenceNumber){
    return isset($values[0]) 
                        ? $referenceNumber ."_". $values[0]["filename"]
                        : "";
}

function getValueFromImage($values, $referenceNumber){
    $newVals = [];
    foreach($values as $val){
        $newVals[] = $referenceNumber ."_". $val["filename"];
    }
    return implode("|", $newVals);
}


function limpiar($String){
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
     
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);
    return $String;
}

function D($string){
    echo "<pre>";
    print_r($string);
    echo "</pre><br>";
}

function strtolower_utf8($string){ 
  $convert_to = array( 
    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", 
    "v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", 
    "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж", 
    "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", 
    "ь", "э", "ю", "я" 
  ); 
  $convert_from = array( 
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", 
    "V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", 
    "Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", 
    "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ", 
    "Ь", "Э", "Ю", "Я" 
  ); 

  return str_replace($convert_from, $convert_to, $string); 
}

?>