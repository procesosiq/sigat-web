<?php
	/**
	
		ALIAS :
		- $arrayConfig [Arreglo que contiene todos los alias de las vistas con respecto a su clase] 
	
	 */
	// echo "<br>"."Usuario ".Session::getInstance()->logged;
 //    echo "<br>"."Cliente ".Session::getInstance()->client;
 //    echo "<br>"."Finca ".Session::getInstance()->finca;
	$arrayConfig = array();
	
	$arrayConfig["index"] = "indexMain";
    $arrayConfig["inspeccion"] = "indexMain";
    $arrayConfig["inspeccionSemanal"] = "indexMain";
    $arrayConfig["inspeccionPrimaDonna"] = "indexMain";
	$arrayConfig["listProduc"] = "Productores";
	$arrayConfig["newProduc"] = "Productores";
	$arrayConfig["agrupacionList"] = "Agrupaciones";
	$arrayConfig["newAgrupacion"] = "Agrupaciones";
	$arrayConfig["fincaList"] = "Fincas";
	$arrayConfig["newFinca"] = "Fincas";
	$arrayConfig["haciendaList"] = "Haciendas";
	$arrayConfig["newHacienda"] = "Haciendas";
	$arrayConfig["LotesList"] = "Lotes";
	$arrayConfig["newLote"] = "Lotes";
	$arrayConfig["importarInformacion"] = "importInformacion";
	$arrayConfig["cicloList"] = "Ciclos2";
	$arrayConfig["newCiclo"] = "Ciclos2";
    $arrayConfig["newProducto"] = "Ciclos2";
    $arrayConfig["listRevisionTecnico"] = "RevisionTecnico";
	/*----------  CICLOS  ----------*/
	/*----------  SECTORES  ----------*/
	$arrayConfig["sectoresList"] = "Sectores";
	$arrayConfig["newSector"] = "Sectores";
	/*----------  SECTORES  ----------*/
	/*----------  PROGRAMAS  ----------*/
	$arrayConfig["programasList"] = "Programas";
	$arrayConfig["newPrograma"] = "Programas";
	/*----------  PROGRAMAS  ----------*/
	/*----------  TIPOS DE CICLOS  ----------*/
	$arrayConfig["tiposCiclosList"] = "TiposCiclos";
	$arrayConfig["newTipoCiclos"] = "TiposCiclos";
	/*----------  TIPOS DE CICLOS  ----------*/
	/*----------  FUMIGADORAS  ----------*/
	$arrayConfig["fumigadorasList"] = "Fumigadoras";
	$arrayConfig["newFumigadora"] = "Fumigadoras";
	/*----------  FUMIGADORAS  ----------*/
	/*----------  PILOTOS  ----------*/
	$arrayConfig["pilotosList"] = "Pilotos";
	$arrayConfig["newPiloto"] = "Pilotos";
	/*----------  PILOTOS  ----------*/
	/*----------  PLACA AVION  ----------*/
	$arrayConfig["placasList"] = "Placas";
	$arrayConfig["newPlaca"] = "Placas";
	/*----------  PLACA AVION  ----------*/
	/*----------  GERENTES  ----------*/
	$arrayConfig["gerentesList"] = "Gerentes";
	$arrayConfig["newGerente"] = "Gerentes";
	/*----------  GERENTES  ----------*/
	/*----------  CYCLE  ----------*/
	$arrayConfig["cycleList"] = "Cycle";
	$arrayConfig["cycles"] = "Cycle";
	/*----------  CYCLE  ----------*/
	

	// $arrayConfig["fotos"] = "fotos";

	// print_r(Session::getInstance());
    /*----------  VALIDACION DE PERMISOS  ----------*/
	 if((Session::getInstance()->membresia->membresia == 'Completa' || Session::getInstance()->membresia->membresia == 'Media')  
	 		&& Session::getInstance()->privileges->access == 1 || Session::getInstance()->isAdmin){
			// $arrayConfig["listCiclos"] = "Ciclos";
			// $arrayConfig["newCiclos"] = "Ciclos";
		#return true;
		
     }elseif(array_key_exists($page, $arrayConfig)){
	 	if(isset(Session::getInstance()->access_fincas->access_special) && Session::getInstance()->access_fincas->access_special > 0 && $page == "index"){

	 	}else{
	 		if(Session::getInstance()->logged == 15 || Session::getInstance()->logged == 29 || Session::getInstance()->logged == 26){
				 //header('Location: http://sigat.procesos-iq.com/clima');
	 		}else{
				 //die("Acceso no Autorizado");
	 		}
	 	}
	 }
	// print_r($arrayConfig);
	/*----------  PATHS y OBJETO GLOBAL ----------*/
	$path = "./controllers";
	$loader = new stdClass;
	/*----------  PATHS y OBJETO GLOBAL ----------*/

	/*----------  CARGAMOS LA CLASE DE LA VISTA  ----------*/
	$controlName = $path."/".$arrayConfig[$page].".php";
	if (file_exists($controlName)){
		include_once $path.'/conexion.php';
		include_once ($controlName);		
		/*----------  CARGAMOS LA CLASE DE LA VISTA  ----------*/

		/*----------  CREAMOS LA INSTANCIA DE LA VISTA  ----------*/
		$nameClass = $arrayConfig[$page];
		$loader = new $nameClass();
		/*----------  CREAMOS LA INSTANCIA DE LA VISTA  ----------*/
	}

?>