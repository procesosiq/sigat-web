<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/

    $permission_write = [
        'READ_ALL_WRITE_ALL',
        'READ_OWN_WRITE_ALL',
        'READ_OWN_WRITE_OWN',
        'READ_NONE_WRITE_ALL',
        'READ_NONE_WRITE_OWN',
        'READ_ALL_RITE_OWN'
    ];
    
    $orodeltiReportes = [
        "analisisProductos",
        "tecnicos",
        "climaOrodelti",
        "climaDiarioOrodelti",
        "listRevisionTecnico",
        "costos"
    ];
    $orodeltiInspeccion = [
        "inspeccionOrodelti",
        "inspeccionOrodeltiApp",
        "inspeccionOrodeltiAppGraficas",
        "resumenInspeccionOrodelti"
    ];

    $inspeccion_orodelti_app = [
        'inspeccionOrodeltiApp',
        'inspeccionOrodeltiAppGraficas'
    ];

    $programa = [
        'programaTecnico',
        'programaEconomico',
        'otros',
        'plagas',
        'otrosSigatokas'
    ];

    $config = [
        'fincasHectareasOrodelti',
        'gerentesList',
        'fincaList',
        'productos',
        'fumigadorasList',
        'pilotos'
    ];
    
    $fincas = (array) Session::getInstance()->fincas_disponibles_cliente_seleccionado;
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                    <?php
                        if((Session::getInstance()->membresia->membresia == 'Completa' || Session::getInstance()->membresia->membresia == 'Media') && 
                            (Session::getInstance()->privileges->access == 1 || (isset(Session::getInstance()->access_fincas->access_special) && Session::getInstance()->access_fincas->access_special > 0))):
                    ?>
                        <li class="nav-item start <?php if($page == 'index'){ ?> active open <? } ?>">
                            <a href="index.php" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Inicio</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <?php
                        endif;
                        ?>

                        <?php
                            if(isset(Session::getInstance()->logged) && Session::getInstance()->logged === 5):
                        ?>
                        <li class="nav-item start <?php if($page == 'weekReport'){ ?> active open <? } ?>">
                            <a href="index.php" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Reporte semanal</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                                <li class="nav-item start <?php if($page == 'historyReport'){ ?> active open <? } ?>">
                                    <a href="index.php" class="nav-link nav-toggle">
                                        <i class="icon-home"></i>
                                        <span class="title">Reporte semanal</span>
                                        <span class="selected"></span>
                                    </a>
                                </li>
                        <?php
                            endif;
                        ?>
                        <?php
                        if(isset(Session::getInstance()->client) && isset(Session::getInstance()->finca)):
                        ?>
                        <li class="heading">
                            <h3 class="uppercase">Menu</h3>
                        </li>
                        <?php
                        if(isset(Session::getInstance()->privileges)): 
                            if(Session::getInstance()->privileges->climas == 'Activo'): 
                        ?>
                        <li class="nav-item <?php if($page == 'clima'){ ?> active <? } ?>">
                            <a href="/clima" class="nav-link nav-toggle hide">
                                <i class="icon-puzzle"></i>
                                <span class="title">Clima</span>
                                <!--span class="arrow"></span-->
                            </a>
						</li>
                        <?php
                            endif;
                            if(Session::getInstance()->privileges->tresm == 'Activo'): 
                        ?>
                        <li class="nav-item hide <?php if($page == 'planta3m'){ ?> active <? } ?>">
                            <a href="planta3m" class="nav-link nav-toggle">
                                <i class="icon-settings"></i>
                                <span class="title">Plantas 3m</span>
                                <!--span class="arrow"></span-->
                            </a>
						</li>
                        <?php
                            endif;
                            if(Session::getInstance()->privileges->cerosem == 'Activo'): 
                        ?>
                        <li class="nav-item hide <?php if($page == 'planta0sem'){ ?> active <? } ?>">
                            <a href="planta0sem" class="nav-link nav-toggle">
                                <i class="icon-settings"></i>
                                <span class="title">Plantas 0 Sem</span>
                                <!--span class="arrow"></span-->
                            </a>
						</li>
                        <?php
                            endif;
                            if(Session::getInstance()->privileges->oncesem == 'Activo'): 
                        ?>
                        <li class="nav-item hide <?php if($page == 'planta11sem'){ ?> active <? } ?>">
                            <a href="planta11sem" class="nav-link nav-toggle">
                                <i class="icon-settings"></i>
                                <span class="title">Plantas 11 Sem</span>
                                <!--span class="arrow"></span-->
                            </a>
						</li>
                        <?php
                            endif;
                            if(Session::getInstance()->privileges->foliar == 'Activo'): 
                        ?>
                        <li class="nav-item hide <?php if($page == 'foliar'){ ?> active <? } ?>">
                            <a href="foliar" class="nav-link nav-toggle">
                                <i class="icon-layers"></i>
                                <span class="title">Emision foliar</span>
                                <!--span class="arrow"></span-->
                            </a>
						</li>

                        <li class="nav-item <?php if($page == 'inspeccion' || $page == 'inspeccionSemanal'){ ?> active open <? } ?>">
                            <a href="javascript" class="nav-link nav-toggle">
                                <i class="icon-layers"></i>
                                <span class="title">Inspección</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <? if(count($fincas) > 1): ?>
                                <li class="nav-item <?php if($page == 'inspeccionSemanal'){ ?> active <? } ?>">
                                    <a href="inspeccionSemanal" class="nav-link nav-toggle">
                                        <span class="title">Semanal</span>
                                    </a>
                                </li>
                                <? endif; ?>
                                <li class="nav-item <?php if($page == 'inspeccion'){ ?> active <? } ?>">
                                    <a href="inspeccion" class="nav-link nav-toggle">
                                        <span class="title">Historico</span>                                        
                                    </a>
                                </li>
                            </ul>
						</li>

                        <?php
                            endif;
                            if(Session::getInstance()->privileges->fotos == 'Activo' && Session::getInstance()->logged != 62): 
                        ?>
                        <li class="nav-item <?php if($page == 'fotos'){ ?> active <? } ?>">
                            <a href="fotos" class="nav-link nav-toggle">
                                <i class="icon-wallet"></i>
                                <span class="title">Fotos</span>
                                <!--span class="arrow"></span-->
                            </a>
						</li>
                        <?php
                            endif;
                            if(Session::getInstance()->privileges->mapas == 'Activo' && Session::getInstance()->isAdmin == 1): 
                        ?>
                        <li class="nav-item <?php if($page == 'mapas'){ ?> active <? } ?>">
                            <a href="mapas" class="nav-link nav-toggle">
                                <i class="icon-pointer"></i>
                                <span class="title">Mapas</span>
                                <!--span class="arrow"></span-->
                            </a>
                        </li>
                        <?php
                            endif;
                            if(Session::getInstance()->privileges->ciclos == 'Activo'): 
                        ?>
                        <li class="nav-item <?= (Session::getInstance()->tipo == 'SIGAT' ? 'hide' : '') ?> <?php if($page == 'listCiclos' || $page == 'listProductos' || $page == 'importarInformacionCiclos'){ ?> open <? } ?>">
                            <a href="javascript:;" class="nav-link nav-toggle ">
                                <i class="icon-puzzle"></i>
                                <span class="title">Aplicación</span>
                                <span class="arrow <?php if($page == 'listCiclos' || $page == 'listProductos' || $page == 'importarInformacionCiclos'){ ?> open <? } ?>"></span>
                            </a>
                            <ul class="sub-menu" style="<?php if($page == 'listCiclos' || $page == 'listProductos' || $page == 'importarInformacionCiclos'){ ?> display: block; <? } ?>">
                                <li class="nav-item <?php if($page == 'listCiclos'){ ?> active <? } ?>">
                                    <a href="listCiclos" class="nav-link nav-toggle">
                                        <i class="icon-settings"></i>
                                        <span class="title">Programa de Aplicación</span>
                                        <!--span class="arrow"></span-->
                                    </a>
                                </li>
                                <li class="nav-item <?php if($page == 'listProductos'){ ?> active <? } ?>">
                                    <a href="listProductos" class="nav-link nav-toggle">
                                        <i class="icon-settings"></i>
                                        <span class="title">Productos</span>
                                        <!--span class="arrow"></span-->
                                    </a>
                                </li>
                                <li class="nav-item <?php if($page == 'sectoresList'){ ?> active <? } ?>">
                                    <a href="sectoresList" class="nav-link nav-toggle">
                                        <i class="icon-settings"></i>
                                        <span class="title">Sectores</span>
                                        <!--span class="arrow"></span-->
                                    </a>
                                </li>
                                <li class="nav-item <?php if($page == 'programasList'){ ?> active <? } ?>">
                                    <a href="programasList" class="nav-link nav-toggle">
                                        <i class="icon-settings"></i>
                                        <span class="title">Programas</span>
                                        <!--span class="arrow"></span-->
                                    </a>
                                </li>
                                <li class="nav-item <?php if($page == 'tiposCiclosList'){ ?> active <? } ?>">
                                    <a href="tiposCiclosList" class="nav-link nav-toggle">
                                        <i class="icon-settings"></i>
                                        <span class="title">Tipos de Ciclos</span>
                                        <!--span class="arrow"></span-->
                                    </a>
                                </li>
                                <li class="nav-item <?php if($page == 'fumigadorasList'){ ?> active <? } ?>">
                                    <a href="fumigadorasList" class="nav-link nav-toggle">
                                        <i class="icon-settings"></i>
                                        <span class="title">Fumigadoras</span>
                                        <!--span class="arrow"></span-->
                                    </a>
                                </li>
                                <li class="nav-item <?php if($page == 'placasList'){ ?> active <? } ?>">
                                    <a href="placasList" class="nav-link nav-toggle">
                                        <i class="icon-settings"></i>
                                        <span class="title">Placas de Avión</span>
                                        <!--span class="arrow"></span-->
                                    </a>
                                </li>
                                <li class="nav-item <?php if($page == 'pilotosList'){ ?> active <? } ?>">
                                    <a href="pilotosList" class="nav-link nav-toggle">
                                        <i class="icon-settings"></i>
                                        <span class="title">Pilotos</span>
                                        <!--span class="arrow"></span-->
                                    </a>
                                </li>
                                <li class="nav-item <?php if($page == 'importarInformacionCiclos'){ ?> active <? } ?>">
                                    <a href="importarInformacionCiclos" class="nav-link nav-toggle">
                                        <i class="icon-settings"></i>
                                        <span class="title">Importar Información</span>
                                        <!--span class="arrow"></span-->
                                    </a>
                                </li>
                            </ul>
                        </li>
                        
                        <?php
                            endif;
                            if((Session::getInstance()->membresia->membresia == 'Completa' || Session::getInstance()->membresia->membresia == 'Media') && 
                                Session::getInstance()->privileges->informe == 'Activo' &&
                                Session::getInstance()->isAdmin == 0):
                        ?>
                        <li class="nav-item <?php if($page == 'listInformes'){ ?> active <? } ?>">
                            <a href="listInformes" class="nav-link nav-toggle">
                                <i class="icon-briefcase"></i>
                                <span class="title">Informes</span>
                                <!--span class="arrow"></span-->
                            </a>
                        </li>
                        <?php
                            endif;
                            if((Session::getInstance()->membresia->membresia == 'Completa' || Session::getInstance()->membresia->membresia == 'Media') && 
                                Session::getInstance()->privileges->access == 1 && 
                                Session::getInstance()->privileges->informe == 'Activo'):
                        ?>
                        <li class="nav-item <?php if($page == 'informe'){ ?> active <? } ?>">
                            <a href="informe" class="nav-link nav-toggle">
                                <i class="icon-briefcase"></i>
                                <span class="title">Enviar informe</span>
                                <!--span class="arrow"></span-->
                            </a>
                        </li>
                        <?php
                        endif;
                        endif;
                        ?>
                        <?php
                        endif;
                            
                            if((Session::getInstance()->membresia->membresia == 'Completa' || Session::getInstance()->membresia->membresia == 'Media') && 
                                Session::getInstance()->privileges->access == 1):
                        ?>
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-puzzle"></i>
                                    <span class="title">Administracion</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item ">
                                        <a href="javascript:;" class="nav-link nav-toggle">
                                            <span class="title">Productores</span>
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li class="nav-item ">
                                                <a href="listProduc" class="nav-link "> Listado </a>
                                            </li>
                                            <li class="nav-item ">
                                                <a href="newProduc" class="nav-link "> Nuevo Productor </a>
                                            </li>
                                        </ul>
                                    </li>
                                   <li class="nav-item  ">
                                        <a href="javascript:;" class="nav-link nav-toggle">
                                            <span class="title">Fincas</span>
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li class="nav-item ">
                                                <a href="haciendaList" class="nav-link "> Listado </a>
                                            </li>
                                            <li class="nav-item ">
                                                <a href="newHacienda" class="nav-link "> Nueva Finca </a>
                                            </li>
                                        </ul>
                                    </li>
                                   <li class="nav-item  ">
                                        <a href="javascript:;" class="nav-link nav-toggle">
                                            <span class="title">Agrupaciones</span>
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li class="nav-item ">
                                                <a href="agrupacionList" class="nav-link "> Listado </a>
                                            </li>
                                            <li class="nav-item ">
                                                <a href="newAgrupacion" class="nav-link "> Nueva Agrupacion </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="nav-item">
                                        <a href="importarInformacion" class="nav-link nav-toggle">
                                            <span class="title">Importar Información</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php
                            endif;
                            
                            $isDemo = Session::getInstance()->logged == 57;
                            if((Session::getInstance()->membresia->membresia == 'Completa' || Session::getInstance()->membresia->membresia == 'Media') && 
                                Session::getInstance()->privileges->configuracion == 'Activo' && Session::getInstance()->privileges->fincas != 'READ_OWN_WRITE_OWN'):
                        ?>
                            <li class="nav-item">
                                <a href="resumenCiclos<?= $isDemo ? 'Demo' : '' ?>" class="nav-link">
                                    <i class="icon-home"></i>
                                    <span class="title">Inicio</span>
                                </a>
                            </li>
                            <li class="nav-item <?= in_array($page, $programa) ? ' active open ' : '' ?>">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-plane"></i>
                                    <span class="title">Programas</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($page == 'programaTecnico'){ ?> active <? } ?>">
                                        <a href="programaTecnico<?= $isDemo ? 'Demo' : '' ?>" class="nav-link">
                                            <i class="fa fa-info"></i>
                                            <span class="title">Técnico</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($page == 'programaEconomico'){ ?> active <? } ?>">
                                        <a href="programaEconomico<?= $isDemo ? 'Demo' : '' ?>" class="nav-link">
                                            <i class="glyphicon glyphicon-euro"></i>
                                            <span class="title">Económico</span>
                                        </a>
                                    </li>
                                    <li class="nav-item hide <?php if($page == 'otros'){ ?> active <? } ?>">
                                        <a href="otros<?= $isDemo ? 'Demo' : '' ?>" class="nav-link">
                                            <span class="title">Foliares</span>
                                        </a>
                                    </li>
                                    <li class="nav-item hide <?= $isDemo ? 'hide' : '' ?> <?= $page == 'plagas' ? 'active' : '' ?>">
                                        <a href="plagas" class="nav-link">
                                            <span class="title">Plagas</span>
                                        </a>
                                    </li>
                                    <li class="nav-item hide <?= $isDemo ? 'hide' : '' ?> <?= $page == 'otrosSigatokas' ? 'active' : '' ?>">
                                        <a href="otrosSigatokas" class="nav-link">
                                            <span class="title">Otros Sigatokas</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item <?= in_array($page, $orodeltiReportes) ? 'active' : '' ?>">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-television"></i>
                                    <span class="title">Reportes</span>
                                    <span class="arrow <?= in_array($page, $orodeltiReportes) ? 'open' : '' ?>"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?= $page == 'tecnicos' ? 'active' : '' ?>">
                                        <a href="tecnicos<?= $isDemo ? 'Demo' : '' ?>" class="nav-link">
                                            <i class="fa fa-info"></i>
                                            <span class="title">Técnico</span>
                                        </a>
                                    </li>
                                    <?php if(Session::getInstance()->privileges->costos_orodelti == 'READ'): ?>
                                    <li class="nav-item <?= $page == 'analisisProductos' ? 'active' : '' ?>">
                                        <a href="analisisProductos" class="nav-link">
                                            <i class="fa fa-copyright"></i>
                                            <span class="title">Análisis Productos</span>
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                    <li class="nav-item <?= $page == 'climaOrodelti' ? 'active' : '' ?>">
                                        <a href="climaOrodelti<?= $isDemo ? 'Demo' : '' ?>" class="nav-link">
                                            <i class="glyphicon glyphicon-cloud"></i>
                                            <span class="title">Clima</span>
                                        </a>
                                    </li>
                                    <li class="nav-item hide <?= $page == 'climaDiarioOrodelti' ? 'active' : '' ?>">
                                        <a href="climaDiarioOrodelti" class="nav-link">
                                            <i class="glyphicon glyphicon-cloud"></i>
                                            <span class="title">Clima Día</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?= $page == 'costos' ? 'active' : '' ?>">
                                        <a href="costos" class="nav-link">
                                            <i class="glyphicon glyphicon-th"></i>
                                            <span class="title">Matriz Excel</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item <?= in_array($page, $orodeltiInspeccion) ? 'active' : '' ?>">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-leaf"></i>
                                    <span class="title">Inspección</span>
                                    <span class="arrow <?= in_array($page, $orodeltiInspeccion) ? 'open' : '' ?>"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?= $page == 'inspeccionOrodelti' ? 'active' : '' ?>">
                                        <a href="inspeccionOrodelti<?= $isDemo ? 'Demo' : '' ?>" class="nav-link">
                                            <i class="fa fa-file-excel-o"></i>
                                            <span class="title">Inspección Excel</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?= $page == 'resumenInspeccionOrodelti' ? 'active' : '' ?>">
                                        <a href="resumenInspeccionOrodelti" class="nav-link">
                                            <i class="fa fa-laptop"></i>
                                            <span class="title">Resumen</span>
                                        </a>
                                    </li>
                                    <!--
                                    <li class="nav-item <?= in_array($page, $inspeccion_orodelti_app) ? 'active open' : '' ?>">
                                        <a href="javascript:;" class="nav-link">
                                            <i class="fa fa-mobile"></i>
                                            <span class="title">Inspección App</span>
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li class="nav-item <?= $page == 'inspeccionOrodeltiApp' ? ' active ' : '' ?>">
                                                <a href="inspeccionOrodeltiApp" class="nav-link">
                                                    <span class="title">Semanal</span>
                                                </a>
                                            </li>
                                            <li class="nav-item <?= $page == 'inspeccionOrodeltiAppGraficas' ? ' active ' : '' ?>">
                                                <a href="inspeccionOrodeltiAppGraficas" class="nav-link">
                                                    <span class="title">Comparativo</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    -->
                                </ul>
                            </li>
                            <li class="nav-item <?= $isDemo || !in_array(Session::getInstance()->privileges->orodelti, $permission_write) ? 'hide' : '' ?> <?= in_array($page, $config) ? 'active' : '' ?>">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-cog   "></i>
                                    <span class="title">Configuración</span>
                                    <span class="arrow <?= in_array($page, $config) ? 'open' : '' ?>"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($page == 'gerentesList'){ ?> active <? } ?>">
                                        <a href="gerentesList" class="nav-link nav-toggle">
                                            <span class="title">Gerente</span>
                                            <!--<span class="arrow"></span>-->
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($page == 'fincaList'){ ?> active <? } ?> ">
                                        <a href="fincaList" class="nav-link nav-toggle">
                                            <span class="title">Finca</span>
                                            <!--<span class="arrow"></span>-->
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($page == 'fincasHectareasOrodelti'){ ?> active <? } ?> ">
                                        <a href="fincasHectareasOrodelti" class="nav-link nav-toggle">
                                            <span class="title">Hectareas</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($page == 'productos'){ ?> active <? } ?>">
                                        <a href="productos" class="nav-link nav-toggle">
                                            <span class="title">Productos</span>
                                            <!--<span class="arrow"></span>-->
                                        </a>
                                        <!--<ul class="sub-menu">
                                            <li class="nav-item ">
                                                <a href="productos" class="nav-link "> Listado </a>
                                            </li>
                                        </ul>-->
                                    </li>
                                   <li class="nav-item <?php if($page == 'fumigadorasList'){ ?> active <? } ?>">
                                        <a href="fumigadorasList" class="nav-link nav-toggle">
                                            <span class="title">Fumigadoras</span>
                                        </a>
                                        <!--<ul class="sub-menu">
                                            <li class="nav-item ">
                                                <a href="fumigadorasList" class="nav-link "> Listado </a>
                                            </li>
                                            <li class="nav-item ">
                                                <a href="newFumigadora" class="nav-link "> Nueva Fumigadora </a>
                                            </li>
                                        </ul>-->
                                    </li>
                                    <!--<li class="nav-item  <?php if($page == 'placasList'){ ?> active <? } ?>">
                                        <a href="placasList" class="nav-link nav-toggle">
                                            <span class="title">Placas</span>
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li class="nav-item ">
                                                <a href="placasList" class="nav-link "> Listado </a>
                                            </li>
                                            <li class="nav-item ">
                                                <a href="newPlaca" class="nav-link "> Nueva Placa </a>
                                            </li>
                                        </ul>
                                    </li>-->
                                    <li class="nav-item <?php if($page == 'pilotos'){ ?> active <? } ?>">
                                        <a href="pilotos" class="nav-link nav-toggle">
                                            <span class="title">Piloto</span>
                                            <!--<span class="arrow"></span>-->
                                        </a>
                                        <!--<ul class="sub-menu">
                                            <li class="nav-item ">
                                                <a href="pilotosList" class="nav-link "> Listado </a>
                                            </li>
                                            <li class="nav-item ">
                                                <a href="newPiloto" class="nav-link "> Nuevo Piloto </a>
                                            </li>
                                        </ul>-->
                                    </li>
                                    <!-- <li class="nav-item">
                                        <a href="importarInformacion" class="nav-link nav-toggle">
                                            <span class="title">Importar Información</span>
                                        </a>
                                    </li> -->
                                </ul>
                            </li>
                        <?php
                            endif;

                            if((Session::getInstance()->membresia->membresia == 'Completa' || Session::getInstance()->membresia->membresia == 'Media') && 
                                Session::getInstance()->privileges->configuracion == 'Activo' && Session::getInstance()->privileges->fincas == 'READ_OWN_WRITE_OWN'):
                        ?>
                            <li class="nav-item <?= ($page == 'programaTecnico' || $page == 'programaEconomico') ? 'open' : '' ?>">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-settings"></i>
                                    <span class="title">Programas</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($page == 'programaTecnico'){ ?> active <? } ?>">
                                        <a href="programaTecnico" class="nav-link">
                                            <span class="title">Técnico</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-puzzle"></i>
                                    <span class="title">Reportes</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="tecnicos" class="nav-link">
                                            <span class="title">Técnico</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php
                            endif;
                        ?>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->