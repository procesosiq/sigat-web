<?php
	require("controllers/conexion.php");
	$token = $_GET["token"];
	$conexion = new M_Conexion;
	$sql = "SELECT (SELECT nombre FROM cat_clientes WHERE id = id_cliente) AS Productor ,id_hacienda ,id_cliente,descripcion 			,
					(SELECT nombre FROM cat_haciendas WHERE id = id_hacienda) AS Finca, 
					WEEK(fecha) as semana FROM informe WHERE token = '{$token}'";
	$datos = $conexion->Consultas(2, $sql);
	$sql_evidencias = "SELECT * FROM foliar WHERE YEAR(fecha) = YEAR(CURRENT_DATE) AND WEEK(fecha) =  ".$datos[0]["semana"]." AND id_hacienda = ".$datos[0]["id_hacienda"]." AND id_cliente = ".$datos[0]["id_cliente"]." AND (evi_malezas IS NOT NULL OR evi_deshoje IS NOT NULL OR evi_deshojefito IS NOT NULL OR evi_enfunde IS NOT NULL OR evi_drenaje IS NOT NULL OR evi_plaga IS NOT NULL OR evi_otrasobs IS NOT NULL)";
	$evidencias = $conexion->Consultas(2, $sql_evidencias);

?>
<!DOCTYPE html>
	<head>
		<meta charset="utf-8" />
		<style>
			html{
				padding: 15px;
			}
			body{
				background: #ffffff !important;
    			/*zoom: 0.55;  using 0.55 or higher results in the elements not lining up correctly */
			}
			.full{
				display: block;
				text-align: center;
			}
			.max_width{
				width: 100%;
			}
			#parent{
				display: inline-block;
				padding-left: 1em;
			}
			.center {
			    display: inline-block;
			    margin: 0 auto;
			}
			.left {
			    display: inline-block;
			    margin: 0 0 0 auto;
			}
			.right {
			    display: inline-block;
			    margin: 0 auto 0 0;
			}
			.chart{
				width: 100% !important;
				height: 300px !important;
			}
			.portlet.light.portlet-fit>.portlet-title{
				padding: 1px 20px 1px !important;
			}
			.unbreakable
			{
			    /* page-break-inside: avoid !important; <-- doesn't work*/
			    display:inline-block;
			}
			.unbreakable:after
			 {
			    display:block;
			    height:0px;
			    visibility: hidden;
			}
		</style>
	</head>
	<body id="contenedor" ng-controller="graficas" ng-cloak>
		<input id="token" type="text" value="<?= $token ?>" style="display:none;"/>
		<div id="report" ng-init="init()">
			<div style="display:none"><img style="display:inline-block;" src="http://sigat.procesos-iq.com/logo_grande.png"/></div>
			<div id="page_one"> <!-- content --></div>
				<div id="parent"><b>PRODUCTOR:</b><?php echo " ".$datos[0]["Productor"]; ?></div>
				<div id="parent"><b>FINCA:</b><?php echo " ".$datos[0]["Finca"]; ?></div>
				<div id="parent"><b>SEMANA:</b><?php echo " ".$datos[0]["semana"]; ?></div>
				
				<br>
				<b>1. RESUMEN EJECUTIVO</b>
				<div id='contentText' style="border : 0px solid black; padding-left : 15px; padding-right : 15px; height:auto ;"><!-- 850px 1 hoja -->
					<div>
						<?php 

						echo str_replace("font-size: 12.8px;", "font-size: 1pt;", $datos[0]["descripcion"]);

						?>
					</div>
					<!--<div>Este informe corresponde a la semana 4 del año 2016.</div>
					<div><br></div>
					<div>Continúan bajos los niveles de infección, aunque se reitera que el organismo causal ya se está&nbsp;<span style="line-height: 1.42857;">desarrollando dentro de la hoja y, en cualquier momento aparece en hojas 3 o 4. Por el momento,&nbsp;</span><span style="line-height: 1.42857;">el hongo está confinado a la hoja 5 de plantas jóvenes.</span></div>
					<div><br></div>
					<div>Los demás parámetros están normales.</div>
					<div><br></div>
					<div>En cuanto a clima, la temperatura mantiene niveles bastante elevados y las lluvias ya están&nbsp;<span style="line-height: 1.42857;">posesionadas en todas las zonas con mayor o menor intensidad. Los reportes de NOAA siguen&nbsp;</span><span style="line-height: 1.42857;">manifestando amenaza o presencia de El Niño.</span></div>
					<div><br></div>
					<div>La emisión foliar tiende al incremento que es característica de la época lluviosa (0.8 y 0.81&nbsp;<span style="line-height: 1.42857;">hojas/semana).</span></div>
					<div><br></div>
					<div>Por lo tanto se recomienda optimizar el manejo de la finca con énfasis en las labores culturales&nbsp;<span style="line-height: 1.42857;">relacionadas con sigatoka, así como seguir con las frecuencias cortas.</span></div>
					<div><br></div>
					<div>Los ácaros siguen presentes en las hojas. Esperemos que el último ciclo con fungicidas en&nbsp;<span style="line-height: 1.42857;">emulsión con aceite haya contribuido al control de esta plaga</span></div>
					<div><br></div>
					<div><span style="font-weight: bold;">Ciclo # 1. Enero 6/2016 a 15 días de frecuencia.Sopral + Tridetox</span></div>
					<div><span style="font-weight: bold; line-height: 1.42857;">Ciclo # 2. Enero 15 a 9 días. Odeon&nbsp;</span><br></div>
					<div><span style="font-weight: bold; line-height: 1.42857;">Ciclo # 3. Enero 22 a 7 días de frecuencia. Odeon</span><br></div>
					<div><span style="font-weight: bold; line-height: 1.42857;">Ciclo # 4. Enero 29 a 7 días. Volley + Mancozeb</span><br></div>
					<div><span style="color: rgb(255, 0, 0); font-weight: bold; line-height: 1.42857;"><br></span></div>
					<div><span style="color: rgb(255, 0, 0); font-weight: bold; line-height: 1.42857;">Ciclo # 5. Febrero 9 a 10 días.</span></div>
					<div><span style="font-weight: bold; line-height: 1.42857;"><br></span></div>
					<div><span style="font-weight: bold; line-height: 1.42857;">Odeon 2.0 L/ha&nbsp;</span><span style="color: rgb(255, 0, 0); font-weight: bold; line-height: 1.42857;"><br></span></div>
					<div><span style="font-weight: bold;"><span style="line-height: 1.42857;">Completar con agua a 6 gls/ha</span><br></span></div>
					<div><span style="font-weight: bold;"><span style="line-height: 1.42857;">En este ciclo se puede aplicar fertilizantes foliares</span><br></span></div>
					<div><span style="color: rgb(255, 0, 0);"><br></span></div>
					<div><span style="color: rgb(255, 0, 0); font-weight: bold;">Ciclo # 6. Febrero 16 a 7 días&nbsp;</span></div>
					<div><br></div>
					<div><span style="font-weight: bold;">Odeon 2.0 L/ha&nbsp;</span></div>
					<div><span style="font-weight: bold;"><span style="line-height: 1.42857;">Completar con agua a 6 gls/ha</span><br></span></div>
					<div><span style="line-height: 1.42857; font-weight: bold;">En este ciclo se puede aplicar fertilizantes foliares</span><br></div>-->
					
				</div>
			</div>
			<div id="page_two" class="unbreakable">
                <div class="row">
                    <div class="col-md-6 col-sm-6" style="width: 810px;">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <!--<h3>
                                           <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
                                        </h3>-->
                                        <small>Estado evolutivo en Hoja 3</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hoja_3_3metros" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
                <div class="row">
                     <div class="col-md-6 col-sm-6" style="width: 810px;">
	                    <!-- BEGIN PORTLET-->
	                    <div class="portlet light portlet-fit bordered">
	                        <div class="portlet-title">
	                            <div class="caption">
	                                <span class="caption-subject font-dark sbold uppercase">
	                                    <!--<h3>
	                                       <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
	                                    </h3>-->
	                                    <small>Estado evolutivo de Hoja 4</small>
	                                </span>
	                            </div>
	                        </div>
	                        <div class="portlet-body">
	                            <div id="hoja_4_3metros" class="chart"> </div>
	                        </div>
	                    </div>
	                    <!-- END PORTLET-->
	                </div>
	            </div>
	            <div class="unbreakable"></div>
	            <div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <!--<h3>
                                           <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
                                        </h3>-->
                                        <small>Estado evolutivo en Hoja 5</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hoja_5_3metros" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
            </div>
            <div class="unbreakable"><!-- content --></div>
            <div id="page_three">
				<!-- BEGIN INTERACTIVE CHART PORTLET-->
                <div class="row">
                    <div class="col-md-6 col-sm-6" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
                                        </h3>
                                        <small>Hoja vieja mas libre estrias (H + VLE)</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="libre_estrias_3metros" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
	            <!-- END INTERACTIVE CHART PORTLET-->
	            <!-- END DASHBOARD STATS 1-->
	            <div class="row">
	                <div class="col-md-6 col-sm-6" style="width: 810px;">
	                    <!-- BEGIN PORTLET-->
	                    <div class="portlet light portlet-fit bordered">
	                        <div class="portlet-title">
	                            <div class="caption">
	                                <span class="caption-subject font-dark sbold uppercase">
	                                    <h3>
	                                       <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
	                                    </h3>
	                                    <small>Hoja vieja mas libre de quema menor al 5%</small>
	                                </span>
	                            </div>
	                        </div>
	                        <div class="portlet-body">
	                            <div id="hoja_vieja_menor_5_3metros" class="chart"> </div>
	                        </div>
	                    </div>
	                    <!-- END PORTLET-->
	                </div>
	            </div>
                <!-- BEGIN INTERACTIVE CHART PORTLET-->
                <div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
                                        </h3>
                                        <small>Hojas totales</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hojas_total_3metros" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="unbreakable"><!-- content --></div>
			<div id="page_four">
				<div class="row">
	                <div class="col-md-12 col-sm-12" style="width: 810px;">
	                    <div class="portlet light portlet-fit bordered">
	                        <div class="portlet-title">
	                            <div class="caption">
	                                <span class="caption-subject font-dark sbold uppercase">
	                                    <h3>
	                                       <i class="icon-settings font-dark"></i> Plantas 0 semanas
	                                    </h3>
	                                    <small>Hojas totales</small>
	                                </span>
	                            </div>
	                        </div>
	                        <div class="portlet-body">
	                            <div id="hojas_total" class="chart"> </div>
	                        </div>
	                    </div>
	                </div>
	        	</div>
		        <!-- END INTERACTIVE CHART PORTLET-->
		        <!-- END DASHBOARD STATS 1-->
		        <div class="row">
		            <div class="col-md-12 col-sm-12" style="width: 810px;">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small >Hoja mas vieja libre de quema menor a 5%</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div  id="hoja_vieja_menor_5" class="chart"> </div>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		        </div>
		    </div>
		    <div class="unbreakable"><!-- content --></div>
		    <div id="page_five">
		    	<div class="row">
		            <div class="col-md-12 col-sm-12" style="width: 810px;">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Hoja mas vieja libre de estrias</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="libre_estrias" class="chart"> </div>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		        </div>
	        	<!-- END DASHBOARD STATS 1-->
	        	<div class="row">
		            <div class="col-md-6 col-sm-6" style="width: 810px;">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Libre de cirugia</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="libre_cirugia" class="chart"> </div>
		                    </div>
		                </div>
	                <!-- END PORTLET-->
	            	</div>
	            </div>
	        	<div class="row">
		            <div class="col-md-6 col-sm-6" style="width: 810px;">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Hoja mas vieja libre de quema mayor a 5%</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="hoja_vieja_mayor_5" class="chart"> </div>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		        </div>
		    </div>
		    <div class="unbreakable"><!-- content --></div>
            <div id="page_six">
                <!-- END PAGE BASE CONTENT -->
                <div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas 11 semanas
                                        </h3>
                                        <small>Hojas totales</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hojas_total_11semanas" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas 11 semanas
                                        </h3>
                                        <small>Hoja mas vieja libre de quema mayor a 5%</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hoja_vieja_mayor_5_11Semanas" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
			</div>
			<div class="unbreakable"><!-- content --></div>
			<div id="page_seven">
				<div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas 11 semanas
                                        </h3>
                                        <small>Hoja mas vieja libre de quema menor a 5%</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hoja_vieja_menor_5_11Semanas" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas 11 semanas
                                        </h3>
                                        <small>Libre de cirugia</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="libre_cirugia_11Semanas" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
			</div>
			<div class="unbreakable"><!-- content --></div>
			<div id="page_eight">
				<div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Emision Foliar
                                        </h3>
                                        <!-- <small>Hojas totales</small> -->
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="foliar" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<div class="unbreakable"><!-- content --></div>
			<div id="page_nine">
				<div id="nine_one">
					<div class="row">
		                <div class="col-md-12 col-sm-12" style="width: 810px;">
		                    <div class="portlet light portlet-fit bordered">
		                        <div class="portlet-title">
		                            <div class="caption">
		                                <span class="caption-subject font-dark sbold uppercase">
		                                	<h3>
		                                		<i class="icon-settings font-dark"></i> Clima
		                                	</h3>
		                                	<small>Temperatura minima</small>
		                                </span>
		                            </div>
		                        </div>
		                        <div class="portlet-body">
		                            <div id="temp_min" class="chart"> </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
	        	</div>
	        	<div id="nine_two">
		            <div class="row">
	                    <div class="col-md-12 col-sm-12" style="width: 810px;">
	                        <div class="portlet light portlet-fit bordered">
	                            <div class="portlet-title">
	                                <div class="caption">
	                                    <i class="icon-settings font-dark"></i>
	                                    <span class="caption-subject font-dark sbold uppercase">Precipitacion (mm lluvia)</span>
	                                </div>
	                            </div>
	                            <div class="portlet-body">
	                                <div id="precp" class="chart"> </div>
	                            </div>
	                        </div>
	                    </div>
					</div>
					<div class="row">
	                    <div class="col-md-12 col-sm-12" style="width: 810px;">
	                        <div class="portlet light portlet-fit bordered">
	                            <div class="portlet-title">
	                                <div class="caption">
	                                    <i class="icon-settings font-dark"></i>
	                                    <span class="caption-subject font-dark sbold uppercase">Temperatura Maxima</span>
	                                </div>
	                            </div>
	                            <div class="portlet-body">
	                                <div id="temp_max" class="chart"> </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
            	</div>
			</div>
			<div class="unbreakable"><!-- content --></div>
			<div id="page_ten">
				<div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Radiacion Solar</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="radiacion" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Dias Sol</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="diassol" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Humedad Relativa Maxima</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="humedadmax" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<div class="unbreakable"><!-- content --></div>
			<div id="page_eleven">
				<div class="row">
					<div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Humedad Relativa Minima</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="humedadmin" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<?php
				$cont = 0;
				foreach ($evidencias as $key => $value) {
					if($value["malezas"] != '(NULL)' && $value["malezas"] != ""){
						$cont++;
						$ruta = "http://sigat.procesos-iq.com/".$value["evi_malezas"];

						echo "
							<div style='width:250px; display:inline-block;'>
								<b>LABOR :</b><br> MALEZAS <br>
								<b>DESCRIPCIÓN:</b><br>". $value["malezas"]."<br>
								".(($value["evi_malezas"]!="(NULL)")? "'<img height='350' width='250' src='".$ruta."'/>" : "''")."
							</div>
						";

						if(($cont%3) == 0)
							echo "<br>";
					}
					if($value["deshoje"] != '(NULL)' && $value["deshoje"] != ""){
						$cont++;
						$ruta = "http://sigat.procesos-iq.com/".$value["evi_deshoje"];

						echo "
							<div style='width:250px; display:inline-block;'>
								<b>LABOR:</b><br>DESHOJE <br>
								<b>DESCRIPCIÓN:</b><br>". $value["deshoje"]."<br>
								".(($value["evi_deshoje"]!="(NULL)")? "'<img height='350' width='250' src='".$ruta."'/>" : "''")."
							</div>
						";
						if(($cont%3) == 0)
							echo "<br>";
					}
					if($value["deshojefito"] != '(NULL)' && $value["deshojefito"] != ""){
						$cont++;
						$ruta = "http://sigat.procesos-iq.com/".$value["evi_deshojefito"];

						echo "
							<div style='width:250px; display:inline-block;'>
								<b>LABOR:</b><br>DESHOJEFITO <br>
								<b>DESCRIPCIÓN:</b><br>". $value["deshojefito"]."<br>
								".(($value["evi_deshojefito"]!="(NULL)")? "'<img height='350' width='250' src='".$ruta."'/>" : "''")."
							</div>
						";
						if(($cont%3) == 0)
							echo "<br>";
					}
					if($value["enfunde"] != '(NULL)' && $value["enfunde"] != ""){
						$cont++;
						$ruta = "http://sigat.procesos-iq.com/".$value["evi_enfunde"];

						echo "
							<div style='width:250px; display:inline-block;'>
								<b>LABOR:</b><br>ENFUNDE <br>
								<b>DESCRIPCIÓN:</b><br>". $value["enfunde"]."<br>
								".(($value["evi_enfunde"]!="(NULL)")? "'<img height='350' width='250' src='".$ruta."'/>" : "''")."
							</div>
						";
						if(($cont%3) == 0)
							echo "<br>";
					}
					if($value["drenaje"] != '(NULL)' && $value["drenaje"] != ""){
						$cont++;
						$ruta = "http://sigat.procesos-iq.com/".$value["evi_drenaje"];

						echo "
							<div style='width:250px; display:inline-block;'>
								<b>LABOR:</b><br>DRENAJES <br>
								<b>DESCRIPCIÓN:</b><br>". $value["drenaje"]."<br>
								".(($value["evi_drenaje"]!="(NULL)")? "'<img height='350' width='250' src='".$ruta."'/>" : "''")."
							</div>
						";
						if(($cont%3) == 0)
							echo "<br>";
					}
					if($value["plaga"] != '(NULL)' && $value["plaga"] != ""){
						$cont++;
						$ruta = "http://sigat.procesos-iq.com/".$value["evi_plaga"];

						echo "
							<div style='width:250px; display:inline-block;'>
								<b>LABOR:</b><br>PLAGA <br>
								<b>DESCRIPCIÓN:</b><br>". $value["plaga"]."<br>
								".(($value["evi_plaga"]!="(NULL)")? "'<img height='350' width='250' src='".$ruta."'/>" : "''")."
							</div>
						";
						if(($cont%3) == 0)
							echo "<br>";
					}
					if($value["otrasobs"] != '(NULL)' && $value["otrasobs"] != ""){
						$cont++;
						$ruta = "http://sigat.procesos-iq.com/".$value["evi_otrasobs"];

						echo "
							<div style='width:250px; display:inline-block;'>
								<b>LABOR:</b><br>OTRAS OBSERVACIÓNES <br>
								<b>DESCRIPCIÓN:</b><br>".$value["otrasobs"]."<br>
								".(($value["evi_otrasobs"]!="(NULL)")? "'<img height='350' width='250' src='".$ruta."'/>" : "''")."
							</div>
						";
						if(($cont%3) == 0)
							echo "<br>";
					}
				}
			?>
		</div>
		<!-- <div class="row">
        	<button ng-click="traer()" id="desPDF" type="button">Descargar PDF</button>
        </div> -->
	</body>
</html>