var NodePDF = require('nodepdf');
var token = process.argv[2]; /// PARAMETRO DESDE CONSOLA


// {
//     'viewportSize': {
//         'width': 2880,
//         'height': 1440
//     },
//     'paperSize': {
//         'format': 'A4',
//         'orientation': 'portrait',
//         'margin': {
//             'top': '1cm',
//             'right': '1cm',
//             'bottom': '1cm',
//             'left': '1cm'
//         }
//     },
//     'outputQuality': '80', //set embedded image quality 0 - 100
//     'zoomFactor': 1,
//     'args': '',
//     'captureDelay': 400

// }
// last argument is optional, sets the width and height for the viewport to render the pdf from. (see additional options)
var pdf = new NodePDF('http://sigat.procesos-iq.com/printPDF.php?page=informe_pdf&token='+token, '../informes/'+token+'.pdf', { 
	'viewportSize': {
        'width': 2880,
        'height': 1440
    },
	'paperSize': {
		'pageFormat': 'A4',
		'margin': {
			'top': '1cm'
		},
		'header': {
			'height': '2.3cm',
			'contents': '<div style="text-align:center; font-size:12px"><img style="display: block; margin: auto;" src="http://sigat.procesos-iq.com/logo_grande.png" height="20" width="80">INFORME DEL ESTADO EVOLUTIVO DE LA SIGATOKA NEGRA</div>' // If you have 2 pages the result looks like this: HEADER 1 / 2 
		},
		'footer': {
			'height': '1cm',
			'contents': 'Numero de paginas {currentPage} / {pages}'
		}
	},
    // 'args': '--debug=true',
    'outputQuality': '100', //set embedded image quality 0 - 100
    'captureDelay' : 15000
});

pdf.on('error', function(msg){
    console.log(msg);
});

pdf.on('done', function(pathToFile){
    console.log(pathToFile);
});

// listen for stdout from phantomjs
pdf.on('stdout', function(stdout){
     // handle
});

// listen for stderr from phantomjs
pdf.on('stderr', function(stderr){
    // handle
});