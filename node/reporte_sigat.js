var page = require('webpage').create();
var token = "59b90e1005a220e2ebc542eb9d950b";

page.paperSize ={
    format        : "A4",
    orientation    : "portrait",
    margin        : { left:"0.5cm", right:"0.5cm", top:"0.5cm", bottom:"0.5cm" },
    header        : {
        height        : "2.3cm",
        contents        : phantom.callback(function(pageNum, numPages){
            return('<div style="text-align:center; font-size:12px"><img style="display: block; margin: auto;" src="http://sigat.procesos-iq.com/logo_grande.png" height="20" width="80">INFORME DEL ESTADO EVOLUTIVO DE LA SIGATOKA NEGRA</div>');
        })
    },
    footer        : {
        height        : "1cm",
        contents        : phantom.callback(function(pageNum, numPages){
            return("Paginas : " + pageNum + " / " + numPages);
        })
    }
};

page.onInitialized = function() {
    if(page.injectJs('core.js')){
        console.log("Polyfill loaded");
    }    
};

page.open('http://sigat.procesos-iq.com/printPDF.php?page=informe_pdf&token='+token, function() {
   /*window.setTimeout(function () {
		page.render('../informes/'+token+'.pdf');
		phantom.exit();
    }, 60000);*/
});