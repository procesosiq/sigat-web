<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    ob_start();
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
                <!-- BEGIN CONTENT BODY -->
                <div class="container" id="contenedor" ng-app="app" ng-controller="3metros" ng-cloak>
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="col-sm-8 col-offset-4" ng-init="init()">
	                     <div class="row">
	                        <div class="col-md-12 col-sm-12">
	                            <div class="portlet light portlet-fit bordered">
	                                <div class="portlet-title">
	                                    <div class="caption">
	                                        <span class="caption-subject font-dark sbold uppercase">
	                                            <h4>
	                                               <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
	                                            </h4>
	                                            <small style="font-size: 70%">Hoja vieja mas libre estrias (H + VLE)</small>
	                                        </span>
	                                    </div>
	                                </div>
	                                <div class="portlet-body">
	                                    <div id="libre_estrias" style="position:absolute !important;" class="chart"> 
	                                    	
	                                    </div>
	                                    <img id="libre_estrias_img" style="display: none" src="">
	                                </div>
	                            </div>
	                        </div>
	                    </div>
						<div class="row">
	                        <div class="col-md-6 col-sm-6">
	                            <!-- BEGIN PORTLET-->
	                            <div class="portlet light portlet-fit bordered">
	                                <div class="portlet-title">
	                                    <div class="caption">
	                                        <span class="caption-subject font-dark sbold uppercase">
	                                            <h4>
	                                               <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
	                                            </h4>
	                                            <small style="font-size: 70%">Estado evolutivo de Hoja 4</small>
	                                        </span>
	                                    </div>
	                                </div>
	                                <div class="portlet-body">
	                                    <div id="hoja_4" class="chart"> </div>
	                                    <img id="hoja_4_img" style="display: none" src="">
	                                </div>
	                            </div>
	                            <!-- END PORTLET-->
	                        </div>
	                        <div class="col-md-6 col-sm-6">
	                            <!-- BEGIN PORTLET-->
	                            <div class="portlet light portlet-fit bordered">
	                                <div class="portlet-title">
	                                    <div class="caption">
	                                        <span class="caption-subject font-dark sbold uppercase">
	                                            <h4>
	                                               <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
	                                            </h4>
	                                            <small style="font-size: 70%">Hoja vieja mas libre de quema menor al 5%</small>
	                                        </span>
	                                    </div>
	                                </div>
	                                <div class="portlet-body">
	                                    <div id="hoja_vieja_menor_5" class="chart"> </div>
	                                    <img id="hoja_vieja_menor_5_img" style="display: none" src="">
	                                </div>
	                            </div>
	                            <!-- END PORTLET-->
	                        </div>
	                    </div>
						<div class="row">
	                        <div class="col-md-6 col-sm-6">
	                            <!-- BEGIN PORTLET-->
	                            <div class="portlet light portlet-fit bordered">
	                                <div class="portlet-title">
	                                    <div class="caption">
	                                        <span class="caption-subject font-dark sbold uppercase">
	                                            <h4>
	                                               <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
	                                            </h4>
	                                            <small style="font-size: 70%">Estado evolutivo en Hoja 3</small>
	                                        </span>
	                                    </div>
	                                </div>
	                                <div class="portlet-body">
	                                    <div id="hoja_3" class="chart"> </div>
	                                    <img id="hoja_3_img" style="display: none" src="">
	                                </div>
	                            </div>
	                            <!-- END PORTLET-->
	                        </div>
	                        <div class="col-md-6 col-sm-6">
	                            <!-- BEGIN PORTLET-->
	                            <div class="portlet light portlet-fit bordered">
	                                <div class="portlet-title">
	                                    <div class="caption">
	                                        <span class="caption-subject font-dark sbold uppercase">
	                                            <h4>
	                                               <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
	                                            </h4>
	                                            <small style="font-size: 70%">Estado evolutivo en Hoja 5</small>
	                                        </span>
	                                    </div>
	                                </div>
	                                <div class="portlet-body">
	                                    <div id="hoja_5" class="chart"> </div>
	                                    <img id="hoja_5_img" style="display: none" src="">
	                                </div>
	                            </div>
	                            <!-- END PORTLET-->
	                        </div>
	                    </div>
	                    <div style='page-break-before:always'></div>
	                    <div class="row">
	                        <div class="col-md-12 col-sm-12">
	                            <div class="portlet light portlet-fit bordered">
	                                <div class="portlet-title">
	                                    <div class="caption">
	                                        <span class="caption-subject font-dark sbold uppercase">
	                                            <h4>
	                                               <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
	                                            </h4>
	                                            <small style="font-size: 70%">Hojas totales</small>
	                                        </span>
	                                    </div>
	                                </div>
	                                <div class="portlet-body">
	                                    <div id="hojas_total" class="chart"> </div>
	                                    <img id="hojas_total_img" style="display: none" src="">
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                    <div class="row">
                    	<button ng-click="traer()" type="button">Descargar PDF</button>
                    </div>
                </div>
                <!-- END CONTENT BODY -->