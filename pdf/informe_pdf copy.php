<!DOCTYPE html>
	<head>
		<meta charset="utf-8" />
		<style>
			html{
				padding: 15px;
			}
			body{
				background: #ffffff !important;
			}
			.full{
				display: block;
				text-align: center;
			}
			.max_width{
				width: 100%;
			}
			#parent{
				display: flex;
			}
			.center {
			    display: inline-block;
			    margin: 0 auto;
			}
			.left {
			    display: inline-block;
			    margin: 0 0 0 auto;
			}
			.right {
			    display: inline-block;
			    margin: 0 auto 0 0;
			}
			.chart{
				width: 100% !important;
				height: 300px !important;
			}
		</style>
	</head>
	<body id="contenedor" ng-controller="graficas" ng-cloak>
		<div id="report" ng-init="init()">
			<div id="page_one">
				<span class="full"><b>INFORME DEL ESTADO EVOLUTIVO DE LA SIGATOKA NEGRA</b></span>
				<div id="parent"><div class="left"><b>FINCA:</b> <?= "NUEVA PUBENZA"?></div> <div class="center"><b>ZONA:</b> <?= "EL ORO"?></div> <div class="right"><b>SECTOR:</b> <?= "EL CAMBIO"?></div></div><br>
				<!-- <b>SEMANA</b><br> -->
				<!-- <table border="1" class="max_width">
					<tbody>
						<tr>
							<td>0<br>1</td>
							<td>0<br>2</td>
							<td>0<br>3</td>
							<td>0<br>4</td>
							<td>0<br>5</td>
							<td>0<br>6</td>
							<td>0<br>7</td>
							<td>0<br>8</td>
							<td>0<br>9</td>
							<td>1<br>0</td>
							<td>1<br>1</td>
							<td>1<br>2</td>
							<td>1<br>3</td>
							<td>1<br>4</td>
							<td>1<br>5</td>
							<td>1<br>6</td>
							<td>1<br>7</td>
							<td>1<br>8</td>
							<td>1<br>9</td>
							<td>2<br>0</td>
							<td>2<br>1</td>
							<td>2<br>2</td>
							<td>2<br>3</td>
							<td>2<br>4</td>
							<td>2<br>5</td>
							<td>2<br>6</td>
						</tr>
						<br>
						<tr>
							<td>2<br>7</td>
							<td>2<br>8</td>
							<td>2<br>9</td>
							<td>3<br>0</td>
							<td>3<br>1</td>
							<td>3<br>2</td>
							<td>3<br>3</td>
							<td>3<br>4</td>
							<td>3<br>5</td>
							<td>3<br>6</td>
							<td>3<br>7</td>
							<td>3<br>8</td>
							<td>3<br>9</td>
							<td>4<br>0</td>
							<td>4<br>1</td>
							<td>4<br>2</td>
							<td>4<br>3</td>
							<td>4<br>4</td>
							<td>4<br>5</td>
							<td>4<br>6</td>
							<td>4<br>7</td>
							<td>4<br>8</td>
							<td>4<br>9</td>
							<td>5<br>0</td>
							<td>5<br>1</td>
							<td>5<br>2</td>
						</tr>
					</tbody>
				</table>
				<br>
				<b>PERIODO</b>
				<table border="1" class="max_width">
					<tr>
						<td>01</td>
						<td>02</td>
						<td>03</td>
						<td>04</td>
						<td>05</td>
						<td>06</td>
						<td>07</td>
						<td>08</td>
						<td>09</td>
						<td>10</td>
						<td>11</td>
						<td>12</td>
						<td>13</td>
					</tr>
				</table> -->
				<b>1. RESUMEN EJECUTIVO</b>
				<div style="border : 1px solid black; padding-left : 15px; padding-right : 15px;">
					Este informe corresponde a la semana 4 del año 2016. 

					Continúan bajos los niveles de infección, aunque se reitera que el organismo causal ya se está 

					desarrollando dentro de la hoja y, en cualquier momento aparece en hojas 3 o 4. Por el momento, 

					el hongo está confinado a la hoja 5 de plantas jóvenes.

					Los demás parámetros están normales.

					En cuanto a clima, la temperatura mantiene niveles bastante elevados y las lluvias ya están 

					posesionadas en todas las zonas con mayor o menor intensidad. Los reportes de NOAA siguen 

					manifestando amenaza o presencia de El Niño.

					La emisión foliar tiende al incremento que es característica de la época lluviosa (0.8 y 0.81 

					hojas/semana).

					Por lo tanto se recomienda optimizar el manejo de la finca con énfasis en las labores culturales 

					relacionadas con sigatoka, así como seguir con las frecuencias cortas.

					Los ácaros siguen presentes en las hojas. Esperemos que el último ciclo con fungicidas en 

					emulsión con aceite haya contribuido al control de esta plaga
				</div>
			</div>
			<div id="page_two">
                <!-- END DASHBOARD STATS 1-->
                <span><b>GRAFICAS  ESTADO EVOLUTICO HOJAS  3 - 4 Y 5</b></span><br>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <!--<h3>
                                           <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
                                        </h3>-->
                                        <small>Estado evolutivo en Hoja 3</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hoja_3_3metros" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
	                    <!-- BEGIN PORTLET-->
	                    <div class="portlet light portlet-fit bordered">
	                        <div class="portlet-title">
	                            <div class="caption">
	                                <span class="caption-subject font-dark sbold uppercase">
	                                    <!--<h3>
	                                       <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
	                                    </h3>-->
	                                    <small>Estado evolutivo de Hoja 4</small>
	                                </span>
	                            </div>
	                        </div>
	                        <div class="portlet-body">
	                            <div id="hoja_4_3metros" class="chart"> </div>
	                        </div>
	                    </div>
	                    <!-- END PORTLET-->
	                </div>
	            </div>
	            <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <!--<h3>
                                           <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
                                        </h3>-->
                                        <small>Estado evolutivo en Hoja 5</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hoja_5_3metros" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
            </div>
            <div id="page_three">
				<!-- BEGIN INTERACTIVE CHART PORTLET-->
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
                                        </h3>
                                        <small>Hoja vieja mas libre estrias (H + VLE)</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="libre_estrias_3metros" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
	            <!-- END INTERACTIVE CHART PORTLET-->
	            <!-- END DASHBOARD STATS 1-->
	            <div class="row">
	                <div class="col-md-12 col-sm-12">
	                    <!-- BEGIN PORTLET-->
	                    <div class="portlet light portlet-fit bordered">
	                        <div class="portlet-title">
	                            <div class="caption">
	                                <span class="caption-subject font-dark sbold uppercase">
	                                    <h3>
	                                       <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
	                                    </h3>
	                                    <small>Hoja vieja mas libre de quema menor al 5%</small>
	                                </span>
	                            </div>
	                        </div>
	                        <div class="portlet-body">
	                            <div id="hoja_vieja_menor_5_3metros" class="chart"> </div>
	                        </div>
	                    </div>
	                    <!-- END PORTLET-->
	                </div>
	            </div>
                <!-- BEGIN INTERACTIVE CHART PORTLET-->
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
                                        </h3>
                                        <small>Hojas totales</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hojas_total_3metros" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div id="page_four">
				<div class="row">
	                <div class="col-md-12 col-sm-12">
	                    <div class="portlet light portlet-fit bordered">
	                        <div class="portlet-title">
	                            <div class="caption">
	                                <span class="caption-subject font-dark sbold uppercase">
	                                    <h3>
	                                       <i class="icon-settings font-dark"></i> Plantas 0 semanas
	                                    </h3>
	                                    <small>Hojas totales</small>
	                                </span>
	                            </div>
	                        </div>
	                        <div class="portlet-body">
	                            <div id="hojas_total" class="chart"> </div>
	                        </div>
	                    </div>
	                </div>
	        	</div>
		        <!-- END INTERACTIVE CHART PORTLET-->
		        <!-- END DASHBOARD STATS 1-->
		        <div class="row">
		            <div class="col-md-12 col-sm-12">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small >Hoja mas vieja libre de quema menor a 5%</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div  id="hoja_vieja_menor_5" class="chart"> </div>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		        </div>
		    </div>
		    <div id="page_five">
		    	<div class="row">
		            <div class="col-md-12 col-sm-12">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Hoja mas vieja libre de estrias</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="libre_estrias" class="chart"> </div>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		        </div>
	        	<!-- END DASHBOARD STATS 1-->
	        	<div class="row">
		            <div class="col-md-12 col-sm-12">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Libre de cirugia</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="libre_cirugia" class="chart"> </div>
		                    </div>
		                </div>
	                <!-- END PORTLET-->
	            	</div>
	            </div>
	        	<div class="row">
		            <div class="col-md-12 col-sm-12">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Hoja mas vieja libre de quema mayor a 5%</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="hoja_vieja_mayor_5" class="chart"> </div>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		        </div>
		    </div>
            <div id="page_six">
                <!-- END PAGE BASE CONTENT -->
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas 11 semanas
                                        </h3>
                                        <small>Hojas totales</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hojas_total_11semanas" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas 11 semanas
                                        </h3>
                                        <small>Hoja mas vieja libre de quema mayor a 5%</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hoja_vieja_mayor_5_11Semanas" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
			</div>
			<div id="page_seven">
				<div class="row">
                    <div class="col-md-12 col-sm-12">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas 11 semanas
                                        </h3>
                                        <small>Hoja mas vieja libre de quema menor a 5%</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hoja_vieja_menor_5_11Semanas" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas 11 semanas
                                        </h3>
                                        <small>Libre de cirugia</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="libre_cirugia_11Semanas" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
			</div>
			<div id="page_eight">
				<div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Emision Foliar
                                        </h3>
                                        <!-- <small>Hojas totales</small> -->
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="foliar" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<div id="page_nine">
				<div id="nine_one">
					<div class="row">
		                <div class="col-md-12 col-sm-12">
		                    <div class="portlet light portlet-fit bordered">
		                        <div class="portlet-title">
		                            <div class="caption">
		                                <span class="caption-subject font-dark sbold uppercase">
		                                	<h3>
		                                		<i class="icon-settings font-dark"></i> Clima
		                                	</h3>
		                                	<small>Temperatura minima</small>
		                                </span>
		                            </div>
		                        </div>
		                        <div class="portlet-body">
		                            <div id="temp_min" class="chart"> </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
	        	</div>
	        	<div id="nine_two">
		            <div class="row">
	                    <div class="col-md-6 col-sm-6">
	                        <div class="portlet light portlet-fit bordered">
	                            <div class="portlet-title">
	                                <div class="caption">
	                                    <i class="icon-settings font-dark"></i>
	                                    <span class="caption-subject font-dark sbold uppercase">Precipitacion (mm lluvia)</span>
	                                </div>
	                            </div>
	                            <div class="portlet-body">
	                                <div id="precp" class="chart"> </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-md-6 col-sm-6">
	                        <div class="portlet light portlet-fit bordered">
	                            <div class="portlet-title">
	                                <div class="caption">
	                                    <i class="icon-settings font-dark"></i>
	                                    <span class="caption-subject font-dark sbold uppercase">Temperatura Maxima</span>
	                                </div>
	                            </div>
	                            <div class="portlet-body">
	                                <div id="temp_max" class="chart"> </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
            	</div>
			</div>
			<div id="page_ten">
				<div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Radiacion Solar</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="radiacion" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Dias Sol</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="diassol" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Humedad Relativa Maxima</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="humedadmax" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<div id="page_eleven">
				<div class="row">
					<div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Humedad Relativa Minima</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="humedadmin" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
		<div class="row">
        	<button ng-click="traer()" id="desPDF" type="button">Descargar PDF</button>
        </div>
	</body>
</html>