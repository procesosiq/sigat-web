<html>
	<body id="contenedor" ng-controller="graficas" ng-cloak>
	<head>
		<meta charset="utf-8" />
		<style>
			html{
				padding: 15px;
				width: 100%;
			}
			.full{
				display: block;
				text-align: center;
			}
			.max_width{
				width: 100%;
			}
			#parent{
				display: block;
			}
			.center {
			    display: inline;
			}
			.left {
			    display: inline;
			}
			.right {
			    display: inline;
			}
			.charts{
				width: 100% !important;
			}
			.image{
				height: 130px;
			}
		</style>
	</head>
		<div ng-init="init()">
			<span class="full"><b>INFORME DEL ESTADO EVOLUTIVO DE LA SIGATOKA NEGRA</b></span>
			<div id="parent"><div class="left"><b>FINCA:</b> <?= "NUEVA PUBENZA"?></div> <div class="center"><b>ZONA:</b> <?= "EL ORO"?></div> <div class="right"><b>SECTOR:</b> <?= "EL CAMBIO"?></div></div><br>
			<b>SEMANA</b><br>
			<table border="1" class="max_width">
				<tbody>
					<tr>
						<td>0<br>1</td>
						<td>0<br>2</td>
						<td>0<br>3</td>
						<td>0<br>4</td>
						<td>0<br>5</td>
						<td>0<br>6</td>
						<td>0<br>7</td>
						<td>0<br>8</td>
						<td>0<br>9</td>
						<td>1<br>0</td>
						<td>1<br>1</td>
						<td>1<br>2</td>
						<td>1<br>3</td>
						<td>1<br>4</td>
						<td>1<br>5</td>
						<td>1<br>6</td>
						<td>1<br>7</td>
						<td>1<br>8</td>
						<td>1<br>9</td>
						<td>2<br>0</td>
						<td>2<br>1</td>
						<td>2<br>2</td>
						<td>2<br>3</td>
						<td>2<br>4</td>
						<td>2<br>5</td>
						<td>2<br>6</td>
					</tr>
					<br>
					<tr>
						<td>2<br>7</td>
						<td>2<br>8</td>
						<td>2<br>9</td>
						<td>3<br>0</td>
						<td>3<br>1</td>
						<td>3<br>2</td>
						<td>3<br>3</td>
						<td>3<br>4</td>
						<td>3<br>5</td>
						<td>3<br>6</td>
						<td>3<br>7</td>
						<td>3<br>8</td>
						<td>3<br>9</td>
						<td>4<br>0</td>
						<td>4<br>1</td>
						<td>4<br>2</td>
						<td>4<br>3</td>
						<td>4<br>4</td>
						<td>4<br>5</td>
						<td>4<br>6</td>
						<td>4<br>7</td>
						<td>4<br>8</td>
						<td>4<br>9</td>
						<td>5<br>0</td>
						<td>5<br>1</td>
						<td>5<br>2</td>
					</tr>
				</tbody>
			</table>
			<br>
			<b>PERIODO</b>
			<table border="1" class="max_width">
				<tr>
					<td>01</td>
					<td>02</td>
					<td>03</td>
					<td>04</td>
					<td>05</td>
					<td>06</td>
					<td>07</td>
					<td>08</td>
					<td>09</td>
					<td>10</td>
					<td>11</td>
					<td>12</td>
					<td>13</td>
				</tr>
			</table>
			<br>
			<b>1. RESUMEN EJECUTIVO</b>
			<div style="border : 1px solid black; padding-left : 15px; padding-right : 15px;">
				Este informe corresponde a la semana 4 del año 2016. 

				Continúan bajos los niveles de infección, aunque se reitera que el organismo causal ya se está 

				desarrollando dentro de la hoja y, en cualquier momento aparece en hojas 3 o 4. Por el momento, 

				el hongo está confinado a la hoja 5 de plantas jóvenes.

				Los demás parámetros están normales.

				En cuanto a clima, la temperatura mantiene niveles bastante elevados y las lluvias ya están 

				posesionadas en todas las zonas con mayor o menor intensidad. Los reportes de NOAA siguen 

				manifestando amenaza o presencia de El Niño.

				La emisión foliar tiende al incremento que es característica de la época lluviosa (0.8 y 0.81 

				hojas/semana).

				Por lo tanto se recomienda optimizar el manejo de la finca con énfasis en las labores culturales 

				relacionadas con sigatoka, así como seguir con las frecuencias cortas.

				Los ácaros siguen presentes en las hojas. Esperemos que el último ciclo con fungicidas en 

				emulsión con aceite haya contribuido al control de esta plaga
			</div>
			<span>GRAFICAS  ESTADO EVOLUTICO HOJAS  3 - 4 Y 5</span><br>
			<div>
				<div class="row">
		                <div class="col-md-12 col-sm-12">
		                    <div class="portlet light portlet-fit bordered">
		                        <div class="portlet-title">
		                            <div class="caption">
		                                <span class="caption-subject font-dark sbold uppercase">
		                                    <h3>
		                                       <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                    </h3>
		                                    <small>Hojas totales</small>
		                                </span>
		                            </div>
		                        </div>
		                        <div class="portlet-body">
		                            <div id="hojas_total" class="chart"> </div>
		                            <img class="image" id="hojas_total_img" style="display: none;" src=""> </img>
		                        </div>
		                    </div>
		                </div>
		        </div>
		        <!-- END INTERACTIVE CHART PORTLET-->
		        <!-- END DASHBOARD STATS 1-->
		        <div class="row">
		            <div class="col-md-12 col-sm-12">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small >Hoja mas vieja libre de quema menor a 5%</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div  id="hoja_vieja_menor_5" class="chart"> </div>
		                        <img class="image" id="hoja_vieja_menor_5_img" style="display: none;" src=""> </img>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		            <div class="col-md-12 col-sm-12">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Hoja mas vieja libre de estrias</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="libre_estrias" class="chart"> </div>
		                        <img class="image" id="libre_estrias_img" style="display: none;" src=""> </img>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		        </div>
		        <!-- END DASHBOARD STATS 1-->
		        <div class="row">
		            <div class="col-md-12 col-sm-12">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Libre de cirugia</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="libre_cirugia" class="chart"> </div>
		                        <img class="image" id="libre_cirugia_img" style="display: none;" src=""> </img>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		            <div class="col-md-12 col-sm-12">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Hoja mas vieja libre de quema mayor a 5%</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="hoja_vieja_mayor_5" class="chart"> </div>
		                        <img class="image" id="hoja_vieja_mayor_5_img" style="display: none;" src=""> </img>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		        </div>
		    </div>
		</div>
		<div class="row">
        	<button ng-click="traer()" type="button">Descargar PDF</button>
        </div>
	</body>
</html>