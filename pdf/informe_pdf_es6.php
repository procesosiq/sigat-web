<?php
	require("controllers/conexion.php");
	$token = $_GET["token"];
	$conexion = new M_Conexion;
	$sql = "SELECT (SELECT nombre FROM cat_clientes WHERE id = id_cliente) AS Productor ,id_hacienda ,id_cliente,descripcion 			,
					(SELECT nombre FROM cat_haciendas WHERE id = id_hacienda) AS Finca, 
					getWeek(fecha) as semana, semana_selected, year_selected
            FROM informe 
            WHERE token = '{$token}'";
	$datos = $conexion->Consultas(2, $sql);
	$sql_evidencias = "SELECT * 
                        FROM foliar 
                        WHERE id_hacienda = {$datos[0]["id_hacienda"]}
                                AND id_cliente = {$datos[0]["id_cliente"]}
                                AND YEAR(fecha) = '{$datos[0]['year_selected']}'
                                AND getWeek(fecha) = '{$datos[0]['semana_selected']}'
                                AND (evi_malezas != '' OR evi_deshoje != '' OR evi_deshojefito != '' OR evi_enfunde != '' OR evi_drenaje != '' OR evi_plaga != '' OR evi_otrasobs != '')";
    
	$evidencias = $conexion->Consultas(2, $sql_evidencias);
    $id_cliente = $datos[0]["id_cliente"];
?>
<!DOCTYPE html>
	<head>
		<meta charset="utf-8" />
		<style>
			html{
				padding: 15px;
			}
			body{
				background: #ffffff !important;
    			/*zoom: 0.55;  using 0.55 or higher results in the elements not lining up correctly */
			}
			.full{
				display: block;
				text-align: center;
			}
			.max_width{
				width: 100%;
			}
			#parent{
				display: inline-block;
				padding-left: 1em;
			}
			.center {
			    display: inline-block;
			    margin: 0 auto;
			}
			.left {
			    display: inline-block;
			    margin: 0 0 0 auto;
			}
			.right {
			    display: inline-block;
			    margin: 0 auto 0 0;
			}
			.chart{
				width: 100% !important;
				height: 300px !important;
			}
			.portlet.light.portlet-fit>.portlet-title{
				padding: 1px 20px 1px !important;
			}
			.unbreakable
			{
			    display:inline-block;
			}
			.unbreakable:after
			 {
			    display:block;
			    height:0px;
			    visibility: hidden;
			    /*page-break-inside: avoid;*/
			}
			@media print {
			    .unbreakable {
			    	page-break-after : avoid !important;
			    }
                .container_grp {
                    page-break-before : always;
                }
			}
            
			.container_grp{
				height: 960px;
				margin-bottom: 5em;
			}
		</style>
	</head>
	<body id="contenedor">
		<input id="token" type="text" value="<?= $token ?>" style="display:none;"/>
		<div id="report">
			<div style="text-align: center; display: none;"><img style="display:inline-block; width: 20%;" src="http://sigat.procesos-iq.com/logo_grande.png"/></div>
			<div id="page_one"> <!-- content --></div>
				<div id="parent"><b>PRODUCTOR:</b><?php echo " ".$datos[0]["Productor"]; ?></div>
				<div id="parent"><b>FINCA:</b><?php echo " ".$datos[0]["Finca"]; ?></div>
				<div id="parent"><b>SEMANA:</b><?php echo " ".$datos[0]["semana_selected"]; ?></div>
				
				<br>
				<b>1. RESUMEN EJECUTIVO</b>
				<div id='contentText' style="border : 0px solid black; padding-left : 15px; padding-right : 15px; height:auto ;"><!-- 850px 1 hoja -->
					<div>
						<?php 
						echo str_replace("font-size: 12.8px;", "font-size: 1pt;", $datos[0]["descripcion"]);
						?>
					</div>
				</div>
			</div>
			<div id="page_1" class="container_grp">
                <div class="row">
                    <div class="col-md-6 col-sm-6" style="width: 810px;">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <!--<h3>
                                           <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
                                        </h3>-->
                                        <small>Estado evolutivo en Hoja 3</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hoja_3_3metros" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
                <div class="row">
                     <div class="col-md-6 col-sm-6" style="width: 810px;">
	                    <!-- BEGIN PORTLET-->
	                    <div class="portlet light portlet-fit bordered">
	                        <div class="portlet-title">
	                            <div class="caption">
	                                <span class="caption-subject font-dark sbold uppercase">
	                                    <!--<h3>
	                                       <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
	                                    </h3>-->
	                                    <small>Estado evolutivo de Hoja 4</small>
	                                </span>
	                            </div>
	                        </div>
	                        <div class="portlet-body">
	                            <div id="hoja_4_3metros" class="chart"> </div>
	                        </div>
	                    </div>
	                    <!-- END PORTLET-->
	                </div>
	            </div>
			</div>

			<div id="page_2" class="container_grp">
	            <div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <!--<h3>
                                           <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
                                        </h3>-->
                                        <small>Estado evolutivo en Hoja 5</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hoja_5_3metros" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
				<!-- BEGIN INTERACTIVE CHART PORTLET-->
                <div class="row">
                    <div class="col-md-6 col-sm-6" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
                                        </h3>
                                        <small>Hoja vieja mas libre estrias (H + VLE)</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="libre_estrias_3metros" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
	            <!-- END INTERACTIVE CHART PORTLET-->
			<div id="page_3" class="container_grp">
	            <!-- END DASHBOARD STATS 1-->
	            <div class="row">
	                <div class="col-md-6 col-sm-6" style="width: 810px;">
	                    <!-- BEGIN PORTLET-->
	                    <div class="portlet light portlet-fit bordered">
	                        <div class="portlet-title">
	                            <div class="caption">
	                                <span class="caption-subject font-dark sbold uppercase">
	                                    <h3>
	                                       <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
	                                    </h3>
	                                    <small>Hoja vieja mas libre de quema menor al 5%</small>
	                                </span>
	                            </div>
	                        </div>
	                        <div class="portlet-body">
	                            <div id="hoja_vieja_menor_5_3metros" class="chart"> </div>
	                        </div>
	                    </div>
	                    <!-- END PORTLET-->
	                </div>
	            </div>
                <!-- BEGIN INTERACTIVE CHART PORTLET-->
                <div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas jovenes 3 metros
                                        </h3>
                                        <small>Hojas totales</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hojas_total_3metros" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

			<div class="container_grp" id="page_four">
				<div class="row">
	                <div class="col-md-12 col-sm-12" style="width: 810px;">
	                    <div class="portlet light portlet-fit bordered">
	                        <div class="portlet-title">
	                            <div class="caption">
	                                <span class="caption-subject font-dark sbold uppercase">
	                                    <h3>
	                                       <i class="icon-settings font-dark"></i> Plantas 0 semanas
	                                    </h3>
	                                    <small>Hojas totales</small>
	                                </span>
	                            </div>
	                        </div>
	                        <div class="portlet-body">
	                            <div id="hojas_total" class="chart"> </div>
	                        </div>
	                    </div>
	                </div>
	        	</div>
		        <!-- END INTERACTIVE CHART PORTLET-->
		        <!-- END DASHBOARD STATS 1-->
		        <div class="row">
		            <div class="col-md-12 col-sm-12" style="width: 810px;">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small >Hoja mas vieja libre de quema menor a 5%</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div  id="hoja_vieja_menor_5" class="chart"> </div>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		        </div>
		    </div>
		    <div class="container_grp" id="page_five">
		    	<div class="row">
		            <div class="col-md-12 col-sm-12" style="width: 810px;">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Hoja mas vieja libre de estrias</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="libre_estrias" class="chart"> </div>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		        </div>
	        	<!-- END DASHBOARD STATS 1-->
	        	<div class="row">
		            <div class="col-md-6 col-sm-6" style="width: 810px;">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Libre de cirugia</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="libre_cirugia" class="chart"> </div>
		                    </div>
		                </div>
	                <!-- END PORTLET-->
	            	</div>
	            </div>
			</div>
			<div class="container_grp" id="page_six">
	        	<div class="row">
		            <div class="col-md-6 col-sm-6" style="width: 810px;">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Hoja mas vieja libre de quema mayor a 5%</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="hoja_vieja_mayor_5" class="chart"> </div>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		        </div>
                <!-- END PAGE BASE CONTENT -->
                <div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas 11 semanas
                                        </h3>
                                        <small>Hojas totales</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hojas_total_11semanas" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<div class="container_grp" id="page_seven">
                <div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas 11 semanas
                                        </h3>
                                        <small>Hoja mas vieja libre de quema mayor a 5%</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hoja_vieja_mayor_5_11Semanas" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
				<div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas 11 semanas
                                        </h3>
                                        <small>Hoja mas vieja libre de quema menor a 5%</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="hoja_vieja_menor_5_11Semanas" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
			</div>
			<div class="container_grp" id="page_eight">
                <div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Plantas 11 semanas
                                        </h3>
                                        <small>Libre de cirugia</small>
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="libre_cirugia_11Semanas" class="chart"> </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
				<div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-dark sbold uppercase">
                                        <h3>
                                           <i class="icon-settings font-dark"></i> Emision Foliar
                                        </h3>
                                        <!-- <small>Hojas totales</small> -->
                                    </span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="foliar" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
            <?php if($id_cliente != 27): ?>
			<div class="container_grp" id="page_nine">
				<div id="nine_one">
					<div class="row">
		                <div class="col-md-12 col-sm-12" style="width: 810px;">
		                    <div class="portlet light portlet-fit bordered">
		                        <div class="portlet-title">
		                            <div class="caption">
		                                <span class="caption-subject font-dark sbold uppercase">
		                                	<h3>
		                                		<i class="icon-settings font-dark"></i> Clima
		                                	</h3>
		                                	<small>Temperatura minima</small>
		                                </span>
		                            </div>
		                        </div>
		                        <div class="portlet-body">
		                            <div id="temp_min" class="chart"> </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
	        	</div>
	        	<div id="nine_two">
		            <div class="row">
	                    <div class="col-md-12 col-sm-12" style="width: 810px;">
	                        <div class="portlet light portlet-fit bordered">
	                            <div class="portlet-title">
	                                <div class="caption">
	                                    <i class="icon-settings font-dark"></i>
	                                    <span class="caption-subject font-dark sbold uppercase">Precipitacion (mm lluvia)</span>
	                                </div>
	                            </div>
	                            <div class="portlet-body">
	                                <div id="precp" class="chart"> </div>
	                            </div>
	                        </div>
	                    </div>
					</div>
				</div>
			</div>
			<div class="container_grp" id="page_ten">
				<div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Temperatura Maxima</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="temp_max" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Radiacion Solar</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="radiacion" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<div class="container_grp" id="page_eleven">
                <!--<div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Dias Sol</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="diassol" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Humedad Relativa Maxima</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="humedadmax" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
			<!--</div>
			<div class="container_grp" id="page_12">-->
				<div class="row">
					<div class="col-md-12 col-sm-12" style="width: 810px;">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Humedad Relativa Minima</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="humedadmin" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
            <?php endif; ?>
			<?php
				$cont = 0;
				foreach ($evidencias as $key => $value) {
					if($value["malezas"] != '(NULL)' && $value["malezas"] != ""){
                        foreach(explode("|", $value["evi_malezas"]) as $img_url){
                            $cont++;
                            $ruta = "http://sigat.procesos-iq.com/".$img_url;
                            
                            echo "
                                <div style='width:250px; display:inline-block;'>
                                    <b>LABOR :</b><br> MALEZAS <br>
                                    <b>DESCRIPCIÓN:</b><br>". $value["malezas"]."<br>
                                    ".(($img_url!="(NULL)")? "<img height='350' width='250' src='".$ruta."'/>" : "")."
                                </div>
                            ";
    
                            if(($cont%3) == 0)
                                echo "<br>";
                        }
					}
					if($value["deshoje"] != '(NULL)' && $value["deshoje"] != ""){
                        foreach(explode("|", $value["evi_deshoje"]) as $img_url){
                            $cont++;
                            $ruta = "http://sigat.procesos-iq.com/".$img_url;

                            echo "
                                <div style='width:250px; display:inline-block;'>
                                    <b>LABOR:</b><br>DESHOJE <br>
                                    <b>DESCRIPCIÓN:</b><br>". $value["deshoje"]."<br>
                                    ".(($img_url!="(NULL)")? "<img height='350' width='250' src='".$ruta."'/>" : "")."
                                </div>
                            ";
                            if(($cont%3) == 0)
                                echo "<br>";
                        }
					}
					if($value["deshojefito"] != '(NULL)' && $value["deshojefito"] != ""){
                        foreach(explode("|", $value["evi_deshojefito"]) as $img_url){
                            $cont++;
                            $ruta = "http://sigat.procesos-iq.com/".$img_url;

                            echo "
                                <div style='width:250px; display:inline-block;'>
                                    <b>LABOR:</b><br>DESHOJEFITO <br>
                                    <b>DESCRIPCIÓN:</b><br>". $value["deshojefito"]."<br>
                                    ".(($img_url!="(NULL)")? "<img height='350' width='250' src='".$ruta."'/>" : "")."
                                </div>
                            ";
                            if(($cont%3) == 0)
                                echo "<br>";
                        }
					}
					if($value["enfunde"] != '(NULL)' && $value["enfunde"] != ""){
                        foreach(explode("|", $value["evi_enfunde"]) as $img_url){
                            $cont++;
                            $ruta = "http://sigat.procesos-iq.com/".$value["evi_enfunde"];

                            echo "
                                <div style='width:250px; display:inline-block;'>
                                    <b>LABOR:</b><br>ENFUNDE <br>
                                    <b>DESCRIPCIÓN:</b><br>". $value["enfunde"]."<br>
                                    ".(($img_url!="(NULL)")? "<img height='350' width='250' src='".$ruta."'/>" : "")."
                                </div>
                            ";
                            if(($cont%3) == 0)
                                echo "<br>";
                        }
					}
					if($value["drenaje"] != '(NULL)' && $value["drenaje"] != ""){
                        foreach(explode("|", $value["evi_drenaje"]) as $img_url){
                            $cont++;
                            $ruta = "http://sigat.procesos-iq.com/".$img_url;

                            echo "
                                <div style='width:250px; display:inline-block;'>
                                    <b>LABOR:</b><br>DRENAJES <br>
                                    <b>DESCRIPCIÓN:</b><br>". $value["drenaje"]."<br>
                                    ".(($img_url!="(NULL)")? "<img height='350' width='250' src='".$ruta."'/>" : "")."
                                </div>
                            ";
                            if(($cont%3) == 0)
                                echo "<br>";
                        }
					}
					if($value["plaga"] != '(NULL)' && $value["plaga"] != ""){
                        foreach(explode("|", $value["evi_plaga"]) as $img_url){
                            $cont++;
                            $ruta = "http://sigat.procesos-iq.com/".$img_url;

                            echo "
                                <div style='width:250px; display:inline-block;'>
                                    <b>LABOR:</b><br>PLAGA <br>
                                    <b>DESCRIPCIÓN:</b><br>". $value["plaga"]."<br>
                                    ".(($img_url!="(NULL)")? "<img height='350' width='250' src='".$ruta."'/>" : "")."
                                </div>
                            ";
                            if(($cont%3) == 0)
                                echo "<br>";
                        }
					}
					if($value["otrasobs"] != '(NULL)' && $value["otrasobs"] != ""){
                        foreach(explode("|", $value["evi_otrasobs"]) as $img_url){
                            $cont++;
                            $ruta = "http://sigat.procesos-iq.com/".$img_url;

                            echo "
                                <div style='width:250px; display:inline-block;'>
                                    <b>LABOR:</b><br>OTRAS OBSERVACIÓNES <br>
                                    <b>DESCRIPCIÓN:</b><br>".$value["otrasobs"]."<br>
                                    ".(($img_url!="(NULL)")? "<img height='350' width='250' src='".$ruta."'/>" : "")."
                                </div>
                            ";
                            if(($cont%3) == 0)
                                echo "<br>";
                        }
					}
				}
			?>
		</div>
		<!-- <div class="row">
        	<button ng-click="traer()" id="desPDF" type="button">Descargar PDF</button>
        </div> -->
	</body>
</html>