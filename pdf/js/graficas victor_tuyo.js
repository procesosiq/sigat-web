function demoFromHTML() {
    var a4  =[ 595.28,  841.89];
    var pdf = new jsPDF({
          unit:'px', 
          format:'a4'
        });
    source = $('html')[0];
    console.log(source);
    specialElementHandlers = {
        '#bypassme': function (element, renderer) {
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 600
    };
    pdf.fromHTML(
    source, // HTML string or DOM elem ref.
    margins.left, // x coord
    margins.top, { // y coord
        'width': margins.width, // max width of content on PDF
        'elementHandlers': specialElementHandlers
    },

    function (dispose) {
        pdf.save('graph.pdf');
    }, margins);
}

function pdfGenerate(){
    html2canvas($("body"), {
                    onrendered: function(canvas) {
                        theCanvas = canvas;
                        // document.body.appendChild(canvas);

                        // Convert and download as image 
                        var canva = Canvas2Image.returnPNG(canvas); 
                        var imgData = canvas.toDataURL(
                        'image/png');              
                        var doc = new jsPDF('p', 'mm');
                        doc.setFontSize(40);
                        doc.text(35, 25, "Octonyan loves jsPDF");
                        doc.addImage(imgData, 'JPEG', 15, 40, 180, 180);
                        doc.save('sample-file.pdf');
                        
                    }
                });
}

app.controller('graficas', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){

    $scope.init = function(){
        $scope.getData();
        // $interval($scope.getData, 30000);
    }

    $scope.random = [];


    $scope.traer = function(){
        var data = {
            html : $($('#contenedor')[0]).html()
        }
        //demoFromHTML();
        pdfGenerate()
        // client.post("./helpers/pdf_helper.php", $scope.getHTML, data) ;
    }

    $scope.responseImagen = function(r){

    }

    $scope.getHTML = function(r){
        
    }

    $scope.getData = function(){
        var data = {
            opt : 'HMVLDQMEN5'
        }
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycMenor5 , data);
        
        data = {};
        data = {
            opt : "HMVLDE"
        }
        
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycLibreStrias , data);

        data = {};
        data = {
            opt : "LIB_DE_CIRUG"
        }
        
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycLibreCirug , data);


        data = {};
        data = {
            opt : "HMVLDQMAY5"
        }
        
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycMayor5 , data);

        data = {};
        data = {
            opt : "HOJTOT"
        }
        
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycHojasTotales , data);

    }

    $scope.printGraphycMenor5 = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hoja_vieja_menor_5");
        }
    }

    $scope.printGraphycLibreStrias = function(r , b){
        b();
        if(r){
            $scope.printData(r , "libre_estrias");
        }
    }

    $scope.printGraphycLibreCirug = function(r , b){
        b();
        if(r){
            $scope.printData(r , "libre_cirugia");
        }
    }

    $scope.printGraphycMayor5 = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hoja_vieja_mayor_5");
        }
    }

    $scope.printGraphycHojasTotales = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hojas_total");
            console.log("hojas_total");
        }
    }

    $scope.getRandomColor = function() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    $scope.printData = function(r , id){

        function saveFlotGraphAsPNG(placeholderID, targetID) {

            var divobj = document.getElementById(placeholderID);
            // console.log(divobj);
            var oImg = Canvas2Image.saveAsPNG(divobj.childNodes[0], true);

            if (!oImg) {
              alert("Sorry, this browser is not capable of saving PNG files!");
              return false;
            }

            oImg.id = "canvasimage";

            document.getElementById(targetID).removeChild(document.getElementById(targetID).childNodes[0]);
            document.getElementById(targetID).appendChild(oImg);

        }

        if(r){
            var data = [];
            var ctx = [];
            var id = id;
            for(var info in r){
                data.push({
                    data : r[info],
                    label: info,
                    lines: {
                        lineWidth: 1,
                    },
                    shadowSize: 0
                })
            }
            console.log(id);
            var plot = $.plot($('#'+id), data, {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.05
                                }, {
                                    opacity: 0.01
                                }]
                            }
                        },
                        points: {
                            show: true,
                            radius: 3,
                            lineWidth: 1
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderColor: "#eee",
                        borderWidth: 1
                    },
                    colors: ["#d12610", "#37b7f3", "#52e136"],
                    xaxis: {
                        ticks: 11,
                        tickDecimals: 0,
                        tickColor: "#eee",
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 0,
                        tickColor: "#eee",
                    }
                });

                // ctx = plot.getCanvas();
                // var source = ctx.toDataURL("image/png");
                // console.log(source);
                 // if ($.support.cssFloat) {   // currently evals to False in IE
                 //     var s = document.createElement("script");
                 //     s.setAttribute("type", "text/javascript");
                 //     s.setAttribute("src", "base64.js");
                 //     var h = document.getElementById("head");
                 //     h.appendChild(s);

                 //     var s2 = document.createElement("script");
                 //     s2.setAttribute("type", "text/javascript");
                 //     s2.setAttribute("src", "canvas2image.js");
                 //     h.appendChild(s2);

                 //     document.getElementById("convertpngbtn").onclick = function() {
                 //         saveFlotGraphAsPNG("placeholder", "main");
                 //     }
                 // } else {
                 //     document.getElementById("convertpngbtn").onclick = function() {
                 //         alert("Image Exporting not available in IE");
                 //     }
                 // }

                function showTooltip(x, y, contents) {
                    $('<div id="tooltip">' + contents + '</div>').css({
                        position: 'absolute',
                        display: 'none',
                        top: y + 5,
                        left: x + 15,
                        border: '1px solid #333',
                        padding: '4px',
                        color: '#fff',
                        'border-radius': '3px',
                        'background-color': '#333',
                        opacity: 0.80
                    }).appendTo("body").fadeIn(200);
                }

                var previousPoint = null;
                $('#'+id).bind("plothover", function(event, pos, item) {
                    $("#x").text(pos.x.toFixed(0));
                    $("#y").text(pos.y.toFixed(2));

                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            showTooltip(item.pageX, item.pageY,"Año " + item.series.label + " Semana " + x + " Valor " + y);
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                
                });

                // saveFlotGraphAsPNG(id,id + "_img");
                //  html2canvas($("#"+id), {
                //     onrendered: function(canvas) {
                //         theCanvas = canvas;
                //         // document.body.appendChild(canvas);

                //         // Convert and download as image 
                //         var canva = Canvas2Image.returnPNG(canvas); 
                //         console.log(canva);
                //         $("#" + id).css('display', 'none').empty();

                //         data = {};
                //         data = {
                //             opt : id,
                //             imagen : canvas.toDataURL()
                //         }
                //         // $("#" + id + "_img").css('display', 'block').attr("src", data.imagen);
                //         //$("#" + id + "_img").css('display', 'block').attr("src",canvas.toDataURL());
                        
                //         // client.post("./pdf/send_imagen.php" ,$scope.responseImagen , data);
                //         // $("#"+id).append(canvas);
                //         // Clean up 
                //         //document.body.removeChild(canvas);
                //     }
                // });
                // $('#'+id).css('display', 'none');
                // var series = plot.getData();
                // // var Axes = plot.getAxes();
                // var getPlaceholder = plot.getPlaceholder();
                // var getPlotOffset = plot.getPlotOffset();
                // console.log(getPlaceholder);
                // console.log(getPlotOffset);
                // // console.log(Axes);
                // for (var i = 0; i < series.length; ++i){
                //     console.log(series[i].color);
                // }
        }   
    }
}]);