function demoFromHTML() {
    var a4  =[ 595.28,  841.89];
    var pdf = new jsPDF({
          unit:'px', 
          format:'a4'
        });
    source = $('html')[0];
    console.log(source);
    specialElementHandlers = {
        '#bypassme': function (element, renderer) {
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 600
    };
    pdf.fromHTML(
    source, // HTML string or DOM elem ref.
    margins.left, // x coord
    margins.top, { // y coord
        'width': margins.width, // max width of content on PDF
        'elementHandlers': specialElementHandlers
    },

    function (dispose) {
        pdf.save('graph.pdf');
    }, margins);
}

function pdfGenerate(){
    var pdf = new jsPDF('portrait', 'mm', [350, 220]);
     /*html2canvas(document.getElementById("page1"), {
         onrendered: function(canvas) {
             //theCanvas = canvas;
             // document.body.appendChild(canvas);

             // Convert and download as image 
             var canva = Canvas2Image.returnPNG(canvas); 
             var imgData = canvas.toDataURL('image/png');
             //var doc = new jsPDF('p', 'mm');
             //doc.setFontSize(14);
             //doc.text(35, 25, "INFORME DEL ESTADO EVOLUTIVO DE LA SIGATOKA NEGRA");
             pdf.addImage(imgData, 'PNG', 0, 0, 300, 300);
             pdf.save('sample-file.pdf');
         }
     });
    //pdf.addImage(img_body(), 'JPEG');
    //pdf.save('sample-file.pdf');

    /*pdf.addHTML(document.body, options, function(){
        pdf.save('sample-file.pdf');
    });*/
    html2canvas($("#page_one"), {
        onrendered: function(canvas) {
            var img = canvas.toDataURL();
            console.log("1");
            pdf.addImage(img, 'JPEG', 15, 20, 180, 50);
            pdf.addPage();
            imagen_2(pdf);
        }
    });
}

function imagen_2(pdf){
    html2canvas($("#page_two"), {
        onrendered: function(canvas) {
            console.log("2");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'PNG', 15, 20, 180, 300);
            pdf.addPage();
            imagen_3(pdf);
        }
    });
}

function imagen_3(pdf){
    html2canvas($("#page_three"), {
        onrendered: function(canvas) {
            console.log("3");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 300);
            pdf.addPage();
            imagen_4(pdf);
        }
    });
}

function imagen_4(pdf){
    html2canvas($("#page_four"), {
        onrendered: function(canvas) {
            console.log("4");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 280);
            pdf.addPage();
            imagen_5(pdf);
        }
    });
}

function imagen_5(pdf){
    html2canvas($("#page_five"), {
        onrendered: function(canvas) {
            console.log("5");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 300);
            pdf.addPage();
            imagen_6(pdf);
        }
    });
}

function imagen_6(pdf){
    html2canvas($("#page_six"), {
        onrendered: function(canvas) {
            console.log("6");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 280);
            pdf.addPage();
            imagen_7(pdf);
        }
    });
}

function imagen_7(pdf){
    html2canvas($("#page_seven"), {
        onrendered: function(canvas) {
            console.log("7");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 280);
            pdf.addPage();
            imagen_8(pdf);
        }
    });
}

function imagen_8(pdf){
    html2canvas($("#page_eight"), {
        onrendered: function(canvas) {
            console.log("8");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 120);
            pdf.addPage();
            imagen_9(pdf);
        }
    });
}

function imagen_9(pdf){
    html2canvas($("#nine_one"), {
        onrendered: function(canvas) {
            console.log("9");
            var img = canvas.toDataURL();
            //pdf.text(20, 20,"pagina 9");
            //window.open(img);
            pdf.addImage(img, 'JPEG', 15, 20, 180, 100);
            pdf.addPage();
            //save_pdf(pdf);
            //pdf.addPage();
            imagen_9_2(pdf);
        }
    });
}

function imagen_9_2(pdf){
    html2canvas($("#nine_two"), {
        onrendered: function(canvas) {
            console.log("9");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 100);
            save_pdf(pdf);
            //pdf.addPage();
            //imagen_10(pdf);
        }
    });   
}

function imagen_10(pdf){
    html2canvas($("#page_ten"), {
        onrendered: function(canvas) {
            console.log("10");
            var img = canvas.toDataURL();
            pdf.text(20, 20,"pagina 10");
            //window.open(img);
            //pdf.addImage(img, 'JPEG', 15, 20, 180, 280);
            pdf.addPage();
            imagen_11(pdf);
        }
    });   
}

function imagen_11(pdf){
    html2canvas($("#page_eleven"), {
        onrendered: function(canvas) {
            console.log("11");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 120);
            //window.open(img);
            save_pdf(pdf);
        }
    });   
}

function save_pdf(pdf){
    pdf.save("sample-file.pdf");
}

app.controller('graficas', ['$scope','$http','client', function($scope,$http,client){

    $scope.init = function(){
        $scope.token = $("#token").val();
        $scope.id_cliente = $("#id_cliente").val()
        let divHeigt = $( "#contentText" ).height();
        let step1 = $( "#step1" ).height();
        // console.log(divHeigt)
        if(divHeigt > 945){
            $( "#contentText" ).height(2000);
            // $( "#step1" ).height(1770);
        }else{
            $( "#contentText" ).height(980);
            // $( "#step1" ).height(130);
        }
        $scope.getData();
        // $interval($scope.getData, 30000);
    }

    $scope.random = [];

    $scope.traer = function(){
        /*var data = {
            html : $($('#contenedor')[0]).html()
        }*/
        //demoFromHTML();
        $("#desPDF").css('display', 'none');
        pdfGenerate()
        // client.post("./helpers/pdf_helper.php", $scope.getHTML, data) ;
    }

    $scope.getData = function(){
        var data = {
            opt : "HMVLDQMEN5",
            token : $("#token").val()
        }
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycMenor5 , data);
        
        data = {};
        data = {
            opt : "HMVLDE",
            token : $("#token").val()
        }
        
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycLibreStrias , data);

        data = {};
        data = {
            opt : "LIB_DE_CIRUG",
            token : $("#token").val()
        }
        
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycLibreCirug , data);


        data = {};
        data = {
            opt : "HMVLDQMAY5",
            token : $scope.token
        }
        
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycMayor5 , data);

        data = {};
        data = {
            opt : "HOJTOT",
            token : $scope.token
        }
        
        client.post("./controllers/semanas_cero.php" ,$scope.printGraphycHojasTotales , data);

        /*----------  HOJA 3 METRO  ----------*/
        
        $scope.getData3Metros();
    }

    $scope.getData11Semanas = function(){
        var data = {
            opt : 'HMVLDQMEN5',
            token : $scope.token
        }
        client.post("./controllers/semanas_once.php" ,$scope.printGraphycMenor511Semanas , data);

        data = {};
        data = {
            opt : "LIB_DE_CIRUG",
            token : $scope.token
        }
        
        client.post("./controllers/semanas_once.php" ,$scope.printGraphycLibreCirug11Semanas , data);


        data = {};
        data = {
            opt : "HMVLDQMAY5",
            token : $scope.token
        }
        
        client.post("./controllers/semanas_once.php" ,$scope.printGraphycMayor511Semanas , data);

        data = {};
        data = {
            opt : "HOJTOT",
            token : $scope.token
        }
        
        client.post("./controllers/semanas_once.php" ,$scope.printGraphycHojasTotales11Semanas , data);

        /*---------- FOLIAR------------*/
        $scope.getDataFoliar()
    }

    $scope.getData3Metros = function(){
        var data = {
            opt : 'HMVLDQMEN5',
            token : $scope.token
        }
        client.post("./controllers/3_metros.php" ,$scope.printGraphycMenor53Metros , data);
        
        data = {};
        data = {
            opt : "HMVLDE",
            token : $scope.token
        }
        
        client.post("./controllers/3_metros.php" ,$scope.printGraphycLibreStrias3Metros , data);

        data = {};
        data = {
            opt : "HOJA4",
            token : $scope.token
        }
        
        client.post("./controllers/3_metros.php" ,$scope.printGraphycHoja43Metros , data);


        data = {};
        data = {
            opt : "HOJA3",
            token : $scope.token
        }
        
        client.post("./controllers/3_metros.php" ,$scope.printGraphycHoja33Metros , data);

        data = {};
        data = {
            opt : "HOJA5",
            token : $scope.token
        }
        
        client.post("./controllers/3_metros.php" ,$scope.printGraphycHoja53Metros , data);

        data = {};
        data = {
            opt : "HOJTOT",
            token : $scope.token
        }
        
        client.post("./controllers/3_metros.php" ,$scope.printGraphycHojasTotales3Metros , data);

        /*-------------11 SEMANAS------------*/
        $scope.getData11Semanas();
    }

    $scope.getDataFoliar = async function(){
        let data = {
            opt : 'GRAFICA',
            token : $scope.token
        }
        // client.post("./controllers/foliar.php" ,$scope.printGraphycFoliar , data);
       /*try {
          console.info('foliar:', data)
          let r = await $http.post("./controllers/foliar.php", data);
          $scope.printGraphycFoliar(r.data);
        } catch(e) {
          console.error('error', e);
          if(!$scope.guardadas) $scope.guardadas = 0;
          $scope.guardadas++
        }*/
        client.post("./controllers/foliar.php" ,$scope.printGraphycFoliar , data);

        await $scope.getDataClima();
    }

    $scope.getDataClima = function(){
        return new Promise(async (resolve) => {
            let data = {
                opt : 'TEMMIN',
                token : $scope.token
            }
            let r = await $http.post("./controllers/climas.php", data)
            $scope.printGraphycMin(r.data)
            
            data = {
                opt : "TEMMAX",
                token : $scope.token
            }
            r = await $http.post("./controllers/climas.php", data);
            $scope.printGraphycMax(r.data)
    
            data = {
                opt : "LLUVIA",
                token : $scope.token
            }
            r = await $http.post("./controllers/climas.php", data);
            $scope.printGraphycLluvia(r.data)
    
            data = {
                opt : "RADSOLAR",
                token : $scope.token
            }
            
            r = await $http.post("./controllers/climas.php", data);
            $scope.printGraphycRadsolar(r.data)
    
            data = {
                opt : "DIASSOL",
                token : $scope.token
            }
            //client.post("./controllers/climas.php" ,$scope.printGraphycDiassol , data);
    
            data = {
                opt : "HUMMIN",
                token : $scope.token
            }
            r = await $http.post("./controllers/climas.php", data);
            $scope.printGraphycHumMin(r.data)
    
            data = {
                opt : "HUMMAX",
                token : $scope.token
            }
            r = await $http.post("./controllers/climas.php", data);
            $scope.printGraphycHumMax(r.data)

            resolve()
        })
    }

    $scope.printGraphycMin = function(r , b){
        if(r){
            $scope.printData(r , "temp_min");
        }
    }

    $scope.printGraphycMax = function(r , b){
        if(r){
            $scope.printData(r , "temp_max");
        }
    }

    $scope.printGraphycLluvia = function(r , b){
        if(r){
            $scope.printData(r , "precp");
        }
    }

    $scope.printGraphycRadsolar = function(r , b){
        if(r){
            $scope.printData(r , "radiacion");
        }
    }

    $scope.printGraphycDiassol = function(r , b){
        if(r){
            $scope.printData(r , "diassol");
        }
    }

    $scope.printGraphycHumMin = function(r , b){
        if(r){
            $scope.printData(r , "humedadmin");
        }
    }

    $scope.printGraphycHumMax = function(r , b){
        if(r){
            $scope.printData(r , "humedadmax");
        }
    }

    $scope.printGraphycFoliar = function(r , b){
        b();
        if(r){
            $scope.printDataFoliar(r , "foliar");
        }
    }

    $scope.printGraphycMenor511Semanas = function(r, b){
        b();
        if(r){
            $scope.printData(r , "hoja_vieja_menor_5_11Semanas");
        }
    }

    $scope.printGraphycLibreCirug11Semanas = function(r, b){
        b();
        if(r){
            $scope.printData(r , "libre_cirugia_11Semanas");
        }
    }

    $scope.printGraphycMayor511Semanas = function(r, b){
        b();
        if(r){
            $scope.printData(r , "hoja_vieja_mayor_5_11Semanas");
        }
    }

    $scope.printGraphycHojasTotales11Semanas = function(r, b){
        b();
        if(r){
            $scope.printData(r , "hojas_total_11semanas");
        }
    }    

    $scope.printGraphycMenor53Metros = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hoja_vieja_menor_5_3metros");
        }
    }

    $scope.printGraphycLibreStrias3Metros = function(r , b){
        b();
        if(r){
            $scope.printData(r , "libre_estrias_3metros");
        }
    }

    $scope.printGraphycHoja43Metros = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hoja_4_3metros");
        }
    }

    $scope.printGraphycHoja53Metros = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hoja_5_3metros");
        }
    }

    $scope.printGraphycHoja33Metros = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hoja_3_3metros");
        }
    }

    $scope.printGraphycHojasTotales3Metros = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hojas_total_3metros");
        }
    }

    $scope.printGraphycMenor5 = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hoja_vieja_menor_5");
        }
    }

    $scope.printGraphycLibreStrias = function(r , b){
        b();
        if(r){
            $scope.printData(r , "libre_estrias");
        }
    }

    $scope.printGraphycLibreCirug = function(r , b){
        b();
        if(r){
            $scope.printData(r , "libre_cirugia");
        }
    }

    $scope.printGraphycMayor5 = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hoja_vieja_mayor_5");
        }
    }

    $scope.printGraphycHojasTotales = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hojas_total");
        }
    }

    $scope.getRandomColor = function() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    var umbrales = {
        temp_min : 21,
        hoja_vieja_menor_5_3metros : 11,
        hojas_total : 13,
        libre_estrias_3metros : 6.5,
        hojas_total_3metros : 11,
        hojas_total_11semanas : 6.5
    }

    $scope.printData = function(r , id){
        if(r && r.datos){
            var data = [];
            var options =  {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.05
                                }, {
                                    opacity: 0.01
                                }]
                            }
                        },
                        points: {
                            show: false,
                            radius: 3,
                            lineWidth: 1
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderColor: "#eee",
                        borderWidth: 1
                    },
                    colors: ["#d12610", "#37b7f3", "#52e136"],
                    xaxis: {
                        ticks: 11,
                        tickDecimals: 0,
                        tickColor: "#eee",
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 0,
                        tickColor: "#eee",
                        min: (r.min > 2) ? (r.min - 2) : r.min,
                    }
                };
            
            for(let info in r.datos){
                let dataClean = r.datos[info].map((v) => [v[0], parseFloat(v[1]) || 0])
                console.log(dataClean)
                data.push({
                    data : dataClean,
                    label: (info==0)?'Umbral':info,
                    lines: {
                        lineWidth: 1,
                    },
                    shadowSize: 0
                });

                // minValue.push(r[info]);
            }
            if(document.getElementById(id)){
                var plot = $.plot($('#'+id), data, options);
                cavasToDataURL(id).then(data => {
                    client.post('./controllers/index.php?accion=Informes.guardarImagenReporte', savedImage, { token  : $scope.token, name : id, data })
                })
            }

            function showTooltip(x, y, contents) {
                $('<div id="tooltip">' + contents + '</div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y + 5,
                    left: x + 15,
                    border: '1px solid #333',
                    padding: '4px',
                    color: '#fff',
                    'border-radius': '3px',
                    'background-color': '#333',
                    opacity: 0.80
                }).appendTo("body").fadeIn(200);
            }

            var previousPoint = null;
            $('#'+id).bind("plothover", function(event, pos, item) {
                $("#x").text(pos.x.toFixed(0));
                $("#y").text(pos.y.toFixed(2));

                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;

                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(2),
                            y = item.datapoint[1].toFixed(2);

                        showTooltip(item.pageX, item.pageY,"Año " + item.series.label + " Semana " + x + " Valor " + y);
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            
            });
        }   
    }

    $scope.printDataFoliar = function(r , id){
        if(r){  
            var data = [];
            var id = id;
            
            for(var info in r.data){
                data.push({
                    data : r.data[info],
                    label: info,
                    lines: {
                        lineWidth: 1,
                    },
                    shadowSize: 0
                })
            }
            var plot = $.plot($('#'+id), data, {
                series: {
                    lines: {
                        show: true,
                        lineWidth: 2,
                        fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 0.05
                            }, {
                                opacity: 0.01
                            }]
                        }
                    },
                    points: {
                        show: false,
                        radius: 3,
                        lineWidth: 1
                    },
                    shadowSize: 2
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    tickColor: "#eee",
                    borderColor: "#eee",
                    borderWidth: 1
                },
                colors: ["#d12610", "#37b7f3", "#52e136"],
                xaxis: {
                    ticks: 11,
                    tickDecimals: 0,
                    tickColor: "#eee",
                },
                yaxis: {
                    ticks: 11,
                    tickDecimals: (r.max - r.min < .1) ? 2 : 1,
                    tickColor: "#eee",
                    max: (r.max + ((r.max - r.min) * .5)),
                    min: (r.min - ((r.max - r.min) * .5))
                }
            });

            cavasToDataURL(id).then(data => {
                client.post('./controllers/index.php?accion=Informes.guardarImagenReporte', savedImage, { token  : $scope.token, name : id, data })
            })

            function showTooltip(x, y, contents) {
                $('<div id="tooltip">' + contents + '</div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y + 5,
                    left: x + 15,
                    border: '1px solid #333',
                    padding: '4px',
                    color: '#fff',
                    'border-radius': '3px',
                    'background-color': '#333',
                    opacity: 0.80
                }).appendTo("body").fadeIn(200);
            }

            var previousPoint = null;
            $('#'+id).bind("plothover", function(event, pos, item) {
                $("#x").text(pos.x.toFixed(0));
                $("#y").text(pos.y.toFixed(2));

                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;

                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(2),
                            y = item.datapoint[1].toFixed(2);

                        showTooltip(item.pageX, item.pageY,"Año " + item.series.label + " Semana " + x + " Valor " + y);
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            
            });
        }   
    }

    function savedImage(r, b){
        b()
        if(!$scope.guardadas) $scope.guardadas = 0
        $scope.guardadas++;
        console.info($scope.guardadas)
        if($scope.id_cliente == 27) if($scope.guardadas == 16) window.location.href = 'pdf/informe_pdf_dom.php?token='+$("#token").val()
        if($scope.guardadas >= 22) window.location.href = 'pdf/informe_pdf_dom.php?token='+$("#token").val()
    }

    function cavasToDataURL(id) {
        return new Promise(function(resolve, reject){

            html2canvas(document.getElementById(id), {
                onrendered: function(canvas) {
                    var dataImage = canvas.toDataURL("image/png");
                    resolve(dataImage)
                }
            })
        })
    }

}]);