function demoFromHTML() {
    var a4  =[ 595.28,  841.89];
    var pdf = new jsPDF({
          unit:'px', 
          format:'a4'
        });
    source = $('html')[0];
    console.log(source);
    specialElementHandlers = {
        '#bypassme': function (element, renderer) {
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 600
    };
    pdf.fromHTML(
    source, // HTML string or DOM elem ref.
    margins.left, // x coord
    margins.top, { // y coord
        'width': margins.width, // max width of content on PDF
        'elementHandlers': specialElementHandlers
    },

    function (dispose) {
        pdf.save('graph.pdf');
    }, margins);
}

function pdfGenerate(){
    var pdf = new jsPDF('portrait', 'mm', [350, 220]);
     /*html2canvas(document.getElementById("page1"), {
         onrendered: function(canvas) {
             //theCanvas = canvas;
             // document.body.appendChild(canvas);

             // Convert and download as image 
             var canva = Canvas2Image.returnPNG(canvas); 
             var imgData = canvas.toDataURL('image/png');
             //var doc = new jsPDF('p', 'mm');
             //doc.setFontSize(14);
             //doc.text(35, 25, "INFORME DEL ESTADO EVOLUTIVO DE LA SIGATOKA NEGRA");
             pdf.addImage(imgData, 'PNG', 0, 0, 300, 300);
             pdf.save('sample-file.pdf');
         }
     });
    //pdf.addImage(img_body(), 'JPEG');
    //pdf.save('sample-file.pdf');

    /*pdf.addHTML(document.body, options, function(){
        pdf.save('sample-file.pdf');
    });*/
    html2canvas($("#page_one"), {
        onrendered: function(canvas) {
            var img = canvas.toDataURL();
            console.log("1");
            pdf.addImage(img, 'JPEG', 15, 20, 180, 50);
            pdf.addPage();
            imagen_2(pdf);
        }
    });
}

function imagen_2(pdf){
    html2canvas($("#page_two"), {
        onrendered: function(canvas) {
            console.log("2");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'PNG', 15, 20, 180, 300);
            pdf.addPage();
            imagen_3(pdf);
        }
    });
}

function imagen_3(pdf){
    html2canvas($("#page_three"), {
        onrendered: function(canvas) {
            console.log("3");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 300);
            pdf.addPage();
            imagen_4(pdf);
        }
    });
}


function imagen_4(pdf){
    html2canvas($("#page_four"), {
        onrendered: function(canvas) {
            console.log("4");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 280);
            pdf.addPage();
            imagen_5(pdf);
        }
    });
}

function imagen_5(pdf){
    html2canvas($("#page_five"), {
        onrendered: function(canvas) {
            console.log("5");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 300);
            pdf.addPage();
            imagen_6(pdf);
        }
    });
}

function imagen_6(pdf){
    html2canvas($("#page_six"), {
        onrendered: function(canvas) {
            console.log("6");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 280);
            pdf.addPage();
            imagen_7(pdf);
        }
    });
}

function imagen_7(pdf){
    html2canvas($("#page_seven"), {
        onrendered: function(canvas) {
            console.log("7");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 280);
            pdf.addPage();
            imagen_8(pdf);
        }
    });
}

function imagen_8(pdf){
    html2canvas($("#page_eight"), {
        onrendered: function(canvas) {
            console.log("8");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 120);
            pdf.addPage();
            imagen_9(pdf);
        }
    });
}

function imagen_9(pdf){
    html2canvas($("#nine_one"), {
        onrendered: function(canvas) {
            console.log("9");
            var img = canvas.toDataURL();
            //pdf.text(20, 20,"pagina 9");
            //window.open(img);
            pdf.addImage(img, 'JPEG', 15, 20, 180, 100);
            pdf.addPage();
            //save_pdf(pdf);
            //pdf.addPage();
            imagen_9_2(pdf);
        }
    });
}

function imagen_9_2(pdf){
    html2canvas($("#nine_two"), {
        onrendered: function(canvas) {
            console.log("9");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 100);
            save_pdf(pdf);
            //pdf.addPage();
            //imagen_10(pdf);
        }
    });   
}

function imagen_10(pdf){
    html2canvas($("#page_ten"), {
        onrendered: function(canvas) {
            console.log("10");
            var img = canvas.toDataURL();
            pdf.text(20, 20,"pagina 10");
            //window.open(img);
            //pdf.addImage(img, 'JPEG', 15, 20, 180, 280);
            pdf.addPage();
            imagen_11(pdf);
        }
    });   
}

function imagen_11(pdf){
    html2canvas($("#page_eleven"), {
        onrendered: function(canvas) {
            console.log("11");
            var img = canvas.toDataURL();
            pdf.addImage(img, 'JPEG', 15, 20, 180, 120);
            //window.open(img);
            save_pdf(pdf);
        }
    });   
}

function save_pdf(pdf){
    pdf.save("sample-file.pdf");
}

init = function(){
    token = $("#token").val();
    var divHeigt = $( "#contentText" ).height();
    var step1 = $( "#step1" ).height();
    // console.log(divHeigt)
    if(divHeigt > 945){
        $( "#contentText" ).height(2000);
        // $( "#step1" ).height(1770);
    }else{
        $( "#contentText" ).height(980);
        // $( "#step1" ).height(130);
    }
    getData();
    // $interval(getData, 30000);
}

traer = function(){
    /*var data = {
        html : $($('#contenedor')[0]).html()
    }*/
    //demoFromHTML();
    $("#desPDF").css('display', 'none');
    pdfGenerate()
    // ahttp.post("./helpers/pdf_helper.php", getHTML, data) ;
}

getData = function(){
    var data = {
        opt : "HMVLDQMEN5",
        token : $("#token").val()
    }
    ahttp.post("./controllers/semanas_cero.php" ,printGraphycMenor5 , data);
    
    data = {};
    data = {
        opt : "HMVLDE",
        token : $("#token").val()
    }
    
    ahttp.post("./controllers/semanas_cero.php" ,printGraphycLibreStrias , data);

    data = {};
    data = {
        opt : "LIB_DE_CIRUG",
        token : $("#token").val()
    }
    
    ahttp.post("./controllers/semanas_cero.php" ,printGraphycLibreCirug , data);


    data = {};
    data = {
        opt : "HMVLDQMAY5",
        token : token
    }
    
    ahttp.post("./controllers/semanas_cero.php" ,printGraphycMayor5 , data);

    data = {};
    data = {
        opt : "HOJTOT",
        token : token
    }
    
    ahttp.post("./controllers/semanas_cero.php" ,printGraphycHojasTotales , data);

    /*----------  HOJA 3 METRO  ----------*/
    
    getData3Metros();
}

getData11Semanas = function(){
    var data = {
        opt : 'HMVLDQMEN5',
        token : token
    }
    ahttp.post("./controllers/semanas_once.php" ,printGraphycMenor511Semanas , data);

    data = {};
    data = {
        opt : "LIB_DE_CIRUG",
        token : token
    }
    
    ahttp.post("./controllers/semanas_once.php" ,printGraphycLibreCirug11Semanas , data);


    data = {};
    data = {
        opt : "HMVLDQMAY5",
        token : token
    }
    
    ahttp.post("./controllers/semanas_once.php" ,printGraphycMayor511Semanas , data);

    data = {};
    data = {
        opt : "HOJTOT",
        token : token
    }
    
    ahttp.post("./controllers/semanas_once.php" ,printGraphycHojasTotales11Semanas , data);

    /*---------- FOLIAR------------*/
    getDataFoliar()
}

getData3Metros = function(){
    var data = {
        opt : 'HMVLDQMEN5',
        token : token
    }
    ahttp.post("./controllers/3_metros.php" ,printGraphycMenor53Metros , data);
    
    data = {};
    data = {
        opt : "HMVLDE",
        token : token
    }
    
    ahttp.post("./controllers/3_metros.php" ,printGraphycLibreStrias3Metros , data);

    data = {};
    data = {
        opt : "HOJA4",
        token : token
    }
    
    ahttp.post("./controllers/3_metros.php" ,printGraphycHoja43Metros , data);


    data = {};
    data = {
        opt : "HOJA3",
        token : token
    }
    
    ahttp.post("./controllers/3_metros.php" ,printGraphycHoja33Metros , data);

    data = {};
    data = {
        opt : "HOJA5",
        token : token
    }
    
    ahttp.post("./controllers/3_metros.php" ,printGraphycHoja53Metros , data);

    data = {};
    data = {
        opt : "HOJTOT",
        token : token
    }
    
    ahttp.post("./controllers/3_metros.php" ,printGraphycHojasTotales3Metros , data);

    /*-------------11 SEMANAS------------*/
    getData11Semanas();
}

getDataFoliar = function(){
    console.log("Entro")
    var data = {
        opt : 'GRAFICA',
        token : token
    }
    ahttp.post("./controllers/foliar.php" ,printGraphycFoliar , data);

    getDataClima();
}

getDataClima = function(){
    var data = {
        opt : 'TEMMIN',
        token : token
    }
    ahttp.post("./controllers/climas.php" ,printGraphycMin , data);
    
    data = {};
    data = {
        opt : "TEMMAX",
        token : token
    }
    
    ahttp.post("./controllers/climas.php" ,printGraphycMax , data);

    data = {};
    data = {
        opt : "LLUVIA",
        token : token
    }
    
    ahttp.post("./controllers/climas.php" ,printGraphycLluvia , data);


    data = {};
    data = {
        opt : "RADSOLAR",
        token : token
    }
    
    ahttp.post("./controllers/climas.php" ,printGraphycRadsolar , data);

    data = {};
    data = {
        opt : "DIASSOL",
        token : token
    }
    
    //ahttp.post("./controllers/climas.php" ,printGraphycDiassol , data);

    data = {};
    data = {
        opt : "HUMMIN",
        token : token
    }
    
    ahttp.post("./controllers/climas.php" ,printGraphycHumMin , data);

    data = {};
    data = {
        opt : "HUMMAX",
        token : token
    }
    
    ahttp.post("./controllers/climas.php" ,printGraphycHumMax , data);
}

printGraphycMin = function(r , b){
    b();
    if(r){
        printData(r , "temp_min");
    }
}

printGraphycMax = function(r , b){
    b();
    if(r){
        printData(r , "temp_max");
    }
}

printGraphycLluvia = function(r , b){
    b();
    if(r){
        printData(r , "precp");
    }
}

printGraphycRadsolar = function(r , b){
    b();
    if(r){
        printData(r , "radiacion");
    }
}

printGraphycDiassol = function(r , b){
    b();
    if(r){
        printData(r , "diassol");
    }
}

printGraphycHumMin = function(r , b){
    b();
    if(r){
        printData(r , "humedadmin");
    }
}

printGraphycHumMax = function(r , b){
    b();
    if(r){
        printData(r , "humedadmax");
    }
}

printGraphycFoliar = function(r , b){
    b();
    if(r){
        printDataFoliar(r , "foliar");
    }
}

printGraphycMenor511Semanas = function(r, b){
    b();
    if(r){
        printData(r , "hoja_vieja_menor_5_11Semanas");
    }
}

printGraphycLibreCirug11Semanas = function(r, b){
    b();
    if(r){
        printData(r , "libre_cirugia_11Semanas");
    }
}

printGraphycMayor511Semanas = function(r, b){
    b();
    if(r){
        printData(r , "hoja_vieja_mayor_5_11Semanas");
    }
}

printGraphycHojasTotales11Semanas = function(r, b){
    b();
    if(r){
        printData(r , "hojas_total_11semanas");
    }
}    

printGraphycMenor53Metros = function(r , b){
    b();
    if(r){
        printData(r , "hoja_vieja_menor_5_3metros");
    }
}

printGraphycLibreStrias3Metros = function(r , b){
    b();
    if(r){
        printData(r , "libre_estrias_3metros");
    }
}

printGraphycHoja43Metros = function(r , b){
    b();
    if(r){
        printData(r , "hoja_4_3metros");
    }
}    
printGraphycHoja53Metros = function(r , b){
    b();
    if(r){
        printData(r , "hoja_5_3metros");
    }
}

printGraphycHoja33Metros = function(r , b){
    b();
    if(r){
        printData(r , "hoja_3_3metros");
    }
}

printGraphycHojasTotales3Metros = function(r , b){
    b();
    if(r){
        printData(r , "hojas_total_3metros");
    }
}

printGraphycMenor5 = function(r , b){
    b();
    if(r){
        printData(r , "hoja_vieja_menor_5");
    }
}

printGraphycLibreStrias = function(r , b){
    b();
    if(r){
        printData(r , "libre_estrias");
    }
}

printGraphycLibreCirug = function(r , b){
    b();
    if(r){
        printData(r , "libre_cirugia");
    }
}

printGraphycMayor5 = function(r , b){
    b();
    if(r){
        printData(r , "hoja_vieja_mayor_5");
    }
}

function printGraphycHojasTotales(r , b){
    b();
    if(r){
        printData(r , "hojas_total");
    }
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

var umbrales = {
    temp_min : 21,
    hoja_vieja_menor_5_3metros : 11,
    hojas_total : 13,
    libre_estrias_3metros : 6.5,
    hojas_total_3metros : 11,
    hojas_total_11semanas : 6.5
}

function printData(r , id){
    if(r && r.datos){
        var data = [];
        var options =  {
                series: {
                    lines: {
                        show: true,
                        lineWidth: 2,
                        fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 0.05
                            }, {
                                opacity: 0.01
                            }]
                        }
                    },
                    points: {
                        show: false,
                        radius: 3,
                        lineWidth: 1
                    },
                    shadowSize: 2
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    tickColor: "#eee",
                    borderColor: "#eee",
                    borderWidth: 1
                },
                colors: ["#d12610", "#37b7f3", "#52e136"],
                xaxis: {
                    ticks: 11,
                    tickDecimals: 0,
                    tickColor: "#eee",
                },
                yaxis: {
                    ticks: 11,
                    tickDecimals: 0,
                    tickColor: "#eee",
                    min: (r.min > 2) ? (r.min - 2) : r.min,
                }
            };
        var id = id;
        for(var info in r.datos){
            var dataClean = r.datos[info].map((v) => [v[0], parseFloat(v[1]) || 0])
            data.push({
                data : dataClean,
                label: (info==0)?'Umbral':info,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            });
        }
        var plot = $.plot($('#'+id), data, options);

        function showTooltip(x, y, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y + 5,
                left: x + 15,
                border: '1px solid #333',
                padding: '4px',
                color: '#fff',
                'border-radius': '3px',
                'background-color': '#333',
                opacity: 0.80
            }).appendTo("body").fadeIn(200);
        }

        var previousPoint = null;
        $('#'+id).bind("plothover", function(event, pos, item) {
            $("#x").text(pos.x.toFixed(0));
            $("#y").text(pos.y.toFixed(2));

            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                    showTooltip(item.pageX, item.pageY,"Año " + item.series.label + " Semana " + x + " Valor " + y);
                }
            } else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        
        });
    }   
}

function printDataFoliar(r , id){
    if(r){  
        var data = [];
        var id = id;
        
        for(var info in r.data){
            data.push({
                data : r.data[info],
                label: info,
                lines: {
                    lineWidth: 1,
                },
                shadowSize: 0
            })
        }
        var plot = $.plot($('#'+id), data, {
            series: {
                lines: {
                    show: true,
                    lineWidth: 2,
                    fill: true,
                    fillColor: {
                        colors: [{
                            opacity: 0.05
                        }, {
                            opacity: 0.01
                        }]
                    }
                },
                points: {
                    show: false,
                    radius: 3,
                    lineWidth: 1
                },
                shadowSize: 2
            },
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#eee",
                borderColor: "#eee",
                borderWidth: 1
            },
            colors: ["#d12610", "#37b7f3", "#52e136"],
            xaxis: {
                ticks: 11,
                tickDecimals: 0,
                tickColor: "#eee",
            },
            yaxis: {
                ticks: 11,
                tickDecimals: (r.max - r.min < .1) ? 2 : 1,
                tickColor: "#eee",
                max: (r.max + ((r.max - r.min) * .5)),
                min: (r.min - ((r.max - r.min) * .5))
            }
        });


        function showTooltip(x, y, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y + 5,
                left: x + 15,
                border: '1px solid #333',
                padding: '4px',
                color: '#fff',
                'border-radius': '3px',
                'background-color': '#333',
                opacity: 0.80
            }).appendTo("body").fadeIn(200);
        }

        var previousPoint = null;
        $('#'+id).bind("plothover", function(event, pos, item) {
            $("#x").text(pos.x.toFixed(0));
            $("#y").text(pos.y.toFixed(2));

            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                    showTooltip(item.pageX, item.pageY,"Año " + item.series.label + " Semana " + x + " Valor " + y);
                }
            } else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        
        });
    }
}

window.victor = init