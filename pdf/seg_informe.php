<html>
	<body id="contenedor" ng-controller="graficas" ng-cloak>
	<head>
		<meta charset="utf-8" />
		<style>
			html{
				padding: 15px;
				width: 100%;
			}
			.full{
				display: block;
				text-align: center;
			}
			.max_width{
				width: 100%;
			}
			#parent{
				display: flex;
			}
			.center {
			    display: inline-block;
			    margin: 0 auto;
			}
			.left {
			    display: inline-block;
			    margin: 0 0 0 auto;
			}
			.right {
			    display: inline-block;
			    margin: 0 auto 0 0;
			}
			.charts{
				width: 100% !important;
			}
			.image{
				height: 130px;
			}
		</style>
	</head>
		<div id="report" ng-init="init()">
			<br>
			<span class="full"><b>INFORME DEL ESTADO EVOLUTIVO DE LA SIGATOKA NEGRA</b></span>
			<br>
			<div id="parent"><div class="left"><b>FINCA:</b> <?= "NUEVA PUBENZA"?></div> <div class="center"><b>ZONA:</b> <?= "EL ORO"?></div> <div class="right"><b>SECTOR:</b> <?= "EL CAMBIO"?></div></div><br>
			<b>1. RESUMEN EJECUTIVO</b>
			<div style="border : 1px solid black; padding-left : 15px; padding-right : 15px;">
				Este informe corresponde a la semana 4 del año 2016. 

				Continúan bajos los niveles de infección, aunque se reitera que el organismo causal ya se está 

				desarrollando dentro de la hoja y, en cualquier momento aparece en hojas 3 o 4. Por el momento, 

				el hongo está confinado a la hoja 5 de plantas jóvenes.

				Los demás parámetros están normales.

				En cuanto a clima, la temperatura mantiene niveles bastante elevados y las lluvias ya están 

				posesionadas en todas las zonas con mayor o menor intensidad. Los reportes de NOAA siguen 

				manifestando amenaza o presencia de El Niño.

				La emisión foliar tiende al incremento que es característica de la época lluviosa (0.8 y 0.81 

				hojas/semana).

				Por lo tanto se recomienda optimizar el manejo de la finca con énfasis en las labores culturales 

				relacionadas con sigatoka, así como seguir con las frecuencias cortas.

				Los ácaros siguen presentes en las hojas. Esperemos que el último ciclo con fungicidas en 

				emulsión con aceite haya contribuido al control de esta plaga
			</div>
			<br><br>
			<span><b>GRAFICAS  ESTADO EVOLUTICO HOJAS  3 - 4 Y 5</b></span><br>
			<div>
				<div class="row">
		                <div class="col-md-12 col-sm-12">
		                    <div class="portlet light portlet-fit bordered">
		                        <div class="portlet-title">
		                            <div class="caption">
		                                <span class="caption-subject font-dark sbold uppercase">
		                                    <h3>
		                                       <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                    </h3>
		                                    <small>Hojas totales</small>
		                                </span>
		                            </div>
		                        </div>
		                        <div class="portlet-body">
		                            <div id="hojas_total" class="chart"> </div>
		                            <img class="image" id="hojas_total_img" style="display: none;" src=""> </img>
		                        </div>
		                    </div>
		                </div>
		        </div>
		        <!-- END INTERACTIVE CHART PORTLET-->
		        <!-- END DASHBOARD STATS 1-->
		        <div class="row">
		            <div class="col-md-6 col-sm-6">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small >Hoja mas vieja libre de quema menor a 5%</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div  id="hoja_vieja_menor_5" class="chart"> </div>
		                        <img class="image" id="hoja_vieja_menor_5_img" style="display: none;" src=""> </img>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		            <div class="col-md-6 col-sm-6">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Hoja mas vieja libre de estrias</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="libre_estrias" class="chart"> </div>
		                        <img class="image" id="libre_estrias_img" style="display: none;" src=""> </img>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		        </div>
		        <!-- END DASHBOARD STATS 1-->
		        <div class="row">
		            <div class="col-md-6 col-sm-6">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Libre de cirugia</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="libre_cirugia" class="chart"> </div>
		                        <img class="image" id="libre_cirugia_img" style="display: none;" src=""> </img>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		            <div class="col-md-6 col-sm-6">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet light portlet-fit bordered">
		                    <div class="portlet-title">
		                        <div class="caption">
		                            <span class="caption-subject font-dark sbold uppercase">
		                                <h3>
		                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
		                                </h3>
		                                <small>Hoja mas vieja libre de quema mayor a 5%</small>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="portlet-body">
		                        <div id="hoja_vieja_mayor_5" class="chart"> </div>
		                        <img class="image" id="hoja_vieja_mayor_5_img" style="display: none;" src=""> </img>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
		            </div>
		        </div>
		    </div>
			
		<div class="row">
        	<button ng-click="traer()" id="desPDF" type="button">Descargar PDF</button>
        </div>
	</body>
	<div id="page2">
		page2
	</div>
</html>