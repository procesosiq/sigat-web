var page = require('webpage').create();
page.paperSize ={
    format        : "A4",
    orientation    : "portrait",
    margin        : { left:"0.5cm", right:"0.5cm", top:"0.5cm", bottom:"0.5cm" },
    'header': {
			'height': '2.3cm',
			'contents': '<div style="text-align:center; font-size:12px"><img style="display: block; margin: auto;" src="http://sigat.procesos-iq.com/logo_grande.png" height="20" width="80">INFORME DEL ESTADO EVOLUTIVO DE LA SIGATOKA NEGRA</div>' // If you have 2 pages the result looks like this: HEADER 1 / 2 
	},
    footer : {
        height : "1cm",
        contents : phantom.callback(function(pageNum, numPages){
            return("Paginas : " + pageNum + " / " + numPages);
        })
    }
};


page.open('http://sigat.procesos-iq.com/printPDF.php?page=informe_pdf&token=7cbbc409ec990f19c78c75bd1e06f2', function() {
   window.setTimeout(function () {
	   	// page.evaluate(function() {
	   		// console.log(document.getElementById('contentText'));
			page.render('github.pdf');
			phantom.exit();
	   	// }
	}, 15000);


});


// page.open('http://sigat.procesos-iq.com/printPDF.php?page=informe_pdf&token=7cbbc409ec990f19c78c75bd1e06f2', function (status) {
//     page.includeJs('http://code.jquery.com/jquery-1.10.0.min.js', function() {
//         page.evaluate(function() {
//             // I couldn't figure out how to get this value programatically.
//             // To get it, print out a page while logging jQuery(document).height().
//             // You can get this value from (jQuery(document).height() / number_of_pages).
//             var height_per_page = 518.57;

//             // When items get cut off, the DOM shows the item as being very close
//             // to the top of the next page. So if we make the elements have a minimum
//             // distance from the top, we can add a page break before the element.
//             // You'll probably have to muck around with this value to get it to
//             // work.
//             var minimum_distance_from_top = 90;

//             // Select whatever elements you want to stop from being cut-off
//             jQuery('.crayon-syntax').each(function() {
//                 // Distance from top of the first page.
//                 var offset_top_document = jQuery(this).offset().top;

//                 // Distance from top of current page.
//                 var offset_top_page = offset_top_document % height_per_page;

//                 if (offset_top_page < minimum_distance_from_top)
//                     jQuery(this).css('page-break-before', 'always');
//             });
//         });

//         page.render('github.pdf');
//         phantom.exit();
//     });
// });