<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/

    if(isset($_GET["page"])){
        if($_GET["page"] != ""){
            $page = $_GET["page"];
        }else{
            $page = "404";
        }
    }else{
       $page = "index"; 
    }
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/


    $cdn = "http://cdn.procesos-iq.com/";
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" ng-app="app" id="global">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Sigat | Informe</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=$cdn?>global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?=$cdn?>global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?=$cdn?>global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?=$cdn?>layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <!-- <link href="<?=$cdn?>layouts/layout4/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color" /> -->
        <link href="<?=$cdn?>layouts/layout4/css/themes/sigat.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?=$cdn?>layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <style>
        ul.page-sidebar-menu > li > .active {
            background: #5b9bd1 !important;
        }
    
    </style>
    <body class="page-container-bg-solid page-header-fixed page-footer-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
            <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="content">
            <div class="page-content-wrapper">
                <?php include ("./pdf/{$page}.php"); ?>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!--[if lt IE 9]>
<script src="<?=$cdn?>global/plugins/respond.min.js"></script>
<script src="<?=$cdn?>global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?=$cdn?>global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=$cdn?>global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=$cdn?>global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/flot/jquery.flot.pie.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/flot/jquery.flot.stack.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/flot/jquery.flot.crosshair.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/flot/jquery.flot.axislabels.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=$cdn?>global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=$cdn?>pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular.min.js"></script>
        
        <script src="http://sigat.procesos-iq.com/views/js/main.js" type="text/javascript"></script>
        <script src="http://lessaworld.com/lessaworld/projects/flotCanvasText/jquery.flot.text.js"></script>
        <script src="http://mrrio.github.io/jsPDF/dist/jspdf.debug.js"></script>

        <script src="http://sigat.procesos-iq.com/pdf/js/base64.js" type="text/javascript"></script>
        <script src="http://sigat.procesos-iq.com/pdf/js/html2canvas.js" type="text/javascript"></script>
        <script src="http://sigat.procesos-iq.com/pdf/js/canvas2image.js" type="text/javascript"></script>
        <script src="http://sigat.procesos-iq.com/pdf/js/graficas.js?1" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?=$cdn?>layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->

        <!--<script src="js/echarts.min.js" type="text/javascript"></script>
        <script src="js/charts.js" type="text/javascript"></script>
        <script src="//unpkg.com/react@15/dist/react.min.js"></script>
        <script src="//unpkg.com/react-dom@15/dist/react-dom.min.js"></script>
        <script src="componentes/charts.js" type="text/javascript"></script>-->
    </body>

</html>