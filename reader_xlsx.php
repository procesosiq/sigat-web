<?php
header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors',1);
error_reporting(E_ALL);

/** DATABASE */
/*$mysqli = @new mysqli("localhost", "auditoriasbonita", "u[V(fTIUbcVb", "sigat");
#$mysqli = @new mysqli("localhost", "root", "", "sigat");
if (mysqli_connect_errno()) {
    printf("Falló la conexión: %s\n", mysqli_connect_error());
    exit();
}
$mysqli->set_charset("utf8");*/

#include './controllers/conexion.php';

/** Directorio de los JSON */
$path = realpath('./doc');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
foreach($objects as $name => $object){
    if('.' != $object->getFileName() && '..' != $object->getFileName() && $path != $object->getPath()){
        $pos1 = strpos($object->getPath(), "d_");

        if($pos1 !== false){
            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
            echo $ext;
            if('xlsx' == $ext){
                switch ($ext){

                    case 'xlsx':
                        $excel = trim(file_get_contents($object->getPathName()));
                        process_xlsx($excel, $object->getFileName());
                        break;

                    default:
                        break;
                }
            }
        }
    }
}

function process_xlsx($excel, $filename){
    #$conexion = new M_Conexion;
    echo $excel;
    #$filas = explode("\n", $excel);
    #foreach($filas as $key => $f){
    #    echo $filas;
        #$porciones[] = explode(";", $f);
    #}

    /*foreach($porciones as $fila => $item){
        foreach ($item as $celda => $value) {
            if($porciones[0][$celda] == "Rain"){
                $lluvia = $value;
            }
            if($porciones[0][$celda] == "Hi Temp"){
                $temp_maxima = $value;
            }
            if($porciones[0][$celda] == "Low Temp"){
                $temp_minima = $value;
            }
            if($porciones[0][$celda] == "Out Hum"){
                $humedad = $value;
            }
            if($porciones[0][$celda] == "Date"){
                $format = 'd/m/Y';
                $date = DateTime::createFromFormat($format, $value);
            }
            if($porciones[0][$celda] == "Solar Rad."){
                $rad_solar = $value;
            }
            if($porciones[0][$celda] == "Solar Energy"){
                $dias_sol = $value;
            }
        }
        if($fila != 0){
            $fecha = $date->format('Y-m-d');
            $consulta = "INSERT INTO datos_clima(temp_maxima,temp_minima,lluvia,humedad,rad_solar,dias_sol,fecha) VALUES($temp_maxima,$temp_minima,$lluvia,$humedad,$rad_solar,$dias_sol,'$fecha');";
            $conexion->Consultas($conexion->insert, $consulta);
        }
    }*/
}

function limpiar($String){
    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
    $String = str_replace(array('í','ì','î','ï'),"i",$String);
    $String = str_replace(array('é','è','ê','ë'),"e",$String);
    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
    $String = str_replace("ç","c",$String);
    $String = str_replace("Ç","C",$String);
    $String = str_replace("ñ","n",$String);
    $String = str_replace("Ñ","N",$String);
    $String = str_replace("Ý","Y",$String);
    $String = str_replace("ý","y",$String);
     
    $String = str_replace("&aacute;","a",$String);
    $String = str_replace("&Aacute;","A",$String);
    $String = str_replace("&eacute;","e",$String);
    $String = str_replace("&Eacute;","E",$String);
    $String = str_replace("&iacute;","i",$String);
    $String = str_replace("&Iacute;","I",$String);
    $String = str_replace("&oacute;","o",$String);
    $String = str_replace("&Oacute;","O",$String);
    $String = str_replace("&uacute;","u",$String);
    $String = str_replace("&Uacute;","U",$String);
    return $String;
}

?>