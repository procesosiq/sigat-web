<?php
	/**
	*  CLASS FROM Fincas
	*/
	class Ciclos2 
	{
		private $conexion;
		private $session;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		public function index(){
			$sWhere = "";
			$sOrder = " ORDER BY id";
			$DesAsc = "ASC";
			$sOrder .= " {$DesAsc}";
			$sLimit = "";
			if(isset($_POST)){

				/*----------  ORDER BY ----------*/
				
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY id {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY fecha {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY nombre {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY email {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY status {$DesAsc}";
				}
				/*----------  ORDER BY ----------*/

				if(isset($_POST['search_id']) && trim($_POST['search_id']) != ""){
					$sWhere .= " AND id = ".$_POST["search_id"];
				}				
				if((isset($_POST['search_date_from']) && trim($_POST['search_date_from']) != "") && (isset($_POST['search_date_to']) && trim($_POST['search_date_to']) != "")){
					$sWhere .= " AND fecha BETWEEN '".$_POST["search_date_from"]."' AND '".$_POST["search_date_to"]."'";
				}
				if(isset($_POST['search_name']) && trim($_POST['search_name']) != ""){
					$sWhere .= " AND nombre LIKE '%".$_POST['search_name']."%'";
				}
				if(isset($_POST['search_email']) && trim($_POST['search_email']) != ""){
					$sWhere .= " AND email LIKE '%".$_POST['search_email']."%'";
				}
				if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
					$sWhere .= " AND status = ".$_POST["order_status"];
				}

				/*----------  LIMIT  ----------*/
				if(isset($_POST['length']) && $_POST['length'] > 0){
					$sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
				}
			}

			$sql = "SELECT ciclos_de_app.*, ciclo_producto1.nombre_comun AS producto1, ciclo_producto1.dosis AS dosis1,
			ciclo_producto2.nombre_comun AS producto2, ciclo_producto2.dosis AS dosis2 FROM ciclos_de_app JOIN ciclo_producto1
			ON ciclo_producto1.id_ciclo = ciclos_de_app.id JOIN ciclo_producto2
			ON ciclo_producto2.id_ciclo = ciclos_de_app.id WHERE id_usuario = '{$this->session->logged}' $sWhere $sOrder $sLimit";
			$res = $this->conexion->link->query($sql);
			$datos = (object)[
				"customActionMessage" => "Error al consultar la informacion",
				"customActionStatus" => "Error",
				"data" => [],
				"draw" => 0,
				"recordsFiltered" => 0,
				"recordsTotal" => 0,
			];
			$count = 1;
			while($fila = $res->fetch_assoc()){
				$fila = (object)$fila;
				$count++;
				$datos->data[] = [
					$fila->id,
					$fila->fecha,
					'',
					$fila->ciclo,
					'',
					$fila->producto1,
					$fila->dosis1,
					$fila->producto2,
					$fila->dosis2,
					$fila->area_app,
					$fila->status,
					'<button class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
				];
			}
			$datos->recordsTotal = count($datos->data);
			$datos->customActionMessage = "Informacion completada con exito";
			$datos->customActionStatus = "OK";

			return json_encode($datos);
		}

		public function create(){
			//echo '<script>alert("lalal");</script>';
			extract($_POST);
				
				$sql = "INSERT INTO ciclos_de_app SET 
				id_usuario = '{$this->session->logged}',
				fecha = CURRENT_DATE ,
				ciclo = '{$txtciclo}', 
				compania_de_aplicacion = '{$txtcompañia}' , 
				area_app = '{$txtarea}' , 
				costo_app_ha = {$txtcosto} , 
				costo_total_app = {$txtotal}";
				$ids = $this->conexion->Consultas(1,$sql);
        		return $ids;
			
		}

		public function update(){
			extract($_POST);
			if((int)$id > 0 && $txtnom != "" && $id_cliente != ""){
				$sql = "UPDATE cat_haciendas SET 
					nombre = '{$txtnom}' , 
					id_cliente = '{$id_cliente}' , 
					fecha = CURRENT_DATE 
					WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		public function changeStatus(){
			extract($_POST);
			if((int)$id > 0 ){
				$status = ($estado == 'activo') ? 1 : 0;
				$sql = "UPDATE cat_haciendas SET status={$status} WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				// echo $sql;
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		public function edit(){
			$response = (object)[
				"success" => 400,
				"data" => [],
				"clientes" => [],
				"agrupaciones" => [],
				"lotes" => [],
			];
			
			$response->clientes = $this->getClient();
			if(isset($_GET['id'])){
				$id = (int)$_GET['id'];
				$response->agrupaciones = $this->getAgrupaciones($id);
				if($id > 0){
					$sql = "SELECT ciclos_de_app.*, ciclo_producto1.nombre_comun AS producto1, ciclo_producto1.dosis AS dosis1,
							ciclo_producto2.nombre_comun AS producto2, ciclo_producto2.dosis AS dosis2 FROM ciclos_de_app JOIN ciclo_producto1
			ON ciclo_producto1.id_ciclo = ciclos_de_app.id JOIN ciclo_producto2
			ON ciclo_producto2.id_ciclo = ciclos_de_app.id WHERE id_usuario = '{$this->session->logged}' AND id = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->data = (object)$res->fetch_assoc();
					}
					$response->lotes = $this->getLotes($id);
					$response->success = 200;
				}
			}
			return json_encode($response);
		}

		public function addLote(){
			extract($_POST);
			$lotes = [];
			if((int)$id > 0 && $id_cliente != ""){
				$sql = "INSERT INTO cat_lotes SET
						id_hacienda = '{$id}',
						id_agrupacion = '{$id_agrupacion}',
						nombre = '{$lote}',
						area = '{$area}',
						id_usuario = '{$this->session->logged}',
						id_cliente = '{$id_cliente}'";
				$this->conexion->Consultas(1,$sql);
				$lotes = $this->getLotes($id);
			}
			return json_encode($lotes);
		}

		public function positionLote(){
			extract($_POST);
			$lotes = [];
			if(is_array($id_lotes) && count($id_lotes) > 0){
				foreach ($id_lotes as $key => $value) {
					$sql = "UPDATE cat_lotes SET position={$key} 
						WHERE id = '{$value}' AND id_usuario = '{$this->session->logged}' AND id_hacienda = '{$id}'";
					$this->conexion->Consultas(1,$sql);
				}
			}
			$lotes = $this->getLotes($id);
			return json_encode($lotes);
		}

		public function removeLote(){
			extract($_POST);
			$lotes = [];
			if((int)$id > 0 && $id_cliente != "" && $id_lote> 0){
				$sql = "UPDATE cat_lotes SET status=0 WHERE id = {$id_lote} AND id_usuario = '{$this->session->logged}' AND id_hacienda = '{$id}'";
				$this->conexion->Consultas(1,$sql);
			}
			$lotes = $this->getLotes($id);
			return json_encode($lotes);
		}

		private function getLotes($id_hacienda){
			$lotes = [];
			if((int)$id_hacienda > 0 && $id_hacienda != ""){
				$sql="SELECT id,(SELECT nombre FROM cat_agrupaciones WHERE id = id_agrupacion) AS agrupacion, nombre , 
				area FROM cat_lotes  
				WHERE id_usuario = '{$this->session->logged}' AND id_hacienda = '{$id_hacienda}' AND status > 0
				ORDER BY position";
				$res = $this->conexion->link->query($sql);
				while($fila = $res->fetch_assoc()){
					$lotes[] = (object)$fila;
				}
			}
			return $lotes;
		}

		private function getClient(){
			$cliente = [];
			$sql = "SELECT id , nombre FROM cat_clientes WHERE id_usuario = '{$this->session->logged}' AND status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$cliente[] = (object)$fila;
			}

			return $cliente;
		}

		private function getAgrupaciones($id_hacienda){
			$cliente = [];
			$sql = "SELECT id , nombre FROM cat_agrupaciones WHERE id_usuario = '{$this->session->logged}' AND id_hacienda = '{$id_hacienda}' AND status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$cliente[] = (object)$fila;
			}

			return $cliente;
		}
	}
?>
