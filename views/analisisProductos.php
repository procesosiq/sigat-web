<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    $cdn = "http://cdn.procesos-iq.com/";
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/    
    include '../controllers/conexion.php';
    $db = new M_Conexion();
    $tiposProductos = $db->queryAll("SELECT id, nombre FROM cat_tipo_productos WHERE status = 1");
?>
<style>
.center-th{
    text-align: center;
}
.right-th{
    text-align: right;
}
</style>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?=$cdn?>/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN CONTENT BODY -->
<div class="page-content" ng-app="app" ng-controller="analisis">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Análisis Productos
            </h1>
        </div>
        <div class="page-toolbar">
            <div style="float:right">
                <div class="col-md-4">
                    <label>Tipo: </label>
                    <select class="input-sm" ng-model="filters.tipo_producto" ng-change="init()">
                        <option value="">TODOS</option>
                        <?php foreach($tiposProductos as $tipo): ?>
                        <option value="<?= $tipo->id ?>"><?= $tipo->nombre ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-offset-1 col-md-3">
                    <label>Año: </label>
                    <select class="input-sm" ng-model="filters.year" ng-change="init()">
                        <option value="{{ a }}" ng-repeat="a in anios" ng-selected="a == filters.year">{{ a }}</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="resumenCiclos">Inicio</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Listado</span>
        </li>
    </ul>

    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-5">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        Dólares por Proveedor
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                Exportar <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:;" ng-click="exportPrint('datatable_ajax_1')"> Imprimir </a>
                                </li>
                                <li>
                                    <a href="javascript:;" ng-click="exportExcel('datatable_ajax_1')">Excel</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <table class="table table-striped table-bordered table-hover" id="datatable_ajax_1">
                            <thead>
                                <tr role="row" class="heading" style="cursor: pointer;">
                                    <th ng-click="search.order = 'proveedor'; search.reverse = !search.reverse;"> Proveedor </th>
                                    <th ng-click="search.order = 'total'; search.reverse = !search.reverse;" class="right-th"> $ TOTAL </th>
                                    <th ng-click="search.order = 'total'; search.reverse = !search.reverse;" class="right-th"> % </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in proveedores | orderObjectBy:search.order:search.reverse">
                                    <td>{{ row.proveedor }}</td>
                                    <td class="right-th">{{ number_format(row.total, 2) }}</td>
                                    <td class="right-th">{{ number_format(row.total / (proveedores | sumOfValue:'total') * 100, 2) }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th class="right-th">{{ number_format((proveedores | sumOfValue: 'total'), 2) }}</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        Dólares por Tipo
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                Exportar <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:;" ng-click="exportPrint('datatable_ajax_3')"> Imprimir </a>
                                </li>
                                <li>
                                    <a href="javascript:;" ng-click="exportExcel('datatable_ajax_3')">Excel</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <table class="table table-striped table-bordered table-hover" id="datatable_ajax_3">
                            <thead>
                                <tr role="row" class="heading" style="cursor: pointer;">
                                    <th ng-click="search3.order = 'tipo'; search3.reverse = !search3.reverse;"> Tipo </th>
                                    <th ng-click="search3.order = 'total'; search3.reverse = !search3.reverse;" class="right-th"> $ TOTAL </th>
                                    <th ng-click="search3.order = 'total'; search3.reverse = !search3.reverse;" class="right-th"> % </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in tipos | orderObjectBy:search3.order:search3.reverse">
                                    <td>{{ row.tipo }}</td>
                                    <td class="right-th">{{ number_format(row.total, 2) }}</td>
                                    <td class="right-th">{{ number_format(row.total / (tipos | sumOfValue:'total') * 100, 2) }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th class="right-th">{{ number_format((tipos | sumOfValue: 'total'), 2) }}</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>

        <div class="col-md-7">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        Dólares por Producto
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                Exportar <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:;" ng-click="exportPrint('datatable_ajax_2')"> Imprimir </a>
                                </li>
                                <li>
                                    <a href="javascript:;" ng-click="exportExcel('datatable_ajax_2')">Excel</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <table class="table table-striped table-bordered table-hover" id="datatable_ajax_2">
                            <thead>
                                <tr role="row" class="heading" style="cursor: pointer;">
                                    <th ng-click="search2.order = 'producto'; search2.reverse = !search2.reverse"> Producto </th>
                                    <th ng-click="search2.order = 'cantidad'; search2.reverse = !search2.reverse" class="right-th"> Cantidad <small>(L/Kg)</small> </th>
                                    <th ng-click="search2.order = 'precio'; search2.reverse = !search2.reverse" class="right-th"> Precio </th>
                                    <th ng-click="search2.order = 'total'; search2.reverse = !search2.reverse" class="right-th"> $ TOTAL </th>
                                    <th ng-click="search2.order = 'total'; search2.reverse = !search2.reverse" class="right-th"> % </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat-start="row in productos | orderObjectBy:search2.order:search2.reverse" ng-click="row.expanded = !row.expanded;">
                                    <td>{{ row.producto }}</td>
                                    <td class="right-th">{{ number_format(row.cantidad, 2) }}</td>
                                    <td class="right-th">{{ number_format(row.precio, 2) }}</td>
                                    <td class="right-th">{{ number_format(row.total, 2) }}</td>
                                    <td class="right-th">{{ number_format((row.total / (productos | sumOfValue:'total')) * 100, 2) }}</td>
                                </tr>
                                <tr ng-repeat="prov in row.detalle" ng-repeat-end="row" ng-show="row.expanded">
                                    <td style="padding-left: 20px;">{{ prov.proveedor | capitalize }}</td>
                                    <td class="right-th">{{ number_format(prov.cantidad, 2) }}</td>
                                    <td class="right-th">{{ number_format(prov.precio, 2) }}</td>
                                    <td class="right-th">{{ number_format(prov.total, 2) }}</td>
                                    <td>{{ number_format(prov.total / row.total * 100, 2) }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>TOTAL:</th>
                                    <th class="right-th">{{ productos | sumOfValue:'cantidad' | number: 2 }}</th>
                                    <th class="right-th">{{ productos | avgOfValue:'precio' | number: 2 }}</th>
                                    <th class="right-th">{{ number_format((productos | sumOfValue:'total'), 2) }}</th>
                                    <th class="right-th"> </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->