<?php
        include_once ("./controllers/datos_ciclos.php");
        $Ciclos = new Datos_Ciclos();
        $datos = (object)$Ciclos->getIndicadores();
?>
                    <div class="row" style="padding: 5px;">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="fa fa-comments"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo round($datos->ciclos_promedio->ciclos_promedio,2)?>">0</span>
                                    </div>
                                    <div class="desc"> Ciclos Promedio </div>
                                </div>
                                <a class="more" href="javascript:;"> +
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo round($datos->ciclos_menor->min_finca,2)?>">0</span></div>
                                    <div class="desc"> <?=$datos->ciclos_menor->finca?>. Mayor Ciclos </div>
                                </div>
                                <a class="more" href="javascript:;"> +
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green-jungle">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo round($datos->ciclos_mayor->max_finca,2)?>">0</span>
                                    </div>
                                    <div class="desc"> <?=$datos->ciclos_mayor->finca?>. Menor Ciclos </div>
                                </div>
                                <a class="more" href="javascript:;"> +
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding: 5px;">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number"> 
                                        <span data-counter="counterup" data-value="<?php echo round($datos->ha_ciclos->ha_ciclos,2)?>"></span></div>
                                    <div class="desc"> $/Ha Ciclos Promedio </div>
                                </div>
                                <a class="more" href="javascript:;"> +
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number"> 
                                        <span data-counter="counterup" data-value="<?php echo round($datos->ha_ciclos_max->costo_ha,2)?>"></span></div>
                                    <div class="desc"> <?=$datos->ha_ciclos_max->finca?>. Mayor $/Ha Ciclos </div>
                                </div>
                                <a class="more" href="javascript:;"> +
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green-jungle">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number"> 
                                        <span data-counter="counterup" data-value="<?php echo round($datos->ha_ciclos_min->costo_ha,2)?>"></span></div>
                                    <div class="desc"> <?=$datos->ha_ciclos_min->finca?>. Menor $/Ha Ciclos </div>
                                </div>
                                <a class="more" href="javascript:;"> +
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding: 5px;">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number"> 
                                        <span data-counter="counterup" data-value="<?php echo round($datos->ha_ciclos_anio->costo_ha,2)?>"></span></div>
                                    <div class="desc"> $/Ha año promedio </div>
                                </div>
                                <a class="more" href="javascript:;"> +
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number"> 
                                        <span data-counter="counterup" data-value="<?php echo round($datos->ha_ciclos_anio_max->ha_ciclos_max,2)?>"></span></div>
                                    <div class="desc"><?=$datos->ha_ciclos_anio_max->finca?>. Mayor $/Ha año </div>
                                </div>
                                <a class="more" href="javascript:;"> +
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green-jungle">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number"> 
                                        <span data-counter="counterup" data-value="<?php echo round($datos->ha_ciclos_anio_min->ha_ciclos_min,2)?>"></span></div>
                                    <div class="desc"><?=$datos->ha_ciclos_anio_min->finca?>. Menor $/Ha año </div>
                                </div>
                                <a class="more" href="javascript:;"> +
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>