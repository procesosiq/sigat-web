<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?> 
    <style>
        ul{
            list-style-type: none;
            display: inline-block;
        }
        ul li{
            display: inline;
            margin-right: 10px;
        }
        .miniature{
            display: inline-block;
        }
    </style>
                <!-- BEGIN CONTENT BODY -->
                <div ng-app="app" class="page-content" ng-controller="clima" ng-cloak>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head" ng-init="init()">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Clima
                                <small>Estadisticas del clima</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <?php include './views/clientes_tags.php';?>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Dashboard</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <!-- BEGIN DASHBOARD STATS 1-->
                        <?php include("clima_tags.php");?>
                    <!-- BEGIN INTERACTIVE CHART PORTLET-->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Temperatura minima</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="temp_min" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_temp_minima">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END INTERACTIVE CHART PORTLET-->
                    <!-- END DASHBOARD STATS 1-->
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Precipitacion (mm lluvia)</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="precp" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_lluvia">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Temperatura Maxima</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="temp_max" class="chart"> </div>
                                    <div class="miniature">
                                        <ul id="check_temp_maxima">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                    </div>
                    <!-- END DASHBOARD STATS 1-->
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Radiacion Solar</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="radiacion" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_rad_solar">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Dias Sol</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="diassol" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_dias_sol">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                    </div>
                    <!-- END DASHBOARD STATS 1-->
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Humedad Relativa Maxima</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="humedadmax" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_hum_max">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Humedad Relativa Minima</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="humedadmin" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_hum_min">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->