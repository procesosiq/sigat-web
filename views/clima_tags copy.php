<?php
        include ("./controllers/clima_header.php");
        $clima = new Datos_TOP();
        $datos = (object)$clima->Clima_Top()[0];
?>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="fa fa-comments"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo round($datos->Tmin,2)?>">0</span> °C
                                    </div>
                                    <div class="desc"> Del dia </div>
                                </div>
                                <a class="more" href="javascript:;"> Temperatura Minima
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo round($datos->Tmax,2)?>">0</span>°C </div>
                                    <div class="desc"> Del dia </div>
                                </div>
                                <a class="more" href="javascript:;"> Temperatura Maxima
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo round($datos->Tlluvia,2)?>">0</span>mm
                                    </div>
                                    <div class="desc"> De la semana </div>
                                </div>
                                <a class="more" href="javascript:;"> Precipitacion
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat purple">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number"> 
                                        <span data-counter="counterup" data-value="<?php echo round($datos->rad_solar,2)?>"></span></div>
                                    <div class="desc"> De la semana </div>
                                </div>
                                <a class="more" href="javascript:;"> Radiacion Solar
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>