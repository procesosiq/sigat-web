<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    $cdn = "http://cdn.procesos-iq.com/";

    /*=====  End of Insertar aqui validacion  de usuarios  ======*/

    $session = Session::getInstance();
    $privilege = $session->privileges->matriz_orodelti;
    
    $write = $privilege == 'WRITE_ALL' || $privilege == 'ALL';
    $aprovee = $privilege == 'APROVEE' || $privilege == 'ALL';
?>
<style>
    td,th {
        text-align : center;
    }
    td {
        white-space: nowrap;
    }
    .min-width {
        min-width : 100px;
    }
    .pointer {
        cursor : pointer;
    }
    .add-product:hover .add-product-plus {
        visibility : visible;
    }
    .add-product-plus{
        visibility : hidden;
    }
    .error {
        border : 1px solid red;
    }

    .change {
        background-color : #58b2ff;
    }
    .borrar td, .borrar th {
        background-color : red !important;
    }
</style>
<link href="<?=$cdn?>/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<div class="page-content" ng-app="app" ng-controller="analisis">

    <div class="page-head">
        <div class="page-title">
            <h1>MATRIZ EXCEL</h1>
        </div>
        <div class="page-toolbar">
            <div style="float:right">
                <div class="col-md-offset-1 col-md-3">
                    <label>Año: </label><br>
                    <select id="anio" class="input-sm" style="width: 100%" ng-model="filters.year" ng-change="init()">
                        <option value="{{ a }}" ng-repeat="a in anios" ng-selected="a == filters.year">{{ a }}</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label>Hectareas: </label>
                    <select class="input-sm" ng-model="filters.tipoHectarea" ng-change="init()">
                        <option value="{{key}}" ng-repeat="(key, value) in tipoHectarea" ng-selected="key == filters.tipoHectarea">{{value}}</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label>Finca: </label>
                    <select class="input-sm" ng-model="filters.finca" ng-change="init()">
                        <option value="MATEO">MATEO</option>
                        <option value="MARIA PAULA">MARIA PAULA</option>
                        <option value="SAN ALBERTO 1">SAN ALBERTO 1</option>
                        <option value="SAN ALBERTO 2">SAN ALBERTO 2</option>
                        <option value="EVA MARIA">EVA MARIA</option>
                        <option value="DELIA MARGARITA">DELIA MARGARITA</option>
                        <option value="ISABELA">ISABELA</option>
                        <option value="ENVIDIA">ENVIDIA</option>
                        <option value="SAN JUAN">SAN JUAN</option>
                        <option value="MARTIN IGNACIO">MARTIN IGNACIO</option>
                        <option value="MARIA GRACIA">MARIA GRACIA</option>
                        <option value="DELIA GRACE">DELIA GRACE</option>
                        <option value="LA NIÑA">LA NIÑA</option>
                        <option value="SAN ANTONIO">SAN ANTONIO</option>
                        <option value="TOMAS">TOMAS</option>
                        <option value="LUCIANA">LUCIANA</option>
                        <option value="MATHIAS 2">MATHIAS 2</option>
                        <option value="MATHIAS 4">MATHIAS 4</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="resumenCiclos">Inicio</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Listado</span>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        SANIDAD
                    </div>
                </div>
                <div class="portlet-body">
                    <? if($write): ?>
                    <button class="btn btn-circle btn-success" ng-click="newRowSigatoka()">
                        Agregar fila <i class="fa fa-plus"></i>
                    </button>
                    <? endif ; ?>
                    <div class="table-scrollable">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th rowspan="2">CICLO</th>
                                    <th rowspan="2">PROGRAMA</th>
                                    <th rowspan="2">SEMANA</th>
                                    <th colspan="2">FECHA</th>
                                    <th rowspan="2">HAS</th>
                                    <th colspan="2">ATRASO</th>
                                    <th colspan="9" style="border : 2px solid black;">PRODUCTOS</th>
                                    <th colspan="9" style="border : 2px solid black;">DOSIS</th>
                                    <th colspan="9" style="border : 2px solid black;">CANTIDAD</th>
                                    <th colspan="9" style="border : 2px solid black;">PRECIOS PRODUCTO</th>
                                    <th colspan="9" style="border : 2px solid black;">COSTO TOTAL</th>
                                    <th rowspan="2">COSTO<br>OPERACION</th>
                                    <th rowspan="2">TOTAL COSTO<br>OPERACION</th>
                                    <th rowspan="2">COSTO TOTAL</th>
                                    <th rowspan="2">COSTO / HAS<br>{{ tipoHectarea[filters.tipoHectarea] }}</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th style="min-width: 80px !important;">PROG.</th>
                                    <th style="min-width: 80px !important;">REAL</th>

                                    <th>DIAS</th>
                                    <th>MOTIVO</th>

                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in sigatoka" ng-class="{ 'borrar' : row.por_borrar }">
                                    <td>
                                        <? if($write): ?>
                                        <button class="btn btn-sm btn-danger btn-circle" ng-click="deleteRow(row)" title="Borrar" ng-hide="row.por_borrar">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        <? endif ; ?>

                                        <? if($aprovee): ?>
                                        <button class="btn btn-sm btn-success btn-circle" ng-click="aproveeRow(row)" ng-if="row.need_aprovee == 1" title="Aprobar">
                                            <i class="fa fa-thumbs-o-up"></i>
                                        </button>
                                        <? endif ; ?>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'num_ciclo')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.num_ciclo }"
                                        title="{{ row.por_aprobar.num_ciclo ? ('Valor pasado : ' + (row.num_ciclo ? row.num_ciclo : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'num_ciclo'">
                                            {{ row.por_aprobar.num_ciclo || row.num_ciclo }}
                                        </span>
                                        <div ng-if="row.editing == 'num_ciclo'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.num_ciclo">
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'programa')" <? endif ; ?> 
                                        ng-class="{ 'change' : row.por_aprobar.programa }"
                                        title="{{ row.por_aprobar.programa ? ('Valor pasado : ' + (row.programa ? row.programa : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'programa'">
                                            {{ row.por_aprobar.programa || row.programa_display || row.programa }}
                                        </span>
                                        <div ng-if="row.editing == 'programa'">
                                            <select class="form-control inline" ng-model="row.por_aprobar.programa" id="editing">
                                                <option value="">Seleccione</option>
                                                <option value="Sigatoka">Sigatoka</option>
                                                <option value="Foliar">Foliar</option>
                                                <option value="Plagas">Plagas</option>
                                                <option value="Erwinia">Erwinia</option>
                                            </select>
                                            <button class="btn btn-xs btn-success" title="Guardar" ng-click="save(row)">
                                                <i class="fa fa-check"></i>
                                            </button>
                                            <button class="btn btn-xs btn-danger" title="Cancelar" ng-click="clear(row)">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'semana')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.semana }"
                                        title="{{ row.por_aprobar.semana ? ('Valor pasado : ' + (row.semana ? row.semana : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'semana'">
                                            {{ row.por_aprobar.semana || row.semana }}
                                        </span>
                                        <div ng-if="row.editing == 'semana'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.semana"
                                                autoComplete="off">
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'fecha_prog')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.fecha_prog }"
                                        title="{{ row.por_aprobar.fecha_prog ? ('Valor pasado : ' + (row.fecha_prog ? row.fecha_prog : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'fecha_prog'">
                                            {{ row.por_aprobar.fecha_prog ? (row.por_aprobar.fecha_prog | formatDate) : (row.fecha_prog | formatDate) }}
                                        </span>
                                        <div ng-if="row.editing == 'fecha_prog'">
                                            <input 
                                                id="editing"
                                                type="text"
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.fecha_prog"
                                                data-provide="datepicker" 
                                                data-date-format="yyyy-mm-dd" 
                                                data-language="es"
                                                value="{{ row.fecha_prog }}"
                                                readonly
                                                >
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'fecha_real')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.fecha_real }"
                                        title="{{ row.por_aprobar.fecha_real ? ('Valor pasado : ' + (row.fecha_real ? row.fecha_real : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'fecha_real'">
                                            {{ row.por_aprobar.fecha_real ? (row.por_aprobar.fecha_real | formatDate) : (row.fecha_real | formatDate) }}
                                        </span>
                                        <div ng-if="row.editing == 'fecha_real'">
                                            <input 
                                                id="editing"
                                                type="text"
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.fecha_real"
                                                data-provide="datepicker" 
                                                data-date-format="yyyy-mm-dd" 
                                                data-language="es"
                                                value="{{ row.fecha_real }}"
                                                readonly
                                                >
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'hectareas_fumigacion')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.hectareas_fumigacion }"
                                        title="{{ row.por_aprobar.hectareas_fumigacion ? ('Valor pasado : ' + (row.hectareas_fumigacion ? row.hectareas_fumigacion : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'hectareas_fumigacion'">
                                            {{ row.por_aprobar.hectareas_fumigacion || row.hectareas_fumigacion }}
                                        </span>
                                        <div ng-if="row.editing == 'hectareas_fumigacion'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.hectareas_fumigacion"
                                                autoComplete="off">
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'atraso')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.atraso }"
                                        title="{{ row.por_aprobar.atraso ? ('Valor pasado : ' + (row.atraso ? row.atraso : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'atraso'">
                                            {{ row.por_aprobar.atraso || row.atraso }}
                                        </span>
                                        <div ng-if="row.editing == 'atraso'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.atraso"
                                                autoComplete="off">
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'motivo')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.motivo }"
                                        title="{{ row.por_aprobar.motivo ? ('Valor pasado : ' + (row.motivo ? row.motivo : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'motivo'">
                                            {{ row.por_aprobar.motivo || row.motivo }}
                                        </span>
                                        <div ng-if="row.editing == 'motivo'">
                                            <select class="form-control inline" ng-model="row.por_aprobar.motivo" id="editing">
                                                <option value="Cond-Labores">Cond-Labores</option>
                                                <option value="Cond-Oper">Cond-Oper</option>
                                                <option value="Cond-Oper-Proc">Cond-Oper-Proc</option>
                                                <option value="Cond-Oper-Prod">Cond-Oper-Prod</option>
                                                <option value="Cond-Proc">Cond-Proc</option>
                                                <option value="Cond-Prod">Cond-Prod</option>
                                                <option value="Cond-Taura">Cond-Taura</option>
                                                <option value="Condiciones">Condiciones</option>
                                                <option value="Lab-Aud">Lab-Aud</option>
                                                <option value="Normal">Normal</option>
                                                <option value="Oper-Proc">Oper-Proc</option>
                                                <option value="Oper-Prod">Oper-Prod</option>
                                                <option value="Operaciones">Operaciones</option>
                                                <option value="Proc-Oper">Proc-Oper</option>
                                                <option value="Proc-Prod">Proc-Prod</option>
                                                <option value="Proceso">Proceso</option>
                                                <option value="Prod-Cond-Proc">Prod-Cond-Proc</option>
                                                <option value="Prod-Opera">Prod-Opera</option>
                                                <option value="Prod-Proc">Prod-Proc</option>
                                                <option value="Producto">Producto</option>
                                            </select>
                                            <button class="btn btn-xs btn-success" title="Guardar" ng-click="save(row)">
                                                <i class="fa fa-check"></i>
                                            </button>
                                            <button class="btn btn-xs btn-danger" title="Cancelar" ng-click="clear(row)">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>

                                    <!-- productos -->
                                    <td 
                                        <? if($write): ?> ng-dblclick="editProducto(row, row.productos_tmp[i-1] || row.productos[i-1])" <? endif ; ?> 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}" 
                                        class="pointer add-product"
                                        ng-class="{ 'change' : productoChange(row, i-1) }"
                                    >
                                        <span data-toggle="tooltip" data-placement="top" data-html="true" 
                                            ng-if="row.productos_tmp[i-1] && row.productos_tmp[i-1].nombre_comercial"
                                            title="
                                                {{ row.productos_tmp[i-1].nombre_comercial != 'AGUA' ? 'Proveedor : ' + row.productos_tmp[i-1].proveedor : '' }}<br>
                                                Dosis : {{ row.productos_tmp[i-1].dosis | number }}<br>
                                                {{ row.productos_tmp[i-1].nombre_comercial != 'AGUA' ? 'Precio : $' + (row.productos_tmp[i-1].precio | number) : '' }}<br>
                                                Cantidad : {{ row.productos_tmp[i-1].cantidad | number }}<br>
                                                {{ row.productos_tmp[i-1].nombre_comercial != 'AGUA' ? 'Total : $' + (row.productos_tmp[i-1].total | number) : '' }}"
                                        >
                                            <span>
                                                {{ row.productos_tmp[i-1].nombre_comercial }}
                                            </span>
                                        </span>
                                        <span data-toggle="tooltip" data-placement="top" data-html="true" 
                                            ng-if="row.productos[i-1] && row.productos[i-1].nombre_comercial && (row.productos[i-1] && !row.productos_tmp[i-1].nombre_comercial)"
                                            title="
                                                {{ row.productos[i-1].nombre_comercial != 'AGUA' ? 'Proveedor : ' + row.productos[i-1].proveedor : '' }}<br>
                                                Dosis : {{ row.productos[i-1].dosis | number }}<br>
                                                {{ row.productos[i-1].nombre_comercial != 'AGUA' ? 'Precio : $' + (row.productos[i-1].precio | number) : '' }}<br>
                                                Cantidad : {{ row.productos[i-1].cantidad | number }}<br>
                                                {{ row.productos[i-1].nombre_comercial != 'AGUA' ? 'Total : $' + (row.productos[i-1].total | number) : '' }}"
                                        >
                                            <span>
                                                {{ row.productos[i-1].nombre_comercial }}
                                            </span>
                                        </span>
                                        <i 
                                            ng-if="!row.productos[i-1] && !row.productos_tmp[i-1].nombre_comercial && !row.por_borrar" 
                                            ng-click="openAddProducto(row)" 
                                            class='fa fa-plus add-product-plus btn btn-sm btn-outline red'
                                        />
                                    </td>
                                    <!-- dosis -->
                                    <td 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}"
                                        ng-class="{ 'change' : row.productos_tmp[i-1] && row.productos[i-1].dosis != row.productos_tmp[i-1].dosis }"
                                    >
                                        {{ (row.productos_tmp[i-1].dosis || row.productos[i-1].dosis) | num }}
                                    </td>
                                    <!-- cantidad -->
                                    <td 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}" 
                                        class="pointer"
                                        ng-class="{ 'change' : row.productos_tmp[i-1] && row.productos[i-1].cantidad != row.productos_tmp[i-1].cantidad }"
                                    >
                                        <span data-toggle="tooltip" data-placement="top" data-html="true" 
                                            title="
                                                Ha : {{ row.por_aprobar.hectareas_fumigacion || row.hectareas_fumigacion }}<br>
                                                Dosis : {{ row.need_aprovee == 1 ? (row.productos_tmp[i-1].dosis | num) : (row.productos[i-1].dosis | num) }}<br>
                                        ">
                                            {{ (row.productos_tmp[i-1].cantidad || row.productos[i-1].cantidad) | number }}
                                        </span>
                                    </td>
                                    <!-- precios -->
                                    <td 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}"
                                        ng-class="{ 'change' : row.productos_tmp[i-1] && row.productos[i-1].precio != row.productos_tmp[i-1].precio }"
                                    >
                                        {{ (row.productos_tmp[i-1].precio || row.productos[i-1].precio) | number }}
                                    </td>
                                    <!-- totales -->
                                    <td 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}" 
                                        class="pointer"
                                        ng-class="{ 'change' : row.productos_tmp[i-1] && row.productos[i-1].total != row.productos_tmp[i-1].total }"
                                    >
                                        <span data-toggle="tooltip" data-placement="top" data-html="true" 
                                            title="
                                                Producto : {{ row[row.need_aprovee == 1 ? 'productos_tmp' : 'productos'][i-1].nombre_comercial }}<br>
                                                Precio : $ {{ row[row.need_aprovee == 1 ? 'productos_tmp' : 'productos'][i-1].precio | number }}<br>
                                                Cantidad : {{ row[row.need_aprovee == 1 ? 'productos_tmp' : 'productos'][i-1].cantidad | number }}
                                        ">
                                            {{ (row.productos_tmp[i-1].total || row.productos[i-1].total) | number }}
                                        </span>
                                    </td>

                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'ha_oper')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.ha_oper }"
                                        title="{{ row.por_aprobar.ha_oper ? ('Valor pasado : ' + (row.ha_oper ? row.ha_oper : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'ha_oper'">
                                            {{ (row.por_aprobar.ha_oper || row.ha_oper) | number }}
                                        </span>
                                        <div ng-if="row.editing == 'ha_oper'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.ha_oper"
                                                autoComplete="off">
                                        </div>
                                    </td>
                                    <td>{{ row.oper = ((row.por_aprobar.ha_oper || row.ha_oper) * (row.por_aprobar.hectareas_fumigacion || row.hectareas_fumigacion)) | number : 2 }}</td>
                                    <td>{{ row.costo_total = (row.oper + (row.need_aprovee == 1 ? (row.productos_tmp | sumOfValue : 'total') : (row.productos | sumOfValue : 'total'))) | number : 2 }}</td>
                                    <td>{{ row.costo_ha = (
                                                row.costo_total / 
                                                (
                                                    row.por_aprobar.hectareas_fumigacion 
                                                        ? (row.por_aprobar.hectareas_fumigacion)
                                                        : (row.ha_total_sem)
                                                )
                                            ) | number : 2 
                                        }}
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>PROM</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>{{ sigatoka | avgOfValue : 'hectareas_fumigacion' | number : 2 }}</th>
                                    <th></th>
                                    <th></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>

                                    <th>
                                        <span ng-hide="true">
                                            {{ max_ciclo_sigatoka_conf = (sigatoka | maxOfValue : 'num_ciclo') }}
                                            {{ max_ciclo_sigatoka_tmp = (sigatoka | maxOfValue : 'por_aprobar' : 'num_ciclo') }}
                                            {{ max_ciclo_sigatoka = max_ciclo_sigatoka_conf >= max_ciclo_sigatoka_tmp ? max_ciclo_sigatoka_conf : max_ciclo_sigatoka_tmp }}
                                        </span>
                                    </th>
                                    <th>{{ (sigatoka | sumOfValue : 'oper')/max_ciclo_sigatoka | number : 2 }}</th>
                                    <th>{{ (sigatoka | sumOfValue : 'costo_total')/max_ciclo_sigatoka | number : 2 }}</th>
                                    <th>
                                        <span ng-show="filters.tipoHectarea == 'FUMIGACION'">
                                            {{ (sigatoka | sumOfValue : 'costo_total')/(sigatoka | sumOfValue : 'hectareas_fumigacion') | number : 2 }}
                                        </span>
                                        <span ng-show="filters.tipoHectarea != 'FUMIGACION'">
                                            {{ (sigatoka | sumOfValue : 'costo_total')/(sigatoka | avgOfValue : 'ha_total_sem')/max_ciclo_sigatoka | number : 2 }}
                                        </span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>TOTAL</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>{{ sigatoka | sumOfValue : 'hectareas_fumigacion' | number : 2 }}</th>
                                    <th></th>
                                    <th></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>

                                    <th></th>
                                    <th>{{ sigatoka | sumOfValue : 'oper' | number : 2 }}</th>
                                    <th>{{ sigatoka | sumOfValue : 'costo_total' | number : 2 }}</th>
                                    <th>{{ sigatoka | sumOfValue : 'costo_ha' | number : 2 }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        FOLIAR
                    </div>
                </div>
                <div class="portlet-body">
                    <? if($write): ?>
                    <button class="btn btn-circle btn-success" ng-click="newRowFoliar()">
                        Agregar fila <i class="fa fa-plus"></i>
                    </button>
                    <? endif ; ?>
                    <div class="table-scrollable">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th rowspan="2">CICLO</th>
                                    <th rowspan="2">PROGRAMA</th>
                                    <th rowspan="2">SEMANA</th>
                                    <th colspan="2">FECHA</th>
                                    <th rowspan="2">HAS</th>
                                    <th colspan="2">ATRASO</th>
                                    <th colspan="9" style="border : 2px solid black;">PRODUCTOS</th>
                                    <th colspan="9" style="border : 2px solid black;">DOSIS</th>
                                    <th colspan="9" style="border : 2px solid black;">CANTIDAD</th>
                                    <th colspan="9" style="border : 2px solid black;">PRECIOS PRODUCTO</th>
                                    <th colspan="9" style="border : 2px solid black;">COSTO TOTAL</th>
                                    <th rowspan="2">COSTO<br>OPERACION</th>
                                    <th rowspan="2">TOTAL COSTO<br>OPERACION</th>
                                    <th rowspan="2">COSTO TOTAL</th>
                                    <th rowspan="2">COSTO / HAS<br>{{ tipoHectarea[filters.tipoHectarea] }}</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th style="min-width: 80px !important;">PROG.</th>
                                    <th style="min-width: 80px !important;">REAL</th>

                                    <th>DIAS</th>
                                    <th>MOTIVO</th>

                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in foliar" ng-class="{ 'borrar' : row.por_borrar }">
                                    <td>
                                        <? if($write): ?>
                                        <button class="btn btn-sm btn-danger btn-circle" ng-click="deleteRow(row)" title="Borrar" ng-hide="row.por_borrar">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        <? endif ; ?>

                                        <? if($aprovee): ?>
                                        <button class="btn btn-sm btn-success btn-circle" ng-click="aproveeRow(row)" ng-if="row.need_aprovee == 1" title="Aprobar">
                                            <i class="fa fa-thumbs-o-up"></i>
                                        </button>
                                        <? endif ; ?>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'num_ciclo')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.num_ciclo }"
                                        title="{{ row.por_aprobar.num_ciclo ? ('Valor pasado : ' + (row.num_ciclo ? row.num_ciclo : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'num_ciclo'">
                                            {{ row.por_aprobar.num_ciclo || row.num_ciclo }}
                                        </span>
                                        <div ng-if="row.editing == 'num_ciclo'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.num_ciclo">
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'programa')" <? endif ; ?> 
                                        ng-class="{ 'change' : row.por_aprobar.programa }"
                                        title="{{ row.por_aprobar.programa ? ('Valor pasado : ' + (row.programa ? row.programa : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'programa'">
                                            {{ row.por_aprobar.programa || row.programa_display || row.programa }}
                                        </span>
                                        <div ng-if="row.editing == 'programa'">
                                            <select class="form-control inline" ng-model="row.por_aprobar.programa" id="editing">
                                                <option value="">Seleccione</option>
                                                <option value="Sigatoka">Sigatoka</option>
                                                <option value="Foliar">Foliar</option>
                                                <option value="Plagas">Plagas</option>
                                                <option value="Erwinia">Erwinia</option>
                                            </select>
                                            <button class="btn btn-xs btn-success" title="Guardar" ng-click="save(row)">
                                                <i class="fa fa-check"></i>
                                            </button>
                                            <button class="btn btn-xs btn-danger" title="Cancelar" ng-click="clear(row)">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'semana')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.semana }"
                                        title="{{ row.por_aprobar.semana ? ('Valor pasado : ' + (row.semana ? row.semana : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'semana'">
                                            {{ row.por_aprobar.semana || row.semana }}
                                        </span>
                                        <div ng-if="row.editing == 'semana'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.semana"
                                                autoComplete="off">
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'fecha_prog')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.fecha_prog }"
                                        title="{{ row.por_aprobar.fecha_prog ? ('Valor pasado : ' + (row.fecha_prog ? row.fecha_prog : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'fecha_prog'">
                                            {{ row.por_aprobar.fecha_prog ? (row.por_aprobar.fecha_prog | formatDate) : (row.fecha_prog | formatDate) }}
                                        </span>
                                        <div ng-if="row.editing == 'fecha_prog'">
                                            <input 
                                                id="editing"
                                                type="text"
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.fecha_prog"
                                                data-provide="datepicker" 
                                                data-date-format="yyyy-mm-dd" 
                                                data-language="es"
                                                value="{{ row.fecha_prog }}"
                                                readonly
                                                >
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'fecha_real')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.fecha_real }"
                                        title="{{ row.por_aprobar.fecha_real ? ('Valor pasado : ' + (row.fecha_real ? row.fecha_real : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'fecha_real'">
                                            {{ row.por_aprobar.fecha_real ? (row.por_aprobar.fecha_real | formatDate) : (row.fecha_real | formatDate) }}
                                        </span>
                                        <div ng-if="row.editing == 'fecha_real'">
                                            <input 
                                                id="editing"
                                                type="text"
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.fecha_real"
                                                data-provide="datepicker" 
                                                data-date-format="yyyy-mm-dd" 
                                                data-language="es"
                                                value="{{ row.fecha_real }}"
                                                readonly
                                                >
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'hectareas_fumigacion')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.hectareas_fumigacion }"
                                        title="{{ row.por_aprobar.hectareas_fumigacion ? ('Valor pasado : ' + (row.hectareas_fumigacion ? row.hectareas_fumigacion : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'hectareas_fumigacion'">
                                            {{ row.por_aprobar.hectareas_fumigacion || row.hectareas_fumigacion }}
                                        </span>
                                        <div ng-if="row.editing == 'hectareas_fumigacion'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.hectareas_fumigacion"
                                                autoComplete="off">
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'atraso')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.atraso }"
                                        title="{{ row.por_aprobar.atraso ? ('Valor pasado : ' + (row.atraso ? row.atraso : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'atraso'">
                                            {{ row.por_aprobar.atraso || row.atraso }}
                                        </span>
                                        <div ng-if="row.editing == 'atraso'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.atraso"
                                                autoComplete="off">
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'motivo')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.motivo }"
                                        title="{{ row.por_aprobar.motivo ? ('Valor pasado : ' + (row.motivo ? row.motivo : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'motivo'">
                                            {{ row.por_aprobar.motivo || row.motivo }}
                                        </span>
                                        <div ng-if="row.editing == 'motivo'">
                                            <select class="form-control inline" ng-model="row.por_aprobar.motivo" id="editing">
                                                <option value="Cond-Labores">Cond-Labores</option>
                                                <option value="Cond-Oper">Cond-Oper</option>
                                                <option value="Cond-Oper-Proc">Cond-Oper-Proc</option>
                                                <option value="Cond-Oper-Prod">Cond-Oper-Prod</option>
                                                <option value="Cond-Proc">Cond-Proc</option>
                                                <option value="Cond-Prod">Cond-Prod</option>
                                                <option value="Cond-Taura">Cond-Taura</option>
                                                <option value="Condiciones">Condiciones</option>
                                                <option value="Lab-Aud">Lab-Aud</option>
                                                <option value="Normal">Normal</option>
                                                <option value="Oper-Proc">Oper-Proc</option>
                                                <option value="Oper-Prod">Oper-Prod</option>
                                                <option value="Operaciones">Operaciones</option>
                                                <option value="Proc-Oper">Proc-Oper</option>
                                                <option value="Proc-Prod">Proc-Prod</option>
                                                <option value="Proceso">Proceso</option>
                                                <option value="Prod-Cond-Proc">Prod-Cond-Proc</option>
                                                <option value="Prod-Opera">Prod-Opera</option>
                                                <option value="Prod-Proc">Prod-Proc</option>
                                                <option value="Producto">Producto</option>
                                            </select>
                                            <button class="btn btn-xs btn-success" title="Guardar" ng-click="save(row)">
                                                <i class="fa fa-check"></i>
                                            </button>
                                            <button class="btn btn-xs btn-danger" title="Cancelar" ng-click="clear(row)">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>

                                    <!-- productos -->
                                    <td 
                                        <? if($write): ?> ng-dblclick="editProducto(row, row.productos_tmp[i-1] || row.productos[i-1])" <? endif ; ?> 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}" 
                                        class="pointer add-product"
                                        ng-class="{ 'change' : productoChange(row, i-1) }"
                                    >
                                        <span data-toggle="tooltip" data-placement="top" data-html="true" 
                                            ng-if="row.productos_tmp[i-1] && row.productos_tmp[i-1].nombre_comercial"
                                            title="
                                                {{ row.productos_tmp[i-1].nombre_comercial != 'AGUA' ? 'Proveedor : ' + row.productos_tmp[i-1].proveedor : '' }}<br>
                                                Dosis : {{ row.productos_tmp[i-1].dosis | number }}<br>
                                                {{ row.productos_tmp[i-1].nombre_comercial != 'AGUA' ? 'Precio : $' + (row.productos_tmp[i-1].precio | number) : '' }}<br>
                                                Cantidad : {{ row.productos_tmp[i-1].cantidad | number }}<br>
                                                {{ row.productos_tmp[i-1].nombre_comercial != 'AGUA' ? 'Total : $' + (row.productos_tmp[i-1].total | number) : '' }}"
                                        >
                                            <span>
                                                {{ row.productos_tmp[i-1].nombre_comercial }}
                                            </span>
                                        </span>
                                        <span data-toggle="tooltip" data-placement="top" data-html="true" 
                                            ng-if="row.productos[i-1] && row.productos[i-1].nombre_comercial && (row.productos[i-1] && !row.productos_tmp[i-1].nombre_comercial)"
                                            title="
                                                {{ row.productos[i-1].nombre_comercial != 'AGUA' ? 'Proveedor : ' + row.productos[i-1].proveedor : '' }}<br>
                                                Dosis : {{ row.productos[i-1].dosis | number }}<br>
                                                {{ row.productos[i-1].nombre_comercial != 'AGUA' ? 'Precio : $' + (row.productos[i-1].precio | number) : '' }}<br>
                                                Cantidad : {{ row.productos[i-1].cantidad | number }}<br>
                                                {{ row.productos[i-1].nombre_comercial != 'AGUA' ? 'Total : $' + (row.productos[i-1].total | number) : '' }}"
                                        >
                                            <span>
                                                {{ row.productos[i-1].nombre_comercial }}
                                            </span>
                                        </span>
                                        <i 
                                            ng-if="!row.productos[i-1] && !row.productos_tmp[i-1].nombre_comercial && !row.por_borrar" 
                                            ng-click="openAddProducto(row)" 
                                            class='fa fa-plus add-product-plus btn btn-sm btn-outline red'
                                        />
                                    </td>
                                    <!-- dosis -->
                                    <td 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}"
                                        ng-class="{ 'change' : row.productos_tmp[i-1] && row.productos[i-1].dosis != row.productos_tmp[i-1].dosis }"
                                    >
                                        {{ (row.productos_tmp[i-1].dosis || row.productos[i-1].dosis) | num }}
                                    </td>
                                    <!-- cantidad -->
                                    <td 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}" 
                                        class="pointer"
                                        ng-class="{ 'change' : row.productos_tmp[i-1] && row.productos[i-1].cantidad != row.productos_tmp[i-1].cantidad }"
                                    >
                                        <span data-toggle="tooltip" data-placement="top" data-html="true" 
                                            title="
                                                Ha : {{ row.hectareas_fumigacion }}<br>
                                                Dosis : {{ (row.productos_tmp[i-1].dosis | num) }}<br>
                                        ">
                                            {{ (row.productos_tmp[i-1].cantidad || row.productos[i-1].cantidad) | number }}
                                        </span>
                                    </td>
                                    <!-- precios -->
                                    <td 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}"
                                        ng-class="{ 'change' : row.productos_tmp[i-1] && row.productos[i-1].precio != row.productos_tmp[i-1].precio }"
                                    >
                                        {{ (row.productos_tmp[i-1].precio || row.productos[i-1].precio) | number }}
                                    </td>
                                    <!-- totales -->
                                    <td 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}" 
                                        class="pointer"
                                        ng-class="{ 'change' : row.productos_tmp[i-1] && row.productos[i-1].total != row.productos_tmp[i-1].total }"
                                    >
                                        <span data-toggle="tooltip" data-placement="top" data-html="true" 
                                            title="
                                                Producto : {{ row[row.need_aprovee == 1 ? 'productos_tmp' : 'productos'][i-1].nombre_comercial }}<br>
                                                Precio : $ {{ row[row.need_aprovee == 1 ? 'productos_tmp' : 'productos'][i-1].precio | number }}<br>
                                                Cantidad : {{ row[row.need_aprovee == 1 ? 'productos_tmp' : 'productos'][i-1].cantidad | number }}
                                        ">
                                            {{ (row.productos_tmp[i-1].total || row.productos[i-1].total) | number }}
                                        </span>
                                    </td>

                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'ha_oper')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.ha_oper }"
                                        title="{{ row.por_aprobar.ha_oper ? ('Valor pasado : ' + (row.ha_oper ? row.ha_oper : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'ha_oper'">
                                            {{ row.por_aprobar.ha_oper || row.ha_oper }}
                                        </span>
                                        <div ng-if="row.editing == 'ha_oper'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.ha_oper"
                                                autoComplete="off">
                                        </div>
                                    </td>
                                    <td>{{ row.oper = ((row.por_aprobar.ha_oper || row.ha_oper) * (row.por_aprobar.hectareas_fumigacion || row.hectareas_fumigacion)) | number : 2 }}</td>
                                    <td>{{ row.costo_total = (row.oper + (row.need_aprovee == 1 ? (row.productos_tmp | sumOfValue : 'total') : (row.productos | sumOfValue : 'total'))) | number : 2 }}</td>
                                    <td>{{ row.costo_ha = (row.costo_total / row.ha_total_sem) | number : 2 }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>PROM</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>{{ foliar | avgOfValue : 'hectareas_fumigacion' | number : 2 }}</th>
                                    <th></th>
                                    <th></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>

                                    <th>
                                        <span ng-hide="true">
                                            {{ max_ciclo_foliar_conf = (foliar | maxOfValue : 'num_ciclo') }}
                                            {{ max_ciclo_foliar_tmp = (foliar | maxOfValue : 'por_aprobar' : 'num_ciclo') }}
                                            {{ max_ciclo_foliar = max_ciclo_foliar_conf >= max_ciclo_foliar_tmp ? max_ciclo_foliar_conf : max_ciclo_foliar_tmp }}
                                        </span>
                                    </th>
                                    <th>{{ (foliar | sumOfValue : 'oper')/max_ciclo_foliar | number : 2 }}</th>
                                    <th>{{ (foliar | sumOfValue : 'costo_total')/max_ciclo_foliar | number : 2 }}</th>
                                    <th>
                                        <span ng-show="filters.tipoHectarea == 'FUMIGACION'">
                                            {{ (foliar | sumOfValue : 'costo_total')/(foliar | sumOfValue : 'hectareas_fumigacion') | number : 2 }}
                                        </span>
                                        <span ng-show="filters.tipoHectarea != 'FUMIGACION'">
                                            {{ (foliar | sumOfValue : 'costo_total')/(foliar | avgOfValue : 'ha_total_sem')/max_ciclo_foliar | number : 2 }}
                                        </span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>TOTAL</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>{{ foliar | sumOfValue : 'hectareas_fumigacion' | number : 2 }}</th>
                                    <th></th>
                                    <th></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>

                                    <th></th>
                                    <th>{{ foliar | sumOfValue : 'oper' | number : 2 }}</th>
                                    <th>{{ foliar | sumOfValue : 'costo_total' | number : 2 }}</th>
                                    <th>{{ foliar | sumOfValue : 'costo_ha' | number : 2 }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        PARCIAL
                    </div>
                </div>
                <div class="portlet-body">
                    <? if($write): ?>
                    <button class="btn btn-circle btn-success" ng-click="newRowParcial()">
                        Agregar fila <i class="fa fa-plus"></i>
                    </button>
                    <? endif ; ?>
                    <div class="table-scrollable">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th rowspan="2">CICLO</th>
                                    <th rowspan="2">PROGRAMA</th>
                                    <th rowspan="2">SEMANA</th>
                                    <th colspan="2">FECHA</th>
                                    <th rowspan="2">HAS</th>
                                    <th colspan="2">ATRASO</th>
                                    <th colspan="9" style="border : 2px solid black;">PRODUCTOS</th>
                                    <th colspan="9" style="border : 2px solid black;">DOSIS</th>
                                    <th colspan="9" style="border : 2px solid black;">CANTIDAD</th>
                                    <th colspan="9" style="border : 2px solid black;">PRECIOS PRODUCTO</th>
                                    <th colspan="9" style="border : 2px solid black;">COSTO TOTAL</th>
                                    <th rowspan="2">COSTO<br>OPERACION</th>
                                    <th rowspan="2">TOTAL COSTO<br>OPERACION</th>
                                    <th rowspan="2">COSTO TOTAL</th>
                                    <th rowspan="2">COSTO / HAS<br>{{ tipoHectarea[filters.tipoHectarea] }}</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th style="min-width: 80px !important;">PROG.</th>
                                    <th style="min-width: 80px !important;">REAL</th>

                                    <th>DIAS</th>
                                    <th>MOTIVO</th>

                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in parcial" ng-class="{ 'borrar' : row.por_borrar }">
                                    <td>
                                        <? if($write): ?>
                                        <button class="btn btn-sm btn-danger btn-circle" ng-click="deleteRow(row)" title="Borrar" ng-hide="row.por_borrar">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        <? endif ; ?>

                                        <? if($aprovee): ?>
                                        <button class="btn btn-sm btn-success btn-circle" ng-click="aproveeRow(row)" ng-if="row.need_aprovee == 1" title="Aprobar">
                                            <i class="fa fa-thumbs-o-up"></i>
                                        </button>
                                        <? endif ; ?>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'sum_ciclo')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.sum_ciclo }"
                                        title="{{ row.por_aprobar.sum_ciclo ? ('Valor pasado : ' + (row.sum_ciclo ? row.sum_ciclo : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'sum_ciclo'">
                                            {{ row.por_aprobar.sum_ciclo || row.sum_ciclo }}
                                        </span>
                                        <div ng-if="row.editing == 'sum_ciclo'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.sum_ciclo">
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'programa')" <? endif ; ?> 
                                        ng-class="{ 'change' : row.por_aprobar.programa }"
                                        title="{{ row.por_aprobar.programa ? ('Valor pasado : ' + (row.programa ? row.programa : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'programa'">
                                            {{ row.por_aprobar.programa || row.programa }}
                                        </span>
                                        <div ng-if="row.editing == 'programa'">
                                            <select class="form-control inline" ng-model="row.por_aprobar.programa" id="editing">
                                                <option value="">Seleccione</option>
                                                <option value="Sigatoka">Sigatoka</option>
                                                <option value="Foliar">Foliar</option>
                                                <option value="Plagas">Plagas</option>
                                                <option value="Erwinia">Erwinia</option>
                                            </select>
                                            <button class="btn btn-xs btn-success" title="Guardar" ng-click="save(row)">
                                                <i class="fa fa-check"></i>
                                            </button>
                                            <button class="btn btn-xs btn-danger" title="Cancelar" ng-click="clear(row)">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'semana')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.semana }"
                                        title="{{ row.por_aprobar.semana ? ('Valor pasado : ' + (row.semana ? row.semana : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'semana'">
                                            {{ row.por_aprobar.semana || row.semana }}
                                        </span>
                                        <div ng-if="row.editing == 'semana'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.semana"
                                                autoComplete="off">
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'fecha_prog')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.fecha_prog }"
                                        title="{{ row.por_aprobar.fecha_prog ? ('Valor pasado : ' + (row.fecha_prog ? row.fecha_prog : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'fecha_prog'">
                                            {{ row.por_aprobar.fecha_prog ? (row.por_aprobar.fecha_prog | formatDate) : (row.fecha_prog | formatDate) }}
                                        </span>
                                        <div ng-if="row.editing == 'fecha_prog'">
                                            <input 
                                                id="editing"
                                                type="text"
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.fecha_prog"
                                                data-provide="datepicker" 
                                                data-date-format="yyyy-mm-dd" 
                                                data-language="es"
                                                value="{{ row.fecha_prog }}"
                                                readonly
                                                >
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'fecha_real')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.fecha_real }"
                                        title="{{ row.por_aprobar.fecha_real ? ('Valor pasado : ' + (row.fecha_real ? row.fecha_real : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'fecha_real'">
                                            {{ row.por_aprobar.fecha_real ? (row.por_aprobar.fecha_real | formatDate) : (row.fecha_real | formatDate) }}
                                        </span>
                                        <div ng-if="row.editing == 'fecha_real'">
                                            <input 
                                                id="editing"
                                                type="text"
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.fecha_real"
                                                data-provide="datepicker" 
                                                data-date-format="yyyy-mm-dd" 
                                                data-language="es"
                                                value="{{ row.fecha_real }}"
                                                readonly
                                                >
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'hectareas_fumigacion')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.hectareas_fumigacion }"
                                        title="{{ row.por_aprobar.hectareas_fumigacion ? ('Valor pasado : ' + (row.hectareas_fumigacion ? row.hectareas_fumigacion : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'hectareas_fumigacion'">
                                            {{ row.por_aprobar.hectareas_fumigacion || row.hectareas_fumigacion }}
                                        </span>
                                        <div ng-if="row.editing == 'hectareas_fumigacion'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.hectareas_fumigacion"
                                                autoComplete="off">
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'atraso')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.atraso }"
                                        title="{{ row.por_aprobar.atraso ? ('Valor pasado : ' + (row.atraso ? row.atraso : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'atraso'">
                                            {{ row.por_aprobar.atraso || row.atraso }}
                                        </span>
                                        <div ng-if="row.editing == 'atraso'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.atraso"
                                                autoComplete="off">
                                        </div>
                                    </td>
                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'motivo')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.motivo }"
                                        title="{{ row.por_aprobar.motivo ? ('Valor pasado : ' + (row.motivo ? row.motivo : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'motivo'">
                                            {{ row.por_aprobar.motivo || row.motivo }}
                                        </span>
                                        <div ng-if="row.editing == 'motivo'">
                                            <select class="form-control inline" ng-model="row.por_aprobar.motivo" id="editing">
                                                <option value="Cond-Labores">Cond-Labores</option>
                                                <option value="Cond-Oper">Cond-Oper</option>
                                                <option value="Cond-Oper-Proc">Cond-Oper-Proc</option>
                                                <option value="Cond-Oper-Prod">Cond-Oper-Prod</option>
                                                <option value="Cond-Proc">Cond-Proc</option>
                                                <option value="Cond-Prod">Cond-Prod</option>
                                                <option value="Cond-Taura">Cond-Taura</option>
                                                <option value="Condiciones">Condiciones</option>
                                                <option value="Lab-Aud">Lab-Aud</option>
                                                <option value="Normal">Normal</option>
                                                <option value="Oper-Proc">Oper-Proc</option>
                                                <option value="Oper-Prod">Oper-Prod</option>
                                                <option value="Operaciones">Operaciones</option>
                                                <option value="Proc-Oper">Proc-Oper</option>
                                                <option value="Proc-Prod">Proc-Prod</option>
                                                <option value="Proceso">Proceso</option>
                                                <option value="Prod-Cond-Proc">Prod-Cond-Proc</option>
                                                <option value="Prod-Opera">Prod-Opera</option>
                                                <option value="Prod-Proc">Prod-Proc</option>
                                                <option value="Producto">Producto</option>
                                            </select>
                                            <button class="btn btn-xs btn-success" title="Guardar" ng-click="save(row)">
                                                <i class="fa fa-check"></i>
                                            </button>
                                            <button class="btn btn-xs btn-danger" title="Cancelar" ng-click="clear(row)">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>

                                    <!-- productos -->
                                    <td 
                                        <? if($write): ?> ng-dblclick="editProducto(row, row.productos_tmp[i-1] || row.productos[i-1])" <? endif ; ?> 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}" 
                                        class="pointer add-product"
                                        ng-class="{ 'change' : productoChange(row, i-1) }"
                                    >
                                        <span data-toggle="tooltip" data-placement="top" data-html="true" 
                                            ng-if="row.productos_tmp[i-1] && row.productos_tmp[i-1].nombre_comercial"
                                            title="
                                                {{ row.productos_tmp[i-1].nombre_comercial != 'AGUA' ? 'Proveedor : ' + row.productos_tmp[i-1].proveedor : '' }}<br>
                                                Dosis : {{ row.productos_tmp[i-1].dosis | number }}<br>
                                                {{ row.productos_tmp[i-1].nombre_comercial != 'AGUA' ? 'Precio : $' + (row.productos_tmp[i-1].precio | number) : '' }}<br>
                                                Cantidad : {{ row.productos_tmp[i-1].cantidad | number }}<br>
                                                {{ row.productos_tmp[i-1].nombre_comercial != 'AGUA' ? 'Total : $' + (row.productos_tmp[i-1].total | number) : '' }}"
                                        >
                                            <span>
                                                {{ row.productos_tmp[i-1].nombre_comercial }}
                                            </span>
                                        </span>
                                        <span data-toggle="tooltip" data-placement="top" data-html="true" 
                                            ng-if="row.productos[i-1] && row.productos[i-1].nombre_comercial && (row.productos[i-1] && !row.productos_tmp[i-1].nombre_comercial)"
                                            title="
                                                {{ row.productos[i-1].nombre_comercial != 'AGUA' ? 'Proveedor : ' + row.productos[i-1].proveedor : '' }}<br>
                                                Dosis : {{ row.productos[i-1].dosis | number }}<br>
                                                {{ row.productos[i-1].nombre_comercial != 'AGUA' ? 'Precio : $' + (row.productos[i-1].precio | number) : '' }}<br>
                                                Cantidad : {{ row.productos[i-1].cantidad | number }}<br>
                                                {{ row.productos[i-1].nombre_comercial != 'AGUA' ? 'Total : $' + (row.productos[i-1].total | number) : '' }}"
                                        >
                                            <span>
                                                {{ row.productos[i-1].nombre_comercial }}
                                            </span>
                                        </span>
                                        <i 
                                            ng-if="!row.productos[i-1] && !row.productos_tmp[i-1].nombre_comercial && !row.por_borrar" 
                                            ng-click="openAddProducto(row)" 
                                            class='fa fa-plus add-product-plus btn btn-sm btn-outline red'
                                        />
                                    </td>
                                    <!-- dosis -->
                                    <td 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}"
                                        ng-class="{ 'change' : row.productos_tmp[i-1] && row.productos[i-1].dosis != row.productos_tmp[i-1].dosis }"
                                    >
                                        {{ (row.productos_tmp[i-1].dosis || row.productos[i-1].dosis) | num }}
                                    </td>
                                    <!-- cantidad -->
                                    <td 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}" 
                                        class="pointer"
                                        ng-class="{ 'change' : row.productos_tmp[i-1] && row.productos[i-1].cantidad != row.productos_tmp[i-1].cantidad }"
                                    >
                                        <span data-toggle="tooltip" data-placement="top" data-html="true" 
                                            title="
                                                Ha : {{ row.hectareas_fumigacion }}<br>
                                                Dosis : {{ (row.productos_tmp[i-1].dosis | num) }}<br>
                                        ">
                                            {{ (row.productos_tmp[i-1].cantidad || row.productos[i-1].cantidad) | number }}
                                        </span>
                                    </td>
                                    <!-- precios -->
                                    <td 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}"
                                        ng-class="{ 'change' : row.productos_tmp[i-1] && row.productos[i-1].precio != row.productos_tmp[i-1].precio }"
                                    >
                                        {{ (row.productos_tmp[i-1].precio || row.productos[i-1].precio) | number }}
                                    </td>
                                    <!-- totales -->
                                    <td 
                                        ng-repeat="i in [1,2,3,4,5,6,7,8,9]" 
                                        style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}" 
                                        class="pointer"
                                        ng-class="{ 'change' : row.productos_tmp[i-1] && row.productos[i-1].total != row.productos_tmp[i-1].total }"
                                    >
                                        <span data-toggle="tooltip" data-placement="top" data-html="true" 
                                            title="
                                                Producto : {{ row[row.need_aprovee == 1 ? 'productos_tmp' : 'productos'][i-1].nombre_comercial }}<br>
                                                Precio : $ {{ row[row.need_aprovee == 1 ? 'productos_tmp' : 'productos'][i-1].precio | number }}<br>
                                                Cantidad : {{ row[row.need_aprovee == 1 ? 'productos_tmp' : 'productos'][i-1].cantidad | number }}
                                        ">
                                            {{ (row.productos_tmp[i-1].total || row.productos[i-1].total) | number }}
                                        </span>
                                    </td>

                                    <td 
                                        <? if($write): ?> ng-dblclick="edit(row, 'ha_oper')" <? endif ; ?>
                                        ng-class="{ 'change' : row.por_aprobar.ha_oper }"
                                        title="{{ row.por_aprobar.ha_oper ? ('Valor pasado : ' + (row.ha_oper ? row.ha_oper : 'Vacio')) : '' }}"
                                    >
                                        <span ng-if="row.editing != 'ha_oper'">
                                            {{ row.por_aprobar.ha_oper || row.ha_oper }}
                                        </span>
                                        <div ng-if="row.editing == 'ha_oper'">
                                            <input 
                                                id="editing"
                                                type="number" 
                                                title="Enter para guardar; Escape para cancelar"
                                                key-bind="{ esc: 'clear(row)', enter: 'save(row)' }" 
                                                class="form-control min-width" 
                                                ng-model="row.por_aprobar.ha_oper"
                                                autoComplete="off">
                                        </div>
                                    </td>
                                    <td>{{ row.oper = ((row.por_aprobar.ha_oper || row.ha_oper) * (row.por_aprobar.hectareas_fumigacion || row.hectareas_fumigacion)) | number : 2 }}</td>
                                    <td>{{ row.costo_total = (row.oper + (row.need_aprovee == 1 ? (row.productos_tmp | sumOfValue : 'total') : (row.productos | sumOfValue : 'total'))) | number : 2 }}</td>
                                    <td>{{ row.costo_ha = (row.costo_total / row.ha_total_sem) | number : 2 }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>PROM</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>{{ parcial | avgOfValue : 'hectareas_fumigacion' | number : 2 }}</th>
                                    <th></th>
                                    <th></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>

                                    <th></th>
                                    <th>{{ (parcial | sumOfValue : 'oper')/parcial.length | number : 2 }}</th>
                                    <th>{{ (parcial | sumOfValue : 'costo_total')/parcial.length | number : 2 }}</th>
                                    <th>{{ (parcial | sumOfValue : 'costo_total')/(parcial | sumOfValue : 'ha_total_sem') | number : 2 }}</th>
                                </tr>
                                <tr>
                                    <th>TOTAL</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>{{ parcial | sumOfValue : 'hectareas_fumigacion' | number : 2 }}</th>
                                    <th></th>
                                    <th></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>

                                    <th></th>
                                    <th>{{ parcial | sumOfValue : 'oper' | number : 2 }}</th>
                                    <th>{{ parcial | sumOfValue : 'costo_total' | number : 2 }}</th>
                                    <th>{{ parcial | sumOfValue : 'costo_ha' | number : 2 }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-producto" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title inline">
                        CICLO {{ originalRow.ciclo }}. Modificar {{ originalProd.nombre_comercial }}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <b>Proveedor</b>
                            </div>
                            <div class="col-md-8">
                                <select class="form-control" ng-model="productoSelected.id_proveedor" ng-change="selectProveedor()" ng-class="{ error : !productoSelected.id_proveedor > 0 }">
                                    <option value="">Seleccione</option>
                                    <option value="{{ p.id }}" ng-repeat="p in proveedores">{{ p.nombre }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <b>Producto</b>
                            </div>
                            <div class="col-md-8">
                                <select class="form-control" ng-model="productoSelected.id_producto" ng-change="selectProducto()" ng-class="{ error : !productoSelected.id_producto > 0 }">
                                    <option value="">Seleccione</option>
                                    <option value="{{ p.id }}" ng-repeat="p in productos | filter : { id_proveedor : productoSelected.id_proveedor } : true">{{ p.nombre_comercial }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <b>Dosis</b>
                            </div>
                            <div class="col-md-8">
                                <select class="form-control" ng-model="productoSelected.dosis" id="select-dosis" ng-class="{ error : !productoSelected.dosis > 0 }">
                                    <option value="">Seleccione</option>
                                    <option value="{{ dosis }}" ng-selected="dosis == productoSelected.dosis" ng-repeat="dosis in productoSelected.producto.dosis_array track by $index">{{ dosis }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <b>Cantidad</b>
                            </div>
                            <div class="col-md-8 text-right">
                                {{ productoSelected.cantidad = multiplicar((originalRow.por_aprobar.hectareas_fumigacion || originalRow.hectareas_fumigacion), productoSelected.dosis) | number }}
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <b>Precio</b>
                            </div>
                            <div class="col-md-8 text-right">
                                <span class="pull-left">$</span>
                                {{ productoSelected.precio }}
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <b>Total</b>
                            </div>
                            <div class="col-md-8 text-right">
                                <span class="pull-left">$</span>
                                {{ productoSelected.precio * productoSelected.cantidad | number : 2 }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" ng-disabled="saving" ng-click="saveProducto()">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="agregar-producto" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title inline">
                        CICLO {{ originalRow.ciclo }}. Agregar Producto
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <b>Proveedor</b>
                            </div>
                            <div class="col-md-8">
                                <select class="form-control" ng-model="productoAdd.id_proveedor" ng-class="{ error: !productoAdd.id_proveedor > 0 }">
                                    <option value="">Seleccione</option>
                                    <option value="{{ p.id }}" ng-repeat="p in proveedores">{{ p.nombre }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <b>Producto</b>
                            </div>
                            <div class="col-md-8">
                                <select class="form-control" ng-model="productoAdd.id_producto" ng-change="selectProductoAdd()" ng-class="{ error: !productoAdd.id_producto > 0 }">
                                    <option value="">Seleccione</option>
                                    <option value="{{ p.id }}" ng-repeat="p in productos | filter : { id_proveedor : productoAdd.id_proveedor } : true">{{ p.nombre_comercial }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <b>Dosis</b>
                            </div>
                            <div class="col-md-8">
                                <select class="form-control" ng-model="productoAdd.dosis" id="select-add-dosis" ng-class="{ error: !productoAdd.dosis > 0 }">
                                    <option value="">Seleccione</option>
                                    <option value="{{ dosis }}" ng-selected="dosis == productoAdd.dosis" ng-repeat="dosis in productoAdd.producto.dosis_array track by $index">{{ dosis }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <b>Cantidad</b>
                            </div>
                            <div class="col-md-8 text-right">
                                {{ productoAdd.cantidad = multiplicar((originalRow.por_aprobar.hectareas_fumigacion || originalRow.hectareas_fumigacion), productoAdd.dosis) | number }}
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <b>Precio</b>
                            </div>
                            <div class="col-md-8 text-right">
                                <span class="pull-left">$</span>
                                {{ productoAdd.precio }}
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <b>Total</b>
                            </div>
                            <div class="col-md-8 text-right">
                                <span class="pull-left">$</span>
                                {{ productoAdd.precio * productoAdd.cantidad | number : 2 }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" ng-disabled="saving" ng-click="addProducto()">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>