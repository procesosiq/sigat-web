<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    $cdn = "http://cdn.procesos-iq.com/";

    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<style>
td,th {
    text-align : center;
}
td {
    white-space: nowrap;
}
</style>
<link href="<?=$cdn?>/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<div class="page-content" ng-app="app" ng-controller="analisis">
    <div class="page-head">
        <div class="page-title">
            <h1>MATRIZ EXCEL</h1>
        </div>
        <div class="page-toolbar">
            <div style="float:right">
                <div class="col-md-offset-1 col-md-3 hide">
                    <label>Año: </label>
                    <select id="anio" class="input-sm" ng-model="filters.year" ng-change="init()">
                        <option value="">Seleccione</option>
                        <option value="{{ a }}" ng-repeat="a in anios" ng-selected="a == filters.year">{{ a }}</option>
                    </select>
                </div>
                <div class="col-md-offset-1 col-md-3">
                    <label>Finca: </label>
                    <select class="input-sm" ng-model="filters.finca" ng-change="init()">
                        <option value="MATEO">MATEO</option>
                        <option value="MARIA PAULA">MARIA PAULA</option>
                        <option value="SAN ALBERTO 1">SAN ALBERTO 1</option>
                        <option value="SAN ALBERTO 2">SAN ALBERTO 2</option>
                        <option value="EVA MARIA">EVA MARIA</option>
                        <option value="DELIA MARGARITA">DELIA MARGARITA</option>
                        <option value="ISABELA">ISABELA</option>
                        <option value="ENVIDIA">ENVIDIA</option>
                        <option value="SAN JUAN">SAN JUAN</option>
                        <option value="MARTIN IGNACIO">MARTIN IGNACIO</option>
                        <option value="MARIA GRACIA">MARIA GRACIA</option>
                        <option value="DELIA GRACE">DELIA GRACE</option>
                        <option value="LA NIÑA">LA NIÑA</option>
                        <option value="SAN ANTONIO">SAN ANTONIO</option>
                        <option value="THOMAS">THOMAS</option>
                        <option value="LUCIANA">LUCIANA</option>
                        <option value="MATHIAS 2">MATHIAS 2</option>
                        <option value="MATHIAS 4">MATHIAS 4</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="resumenCiclos">Inicio</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Listado</span>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        SIGATOKA
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2">CICLO</th>
                                    <th rowspan="2">FREC.</th>
                                    <th rowspan="2">PROGRAMA</th>
                                    <th rowspan="2">SEMANA</th>
                                    <th colspan="2">FECHA</th>
                                    <th rowspan="2">HAS</th>
                                    <th colspan="2">ATRASO</th>
                                    <th colspan="9" style="border : 2px solid black;">PRODUCTOS</th>
                                    <th colspan="9" style="border : 2px solid black;">DOSIS</th>
                                    <th colspan="9" style="border : 2px solid black;">CANTIDAD</th>
                                    <th colspan="9" style="border : 2px solid black;">PRECIOS PRODUCTO</th>
                                    <th colspan="9" style="border : 2px solid black;">COSTO TOTAL</th>
                                    <th rowspan="2">COSTO<br>OPERACION</th>
                                    <th rowspan="2">TOTAL COSTO<br>OPERACION</th>
                                    <th rowspan="2">COSTO TOTAL</th>
                                    <th rowspan="2">COSTO / HAS<br>FUMIGACION</th>
                                </tr>
                                <tr>
                                    <th style="min-width: 80px !important;">PROG.</th>
                                    <th style="min-width: 80px !important;">REAL</th>

                                    <th>DIAS</th>
                                    <th>MOTIVO</th>

                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in sigatoka">
                                    <td>{{ row.ciclo }}</td>
                                    <td></td>
                                    <td>{{ row.programa }}</td>
                                    <td>{{ row.semana }}</td>
                                    <td>{{ row.fecha_prog }}</td>
                                    <td>{{ row.fecha_real }}</td>
                                    <td>{{ row.hectareas_fumigacion }}</td>
                                    <td>{{ row.atraso }}</td>
                                    <td>{{ row.motivo }}</td>

                                    <!-- productos -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        {{ row.productos[i-1].nombre_comercial }}
                                    </td>
                                    <!-- dosis -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        {{ row.productos[i-1].dosis | num }}
                                    </td>
                                    <!-- cantidad -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        {{ row.productos[i-1].cantidad | number }}
                                    </td>
                                    <!-- precios -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        {{ row.productos[i-1].precio | number }}
                                    </td>
                                    <!-- totales -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        <span title="{{ row.productos[i-1].nombre_comercial }}">{{ row.productos[i-1].total | number }}</span>
                                    </td>

                                    <td>{{ row.ha_oper | number : 2 }}</td>
                                    <td>{{ row.oper | number : 2 }}</td>
                                    <td>{{ row.costo_total | number : 2 }}</td>
                                    <td>{{ row.costo_ha = (row.costo_total / row.hectareas_fumigacion) | number : 2 }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>

                                    <th></th>
                                    <th>{{ sigatoka | sumOfValue : 'oper' | number : 2 }}</th>
                                    <th>{{ sigatoka | sumOfValue : 'costo_total' | number : 2 }}</th>
                                    <th>{{ sigatoka | sumOfValue : 'costo_ha' | number : 2 }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        FOLIARES
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2">CICLO</th>
                                    <th rowspan="2">FREC.</th>
                                    <th rowspan="2">PROGRAMA</th>
                                    <th rowspan="2">SEMANA</th>
                                    <th colspan="2">FECHA</th>
                                    <th rowspan="2">HAS</th>
                                    <th colspan="2">ATRASO</th>
                                    <th colspan="9" style="border : 2px solid black;">PRODUCTOS</th>
                                    <th colspan="9" style="border : 2px solid black;">DOSIS</th>
                                    <th colspan="9" style="border : 2px solid black;">CANTIDAD</th>
                                    <th colspan="9" style="border : 2px solid black;">PRECIOS PRODUCTO</th>
                                    <th colspan="9" style="border : 2px solid black;">COSTO TOTAL</th>
                                    <th rowspan="2">COSTO<br>OPERACION</th>
                                    <th rowspan="2">TOTAL COSTO<br>OPERACION</th>
                                    <th rowspan="2">COSTO TOTAL</th>
                                    <th rowspan="2">COSTO / HAS<br>FUMIGACION</th>
                                </tr>
                                <tr>
                                    <th style="min-width: 80px !important;">PROG.</th>
                                    <th style="min-width: 80px !important;">REAL</th>

                                    <th>DIAS</th>
                                    <th>MOTIVO</th>

                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in foliar">
                                    <td>{{ $index+1 }}</td>
                                    <td></td>
                                    <td>{{ row.programa }}</td>
                                    <td>{{ row.semana }}</td>
                                    <td>{{ row.fecha_prog_c }}</td>
                                    <td>{{ row.fecha_real_c }}</td>
                                    <td>{{ row.hectareas_fumigacion }}</td>
                                    <td>{{ row.atraso }}</td>
                                    <td>{{ row.motivo }}</td>

                                    <!-- productos -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        {{ row.productos[i-1].nombre_comercial }}
                                    </td>
                                    <!-- dosis -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        {{ row.productos[i-1].dosis | num }}
                                    </td>
                                    <!-- cantidad -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        {{ row.productos[i-1].cantidad | number }}
                                    </td>
                                    <!-- precios -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        {{ row.productos[i-1].precio | number }}
                                    </td>
                                    <!-- totales -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        <span title="{{ row.productos[i-1].nombre_comercial }}">{{ row.productos[i-1].total | number }}</span>
                                    </td>

                                    <td>{{ row.ha_oper | number : 2 }}</td>
                                    <td>{{ row.oper | number : 2 }}</td>
                                    <td>{{ row.costo_total | number : 2 }}</td>
                                    <td>{{ row.costo_ha = (row.costo_total / row.hectareas_fumigacion) | number : 2 }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>

                                    <th></th>
                                    <th>{{ foliar | sumOfValue : 'oper' | number : 2 }}</th>
                                    <th>{{ foliar | sumOfValue : 'costo_total' | number : 2 }}</th>
                                    <th>{{ foliar | sumOfValue : 'costo_ha' | number : 2 }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        PARCIAL
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2">CICLO</th>
                                    <th rowspan="2">FREC.</th>
                                    <th rowspan="2">PROGRAMA</th>
                                    <th rowspan="2">SEMANA</th>
                                    <th colspan="2">FECHA</th>
                                    <th rowspan="2">HAS</th>
                                    <th colspan="2">ATRASO</th>
                                    <th colspan="9" style="border : 2px solid black;">PRODUCTOS</th>
                                    <th colspan="9" style="border : 2px solid black;">DOSIS</th>
                                    <th colspan="9" style="border : 2px solid black;">CANTIDAD</th>
                                    <th colspan="9" style="border : 2px solid black;">PRECIOS PRODUCTO</th>
                                    <th colspan="9" style="border : 2px solid black;">COSTO TOTAL</th>
                                    <th rowspan="2">COSTO<br>OPERACION</th>
                                    <th rowspan="2">TOTAL COSTO<br>OPERACION</th>
                                    <th rowspan="2">COSTO TOTAL</th>
                                    <th rowspan="2">COSTO / HAS<br>FUMIGACION</th>
                                </tr>
                                <tr>
                                    <th style="min-width: 80px !important;">PROG.</th>
                                    <th style="min-width: 80px !important;">REAL</th>

                                    <th>DIAS</th>
                                    <th>MOTIVO</th>

                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="border : 2px solid black;">
                                        P{{i}}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in parcial">
                                    <td>{{ $index+1 }}</td>
                                    <td></td>
                                    <td>{{ row.programa }}</td>
                                    <td>{{ row.semana }}</td>
                                    <td>{{ row.fecha_prog }}</td>
                                    <td>{{ row.fecha_real }}</td>
                                    <td>{{ row.hectareas_fumigacion }}</td>
                                    <td>{{ row.atraso }}</td>
                                    <td>{{ row.motivo }}</td>

                                    <!-- productos -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        {{ row.productos[i-1].nombre_comercial }}
                                    </td>
                                    <!-- dosis -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        {{ row.productos[i-1].dosis | num }}
                                    </td>
                                    <!-- cantidad -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        {{ row.productos[i-1].cantidad | number }}
                                    </td>
                                    <!-- precios -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        {{ row.productos[i-1].precio | number }}
                                    </td>
                                    <!-- totales -->
                                    <td ng-repeat="i in [1,2,3,4,5,6,7,8,9]" style="{{ (i == 1 || i == 9) ? ('border-'+ ((i == 1) ? 'left' : 'right') +' : 2px solid black;') : '' }}">
                                        <span title="{{ row.productos[i-1].nombre_comercial }}">{{ row.productos[i-1].total | number }}</span>
                                    </td>

                                    <td>{{ row.ha_oper | number : 2 }}</td>
                                    <td>{{ row.oper | number : 2 }}</td>
                                    <td>{{ row.costo_total | number : 2 }}</td>
                                    <td>{{ row.costo_ha = (row.costo_total / row.hectareas_fumigacion) | number : 2 }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>
                                    <th ng-repeat="i in [1,2,3,4,5,6,7,8,9]"></th>

                                    <th></th>
                                    <th>{{ parcial | sumOfValue : 'oper' | number : 2 }}</th>
                                    <th>{{ parcial | sumOfValue : 'costo_total' | number : 2 }}</th>
                                    <th>{{ parcial | sumOfValue : 'costo_ha' | number : 2 }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>