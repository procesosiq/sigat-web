<?php
    $cdn = "http://cdn.procesos-iq.com/";
?>
<style>
    .custom-header{
        background: #337ab7;
        color: white;
        padding: 2px;
        font-weight: bold;
        text-align: center;
    }
</style>
<link href="<?=$cdn?>/global/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper" ng-app="app">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Módulo de Ciclos</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <!--<li>
                <a href="productosList">Listado de Productos</a>
                <i class="fa fa-circle"></i>
            </li>-->
            <li>
                <span class="active">Registro de Ciclos</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row" ng-controller="cycles">
            <div class="col-md-12" ng-init="index()">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-social-dribbble font-purple-soft"></i>
                            <span class="caption-subject font-purple-soft bold uppercase">CICLOS</span>
                        </div>
                        <div class="actions">
                           
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div ng-include="'./views/cycles/form.html?i=<?=rand(10,100)?>'"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->