<?php

class Datos_Ciclos{
	
	private $bd;
	private $session;
	
	public function __construct() {
        $this->bd = new M_Conexion();
        $this->session = Session::getInstance();
    }

	private function Indicadores(){
		$response = new stdClass;

        // 11/05/2017 - TAG: CICLOS PROMEDIO
        $sql = "SELECT AVG(fila) AS ciclos_promedio FROM (
                SELECT finca ,  COUNT(1) AS fila FROM ciclos_aplicacion_historico
                GROUP BY finca) AS ciclos_promedio";
        $response->ciclos_promedio = $this->bd->queryRow($sql);
        // 11/05/2017 - TAG: CICLOS PROMEDIO
        // 11/05/2017 - TAG: MAYOR CICLO
        $sql = "SELECT finca  , fecha_real,  COUNT(1) AS max_finca FROM ciclos_aplicacion_historico
                GROUP BY finca
                ORDER BY COUNT(1) ASC, fecha_real DESC
                LIMIT 1";
        $response->ciclos_mayor = $this->bd->queryRow($sql);
        // 11/05/2017 - TAG: MAYOR CICLO
        // 11/05/2017 - TAG: MENOR CICLO
        $sql = "SELECT finca  , fecha_real,  COUNT(1) AS min_finca FROM ciclos_aplicacion_historico
                GROUP BY finca
                ORDER BY COUNT(1) DESC , fecha_real ASC
                LIMIT 1";
        $response->ciclos_menor = $this->bd->queryRow($sql);
        // 11/05/2017 - TAG: MENOR CICLO
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO
        $sql = "SELECT AVG(costo_ha) ha_ciclos FROM ciclos_aplicacion_historico";
        $response->ha_ciclos = $this->bd->queryRow($sql);
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO
                // 11/05/2017 - TAG: HA CICLOS PROMEDIO MAX
        $sql = "SELECT finca , costo_ha FROM ciclos_aplicacion_historico ORDER BY costo_ha DESC LIMIT 1";
        $response->ha_ciclos_max = $this->bd->queryRow($sql);
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO MAX
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO MIN
        $sql = "SELECT finca , costo_ha FROM ciclos_aplicacion_historico ORDER BY costo_ha ASC LIMIT 1";
        $response->ha_ciclos_min = $this->bd->queryRow($sql);
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO MIN
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO
        $sql = "SELECT AVG(costo_ha)  AS costo_ha
                FROM (
                SELECT finca , SUM(costo_ha) AS costo_ha FROM ciclos_aplicacion_historico
                GROUP BY finca) AS costo_ha_anio";
        $response->ha_ciclos_anio = $this->bd->queryRow($sql);
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MAX
        $sql = "SELECT finca , costo_ha AS ha_ciclos_max
                FROM (
                SELECT finca , SUM(costo_ha) AS costo_ha FROM ciclos_aplicacion_historico
                GROUP BY finca) AS costo_ha_anio
                ORDER BY costo_ha DESC
                LIMIT 1";
        $response->ha_ciclos_anio_max = $this->bd->queryRow($sql);
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MAX
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MIN
        $sql = "SELECT finca , costo_ha AS ha_ciclos_min
                FROM (
                SELECT finca , SUM(costo_ha) AS costo_ha FROM ciclos_aplicacion_historico
                GROUP BY finca) AS costo_ha_anio
                ORDER BY costo_ha ASC
                LIMIT 1";
        $response->ha_ciclos_anio_min = $this->bd->queryRow($sql);
        // 11/05/2017 - TAG: HA CICLOS PROMEDIO AÑO MIN

        return $response;
	}

	public function getIndicadores(){ 
		$response = new stdClass;
		$response = $this->Indicadores();
		return $response;
	}
}

?>