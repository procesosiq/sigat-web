<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<style>
    .listado{
        list-style-type: none;
        display: inline-block;
    }
    .listado > li{
        display: inline;
        margin-right: 10px;
    }
    .miniature{
        display: inline-block;
    }
    .center-th {
        text-align : center;
    }
    .label_chart {
        background-color: #fff;
        padding: 2px;
        margin-bottom: 8px;
        border-radius: 3px 3px 3px 3px;
        border: 1px solid #E6E6E6;
        display: inline-block;
        margin: 0 auto;
    }
    .legendLabel{
        padding: 3px !important;
    }
    .textTittle {
        font-size: 11px !important;
    }
    table > th {
        text-align: center !important;
    }
    tbody > td {
        text-align: center !important;
    }
    td , th {
        text-align: center;
    }
    div {
        border-radius : unset !important;
    }
    .drag-handle {
        display: none !important;
    }
    div.react-grid-Main > div.react-grid-Grid > div > div > div > div > div > div > div, div.react-grid-Main > div.react-grid-Grid > div > div > div > div > div > div > div > div, div.react-grid-Main > div.react-grid-Grid > div > div > div > div > div > div > div > div > span, div.react-grid-Main > div.react-grid-Grid > div > div > div > div > div > div > div > div > span > div {
        border : unset !important;
        height : 100%;
    }
</style>
<!-- BEGIN CONTENT BODY -->
<div ng-app="app" class="page-content" ng-controller="controller">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Asignar Hectareas</h1>
        </div>
        <!-- END PAGE TITLE -->
        <div class="page-toolbar">
            
        </div>
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="index.php">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Asignar Hectareas</span>
        </li>
    </ul>
    
    <div class="portlet box green-haze">
        <div class="portlet-title">
            <div class="caption">
            </div>
            <div class="actions">
                <select class="form-control" ng-model="filters.anio" ng-change="init()">
                    <option value="{{y}}" ng-repeat="y in years" ng-selected="y == filters.anio">{{y}}</option>
                </select>
            </div>
        </div>
        <div class="portlet-body">
            <div class="panel-group accordion" id="accordion3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" aria-expanded="false">
                                Fumigación
                            </a>
                        </h4>
                    </div>
                    <div id="collapse_3_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div id="table-fumigacion"></div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" aria-expanded="false">
                                Producción
                            </a>
                        </h4>
                    </div>
                    <div id="collapse_3_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div id="table-produccion"></div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false">
                                Neta
                            </a>
                        </h4>
                    </div>
                    <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div id="table-neta"></div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4" aria-expanded="false">
                                Banano
                            </a>
                        </h4>
                    </div>
                    <div id="collapse_3_4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div id="table-banano"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->   

<script src="componentes/FilterableSortableTable.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>