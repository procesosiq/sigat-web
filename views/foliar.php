<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
                <!-- BEGIN CONTENT BODY -->
                <div ng-app="app" class="page-content" ng-controller="foliar" ng-cloak>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head" ng-init="init()">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>EMISION FOLIAR
                                <small>Estadisticas de Emision Foliar</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <?php include './views/clientes_tags.php';?>
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Emision Foliar</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <!-- BEGIN DASHBOARD STATS 1-->
                        <?php include("clima_tags.php");?>
                    <!-- BEGIN INTERACTIVE CHART PORTLET-->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-dark sbold uppercase">
                                            <h3>
                                               <i class="icon-settings font-dark"></i> Emision Foliar
                                            </h3>
                                            <!-- <small>Hojas totales</small> -->
                                        </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="foliar" class="chart"> </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Datos 
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                                    </div>
                                </div>
                                <div class="portlet-body" style="display: none;">
                                   <div class="caption">
                                        <span class="caption-subject font-dark sbold uppercase">
                                            <h3>
                                               <i class="icon-settings font-dark"></i> Emision Foliar
                                            </h3>
                                            <!-- <small>Hojas totales</small> -->
                                        </span>
                                    </div>
                                    <div class="table-scrollable"> 
                                        <table class="table table-striped table-bordered table-hover textTittle">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th class="textTittle" colspan="{{semanas.length}}"> Semanas </th>
                                                </tr>
                                                <tr>
                                                    <th>Foco</th>
                                                    <th ng-repeat="(key, value) in semanas">{{value}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="(key,value) in charts.foliar">
                                                    <td class="textTittle">{{key}}</td>
                                                    <td class="textTittle" ng-repeat="(llave, valor) in semanas">
                                                        {{value[valor] | number : 1}}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END INTERACTIVE CHART PORTLET-->
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->