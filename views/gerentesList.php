<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    $cdn = "http://cdn.procesos-iq.com/";
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<style>
    .center-th {
        text-align: center !important;
    }

    .center-th > tbody > tr > td {
        text-align: center !important;  
        background: red; 
    }

</style>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=$cdn?>/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Listado de Gerentes
                                <small>Gerentes actualmente registrados</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="resumenCiclos">Inicio</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Listado</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                   <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Listado de Gerentes</span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                <a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nuevo Gerente</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="2%"># </th>
                                                    <th width="5%"> ID&nbsp;# </th>
                                                    <th width="25%"> Nombre </th>
                                                    <th width="25%"> Correo </th>
                                                    <th width="10%" class="center-th"> Fincas </th>
                                                    <th width="5%"> Status </th>
                                                    <th width="15%"> Acciones </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_id"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_name"></td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_user"> </td>
                                                    <td class="center-th"><input type="text" class="form-control form-filter input-sm" name="search_pass"> </td>
                                                    <td>
                                                        <select name="order_status" class="form-control form-filter input-sm">
                                                            <option value="">Select...</option>
                                                            <option value="1">Activo</option>
                                                            <option value="0">Inactivo</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-search"></i> </button>
                                                            <button class="btn btn-sm red btn-outline filter-cancel"><i class="fa fa-times"></i> </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->