<?php
    $session = Session::getInstance();
?>
    <script src="http://hayageek.github.io/jQuery-Upload-File/4.0.10/jquery.uploadfile.min.js"></script>
    <link href="http://hayageek.github.io/jQuery-Upload-File/4.0.10/uploadfile.css" rel="stylesheet">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper" ng-app="app" ng-controller="importarInformacion">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Módulo de Importación de Información</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="listProduc">Importador</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Nueva Importación</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-pane" id="tab_1">
                                <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>HACER UNA NUEVA IMPORTACION</div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="horizontal-form" id="form" method="post" action="../excel/guardar_archivos.php" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <h3 class="form-section">SELECCIONAR ARCHIVO EXCEL</h3>
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <select ng-model="selected" name="selected">
                                                                <!--<option value="1">Fincas</option>
                                                                <option value="2">Foliar</option>-->
                                                                <option value="3">Climas</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input id="usuario" name="usuario" type="text" value="<?= $session->id ?>" class="hide"/>
                                                        <input id="fileToUpload" name="fileToUpload" type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions right">
                                                <div id="editor"></div>
                                                <button id="cancel" type="button" class="btn default cancel">Cancelar</button>
                                                <input id="send" type="submit" value="Subir" class="btn blue btnadd">
                                                    <!--<i class="fa fa-check"></i> Subir
                                                </input>-->
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->