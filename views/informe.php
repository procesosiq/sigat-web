<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    $session = Session::getInstance();
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>

<style>
.modal-dialog {
    width : 820px;
}
.charts {
    height : 400px;
    width : 656px;
}
@media all {
	.page-break	{ display: none; }
}

@media print {
	.page-break	{ display: block; page-break-before: always; }
}
.table td, .table th {
    font-size: 9px !important;
}
</style>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?=$cdn?>global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

<?php if(isset($_GET['download'])): ?>
    <a id="download" style="display: none;" href="http://sigat.procesos-iq.com/informes/<?= $_GET['download'] ?>" download></a>
<?php endif; ?>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN CONTENT BODY -->
<div class="page-content" ng-app="app" ng-controller="informe" ng-cloak>
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head" ng-init="init()">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>ENVIAR INFORME
                <!-- <small>Mapas</small> -->
            </h1>
        </div>
        <!-- END PAGE TITLE -->
        <?php include './views/clientes_tags.php';?>
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="index.php">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Informe</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <!-- BEGIN DASHBOARD STATS 1-->
    <?php #include("clima_tags.php");?>
    <!-- BEGIN INTERACTIVE CHART PORTLET-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXTRAS PORTLET-->
            <div class="portlet light form-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Resumen</span>
                    </div>
                    <div class="actions">

                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="row">
                        <form class="form-horizontal form-bordered" method="GET" action="informe_to_pdf">
                            <div class="form-body">
                                <div class="form-group last">
                                    <label class="control-label col-md-2">Informes</label>
                                    <div class="col-md-10">
                                            <div class="portlet box green-meadow">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i>Listados Anteriores </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body" style="display: none;"> 
                                                    <table class="table table-striped table-hover table-bordered" id="list_informe">
                                                        <thead>
                                                            <tr>
                                                                <th> # </th>
                                                                <th> ID </th>
                                                                <th> Productor </th>
                                                                <th> Hacienda </th>
                                                                <th> Semana </th>
                                                                <th> Estado </th>
                                                                <th> UID </th>
                                                                <th> Acciones </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="id_usuario" name="id_usuario" value="{{id}}"/>
                                    <label class="control-label col-md-2">Cuerpo del correo</label>
                                    <div class="col-md-10">
                                        <textarea name="summernote" id="summernote_2"> </textarea>
                                    </div>
                                    <label class="control-label col-md-2">Resumen del pdf</label>
                                    <div class="col-md-10">
                                        <textarea name="summernote" id="summernote_1"> </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-2 col-md-10">
                                        Semana de Inspección
                                        <select name="semana" id="semana" class="input-sm" ng-model="semana">
                                            <option ng-repeat="sem in semanas" value="{{sem}}" ng-selected="sem == semana">{{sem}}</option>
                                        </select>
                                        <button ng-click="enviar()" type="button" class="btn green">
                                            <i class="fa fa-check"></i> Enviar
                                         </button>
                                        <button type="button" class="btn default">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div id="nsecado" class="modal fade" tabindex="-1" data-width="810">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header blue">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">PREVISUALIZACIÓN</h4>
                                </div>
                                <div class="modal-body form">
                                    <div id="previsualizar">
                                    
                                    </div>
                                </div>
                                <div class="modal-footer">  
                                    <button type="button" class="btn green-jungle" ng-click="imprimir()">Imprimir</button>
                                    <button type="button" class="btn btn-primary" ng-click="send()">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row hide" id="informePrimaDonna" style="width : 656px;">
                    <div id="grafica-m3-h3" class="charts"></div>
                    <div id="table-grafica-m3-h3" class="table-responisve">
                        <table class="table table-bordered">
                            <tr>
                                <th></th>
                                <th ng-repeat="finca in data['grafica-m3-h3'].legend">{{ finca }}</th>
                            </tr>
                            <tbody>
                                <tr ng-repeat="(key, values) in data['grafica-m3-h3'].data">
                                    <th>
                                        <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                    </th>
                                    <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div id="grafica-m3-h4" class="charts"></div>
                    <div id="table-grafica-m3-h4" class="table-responisve">
                        <table class="table table-bordered">
                            <tr>
                                <th></th>
                                <th ng-repeat="finca in data['grafica-m3-h4'].legend">{{ finca }}</th>
                            </tr>
                            <tbody>
                                <tr ng-repeat="(key, values) in data['grafica-m3-h4'].data">
                                    <th>
                                        <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                    </th>
                                    <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div id="grafica-m3-h5" class="charts"></div>
                    <div id="table-grafica-m3-h5" class="table-responisve">
                        <table class="table table-bordered">
                            <tr>
                                <th></th>
                                <th ng-repeat="finca in data['grafica-m3-h5'].legend">{{ finca }}</th>
                            </tr>
                            <tbody>
                                <tr ng-repeat="(key, values) in data['grafica-m3-h5'].data">
                                    <th>
                                        <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                    </th>
                                    <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div id="grafica-m3-hvlq" class="charts"></div>
                    <div id="table-grafica-m3-hvlq" class="table-responisve">
                        <table class="table table-bordered">
                            <tr>
                                <th></th>
                                <th ng-repeat="finca in data['grafica-m3-hvlq'].legend">{{ finca }}</th>
                            </tr>
                            <tbody>
                                <tr ng-repeat="(key, values) in data['grafica-m3-hvlq'].data">
                                    <th>
                                        <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                    </th>
                                    <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div id="grafica-m3-hvle" class="charts"></div>
                    <div id="table-grafica-m3-hvle" class="table-responisve">
                        <table class="table table-bordered">
                            <tr>
                                <th></th>
                                <th ng-repeat="finca in data['grafica-m3-hvle'].legend">{{ finca }}</th>
                            </tr>
                            <tbody>
                                <tr ng-repeat="(key, values) in data['grafica-m3-hvle'].data">
                                    <th>
                                        <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                    </th>
                                    <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div id="grafica-s0-ht" class="charts"></div>
                    <div id="table-grafica-s0-ht" class="table-responisve">
                        <table class="table table-bordered">
                            <tr>
                                <th></th>
                                <th ng-repeat="finca in data['grafica-s0-ht'].legend">{{ finca }}</th>
                            </tr>
                            <tbody>
                                <tr ng-repeat="(key, values) in data['grafica-s0-ht'].data">
                                    <th>
                                        <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                    </th>
                                    <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div id="grafica-s0-hvle" class="charts"></div>
                    <div id="table-grafica-s0-hvle" class="table-responisve">
                        <table class="table table-bordered">
                            <tr>
                                <th></th>
                                <th ng-repeat="finca in data['grafica-s0-hvle'].legend">{{ finca }}</th>
                            </tr>
                            <tbody>
                                <tr ng-repeat="(key, values) in data['grafica-s0-hvle'].data">
                                    <th>
                                        <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                    </th>
                                    <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div id="grafica-s0-hvlq" class="charts"></div>
                    <div id="table-grafica-s0-hvlq" class="table-responisve">
                        <table class="table table-bordered">
                            <tr>
                                <th></th>
                                <th ng-repeat="finca in data['grafica-s0-hvlq'].legend">{{ finca }}</th>
                            </tr>
                            <tbody>
                                <tr ng-repeat="(key, values) in data['grafica-s0-hvlq'].data">
                                    <th>
                                        <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                    </th>
                                    <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div id="grafica-s11-ht" class="charts"></div>
                    <div id="table-grafica-s11-ht" class="table-responisve">
                        <table class="table table-bordered">
                            <tr>
                                <th></th>
                                <th ng-repeat="finca in data['grafica-s11-ht'].legend">{{ finca }}</th>
                            </tr>
                            <tbody>
                                <tr ng-repeat="(key, values) in data['grafica-s11-ht'].data">
                                    <th>
                                        <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                    </th>
                                    <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div id="grafica-s11-hvlq" class="charts"></div>
                    <div id="table-grafica-s11-hvlq" class="table-responisve">
                        <table class="table table-bordered">
                            <tr>
                                <th></th>
                                <th ng-repeat="finca in data['grafica-s11-hvlq'].legend">{{ finca }}</th>
                            </tr>
                            <tbody>
                                <tr ng-repeat="(key, values) in data['grafica-s11-hvlq'].data">
                                    <th>
                                        <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                    </th>
                                    <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div id="informe-html" style="width : 656px;" class="hide">
                    REPORTE PRIMA DONNA SEMANA {{semana}}/<?= Session::getInstance()->year ?>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->