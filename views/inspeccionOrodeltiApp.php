<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
    <style>
        .listado{
            list-style-type: none;
            display: inline-block;
        }
        .listado > li{
            display: inline;
            margin-right: 10px;
        }
        .miniature{
            display: inline-block;
        }
        .center-th {
            text-align : center;
        }
        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .legendLabel{
            padding: 3px !important;
        }
        .textTittle {
            font-size: 11px !important;
        }
        table > th {
            text-align: center !important;
        }
        tbody > td {
            text-align: center !important;
        }
        td , th {
            text-align: center;
        }
        .chart {
            height: 500px;
        }
    </style>
    <!-- BEGIN CONTENT BODY -->
    <div ng-app="app" class="page-content" ng-controller="inspeccion">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Inspección</h1>
            </div>
            <!-- END PAGE TITLE -->
            <div class="page-toolbar">
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                        {{filters.gerenteName}}
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li ng-click="setFilterGerente({id : '' , label : 'ORODELTI'})">
                            <a href="#">
                                ORODELTI
                            </a>
                        </li>
                        <li ng-repeat="(key , value) in gerentes" ng-click="setFilterGerente(value)">
                            <a href="#">
                                {{value.label}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                        {{filters.year}}
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <!--<li ng-click="setFilterYear({id : '' , label : ''})">
                            <a href="#">
                                TODOS
                            </a>
                        </li>-->
                        <li ng-repeat="(key , value) in years" ng-click="setFilterYear(value)">
                            <a href="#">
                                {{value.label}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.php">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Inspección</span>
            </li>
        </ul>
        
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                        </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body">
                <div class="panel-group accordion" id="accordion3">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="tab_3m" class="accordion-toggle accordion-toggle-styled collapsed" ng-click="getGraficas3M()" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" aria-expanded="false"> Plantas 3m </a>
                            </h4>
                        </div>
                        <div id="collapse_3_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">

                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_libre_estrias" class="label_chart">HVLE</div>
                                            <div id="libre_estrias" style="width: 100%;" class="chart"> </div>
                                        </div>
                                        <div class="portlet-footer">
                                            <div class="miniature">
                                                <ul id="check_libre_estrias" class="listado">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_libre_estrias_afa" class="label_chart">HVLE AFA</div>
                                            <div id="libre_estrias_afa" style="width: 100%;" class="chart"> </div>
                                        </div>
                                        <div class="portlet-footer">
                                            <div class="miniature">
                                                <ul id="check_libre_estrias_Afa" class="listado">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <!-- BEGIN PORTLET-->
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hoja_vieja_menor_5" class="label_chart">HT</div>
                                            <div id="hoja_vieja_menor_5" class="chart"> </div>
                                        </div>
                                        <div class="portlet-footer">
                                            <div class="miniature">
                                                <ul id="check_hoja_vieja_menor_5" class="listado">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PORTLET-->
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <!-- BEGIN PORTLET-->
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hoja_vieja_menor_5" class="label_chart">HVLQ</div>
                                            <div id="hvlq" class="chart"> </div>
                                        </div>
                                    </div>
                                    <!-- END PORTLET-->
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <!-- BEGIN PORTLET-->
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div class="label_chart">HVLQ AFA</div>
                                            <div id="hvlq_afa" class="chart"> </div>
                                        </div>
                                    </div>
                                    <!-- END PORTLET-->
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <!-- BEGIN PORTLET-->
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hoja_3" class="label_chart">EE H2</div>
                                            <div id="hoja_2" class="chart"> </div>
                                        </div>
                                        <div class="portlet-footer">
                                            <div class="miniature">
                                                <ul id="check_hoja_2" class="listado">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PORTLET-->
                                </div>
                                                                    
                                <div class="col-md-12 col-sm-6">
                                    <!-- BEGIN PORTLET-->
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hoja_3" class="label_chart">EE H3</div>
                                            <div id="hoja_3" class="chart"> </div>
                                        </div>
                                        <div class="portlet-footer">
                                            <div class="miniature">
                                                <ul id="check_hoja_3" class="listado">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PORTLET-->
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <!-- BEGIN PORTLET-->
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hoja_4" class="label_chart">EE H4</div>
                                            <div id="hoja_4" class="chart"> </div>
                                        </div>
                                        <div class="portlet-footer">
                                            <div class="miniature">
                                                <ul id="check_hoja_4" class="listado">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PORTLET-->
                                </div>
                                
                                <div class="col-md-12 col-sm-6">
                                    <!-- BEGIN PORTLET-->
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hoja_5" class="label_chart">EE H5</div>
                                            <div id="hoja_5" class="chart"> </div>
                                        </div>
                                        <div class="portlet-footer">
                                            <div class="miniature">
                                                <ul id="check_hoja_5" class="listado">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PORTLET-->
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered all-centered">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th colspan="5">3 METROS</th>
                                                </tr>
                                                <tr>
                                                    <th>SEM</th>
                                                    <th>FINCA</th>
                                                    <th>H3</th>
                                                    <th>H4</th>
                                                    <th>H5</th>
                                                    <th>HT</th>
                                                    <th>H+VLE</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="row in data_tables">
                                                    <td>{{ row.semana }}</td>
                                                    <td>{{ row.finca }}</td>
                                                    <td>{{ row.h3_3m }}</td>
                                                    <td>{{ row.h4_3m }}</td>
                                                    <td>{{ row.h5_3m }}</td>
                                                    <td>{{ row.ht_3m }}</td>
                                                    <td>{{ row.hvl_3m }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="tab_0s" class="accordion-toggle accordion-toggle-styled collapsed" ng-click="getGraficasS0()" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" aria-expanded="false"> Plantas 0 Sem </a>
                            </h4>
                        </div>
                        <div id="collapse_3_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                
                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hojas_total" class="label_chart">HT</div>
                                            <div id="hojas_total" class="chart"> </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hojas_total" class="label_chart">HT AFA</div>
                                            <div id="0s_ht_afa" class="chart"> </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_libre_estrias_0S" class="label_chart">HVLE</div>
                                            <div id="libre_estrias_0S" class="chart"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_libre_estrias_0S" class="label_chart">H+VLQ</div>
                                            <div id="0s_hvlq" class="chart"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_libre_estrias_0S" class="label_chart">H+VLQ AFA</div>
                                            <div id="0s_hvlq_afa" class="chart"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6 hide">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_libre_estrias_0S" class="label_chart">H+VLQ>5%</div>
                                            <div id="0s_hvlq_mayor_5" class="chart"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>SEM</th>
                                                    <th>FINCA</th>
                                                    <th>H+VLE</th>
                                                    <th>HT</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="row in data_tables">
                                                    <td>{{ row.semana }}</td>
                                                    <td>{{ row.finca }}</td>
                                                    <td>{{ row.hvl_0s }}</td>
                                                    <td>{{ row.ht_0s }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="tab_6s" class="accordion-toggle accordion-toggle-styled collapsed" ng-click="getGraficasS6()" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_5" aria-expanded="false"> Plantas 6 Sem </a>
                            </h4>
                        </div>
                        <div id="collapse_3_5" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                
                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hojas_total" class="label_chart">HT</div>
                                            <div id="6s_ht" class="chart"> </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hojas_total" class="label_chart">HT AFA</div>
                                            <div id="6s_ht_afa" class="chart"> </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_libre_estrias_0S" class="label_chart">HVLE</div>
                                            <div id="6s_hvle" class="chart"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_libre_estrias_0S" class="label_chart">H+VLQ</div>
                                            <div id="6s_hvlq" class="chart"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_libre_estrias_0S" class="label_chart">H+VLQ AFA</div>
                                            <div id="6s_hvlq_afa" class="chart"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6 hide">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_libre_estrias_0S" class="label_chart">H+VLQ>5%</div>
                                            <div id="6s_hvlq_mayor_5" class="chart"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>SEM</th>
                                                    <th>FINCA</th>
                                                    <th>H+VLE</th>
                                                    <th>HT</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="row in data_tables">
                                                    <td>{{ row.semana }}</td>
                                                    <td>{{ row.finca }}</td>
                                                    <td>{{ row.hvl_0s }}</td>
                                                    <td>{{ row.ht_0s }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="tab_11s" class="accordion-toggle accordion-toggle-styled collapsed" ng-click="getGraficasS11()" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false"> Plantas 11 Sem </a>
                            </h4>
                        </div>
                        <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                
                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hojas_total_11S" class="label_chart">HT</div>
                                            <div id="hojas_total_11S" class="chart"> </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hojas_total_11S" class="label_chart">HT AFA</div>
                                            <div id="11s_ht_afa" class="chart"> </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hojas_total_11S" class="label_chart">HVLE</div>
                                            <div id="11s_hvle" class="chart"> </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hojas_total_11S" class="label_chart">HVLQ</div>
                                            <div id="11s_hvlq" class="chart"> </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-6">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hojas_total_11S" class="label_chart">HVLQ AFA</div>
                                            <div id="11s_hvlq_afa" class="chart"> </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered all-centered">
                                            <thead>
                                                <tr>
                                                    <th>SEM</th>
                                                    <th>FINCA</th>
                                                    <th>HT</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="row in data_tables">
                                                    <td>{{ row.semana }}</td>
                                                    <td>{{ row.finca }}</td>
                                                    <td>{{ row.ht_11s }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" ng-click="getFoliar()" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4" aria-expanded="false"> Foliar </a>
                            </h4>
                        </div>
                        <div id="collapse_3_4" class="panel-collapse collapse" aria-expanded="true" style="">
                            <div class="panel-body">
                                <div class="col-md-12 col-sm-12">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-body">
                                            <div id="label_hojas_total_11S" class="label_chart"></div>
                                            <div id="foliar" class="chart"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->   