<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<style>
    .listado{
        list-style-type: none;
        display: inline-block;
    }
    .listado > li{
        display: inline;
        margin-right: 10px;
    }
    .miniature{
        display: inline-block;
    }
    .center-th {
        text-align : center;
    }
    .label_chart {
        background-color: #fff;
        padding: 2px;
        margin-bottom: 8px;
        border-radius: 3px 3px 3px 3px;
        border: 1px solid #E6E6E6;
        display: inline-block;
        margin: 0 auto;
    }
    .legendLabel{
        padding: 3px !important;
    }
    .textTittle {
        font-size: 11px !important;
    }
    table > th {
        text-align: center !important;
    }
    tbody > td {
        text-align: center !important;
    }
    td , th {
        text-align: center;
    }
</style>
<!-- BEGIN CONTENT BODY -->
<div ng-app="app" class="page-content" ng-controller="inspeccion">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Inspección App | Comparativo</h1>
        </div>
        <!-- END PAGE TITLE -->
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                    {{ filters.gerenteName }}
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li ng-click="setFilterGerente({id : '' , nombre : 'ORODELTI'})">
                        <a href="#">
                            ORODELTI
                        </a>
                    </li>
                    <li ng-repeat="g in gerentes" ng-click="setFilterGerente(g)">
                        <a href="#">
                            {{g.nombre}}
                        </a>
                    </li>
                </ul>
            </div>
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                    SEM {{ filters.week }}
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li ng-repeat="w in weeks" ng-click="setFilterWeek(y)">
                        <a href="#">
                            {{ w }}
                        </a>
                    </li>
                </ul>
            </div>
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                    {{filters.year}}
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li ng-repeat="y in years" ng-click="setFilterYear(y)">
                        <a href="#">
                            {{ y }}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="index.php">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Inspección</span>
        </li>
    </ul>
    
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                    </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="btn-group pull-right" style="margin-right : 15px">
                    <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                        Exportar <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="javascript:;" ng-click="exportExcel('datatable')">Excel</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="datatable">
                            <thead>
                                <tr>
                                    <th rowspan="2">FINCAS</th>
                                    <th rowspan="2">HOJA 2</th>
                                    <th rowspan="2">HOJA 3</th>
                                    <th rowspan="2">HOJA 4</th>
                                    <th rowspan="2">HOJA 5</th>
                                    <th colspan="2">3 METROS</th>
                                    <th colspan="2">0 SEM</th>
                                    <th>11 SEM</th>
                                    <th rowspan="2">EF</th>
                                </tr>
                                <tr>
                                    <th>HT</th>
                                    <th>H+VLE</th>
                                    <th>HT</th>
                                    <th>H+VLE</th>
                                    <th>HT</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in data">
                                    <td>{{ row.finca }}</td>
                                    <td class="text-right">{{ row.hoja_2 }} %</td>
                                    <td class="text-right">{{ row.hoja_3 }} %</td>
                                    <td class="text-right">{{ row.hoja_4 }} %</td>
                                    <td class="text-right">{{ row.hoja_5 }} %</td>
                                    <td>{{ row.m3_ht }}</td>
                                    <td>{{ row.m3_hvle }}</td>
                                    <td>{{ row.s0_ht }}</td>
                                    <td>{{ row.s0_hvle }}</td>
                                    <td>{{ row.s11_ht }}</td>
                                    <td>{{ row.ef }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-body">
                            <div id="label_libre_estrias" class="label_chart">ESTADO EVOLUTIVO</div>
                            <div id="estado_evolutivo" style="width: 100%;" class="chart"> </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-body">
                            <div id="label_libre_estrias" class="label_chart">PLANTAS 3 METROS</div>
                            <div id="plantas_3_metros" style="width: 100%;" class="chart"> </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-body">
                            <div id="label_libre_estrias" class="label_chart">PLANTAS 0 SEMANAS</div>
                            <div id="plantas_0_semanas" style="width: 100%;" class="chart"> </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-body">
                            <div id="label_libre_estrias" class="label_chart">PLANTAS 11 SEMANAS - HT</div>
                            <div id="plantas_11_semanas" style="width: 100%;" class="chart"> </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="portlet light portlet-fit bordered">
                        <div class="portlet-body">
                            <div id="label_libre_estrias" class="label_chart">EMISION FOLIAR</div>
                            <div id="emision_foliar" style="width: 100%;" class="chart"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->   