<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    $Clientes = $loader->getClientesDiponibles();
    if(Session::getInstance()->id_client != 25){
        ?>
        <script>
            window.location.replace('http://sigat.procesos-iq.com/inspeccion');
        </script>
        <?php
    }

    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
    <style>
        .listado{
            list-style-type: none;
            display: inline-block;
        }
        .listado > li{
            display: inline;
            margin-right: 10px;
        }
        .miniature{
            display: inline-block;
        }
        .center-th {
            text-align : center;
        }
        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: block;
            margin: 0 auto;
            text-align: center;
        }
        .legendLabel{
            padding: 3px !important;
        }
        .textTittle {
            font-size: 11px !important;
        }
        table > th {
            text-align: center !important;
        }
        tbody > td {
            text-align: center !important;
        }
        .chart {
            height : 400px !important;
        }
    </style>
                <!-- BEGIN CONTENT BODY -->
                <div ng-app="app" class="page-content" ng-controller="inspeccion">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Inspección</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <div class="page-toolbar">
                            <div id="" class="pull-right tooltips">
                                <select id="client" name="client" class="ticket-assign form-control input-medium" ng-model="id_cliente" ng-change="getFincas(true)">
                                    <?php foreach ($Clientes as $key => $value): ?>
                                        <option value="<?= $value['id'] ?>"><?= $value['nombre'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <select name="hacienda" id="hacienda" class="form-control" ng-model="hacienda" ng-change="save()">
                                    <option ng-repeat="fin in haciendas" value="{{fin.id}}" ng-selected="fin.id == hacienda">{{fin.nombre}}</option>
                                    <!--<option ng-if="id_cliente == id_cliente_original" value="COMPARACION">COMPARACIÓN</option>-->
                                </select>
                            </div>
                            <!-- END THEME PANEL -->
                        </div>
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Inspección</span>
                        </li>
                    </ul>
                    
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                 </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="panel-group accordion" id="accordion3">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a id="tab_3m" class="accordion-toggle accordion-toggle-styled collapsed" ng-click="getGraficas3M()" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" aria-expanded="false"> Plantas 3m </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">

                                            <div class="col-md-12 col-sm-12">
                                                <div class="col-md-5 pull-right">
                                                    <div class="col-md-5">
                                                        Max <input type="number" step="0.1" class="form-control" ng-model="graficas.m3.libre_strias_max">
                                                    </div>
                                                    <div class="col-md-5">
                                                        Min <input type="number" step="0.1" class="form-control" ng-model="graficas.m3.libre_strias_min">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn" ng-click="reloadGrafica('libre_estrias', graficas.m3.libre_strias, graficas.m3.libre_strias_max, graficas.m3.libre_strias_min)">Aplicar</button>
                                                    </div>
                                                </div>
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_libre_estrias" class="label_chart">HVLE</div>
                                                        <div id="libre_estrias" style="width: 100%;" class="chart"> </div>
                                                    </div>
                                                    <div class="portlet-footer">
                                                        <div class="miniature">
                                                            <ul id="check_libre_estrias" class="listado">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                            
                                            <div class="col-md-12 col-sm-12">
                                                <div class="col-md-5 pull-right">
                                                    <div class="col-md-5">
                                                        Max <input type="number" step="0.1" class="form-control" ng-model="graficas.m3.hojas_4_max">
                                                    </div>
                                                    <div class="col-md-5">
                                                        Min <input type="number" step="0.1" class="form-control" ng-model="graficas.m3.hojas_4_min">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn" ng-click="reloadGrafica('hoja_4', graficas.m3.hojas_4, graficas.m3.hojas_4_max, graficas.m3.hojas_4_min)">Aplicar</button>
                                                    </div>
                                                </div>
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hoja_4" class="label_chart">EE H4</div>
                                                        <div id="hoja_4" class="chart"> </div>
                                                    </div>
                                                    <div class="portlet-footer">
                                                        <div class="miniature">
                                                            <ul id="check_hoja_4" class="listado">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END PORTLET-->
                                            </div>

                                            <div class="col-md-12 col-sm-6">
                                                <div class="col-md-5 pull-right">
                                                    <div class="col-md-5">
                                                        Max <input type="number" step="0.1" class="form-control" ng-model="graficas.m3.hoja_vieja_menor_5_max">
                                                    </div>
                                                    <div class="col-md-5">
                                                        Min <input type="number" step="0.1" class="form-control" ng-model="graficas.m3.hoja_vieja_menor_5_min">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn" ng-click="reloadGrafica('hoja_vieja_menor_5', graficas.m3.hoja_vieja_menor_5, graficas.m3.hoja_vieja_menor_5_max, graficas.m3.hoja_vieja_menor_5_min)">Aplicar</button>
                                                    </div>
                                                </div>
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hoja_vieja_menor_5" class="label_chart">Q<5</div>
                                                        <div id="hoja_vieja_menor_5" class="chart"> </div>
                                                    </div>
                                                    <div class="portlet-footer">
                                                        <div class="miniature">
                                                            <ul id="check_hoja_vieja_menor_5" class="listado">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END PORTLET-->
                                            </div>
                                                                               
                                            <div class="col-md-12 col-sm-6">
                                                <div class="col-md-5 pull-right">
                                                    <div class="col-md-5">
                                                        Max <input type="number" step="0.1" class="form-control" ng-model="graficas.m3.hojas_3_max">
                                                    </div>
                                                    <div class="col-md-5">
                                                        Min <input type="number" step="0.1" class="form-control" ng-model="graficas.m3.hojas_3_min">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn" ng-click="reloadGrafica('hoja_3', graficas.m3.hojas_3, graficas.m3.hojas_3_max, graficas.m3.hojas_3_min)">Aplicar</button>
                                                    </div>
                                                </div>
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hoja_3" class="label_chart">EE H3</div>
                                                        <div id="hoja_3" class="chart"> </div>
                                                    </div>
                                                    <div class="portlet-footer">
                                                        <div class="miniature">
                                                            <ul id="check_hoja_3" class="listado">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END PORTLET-->
                                            </div>
                                            
                                            <div class="col-md-12 col-sm-6">
                                                <div class="col-md-5 pull-right">
                                                    <div class="col-md-5">
                                                        Max <input type="number" step="0.1" class="form-control" ng-model="graficas.m3.hojas_5_max">
                                                    </div>
                                                    <div class="col-md-5">
                                                        Min <input type="number" step="0.1" class="form-control" ng-model="graficas.m3.hojas_5_min">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn" ng-click="reloadGrafica('hoja_5', graficas.m3.hojas_5, graficas.m3.hojas_5_max, graficas.m3.hojas_5_min)">Aplicar</button>
                                                    </div>
                                                </div>
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hoja_5" class="label_chart">EE H5</div>
                                                        <div id="hoja_5" class="chart"> </div>
                                                    </div>
                                                    <div class="portlet-footer">
                                                        <div class="miniature">
                                                            <ul id="check_hoja_5" class="listado">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END PORTLET-->
                                            </div>
                                            
                                            <div class="tabbable-custom nav-justified">
                                                <ul class="nav nav-tabs nav-justified">
                                                    <li class="active">
                                                        <a href="#tab_1_1" data-toggle="tab"> HVLE </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_2" data-toggle="tab"> EE H4 </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_3" data-toggle="tab"> Q<5 </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_4" data-toggle="tab"> EE H3 </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_5" data-toggle="tab"> EE H5 </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_1_1">
                                                        <div class="table-scrollable"> 
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-hide="value.name == 'Umbral'" ng-repeat="(key, value) in graficas.m3.libre_strias.series">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.m3.libre_strias.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.m3.libre_strias, valor.data[key]) }}" ng-hide="valor.name == 'Umbral'" ng-repeat="(llave, valor) in graficas.m3.libre_strias.series">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_1_2">
                                                        <div class="table-scrollable"> 
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th ng-repeat="(key, value) in graficas.m3.hojas_4.series" class="center-th" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.m3.hojas_4.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.m3.hojas_4, valor.data[key], true) }}" ng-hide="valor.name == 'Umbral'" ng-repeat="(llave, valor) in graficas.m3.hojas_4.series">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_1_3">
                                                        <div class="table-scrollable"> 
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.m3.hoja_vieja_menor_5.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.m3.hoja_vieja_menor_5.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.m3.hoja_vieja_menor_5, valor.data[key]) }}" ng-repeat="(llave, valor) in graficas.m3.hoja_vieja_menor_5.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_1_4">
                                                        <div class="table-scrollable"> 
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.m3.hojas_3.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.m3.hojas_3.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.m3.hojas_3, valor.data[key], true) }}" ng-repeat="(llave, valor) in graficas.m3.hojas_3.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_1_5">
                                                        <div class="table-scrollable"> 
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.m3.hojas_5.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.m3.hojas_5.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.m3.hojas_5, valor.data[key], true) }}" ng-repeat="(llave, valor) in graficas.m3.hojas_5.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a id="tab_0s" class="accordion-toggle accordion-toggle-styled collapsed" ng-click="getGraficasS0()" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" aria-expanded="false"> Plantas 0 Sem </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            
                                            <div class="col-md-12 col-sm-12">
                                                <div class="col-md-5 pull-right">
                                                    <div class="col-md-5">
                                                        Max <input type="number" step="0.1" class="form-control" ng-model="graficas.s0.hojas_total_S0_max">
                                                    </div>
                                                    <div class="col-md-5">
                                                        Min <input type="number" step="0.1" class="form-control" ng-model="graficas.s0.hojas_total_S0_min">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn" ng-click="reloadGrafica('hojas_total', graficas.s0.hojas_total_S0, graficas.s0.hojas_total_S0_max, graficas.s0.hojas_total_S0_min)">Aplicar</button>
                                                    </div>
                                                </div>
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hojas_total" class="label_chart">Hojas totales</div>
                                                        <div id="hojas_total" class="chart"> </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-6">
                                                <div class="col-md-5 pull-right">
                                                    <div class="col-md-5">
                                                        Max <input type="number" step="0.1" class="form-control" ng-model="graficas.s0.hoja_vieja_menor_5_S0_max">
                                                    </div>
                                                    <div class="col-md-5">
                                                        Min <input type="number" step="0.1" class="form-control" ng-model="graficas.s0.hoja_vieja_menor_5_S0_min">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn" ng-click="reloadGrafica('hoja_vieja_menor_5_0S', graficas.s0.hoja_vieja_menor_5_S0, graficas.s0.hoja_vieja_menor_5_S0_max, graficas.s0.hoja_vieja_menor_5_S0_min)">Aplicar</button>
                                                    </div>
                                                </div>
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hoja_vieja_menor_5_0S" class="label_chart">Q<5</div>
                                                        <div id="hoja_vieja_menor_5_0S" class="chart"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-6">
                                                <div class="col-md-5 pull-right">
                                                    <div class="col-md-5">
                                                        Max <input type="number" step="0.1" class="form-control" ng-model="graficas.s0.libre_estrias_S0_max">
                                                    </div>
                                                    <div class="col-md-5">
                                                        Min <input type="number" step="0.1" class="form-control" ng-model="graficas.s0.libre_estrias_S0_min">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn" ng-click="reloadGrafica('libre_estrias_0S', graficas.s0.libre_estrias_S0, graficas.s0.libre_estrias_S0_max, graficas.s0.libre_estrias_S0_min)">Aplicar</button>
                                                    </div>
                                                </div>
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_libre_estrias_0S" class="label_chart">HVLE</div>
                                                        <div id="libre_estrias_0S" class="chart"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tabbable-custom nav-justified">
                                                <ul class="nav nav-tabs nav-justified">
                                                    <li class="active">
                                                        <a href="#tab_2_1" data-toggle="tab"> HT </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_2_2" data-toggle="tab"> Q<5 </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_2_3" data-toggle="tab"> HVLE </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_2_1">
                                                        <div class="table-scrollable">
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.s0.hojas_total_S0.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.s0.hojas_total_S0.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.s0.hojas_total_S0, valor.data[key]) }}" ng-repeat="(llave, valor) in graficas.s0.hojas_total_S0.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>                                                        
                                                    </div>

                                                    <div class="tab-pane" id="tab_2_2">
                                                       <div class="table-scrollable">
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.s0.hoja_vieja_menor_5_S0.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.s0.hoja_vieja_menor_5_S0.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.s0.hoja_vieja_menor_5_S0, valor.data[key]) }}" ng-repeat="(llave, valor) in graficas.s0.hoja_vieja_menor_5_S0.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>        
                                                    </div>

                                                    <div class="tab-pane" id="tab_2_3">
                                                        <div class="table-scrollable">
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.s0.libre_estrias_S0.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.s0.libre_estrias_S0.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.s0.libre_estrias_S0, valor.data[key]) }}" ng-repeat="(llave, valor) in graficas.s0.libre_estrias_S0.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>       
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a id="tab_11s" class="accordion-toggle accordion-toggle-styled collapsed" ng-click="getGraficasS11()" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false"> Plantas 11 Sem </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            
                                            <div class="col-md-12 col-sm-6">
                                                <div class="col-md-5 pull-right">
                                                    <div class="col-md-5">
                                                        Max <input type="number" step="0.1" class="form-control" ng-model="graficas.s11.hojas_total_S11_max">
                                                    </div>
                                                    <div class="col-md-5">
                                                        Min <input type="number" step="0.1" class="form-control" ng-model="graficas.s11.hojas_total_S11_min">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn" ng-click="reloadGrafica('hojas_total_11S', graficas.s11.hojas_total_S11, graficas.s11.hojas_total_S11_max, graficas.s11.hojas_total_S11_min)">Aplicar</button>
                                                    </div>
                                                </div>
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hojas_total_11S" class="label_chart">HT</div>
                                                        <div id="hojas_total_11S" class="chart"> </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-12 col-sm-6">
                                                <div class="col-md-5 pull-right">
                                                    <div class="col-md-5">
                                                        Max <input type="number" step="0.1" class="form-control" ng-model="graficas.s11.hoja_vieja_menor_5_S11_max">
                                                    </div>
                                                    <div class="col-md-5">
                                                        Min <input type="number" step="0.1" class="form-control" ng-model="graficas.s11.hoja_vieja_menor_5_S11_min">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn" ng-click="reloadGrafica('hoja_vieja_menor_5_11S', graficas.s11.hoja_vieja_menor_5_S11, graficas.s11.hoja_vieja_menor_5_S11_max, graficas.s11.hoja_vieja_menor_5_S11_min)">Aplicar</button>
                                                    </div>
                                                </div>
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hoja_vieja_menor_5_11S" class="label_chart">Q<5</div>
                                                        <div id="hoja_vieja_menor_5_11S" class="chart"> </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tabbable-custom nav-justified">
                                                <ul class="nav nav-tabs nav-justified">
                                                    <li class="active">
                                                        <a href="#tab_3_1" data-toggle="tab"> HT </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_3_2" data-toggle="tab"> Q<5 </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_3_1">
                                                        <div class="table-scrollable">
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.s11.hojas_total_S11.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.s11.hojas_total_S11.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.s11.hojas_total_S11, valor.data[key]) }}" ng-repeat="(llave, valor) in graficas.s11.hojas_total_S11.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>                                                        
                                                    </div>

                                                    <div class="tab-pane" id="tab_3_2">
                                                        <div class="table-scrollable">
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.s11.hoja_vieja_menor_5_S11.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.s11.hoja_vieja_menor_5_S11.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.s11.hoja_vieja_menor_5_S11, valor.data[key]) }}" ng-repeat="(llave, valor) in graficas.s11.hoja_vieja_menor_5_S11.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a id="tab_3m" class="accordion-toggle accordion-toggle-styled collapsed" ng-click="getGraficas3M()" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" aria-expanded="false"> Gráficas </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">

                                            <div class="col-md-12 col-sm-12">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_libre_estrias" class="label_chart row">PLANTAS JOVENES - EE HOJA 3-4-5</div>
                                                        <div class="col-md-5">
                                                            <div class="col-md-4">
                                                                MAX <input type="number" class="form-control" ng-model="graficas.m3.libre_strias.yAxis.max">
                                                            </div>
                                                            <div class="col-md-4">
                                                                MIN <input type="number" class="form-control" ng-model="graficas.m3.libre_strias.yAxis.min">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span style="visibility:hidden">A</span>
                                                                <button class="btn" ng-click="reloadGrafica('libre_estrias', graficas.m3.libre_strias)">Aplicar</button>
                                                            </div>
                                                        </div>
                                                        <div id="libre_estrias" style="width: 100%;" class="chart"> </div>
                                                    </div>
                                                    <div class="portlet-footer">
                                                        <div class="miniature">
                                                            <ul id="check_libre_estrias" class="listado">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                            
                                            <div class="col-md-12 col-sm-12">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hoja_4" class="label_chart">HOJAS TOTALES - PLANTAS 0 SEM - 11 SEM</div>
                                                        <div class="col-md-5">
                                                            <div class="col-md-4">
                                                                MAX <input type="number" class="form-control" ng-model="graficas.m3.hojas_4.yAxis[0].max">
                                                            </div>
                                                            <div class="col-md-4">
                                                                MIN <input type="number" class="form-control" ng-model="graficas.m3.hojas_4.yAxis[0].min">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span style="visibility:hidden">A</span>
                                                                <button class="btn" ng-click="reloadGrafica('hoja_4', graficas.m3.hojas_4)">Aplicar</button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5 pull-right">
                                                            <div class="col-md-4">
                                                                MAX <input type="number" class="form-control" ng-model="graficas.m3.hojas_4.yAxis[1].max">
                                                            </div>
                                                            <div class="col-md-4">
                                                                MIN <input type="number" class="form-control" ng-model="graficas.m3.hojas_4.yAxis[1].min">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span style="visibility:hidden">A</span>
                                                                <button class="btn" ng-click="reloadGrafica('hoja_4', graficas.m3.hojas_4)">Aplicar</button>
                                                            </div>
                                                        </div>
                                                        <div id="hoja_4" class="chart"> </div>
                                                    </div>
                                                    <div class="portlet-footer">
                                                        <div class="miniature">
                                                            <ul id="check_hoja_4" class="listado">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hoja_vieja_menor_5" class="label_chart">H+VLE - PLANTAS 3M - 0 SEM</div>
                                                        <div class="col-md-5">
                                                            <div class="col-md-4">
                                                                MAX <input type="number" class="form-control" ng-model="graficas.m3.hoja_vieja_menor_5.yAxis.max">
                                                            </div>
                                                            <div class="col-md-4">
                                                                MIN <input type="number" class="form-control" ng-model="graficas.m3.hoja_vieja_menor_5.yAxis.min">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span style="visibility:hidden">A</span>
                                                                <button class="btn" ng-click="reloadGrafica('hoja_vieja_menor_5', graficas.m3.hoja_vieja_menor_5)">Aplicar</button>
                                                            </div>
                                                        </div>
                                                        <div id="hoja_vieja_menor_5" class="chart"> </div>
                                                    </div>
                                                    <div class="portlet-footer">
                                                        <div class="miniature">
                                                            <ul id="check_hoja_vieja_menor_5" class="listado">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                                               
                                            <div class="col-md-12 col-sm-12">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hoja_3" class="label_chart">Q<5% - PLATAS 0 SEM - 11 SEM</div>
                                                        <div class="col-md-5">
                                                            <div class="col-md-4">
                                                                MAX <input type="number" class="form-control" ng-model="graficas.m3.hojas_3.yAxis[0].max">
                                                            </div>
                                                            <div class="col-md-4">
                                                                MIN <input type="number" class="form-control" ng-model="graficas.m3.hojas_3.yAxis[0].min">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span style="visibility:hidden">A</span>
                                                                <button class="btn" ng-click="reloadGrafica('hoja_3', graficas.m3.hojas_3)">Aplicar</button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5 pull-right">
                                                            <div class="col-md-4">
                                                                MAX <input type="number" class="form-control" ng-model="graficas.m3.hojas_3.yAxis[1].max">
                                                            </div>
                                                            <div class="col-md-4">
                                                                MIN <input type="number" class="form-control" ng-model="graficas.m3.hojas_3.yAxis[1].min">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span style="visibility:hidden">A</span>
                                                                <button class="btn" ng-click="reloadGrafica('hoja_3', graficas.m3.hojas_3)">Aplicar</button>
                                                            </div>
                                                        </div>
                                                        <div id="hoja_3" class="chart"> </div>
                                                    </div>
                                                    <div class="portlet-footer">
                                                        <div class="miniature">
                                                            <ul id="check_hoja_3" class="listado">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-12 col-sm-12">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hoja_5" class="label_chart">HOJAS TOTALES - PLANTAS 3M</div>
                                                        <div class="col-md-5">
                                                            <div class="col-md-4">
                                                                MAX <input type="number" class="form-control" ng-model="graficas.m3.hojas_5.yAxis.max">
                                                            </div>
                                                            <div class="col-md-4">
                                                                MIN <input type="number" class="form-control" ng-model="graficas.m3.hojas_5.yAxis.min">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span style="visibility:hidden">A</span>
                                                                <button class="btn" ng-click="reloadGrafica('hoja_5', graficas.m3.hojas_5)">Aplicar</button>
                                                            </div>
                                                        </div>
                                                        <div id="hoja_5" class="chart"> </div>
                                                    </div>
                                                    <div class="portlet-footer">
                                                        <div class="miniature">
                                                            <ul id="check_hoja_5" class="listado">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="tabbable-custom nav-justified">
                                                <ul class="nav nav-tabs nav-justified">
                                                    <li class="active">
                                                        <a href="#tab_1_1" data-toggle="tab"> EE HOJA 3-4-5 </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_2" data-toggle="tab"> HOJAS TOTALES 0/11 SEM </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_3" data-toggle="tab"> H+VLE </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_4" data-toggle="tab"> Q<5% </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_5" data-toggle="tab"> HOJAS TOTALES 3M </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_1_1">
                                                        <div class="table-scrollable"> 
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-hide="value.name == 'Umbral'" ng-repeat="(key, value) in graficas.m3.libre_strias.series">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.m3.libre_strias.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.m3.libre_strias, valor.data[key]) }}" ng-hide="valor.name == 'Umbral'" ng-repeat="(llave, valor) in graficas.m3.libre_strias.series">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_1_2">
                                                        <div class="table-scrollable"> 
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th ng-repeat="(key, value) in graficas.m3.hojas_4.series" class="center-th" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.m3.hojas_4.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.m3.hojas_4, valor.data[key], true) }}" ng-hide="valor.name == 'Umbral'" ng-repeat="(llave, valor) in graficas.m3.hojas_4.series">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_1_3">
                                                        <div class="table-scrollable"> 
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.m3.hoja_vieja_menor_5.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.m3.hoja_vieja_menor_5.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.m3.hoja_vieja_menor_5, valor.data[key]) }}" ng-repeat="(llave, valor) in graficas.m3.hoja_vieja_menor_5.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_1_4">
                                                        <div class="table-scrollable"> 
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.m3.hojas_3.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.m3.hojas_3.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.m3.hojas_3, valor.data[key], true) }}" ng-repeat="(llave, valor) in graficas.m3.hojas_3.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_1_5">
                                                        <div class="table-scrollable"> 
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.m3.hojas_5.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.m3.hojas_5.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.m3.hojas_5, valor.data[key], true) }}" ng-repeat="(llave, valor) in graficas.m3.hojas_5.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default hide">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a id="tab_0s" class="accordion-toggle accordion-toggle-styled collapsed" ng-click="getGraficasS0()" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" aria-expanded="false"> Plantas 0 Sem </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            
                                            <div class="col-md-12 col-sm-6">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hojas_total" class="label_chart">Hojas totales</div>
                                                        <div id="hojas_total" class="chart"> </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-6">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hoja_vieja_menor_5_0S" class="label_chart">Q<5</div>
                                                        <div id="hoja_vieja_menor_5_0S" class="chart"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-6">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_libre_estrias_0S" class="label_chart">HVLE</div>
                                                        <div id="libre_estrias_0S" class="chart"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tabbable-custom nav-justified">
                                                <ul class="nav nav-tabs nav-justified">
                                                    <li class="active">
                                                        <a href="#tab_2_1" data-toggle="tab"> HT </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_2_2" data-toggle="tab"> Q<5 </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_2_3" data-toggle="tab"> HVLE </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_2_1">
                                                        <div class="table-scrollable">
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.s0.hojas_total_S0.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.s0.hojas_total_S0.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.s0.hojas_total_S0, valor.data[key]) }}" ng-repeat="(llave, valor) in graficas.s0.hojas_total_S0.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>                                                        
                                                    </div>

                                                    <div class="tab-pane" id="tab_2_2">
                                                       <div class="table-scrollable">
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.s0.hoja_vieja_menor_5_S0.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.s0.hoja_vieja_menor_5_S0.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.s0.hoja_vieja_menor_5_S0, valor.data[key]) }}" ng-repeat="(llave, valor) in graficas.s0.hoja_vieja_menor_5_S0.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>        
                                                    </div>

                                                    <div class="tab-pane" id="tab_2_3">
                                                        <div class="table-scrollable">
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.s0.libre_estrias_S0.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.s0.libre_estrias_S0.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.s0.libre_estrias_S0, valor.data[key]) }}" ng-repeat="(llave, valor) in graficas.s0.libre_estrias_S0.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>       
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default hide">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a id="tab_11s" class="accordion-toggle accordion-toggle-styled collapsed" ng-click="getGraficasS11()" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false"> Plantas 11 Sem </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            
                                            <div class="col-md-12 col-sm-6">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hojas_total_11S" class="label_chart">HT</div>
                                                        <div id="hojas_total_11S" class="chart"> </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-12 col-sm-6">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hoja_vieja_menor_5_11S" class="label_chart">Q<5</div>
                                                        <div id="hoja_vieja_menor_5_11S" class="chart"> </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tabbable-custom nav-justified">
                                                <ul class="nav nav-tabs nav-justified">
                                                    <li class="active">
                                                        <a href="#tab_3_1" data-toggle="tab"> HT </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_3_2" data-toggle="tab"> Q<5 </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_3_1">
                                                        <div class="table-scrollable">
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.s11.hojas_total_S11.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.s11.hojas_total_S11.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.s11.hojas_total_S11, valor.data[key]) }}" ng-repeat="(llave, valor) in graficas.s11.hojas_total_S11.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>                                                        
                                                    </div>

                                                    <div class="tab-pane" id="tab_3_2">
                                                        <div class="table-scrollable">
                                                            <table class="table table-striped table-bordered table-hover textTittle">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center-th">SEM</th>
                                                                        <th class="center-th" ng-repeat="(key, value) in graficas.s11.hoja_vieja_menor_5_S11.series" ng-hide="value.name == 'Umbral'">{{value.name}}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="(key, value) in graficas.s11.hoja_vieja_menor_5_S11.xAxis.data">
                                                                        <td class="center-th">{{value}}</td>
                                                                        <td class="center-th {{ getClassColor(graficas.s11.hoja_vieja_menor_5_S11, valor.data[key]) }}" ng-repeat="(llave, valor) in graficas.s11.hoja_vieja_menor_5_S11.series" ng-hide="valor.name == 'Umbral'">
                                                                            {{ valor.data[key] | number: 2 }}
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                                
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle accordion-toggle-styled collapsed" ng-click="getFoliar()" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4" aria-expanded="false"> Foliar </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_4" class="panel-collapse collapse" aria-expanded="true" style="">
                                        <div class="panel-body">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="label_hojas_total_11S" class="label_chart"></div>
                                                        <div id="foliar" class="chart"> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default hide">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_5" aria-expanded="false"> Por Lote </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_5" class="panel-collapse collapse" aria-expanded="true" style="">
                                        <div class="panel-body">
                                            <select id="semana" class="input-sm pull-right" style="color:black;" ng-model="filters.semana" ng-change="getTablaPorLote()">
                                                <option ng-repeat="(key, value) in semanas" value="{{ key }}" ng-selected="key == filters.semana">{{ value }}</option>
                                            </select>
                                            <div class="table-responsive table-scrollable">
                                                <table class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th class="center-th" colspan="5">3 METROS</th>
                                                            <th class="center-th" colspan="3">0 SEMANAS</th>
                                                            <th class="center-th" colspan="2">11 SEMANAS</th>
                                                        </tr>
                                                        <tr>
                                                            <th class="center-th">LOTE</th>

                                                            <th class="center-th">H3</th>
                                                            <th class="center-th">H4</th>
                                                            <th class="center-th">H5</th>
                                                            <th class="center-th">LE</th>
                                                            <th class="center-th">Q<5%</th>

                                                            <th class="center-th">HT</th>
                                                            <th class="center-th">Q<5%</th>
                                                            <th class="center-th">LE</th>

                                                            <th class="center-th">HT</th>
                                                            <th class="center-th">Q<5%</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr ng-repeat="row in table_lote">
                                                            <td class="center-th">{{ row.lote }}</td>
                                                            <td class="center-th {{ semaforo(row.m3_h3, 25, true) }}">{{ row.m3_h3 }}</td>
                                                            <td class="center-th {{ semaforo(row.m3_h4, 30, true) }}">{{ row.m3_h4 }}</td>
                                                            <td class="center-th {{ semaforo(row.m3_h5, 35, true) }}">{{ row.m3_h5 }}</td>
                                                            <td class="center-th {{ semaforo(row.m3_hmvle, 6.5) }}">{{ row.m3_hmvle | number:2 }}</td>
                                                            <td class="center-th {{ semaforo(row.m3_hmvlqm, 11) }}">{{ row.m3_hmvlqm | number:2 }}</td>

                                                            <td class="center-th {{ semaforo(row.s0_ht, 13) }}">{{ row.s0_ht | number:2 }}</td>
                                                            <td class="center-th">{{ row.s0_hmvlqm | number:2 }}</td>
                                                            <td class="center-th">{{ row.s0_hmvle  | number:2 }}</td>

                                                            <td class="center-th {{ semaforo(row.s11_ht, 6.5) }}">{{ row.s11_ht | number:2 }}</td>
                                                            <td class="center-th">{{ row.s11_hmvlqm | number:2 }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>            
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle accordion-toggle-styled collapsed" ng-click="getClima()" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_7" aria-expanded="false"> Clima </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_7" class="panel-collapse collapse" aria-expanded="true" style="">
                                        <div class="panel-body">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div id="id_label_temp_min" class="label_chart">TEMP MIN</div>
                                                        <div id="temp_min" class="chart"> </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div class="label_chart">TEMP MAX</div>
                                                        <div id="temp_max" class="chart"> </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div class="label_chart">LLUVIA</div>
                                                        <div id="lluvia" class="chart"> </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-body">
                                                        <div class="label_chart">RAD SOLAR (W/M2)</div>
                                                        <div id="rad_solar" class="chart"> </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-title">
                                                        <div class="tools">
                                                            <ng-calendarapp search=""></ng-calendarapp>
                                                        </div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="label_chart">RAD SOLAR POR HORA (W/M2)</div>
                                                        <div id="rad_solar_horas" class="chart"> </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12">
                                                <div class="portlet light portlet-fit bordered">
                                                    <div class="portlet-title">
                                                        <div class="tools">
                                                            <select class="form-control input-sm" ng-model="filters.luz" ng-change="changeLuz()">
                                                                <option value="100" ng-selected="100 == filters.luz">100 (W/M2)</option>
                                                                <option value="150" ng-selected="150 == filters.luz">150 (W/M2)</option>
                                                                <option value="200" ng-selected="200 == filters.luz">200 (W/M2)</option>
                                                                <option value="400" ng-selected="400 == filters.luz">400 (W/M2)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="label_chart">HORAS LUZ</div>
                                                        <div id="horas_luz" class="chart"> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_8" aria-expanded="false"> Importar Información </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_8" class="panel-collapse collapse" aria-expanded="true" style="">
                                        <div class="panel-body">
                                            <h2 style="display: inline-block;">Importar Información</h2>
                                            <button class="btn blue pull-right" ng-click="subir()">Subir</button>
                                            <br><br>
                                            <div class="portlet-body">
                                                <img src="./img/formato_excel_inspeccion_palmar.png" style="width : 100%"/>
                                                <h3>Excel (.xlsx)</h3>
                                                <input id="input-file" file="file" type="file" accept=".xlsx">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->   