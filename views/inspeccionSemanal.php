<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    $Clientes = $loader->getClientesDiponibles();
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
    <style>
        .listado{
            list-style-type: none;
            display: inline-block;
        }
        .listado > li{
            display: inline;
            margin-right: 10px;
        }
        .miniature{
            display: inline-block;
        }
        .center-th {
            text-align : center;
        }
        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .legendLabel{
            padding: 3px !important;
        }
        .textTittle {
            font-size: 11px !important;
        }
        table > th {
            text-align: center !important;
        }
        tbody > td {
            text-align: center !important;
        }
        .charts {
            height : 400px;
        }
        hr {
            margin-bottom : 15px;
        }
    </style>
    <!-- BEGIN CONTENT BODY -->
    <div ng-app="app" class="page-content" ng-controller="inspeccion">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Inspección</h1>
            </div>
            <!-- END PAGE TITLE -->
            <div class="page-toolbar">
                <div id="" class="pull-right tooltips">
                    <select id="client" name="client" class="ticket-assign form-control input-medium" ng-model="id_cliente" ng-change="getFincas(true)">
                        <?php foreach ($Clientes as $key => $value): ?>
                            <option value="<?= $value['id'] ?>"><?= $value['nombre'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <select name="hacienda" id="hacienda" class="form-control hide" ng-model="hacienda" ng-change="save()">
                        <option ng-repeat="fin in haciendas" value="{{fin.id}}" ng-selected="fin.id == hacienda">{{fin.nombre}}</option>
                    </select>
                    <div class="col-md-12" style="padding: 0">
                        <div class="col-md-6">
                            Semana:
                        </div>
                        <div class="col-md-6" style="padding: 0">
                            <select class="form-control" ng-model="filters.semana" ng-change="init()">
                                <option value="{{sem}}" ng-repeat="sem in semanas" ng-selected="sem == filters.semana">{{sem}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- END THEME PANEL -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.php">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Inspección</span>
            </li>
        </ul>
        
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="mt-element-list col-md-4">
                        <div class="mt-list-container list-default ext-1 group" style="padding: 0;">
                            <a class="list-toggle-container collapsed" data-toggle="collapse" href="#completed" aria-expanded="false">
                                <div class="list-toggle done uppercase"> Fincas Abreviaturas
                                </div>
                            </a>
                            <div class="panel-collapse collapse" id="completed" aria-expanded="false" style="height: 0px;">
                                <ul>
                                    <li class="mt-list-item done" ng-repeat="f in fincas">
                                        <div class="list-item-content" style="padding:0px;">
                                            {{ f.nombre_real }} - {{ f.alias }}
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>

                <div id="grafica-m3-h3" class="charts"></div>
                <div class="table-responisve">
                    <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <th ng-repeat="finca in data['grafica-m3-h3'].legend">{{ finca }}</th>
                        </tr>
                        <tbody>
                            <tr ng-repeat="(key, values) in data['grafica-m3-h3'].data">
                                <th>
                                    <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                </th>
                                <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div id="grafica-m3-h4" class="charts"></div>
                <div class="table-responisve">
                    <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <th ng-repeat="finca in data['grafica-m3-h4'].legend">{{ finca }}</th>
                        </tr>
                        <tbody>
                            <tr ng-repeat="(key, values) in data['grafica-m3-h4'].data">
                                <th>
                                    <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                </th>
                                <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div id="grafica-m3-h5" class="charts"></div>
                <div class="table-responisve">
                    <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <th ng-repeat="finca in data['grafica-m3-h5'].legend">{{ finca }}</th>
                        </tr>
                        <tbody>
                            <tr ng-repeat="(key, values) in data['grafica-m3-h5'].data">
                                <th>
                                    <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                </th>
                                <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div id="grafica-m3-hvlq" class="charts"></div>
                <div class="table-responisve">
                    <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <th ng-repeat="finca in data['grafica-m3-hvlq'].legend">{{ finca }}</th>
                        </tr>
                        <tbody>
                            <tr ng-repeat="(key, values) in data['grafica-m3-hvlq'].data">
                                <th>
                                    <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                </th>
                                <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div id="grafica-m3-hvle" class="charts"></div>
                <div class="table-responisve">
                    <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <th ng-repeat="finca in data['grafica-m3-hvle'].legend">{{ finca }}</th>
                        </tr>
                        <tbody>
                            <tr ng-repeat="(key, values) in data['grafica-m3-hvle'].data">
                                <th>
                                    <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                </th>
                                <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div id="grafica-s0-ht" class="charts"></div>
                <div class="table-responisve">
                    <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <th ng-repeat="finca in data['grafica-s0-ht'].legend">{{ finca }}</th>
                        </tr>
                        <tbody>
                            <tr ng-repeat="(key, values) in data['grafica-s0-ht'].data">
                                <th>
                                    <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                </th>
                                <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div id="grafica-s0-hvle" class="charts"></div>
                <div class="table-responisve">
                    <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <th ng-repeat="finca in data['grafica-s0-hvle'].legend">{{ finca }}</th>
                        </tr>
                        <tbody>
                            <tr ng-repeat="(key, values) in data['grafica-s0-hvle'].data">
                                <th>
                                    <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                </th>
                                <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div id="grafica-s0-hvlq" class="charts"></div>
                <div class="table-responisve">
                    <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <th ng-repeat="finca in data['grafica-s0-hvlq'].legend">{{ finca }}</th>
                        </tr>
                        <tbody>
                            <tr ng-repeat="(key, values) in data['grafica-s0-hvlq'].data">
                                <th>
                                    <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                </th>
                                <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div id="grafica-s11-ht" class="charts"></div>
                <div class="table-responisve">
                    <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <th ng-repeat="finca in data['grafica-s11-ht'].legend">{{ finca }}</th>
                        </tr>
                        <tbody>
                            <tr ng-repeat="(key, values) in data['grafica-s11-ht'].data">
                                <th>
                                    <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                </th>
                                <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div id="grafica-s11-hvlq" class="charts"></div>
                <div class="table-responisve">
                    <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <th ng-repeat="finca in data['grafica-s11-hvlq'].legend">{{ finca }}</th>
                        </tr>
                        <tbody>
                            <tr ng-repeat="(key, values) in data['grafica-s11-hvlq'].data">
                                <th>
                                    <span class="bg-{{ key == 'FOCO' ? 'red-thunderbird' : 'blue-ebonyclay' }}">&nbsp;&nbsp;</span> {{ key }}
                                </th>
                                <td ng-repeat="val in values.data track by $index">{{ val }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->   