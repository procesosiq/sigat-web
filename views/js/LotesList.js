var TableDatatablesAjax = function () {

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }

    var handleRecords = function () {
        grid.init("#datatable_ajax" , "../controllers/index.php?accion=Lotes");
        $('#datatable_ajax tbody').on( 'click', 'button', function () {
            // console.log(grid.table.getSelectedRows());
            // console.log($(this).parents('tr'));
            // console.log(grid.table.getDataTable());
             var data = grid.tableEdit.row( $(this).parents('tr') ).data();
             if(data.length > 0){
                document.location.href = "/newProduc?id=" + data[1];
             }
        } );
    }

    return {

        //main function to initiate the module
        init: function () {

            initPickers();
            handleRecords();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesAjax.init();
});