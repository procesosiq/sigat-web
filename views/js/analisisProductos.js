app.service('resumen', function($http){
    var service = {};
    var options = {
        url: "./controllers/index.php",
        method: "POST"
    };

    service.proveedores = function(params){
        var option = options;
        option.data = {
            accion: "AnalisisProductos.proveedores" , 
            params : params
        };
        return $http(option);
    }

    service.productos = function(params){
        var option = options;
        option.data = {
            accion: "AnalisisProductos.productos" , 
            params : params
        };
        return $http(option);
    }

    service.tipos = function(params){
        var option = options;
        option.data = {
            accion: "AnalisisProductos.tipos" , 
            params : params
        };
        return $http(option);
    }

    service.anios = function(params){
        var option = options;
        option.data = {
            accion: "AnalisisProductos.years" , 
            params : params
        };
        return $http(option);
    }

    return service;
});

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        if(["peso", "manos", "calibre", "dedos"].indexOf(key) > -1){
            sum = sum / count;
        }
        return sum;
    }
})

app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.controller('analisis', ['$scope', '$http', '$interval', '$controller', 'resumen', function ($scope, $http, $interval, $controller, resumen) {
    $scope.search = {
        order : 'proveedor',
        revserse : false
    }
    $scope.search2 = {
        order : 'producto',
        revserse : false
    }
    $scope.search3 = {
        order : 'tipo',
        revserse : false
    }
    $scope.filters = {
        tipo_producto : "",
        year : moment().year()
    }
    
    $scope.proccess = function (r) {
        if (r.hasOwnProperty("data")) {
            return r.data;
        }
    };

    $scope.init = function(){
        let data = $scope.filters
        resumen.proveedores(data).then($scope.processProveedores)
        resumen.productos(data).then($scope.processProductos)
        resumen.tipos(data).then($scope.processTipos)
        resumen.anios(data).then($scope.processAnios)
    }

    $scope.processProveedores = function(r){
        let data = $scope.proccess(r);
        $scope.proveedores = data.data
    }

    $scope.processProductos = function(r){
        let data = $scope.proccess(r);
        $scope.productos = data.data
    }
    $scope.processTipos = function(r){
        let data = $scope.proccess(r);
        $scope.tipos = data.data
    }
    $scope.processAnios = function(r){
        let data = $scope.proccess(r);
        $scope.anios = data.years
    }
    
    // 19/06/2017 - TAG: GRAFICAS
    $scope.chart_ciclos_aplicados = new echartsPlv();
    $scope.chart_ciclos_aplicados_sum = new echartsPlv();
    $scope.chart_ciclos_aplicados_avg = new echartsPlv();
    $scope.motivo_char = new echartsPlv();
    // 19/06/2017 - TAG: GRAFICAS
    
    $scope.exportExcel = function(id_table, title){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var contentTable = table.innerHTML            
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(id_table, title || "")
    }

    $scope.exportPrint = function(id_table){
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;
        var newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
    }

    $scope.number_format = function(amount, decimals) {
        amount += ''; // por si pasan un numero en vez de un string
        amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

        decimals = decimals || 0; // por si la variable no fue fue pasada

        // si no es un numero o es igual a cero retorno el mismo cero
        if (isNaN(amount) || amount === 0) 
            return parseFloat(0).toFixed(decimals);

        // si es mayor o menor que cero retorno el valor formateado como numero
        amount = '' + amount.toFixed(decimals);

        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;

        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

        return amount_parts.join('.');
    }

    $scope.init()

}])