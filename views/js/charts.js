function echartsPlv(){
	var self = this;
	self = {
		echarts : echarts,
		callback : function(){},
		theme : "",
		id : "",
		options : "",
		required : function(){
			if(!(typeof echarts === 'object')) throw Error("Echarts Library is Required");
			if(!this.id) throw Error("ID is Required");
			if(!this.options) throw Error("Options is Required");
		},
		chart : {}
	} 

	function call(options , id, callback , theme){
		self.id = id;
		self.theme = theme || "infographic";
		self.options = options;
		self.callback = callback || function(){};
		self.required();
		var charts = self.echarts.init(document.getElementById(self.id) , self.theme);
		charts.setOption(self.options);
		self.chart = charts
		// console.log(charts);
		$(window).resize(function(){charts.resize();})
		return charts;
	}
	
	echartsPlv.prototype.init = function(id , options , callback , theme){
		return call(options , id , callback , theme);
	}
	echartsPlv.prototype.reload = function(option , theme){
		
		option || self.options;
		theme = theme || self.theme;
		console.log(self)
		return call(self.id , self.options , self.callback , self.theme);
	}
}