$(function(){
    
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        autoclose: true
    });
    $("#btnadd").click(function(){
        
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre de cliente");
            return false;
        }
        else if($("#txtemail").val()==''){
            alert("Favor de ingresar un email del cliente");
            return false;
        }
        else if($("#txtfec").val()==''){
            alert("Favor de ingresar fecha de registro");
            return false;
        }
        else if($("#txtrazon").val()==''){
            alert("Favor de ingresar la razón social");
            return false;
        }
        else if($("#txtruc").val()==''){
            alert("Favor de ingresar el RUC");
            return false;
        }
        else if($("#txtdircli").val()==''){
            alert("Favor de ingresar una dirección");
            return false;
        }
        else if($("#txttel").val()==''){
            alert("Favor de ingresar un teléfono");
            return false;
        }
        // else if($("#txtciudad").val()==''){
        //     alert("Favor de ingresar una ciudad");
        //     return false;
        // }
        else if($("#txtnom_contacto").val()==''){
            alert("Favor de ingresar un nombre de contacto");
            return false;
        }
        else if($("#txttel_contacto").val()==''){
            alert("Favor de ingresar un numero de telefono");
            return false;
        }
        else{ 
            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: "accion=clientes.AddCliente&txtnom="+$("#txtnom").val()+"&txtemail="+$("#txtemail").val()+"&s_tipocli="+$("#s_tipocli").val()+
                "&txtfec="+$("#txtfec").val()+"&s_sucursales="+$("#s_sucursales").val()+"&txtrazon="+$("#txtrazon").val()+
                "&txtruc="+$("#txtruc").val()+"&txtdircli="+$("#txtdircli").val()+"&txttel="+$("#txttel").val()+"&txtCargo="+$("#txtCargo").val()+
                "&txttel_contacto="+$("#txttel_contacto").val()+"&txtnom_contacto="+$("#txtnom_contacto").val(),
				success: function(msg){
                    alert("Cliente registrado con el ID "+ msg , "CLIENTES" , 'success' , function(){
                        document.location.href='/clienteList';
                    });
                    $('input[type=text]').each(function() {
                        $(this).val('');
                    });
				}
			});
        }
        return false;
	});
    
    $("#btnupd").click(function(){
        
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre de cliente");
            return false;
        }
        else if($("#txtemail").val()==''){
            alert("Favor de ingresar un email del cliente");
            return false;
        }
        else if($("#txtfec").val()==''){
            alert("Favor de ingresar fecha de registro");
            return false;
        }
        else if($("#txtrazon").val()==''){
            alert("Favor de ingresar la razón social");
            return false;
        }
        else if($("#txtruc").val()==''){
            alert("Favor de ingresar el RUC");
            return false;
        }
        else if($("#txtdircli").val()==''){
            alert("Favor de ingresar una dirección");
            return false;
        }
        else if($("#txttel").val()==''){
            alert("Favor de ingresar un teléfono");
            return false;
        }
        // else if($("#txtciudad").val()==''){
        //     alert("Favor de ingresar una ciudad");
        //     return false;
        // }
        else if($("#txtnom_contacto").val()==''){
            alert("Favor de ingresar un nombre de contacto");
            return false;
        }
        else if($("#txttel_contacto").val()==''){
            alert("Favor de ingresar un numero de telefono");
            return false;
        }
        else{ 
            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: "accion=clientes.UpdateCliente&txtnom="+$("#txtnom").val()+"&txtemail="+$("#txtemail").val()+"&s_tipocli="+$("#s_tipocli").val()+
                "&txtfec="+$("#txtfec").val()+"&s_sucursales="+$("#s_sucursales").val()+"&txtrazon="+$("#txtrazon").val()+
                "&txtruc="+$("#txtruc").val()+"&txtdircli="+$("#txtdircli").val()+"&txttel="+$("#txttel").val()+"&txtCargo="+$("#txtCargo").val()+"&idcli="+$("#idcli").val()+
                "&txttel_contacto="+$("#txttel_contacto").val()+"&txtnom_contacto="+$("#txtnom_contacto").val(),
				success: function(msg){ 
                    if(msg){
                        alert("Cliente modificado con éxito" , "CLIENTES" , 'success' , function(){
                            document.location.href='/clienteList';
                        });
                    }
                    else{
                        alert("Error encontrado, favor de intentar más tarde");
                    }
				}
			});
        }
        return false;
	});

    $("#s_tipocli").change(function(){
        alert("cambio");
    });
    
    $("#btnCancelar").click(function(){
        window.location.assign("http://cegaservices.procesos-iq.com/clienteList");
    });

    $("#btnAddcontact").on("click" , function(){
        addRow()
    });

    function addRow(){
        var data = {
            id : "<? echo $_GET['id']?>",
            nombre : $("#txtnom_contacto").val(),
            cargo : $("#txtCargo").val(),
            telefono : $("#txttel_contacto").val(),
            correo : $("#txtemail").val(),
            area : $("#s_areas").val()
        }

        if(data.lote != ""){
            console.log(data);
            ahttp.post("./controllers/index.php?accion=clientes.addContact",printData , data);
        }
    }

    function printData(r , b){
        b();
        if(r){
            var inHTML = [];

            $.each(r, function(index, value){
                inHTML.push("<tr>");
                    inHTML.push("<td>");
                    inHTML.push(value.id);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push(value.nombre);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push(value.cargo);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push(value.telefono);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push("<td>");
                    inHTML.push(value.correo);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push(value.area);
                    inHTML.push("</td>");
                    inHTML.push('<button type="button" class="btn btn-sm red-thunderbird removeRow" id="'+value.id+'">Eliminar</button>');
                    inHTML.push("</td>");
                inHTML.push("</tr>"); 
            });

            $("#rowsFilas").html(inHTML.join("")); //add generated tr html to corresponding table
            $("#txtnom_contacto").val('');
            $("#txtCargo").val('');
            $("#txttel_contacto").val('');
            $("#txtemail").val('');
            $("#s_areas").val('');
        }
    }

});