function generateClima(){
    html2canvas(document.getElementById("contenedor"), {
        onrendered: function(canvas) {     
            var imgData = canvas.toDataURL('image/png');
            
            var doc = new jsPDF(); //210mm wide and 297mm high
            var nombre = parseInt((Math.random() * (100 * 99 * 60 * Math.PI) * Math.random()) * (60 * 120));
            doc.addImage(imgData, 'JPEG', 0, 0,205,297);
            doc.save('Clima_'+nombre+'.pdf');
            $(".logo_clima").css("display" , "none");
        }
    });
}
$(".generatePdf").css("display" , "none");
$(".logo_clima").css("display" , "none");

$(".generatePdf").on("click" , function(){
    $(".logo_clima").css("display" , "none");
    generateClima();
});


app.controller('clima', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){
    
    $scope.init = function(){
        $scope.getData();
    }

    $scope.random = [];
    
    // $scope.getRandomData = function() {
    //     if ($scope.random.length > 0) $scope.random = $scope.random.slice(1);
    //     // do a random walk
    //     while ($scope.random.length < totalPoints) {
    //         var prev = $scope.random.length > 0 ? $scope.random[$scope.random.length - 1] : 50;
    //         var y = prev + Math.random() * 10 - 5;
    //         if (y < 0) y = 0;
    //         if (y > 100) y = 100;
    //         $scope.random.push(y);
    //     }
    //     // zip the generated y values with the x values
    //     var res = [];
    //     for (var i = 0; i < $scope.random.length; ++i) {
    //         res.push([i, $scope.random[i]]);
    //     }

    //     return res;
    // }

    $scope.getData = function(){
        var data = {
            opt : 'TEMMIN'
        }
        client.post("./controllers/climas.php" ,$scope.printGraphycMin , data);
        
        data = {};
        data = {
            opt : "TEMMAX"
        }
        
        client.post("./controllers/climas.php" ,$scope.printGraphycMax , data);

        data = {};
        data = {
            opt : "LLUVIA"
        }
        
        client.post("./controllers/climas.php" ,$scope.printGraphycLluvia , data);


        data = {};
        data = {
            opt : "RADSOLAR"
        }
        
        client.post("./controllers/climas.php" ,$scope.printGraphycRadsolar , data);

        data = {};
        data = {
            opt : "DIASSOL"
        }
        
        client.post("./controllers/climas.php" ,$scope.printGraphycDiassol , data);

        data = {};
        data = {
            opt : "HUMMIN"
        }
        
        client.post("./controllers/climas.php" ,$scope.printGraphycHumMin , data);

        data = {};
        data = {
            opt : "HUMMAX"
        }
        
        client.post("./controllers/climas.php" ,$scope.printGraphycHumMax , data);
    }

    $scope.printGraphycMin = function(r , b){
        b();
        if(r){
            $scope.printData(r , "temp_min");
            // console.log("temp_min")
        }
    }

    $scope.printGraphycMax = function(r , b){
        b();
        if(r){
            $scope.printData(r , "temp_max");
            // console.log("temp_max")
        }
    }

    $scope.printGraphycLluvia = function(r , b){
        b();
        if(r){
            $scope.printData(r , "precp");
            // console.log("precp")
        }
    }

    $scope.printGraphycRadsolar = function(r , b){
        b();
        if(r){
            $scope.printData(r , "radiacion");
            // console.log("radiacion")
        }
    }

    $scope.printGraphycDiassol = function(r , b){
        b();
        if(r){
            $scope.printData(r , "diassol");
            // console.log("diassol")
        }
    }

    $scope.printGraphycHumMin = function(r , b){
        b();
        if(r){
            $scope.printData(r , "humedadmin");
            // console.log("humedadmin")
        }
    }

    $scope.printGraphycHumMax = function(r , b){
        b();
        if(r){
            $scope.printData(r , "humedadmax");
            // console.log("humedadmax")
        }
    }

    $scope.printData = function(r , id){
        if(r){  
            var options =  {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.05
                                }, {
                                    opacity: 0.01
                                }]
                            }
                        },
                        points: {
                            show: false,
                            radius: 3,
                            lineWidth: 1
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderColor: "#eee",
                        borderWidth: 1
                    },
                    colors: ["#d12610", "#37b7f3", "#52e136"],
                    xaxis: {
                        ticks: 11,
                        tickDecimals: 0,
                        tickColor: "#eee",
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 0,
                        tickColor: "#eee",
                        min: (r.min > 2) ? (r.min - 2) : r.min,
                    },
                    legend:{         
                        backgroundOpacity: 0.5,
                        noColumns: 0,  
                        position: 'sw'
                    }
                };

            /*for(var info in r.datos){
                // console.log(r.datos)
                data.push({
                    data : r.datos[info],
                    label: "<label style='margin:0;'><input name='"+id+"-"+info+"' id='"+id+"-"+info+"' checked='checked' class='checkboxs' type='checkbox' style='margin:0;'/>"+((info==0)?'Umbral':info)+"</label>",
                    info : info,
                    lines: {
                        lineWidth: 1,
                    },
                    shadowSize: 0
                });
                // minValue.push(r[info]);
            }
            // console.log(minValue);
            var plot = $.plot($('#'+id), data,options);*/

            var id = id;
            var check_temp_minima = $("#check_temp_minima");
            var check_temp_maxima = $("#check_temp_maxima");
            var check_rad_solar = $("#check_rad_solar");
            var check_lluvia = $("#check_lluvia");
            var check_hum_min = $("#check_hum_min");
            var check_hum_max = $("#check_hum_max");
            var check_dias_sol = $("#check_dias_sol");
            var i = 0;
            var check = 'checked="checked"';
            for(var info in r.datos){
                if(id == "temp_min"){
                    if(Object.keys(r.validate).length > 0 && r.validate[id].indexOf(info) != -1){
                        check = "";
                    } else{
                        check = 'checked="checked"';
                    } 
                    var li = $('<li />').appendTo(check_temp_minima);
                }
                else if (id == "temp_max"){
                    if(Object.keys(r.validate).length > 0 && r.validate[id].indexOf(info) != -1){
                        check = "";
                    } else{
                        check = 'checked="checked"';
                    } 
                    var li = $('<li />').appendTo(check_temp_maxima);
                }
                else if (id == "humedadmax"){
                    if(Object.keys(r.validate).length > 0 && r.validate[id].indexOf(info) != -1){
                        check = "";
                    } else{
                        check = 'checked="checked"';
                    } 
                    var li = $('<li />').appendTo(check_hum_max);
                    $(".generatePdf").css("display" , "");
                }
                else if (id == "humedadmin"){
                    if(Object.keys(r.validate).length > 0 && r.validate[id].indexOf(info) != -1){
                        check = "";
                    } else{
                        check = 'checked="checked"';
                    } 
                    var li = $('<li />').appendTo(check_hum_min);
                }
                else if (id == "diassol"){
                    if(Object.keys(r.validate).length > 0 && r.validate[id].indexOf(info) != -1){
                        check = "";
                    } else{
                        check = 'checked="checked"';
                    } 
                    var li = $('<li />').appendTo(check_dias_sol);
                }
                else if (id == "precp"){
                    if(Object.keys(r.validate).length > 0 && r.validate[id].indexOf(info) != -1){
                        check = "";
                    } else{
                        check = 'checked="checked"';
                    } 
                    var li = $('<li />').appendTo(check_lluvia);
                }
                else if (id == "radiacion"){
                    if(Object.keys(r.validate).length > 0 && r.validate[id].indexOf(info) != -1){
                        check = "";
                    } else{
                        check = 'checked="checked"';
                    } 
                    var li = $('<li />').appendTo(check_rad_solar);
                }

                $('<input name="' + info + '" id="' + info + '" class="labels" data-id="'+id+'" type="checkbox" '+check+' />').appendTo(li);
                $('<label>', {
                    text: (info==0)?'Umbral':info
                }).appendTo(li);
            }

            function plotAccordingToChoices() {
                var data = [];
                if(id == "temp_min"){
                    options.legend.container = $("#label_temp_min");
                    check_temp_minima.find("input:checked").each(function() {
                        var key = this.name;
                        for(var info in r.datos){
                            if (info === key) {
                                data.push({
                                    data : r.datos[info],
                                    label: (info==0)?'Umbral':info,
                                    info : info,
                                    lines: {
                                        lineWidth: 1,
                                    },
                                    shadowSize: 0
                                });
                            }
                        }
                    });
                }
                if(id == "temp_max"){
                    options.legend.container = $("#label_temp_max");
                    check_temp_maxima.find("input:checked").each(function() {
                        var key = this.name;
                        for(var info in r.datos){
                            if (info === key) {
                                data.push({
                                    data : r.datos[info],
                                    label: (info==0)?'Umbral':info,
                                    info : info,
                                    lines: {
                                        lineWidth: 1,
                                    },
                                    shadowSize: 0
                                });
                            }
                        }
                    });
                }
                if(id == "humedadmax"){
                    options.legend.container = $("#label_humedadmax");
                    options.yaxis.ticks = 10;
                    check_hum_max.find("input:checked").each(function() {
                        var key = this.name;
                        for(var info in r.datos){
                            if (info === key) {
                                data.push({
                                    data : r.datos[info],
                                    label: (info==0)?'Umbral':info,
                                    info : info,
                                    lines: {
                                        lineWidth: 1,
                                    },
                                    shadowSize: 0
                                });
                            }
                        }
                    });
                }
                if(id == "humedadmin"){
                    options.legend.container = $("#label_humedadmin");
                    options.yaxis.ticks = 10;
                    check_hum_min.find("input:checked").each(function() {
                        var key = this.name;
                        for(var info in r.datos){
                            if (info === key) {
                                data.push({
                                    data : r.datos[info],
                                    label: (info==0)?'Umbral':info,
                                    info : info,
                                    lines: {
                                        lineWidth: 1,
                                    },
                                    shadowSize: 0
                                });
                            }
                        }
                    });
                }
                if(id == "diassol"){
                    options.legend.container = $("#label_diassol");
                    check_dias_sol.find("input:checked").each(function() {
                        var key = this.name;
                        for(var info in r.datos){
                            if (info === key) {
                                data.push({
                                    data : r.datos[info],
                                    label: (info==0)?'Umbral':info,
                                    info : info,
                                    lines: {
                                        lineWidth: 1,
                                    },
                                    shadowSize: 0
                                });
                            }
                        }
                    });
                }
                if(id == "precp"){
                    options.legend.container = $("#label_precp");
                    check_lluvia.find("input:checked").each(function() {
                        var key = this.name;
                        for(var info in r.datos){
                            if (info === key) {
                                data.push({
                                    data : r.datos[info],
                                    label: (info==0)?'Umbral':info,
                                    info : info,
                                    lines: {
                                        lineWidth: 1,
                                    },
                                    shadowSize: 0
                                });
                            }
                        }
                    });
                }
                if(id == "radiacion"){
                    options.legend.container = $("#label_radiacion");
                    check_rad_solar.find("input:checked").each(function() {
                        var key = this.name;
                        for(var info in r.datos){
                            if (info === key) {
                                data.push({
                                    data : r.datos[info],
                                    label: (info==0)?'Umbral':info,
                                    info : info,
                                    lines: {
                                        lineWidth: 1,
                                    },
                                    shadowSize: 0
                                });
                            }
                        }
                    });
                }
                var plot = $.plot($('#'+id), data,options);
            }

            function showTooltip(x, y, contents) {
                $('<div id="tooltip">' + contents + '</div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y + 5,
                    left: x - 30,
                    border: '1px solid #333',
                    padding: '4px',
                    color: '#fff',
                    'border-radius': '3px',
                    'background-color': '#333',
                    opacity: 0.80
                }).appendTo("body").fadeIn(200);
            }

            var previousPoint = null;
            $('#'+id).bind("plothover", function(event, pos, item) {
                $("#x").text(pos.x.toFixed(0));
                $("#y").text(pos.y.toFixed(2));

                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;

                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(2),
                            y = item.datapoint[1].toFixed(2);

                        showTooltip(item.pageX, item.pageY,"Año " + item.series.info + " Semana " + x + " Valor " + y);
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            
            });

            plotAccordingToChoices();
            check_temp_minima.find("input").change(plotAccordingToChoices);
            check_temp_maxima.find("input").change(plotAccordingToChoices);
            check_rad_solar.find("input").change(plotAccordingToChoices);
            check_lluvia.find("input").change(plotAccordingToChoices);
            check_hum_min.find("input").change(plotAccordingToChoices);
            check_hum_max.find("input").change(plotAccordingToChoices);
            check_dias_sol.find("input").change(plotAccordingToChoices);

                $(".miniature").on("click" ,".labels", function(event){
                    var id = $(this).attr("data-id");
                    var key = [];
                    event.stopImmediatePropagation();
                    if(id == "temp_min"){
                        check_temp_minima.find("input:checked").each(function() {
                            key.push(this.name);
                        });
                    }
                    if(id == "temp_max"){
                        check_temp_maxima.find("input:checked").each(function() {
                            key.push(this.name);
                        });
                    }
                    if(id == "humedadmax"){
                        check_hum_max.find("input:checked").each(function() {
                            key.push(this.name);
                        });
                    }
                    if(id == "humedadmin"){
                        check_hum_min.find("input:checked").each(function() {
                            key.push(this.name);
                        });
                    }
                    if(id == "diassol"){
                        check_dias_sol.find("input:checked").each(function() {
                            key.push(this.name);
                        });
                    }
                    if(id == "precp"){
                        check_lluvia.find("input:checked").each(function() {
                            key.push(this.name);
                        });
                    }
                    if(id == "radiacion"){
                        check_rad_solar.find("input:checked").each(function() {
                            key.push(this.name);
                        });
                    }

                    $scope.saveLabel(id , key);
                })
        }   
    }

    $scope.saveLabel = function(type , labels){
        if(labels.length > 0){
            var data = {
                opt : "SAVE_LABELS",
                type : type,
                labels : labels
            }
            client.post("./controllers/climas.php" ,function(r,b){b();} , data);
        }
    }

    // $scope.grafica = function(id){
    //     if ($('#'+id).size() != 1) {
    //         return;
    //     }

    //     function randValue() {
    //         return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
    //     }
    //     var pageviews = [
    //         [1, randValue()],
    //         [2, randValue()],
    //         [3, 2 + randValue()],
    //         [4, 3 + randValue()],
    //         [5, 5 + randValue()],
    //         [6, 10 + randValue()],
    //         [7, 15 + randValue()],
    //         [8, 20 + randValue()],
    //         [9, 25 + randValue()],
    //         [10, 30 + randValue()],
    //         [11, 35 + randValue()],
    //         [12, 25 + randValue()],
    //         [13, 15 + randValue()],
    //         [14, 20 + randValue()],
    //         [15, 45 + randValue()],
    //         [16, 50 + randValue()],
    //         [17, 65 + randValue()],
    //         [18, 70 + randValue()],
    //         [19, 85 + randValue()],
    //         [20, 80 + randValue()],
    //         [21, 75 + randValue()],
    //         [22, 80 + randValue()],
    //         [23, 75 + randValue()],
    //         [24, 70 + randValue()],
    //         [25, 65 + randValue()],
    //         [26, 75 + randValue()],
    //         [27, 80 + randValue()],
    //         [28, 85 + randValue()],
    //         [29, 90 + randValue()],
    //         [30, 95 + randValue()]
    //     ];
    //     var visitors = [
    //         [1, randValue() - 5],
    //         [2, randValue() - 5],
    //         [3, randValue() - 5],
    //         [4, 6 + randValue()],
    //         [5, 5 + randValue()],
    //         [6, 20 + randValue()],
    //         [7, 25 + randValue()],
    //         [8, 36 + randValue()],
    //         [9, 26 + randValue()],
    //         [10, 38 + randValue()],
    //         [11, 39 + randValue()],
    //         [12, 50 + randValue()],
    //         [13, 51 + randValue()],
    //         [14, 12 + randValue()],
    //         [15, 13 + randValue()],
    //         [16, 14 + randValue()],
    //         [17, 15 + randValue()],
    //         [18, 15 + randValue()],
    //         [19, 16 + randValue()],
    //         [20, 17 + randValue()],
    //         [21, 18 + randValue()],
    //         [22, 19 + randValue()],
    //         [23, 20 + randValue()],
    //         [24, 21 + randValue()],
    //         [25, 14 + randValue()],
    //         [26, 24 + randValue()],
    //         [27, 25 + randValue()],
    //         [28, 26 + randValue()],
    //         [29, 27 + randValue()],
    //         [30, 31 + randValue()]
    //     ];

    //     var plot = $.plot($('#'+id), [{
    //         data: pageviews,
    //         label: "Unique Visits",
    //         lines: {
    //             lineWidth: 1,
    //         },
    //         shadowSize: 0

    //     }, {
    //         data: visitors,
    //         label: "Page Views",
    //         lines: {
    //             lineWidth: 1,
    //         },
    //         shadowSize: 0
    //     }], {
    //         series: {
    //             lines: {
    //                 show: true,
    //                 lineWidth: 2,
    //                 fill: true,
    //                 fillColor: {
    //                     colors: [{
    //                         opacity: 0.05
    //                     }, {
    //                         opacity: 0.01
    //                     }]
    //                 }
    //             },
    //             points: {
    //                 show: true,
    //                 radius: 3,
    //                 lineWidth: 1
    //             },
    //             shadowSize: 2
    //         },
    //         grid: {
    //             hoverable: true,
    //             clickable: true,
    //             tickColor: "#eee",
    //             borderColor: "#eee",
    //             borderWidth: 1
    //         },
    //         colors: ["#d12610", "#37b7f3", "#52e136"],
    //         xaxis: {
    //             ticks: 11,
    //             tickDecimals: 0,
    //             tickColor: "#eee",
    //         },
    //         yaxis: {
    //             ticks: 11,
    //             tickDecimals: 0,
    //             tickColor: "#eee",
    //         }
    //     });


    //     function showTooltip(x, y, contents) {
    //         $('<div id="tooltip">' + contents + '</div>').css({
    //             position: 'absolute',
    //             display: 'none',
    //             top: y + 5,
    //             left: x + 15,
    //             border: '1px solid #333',
    //             padding: '4px',
    //             color: '#fff',
    //             'border-radius': '3px',
    //             'background-color': '#333',
    //             opacity: 0.80
    //         }).appendTo("body").fadeIn(200);
    //     }

    //     var previousPoint = null;
    //     $('#'+id).bind("plothover", function(event, pos, item) {
    //         $("#x").text(pos.x.toFixed(2));
    //         $("#y").text(pos.y.toFixed(2));

    //         if (item) {
    //             if (previousPoint != item.dataIndex) {
    //                 previousPoint = item.dataIndex;

    //                 $("#tooltip").remove();
    //                 var x = item.datapoint[0].toFixed(2),
    //                     y = item.datapoint[1].toFixed(2);

    //                 showTooltip(item.pageX, item.pageY, item.series.label + " of " + x + " = " + y);
    //             }
    //         } else {
    //             $("#tooltip").remove();
    //             previousPoint = null;
    //         }
    //     });
    // }


}]);
