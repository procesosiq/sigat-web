app.service('request', function($http){
    var service = {};
    var options = {
        url: "./controllers/index.php",
        method: "POST"
    }; 

    service.tags = function (callback, params) {
        options.data = {
            accion: "ClimaDiarioOrodelti.tags",
            params : params
        }
        $http(options).then((r) => {
            callback(r.data)
        });
    }
    service.datatable = function(callback, params) {
        options.data = {
            accion: "ClimaDiarioOrodelti.detalle",
            params : params
        }
        $http(options).then((r) => {
            callback(r.data)
        })
    }
    service.horasluz = function(callback, params) {
        options.data = {
            accion: "ClimaDiarioOrodelti.horasLuz",
            params : params
        }
        $http(options)
            .then((r) => {
                callback(r.data || {data: []})
            })
            .catch(err => callback({data : []}))
    }
    service.graficasViento = function(callback, params){
        options.data = {
            accion: "ClimaDiarioOrodelti.graficasViento",
            params : params
        }
        $http(options)
            .then(r => callback(r.data))
    }
    service.last = function(callback, params){
        options.data = {
            accion: "ClimaDiarioOrodelti.last",
            params : params
        }
        $http(options).then(r => callback(r.data))
    }
    service.estaciones = function(callback, params){
        options.data = {
            accion: "ClimaDiarioOrodelti.estaciones",
            params : params
        }
        $http(options).then(r => callback(r.data))
    }

    return service;
});

app.controller('controller', ['$scope', 'request', function ($scope, $request) {

    $scope.fecha = moment().format('YYYY-MM-DD')
    $scope.filters = {
        hora_inicio : moment().format('HH:mm'),
        hora_fin : moment().format('HH:mm'),
        luz : 400,
        fecha_inicial : moment().format('YYYY-MM-DD'),
        fecha_final : moment().format('YYYY-MM-DD'),
    }

    $scope.StartEndDateDirectives = {
        startDate : moment(),
        endDate :moment(),
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
            $scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;

            //$scope.difDias = moment($scope.filters.fecha_inicial).isBefore($scope.filters.fecha_final);
        }
        $scope.init()
    }

    $scope.loadTags = () => {
        load.block('indicadores')
        $request.tags((r) => {
            $scope.tags = r.tags
            load.unblock('indicadores')
        }, $scope.filters)
    }
    $scope.loadDataTable = () => {
        load.block('datatable')
        $request.datatable((r) => {
            $scope.datatable = r.data
            load.unblock('datatable')
        }, $scope.filters)
    }
    $scope.loadGraficasViento = () => {
        $request.graficasViento(r => {
            if(r){
                printGraficaDireccionViento(r.direccion_viento)
                printGraficaVelocidadViento(r.velocidad_viento)
            }
        }, $scope.filters)
    }
    $scope.loadHorasLuz = () => {
        load.block('grafica_horas_luz')
        $request.horasluz((r) => {
            printGraficaHorasLuz(r.data)
            load.unblock('grafica_horas_luz')
        }, $scope.filters)
    }

    printGraficaHorasLuz = (data) => {
        if(data.data.length == 0){
            var props = {
                series: [0],
                legend: ['NO HAY DATOS'],
                umbral: null,
                id: "grafica_horas_luz",
                type : 'line',
                zoom : false,
                legendBottom : true,
                min : data.min
            }
            ReactDOM.render(React.createElement(Historica, props), document.getElementById('grafica_horas_luz'));
        }else{
            var props = {
                series: data.data,
                legend: data.legend,
                umbral: null,
                id: "grafica_horas_luz",
                type : 'line',
                zoom : false,
                legendBottom : true,
                min : data.min
            }
            ReactDOM.render(React.createElement(Historica, props), document.getElementById('grafica_horas_luz'));
        }
    }
    printGraficaDireccionViento = (data) => {
        var props = {
            //titulo : 'Viento',
            //subtitulo : 'Dirección',
            data: [
                {
                    label : 'N',
                    value : data['N'] || 0
                },
                {
                    label : 'NE',
                    value : data['NE'] || 0
                },
                {
                    label : 'E',
                    value : data['E'] || 0
                },
                {
                    label : 'SE',
                    value : data['SE'] || 0
                },
                {
                    label : 'S',
                    value : data['S'] || 0
                },
                {
                    label : 'SW',
                    value : data['SW'] || 0
                },
                {
                    label : 'W',
                    value : data['W'] || 0
                },
                {
                    label  : 'NW',
                    value : data['NW'] || 0
                }
            ],
            id: "grafica-direccion-viento",
        }
        ReactDOM.render(React.createElement(Brujula, props), document.getElementById('grafica-direccion-viento'));
    }
    printGraficaVelocidadViento = (kmh) => {
        var props = {
            titulo : 'Viento',
            subtitulo : 'Velocidad',
            unidad : 'km/h',
            value : kmh,
            id: "grafica-velocidad-viento",
        }
        ReactDOM.render(React.createElement(Medidor, props), document.getElementById('grafica-velocidad-viento'));
    }

    $scope.init = () => {
        $request.estaciones((r) => {
            var keys = r.estaciones.map((row) => row.id)
            if(keys.indexOf($scope.filters.estacion) == -1){
                $scope.filters.estacion = keys[0]
            }
            $scope.estaciones = r.estaciones

            $scope.loadTags()
            $scope.loadDataTable()
            $scope.loadHorasLuz()
            $scope.loadGraficasViento()
        }, $scope.filters)
    }
    $scope.last = () => {
        $request.last((r) => {
            $scope.fecha = r.fecha
            $scope.filters.fecha_inicial = r.fecha
            $scope.filters.fecha_final = r.fecha
            $scope.filters.hora_inicio = r.hora
            $scope.filters.hora_fin = r.hora
            $("date-picker").html(`${r.fecha} - ${r.fecha}`)

            initPickers()
            $scope.init()
        })
    }
    $scope.last()


    initPickers = () => {
        $('#timepicker').timepicker({
            minuteStep: 1,
            appendWidgetTo: 'body',
            showMeridian: false,
            defaultTime: false
        });
        $('#timepicker').timepicker('setTime', $scope.filters.hora_inicio);
        $('#timepicker').timepicker().on('changeTime.timepicker', function(e) {
            setTimeout(() => {
                $scope.isChangeHour = true
                $scope.$apply()
            }, 100)
    
            let inicio = e.time.value.split(':')
            let fin = $scope.filters.hora_fin.split(':')
    
            let inicio_time = moment().hour(inicio[0]).minute(inicio[1])
            let fin_time = moment().hour(fin[0]).minute(inicio[1])
            if(inicio_time.isBefore(fin_time) || inicio_time.isSame(fin_time)){
                $scope.filters.hora_inicio = e.time.value
            }else{
                alert(`El tiempo debe ser menor a ${$scope.filters.hora_fin}`)
            }
        });
    
        $('#timepicker2').timepicker({
            minuteStep: 1,
            appendWidgetTo: 'body',
            showMeridian: false,
            defaultTime: false
        });
        $('#timepicker2').timepicker('setTime', $scope.filters.hora_fin);
        $('#timepicker2').timepicker().on('changeTime.timepicker', function(e) {
            setTimeout(() => {
                $scope.isChangeHour = true
                $scope.$apply()
            }, 100)
            
            let inicio = $scope.filters.hora_inicio.split(':')
            let fin = e.time.value.split(':')
    
            let inicio_time = moment().hour(inicio[0]).minute(inicio[1])
            let fin_time = moment().hour(fin[0]).minute(inicio[1])
            if(inicio_time.isBefore(fin_time) || inicio.isSame(fin_time)){
                $scope.filters.hora_fin = e.time.value
            }else{
                alert(`El tiempo debe ser mayor a ${$scope.filters.hora_inicio}`)
            }
        });
    }

}])