moment.lang('es')

app.service('costos', function($http){
    let service = {};
    let options = {
        url: "./controllers/index.php",
        method: "POST"
    };

    service.index = function(callback, params){
        let option = options;
        option.data = {
            accion: "CostosOrodelti.index" , 
            params : params
        };
        load.block()
        $http(option).then((r) => {
            load.unblock()
            callback(r.data)
        });
    }

    service.productos =  function(callback, params){
        let option = options;
        option.data = {
            accion: "CostosOrodelti.productos" , 
            params : params
        };
        load.block()
        $http(option).then((r) => {
            load.unblock()
            callback(r.data)
        });
    }

    service.getProducto = function(callback, params){
        let option = options;
        option.data = {
            accion: "CostosOrodelti.getProducto",
            params : params
        };
        load.block()
        $http(option).then((r) => {
            load.unblock()
            callback(r.data)
        });
    }

    service.updateProducto = function(callback, params){
        let option = options;
        option.data = {
            accion: "CostosOrodelti.updateProductoTemporal",
            params : params
        };
        load.block()
        $http(option).then((r) => {
            load.unblock()
            callback(r.data)
        });
    }

    service.addProducto = function(callback, params){
        let option = options;
        option.data = {
            accion: "CostosOrodelti.addProductoTemporal",
            params : params
        };
        load.block()
        $http(option).then((r) => {
            load.unblock()
            callback(r.data)
        });
    }

    service.getPrecioProducto = function(callback, params){
        let option = options
        option.data = {
            accion : "CostosOrodelti.getPrecioProducto",
            params : params
        }
        load.block()
        $http(option).then((r) => {
            load.unblock()
            callback(r.data)
        });
    }

    service.updateCiclo = function(callback, params){
        let option = options
        option.data = {
            accion : "CostosOrodelti.updateCicloTemporal",
            params : params
        }
        load.block()
        $http(option).then((r) => {
            load.unblock()
            callback(r.data)
        });
    }

    service.agregarSigatoka = function(callback, params){
        let option = options
        option.data = {
            accion : "CostosOrodelti.agregarSigatoka",
            params : params
        }
        load.block()
        $http(option).then((r) => {
            load.unblock()
            callback(r.data)
        });
    }

    service.agregarFoliar = function(callback, params){
        let option = options
        option.data = {
            accion : "CostosOrodelti.agregarFoliar",
            params : params
        }
        load.block()
        $http(option).then((r) => {
            load.unblock()
            callback(r.data)
        });
    }

    service.agregarParcial = function(callback, params){
        let option = options
        option.data = {
            accion : "CostosOrodelti.agregarParcial",
            params : params
        }
        load.block()
        $http(option).then((r) => {
            load.unblock()
            callback(r.data)
        });
    }

    service.deleteRow = function(callback, params){
        let option = options
        option.data = {
            accion : "CostosOrodelti.deleteRowTemporal",
            params : params
        }
        load.block()
        $http(option).then((r) => {
            load.unblock()
            callback(r.data)
        });
    }

    service.aproveeRow = function(params){
        return new Promise((resolve) => {
            let option = options
            option.data = {
                accion : "CostosOrodelti.aproveeRow",
                params : params
            }
            load.block()
            $http(option).then((r) => {
                load.unblock()
                resolve(r.data)
            });
        })
    }

    return service;
});

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        let sum = 0;
        angular.forEach(data,function(value){
            if(value.por_aprobar && value.por_aprobar[key] != "" && value.por_aprobar[key] != undefined && parseFloat(value.por_aprobar[key])){
                sum = sum + parseFloat(value.por_aprobar[key], 10);
            } else if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('maxOfValue', function () {
    return function (data, key, key2 = null) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;

        let max = null;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                if(key2){
                    if(value[key][key2]){
                        if(max == null || max < parseFloat(value[key][key2])) max = parseFloat(value[key][key2])
                    }
                }else{
                    if(max == null || max < parseFloat(value[key])) max = parseFloat(value[key])
                }
            }
        });
        return max;
    }
})

app.filter('capitalize', function() {
    return function(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

app.filter('num', function() {
    return function(input) {
        if(input)
            return new Number(input)
        else
            return ''
    }
});

app.filter('formatDate', function() {
    return function(input) {
        if(input){
            let dia = moment(input).format('DD')+'-'
            let mes = moment(input).format('MMMM').substr(0,3)
            return dia + (mes.charAt(0).toUpperCase() + mes.substr(1).toLowerCase())
        }else
            return ''
    }
});

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        let sum = 0;
        let count = 0;
        angular.forEach(data,function(value){
            if(value.por_aprobar && value.por_aprobar[key] != "" && value.por_aprobar[key] != undefined && parseFloat(value.por_aprobar[key])){
                sum = sum + parseFloat(value.por_aprobar[key], 10);
                count++;
            } else if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.constant('keyCodes', {
        esc: 27,
        space: 32,
        enter: 13,
        tab: 9,
        backspace: 8,
        shift: 16,
        ctrl: 17,
        alt: 18,
        capslock: 20,
        numlock: 144
    })
    .directive('keyBind', ['keyCodes', function (keyCodes) {
        function map(obj) {
            var mapped = {};
            for (var key in obj) {
                var action = obj[key];
                if (keyCodes.hasOwnProperty(key)) {
                    mapped[keyCodes[key]] = action;
                }
            }
            return mapped;
        }

        return function (scope, element, attrs) {
            var bindings = map(scope.$eval(attrs.keyBind));
            element.bind("keydown", function (event) {
                if (bindings.hasOwnProperty(event.which)) {
                    scope.$apply(function() {
                        scope.$eval(bindings[event.which]);
                    });
                }
            });
        };
    }]);

app.controller('analisis', ['$scope', 'costos', function ($scope, $request) {
    
    $scope.productoSelected = {}
    $scope.originalProd = {}
    $scope.editing = false
    $scope.originalRow = null
    $scope.filters = {
        year : "<?= isset($_GET['year']) ? $_GET['year'] : date('Y') ?>",
        tipoHectarea : 'FUMIGACION',
        finca : '<?= isset($_GET["finca"]) ? $_GET["finca"] : "SAN ALBERTO 1" ?>'
    }
    $scope.saving = false
    $scope.sigatoka = []
    $scope.foliar = []
    $scope.parcial = []

    $scope.anios = [
        2019,
        2018,
        2017
    ]

    $scope.tipoHectarea = {
        'FUMIGACION' : 'FUMIGACIÓN',
        'PRODUCCION' : 'PRODUCCIÓN',
        'NETA' : 'NETA',
        'BANANO' : 'BANANO'
    }

    $scope.productos = () => {
        $request.productos((r) => {
            $scope.proveedores = r.proveedores
            $scope.productos = r.productos
        })
    }

    $scope.init = () => {
        $request.index((r) => {
            $scope.sigatoka = r.sigatoka
            $scope.parcial = r.parcial
            $scope.foliar = r.foliar

            setTimeout(() => {
                $('[data-toggle="tooltip"]').tooltip()
            }, 200)
        }, $scope.filters)
    }
    
    $scope.deleteRow = (row) => {
        let _continue = true
        if(row.productos.length > 0){
            if(!confirm('¿Seguro de eliminar esta fila?')){
                _continue = false
            }
        }

        if(_continue){
            $request.deleteRow((r) => {
                if(r.status === 200){
                    toastr.success('Por aprobar eliminado')
                    $scope.init()
                }
            }, row)
        }
    }

    $scope.edit = (row, field) => {
        if(row.por_borrar){
            return
        }
        if(!$scope.editing){
            if(['ciclo', 'semana', 'hectareas_fumigacion', 'atraso', 'total', 'ha_oper'].indexOf(field) > -1){
                row.por_aprobar[field] = Number(row.por_aprobar[field])
            }
            if(field === 'programa'){
                if((row.por_aprobar[field] || '').split('-').length > 1){
                    toastr.error('No se puede editar')
                    return;
                }
            }
            row.editing = field
            $scope.editing = true
            $scope.originalRow = angular.copy(row)
            setTimeout(() => {
                document.getElementById("editing").focus()
            }, 150)
        }else{
            toastr.error(`Favor de terminar de editar primero`)
        }
    }

    $scope.editProducto = (row, prod, index) => {
        if(row.por_borrar){
            return
        }
        if(!row.productos_tmp){
            row.productos_tmp = angular.copy(row.productos)
            prod = row.productos_tmp[index]
        }

        if(prod.nombre_comercial != ''){
            if(!$scope.editing){
                $("#edit-producto").modal('show')
                $scope.productoSelected = angular.copy(prod)
                $scope.originalProd = angular.copy(prod)
                $scope.originalRow = angular.copy(row)
                $scope.selectProducto()
            }else{
                toastr.error(`Favor de terminar de editar primero`)
            }
        }
    }

    $scope.openAddProducto = (row) => {
        $scope.productoAdd = {}
        $scope.originalRow = angular.copy(row)
        $("#agregar-producto").modal('show')
    }

    $scope.save = (row) => {
        if(row.por_aprobar[row.editing] != $scope.originalRow[row.editing]){
            //change
            $request.updateCiclo((r) => {
                if(r.status === 200){
                    $scope.originalRow = null
                    $scope.clear(row)
                    $scope.init()
                }
            }, row)
        }else{
            $scope.originalRow = null
            $scope.clear(row)
        }
        
    }

    $scope.saveProducto = () => {
        if(confirm('¿Estas seguro de realizar estos cambios? la información sera reemplazada')){
            $scope.saving = true
            if($scope.productoSelected.id_producto > 0 && $scope.productoSelected.id_proveedor > 0 && $scope.productoSelected.dosis > 0){
                $request.updateProducto((r) => {
                    load.unblock()
                    if(r.status === 200){
                        alert('Guardado con éxito', '', 'success', () => {
                            $("#edit-producto").modal('hide')
                            $scope.saving = false
                            $scope.init()
                        })
                    }else{
                        $scope.saving = false
                    }
                }, {
                    ciclo : $scope.originalRow,
                    productoNuevo : $scope.productoSelected,
                    productoViejo : $scope.originalProd
                })
            }else{
                toastr.error('Complete los campos')
            }
        }
    }

    $scope.clear = (row) => {
        if($scope.editing){
            if($scope.originalRow){
                // devolver valor
                Object.assign(row, angular.copy($scope.originalRow))
            }
            $scope.editing = false
            row.editing = null
        }
    }

    $scope.selectProducto = () => {
        $scope.productoSelected.producto = $scope.productos.filter((p) => p.id == $scope.productoSelected.id_producto)[0]
        $request.getPrecioProducto((r) => {
            $scope.productoAdd.precio = r.precio

            if($scope.productoSelected)
                $scope.productoSelected.precio = r.precio

            if($scope.productoSelected.id_producto != $scope.originalProd.id_producto){
                $scope.productoSelected.dosis = ''
                $("#select-dosis").val('')
            }else{
                $("#select-dosis").val(Number($scope.productoSelected.dosis))
            }
        }, {
            fecha : $scope.originalRow.por_aprobar.fecha_real || $scope.originalRow.fecha_real,
            id_producto : $scope.productoSelected.id_producto
        })
    }

    $scope.selectProductoAdd = () => {
        $scope.productoAdd.producto = $scope.productos.filter((p) => p.id == $scope.productoAdd.id_producto)[0]
        $request.getPrecioProducto((r) => {
            $scope.productoAdd.precio = r.precio
            if($scope.productoAdd.id_producto != $scope.originalProd.id_producto){
                $scope.productoAdd.dosis = ''
                $("#select-add-dosis").val('')
            }else{
                $("#select-add-dosis").val(Number($scope.productoAdd.dosis))
            }
            $scope.$apply()
        }, {
            fecha : $scope.originalRow.por_aprobar.fecha_real || $scope.originalRow.fecha_real,
            id_producto : $scope.productoAdd.id_producto,
            id_proveedor : $scope.productoAdd.id_proveedor
        })
    }

    $scope.addProducto = () => {
        if(confirm('¿Estas seguro de agregar este producto?')){
            $scope.saving = true
            if($scope.productoAdd.id_producto > 0 && $scope.productoAdd.id_proveedor > 0 && $scope.productoAdd.dosis > 0){
                $request.addProducto((r) => {
                    if(r.status === 200){
                        alert('Agregado con éxito', '', 'success', () => {
                            $("#agregar-producto").modal('hide')
                            $scope.saving = false
                            $scope.init()
                        })
                    }else{
                        $scope.saving = false
                        alert('Ocurrio algo inesperado')
                    }
                }, {
                    ciclo : $scope.originalRow,
                    producto : $scope.productoAdd
                })
            }else{
                toastr.error('Completa los campos')
            }
        }
    }

    $scope.multiplicar = (val1, val2) => {
        let val = parseFloat(val1) * parseFloat(val2)
        return val
    }

    $scope.saveCiclo = () => {

    }

    $scope.aproveeRow = async (row) => {
        if(confirm('¿Esta seguro de hacer esta acción?')){
            let r = await $request.aproveeRow(row)
            if(r.status === 200){
                toastr.success('Guardado con éxito')
                $scope.init()
            }else{
                toastr.error('Algo inesperado paso')
            }
        }
    }

    $scope.productoChange = (row, index) => {
        if(!row.productos[index] && row.productos_tmp && row.productos_tmp[index])
            return true

        if(row.productos[index] && row.productos_tmp && row.productos_tmp[index])
            if(row.productos[index].id_producto != row.productos_tmp[index].id_producto)
                return true

        return false
    }

    /****** NUEVA FILA */
    $scope.newRowSigatoka = () => {
        $request.agregarSigatoka((r) => {
            $scope.init()
        }, {
            finca : $scope.filters.finca,
            anio : $scope.filters.year
        })
    }
    $scope.newRowFoliar = () => {
        $request.agregarFoliar((r) => {
            $scope.init()
        }, {
            finca : $scope.filters.finca,
            anio : $scope.filters.year
        })
    }
    $scope.newRowParcial = () => {
        $request.agregarParcial((r) => {
            $scope.init()
        }, {
            finca : $scope.filters.finca,
            anio : $scope.filters.year
        })
    }
    /****** NUEVA FILA */

    $scope.init()
    $scope.productos()
}])