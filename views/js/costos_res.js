app.service('costos', function($http){
    var service = {};
    var options = {
        url: "./controllers/index.php",
        method: "POST"
    };

    service.index = function(callback, params){
        var option = options;
        option.data = {
            accion: "CostosOrodelti.index" , 
            params : params
        };
        load.block()
        $http(option).then((r) => {
            load.unblock()
            callback(r.data)
        });
    }

    return service;
});

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        if(["peso", "manos", "calibre", "dedos"].indexOf(key) > -1){
            sum = sum / count;
        }
        return sum;
    }
})

app.filter('capitalize', function() {
    return function(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

app.filter('num', function() {
    return function(input) {
        if(input)
            return new Number(input)
        else
            return ''
    }
});

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.controller('analisis', ['$scope', 'costos', function ($scope, $request) {
    $scope.filters = {
        year : moment().year(),
        finca : 'SAN ALBERTO 1'
    }

    $scope.anios = [
        2018,
        2017
    ]

    $scope.init = () => {
        $("#anio").val('2018')
        $request.index((r) => {
            $scope.sigatoka = r.sigatoka
            $scope.parcial = r.parcial
            $scope.foliar = r.foliar
        }, $scope.filters)
    }

    $scope.init()
}])