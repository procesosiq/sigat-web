app.service('ciclos', function ($http) {
    var service = {};
    var options = {
        url: "./controllers/index.php",
        method: "POST"
    };

    service.listado = function () {
        var option = options;
        option.data = {
            accion: "Cycles"
        };
        return $http(option);
    };

    service.fincas = function () {
        var option = options;
        option.data = {
            accion: "Cycles.getFincas"
        };
        return $http(option);
    };

    service.pilotos = function () {
        var option = options;
        option.data = {
            accion: "Cycles.getPilotos"
        };
        return $http(option);
    };

    // service.tipoProductos = function () {
    //     var option = options;
    //     option.data = {
    //         accion: "Cycles.getTipoProductos"
    //     };
    //     return $http(option);
    // };

    service.fumigadoras = function () {
        var option = options;
        option.data = {
            accion: "Cycles.getFumigadoras"
        };
        return $http(option);
    };

    service.fungicidas = function () {
        var option = options;
        option.data = {
            accion: "Cycles.getFungicidas"
        };
        return $http(option);
    };

    service.emulsificantes = function () {
        var option = options;
        option.data = {
            accion: "Cycles.getEmulsificantes"
        };
        return $http(option);
    };

    service.aceites = function () {
        var option = options;
        option.data = {
            accion: "Cycles.getAceites"
        };
        return $http(option);
    };

    service.foliares = function () {
        var option = options;
        option.data = {
            accion: "Cycles.getFoliares"
        };
        return $http(option);
    };

    // 02/05/2017 - TAG: CYCLES METHOD
    service.show = function (id_cycle) {
        var option = options;
        option.data = {
            accion: "Cycles.show",
            id_cycle: id_cycle
        };
        return $http(option);
    };

    service.create = function (cycle) {
        var option = options;
        cycle.accion = "Cycles.create";
        option.data = cycle;
        return $http(option);
    };

    service.update = function (cycle) {
        var option = options;
        cycle.accion = "Cycles.update";
        option.data = cycle;
        return $http(option);
    };
    // 02/05/2017 - TAG: CYCLES METHOD

    return service;
});
app.controller('cycles', ['$scope', '$http', '$interval', '$controller' , 'ciclos', function ($scope, $http, $interval, $controlle , ciclos) {

    // 02/05/2017 - TAG: ARRAY FOR SELECT
    $scope.fincas = [];
    $scope.pilotos = [];
    $scope.foliares = [];
    $scope.aceites = [];
    $scope.fungicidas = [];
    $scope.fumigadoras = [];
    $scope.emulsificantes = [];
    $scope.tipo_ciclos = ['CICLO','PARCIAL'];
    // 02/05/2017 - TAG: ARRAY FOR SELECT

    // 02/05/2017 - TAG: MODEL FOR CYCLE
    $scope.cycle = {
        id_cycle : "<?=$_GET['id']?>",
        codigo : "<?=$_GET['id']?>",
        id_hacienda : "",
        fecha : "",
        id_sector : "",
        sector : "",
        hectares_fimigacion : "",
        notificacion : "",
        programa : "",
        tipo_ciclo : "",
        num_ciclo : "",
        id_fumigadora : "",
        fumigadora : "",
        id_piloto : "",
        piloto : "",
        id_placa_avion : "",
        placa_avion : "",
        hora_salida : "",
        hora_llegada : "",
        id_fungicida : "",
        cantidad : "",
        dosis : "",
        id_fungicida2 : "",
        cantidad_2 : "",
        dosis_2 : "",
        catidad_agua : "",
        dosis_agua : "",
        id_aceite : "",
        aceite : "",
        cantidad_aceite : "",
        dosis_aceite : "",
        id_emulsificante : "",
        emulsificante : "",
        cantidad_emulsificante : "",
        dosis_emulsificante : "",
        id_foliar_1 : "",
        foliar_1 : "",
        cantidad_foliar_1 : "",
        dosis_foliar_1 : "",
        id_foliar_2 : "",
        foliar_2 : "",
        cantidad_foliar_2 : "",
        dosis_foliar_2 : "",
        id_foliar_3 : "",
        foliar_3 : "",
        cantidad_foliar_3 : "",
        dosis_foliar_3 : "",
    }
    // 02/05/2017 - TAG: MODEL FOR CYCLE

    $scope.oldCycle = angular.copy($scope.cycle);

    $scope.proccess = function (r) {
        if (r.hasOwnProperty("data")) {
            return r.data;
        }
    };

    // 02/05/2017 - TAG: ADD METHODS CYCLES
    $scope.fincas = function () {
        ciclos.fincas().then(function (data) {
            return $scope.fincas = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.pilotos = function () {
        ciclos.pilotos().then(function (data) {
            return $scope.pilotos = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.fumigadoras = function () {
        ciclos.fumigadoras().then(function (data) {
            return $scope.fumigadoras = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.fungicidas = function () {
        ciclos.fungicidas().then(function (data) {
            return $scope.fungicidas = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.aceites = function () {
        ciclos.aceites().then(function (data) {
            return $scope.aceites = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.foliares = function () {
        ciclos.foliares().then(function (data) {
            return $scope.foliares = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.emulsificantes = function () {
        ciclos.emulsificantes().then(function (data) {
            return $scope.emulsificantes = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.show = function () {
        ciclos.show($scope.cycle.id_cycle).then(function (data) {
            var response = $scope.proccess(data);
            if (response.hasOwnProperty("success") && response.success == 200) {
                $scope.cycle = response.data;
            }
        }).catch(function (error) {
            return console.log(error);
        });
    };
    // 02/05/2017 - TAG: ADD METHODS CYCLES


    $scope.index = function(){
        $scope.fincas();
        $scope.fungicidas();
        $scope.fumigadoras();
        $scope.emulsificantes();
        $scope.foliares();
        $scope.pilotos();
        $scope.show();
        if ($('.date-picker').length > 0) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                autoclose: true
            });
        }
    }

    $scope.cancel = function(){
        document.location.href = "cycleList";
    }

    $scope.save = function(){
        ciclos.update($scope.cycle).then(function (data) {
            return $scope.success(data);
        }).catch(function (error) {
            return console.log(error);
        });
    }

    $scope.success = function (r) {
        var response = $scope.proccess(r);
        $scope.cycle.id_cycle = response.data;
        $scope.cycle.codigo = response.data;
        alert("Registro registrado/modificado con el ID " + $scope.cycle.id_cycle, "Ciclos", "success");
    };

}]);