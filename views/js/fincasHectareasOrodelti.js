app.service('request', ['$http', ($http) => {
    let service = {}
    let conf = 'controllers/index.php?accion='

    service.index = (callback, params = {}) => {
        load.block()
        $http.post(`${conf}ConfigFincasOrodelti.index`, params).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.save = (params = {}) => {
        return new Promise((resolve, reject) => {
            $http.post(`${conf}ConfigFincasOrodelti.saveHa`, params).then((r) => {
                resolve(r.data)
            })
        })
    }

    return service
}])

app.controller('controller', ['$scope','request', function($scope,$request){

    $scope.years = []
    $scope.filters = {
        anio : moment().year()
    }
    for(let anio = 2016; anio <= $scope.filters.anio; anio++){
        $scope.years.push(anio)
    }

    save = (id, campo, valor, id_tipo) => {
        if(id > 0 && id_tipo != ''){
            let tipo = id_tipo.replace('table-', '').toUpperCase()

            swal({
                title: "Guardando",
                text: "Espere un momento",
            }).then(() => {
                return $request.save({
                    id_finca : id, 
                    semana : campo.replace('sem_', ''), 
                    hectareas : valor,
                    anio : $scope.filters.anio,
                    tipo
                })
            })
            .then((r) => {
                if(r.status === 200){
                    swal({
                        title: "Guardado",
                        text: 'Guardado con éxito',
                        icon: 'success',
                    })
                    .then(() => {
                        $scope.init()
                    });
                }else{
                    return swal("No a sido posible guardar, intente mas tarde");
                }
            })
        }
    }
    
    $scope.init = () => {
        $request.index((r) => {
            renderTable('table-fumigacion', r.fumigacion, r.semanas)
            renderTable('table-produccion', r.produccion, r.semanas)
            renderTable('table-neta', r.neta, r.semanas)
            renderTable('table-banano', r.banano, r.semanas)
        }, $scope.filters)
    }

    renderTable = (id, data, semanas) => {
		let props = {
            header : [
				{
                    key : 'finca',
                    name : 'FINCA',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
					resizable : true,
					width : 150
				},
            ],
            data : data,
            height : 500
		}
		
		for(let i in semanas){
			let sem = semanas[i]
			props.header.push({
				key : `sem_${sem}`,
				name : sem,
				titleClass : 'text-center',
				sortable : true,
				alignContent : 'center',
				filterable : true,
				resizable : true,
                editable : true,
                draggable : false,
                events: {
					onKeyDown: function(ev, column) {
						if (ev.key === 'Enter') {
							let index = parseInt(column.rowIdx)
							let key = column.column.key
							let row = window[id].rowGetter(index)
							save(row.id_finca, key, row[key].toString().toUpperCase(), id)
						}
					},
                },
				customCell : (row_data) => {
                    if(i > 0){
                        let past_sem = semanas[i-1]
                        if(row_data[`sem_${sem}`] != row_data[`sem_${past_sem}`]) {
                            row_data[`class_sem_${sem}`] = 'bg-red-thunderbird bg-font-red-thunderbird'
                        }else{
                            row_data[`class_sem_${sem}`] = 'bg-green-haze bg-font-green-haze'
                        }
                    }else{
                        row_data[`class_sem_${sem}`] = 'bg-green-haze bg-font-green-haze'
                    }

					return `
						<div style={{height: '100%'}} class="${row_data[`class_sem_${sem}`]}">
							${row_data[`sem_${sem}`]}
						</div>
                    `
				}
			})
		}
        $("#"+id).html("")
        window[id] = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById(id))
	}

    $scope.init()

}]);
