app.controller('foliar', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){
    
    $scope.semanas = [];
    $scope.color = ["#37b7f3", "#33691E", "#AB47BC", "#EC407A", "#EF5350", "#00897B", "#CDDC39", "#FF9800", "#795548"];
    $scope.charts = {
        foliar : [],
    }
    $scope.generateWeeks = function(){
        for (var i = 1; 53 > i; i++) {
            $scope.semanas.push(i);
        }
        // console.log($scope.semanas);
    }
    
    $scope.init = function(){
        $scope.generateWeeks();
        $scope.getData();
        $interval($scope.getData, 50000);
    }

    $scope.random = [];
    
    // $scope.getRandomData = function() {
    //     if ($scope.random.length > 0) $scope.random = $scope.random.slice(1);
    //     // do a random walk
    //     while ($scope.random.length < totalPoints) {
    //         var prev = $scope.random.length > 0 ? $scope.random[$scope.random.length - 1] : 50;
    //         var y = prev + Math.random() * 10 - 5;
    //         if (y < 0) y = 0;
    //         if (y > 100) y = 100;
    //         $scope.random.push(y);
    //     }
    //     // zip the generated y values with the x values
    //     var res = [];
    //     for (var i = 0; i < $scope.random.length; ++i) {
    //         res.push([i, $scope.random[i]]);
    //     }

    //     return res;
    // }

    $scope.getData = function(){
        var data = {
            opt : 'GRAFICA'
        }
        client.post("./controllers/foliar.php" ,$scope.printGraphyc , data);
    }

    $scope.printGraphyc = function(r , b){
        b();
        if(r){
            $scope.printData(r.data , "foliar");
            $scope.charts.foliar = r.table;
            console.log("foliar")
        }
    }

    $scope.printData = function(r , id){
        if(r){  
            var data = [];
            var id = id;
            for(var info in r){
                data.push({
                    data : r[info],
                    label: info,
                    lines: {
                        lineWidth: 1,
                    },
                    shadowSize: 0
                })
            }
            console.log(id);
            var plot = $.plot($('#'+id), data, {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.05
                                }, {
                                    opacity: 0.01
                                }]
                            }
                        },
                        points: {
                            show: false,
                            radius: 3,
                            lineWidth: 1
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderColor: "#eee",
                        borderWidth: 1
                    },
                    colors: ["#d12610", "#37b7f3", "#52e136"],
                    xaxis: {
                        ticks: 11,
                        tickDecimals: 0,
                        tickColor: "#eee",
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 1,
                        tickColor: "#eee",
                        max: 1.40
                    }
                });


                function showTooltip(x, y, contents) {
                    $('<div id="tooltip">' + contents + '</div>').css({
                        position: 'absolute',
                        display: 'none',
                        top: y + 5,
                        left: x - 30,
                        border: '1px solid #333',
                        padding: '4px',
                        color: '#fff',
                        'border-radius': '3px',
                        'background-color': '#333',
                        opacity: 0.80
                    }).appendTo("body").fadeIn(200);
                }

                var previousPoint = null;
                $('#'+id).bind("plothover", function(event, pos, item) {
                    $("#x").text(pos.x.toFixed(0));
                    $("#y").text(pos.y.toFixed(2));

                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            showTooltip(item.pageX, item.pageY,"Año " + item.series.label + " Semana " + x + " Valor " + y);
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                
                });
        }   
    }
}]);
