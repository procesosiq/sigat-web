app.controller('mainIndex', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){
    
    $scope.init = function(){
        
    }

    $scope.haciendas = []

    $("#client").on("change" , function(e){
        var data = {
            id_client : $(this).val(),
            accion : "indexMain.getFincasDisponibles"
        }
        // console.log("Entro")
        client.post("./controllers/index.php?accion=indexMain.getFincas",$scope.getData , data);
    })

    $scope.save = function(){
        var data = {
            id_hacienda : $("#hacienda").val(),
            accion : "indexMain.saveFinca"
        }
        // console.log("Entro")
        client.post("./controllers/index.php?accion=indexMain.saveFinca",function(r , b){
            b();
            alert("Cliente Registrado",'Sigat' , "success" , function(){
                document.location.href= r.path;
            })
        } , data);
    }

    $scope.getData = function(r , b){
        b();
        if(r){
            $scope.haciendas = r;
        }
    }
}]);
