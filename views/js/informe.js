var descripcion_data;
var introduccion_data;
var ComponentsEditors = function () {
    
    // var handleWysihtml5 = function () {
    //     if (!jQuery().wysihtml5) {
    //         return;
    //     }

    //     if ($('.wysihtml5').size() > 0) {
    //         $('.wysihtml5').wysihtml5({
    //             "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
    //         });
    //     }
    // }

    var handleSummernote = function () {
        // $('#summernote_1').summernote({
        //     height: 300,
        //     onKeyup: function (e) {
        //         descripcion_data = $(this).code();
        //     }
        // });
        // $('#summernote_2').summernote({
        //     height: 300,
        //     onKeyup: function (e) {
        //         introduccion_data = $(this).code();
        //     }
        // });
        //API:
        //var sHTML = $('#summernote_1').code(); // get code
        //$('#summernote_1').destroy(); // destroy
        grid.msjStatus.inactive = 'BORRADOR';
        grid.msjStatus.active = 'FINALIZADO';
        grid.msjButton = 'VER INFORME';
        grid.initOrder.order = 'desc';
        grid.init("#list_informe" , "../controllers/index.php?accion=Informes");

        $('#list_informe tbody').on( 'click', 'button', function () {
             var data = grid.tableEdit.row( $(this).parents('tr') ).data();
             if(data.length > 0){
                // document.location.href = "/newFinca?id=" + data[1];
                ahttp.post("../controllers/index.php?accion=Informes.getUrl" , getInfo , { id: data[1] } );
             }

             function getInfo(r , b){
                b();
                if(r){
                   window.open(r);
                }
            }
        } );
    }

    return {
        //main function to initiate the module
        init: function () {
            // handleWysihtml5();
            handleSummernote();
        }
    };

}();

jQuery(document).ready(function() {    
    ComponentsEditors.init(); 
});

app.controller('informe', ['$scope','$http','$interval','client', function($scope,$http,$interval,client){

    $scope.semana = moment().year() > parseInt("<?= $sesion->year ?>") ? 52 : moment().week()
    $scope.semanas = []
    $scope.data = {}
    let to = $scope.semana
    for(let i = 1; i <= to; i++){
        $scope.semanas.push(i)
    }

    $scope.descripcion = "";
    $scope.LiveWrite = true;
    $scope.id = 0;
    $scope.init = function(){
        let download = document.getElementById('download')
        if(download){
            window.open(download.href)
          $http.post('./controllers/index.php?accion=Informes.enviar', { token : "<?= $_GET['download'] ?>" })
            .then(() => {
              alert("Correo enviado con éxito", "Sigat", "success")
            })
        }
        let data = {
            search : 1
        }
        client.post("../controllers/index.php?accion=Informes.getInforme", $scope.getInfo, data);
    }

    const getParameter = (p) => {
        let url_string = window.location.href
        let url = new URL(url_string);
        let c = url.searchParams.get(p);
        return c
    }

    $scope.changeSemana = () => {
        if(getParameter('download')){
            window.location.href = 'informe'
            return
        }
        
        $scope.init()
    }

    $scope.getInfo = function(r,b){
        b();
        if(r.success == 200){
            $scope.semana = r.ultima_semana_fotos
            $('#summernote_2').val(r.data.informe);
            $('#summernote_1').val(r.data.descripcion);
            $('#summernote_1').summernote({
                height: 300,
                onKeyup: function (e) {
                    descripcion_data = $(this).code();
                    $scope.saveKeyup();
                }
            });
            $('#summernote_2').summernote({
                height: 300,
                onKeyup: function (e) {
                    introduccion_data = $(this).code();
                    $scope.saveKeyup();
                }
            });
            $scope.id = r.data.id;
        }
    }

    $scope.saveKeyup = function(){
        if($scope.LiveWrite){
            var data = {
                descripcion : $('#summernote_1').code(),
                informe : $('#summernote_2').code()
            }
            $http.post("../controllers/index.php?accion=Informes.saveInformeRL", data).success(function(data, status) {
                if(data && data.success != 200){
                    alert("Error al grabar");
                }else{
                    $scope.id = data.id;
                }
            });
        }
    }

    $scope.enviar = function(){
        if("<?= Session::getInstance()->client ?>" == "25"){
            $scope.loadInformePrimaDonaa()
        }else{
            var data = {
                id : $scope.id,
                descripcion : $('#summernote_1').code(),
                introduccion : $('#summernote_2').code(),
                semana : $scope.semana
            }
    
            client.post('../controllers/guardar_informe.php', $scope.save_pdf,  data)
        }
    }

    $scope.token = "5ef0b4eba35ab2d6180b0bca7e46b6"
    $scope.save_pdf = function(data, callback){
        callback();
        /*let modal = $("#nsecado").modal()
        modal.trigger('toggle')
        $scope.token = data.token;
        
        prepareFrame('http://sigat.procesos-iq.com/printPDF.php?page=informe_pdf&token='+data.token);*/
        setTimeout(() => {
            $scope.token = data
            //download('http://sigat.procesos-iq.com/informes/'+data+'.pdf')
            $scope.imprimir()
            /*alert("Se envio correctamente", "", "success", () => {
                location.reload();
            })*/
        }, 250) 
    }

    var prepareFrame = function(url) {
        $("#previsualizar").html("")
        var ifrm = document.createElement("iframe");
        ifrm.setAttribute("src", url);
        ifrm.style.width = "820px";
        ifrm.style.height = "400px";
        document.getElementById("previsualizar").appendChild(ifrm);

        setTimeout(() => {
            $scope.descargar()
        }, 30000)
    }

    $scope.descargar = () => {
        console.log("descargando")

        var html = $("iframe").contents().find("html").html()
        $("#canvas").html(html)
    }

    $scope.imprimir = () => {
        var win = window.open('http://sigat.procesos-iq.com/printPDF.php?page=informe_pdf&token=' + $scope.token, '_blank')
        win.focus()
        //win.print()
    }

    function download(url){
        var link = document.createElement("a");
        link.download = 'informe.pdf';
        link.href = url;
        link.click();
    }

    $scope.loadInformePrimaDonaa = () => {
        var data = {
            accion : "InspeccionSemanal.dataGraficas",
            semana : $scope.semana
        }
        client.post("./controllers/index.php?accion=InspeccionSemanal.dataGraficas", function(r , b){
            b();
            if(r){
                load.block()
                $("#informePrimaDonna").removeClass('hide')
                printGraficasInspeccion(r)
                setTimeout(() => {
                    renderInforme()
                }, 1000)
            }
        }, data);
    }

    printGraficasInspeccion = (data) => {
        printGrafica(data.m3_h3, 'grafica-m3-h3', '3M - EE HOJA 3')
        printGrafica(data.m3_h4, 'grafica-m3-h4', '3M - EE HOJA 4')
        printGrafica(data.m3_h5, 'grafica-m3-h5', '3M - EE HOJA 5')
        printGrafica(data.m3_hvlq, 'grafica-m3-hvlq', '3M - H+VLQ<5%')
        printGrafica(data.m3_hvle, 'grafica-m3-hvle', '3M - H+VLE')
        printGrafica(data.s0_ht, 'grafica-s0-ht', 'S0 - HT')
        printGrafica(data.s0_hvle, 'grafica-s0-hvle', 'S0 - H+VLE')
        printGrafica(data.s0_hvlq, 'grafica-s0-hvlq', 'S0 - H+VLQ<5%')
        printGrafica(data.s11_ht, 'grafica-s11-ht', 'S11 - HT')
        printGrafica(data.s11_hvlq, 'grafica-s11-hvlq', 'S11 - H+VLQ<5%')
    }

    graficasToInforme = []
    printGrafica = (data, id, titulo) => {
        if(data && id){
            $scope.data[id] = data
            var options = {
                series: data.data,
                legend: data.legend,
                umbral: data.umbral || null,
                id: id,
                titulo : titulo || "",
                min : 0,
                max : data.max || null,
                zoom : false
            }
            ReactDOM.render(React.createElement(Historica, options), document.getElementById(id));
            setTimeout(() => {
                graficasToInforme.push($("#"+id+" canvas")[0])
                graficasToInforme.push($("#table-"+id)[0])
            }, 100)
        }
    }

    function appendText(text){
        var span = document.createElement('span')
        span.innerHTML = `
            ${text}
            `
        informe_html.append(span)
    }
    function appendLogo(){
        var sigat = document.createElement('img');
        var canvas = document.createElement('canvas');
      var ctx = canvas.getContext("2d");
        sigat.src = 'http://sigat.procesos-iq.com/img/logo.png';
        sigat.width = 180
        sigat.height = 80
        ctx.drawImage(sigat, 180, 80);
      var img = document.createElement('img');
      img.src = canvas.toDataURL()
      var base64 = getBase64Image(sigat);
      informe_html.append(sigat);
        appendText("")
    }

function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}


    async function renderInforme () {

        informe_html = $("#informe-html")
        informe_html.html("")

        appendLogo()
        appendText("REPORTE PRIMA DONNA SEMANA "+$scope.semana+"/<?= Session::getInstance()->year ?>")

        var i = 0
        for(let i in graficasToInforme) {
            await append(informe_html, graficasToInforme[i], i)
        }
        setTimeout(() => {
            load.unblock()
          Export2Doc('informe-html', 'informe');
            $("#informePrimaDonna").addClass('hide')
        }, 1000)
    }

function Export2Doc(element, filename = ''){
    var preHtml = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'><head><meta charset='utf-8'><title>Export HTML To Doc</title></head><body>";
    var postHtml = "</body></html>";
    var html = preHtml+document.getElementById(element).innerHTML+postHtml;

    var blob = new Blob(['\ufeff', html], {
        type: 'application/msword'
    });
    
    // Specify link url
    var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);
    
    // Specify file name
    filename = filename?filename+'.doc':'document.doc';
    
    // Create download link element
    var downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob ){
        navigator.msSaveOrOpenBlob(blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = url;
        
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
    
    document.body.removeChild(downloadLink);
}

    function append(informe_html, el, i){
        return new Promise((resolve, reject) => {
            if($(el).prop('tagName') == 'DIV'){
                html2canvas($(el)).then(canvas => {
                    let dataUrl = canvas.toDataURL()
                    var imageFoo = document.createElement('img');
                    imageFoo.src = dataUrl;
                    imageFoo.width = 600;
                    imageFoo.height = 50;
                    imageFoo.style.width = 600;
                    imageFoo.style.height = 50;
                    informe_html.append(imageFoo);
                    resolve()
                });
            }
            else {
                let dataUrl = el.toDataURL()
                var imageFoo = document.createElement('img');
                imageFoo.src = dataUrl;
                imageFoo.width = 600;
                imageFoo.height = 350;
                informe_html.append(imageFoo);
    
                if(i+1 % 4 == 0){
                    var div = document.createElement("div")
                    div.classList.add('page-break')
                    informe_html.append(div)
                    appendLogo()
                }
                /*if(i == 4) {
                    appendText("PLANTAS RECIÉN PARIDAS (0 SEMANAS)")
                }
                if(i == 7) {
                    appendText("PLANTAS A LA COSECHA (11 SEMANAS)")
                }*/
    
                resolve()
            }
    
        })
    }
}]);
