getParams = (accion, data) => {
    data = data || {}
    var params = {
        accion : accion
    }
    Object.keys(data).map((value, index) => {
        params[value] = data[value]
    })
    return params
}

app.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});

app.controller('inspeccion', ['$scope','$http','client', function($scope,$http,client){
    
    $scope.filters = {
        semana : null,
        luz : 400,
        fecha_inicial : '',
        fecha_final : '',
        tipodata : 'normal'
    }
    $scope.hacienda = "<?= Session::getInstance()->finca ?>"
    $scope.id_cliente = "<?=  Session::getInstance()->id_client ?>"
    $scope.id_cliente_original = "<?=  Session::getInstance()->id_client ?>"

    $scope.graficas = {
        m3 : {
            hvmle : new echartsPlv(),
            h4 : new echartsPlv(),
            h3 : new echartsPlv(),
            h5 : new echartsPlv(),
            hmvlqm : new echartsPlv(),
            loaded : false
        },
        s0 : {
            ht : new echartsPlv(),
            hmvlqm : new echartsPlv(),
            hmvle : new echartsPlv(),
            loaded : false    
        },
        s11 : {
            ht : new echartsPlv(),
            hmvlqm : new echartsPlv(),
            loaded : false
        },
        foliar : {
            foliar : new echartsPlv(),
            loaded : false
        },
        clima : {
            temp_min : new echartsPlv(),
            temp_max : new echartsPlv(),
            lluvia : new echartsPlv(),
            rad_solar : new echartsPlv(),
            rad_solar_horas : new echartsPlv(),
            horas_luz : new echartsPlv(),
            humedad_max : new echartsPlv(),
            humedad_min : new echartsPlv(),
            loaded : false
        }
    }
    $scope.getClassColor = (data, value, reverse) => {
        if(value != null)
        if(data.legend.data.indexOf("Umbral") > -1){
            let umbralValue = data.series[0].data[0] || 0
            if(!reverse){
                if(value > umbralValue)
                    return 'bg-green-jungle bg-font-green-jungle'
                else if(value == umbralValue)
                    return 'bg-yellow-gold bg-font-yellow-gold'
                else
                    return 'bg-red-thunderbird bg-font-red-thunderbird'
            }else{
                if(value < umbralValue)
                    return 'bg-green-jungle bg-font-green-jungle'
                else if(value == umbralValue)
                    return 'bg-yellow-gold bg-font-yellow-gold'
                else
                    return 'bg-red-thunderbird bg-font-red-thunderbird'
            }
        }
    }
    $scope.semaforo = (value, umbral, reverse) => {
        if(value != null)
        if(!reverse){
            if(value > umbral)
                return 'bg-green-jungle bg-font-green-jungle'
            else if(value == umbral)
                return 'bg-yellow-gold bg-font-yellow-gold'
            else
                return 'bg-red-thunderbird bg-font-red-thunderbird'
        }else{
            if(value < umbral)
                return 'bg-green-jungle bg-font-green-jungle'
            else if(value == umbral)
                return 'bg-yellow-gold bg-font-yellow-gold'
            else
                return 'bg-red-thunderbird bg-font-red-thunderbird'
        }
    }

    $scope.save = () => {
        if(!$scope.hacienda > 0){
            alert("Seleccione una hacienda")
            return;
        }

        if($scope.filters.tipodata != 'comparacion'){
            var data = {
                id_hacienda : $scope.hacienda,
                accion : "indexMain.saveFinca"
            }
            client.post("./controllers/index.php?accion=indexMain.saveFinca",function(r , b){
                b();
                document.location.href= r.path;
            } , data);
        }else{
            if($("#tab_3m").attr('aria-expanded') != 'false'){
                $scope.getGraficas3M()
            }
            if($("#tab_0s").attr('aria-expanded') != 'false'){
                $scope.getGraficasS0()
            }
            if($("#tab_11s").attr('aria-expanded') != 'false'){
                $scope.getGraficasS11()
            }
        }
    }

    $scope.saveEmision = (id_foliar, emision, $index) => {
        if(confirm(`¿Estas seguro que deseas cambiar esta emisión?`)){
            client.post('./controllers/index.php?accion=Inspeccion.saveFoliar', function(r, b){
                b()
                if(r.status == 200){
                    alert('Se guardo con correctamente', '', 'success')
                    $scope.graficas.foliar.loaded = false
                    $scope.getFoliar()
                }else{
                    alert(`Ocurrio algun problema, favor de intentar mas tarde`)
                }
            }, {
                id_foliar : id_foliar,
                emision : emision,
                valor : $scope.foliar.emisiones[$index][`emision${emision}`]
            })
        }else{
            let old_value = $scope.foliar.real_emisiones[$index][`emision${emision}`]
            $scope.foliar.emisiones[$index][`emision${emision}`] = Number(old_value)
        }
    }

    $scope.getFincas = (reload) => {
        if(reload) $scope.hacienda = 0

        var data = {
            id_client : $scope.id_cliente,
            accion : "indexMain.getFincas"
        }
        client.post("./controllers/index.php?accion=indexMain.getFincasDisponibles", function(r , b){
            b();
            if(r){
                $scope.haciendas = r;
            }
        }, data);
    }
    $scope.getFincas()

    $scope.getTablaPorLote = () => {
        $scope.loadingSigatoka = true
        $http.post('/controllers/index.php', getParams('Inspeccion', $scope.filters)).then( r => {
            $scope.loadingSigatoka = false

            if(r.data){
                if(r.data.last_semana){
                    $scope.filters.semana = r.data.last_semana
                }
                $scope.semanas = r.data.semanas
                $scope.table_lote = r.data.table_lote
                $scope.table_lote_anio = r.data.table_lote_anio
            }
        })
    }

    $scope.reloadGrafica = (id, data, max, min) => {
        data.yAxis.max = max
        data.yAxis.min = min
        new echartsPlv().init(id, data)
    }

    /* BEGIN 3M */
    $scope.getGraficas3M = () => {
        if($scope.graficas.m3.loaded && $scope.filters.tipodata != 'comparacion')
            return;
        if($scope.filters.tipodata == 'comparacion') $scope.graficas.m3.loaded = false;

        setTimeout(() => {
            var url_3m = $scope.filters.tipodata != 'comparacion' ? 'Inspeccion.get3M' : 'Inspeccion.get3MComparativo';
            $http.post('/controllers/index.php', getParams(url_3m, $scope.filters)).then( r => {
                if(r.data){
                    $scope.graficas.m3.hvmle.init("libre_estrias", r.data.data_chart)
                    $scope.graficas.m3.libre_strias = r.data.data_chart
                    $scope.graficas.m3.libre_strias_max = r.data.max
                    $scope.graficas.m3.libre_strias_min = r.data.min
                }
            })

            var url_qm = $scope.filters.tipodata != 'comparacion' ? 'Inspeccion.getHMVLQM' : 'Inspeccion.getHMVLQMComparativo';
            $http.post('/controllers/index.php', getParams(url_qm, $scope.filters)).then( r => {
                if(r.data){
                    $scope.graficas.m3.hmvlqm.init("hoja_vieja_menor_5", r.data.hmvlqm)
                    $scope.graficas.m3.hoja_vieja_menor_5 = r.data.hmvlqm
                    $scope.graficas.m3.hoja_vieja_menor_5_max = r.data.max
                    $scope.graficas.m3.hoja_vieja_menor_5_min = r.data.min
                }
            })
    
            var url_h4 = $scope.filters.tipodata != 'comparacion' ? 'Inspeccion.getH4' : 'Inspeccion.getH4Comparativo';
            $http.post('/controllers/index.php', getParams(url_h4, $scope.filters)).then( r => {
                if(r.data){
                    $scope.graficas.m3.h4.init("hoja_4", r.data.h4)
                    $scope.graficas.m3.hojas_4 = r.data.h4
                    $scope.graficas.m3.hojas_4_max = r.data.max
                    $scope.graficas.m3.hojas_4_min = r.data.min
                }
            })
    
            var url_h3 = $scope.filters.tipodata != 'comparacion' ? 'Inspeccion.getH3' : 'Inspeccion.getH3Comparativo';
            $http.post('/controllers/index.php', getParams(url_h3, $scope.filters)).then( r => {
                if(r.data){
                    $scope.graficas.m3.h3.init("hoja_3", r.data.h3)
                    $scope.graficas.m3.hojas_3 = r.data.h3
                    $scope.graficas.m3.hojas_3_max = r.data.max
                    $scope.graficas.m3.hojas_3_min = r.data.min
                }
            })
    
            var url_h5 = $scope.filters.tipodata != 'comparacion' ? 'Inspeccion.getH5' : 'Inspeccion.getH5Comparativo';
            $http.post('/controllers/index.php', getParams(url_h5, $scope.filters)).then( r => {
                if(r.data){
                    $scope.graficas.m3.h5.init("hoja_5", r.data.h5)
                    $scope.graficas.m3.hojas_5 = r.data.h5
                    $scope.graficas.m3.hojas_5_max = r.data.max
                    $scope.graficas.m3.hojas_5_min = r.data.min
                }
            })
            $scope.graficas.m3.loaded = true;
        }, 250)
    }  
    /* END 3M */

    /* BEGIN 0S */
    $scope.getGraficasS0 = () => {
        if($scope.graficas.s0.loaded && $scope.filters.tipodata != 'comparacion')
            return;
        if($scope.filters.tipodata == 'comparacion') $scope.graficas.s0.loaded = false;

        setTimeout(() => {
            var url_0s = $scope.filters.tipodata != 'comparacion' ? 'Inspeccion.getHT0S' : 'Inspeccion.getHT0SComparativo';
            $http.post('/controllers/index.php',getParams(url_0s,$scope.filters)).then(r => {
                if(r.data){
                    $scope.graficas.s0.ht.init("hojas_total",r.data.ht)
                    $scope.graficas.s0.hojas_total_S0 = r.data.ht
                    $scope.graficas.s0.hojas_total_S0_max = Number(r.data.max)
                    $scope.graficas.s0.hojas_total_S0_min = Number(r.data.min)
                }
            })
            
            var url_qm0s = $scope.filters.tipodata != 'comparacion' ? 'Inspeccion.getHMVLQM0S' : 'Inspeccion.getHMVLQM0SComparativo';
            $http.post('/controllers/index.php',getParams(url_qm0s, $scope.filters)).then(r => {
                if(r.data){
                    $scope.graficas.s0.hmvlqm.init("hoja_vieja_menor_5_0S", r.data.hmvlqm)
                    $scope.graficas.s0.hoja_vieja_menor_5_S0 = r.data.hmvlqm
                    $scope.graficas.s0.hoja_vieja_menor_5_S0_max = r.data.max
                    $scope.graficas.s0.hoja_vieja_menor_5_S0_min = r.data.min
                }
            })
            
            var url_hv0s = $scope.filters.tipodata != 'comparacion' ? 'Inspeccion.getHMVLE0S' : 'Inspeccion.getHMVLE0SComparativo';
            $http.post('/controllers/index.php',getParams(url_hv0s, $scope.filters)).then(r => {
                if(r.data){
                    $scope.graficas.s0.hmvle.init("libre_estrias_0S",r.data.hmvle)
                    $scope.graficas.s0.libre_estrias_S0 = r.data.hmvle
                    $scope.graficas.s0.libre_estrias_S0_max = r.data.max
                    $scope.graficas.s0.libre_estrias_S0_min = r.data.min
                }
            })
    
            $scope.graficas.s0.loaded = true;
        }, 250)
    }
    /* END 0S */

    $scope.getGraficasS11 = () => {
        if($scope.graficas.s11.loaded && $scope.filters.tipodata != 'comparacion')
            return;
        if($scope.filters.tipodata == 'comparacion') $scope.graficas.s11.loaded = false;

        setTimeout(() => {
            var url_ht = $scope.filters.tipodata != 'comparacion' ? 'Inspeccion.getHT11S' : 'Inspeccion.getHT11SComparativo';
            $http.post('/controllers/index.php',getParams(url_ht, $scope.filters)).then(r => {
                if(r.data){
                    $scope.graficas.s11.ht.init("hojas_total_11S",r.data.ht)
                    $scope.graficas.s11.hojas_total_S11 = r.data.ht
                    $scope.graficas.s11.hojas_total_S11_max = r.data.max
                    $scope.graficas.s11.hojas_total_S11_min = r.data.min
                }
            })
            
            var url_qm = $scope.filters.tipodata != 'comparacion' ? 'Inspeccion.getHMVLQM11S' : 'Inspeccion.getHMVLQM11SComparativo';
            $http.post('/controllers/index.php',getParams(url_qm, $scope.filters)).then(r => {
                if(r.data){
                    $scope.graficas.s11.hmvlqm.init("hoja_vieja_menor_5_11S",r.data.hmvlqm)
                    $scope.graficas.s11.hoja_vieja_menor_5_S11 = r.data.hmvlqm
                    $scope.graficas.s11.hoja_vieja_menor_5_S11_max = r.data.max
                    $scope.graficas.s11.hoja_vieja_menor_5_S11_min = r.data.min
                }
            })
    
            $scope.graficas.s11.loaded = true;
        }, 250)
    }

    window.Grafica_foliar = null
    $scope.getFoliar = () => {
        if(Grafica_foliar){
            setTimeout(() => {
                Grafica_foliar.resize()
            }, 250)
        }

        if($scope.graficas.foliar.loaded)
            return;

        $scope.loadingFoliar = true
        $http.post('/controllers/index.php', getParams('Inspeccion.getEmisionFoliar', $scope.filters)).then( r => {
            $scope.loadingFoliar = false
            if(r.data){
                window.Grafica_foliar = $scope.graficas.foliar.foliar.init("foliar", r.data.data)

                $scope.foliar = {
                    semanas: [],
                    data : r.data.table,
                    emisiones : [],
                    real_emisiones : r.data.table_emision
                }
                r.data.table_emision.map((row) => {
                    $scope.foliar.emisiones.push({
                        id : row.id,
                        foco : row.foco,
                        fecha : row.fecha,
                        semana : row.semana,
                        emision1 : Number(row.emision1),
                        emision2 : Number(row.emision2),
                        emision3 : Number(row.emision3),
                        emision4 : Number(row.emision4),
                        emision5 : Number(row.emision5),
                        emision6 : Number(row.emision6),
                        emision7 : Number(row.emision7),
                        emision8 : Number(row.emision8),
                        emision9 : Number(row.emision9),
                        emision10 : Number(row.emision10),
                    })
                })
                Object.keys(r.data.table).map((foco) => {
                    let semanas = r.data.table[foco]
                    Object.keys(semanas).map((sem) => {
                        if($scope.foliar.semanas.indexOf(sem) == -1){
                            $scope.foliar.semanas.push(parseInt(sem))
                        }
                    })
                })
                $scope.foliar.semanas.sort(function sortNumber(a,b) {
                    return a - b;
                })
            }
        })
        $scope.graficas.foliar.loaded = true;
    }

    $scope.getClima = () => {
        if($scope.graficas.clima.loaded)
            return;

        $http.post('/controllers/index.php', getParams('Inspeccion.getTempMin', $scope.filters)).then( r => {
            if(r.data){
                $scope.graficas.clima.temp_min.init("temp_min", r.data.data)
                $scope.graficas.clima.temp_min_data = r.data.data
                $scope.graficas.clima.temp_min_max = r.data.max
                $scope.graficas.clima.temp_min_min = r.data.min
            }
        })

        $http.post('/controllers/index.php', getParams('Inspeccion.getTempMax', $scope.filters)).then( r => {
            if(r.data){
                $scope.graficas.clima.temp_max.init("temp_max", r.data.data)
                $scope.graficas.clima.temp_max_data = r.data.data
                $scope.graficas.clima.temp_max_max = r.data.max
                $scope.graficas.clima.temp_max_min = r.data.min
            }
        })

        $http.post('/controllers/index.php', getParams('Inspeccion.getLluvia', $scope.filters)).then( r => {
            if(r.data){
                $scope.graficas.clima.lluvia.init("lluvia", r.data.data)
                $scope.graficas.clima.lluvia_data = r.data.data
                $scope.graficas.clima.lluvia_max = r.data.max
                $scope.graficas.clima.lluvia_min = r.data.min
            }
        })

        $http.post('/controllers/index.php', getParams('Inspeccion.getHumedadMax', $scope.filters)).then( r => {
            if(r.data){
                $scope.graficas.clima.humedad_max.init("humedad_max", r.data.data)
                $scope.graficas.clima.humedad_max_data = r.data.data
                $scope.graficas.clima.humedad_max_max = r.data.max
                $scope.graficas.clima.humedad_max_min = r.data.min
            }
        })

        $http.post('/controllers/index.php', getParams('Inspeccion.getRadSolar', $scope.filters)).then( r => {
            if(r.data){
                $scope.graficas.clima.rad_solar.init("rad_solar", r.data.data)
                $scope.graficas.clima.rad_solar_data = r.data.data
                $scope.graficas.clima.rad_solar_max = r.data.max
                $scope.graficas.clima.rad_solar_min = r.data.min
            }
        })

        $http.post('/controllers/index.php', getParams('Inspeccion.getHorasLuz', $scope.filters)).then( r => {
            if(r.data){
                $scope.graficas.clima.horas_luz.init("horas_luz", r.data.data)
                $scope.graficas.clima.horas_luz_data = r.data.data
                $scope.graficas.clima.horas_luz_max = r.data.max
                $scope.graficas.clima.horas_luz_min = r.data.min
            }
        })

        $http.post('/controllers/index.php', getParams('Inspeccion.getRadSolarHoras', $scope.filters)).then( r => {
            if(r.data){
                if(r.data.fecha){
                    $scope.filters.fecha_inicial = r.data.fecha
                    $scope.filters.fecha_final = r.data.fecha
                    $("#date-calendar").html(`${r.data.fecha} - ${r.data.fecha}`)
                }
                $scope.graficas.clima.rad_solar_horas.init("rad_solar_horas", r.data.data)
            }
        })
        $scope.graficas.clima.loaded = true;
    }
    
    $scope.changeLuz = () => {
        $http.post('/controllers/index.php', getParams('Inspeccion.getHorasLuz', $scope.filters)).then( r => {
            if(r.data){
                $scope.graficas.clima.horas_luz.init("horas_luz", r.data.data)
                $scope.graficas.clima.horas_luz_data = r.data.data
                $scope.graficas.clima.horas_luz_max = r.data.max
                $scope.graficas.clima.horas_luz_min = r.data.min
            }
        })
    }

    $scope.getAplicaciones = () => {
        $http.post('/controllers/index.php', getParams('Inspeccion.getAplicaciones', $scope.filters)).then( r => {
            if(r.data){
                $scope.aplicaciones = r.data.data
            }
        })
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
            $scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
        }
        
        $http.post('/controllers/index.php', getParams('Inspeccion.getRadSolarHoras', $scope.filters)).then( r => {
            if(r.data){
                if(r.data.fecha){
                    $scope.filters.fecha_inicial = r.data.fecha
                    $scope.filters.fecha_final = r.data.fecha
                    $("#date-calendar").html(`${r.data.fecha} - ${r.data.fecha}`)
                }
                $scope.graficas.clima.rad_solar_horas.init("rad_solar_horas", r.data.data)
            }
        })
    }

    $scope.getClimaAnio = () => {
        $scope.loadingClima = true
        $http.post('/controllers/index.php', getParams('Inspeccion.getClimaAnio', $scope.filters))
        .then(r => {
            $scope.loadingClima = false
            $scope.clima_anio = r.data
        })
    }

    /* END 0S */
    $scope.init = () => {
        $scope.getTablaPorLote()
        $scope.getFoliar()
        $scope.getClimaAnio()
    }

    $scope.exportExcel = function(id_table, title){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var contentTable = table.innerHTML
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(id_table, title)
    }

    $scope.exportPrint = function(id_table){
        $("#"+id_table).css("display", "")
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;
        newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
        $("#"+id_table).css("display", "none")
    }

    $scope.init()
    setTimeout(() => {
        $(`input[type=radio][name=comparacion][value=${$scope.filters.tipodata}]`).click()
    }, 1000)
}]);
