getParams = (accion, data) => {
    data = data || {}
    var params = {
        accion : accion
    }
    Object.keys(data).map((value, index) => {
        params[value] = data[value]
    })
    return params
}

app.service('request', ['$http', ($http) => {

    service = {}

    service.index = (callback, params) => {
        $http.post('./controllers/index.php?accion=InspeccionOrodeltiAppGraficas.index', params || {})
        .then(r => {
            callback(r.data)
        })
    }

    service.data = (callback, params) => {
        $http.post('./controllers/index.php?accion=InspeccionOrodeltiAppGraficas.data', params || {})
        .then(r => {
            callback(r.data)
        })
    }

    return service

}])

app.controller('inspeccion', ['$scope', 'request', function($scope, $request){
    
    $scope.filters = {
        gerente : '',
        gerenteName : 'ORODELTI',
        year : null,
        week : null,
    }
    $scope.gerentes = {}
    $scope.data = []

    $scope.setFilterYear = year => {
        $scope.filters.year = year
        $scope.getFilters().then(() => $scope.getData())
    }

    $scope.setFilterGerente = gerente => {
        $scope.filters.gerente = gerente.id
        $scope.filters.gerenteName = gerente.nombre
        $scope.getData();
    }
    
    $scope.getFilters = () => {
        return new Promise((resolve, reject) => {
            $request.index((r) => {
                $scope.years = r.years
                $scope.weeks = r.weeks
                $scope.gerentes = r.gerentes
    
                if(r.last_year){
                    $scope.filters.year = r.last_year
                }
                if(r.last_week){
                    $scope.filters.week = r.last_week
                }
                resolve()
            }, $scope.filters)
        })
    }

    $scope.getData = () => {
        return new Promise((resolve, reject) => {
            $request.data((r) => {
                $scope.data = r.data

                new echartsPlv().init('estado_evolutivo', r.graficas['ESTADO EVOLUTIVO'])
                new echartsPlv().init('plantas_3_metros', r.graficas['PLANTAS 3 METROS'])
                new echartsPlv().init('plantas_0_semanas', r.graficas['PLANTAS 0 SEMANAS'])
                new echartsPlv().init('plantas_11_semanas', r.graficas['PLANTAS 11 SEMANAS'])

                resolve()
            }, $scope.filters)
        })
    }

    $scope.init = () => {
        $scope.getFilters().then(() => $scope.getData())
    }

    $scope.exportExcel = function(id_table, title){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var contentTable = table.innerHTML
                // remove filters
                /*var cut = contentTable.search('<tr role="row" class="filter">')
                var cut2 = contentTable.search('</thead>')

                var part1 = contentTable.substring(0, cut)
                var part2 = contentTable.substring(cut2, contentTable.length)
                contentTable = part1 + part2*/
                
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(id_table, title || "")
    }

    $scope.init()

}]);
