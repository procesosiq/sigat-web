getParams = (accion, data) => {
    data = data || {}
    var params = {
        accion : accion
    }
    Object.keys(data).map((value, index) => {
        params[value] = data[value]
    })
    return params
}

app.directive('file', function () {
    return {
        scope: {
            file: '='
        },
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var file = event.target.files[0];
                scope.file = file ? file : undefined;
                scope.$apply();
            });
        }
    };
});

app.controller('inspeccion', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){
    
    $scope.filters = {
        semana : null,
        luz : 100,
        fecha_inicial : '',
        fecha_final : '',
        gerenteName : "SIGAT DEMO",
        gerente : '',
        year : moment().year()
    }
    $scope.gerentes = {
        
    }
    $scope.hacienda = "<?= Session::getInstance()->finca ?>"
    $scope.id_cliente = "<?=  Session::getInstance()->id_client ?>"
    $scope.id_cliente_original = "<?=  Session::getInstance()->id_client ?>"

    $scope.graficas = {
        m3 : {
            hvmle : new echartsPlv(),
            h4 : new echartsPlv(),
            h3 : new echartsPlv(),
            h5 : new echartsPlv(),
            hmvlqm : new echartsPlv(),
            loaded : false
        },
        s0 : {
            ht : new echartsPlv(),
            hmvle : new echartsPlv(),
            loaded : false    
        },
        s11 : {
            ht : new echartsPlv(),
            hmvlqm : new echartsPlv(),
            loaded : false
        },
        foliar : {
            foliar : new echartsPlv(),
            loaded : false
        },
        clima : {
            temp_min : new echartsPlv(),
            temp_max : new echartsPlv(),
            lluvia : new echartsPlv(),
            rad_solar : new echartsPlv(),
            rad_solar_horas : new echartsPlv(),
            horas_luz : new echartsPlv(),
            loaded : false
        }
    }
    $scope.getClassColor = (data, value, reverse) => {
        if(value != null)
        if(data.legend.data.indexOf("Umbral") > -1){
            let umbralValue = data.series[0].data[0] || 0
            if(!reverse){
                if(value > umbralValue)
                    return 'bg-green-jungle bg-font-green-jungle'
                else if(value == umbralValue)
                    return 'bg-yellow-gold bg-font-yellow-gold'
                else
                    return 'bg-red-thunderbird bg-font-red-thunderbird'
            }else{
                if(value < umbralValue)
                    return 'bg-green-jungle bg-font-green-jungle'
                else if(value == umbralValue)
                    return 'bg-yellow-gold bg-font-yellow-gold'
                else
                    return 'bg-red-thunderbird bg-font-red-thunderbird'
            }
        }
    }
    $scope.semaforo = (value, umbral, reverse) => {
        if(value != null)
        if(!reverse){
            if(value > umbral)
                return 'bg-green-jungle bg-font-green-jungle'
            else if(value == umbral)
                return 'bg-yellow-gold bg-font-yellow-gold'
            else
                return 'bg-red-thunderbird bg-font-red-thunderbird'
        }else{
            if(value < umbral)
                return 'bg-green-jungle bg-font-green-jungle'
            else if(value == umbral)
                return 'bg-yellow-gold bg-font-yellow-gold'
            else
                return 'bg-red-thunderbird bg-font-red-thunderbird'
        }
    }

    $scope.save = () => {
        if(!$scope.hacienda > 0){
            alert("Seleccione una hacienda")
            return;
        }

        if($scope.hacienda != 'COMPARACION'){
            var data = {
                id_hacienda : $scope.hacienda,
                accion : "indexMain.saveFinca"
            }
            client.post("./controllers/index.php?accion=indexMain.saveFinca",function(r , b){
                b();
                document.location.href= r.path;
            } , data);
        }else{
            if($("#tab_3m").attr('aria-expanded') != 'false'){
                $scope.getGraficas3M()
            }
            if($("#tab_0s").attr('aria-expanded') != 'false'){
                $scope.getGraficasS0()
            }
            if($("#tab_11s").attr('aria-expanded') != 'false'){
                $scope.getGraficasS11()
            }
        }
    }

    $scope.getGerentes = () => {
        $http.post('./controllers/index.php?accion=InspeccionOrodeltiDemo.gerentes').then(r => {
            $scope.gerentes = r.data.data
        })
        $http.post('./controllers/index.php?accion=InspeccionOrodeltiDemo.years').then(r => {
            $scope.filters.year = r.data.selected
            $scope.years = r.data.data
        })
    }

    $scope.setFilterGerente = function(data){
        if(data){
            $scope.filters.gerente = data.id
            $scope.filters.gerenteName = data.label
            
            $scope.graficas.m3.loaded = false
            $scope.graficas.s0.loaded = false
            $scope.graficas.s11.loaded = false
            $scope.data_tables = false

            if($("#tab_3m").attr('aria-expanded') != 'false'){
                $scope.getGraficas3M()
            }
            if($("#tab_0s").attr('aria-expanded') != 'false'){
                $scope.getGraficasS0()
            }
            if($("#tab_11s").attr('aria-expanded') != 'false'){
                $scope.getGraficasS11()
            }
        }
    }

    $scope.setFilterYear = function(data){
        if(data){
            $scope.filters.year = data.id
            
            $scope.graficas.m3.loaded = false
            $scope.graficas.s0.loaded = false
            $scope.graficas.s11.loaded = false
            $scope.data_tables = false

            if($("#tab_3m").attr('aria-expanded') != 'false'){
                $scope.getGraficas3M()
            }
            if($("#tab_0s").attr('aria-expanded') != 'false'){
                $scope.getGraficasS0()
            }
            if($("#tab_11s").attr('aria-expanded') != 'false'){
                $scope.getGraficasS11()
            }
        }
    }

    $scope.getFincas = (reload) => {
        if(reload) $scope.hacienda = 0

        var data = {
            id_client : $scope.id_cliente,
            accion : "indexMain.getFincas"
        }
        client.post("./controllers/index.php?accion=indexMain.getFincas", function(r , b){
            b();
            if(r){
                $scope.haciendas = r;
            }
        }, data);
    }
    $scope.getFincas()

    $scope.getTablaPorLote = () => {
        $http.post('/controllers/index.php', getParams('InspeccionOrodeltiDemo', $scope.filters)).then( r => {
            if(r.data){
                if(r.data.last_semana){
                    $scope.filters.semana = r.data.last_semana
                }
                $scope.semanas = r.data.semanas
                $scope.table_lote = r.data.table_lote
            }
        })
    }

    /* BEGIN 3M */
    $scope.getGraficas3M = () => {
        if($scope.graficas.m3.loaded && $scope.hacienda != 'COMPARACION')
            return;
        if($scope.hacienda == 'COMPARACION') $scope.graficas.m3.loaded = false;

        setTimeout(() => {
            if(!$scope.data_tables){
                $http.post('/controllers/index.php', getParams('InspeccionOrodeltiDemo.getDataTables', $scope.filters)).then(r => {
                    $scope.data_tables = r.data.data || []
                })
            }

            var url_3m = $scope.hacienda != 'COMPARACION' ? 'InspeccionOrodeltiDemo.get3M' : 'InspeccionOrodeltiDemo.get3MComparativo';
            $http.post('/controllers/index.php', getParams(url_3m, $scope.filters)).then( r => {
                if(r.data){
                    $scope.graficas.m3.hvmle.init("libre_estrias", r.data.data_chart)
                    $scope.graficas.m3.libre_strias = r.data.data_chart
                }
            })

            var url_qm = $scope.hacienda != 'COMPARACION' ? 'InspeccionOrodeltiDemo.getHMVLQM' : 'InspeccionOrodeltiDemo.getHMVLQMComparativo';
            $http.post('/controllers/index.php', getParams(url_qm, $scope.filters)).then( r => {
                if(r.data){
                    $scope.graficas.m3.hmvlqm.init("hoja_vieja_menor_5", r.data.hmvlqm)
                    $scope.graficas.m3.hoja_vieja_menor_5 = r.data.hmvlqm
                }
            })
    
            var url_h4 = $scope.hacienda != 'COMPARACION' ? 'InspeccionOrodeltiDemo.getH4' : 'InspeccionOrodeltiDemo.getH4Comparativo';
            $http.post('/controllers/index.php', getParams(url_h4, $scope.filters)).then( r => {
                if(r.data){
                    $scope.graficas.m3.h4.init("hoja_4", r.data.h4)
                    $scope.graficas.m3.hojas_4 = r.data.h4
                }
            })
    
            var url_h3 = $scope.hacienda != 'COMPARACION' ? 'InspeccionOrodeltiDemo.getH3' : 'InspeccionOrodeltiDemo.getH3Comparativo';
            $http.post('/controllers/index.php', getParams(url_h3, $scope.filters)).then( r => {
                if(r.data){
                    $scope.graficas.m3.h3.init("hoja_3", r.data.h3)
                    $scope.graficas.m3.hojas_3 = r.data.h3
                }
            })
    
            var url_h5 = $scope.hacienda != 'COMPARACION' ? 'InspeccionOrodeltiDemo.getH5' : 'InspeccionOrodeltiDemo.getH5Comparativo';
            $http.post('/controllers/index.php', getParams(url_h5, $scope.filters)).then( r => {
                if(r.data){
                    $scope.graficas.m3.h5.init("hoja_5", r.data.h5)
                    $scope.graficas.m3.hojas_5 = r.data.h5
                }
            })
            $scope.graficas.m3.loaded = true;
        }, 250)
    }  
    /* END 3M */

    /* BEGIN 0S */
    $scope.getGraficasS0 = () => {
        if($scope.graficas.s0.loaded && $scope.hacienda != 'COMPARACION')
            return;
        if($scope.hacienda == 'COMPARACION') $scope.graficas.s0.loaded = false;

        setTimeout(() => {
            if(!$scope.data_tables){
                $http.post('/controllers/index.php', getParams('InspeccionOrodeltiDemo.getDataTables', $scope.filters)).then(r => {
                    $scope.data_tables = r.data.data || []
                })
            }

            var url_0s = $scope.hacienda != 'COMPARACION' ? 'InspeccionOrodeltiDemo.getHT0S' : 'InspeccionOrodeltiDemo.getHT0SComparativo';
            $http.post('/controllers/index.php',getParams(url_0s,$scope.filters)).then(r => {
                if(r.data){
                    $scope.graficas.s0.ht.init("hojas_total",r.data.ht)
                }
            })
            
            var url_qm0s = $scope.hacienda != 'COMPARACION' ? 'InspeccionOrodeltiDemo.getHVLE0S' : 'InspeccionOrodeltiDemo.getHVLE0SComparativo';
            $http.post('/controllers/index.php',getParams(url_qm0s, $scope.filters)).then(r => {
                if(r.data){
                    $scope.graficas.s0.hmvle.init("libre_estrias_0S",r.data.hmvle)
                }
            })
    
            $scope.graficas.s0.loaded = true;
        }, 250)
    }
    /* END 0S */

    $scope.getGraficasS11 = () => {
        if(!$scope.data_tables){
            $http.post('/controllers/index.php', getParams('InspeccionOrodeltiDemo.getDataTables', $scope.filters)).then(r => {
                $scope.data_tables = r.data.data || []
            })
        }

        if($scope.graficas.s11.loaded && $scope.hacienda != 'COMPARACION')
            return;
        if($scope.hacienda == 'COMPARACION') $scope.graficas.s11.loaded = false;

        setTimeout(() => {
            var url_ht = $scope.hacienda != 'COMPARACION' ? 'InspeccionOrodeltiDemo.getHT11S' : 'InspeccionOrodeltiDemo.getHT11SComparativo';
            $http.post('/controllers/index.php',getParams(url_ht, $scope.filters)).then(r => {
                if(r.data){
                    $scope.graficas.s11.ht.init("hojas_total_11S",r.data.ht)
                    $scope.graficas.s11.hojas_total_S11 = r.data.ht
                }
            })
    
            $scope.graficas.s11.loaded = true;
        }, 250)
    }

    $scope.getFoliar = () => {
        if($scope.graficas.foliar.loaded)
            return;

        $http.post('/controllers/index.php', getParams('InspeccionOrodeltiDemo.getEmisionFoliar', $scope.filters)).then( r => {
            if(r.data){
                $scope.graficas.foliar.foliar.init("foliar", r.data.data)
            }
        })
        $scope.graficas.foliar.loaded = true;
    }

    $scope.getClima = () => {
        if($scope.graficas.clima.loaded)
            return;

        $http.post('/controllers/index.php', getParams('InspeccionOrodeltiDemo.getTempMin', $scope.filters)).then( r => {
            if(r.data){
                $scope.graficas.clima.temp_min.init("temp_min", r.data.data)
            }
        })

        $http.post('/controllers/index.php', getParams('InspeccionOrodeltiDemo.getTempMax', $scope.filters)).then( r => {
            if(r.data){
                $scope.graficas.clima.temp_max.init("temp_max", r.data.data)
            }
        })

        $http.post('/controllers/index.php', getParams('InspeccionOrodeltiDemo.getLluvia', $scope.filters)).then( r => {
            if(r.data){
                $scope.graficas.clima.lluvia.init("lluvia", r.data.data)
            }
        })

        $http.post('/controllers/index.php', getParams('InspeccionOrodeltiDemo.getRadSolar', $scope.filters)).then( r => {
            if(r.data){
                $scope.graficas.clima.rad_solar.init("rad_solar", r.data.data)
            }
        })

        $http.post('/controllers/index.php', getParams('InspeccionOrodeltiDemo.getHorasLuz', $scope.filters)).then( r => {
            if(r.data){
                $scope.graficas.clima.horas_luz.init("horas_luz", r.data.data)
            }
        })

        $http.post('/controllers/index.php', getParams('InspeccionOrodeltiDemo.getRadSolarHoras', $scope.filters)).then( r => {
            if(r.data){
                if(r.data.fecha){
                    $scope.filters.fecha_inicial = r.data.fecha
                    $scope.filters.fecha_final = r.data.fecha
                    $("#date-calendar").html(`${r.data.fecha} - ${r.data.fecha}`)
                }
                $scope.graficas.clima.rad_solar_horas.init("rad_solar_horas", r.data.data)
            }
        })
        $scope.graficas.clima.loaded = true;
    }
    
    $scope.changeLuz = () => {
        $http.post('/controllers/index.php', getParams('InspeccionOrodeltiDemo.getHorasLuz', $scope.filters)).then( r => {
            if(r.data){
                $scope.graficas.clima.horas_luz.init("horas_luz", r.data.data)
            }
        })
    }

    $scope.getAplicaciones = () => {
        $http.post('/controllers/index.php', getParams('InspeccionOrodeltiDemo.getAplicaciones', $scope.filters)).then( r => {
            if(r.data){
                $scope.aplicaciones = r.data.data
            }
        })
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
            $scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
        }
        
        $http.post('/controllers/index.php', getParams('InspeccionOrodeltiDemo.getRadSolarHoras', $scope.filters)).then( r => {
            if(r.data){
                if(r.data.fecha){
                    $scope.filters.fecha_inicial = r.data.fecha
                    $scope.filters.fecha_final = r.data.fecha
                    $("#date-calendar").html(`${r.data.fecha} - ${r.data.fecha}`)
                }
                $scope.graficas.clima.rad_solar_horas.init("rad_solar_horas", r.data.data)
            }
        })
    }

    /* END 0S */
    $scope.init = () => {
        $scope.getGerentes()
        $scope.getTablaPorLote()
    }

    $scope.exportExcel = function(id_table, title){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var contentTable = table.innerHTML
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(id_table, "Información de Transtito")
    }

    $scope.exportPrint = function(id_table){
        $("#"+id_table).css("display", "")
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;
        newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
        $("#"+id_table).css("display", "none")
    }

    $scope.init()

    $scope.subir = () => {
        if(!$scope.isSet){
            alert("Necesita seleccionar un archivo .xlsx primero")
        }else{
            var ext = $scope.file.name.split(".")
            ext = ext[ext.length-1]
            if(ext !== "xlsx"){
                alert("El archivo no es .xlsx")
            }
            else{
                $http({
                    method: 'POST',
                    url: '/controllers/index.php?accion=InspeccionOrodeltiDemo.importar',
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },
                    data: {
                        upload: $scope.file
                    },
                    transformRequest: function (data, headersGetter) {
                        var formData = new FormData();
                        angular.forEach(data, function (value, key) {
                            formData.append(key, value);
                        });
        
                        var headers = headersGetter();
                        delete headers['Content-Type'];
        
                        return formData;
                    }
                })
                .success(function (data) {
                    alert("Se subio correctamente", "Importar", "success", () => {
                        location.reload();
                    })
                })
                .error(function (data, status) {
                    alert("Ocurrio un error, intente nuevamente mas tarde")
                });
            }
        }
    }

    $('#input-file').bind('change', function() {
        $scope.isSet = true
        //alert('This file size is: ' + this.files[0].size/1024/1024 + "MB");
    });
}]);
