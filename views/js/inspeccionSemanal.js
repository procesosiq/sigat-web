getParams = (accion, data) => {
    data = data || {}
    var params = {
        accion : accion
    }
    Object.keys(data).map((value, index) => {
        params[value] = data[value]
    })
    return params
}

app.controller('inspeccion', ['$scope','$http', 'client', function($scope, $http, client){
    
    $scope.filters = {
        year : moment().year(),
        semana : moment().week(),
    }
    $scope.data = {}
    $scope.hacienda = "<?= Session::getInstance()->finca ?>"
    $scope.id_cliente = "<?=  Session::getInstance()->id_client ?>"
    $scope.id_cliente_original = "<?=  Session::getInstance()->id_client ?>"

    var graficas = {
        m3_h3 : new echartsPlv(),
        m3_h4 : new echartsPlv(),
        m3_h5 : new echartsPlv(),
        m3_hvlq : new echartsPlv(),
        m3_hvle : new echartsPlv(),
        s0_ht : new echartsPlv(),
        s0_hvle : new echartsPlv(),
        s0_hvlq : new echartsPlv(),
        s11_ht : new echartsPlv(),
        s11_hvlq : new echartsPlv(),
    }

    $scope.save = () => {
        if(!$scope.hacienda > 0){
            alert("Seleccione una hacienda")
            return;
        }

        if($scope.hacienda != 'COMPARACION'){
            var data = {
                id_hacienda : $scope.hacienda,
                accion : "indexMain.saveFinca"
            }
            client.post("./controllers/index.php?accion=indexMain.saveFinca",function(r , b){
                b();
                window.location.reload();
            } , data);
        }else{
            if($("#tab_3m").attr('aria-expanded') != 'false'){
                $scope.getGraficas3M()
            }
            if($("#tab_0s").attr('aria-expanded') != 'false'){
                $scope.getGraficasS0()
            }
            if($("#tab_11s").attr('aria-expanded') != 'false'){
                $scope.getGraficasS11()
            }
        }
    }

    $scope.getLast = () => {
        return new Promise((resolve, reject) => {
            var data = {
                id_client : $scope.id_cliente,
                accion : "InspeccionSemanal.last"
            }
            client.post("./controllers/index.php?accion=InspeccionSemanal.last", function(r , b){
                b();
                if(r){
                    $scope.filters.semana = r.semana
                    resolve()
                }else{
                    reject()
                }
            }, data);
        })
    }

    $scope.getFincas = (reload) => {
        if(reload) $scope.hacienda = 0

        var data = {
            id_client : $scope.id_cliente,
            accion : "indexMain.getFincasDisponibles"
        }
        client.post("./controllers/index.php?accion=indexMain.getFincasDisponibles", function(r , b){
            b();
            if(r){
                $scope.haciendas = r;
                $scope.hacienda = r[0].id
                $scope.save()
            }
        }, data);
    }

    $scope.getSemanasFincas = () => {
        var data = {
            accion : "InspeccionSemanal.getSemanasFincas"
        }
        client.post("./controllers/index.php?accion=InspeccionSemanal.getSemanasFincas", function(r , b){
            b();
            if(r){
                $scope.semanas = r;
            }
        }, data);
    }
    $scope.getSemanasFincas()

    $scope.init = () => {
        var data = {
            accion : "InspeccionSemanal.dataGraficas",
            semana : $scope.filters.semana
        }
        client.post("./controllers/index.php?accion=InspeccionSemanal.dataGraficas", function(r , b){
            b();
            if(r){
                $scope.fincas = r.fincas
                printGraficasInspeccion(r)
            }
        }, data);
    }
    printGraficasInspeccion = (data) => {
        printGrafica(data.m3_h3, 'grafica-m3-h3', '3M - EE HOJA 3')
        printGrafica(data.m3_h4, 'grafica-m3-h4', '3M - EE HOJA 4')
        printGrafica(data.m3_h5, 'grafica-m3-h5', '3M - EE HOJA 5')
        printGrafica(data.m3_hvlq, 'grafica-m3-hvlq', '3M - H+VLQ<5%')
        printGrafica(data.m3_hvle, 'grafica-m3-hvle', '3M - H+VLE')
        printGrafica(data.s0_ht, 'grafica-s0-ht', 'S0 - HT')
        printGrafica(data.s0_hvle, 'grafica-s0-hvle', 'S0 - H+VLE')
        printGrafica(data.s0_hvlq, 'grafica-s0-hvlq', 'S0 - H+VLQ<5%')
        printGrafica(data.s11_ht, 'grafica-s11-ht', 'S11 - HT')
        printGrafica(data.s11_hvlq, 'grafica-s11-hvlq', 'S11 - H+VLQ<5%')
    }

    printGrafica = (data, id, titulo) => {
        if(data && id){
            $scope.data[id] = data
            var options = {
                series: data.data,
                legend: data.legend,
                umbral: data.umbral || null,
                id: id,
                titulo : titulo || "",
                min : 0,
                max : data.max || null
            }
            ReactDOM.render(React.createElement(Historica, options), document.getElementById(id));
        }
    }
    $scope.getLast().then(( ) => {
        $scope.init()
    })

}]);
