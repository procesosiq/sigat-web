var descripcion_data;
var introduccion_data;
var ComponentsEditors = function () {

    var handleSummernote = function () {
        grid.msjStatus.inactive = 'BORRADOR';
        grid.msjStatus.active = 'FINALIZADO';
        grid.msjButton = 'VER INFORME';
        grid.initOrder.order = 'desc';
        grid.init("#list_informe" , "../controllers/index.php?accion=Informes");

        $('#list_informe tbody').on( 'click', 'button', function () {
             var data = grid.tableEdit.row( $(this).parents('tr') ).data();
             if(data.length > 0){
                // document.location.href = "/newFinca?id=" + data[1];
                ahttp.post("../controllers/index.php?accion=Informes.getUrl" , getInfo , { id: data[1] } );
             }

             function getInfo(r , b){
                b();
                if(r){
                   window.open(r);
                }
            }
        } );
    }

    return {
        init: function () {
            handleSummernote();
        }
    };

}();

jQuery(document).ready(function() {    
   ComponentsEditors.init(); 
});

app.controller('informe', ['$scope','$http', function($scope,$http){

}]);
