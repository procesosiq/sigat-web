$.fn.dataTable.pipeline = function ( opts ) {
    // Configuration options
    console.log(opts);
    var conf = $.extend( {
        pages: 5,     // number of pages to cache
        url: '',      // script url
        data: null,   // function or object with parameters to send to the server
                      // matching how `ajax.data` works in DataTables
        method: 'POST' // Ajax HTTP method
    }, opts );
 
    // Private variables for storing the cache
    var cacheLower = -1;
    var cacheUpper = null;
    var cacheLastRequest = null;
    var cacheLastJson = null;
 
    return function ( request, drawCallback, settings ) {
        var ajax          = false;
        var requestStart  = request.start;
        var drawStart     = request.start;
        var requestLength = request.length;
        var requestEnd    = requestStart + requestLength;
         
        if ( settings.clearCache ) {
            // API requested that the cache be cleared
            ajax = true;
            settings.clearCache = false;
        }
        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {
            // outside cached data - need to make a request
            ajax = true;
        }
        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||
                  JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||
                  JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )
        ) {
            // properties changed (ordering, columns, searching)
            ajax = true;
        }
         
        // Store the request for checking next time around
        cacheLastRequest = $.extend( true, {}, request );
 
        if ( ajax ) {
            // Need data from the server
            if ( requestStart < cacheLower ) {
                requestStart = requestStart - (requestLength*(conf.pages-1));
 
                if ( requestStart < 0 ) {
                    requestStart = 0;
                }
            }
             
            cacheLower = requestStart;
            cacheUpper = requestStart + (requestLength * conf.pages);
 
            request.start = requestStart;
            request.length = requestLength*conf.pages;
 
            // Provide the same `data` options as DataTables.
            console.log(conf);
            if ( $.isFunction ( conf.data ) ) {
                // As a function it is executed with the data object as an arg
                // for manipulation. If an object is returned, it is used as the
                // data object to submit
                var d = conf.data( request );
                if ( d ) {
                    $.extend( request, d );
                }
            }
            else if ( $.isPlainObject( conf.data ) ) {
                // As an object, the data given extends the default
                $.extend( request, conf.data );
            }

            settings.jqXHR = $.ajax( {
                "type":     conf.method,
                "url":      conf.url,
                "data":     request,
                "dataType": "json",
                "cache":    false,
                "success":  function ( json ) {
                    cacheLastJson = $.extend(true, {}, json);
 
                    if ( cacheLower != drawStart ) {
                        json.data.splice( 0, drawStart-cacheLower );
                    }
                    json.data.splice( requestLength, json.data.length );
                    $('#datatable_ajax tbody').off( 'click', 'button');
                    $('#datatable_ajax').off('click', '.filter-submit');
                    $('#datatable_ajax').off('click', '.filter-cancel');
                    drawCallback( json );
                }
            } );
        }
        else {
            json = $.extend( true, {}, cacheLastJson );
            
            json.draw = request.draw; // Update the echo for each response
            json.data.splice( 0, requestStart-cacheLower );
            json.data.splice( requestLength, json.data.length );
            $('#datatable_ajax tbody').off( 'click', 'button');
            $('#datatable_ajax').off('click', '.filter-submit');
            $('#datatable_ajax').off('click', '.filter-cancel');
            drawCallback(json);
        }
    }
};

$.fn.dataTable.Api.register( 'clearPipeline()', function () {
    return this.iterator( 'table', function ( settings ) {
        settings.clearCache = true;
    } );
} );

var TableDatatablesAjax = function () {

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }

    var handleRecords = function () {
        var grid = new Datatable();
        grid.params = {};
        grid.init({
            src : "#datatable_ajax",
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Cargando...',
            dataTable: {
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "Todos"] // change per page values here
                ],
                "language": {
                    "lengthMenu": "Vista de _MENU_ registros por página",
                    "zeroRecords": "No se encontro ningun registro",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "aria": {
                        "sortAscending": ": orden acendente",
                        "sortDescending": ": orden decendente"
                    },
                    "emptyTable": "No hay registros en la tabla",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Ultimo",
                        "next":       "Siguiente",
                        "previous":   "Anterior",
                        "page": "Página",
                        "pageOf": "de"
                    },
                    "select": {
                        "rows": {
                            _: " %d filas selecionadas",
                            0: "",
                            1: "1 fila seleccionada"
                        }
                    }
                },
                "pageLength": 10,
                "ajax":  $.fn.dataTable.pipeline({
                    url: "../controllers/index.php?accion=Ciclos2.indexProductos", // ajax source
                    pages: 10, // number of pages to cache,
                    data : function(){
                        var table = $('#datatable_ajax');
                        grid.params = {};
                        $('textarea.form-filter, select.form-filter, input.form-filter:not([type="radio"],[type="checkbox"])', table).each(function() {
                            var _self = $(this);
                            var name = _self.attr("name");
                            var data = [];
                            if (!data[name]) {
                                data[name] = _self.val();
                            }
                            $.extend( grid.params, data);
                        });

                        // get all checkboxes
                        $('input.form-filter[type="checkbox"]:checked', table).each(function() {
                            var _self = $(this);
                            var name = _self.attr("name");
                            var data = [];
                            if (!data[name]) {
                                data[name] = _self.val();
                            }
                            $.extend( grid.params, data);
                        });

                        // get all radio buttons
                        $('input.form-filter[type="radio"]:checked', table).each(function() {
                            var _self = $(this);
                            var name = _self.attr("name");
                            var data = [];
                            if (!data[name]) {
                                data[name] = _self.val();
                            }
                            $.extend( grid.params, data);
                        });
                        var data = {
                            data : grid.params
                        }

                        return grid.params;
                    }
                }),
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [ -1 ],  
                        "orderable": false
                    }
                ],
                "order": [
                    [0, "asc"]
                ],
                "buttons": [
                    { extend: 'print', className: 'btn dark btn-outline', "title" : "Listado de Productos", "text" : "Imprimir",
                        customize: function ( win ) {
                            $(win.document.body)
                                .css( 'font-size', '10pt' )
                                .css( 'float', 'rigth' )
                                .prepend(
                                    '<img src="http://sigat.procesos-iq.com/15040.png" />'
                                );
         
                            $(win.document.body).find( 'table' )
                                .addClass( 'compact' )
                                .css( 'font-size', 'inherit' );
                        },
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    { extend: 'pdfHtml5',"text" : "PDF", "title" : "Listado de Productos" ,className: 'btn green btn-outline',
                        customize: function ( doc ) {
                            var cols = [];
                               cols[0] = {text: 'Left part', alignment: 'left', margin:[20] };
                               cols[1] = {text: 'Right part', alignment: 'right', margin:[0,0,20] };
                               var objFooter = {};
                               objFooter['columns'] = cols;
                               doc['footer']=objFooter;
                               doc.content.unshift(
                                {
                                    margin: [0, 0, 0, 12],
                                    alignment: 'left',
                                    image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAoCAYAAAAcwQPnAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAABCkSURBVHja7Jx5dFXVvcc/+5x77pyRzDMBwmjyQKWAAta+Wp/Poa0VtRWlEimtVlsrTo1PEMVVpMpTBqkGqoaIVgsyCQoyiTLJIJCEMIUhCZAJMt3pnLPfH/difeuVDBBewXW/a92Vu27O+e29f7/v+U173yuklJVACtBEGGFcOCKAKkvoDd/6G0YYF0wuJayDMC4GwsQK46LAcilMwuNrovrkYXaXrMfpiMEZEcWWHYu4ZtBtDB54S9hKlyGElLLxX5Vf7d73GcWLn6fF4+doVS3oZxBCcsZjIpQWot0unnpgPlcN/GHYUpcXmv6lofCjVTPYtmcdJ2tqMA0LbncyCXFpxMWkkZWai8Xq5vnZ97B77+awqcI5VsdQcXwPh46VkpLQH4slEofdBihYLAoOm4rfb+CwJhFQFKa+8WtO150JWytMrPaxbvPfqamrBewYpsSUAtMEBBiAL2Di9QWIi+zO8fpyXpv3VNhaYWK1jTONdXy+bSkWLRqPT+L3S7xeE6/fRAgQIkgyw5T4/QHSU3qy8esP2LRpQ9hi362qMMCa3cv4/PNN1NWeRgI2p4P07ExGDr6e3NRcvirbgaEbDB5wVbvSSg5s4kT9cSJcyQT8EkUJeSoTVBUEYJpgsQgQIKQdxaHz6YZihgwZ3jWVqKeVNWvWsGXLVqqrq/B4vChCweVykZiUQF5eHsOHjyA2NhaA5uZmtmzZwrBhw7Db7RfNIFVVVWzatAmPx8OQIUPo0aPHBctcsmQJs2bNJD8/n5yc3lgsFnQ9gGGY31wjpYnL5cZmsyGEwO/30dTUBCgIEfJCikBRFDweL3v27KaoqIjf//4P3Hzzf3aeWKvL3+LpggKaj1q4Km8YKYlpaJqFprpmPt27iqlzphHTK4nyPQe5u/dPGPz63Pbzq8oSdCOAqiqgAQgURSIARQGLBWw2gaoKJBLDMIlPSGH3/o0cKK2gZ9+s81by2rVrmT79FVZ9uoqIiEj69+9P9+7ZJCYkoigKzc3N7Ni+k79/uJDq6iri4uMYOXIk+/btY+2adWzdupVBVw66aMT6r/96hsLCoA5vuukmli1bdsEy6+rqWLFiJStWrATA7XITExODpmlIKUOkUUBIdF3HNE0URcGiaphmkHzBKGLS0FDPmcbGb2SPHPH9zhOreNdj/PbXf+b7sfewYPU7WFz/5Ak7WMVdL/4cX/lpbIOcHXsqTx4GacU0BVJIpBGcuKYJLBaBogJqMFALBKoiiIpwcfLAMUrKdp8XsbZu3cLvfvc7vvjiS5wOJ5MnP8+4X43D7XadOw9cu45JkyYye/brIYO4aPV4Lhqpysv3MW/ePFRVJS0tleXLl7N48WJuvfXWC5IbEeEGIKdXbx588DdceeWVREdHB4kF2DQrNruN/PyxLP/44xBhRjJ/fjG6HsDr9QaJJSWe1laOHj1KYWEhS5YuweVydi4Ubqp4l7nL/swNvW/jrZf/OakAUnqksP7NtSTd3J2D1Yc6tNCTddVINPx+iSlBmhIpwZQSUFEEGLpEWEBVBEKAz2vi1yVen7fTip00aSITJ04C4I7b7+Dtd97G7mg/nI28biSfXbeGJYuX8JOf/hiv14vVar1oxJo2bRqxMd0YcMUA9u7dA8Abf/nLBROrvHw/ACtXriSre+Y5r4uOiUbTLOiGTkJCAqlpKf/0uoGDBnLbj2/j6quvZueuXZ1L3kuPb8DtFPQamIy1A+3T8TeMRQuoHVqozW7BZlOw2wV2WzDsWTSBKQEZ9FyKAGkGcy3DkOi6AMWk1dPYKaWOHTv2G1LdNepu3v/g/Q6R6tu45dZbePXV19ANAz0QuCikOnToEG+88SYvv/wKz058lpqaWtLT0/jk00/ZsOHCipbrrruO12fPaZNUwTwrFBaFgj/gb1fuzBkz+dntt3fOY+lGgISYVI40buCLvasZlveDNgd5Mv8x6n9S14FlmrgcEBmp4LILTDNYAeq6REqB1QoeHSRBcgkBUoJqEbQ26nhbRIcVWlDwR+bOnYvdbiMrM5tXpr983sYZPXo0016aRmVl5UUh1tSXpgbJf9edaFaNvLxcDhw4gN/vZ/asWQwffv5Fy9ChQxk6dGiXz3nw9wZ3vt0Q7+6BL9CKM1Jj+prfsHRbcZuD2J12UtJT2281NNVwpPowhmHD44NWr8Tjk/gCwapD00QogRfYbALNGvzMNA1iE5zEJmodWvS2bduYOvVPxMd3w+v1cd+995KUnHQBeUoEyz/+mO8NGdLlBjp9+jRzXp/DL8fcj2YNru+BBx6gpaWVtLRUPvz7h2zc+MV3o481PPs+rJ4kfGYjEe5I/rTot4ybdQuf7PgIwzTOe8CoiESy04ZRW19PIACGDroeDHcy1GbQLAKnS2C1CWw2BYdTQVMt9BzQja0H53Gson2vUVhYSCBgYLXaiI6K5vvXX3/ByurTpzcZGeldboSXQt7qqSef/FYIzyc39woaGxvx+wPMm1v43SBWt5hkHrm+iIZSK0cq99MzuycVdaU8WngfP3vxB8z9ZA5nWs5vm+VH144iNtqFzapjswlsGtitAps16K0Mk2BSDwgFFBU0TcHpcLOnbBvLVyxsU35TUxMbN35OZGQELa0tJKckk53d/ZI0QHNzE1OmvMion42iV+9e/4gAdjuj7ryTxsYmkpMTWbRoEaWlZd+NzvuA/gN5dfxnDLaOZu+6IzQ01JPVPYuTzZX88e0/MPLxYUwufp66xvpOeq1uCMVOq1fHHxCYUhAwIKCDqpxNJIP5VyAAPj8EdIluSkzDRktT25VhRcVhTp48gcvlwtB13E4XTpfzkjTAjBkzgl5r2rT/878Jj01gxIjheDxe6urrmTlzxnen856encyUp17j9o35vLv6DdZsXUa9VktGRjreZoPJ85/j9aVzef7e5/jljfd0aFDD0EGaKIqKZgl1dE2JooJQBIoqMQyJIWUwgVdAmgIpTZrrIDG+7T5Wa2srPp+fiAgbqqri9Xnx+/y4XK4ONRNra2txuVzExMTgcDgwDOObJqIpTWpqapCmJDU19YIN8M47bxMdFUXh3Dfx+bwEAnqwxDFNUlKSCQR0AoEASUkJvPtuMQ8//DA5OTmXP7HO4spr8rjymhlUlDxJ8Yp5vLexiCPGEXpl9+R0XSv3F4xm0+6vmDPhlXZllRzciCGbiXQlIaUMbj4jCFW7CIK9LUUJNksFCnabQnNzDQ5SueaakW17xKho7HY7hmHgcDqpPnGCY8eOERMb0+7cjhw5wgvPv4Cu65SWleJw2ImOjsY0TQxDp7rqJHa7gxEjhjN9+nQcTsd5K3/uvEJKSsq4+qrBzC+aT2trK6qqflP6C6GQmJRIbGwsmqZx4sRh3vrrW7ww5YXvDrHOIqtfGk/3e4ZxdzzI7AWvMXPdLHS3QtaAAfxl3nQGpvRn/C/y25SxvWwNoCIUFaREVUK5lAKmGWSXZlVQLSCQKFIhItrKmhVH6dPtTrJ6xbeTZPchMzOLXbt2kpaWxsGqQ3y56Uty83LbXd+gQYMoLCzEarXxt7+9R/4D+UgpsdvttLS0ctstP+alaS8RGRWJUMQFKf+Zgmfo07svW7YGz5vJ0NrP4qz88ePHM2fOHLp1i6X43WIefuQREhMTLt8cqy3EpcfyzIRn+WzKGuLUbjQ010NqFLPnzwGPbPPe6MhuqBYTVQFVDW7jOOwCl0tBVQWGQahZGsq1VJOKoyfwVqUzbuzDHZrf7bf/FJ/Pj6Io2O1Wiovnd3ht0THROF0O7hszhrvv/jlCCGw2Kz2ye/Dqa/9Nr5yeJCYmXNBm9IcffkBVVTUFBQX/i0jffp3Fgw8+RFRUFHa7jYqKCmbPnnV5Ju9+Gpi3eDLVx2vbFdAvtx9vPjEHDQuK3cqJU7UcP3q0zXt0vRUhVIQQoVfwc1UJNkQVi8BuE8GQKMFqhRMVp7jrp/kMu65jG8ATJjxObm4u+/cfIDMri/XrNzAp1IHv1AMUF4dhGPj9AWJiYoiLi+sSxU+ePJmMtAx+cc/P2732iisGcOedo6isrCYyMoIFCxbQ0txy+RHLiov1e9/ly+2rOyRkUM5AemXnYB6vIckSS/I59pjOIjYqDql4sFgVbDawagJFFcF8Swb3B+1WcNqDLQhNSFKSrWT0iOzwwoQQLF26lIyMDPaVlZOYmMDESRMpKirqlIJEiPVCCAzDINAFWzpLly5h166vmfTcc53o/N+L1aoRFRXFvn3BzequxtmHXEqJqqhdTyywkj0gjr2B4mAzqR2UntrH9iO7YA/89u5fobra7o737zEcm8XEqkksFoGqgdUCFlWgmxJFCZ7R0jRw2IJVY3xqBF+VfkzDKV+HF5eens6WLVu44Yc3cPLkqZCBRjNx4rOdUDbfFBVSyo6oo10UFBSQnprOmF/e1+F7rr32WsaNG0d1dTUOh53Zr8+mpaVrvdbp06dRVQuKoqDr+sUgliQxIYlVuxYz5s9XU9Gw55wCvj71NT986Ef43q/h6QefJv+Rce0OmtfrRlLjcmj2VCPM0FlkwJDBLjxCIE3QjWBCb9UUIuNcbNq0jfJ9+zu1wMTERFZ+spK3/voWffv2BWDSpOdITU1h5qwZ1NWfe3+zpKSElStXEhMdRSDgx+fz/YNl5wGvz8vo0fewa9fX9O3ft9P3JyQkoOsGUVGRlJSU8NBDD3UZqd57bwGrVq/CZrOhaRqbN3/Jnj17zt/7nevrX/c+PpRDW3Wu+F4vjtXtJ7lbNmkp3YmOigUhONVQw+adW1m9ei1ZEdm8VjCdm+/u+HcA129+n3nLxpKW0gfdZwuW8yZgSpTQ6QbTBLtdIT5VZdH8zbhb/oOPli4878X6/X6WL1vOihUrWLZsGccrj6NpGn379CGre3eio6NQFBVPayvl+/ezc+dOpJQoobNI3TO7s33HdqJjojs1rq7r/OHRRykqmk99Qz3dYmOxWDTi4+N5/PHHGX3v6DbvX7hwIVOnTmV/eXno3JqGrhvU1deRe0UuGRmZTJjwGNcOv7bTOlm0aBEvT3uZr7Z/haIIoqIikTLoveLi4hkwYABPPPEEI0aO6IzYpnMSa+2nm0hJTSKnXxYN1c2s3/wZpQe/prqmmsbGRnx+nTh3PDeNuIkbb7kxdBK0k83Bv01i+eZJ5PTuh4VuNDYGkIZEqkFiRcdo2CMCLC7eTn1Zbz5atILsnOQueUJN06SstIwd27dTWlZGdXU1LS0toRxK4Ha7ycrMJDEpCYfDgdvtJjMzg6uuvip42rKT2LjxC1qamklNS0VVVfx+PwcOHCQ5JandkweHDx9m546dZGRk4HQ6MQwDRVEwDIMTJ05SX1dH3r/l0advn07P6/Dhw2z/agcZGem43W4Mw0CI4BHk2tpaKisryc3NpV//fl1DrP8XSHineDYfrXmR+GwvcQk98TSAM0JFdSg0nTnFsqIyesTcwdx5hSSkhH+35DLBv5hYIRwoq6JoYQEb9nxAr36ZRDh1tqw7Sm1FLLf9+3iem/JHLNawtcLEOg8Yfli74TMWLJnCvl01jBh8B2PG5NOzb1LYTGFiXTgmPDqGvNybuGfMqLB5LmNiXXo/Y2RpITLGHjbNZY5LjlgH91dSX98QtsxlDsulNqH773+azMzMsGUuc1xyOVYY4RwrjDDaDIVNIY8V/jnuMLoCEUDT/wwAa2F+2VijHr4AAAAASUVORK5CYII='
                                }
                               );
                        },
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    { extend: 'excel', "text" :"Excel","title" : "Listado de Productos" ,className: 'btn yellow btn-outline ',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    { extend: 'csv',"text" :"CSV", "title" : "Listado de Productos" , className: 'btn purple btn-outline ',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        text: 'Nuevo Producto',
                        className: 'btn blue btn-outline',
                        action: function ( e, dt, node, config ) {
                            document.location = "/newProducto"
                        }
                    }
                ],
                select: false,
                "bSort": true,
                "processing": true,
                "serverSide": true,
                "dom": "<'row' <'col-md-12'B>><'row'><'row'<'col-md-6 col-sm-12'l>r><'table-scrollable't><'row'<'col-md-6 col-sm-12'i><'col-md-6 col-sm-12'p>>",
                "fnDrawCallback": function( oSettings ) {
                    var count = 0;
                    function details (event) {
                        event.preventDefault();
                        event.stopPropagation();
                        
                        var btnid = $(this).prop("id");            
                        var data = grid.getDataTable().row( $(this).parents('tr') ).data();

                         if(data.length > 0){
                            if(btnid == "edit"){
                                document.location.href = "/newProducto?id=" + data[0];
                            }
                            else if(btnid == "status"){
                                if(confirm("¿Esta seguro de cambiar el registro?")){
                                     $.ajax({
                                        type: "POST",
                                        url: "controllers/index.php",
                                        data: "accion=Ciclos2.changeStatusProducto&id="+data[0],
                                        success: function(msg){
                                            alert("Registro cambiado" , "Productos" , 'success' , function(){
                                                $('#datatable_ajax tbody').off( 'click', 'button',  details);
                                                $('#datatable_ajax').off('click', '.filter-submit');
                                                $('#datatable_ajax').off('click', '.filter-cancel');
                                                grid.getDataTable().clearPipeline().draw();
                                            });
                                        }
                                    });
                                }
                            }
                         }
                    }
                    $('#datatable_ajax tbody > button').unbind('click', details)
                    $('#datatable_ajax_tools > li > a.tool-action').on('click', function(event) {
                        event.preventDefault();
                        var action = $(this).attr('data-action');
                        grid.getDataTable().button(action).trigger();
                    });

                    $('#datatable_ajax tbody').on( 'click', 'button',  details);
                    
                    $('#datatable_ajax').on('click', '.filter-submit', function(e) {
                        grid.getDataTable().clearPipeline().draw();
                    });

                    $('#datatable_ajax').on('click', '.filter-cancel', function(e) {
                         $('textarea.form-filter, select.form-filter, input.form-filter', $('#datatable_ajax')).each(function() {
                            $(this).val("");
                        });
                        $('input.form-filter[type="checkbox"]', $('#datatable_ajax')).each(function() {
                            $(this).attr("checked", false);
                        });
                        grid.getDataTable().clearPipeline().draw();
                    });
                 }
            }
        });

        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });

        grid.setAjaxParam("customActionType", "group_action");
        grid.getDataTable().ajax.reload();
        grid.clearAjaxParams();

        $('#datatable_ajax_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            grid.getDataTable().button(action).trigger();
        });        
    }

    return {
        //main function to initiate the module
        init: function () {
            initPickers();
            handleRecords();
        }
    };
}();

jQuery(document).ready(function() {
    TableDatatablesAjax.init();
});