'use strict';

app.service('request', function ($http) {
    var service = {};
    var options = {
        url: "./controllers/index.php",
        method: "POST"
    };

    service.get = function (callback, filters) {
        var option = options
        filters.accion = "RevisionTecnico.listado"
        option.data = filters
        load.block()
        $http(option)
        .then((r) => {
            load.unblock()
            callback(r.data)
        })
    };

    service.delete = function(callback, params){
        var option = options
        params.accion = "RevisionTecnico.delete"
        option.data = params
        $http(option)
        .then((r) => {
            callback(r.data)
        })
    }
    return service;
});

app.controller('tecnicos', ['$scope', 'request', function ($scope, $request) {

    $scope.init = ( ) => {
        $scope.get()
    }
    
    $scope.get = ( ) => {
        $request.get((r) => {
            $scope.data = r.data
        }, {})
    }

    $scope.borrar = (row) => {
        if(confirm('¿Estas seguro de borrar este registro?')){
            $request.delete((r) => {
                if(r.status === 200){
                    alert('Eliminado con éxito', '', 'success')
                    $scope.get()
                }else{
                    alert('Ocurrio algo inesperado, contacte a soporte')
                }
            }, {
                id : row.id
            })
        }
    }

    $scope.init()

}]);