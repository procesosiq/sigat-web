// /*==============================================
// 	=            Modificacion del Alert            =
// 	==============================================*/
	window.old_alert = window.alert; 
	window.delay = 1000;
	window.alert = function(msj , categoria , typeMsj , callback){
		var titulo = typeof categoria == "undefined" ? "Notificacion" : categoria;
		var method = typeof typeMsj == "undefined" ? "error" : typeMsj;
		// var position = method == "error" ? "toast-top-full-width" : "toast-top-right";
		var functionCallback = typeof callback == "undefined" ? null : callback;
		var delay = window.delay;
		toastr.options = {
		  "closeButton": true,
		  "positionClass": "toast-top-full-width",
		  "onclick": functionCallback,
		  "showDuration": "1000",
		  "hideDuration": delay,
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}

		toastr[method](msj, titulo);

		if(callback){
			setTimeout(callback, delay);
		}
	}
			
	
// 	/*=====  End of Modificacion del Alert  ======*/

	var app = angular.module('app', []);

	// app.controller('menu', ['$scope', function ($scope) {
		
	// 	$scope.showNav = function(){
	// 		var content = $("#menuSidebar > li#catalogos");
	// 		var subconetent = $("#menuSidebar > li#catalogos > ul.sub-menu");
	// 		if(!content.hasClass('open')){
	// 			content.addClass('open');
	// 			subconetent.css('display', 'block');
	// 		}else{
	// 			content.removeClass('open');
	// 			subconetent.css('display', 'none');
	// 		}
	// 	}
	// }]);

	var load = {
		block : function(tableList){
		var config = (tableList && tableList != "") ? { target: '#' + tableList, animate: true } : { animate: true };
		App.blockUI(config);
		},
		unblock: function(tableList) { 
			var config = (tableList && tableList != "") ? '#' + tableList : '';
			App.unblockUI(config) 
		}
	};

	/////////// PLUGINS JQUERY
	$.fn.serializeObject = function() {
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};


	function ahttp (){
        var _this = this;
        _this = {
        url : "",
        method : "",
        header : {},
        data : {},
        callback : "" ,
        required : function(){
            if(!this.url) throw Error("url es Requida");
            if(!this.method) throw Error("Metodo es Requida");
            if(!this.dataType) this.dataType = 'json';
        },
        block : function(){
            load.block(ahttp.target);
        },
        unblock: function() { load.unblock(ahttp.target);  }
        }

        function call(url , method , callback,  data, header , dataType){
            _this.url = url;
            _this.method = method;
            _this.header = header;
            _this.data = data;
            _this.callback = callback;
            _this.required();
            _this.block();
            var request = $.ajax({
                url: _this.url,
                method: _this.method ,
                data: _this.data,
                dataType: _this.dataType
            });

            request.success(function( r ) {
                console.log(r);
                callback(r , _this.unblock);
            });

            request.fail(function( jqXHR, textStatus ) {
            	load.unblock(ahttp.target);
                throw Error(jqXHR);
            });
        }

        ahttp.target  = "";

        ahttp.prototype.get = function(url , callback, data , header , dataType){
            return call(url , "get" , callback, data , header , dataType)
        }
        ahttp.prototype.post = function(url , callback, data , header , dataType){
            return call(url , "post" , callback, data , header , dataType)
        }
        ahttp.prototype.put = function(url , callback, data , header , dataType){
            return call(url , "put" , callback, data , header , dataType)
        }
        ahttp.prototype.delete = function(url , callback, data , header ,dataType){
            return call(url , "delete" , callback, data , header , dataType)
        }
    };


	////////// FACTORY PARA LAS PETICIONES HTTP
	app.service('client', function ($http) {
		var _this = this;
		_this.debug = false;
		_this.params = {
			url : "",
			method : "",
			header : {},
			data : {},
			callback : "" ,
			required : function(){
				if(!this.url) throw Error("url is Required");
				if(!this.method) throw Error("Metodo is Required");
				if(!typeof this.callback == 'function') throw Error("Callback is Required");
			},
	    block : function(){
	      load.block(_this.target);
	    },
	    unblock: function() { load.unblock(_this.target); }
		}

		function call(url , method , callback,  data, header){
			_this.params.url = url;
			_this.params.method = method;
			_this.params.header = header;
			_this.params.data = data;
			_this.params.callback = callback;
			_this.params.required();
			_this.params.block();

			var dataConfig = {};
			dataConfig.url = _this.params.url;
			dataConfig.method = _this.params.method;
			dataConfig.header = _this.params.header;
			if(dataConfig.method == "get" ){
				dataConfig.params = _this.params.data;
			}else{
				dataConfig.data = _this.params.data;
			}
			// console.log(dataConfig);
			$http({
	                url : dataConfig.url  , 
	                method : dataConfig.method ,
	                headers : dataConfig.header,
	                data : dataConfig.data,
	                params : dataConfig.params
	            }).then(function(r){
	            	// console.log(r);
	            	// console.log(r.data);
				    callback(r.data , _this.params.unblock);
				}, function(result){
					CallbackError( _this.params.unblock , result);
				})
		};

		function CallbackError(block , error){
			block();
			if(_this.debug){
				if(error.status == 500 ){
					throw new Error(error.statusText);
				}
			}
		}

		_this.target  = "";

		_this.get = function(url , callback, data , header){
			if(angular.isObject(data)){

			}
			return call(url , "get" , callback, data , header)
		}

		_this.post = function(url , callback, data , header){
			return call(url , "post" , callback, data , header)
		}

		_this.put = function(url , callback, data , header){
			return call(url , "put" , callback, data , header)
		}

		_this.delete = function(url , callback, data , header){
			return call(url , "delete" , callback, data , header)
		}

		return _this;

	})
	////////// FACTORY PARA LAS PETICIONES HTTP