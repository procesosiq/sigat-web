$.fn.sortElements = (function(){
    
    var sort = [].sort;
    
    return function(comparator, getSortable) {
        
        getSortable = getSortable || function(){return this;};
        
        var placements = this.map(function(){
            
            var sortElement = getSortable.call(this),
                parentNode = sortElement.parentNode,
                
                // Since the element itself will change position, we have
                // to have some way of storing it's original position in
                // the DOM. The easiest way is to have a 'flag' node:
                nextSibling = parentNode.insertBefore(
                    document.createTextNode(''),
                    sortElement.nextSibling
                );
            
            return function() {
                
                if (parentNode === this) {
                    throw new Error(
                        "You can't sort elements if any one is a descendant of another."
                    );
                }
                
                // Insert before flag:
                parentNode.insertBefore(this, nextSibling);
                // Remove flag:
                parentNode.removeChild(nextSibling);
                
            };
            
        });
       
        return sort.call(this, comparator).each(function(i){
            placements[i].call(getSortable.call(this));
        });
        
    };
    
})();

$(function(){

    $("#btnaddciclo").click(function(){
		//alert('lala');
        if(!$.isNumeric($("#txtcosto").val()) && $("#txtcosto").val() != ''){
            alert("Favor de ingresar un dato numerico en Costo de Aplicación por Ha");
            return false;
        }
		else if(!$.isNumeric($("#txtotal").val()) && $("#txtotal").val() != ''){
            alert("Favor de ingresar un dato numerico en Costo total de Aplicación");
            return false;
        }
        else{ 
            var data = {
                id : "<?= $_GET['id']?>",
                url : "controllers/index.php",
                option : "accion=Ciclos2.create",
                params : "&txtfecha="+$("#txtfecha").val()+"&txtciclo="+$("#txtciclo").val()+"&txtcompañia="+$("#txtcompañia").val()+"&txtotal="+$("#txtotal").val()+"&txtcosto="+$("#txtcosto").val()+"&txtarea="+$("#txtarea").val()+"&equipo_aplicacion="+$("#equipo_aplicacion :selected").val()+"&id=<? echo $_GET['id']?>",
            }

            if(parseInt(data.id) > 0){
                data.option = "accion=Ciclos2.update";
            }
			
            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: data.option + data.params,
				success: function(msg){
					alert("Registro "+((parseInt(data.id)>0)?"modificado":"añadido")+" con el ID "+ msg , "Ciclos" , "success" , function(){
                        if(!parseInt(data.id) > 0){
                            $('input[type=text]').each(function() {
                                $(this).val('');
                            });
                            document.location.href = "/listCiclos";
                        }
                    });
				}
			});
        }
        return false;
	});

    $(".cancel").on("click" , function(){
        document.location.href = "/listCiclos";
    });

});

$(document).ready(function(){
    var productos = []
    var producto_1 = {}, producto_2 = {}, aceite = {}, emulsificante = {}
    var id = "<?= $_GET['id'] ?>"
    var filtrado1 = true, filtrado2 = true

    $("#tab_2 .form-actions").delegate("div div", "click", change)
    $("#tab_2 .form-actions div .bootstrap-switch-handle-on").click(change)
    $("#tab_2 .form-actions div .bootstrap-switch-handle-off").click(change)

    $("#tab_3 .form-actions").delegate("div div", "click", change2)
    $("#tab_3 .form-actions div .bootstrap-switch-handle-on").click(change2)
    $("#tab_3 .form-actions div .bootstrap-switch-handle-off").click(change2)

    if(parseInt(id)>0){
        $("#btnaddciclo").html("Guardar")
        $("#btnproduct1").html("Guardar")
        $("#btnproduct2").html("Guardar")
        $("#btnaddgen").html("Guardar")
        $("#caption_nuevo").html("Editar Ciclo")
    }

    $.ajax({
        type: "POST",
        url: "controllers/index.php?accion=Ciclos2.getProductos",
        success: function(msg){
            productos = JSON.parse(msg)
            for(var i = 0; i < productos.length; i++){
                if(productos[i].id == $("#id_producto_1").val()){
                    producto_1 = productos[i]
                    $("#molProd1").val(producto_1.ingrediente_activo)
                    $("#provProd1").val(producto_1.casa_comercial)
                    //$("#dosProd1").val(producto_1.dosis)

                    var dosis = $("#dosis_num_1").val()
                    if(producto_1.dosis != "")
                        $("#dosProd1").append('<option value="1" '+(dosis==1?"selected":"")+'>'+producto_1.dosis+'</option>')
                    if(producto_1.dosis2 != "" && producto_1.dosis2 != null)
                        $("#dosProd1").append('<option value="2" '+(dosis==2?"selected":"")+'>'+producto_1.dosis2+'</option>')
                    if(producto_1.dosis3 != "" && producto_1.dosis3 != null)
                        $("#dosProd1").append('<option value="3" '+(dosis==3?"selected":"")+'>'+producto_1.dosis3+'</option>')

                    var total = ($("#txtarea").val() * parseFloat($("#dosProd1 :selected").text())) || 0
                    $("#totalL1").val(total.toFixed(2));
                }
                if(productos[i].id == $("#id_producto_2").val()){
                    producto_2 = productos[i]
                    $("#molProd2").val(producto_2.ingrediente_activo)
                    $("#provProd2").val(producto_2.casa_comercial)
                    //$("#dosProd2").val(producto_2.dosis)

                    var dosis = $("#dosis_num_2").val()
                    if(producto_2.dosis != "")
                        $("#dosProd2").append('<option value="1" '+(dosis==1?"selected":"")+'>'+producto_2.dosis+'</option>')
                    if(producto_2.dosis2 != "" && producto_2.dosis2 != null)
                        $("#dosProd2").append('<option value="2" '+(dosis==2?"selected":"")+'>'+producto_2.dosis2+'</option>')
                    if(producto_2.dosis3 != "" && producto_2.dosis3 != null)
                        $("#dosProd2").append('<option value="3" '+(dosis==3?"selected":"")+'>'+producto_2.dosis3+'</option>')

                    var total  = ($("#txtarea").val() * parseFloat($("#dosProd2 :selected").text())) || 0
                    $("#totalL2").val(total.toFixed(2));
                }
                if(productos[i].id == $("#id_emulsificante").val()){
                    emulsificante = productos[i]
                    $("#provEmul").val(emulsificante.casa_comercial)

                    var dosis = $("#dosis_emulsificante").val()
                    if(emulsificante.dosis != "")
                        $("#select_dosis_emulsificante").append('<option value="1" '+(dosis==1?"selected":"")+'>'+emulsificante.dosis+'</option>')
                    if(emulsificante.dosis2 != "" && emulsificante.dosis2 != null)
                        $("#select_dosis_emulsificante").append('<option value="2" '+(dosis==2?"selected":"")+'>'+emulsificante.dosis2+'</option>')
                    if(emulsificante.dosis3 != "" && emulsificante.dosis3 != null)
                        $("#select_dosis_emulsificante").append('<option value="3" '+(dosis==3?"selected":"")+'>'+emulsificante.dosis3+'</option>')

                    var total = ($("#txtarea").val() * parseFloat($("#select_dosis_emulsificante :selected").text())) || 0
                    $("#totalLEmul").val(total.toFixed(2))
                }
                if(productos[i].id == $("#id_aceite").val()){
                    aceite = productos[i]
                    $("#provAceite").val(aceite.casa_comercial)

                    var dosis = $("#dosis_aceite").val()
                    if(aceite.dosis != "")
                        $("#select_dosis_aceite").append('<option value="1" '+(dosis==1?"selected":"")+'>'+aceite.dosis+'</option>')
                    if(aceite.dosis2 != "" && aceite.dosis2 != null)
                        $("#select_dosis_aceite").append('<option value="2" '+(dosis==2?"selected":"")+'>'+aceite.dosis2+'</option>')
                    if(aceite.dosis3 != "" && aceite.dosis3 != null)
                        $("#select_dosis_aceite").append('<option value="3" '+(dosis==3?"selected":"")+'>'+aceite.dosis3+'</option>')

                    var total = ($("#txtarea").val() * parseFloat($("#select_dosis_aceite :selected").text())) || 0
                    $("#totalLAceite").val(total.toFixed(2));
                }
            }

            costoFungicida1Change()
            costoFungicida2Change()
        }
    });

    $("#btnproduct1").click(function(){
        //alert('lala');
        if(!$.isNumeric($("#costoProd1").val()) && $("#costoProd1").val() != ''){
            alert("Favor de ingresar un dato numerico en Costo (L o Kg)");
            return false;
        }
        else if(!producto_1.hasOwnProperty("id")){
            alert("Favor de elegir un producto");
            return false;
        }
        else{ 
            var data = {
                id : "<?= $_GET['id']?>",
                url : "controllers/index.php",
                option : "accion=Ciclos2.updateProducto1",
                params : "&nomProd1="+$("#nomProd1").val()+"&molProd1="+$("#molProd1").val()+"&provProd1="+$("#provProd1").val()+"&dosProd1="+$("#dosProd1").val()+"&costoProd1="+$("#costoProd1").val()+"&totalHa1="+$("#totalHa1").val()+"&id=<? echo $_GET['id']?>",
            }
            
            $.ajax({
                type: "POST",
                url: "controllers/index.php",
                data: data.option + data.params,
                success: function(msg){
                    alert("Registro registrado/modificado con el ID "+ msg , "Ciclos" , "success" , function(){
                        if(!parseInt(data.id) > 0){
                            $('input[type=text]').each(function() {
                                $(this).val('');
                            });
                            document.location.href = "/listCiclos";
                        }
                    });
                }
            });
        }
        return false;
    });
    
    $("#btnproduct2").click(function(){
        //alert('lala');
        if(!$.isNumeric($("#costoProd2").val())&& $("#costoProd2").val() != ''){
            alert("Favor de ingresar un dato numerico en Costo (L o Kg)");
            return false;
        }
        else if(!producto_2.hasOwnProperty("id")){
            alert("Favor de elegir un producto");
            return false;
        }
        else{ 
            var data = {
                id : "<?= $_GET['id']?>",
                url : "controllers/index.php",
                option : "accion=Ciclos2.updateProducto2",
                params : "&nomProd2="+$("#nomProd2").val()+"&molProd2="+$("#molProd2").val()+"&provProd2="+$("#provProd2").val()+"&dosProd2="+$("#dosProd2").val()+"&costoProd2="+$("#costoProd2").val()+"&totalHa2="+$("#totalHa2").val()+"&id=<? echo $_GET['id']?>",
            }
            
            $.ajax({
                type: "POST",
                url: "controllers/index.php",
                data: data.option + data.params,
                success: function(msg){
                    alert("Registro registrado/modificado con el ID "+ msg , "Ciclos" , "success" , function(){
                        if(!parseInt(data.id) > 0){
                            $('input[type=text]').each(function() {
                                $(this).val('');
                            });
                            document.location.href = "/listCiclos";
                        }
                    });
                }
            });
        }
        return false;
    });

    $("#nomProd1").change(function(){
        if($("#nomProd1 :selected").val() != ""){
            for(var i = 0; i < productos.length; i++){
                if(productos[i].id == $("#nomProd1 :selected").val()){
                    producto_1 = productos[i]
                    $("#molProd1").val(producto_1.ingrediente_activo)
                    $("#provProd1").val(producto_1.casa_comercial)
                    $("#dosProd1").html("")
                    if(producto_1.dosis != "")
                        $("#dosProd1").append('<option value="1">'+producto_1.dosis+'</option>')
                    if(producto_1.dosis2 != "" && producto_1.dosis2 != null)
                        $("#dosProd1").append('<option value="2">'+producto_1.dosis2+'</option>')
                    if(producto_1.dosis3 != "" && producto_1.dosis3 != null)
                        $("#dosProd1").append('<option value="3">'+producto_1.dosis3+'</option>')

                    costoFungicida1Change()
                }
            }
        }else{
            producto_1 = {}
        }
    })

    $("#nomProd2").change(function(){
        if($("#nomProd2 :selected").val() != ""){
            for(var i = 0; i < productos.length; i++){
                if(productos[i].id == $("#nomProd2 :selected").val()){
                    producto_2 = productos[i]
                    $("#molProd2").val(producto_2.ingrediente_activo)
                    $("#provProd2").val(producto_2.casa_comercial)
                    $("#dosProd2").html("")
                    if(producto_2.dosis != "")
                        $("#dosProd2").append('<option value="1">'+producto_2.dosis+'</option>')
                    if(producto_2.dosis2 != "" && producto_2.dosis2 != null)
                        $("#dosProd2").append('<option value="2">'+producto_2.dosis2+'</option>')
                    if(producto_2.dosis3 != "" && producto_2.dosis3 != null)
                        $("#dosProd2").append('<option value="3">'+producto_2.dosis3+'</option>')

                    costoFungicida2Change()
                }
            }
        }else{
            producto_2 = {}
        }
    })

    $("#nomEmul").change(function(){
        if($("#nomEmul :selected").val() != ""){
            for(var i = 0; i < productos.length; i++){
                if(productos[i].id == $("#nomEmul :selected").val()){
                    emulsificante = productos[i]
                    $("#id_emulsificante").val(emulsificante.id)
                    $("#provEmul").val(emulsificante.casa_comercial)
                }
            }
        }else{
            emulsificante = {}
            $("#id_emulsificante").val(0)
            $("#provEmul").val("")
        }
    })

    $("#nomAceite").change(function(){
        if($("#nomAceite :selected").val() != ""){
            for(var i = 0; i < productos.length; i++){
                if(productos[i].id == $("#nomAceite :selected").val()){
                    aceite = productos[i]
                    $("#id_aceite").val(aceite.id)
                    $("#provAceite").val(aceite.casa_comercial)
                }
            }
        }else{
            aceite = {}
            $("#id_aceite").val(0)
            $("#provAceite").val("")
        }
    })

    $("#btnaddgen").click(function(){
        //alert('lala');
        if(!$.isNumeric($("#litroEmul").val())&& $("#litroEmul").val() != ''){
            alert("Favor de ingresar un dato numerico en Costo litro de emulsificante");
            return false;
        }
        else if(!$.isNumeric($("#totalEmul").val())&& $("#totalEmul").val() != ''){
            alert("Favor de ingresar un dato numerico en Costo total emulsificante");
            return false;
        }
        else if(!$.isNumeric($("#litroAceite").val())&& $("#litroAceite").val() != ''){
            alert("Favor de ingresar un dato numerico en Costo litro de aceite");
            return false;
        }
        else if(!$.isNumeric($("#totalAceite").val())&& $("#totalAceite").val() != ''){
            alert("Favor de ingresar un dato numerico en Costo total aceite");
            return false;
        }
        else if(!$.isNumeric($("#totalCiclo").val())&& $("#totalCiclo").val() != ''){
            alert("Favor de ingresar un dato numerico en Total costo ciclo");
            return false;
        }
        else if(!$.isNumeric($("#totalHa").val())&& $("#totalHa").val() != ''){
            alert("Favor de ingresar un dato numerico en Total costo ciclo por Ha");
            return false;
        }
        else{ 
            var data = {
                id : "<?= $_GET['id']?>",
                url : "controllers/index.php",
                option : "accion=Ciclos2.updateDatos",
                params : "&nomEmul="+$("#nomEmul").val()+"&litroEmul="+$("#litroEmul").val()+"&totalEmul="+$("#totalEmul").val()+"&provEmul="+$("#provEmul").val()+"&nomAceite="+$("#nomAceite").val()+"&litroAceite="+$("#litroAceite").val()+"&totalAceite="+$("#totalAceite").val()+"&provAceite="+$("#provAceite").val()+"&totalCiclo="+$("#totalCiclo").val()+"&totalHa="+$("#totalHa").val()+"&id=<? echo $_GET['id']?>",
            }
            
            $.ajax({
                type: "POST",
                url: "controllers/index.php",
                data: data.option + data.params,
                success: function(msg){
                    alert("Registro registrado/modificado con el ID "+ msg , "Ciclos" , "success" , function(){
                        if(!parseInt(data.id) > 0){
                            $('input[type=text]').each(function() {
                                $(this).val('');
                            });
                            document.location.href = "/listCiclos";
                        }
                    });
                }
            });
        }
        return false;
    });

    $("#dosProd1").change(function(){
        costoFungicida1Change()
    })

    $("#costoProd1").change(function(){
        costoFungicida1Change()
    })

    $("#dosProd2").change(function(){
        costoFungicida2Change()
    })

    $("#costoProd2").change(function(){
        costoFungicida2Change()
    })

    $("#txtarea").change(function(){
        $("#totalL1").val(($("#txtarea").val() * parseFloat($("#dosProd1 :selected").text())).toFixed(2));
        $("#totalL2").val(($("#txtarea").val() * parseFloat($("#dosProd2 :selected").text())).toFixed(2));
        $("#totalLEmul").val(($("#txtarea").val() * parseFloat($("#select_dosis_emulsificante :selected").text())).toFixed(2));
        $("#totalLAceite").val(($("#txtarea").val() * parseFloat($("#select_dosis_aceite :selected").text())).toFixed(2));

        costoFungicida1Change()
        costoFungicida2Change()
    })

    function costoFungicida1Change(){
        var dosis = $("#dosProd1 :selected").text()
        var costo = $("#costoProd1").val() || 0
        var totalL = $("#totalL1").val() || 0

        if($.isNumeric(dosis) && $.isNumeric(costo)){
            var total = (dosis * costo) || 0
            $("#totalHa1").val(total.toFixed(2))
            $("#totalProd1").val((costo * totalL).toFixed(2))
        }else{
            $("#totalHa1").val("")
            $("#totalProd1").val("")
        }
    }

    function costoFungicida2Change(){
        var dosis = $("#dosProd2 :selected").text()
        var costo = $("#costoProd2").val() || 0
        var totalL = $("#totalL2").val() || 0

        if($.isNumeric(dosis) && $.isNumeric(costo)){
            var total = (dosis * costo) || 0
            $("#totalHa2").val(total.toFixed(2))
            $("#totalProd2").val((costo * totalL).toFixed(2))
        }else{
            $("#totalHa2").val("")
            $("#totalProd2").val("")
        }
    }

    function change(){
        filtrado1 = $("#filterFungicida1").is(":checked")
        $("#nomProd1").html("")

        if(filtrado1){
            for(var i = 0; i < productos.length; i++){
                var pro = productos[i]
                if(pro.nombre_comercial != null || pro.ingrediente_activo != null)
                    $("#nomProd1").append('<option value="'+pro.id+'" '+(pro.id == producto_1.id ? 'selected' : '')+'>'+((pro.nombre_comercial != null) ? pro.nombre_comercial : pro.ingrediente_activo )+'</option>')
            }
        }else{
            for(var i = 0; i < productos.length; i++){
                var pro = productos[i]
                if(pro.ingrediente_activo == $("#molProd1").val() && (pro.nombre_comercial != null || pro.ingrediente_activo != null)){
                    $("#nomProd1").append('<option value="'+pro.id+'" '+(pro.id == producto_1.id ? 'selected' : '')+'>'+((pro.nombre_comercial != null) ? pro.nombre_comercial : pro.ingrediente_activo )+'</option>')
                }
            }
        }
    }

    function change2(){
        filtrado2 = $("#filterFungicida2").is(":checked")
        $("#nomProd2").html("")

        if(filtrado2){
            for(var i = 0; i < productos.length; i++){
                var pro = productos[i]
                if(pro.nombre_comercial != null || pro.ingrediente_activo != null)
                    $("#nomProd2").append('<option value="'+pro.id+'" '+(pro.id == producto_2.id ? 'selected' : '')+'>'+((pro.nombre_comercial != null) ? pro.nombre_comercial : pro.ingrediente_activo )+'</option>')
            }
        }else{
            for(var i = 0; i < productos.length; i++){
                var pro = productos[i]
                if(pro.ingrediente_activo == $("#molProd2").val() && (pro.nombre_comercial != null || pro.ingrediente_activo != null)){
                    $("#nomProd2").append('<option value="'+pro.id+'" '+(pro.id == producto_2.id ? 'selected' : '')+'>'+((pro.nombre_comercial != null) ? pro.nombre_comercial : pro.ingrediente_activo )+'</option>')
                }
            }
        }
    }
})
