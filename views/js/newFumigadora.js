$(function(){
    $(".btnadd").click(function(){
        
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre de la Fumigadora");
            return false;
        }
        else{ 
            var data = {
                id : "<? echo $_GET['id']?>",
                url : "controllers/index.php",
                option : "accion=Fumigadoras.create",
                params :"&txtnom="+$("#txtnom").val()+"&id=<? echo $_GET['id']?>",
            }

            if(parseInt(data.id) > 0){
                data.option = "accion=Fumigadoras.update";
            }

            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: data.option + data.params,
				success: function(msg){
					alert("Registro registrado/modificado con el ID "+ msg , "Fumigadoras" , "success" , function(){
                        if(!parseInt(data.id) > 0){
                            $('input[type=text]').each(function() {
                                $(this).val('');
                            });
                            document.location.href = "fumigadorasList";
                        }
                    });
				}
			});
        }
        return false;
	});


    $(".addPrice").on("click" , function(){
        var precio = parseFloat($("#precio").val());
        var id_finca = parseInt($("#finca").val()) || 0;
        var galones = parseInt($("#galones").val()) || 0;
        var fecha = $("#fecha").val()

        if (isNaN(precio)) {
            alert("Favor de ingresar un precio valido");
        } else if (precio <= 0) {
            alert("El precio debe ser mayor a 0");
        } else if (("<?= $_GET['id'] ?>" == 1 && !id_finca > 0) || ("<?= $_GET['id'] ?>" != 1 && !galones > 0)) {
            if("<?= $_GET['id'] ?>" == 1){
                alert("Debe seleccionar la finca");
            }else{
                alert("Debe seleccionar los galones");
            }
        }else{
            var data = {
                id : "<?= $_GET['id'] ?>",
                option : "accion=Fumigadoras.savePrecios",
                params : `&galones=${galones}&id_finca=${id_finca}&fecha=${fecha}&precio=${precio}&id=<?= $_GET['id']?>`,
            }
             $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: data.option + data.params,
				success: function(data){
                    $("#precio").val('')
                    alert("Se guardo el precio correctamente", "", "success")
					addPrice(data)
				}
			});
        }
    });


    if ($('.date-picker').length > 0) {
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }

    $("#container-html").on("click" , '.dropDetalle' , function(){
        var data = $(this).data("fumigadora");
        console.log(data)
    });

    var addPrice = function(data){
        var res = data //JSON.parse(data)
        var contenedor = $("#container-html");

        contenedor.empty();
        var rows = res.data.map(function(elemt){
            var info = JSON.stringify(elemt)
            var group = 'galones'
            if("<?= $_GET['id'] ?>" == 1) group = 'finca'

            return `<tr><td>` + elemt[group] + `</td><td>` + elemt.precio + `</td><td><button type="button" data-fumigadora='${info}' class="btn red dropDetalle">Eliminar</button></td></tr>`;
        })

        contenedor.append(rows);
    }

    var getPrecios = function(){
        var data = {
            id : "<?= $_GET['id']?>",
            option : "accion=Fumigadoras.getPrecios",
            params :"&id=<?= $_GET['id']?>&finca="+$("#finca").val(),
        }
        $.ajax({
            type: "POST",
            url: "controllers/index.php",
            data: data.option + data.params,
            success: function(data){
                addPrice(data)
            }
        });
    }

    getPrecios();

    $("#finca").on('change', function(){
        let id_finca = $("#finca").val()
        let id = "<?= $_GET['id'] ?>"

        if(id > 0){
            var data = {
                option : "accion=Fumigadoras.getPrecios",
                params :"&id="+id+"&finca="+id_finca,
            }
            $.ajax({
                type: "POST",
                url: "controllers/index.php",
                data: data.option + data.params,
                success: function(data){
                    addPrice(data)
                }
            });
        }
    })

    $(".cancel").on("click" , function(){
        document.location.href = "fumigadorasList";
    });

    $("#container-html").on('click', 'button.dropDetalle', function(){
        var json = JSON.parse($(this).attr('data-fumigadora'))
        let id = json.id || 0
        let id_finca = json.id_finca || 0
        let galones = json.galones || 0
        let precio = json.precio || 0

        if(id > 0){
            var data = {
                option : "accion=Fumigadoras.dropPrecios",
                params :"&id=<?= $_GET['id'] ?>&id_precio="+id+"&finca="+id_finca+"&precio="+precio+"&galones="+galones,
            }
            $.ajax({
                type: "POST",
                url: "controllers/index.php",
                data: data.option + data.params,
                success: function(data){
                    addPrice(data)
                }
            });
        }
    })
});