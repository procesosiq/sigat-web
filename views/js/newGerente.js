
var services = function(){
	this.url = "controllers/index.php";
	services.prototype.save = function(callback , options) {
		var data = {
			accion : "Gerentes.create",
			params : options
		}

		var path = this.url;

		ahttp.post(path, callback , data);
	}

	services.prototype.update = function(callback , options) {
		var data = {
			accion : "Gerentes.update",
			params : options
		}

		var path = this.url;
		
		ahttp.post(path, callback , data);
	}

	services.prototype.fincas = function() {
		var data = {
			accion : "indexMain.getFincasPOST",
			params : options
		}

		var path = this.url;

		ahttp.post(path,callback , data);
	};
}

var validate = function(){

	validate.editing = false;

	validate.prototype.isValid = function() {
		var data = "";
		if($("#nombre").val()==''){
            alert("Favor de ingresar un nombre del Gerente");
            return false;
        }
        return true;
	}

	validate.prototype.params = function() {
		var data = {
			nombre : $("#nombre").val(),
			user : $("#user").val(),
			pass : $("#pass").val(),
			fincas : $("#fincas").val(),
			id : "<? echo $_GET['id']?>",
		}

        return JSON.stringify(data);
	}

	validate.prototype.reset = function(){
		$("#nombre").val("");
		$("#user").val("");
		$("#pass").val("");
		$("#fincas").val("");
	}

	validate.prototype.watch_changes = function(){
		$("#nombre").change(form_changed)
		$("#user").change(form_changed)
		$("#pass").change(form_changed)
		$("#fincas").change(form_changed)
	}

	validate.prototype.fincas = function() {

	}
}

function success(r , b, success){
	b();
	if(r){
		alert("Registrado Satisfactoriamente", "Gerentes", "success" , function(){
            if(!isNaN(parseInt(r)) && parseInt(r) > 0){
                $('input[type=text]').each(function() {
                    $(this).val('');
                });
                document.location.href = "newGerente?id=<? echo $_GET['id']?>";
            }
        });
	}
}

function form_changed(){
	$(".lbladd").text("Guardar")
}

jQuery(document).ready(function() {    
	$('#fincas').multiSelect({
		selectableHeader: "<div class='custom-header'> Fincas sin asignar </div>",
  		selectionHeader: "<div class='custom-header'> Fincas seleccionadas </div>",
  		afterSelect: function(values){
  			console.log(values)	
		}
	});
   	var service = new services();
   	var form = new validate();
	var cambios = 0;

	if("<? echo $_GET['id']?>" > 0){
		form.watch_changes();
		form.editing = true;
	}

	$(".btnadd").on('click', function(event) {
	   	event.preventDefault();
	   	/* Act on the event */
	   	if(form.isValid()){
			if(form.editing){
				service.update(success, form.params());
			}else{
	   			service.save(success, form.params());
				form.reset();
			}
	   	}
   });

    $(".cancel").on("click" , function(){
        document.location.href = "gerentesList";
    });

});


