$(function(){
    $(".btnadd").click(function(){
        
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre del Sector");
            return false;
        }
        else if($("#hectarea").val()==''){
            alert("Favor de ingresar hectareas");
            return false;
        }
        else if($("#s_fumigadora").val()==''){
            alert("Favor de ingresar Fumigadora");
            return false;
        }
        else if($("#placas").val()==''){
            alert("Favor de ingresar Placa del Avion");
            return false;
        }

        else{ 
            var data = {
                id : "<? echo $_GET['id']?>",
                url : "controllers/index.php",
                option : "accion=Pilotos.create",
                params : "&txtnom="+$("#txtnom").val()+"&id_fumigadora="+$("#s_fumigadora").val()+"&id_placa="+$("#placas").val()+"&id=<? echo $_GET['id']?>",
            }

            if(parseInt(data.id) > 0){
                data.option = "accion=Pilotos.update";
            }

            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: data.option + data.params,
				success: function(msg){
					alert("Registro registrado/modificado con el ID "+ msg , "Pilotos" , "success" , function(){
                        if(!parseInt(data.id) > 0){
                            $('input[type=text]').each(function() {
                                $(this).val('');
                            });
                            document.location.href = "pilotosList";
                        }
                    });
				}
			});
        }
        return false;
	});

    $(".cancel").on("click" , function(){
        document.location.href = "pilotosList";
    });

    $("#s_fumigadora").on("change" , function(e){
        var data = {
            id_fumigadora : $(this).val(),
            accion : "Pilotos.getPlacas"
        }
        // console.log("Entro")
        ahttp.post("./controllers/index.php?accion=Pilotos.getPlacas",getData , data);
    });

    function getData(r, b){
        b();
        if(r){
            $("#placas").html("");
            var inHTML = "";

            $.each(r, function(index, value){
                var newItem = '<option value="'+value.id+'">'+value.nombre+'</option>';
                inHTML += newItem;  
            });

            $("#placas").append(inHTML);
        }
    }
});