$(function(){
    $(".btnadd").click(function(){
        
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre del la Placa");
            return false;
        }
        if($("#s_fumigadora").val()==''){
            alert("Favor de ingresar una Fumigadora");
            return false;
        }
        else{ 
            var data = {
                id : "<? echo $_GET['id']?>",
                url : "controllers/index.php",
                option : "accion=Placas.create",
                params :"&id_fumigadora="+$("#s_fumigadora").val()+"&txtnom="+$("#txtnom").val()+"&id=<? echo $_GET['id']?>",
            }

            if(parseInt(data.id) > 0){
                data.option = "accion=Placas.update";
            }

            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: data.option + data.params,
				success: function(msg){
					alert("Registro registrado/modificado con el ID "+ msg , "Ciclos" , "success" , function(){
                        if(!parseInt(data.id) > 0){
                            $('input[type=text]').each(function() {
                                $(this).val('');
                            });
                            document.location.href = "placasList";
                        }
                    });
				}
			});
        }
        return false;
	});

    $(".cancel").on("click" , function(){
        document.location.href = "placasList";
    });
});