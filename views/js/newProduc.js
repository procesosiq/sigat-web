$(function(){
    $(".btnadd").click(function(){
        
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre de Productor");
            return false;
        }
        else if($("#txtemail").val()==''){
            alert("Favor de ingresar un email del Productor");
            return false;
        }
        else{ 
            var data = {
                id : "<? echo $_GET['id']?>",
                url : "controllers/index.php",
                option : "accion=Productores.create",
                params : "&txtnom="+$("#txtnom").val()+"&txtemail="+$("#txtemail").val()+"&id=<? echo $_GET['id']?>",
            }

            if(parseInt(data.id) > 0){
                data.option = "accion=Productores.update";
            }

            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: data.option + data.params,
				success: function(msg){
					alert("Productor registrado/modificado con el ID "+ msg , "Productores" , "success" , function(){
                        if(!parseInt(data.id) > 0){
                            $('input[type=text]').each(function() {
                                $(this).val('');
                            });
                            document.location.href = "listProduc";
                        }
                    });
				}
			});
        }
        return false;
	});

    $(".createUser").on('click', function(event) {
        event.preventDefault();
        var data = {
                id : "<? echo $_GET['id']?>",
                option : "accion=Productores.createUser&id=<? echo $_GET['id']?>",
            }

        $.ajax({
            type: "POST",
            url: "controllers/index.php",
            data: data.option,
            dataType: "json",
            success: function(msg){
                msg = JSON.stringify(msg);
                console.log(msg)
                alert("Usuario productor registrado/modificado " , "Productores" , "success");
                // if(msg.success == 200){
                // }else{
                //     alert("Error en Usuario productor " , "Productores" , "error");
                // }
            }
        });
    });

    $(".cancel").on("click" , function(){
        document.location.href = "listProduc";
    })
    
});