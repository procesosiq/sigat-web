$.fn.sortElements = (function(){
    
    var sort = [].sort;
    
    return function(comparator, getSortable) {
        
        getSortable = getSortable || function(){return this;};
        
        var placements = this.map(function(){
            
            var sortElement = getSortable.call(this),
                parentNode = sortElement.parentNode,
                
                // Since the element itself will change position, we have
                // to have some way of storing it's original position in
                // the DOM. The easiest way is to have a 'flag' node:
                nextSibling = parentNode.insertBefore(
                    document.createTextNode(''),
                    sortElement.nextSibling
                );
            
            return function() {
                
                if (parentNode === this) {
                    throw new Error(
                        "You can't sort elements if any one is a descendant of another."
                    );
                }
                
                // Insert before flag:
                parentNode.insertBefore(this, nextSibling);
                // Remove flag:
                parentNode.removeChild(nextSibling);
                
            };
            
        });
       
        return sort.call(this, comparator).each(function(i){
            placements[i].call(getSortable.call(this));
        });
        
    };
    
})();

$(function(){

    $("#btnaddciclo").click(function(){
		//alert('lala');
        if($("#agroquimico").val() == ''){
            alert("Favor de ingresar Agroquimico");
            return false;
        }
		else if($("#nombre_comercial").val() == ''){
            alert("Favor de ingresar Nombre Comercial");
            return false;
        }
        else if($("#ingrediente_activo").val() == ''){
            alert("Favor de ingresar Ingrediente Activo");
            return false;
        }
        else if($("#casa_comercial").val() == ''){
            alert("Favor de ingresar Casa Comercial");
            return false;
        }
        else if($("#dosis").val() == ''){
            alert("Favor de ingresar Dosis");
            return false;
        }
        else if($("#precio").val() == ''){
            alert("Favor de ingresar Precio");
            return false;
        }else if(!$.isNumeric($("#precio").val())){
            alert("Precio debe ser tipo numerico");
            return false;
        }
        else{ 
            var data = {
                id : "<?= $_GET['id']?>",
                url : "controllers/index.php",
                option : "accion=Ciclos2.createProducto",
                params : "&agroquimico="+$("#agroquimico").val()+"&nombre_comercial="+$("#nombre_comercial").val()+"&ingrediente_activo="+$("#ingrediente_activo").val()+"&casa_comercial="+$("#casa_comercial").val()+"&dosis="+$("#dosis").val()+"&dosis_2="+$("#dosis_2").val()+"&dosis_3="+$("#dosis_3").val()+"&precio="+$("#precio").val()+"&id=<? echo $_GET['id']?>",
            }
			
            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: data.option + data.params,
				success: function(msg){
					alert("Registro "+((parseInt(data.id)>0)?"modificado":"añadido")+" con el ID "+ msg , "Productos" , "success" , function(){
                        document.location.href = "/listProductos";
                    });
				}
			});
        }
        return false;
	});

    $(".cancel").on("click" , function(){
        document.location.href = "/listProductos";
    });



    /* --- */
    var id = "<?= $_GET['id'] ?>"
    if(parseInt(id)>0){
        $("#btnaddciclo").html("Guardar")
        $("#caption_nuevo").html("Editar Producto")
    }
});