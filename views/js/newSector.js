$(function(){
    $(".btnadd").click(function(){
        
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre del Sector");
            return false;
        }
        else if($("#hectarea").val()==''){
            alert("Favor de ingresar hectareas");
            return false;
        }
        else if($("#s_sucursales").val()==''){
            alert("Favor de ingresar Productor");
            return false;
        }
        else if($("#s_sucursales").val()==''){
            alert("Favor de ingresar Finca");
            return false;
        }
        else{ 
            var data = {
                id : "<? echo $_GET['id']?>",
                url : "controllers/index.php",
                option : "accion=Sectores.create",
                params : "&hectarea="+$("#hectarea").val()+"&txtnom="+$("#txtnom").val()+"&id_cliente="+$("#s_sucursales").val()+"&id_hacienda="+$("#s_sucursales_finca").val()+"&id=<? echo $_GET['id']?>",
            }

            if(parseInt(data.id) > 0){
                data.option = "accion=Sectores.update";
            }

            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: data.option + data.params,
				success: function(msg){
					alert("Registro registrado/modificado con el ID "+ msg , "Sectores" , "success" , function(){
                        if(!parseInt(data.id) > 0){
                            $('input[type=text]').each(function() {
                                $(this).val('');
                            });
                            document.location.href = "sectoresList";
                        }
                    });
				}
			});
        }
        return false;
	});

    $(".cancel").on("click" , function(){
        document.location.href = "sectoresList";
    });

    $("#s_sucursales").on("change" , function(e){
        var data = {
            id_client : $(this).val(),
            accion : "indexMain.getFincasPOST"
        }
        // console.log("Entro")
        ahttp.post("./controllers/index.php?accion=indexMain.getFincasPOST",getData , data);
    });

    function getData(r, b){
        b();
        if(r){
            $("#s_sucursales_finca").html("");
            var inHTML = "";

            $.each(r, function(index, value){
                var newItem = '<option value="'+value.id+'">'+value.nombre+'</option>';
                inHTML += newItem;  
            });

            $("#s_sucursales_finca").append(inHTML);
        }
    }

    
});