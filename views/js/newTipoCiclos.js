$(function(){
    $(".btnadd").click(function(){
        
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre del Ciclo");
            return false;
        }
        else{ 
            var data = {
                id : "<? echo $_GET['id']?>",
                url : "controllers/index.php",
                option : "accion=TiposCiclos.create",
                params :"&txtnom="+$("#txtnom").val()+"&id=<? echo $_GET['id']?>",
            }

            if(parseInt(data.id) > 0){
                data.option = "accion=TiposCiclos.update";
            }

            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: data.option + data.params,
				success: function(msg){
					alert("Registro registrado/modificado con el ID "+ msg , "Ciclos" , "success" , function(){
                        if(!parseInt(data.id) > 0){
                            $('input[type=text]').each(function() {
                                $(this).val('');
                            });
                            document.location.href = "tiposCiclosList";
                        }
                    });
				}
			});
        }
        return false;
	});

    $(".cancel").on("click" , function(){
        document.location.href = "tiposCiclosList";
    });
});