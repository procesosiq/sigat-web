    /*==================================
    =            CALENDARIO            =
    ==================================*/
    var calendar = {
        fullcalendarOrder : $('#calendar_object'),
        config : function(Loadevents){

            if (!jQuery().fullCalendar) {
                throw new Error("No existe el plugin de calendario")
                return;
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            if (App.isRTL()) {
                if (this.fullcalendarOrder.parents(".portlet").width() <= 720) {
                    this.fullcalendarOrder.addClass("mobile");
                    h = {
                        right: 'title, prev, next',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today'
                    };
                } else {
                    this.fullcalendarOrder.removeClass("mobile");
                    h = {
                        right: 'title',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today, prev,next'
                    };
                }
            } else {
                if (this.fullcalendarOrder.parents(".portlet").width() <= 720) {
                    this.fullcalendarOrder.addClass("mobile");
                    h = {
                        left: 'title, prev, next',
                        center: '',
                        right: 'today,month,agendaWeek,agendaDay'
                    };
                } else {
                    this.fullcalendarOrder.removeClass("mobile");
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                }
            }

            var events = Loadevents || [{
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    backgroundColor: App.getBrandColor('yellow'),
                    description : "Prueba de Evento",
                    // url: 'http://google.com/',
                }];

                // console.log(events)

            return { //re-initialize the calendar
                header: h,
                defaultView: 'agendaWeek', // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/ 
                slotMinutes: 15,
                lang: 'es',
                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar !!!
                events: events,
				drop : function(date, allDay) { // this function is called when something is dropped
					if($("#gruposcalendario").val()==0){
						console.log("todos");
					alert("Para poder agendar necesita seleccionar un grupo", "Ordenes", "error");}
				else{
                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');
                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.className = $(this).attr("data-class");
					var id = copiedEventObject.title.split(" ");
					var data = {
						id: id[0],
						grupo: $("#gruposcalendario").val(),
						fecha: date.format("YYYY-MM-DD HH:mm")
					}
        	ahttp.post("./controllers/index.php?accion=Ordenes.editCalendar" , function(r, b){
				b();
				//console.log(r);
				eventCalendar();
				printCal();
				
				 //alert(' El evento ' + event.title + " ha sido movido " + event.start.format("YYYY-MM-DD HH:mm") , 'Ordenes' , 'info');
            	//$scope.formData.materiales = r.materiales;
				}, data);
				
                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar_object').fullCalendar('renderEvent', copiedEventObject, true);

                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }
                }
				},
                eventClick: function(calEvent, jsEvent, view) {
                    var msj = 'Evento : ' + calEvent.title + ' <br> Fecha : ' + calEvent.start.format("YYYY-MM-DD HH:mm")
					+ ' <br> Cliente : ' + calEvent.cliente + ' <br> Tipo trabajo : ' + calEvent.tipo_trabajo 
					+ ' <br> <button class="btn btn-sm bg-red-thunderbird bg-font-red-thunderbird" onclick="removeEvent('+"'"+calEvent.title+"'"+')">Remover evento</button>';
                    alert(msj, 'Ordenes' , 'success');
                    // change the border color just for fun
                    $(this).css('border-color', 'red');

                },
                eventDrop: function(event, delta, revertFunc) {

					if (confirm("Esta seguro de cambiar el evento?")) {
                       
					var id =  event.title.split(" ");
					
					var data = {
						id: id[0],
						grupo: 0,
						fecha: event.start.format("YYYY-MM-DD HH:mm")
					}
        	ahttp.post("./controllers/index.php?accion=Ordenes.editCalendar" , function(r, b){
				b();
				//console.log(r);
				eventCalendar();
				printCal();
				
				 alert(' El evento ' + event.title + " ha sido movido " + event.start.format("YYYY-MM-DD HH:mm") , 'Ordenes' , 'info');
            	//$scope.formData.materiales = r.materiales;
				}, data);
                   

                    
					}
					else {
						revertFunc();
					}
                
				}
            }
        },
        init : function(){
            // console.log(this.config());
            var config = this.config();
            this.fullcalendarOrder.fullCalendar('destroy'); // destroy the calendar
            this.fullcalendarOrder.fullCalendar(config);
        },
        reload : function(events){
			//console.log("entre");
            this.fullcalendarOrder.fullCalendar('destroy'); // destroy the calendar
            if(events)
                this.fullcalendarOrder.fullCalendar(this.config(events));
            else
                this.fullcalendarOrder.fullCalendar(this.config());
        }
    }
    calendar.init();
    /*=====  End of CALENDARIO  ======*/
var removeEvent = function(title){
	if (confirm("Esta seguro de remover el evento "+title+"?")) {
                       
					var id =  title.split(" ");
					
					var data = {
						id: id[0],
					}
        	ahttp.post("./controllers/index.php?accion=Ordenes.removeCalendar" , function(r, b){
				b();
				//console.log(r);
				eventCalendar();
				printCal();
				
				 alert(' El evento ' + title + " ha sido removido" , 'Ordenes' , 'info');
            	//$scope.formData.materiales = r.materiales;
				}, data);
                   

                    
					}
}
	$.fn.dataTable.pipeline = function ( opts ) {
    // Configuration options
    console.log(opts);
    var conf = $.extend( {
        pages: 5,     // number of pages to cache
        url: '',      // script url
        data: null,   // function or object with parameters to send to the server
                      // matching how `ajax.data` works in DataTables
        method: 'POST' // Ajax HTTP method
    }, opts );
 
    // Private variables for storing the cache
    var cacheLower = -1;
    var cacheUpper = null;
    var cacheLastRequest = null;
    var cacheLastJson = null;
 
    return function ( request, drawCallback, settings ) {
        var ajax          = false;
        var requestStart  = request.start;
        var drawStart     = request.start;
        var requestLength = request.length;
        var requestEnd    = requestStart + requestLength;
         
        if ( settings.clearCache ) {
            // API requested that the cache be cleared
            ajax = true;
            settings.clearCache = false;
        }
        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {
            // outside cached data - need to make a request
            ajax = true;
        }
        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||
                  JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||
                  JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )
        ) {
            // properties changed (ordering, columns, searching)
            ajax = true;
        }
         
        // Store the request for checking next time around
        cacheLastRequest = $.extend( true, {}, request );
 
        if ( ajax ) {
            // Need data from the server
            if ( requestStart < cacheLower ) {
                requestStart = requestStart - (requestLength*(conf.pages-1));
 
                if ( requestStart < 0 ) {
                    requestStart = 0;
                }
            }
             
            cacheLower = requestStart;
            cacheUpper = requestStart + (requestLength * conf.pages);
 
            request.start = requestStart;
            request.length = requestLength*conf.pages;
 
            // Provide the same `data` options as DataTables.
            console.log(conf);
            if ( $.isFunction ( conf.data ) ) {
                // As a function it is executed with the data object as an arg
                // for manipulation. If an object is returned, it is used as the
                // data object to submit
                var d = conf.data( request );
                if ( d ) {
                    $.extend( request, d );
                }
            }
            else if ( $.isPlainObject( conf.data ) ) {
                // As an object, the data given extends the default
                $.extend( request, conf.data );
            }

            settings.jqXHR = $.ajax( {
                "type":     conf.method,
                "url":      conf.url,
                "data":     request,
                "dataType": "json",
                "cache":    false,
                "success":  function ( json ) {
                    cacheLastJson = $.extend(true, {}, json);
 
                    if ( cacheLower != drawStart ) {
                        json.data.splice( 0, drawStart-cacheLower );
                    }
                    if($.isArray( json.data )){
                        json.data.splice( requestLength, json.data.length );
                    }
                    $('#datatable_ajax tbody').off( 'click', 'button');
                    $('#datatable_ajax').off('click', '.filter-submit');
                    $('#datatable_ajax').off('click', '.filter-cancel');
                    drawCallback( json );
                }
            } );
        }
        else {
            json = $.extend( true, {}, cacheLastJson );
            
            json.draw = request.draw; // Update the echo for each response
            console.log($.isPlainObject( json.data ));
            console.log($.isArray( json.data ));
            if($.isArray( json.data )){
                json.data.splice( 0, requestStart-cacheLower );
                json.data.splice( requestLength, json.data.length );
	            $('#datatable_ajax tbody').off( 'click', 'button');
	            $('#datatable_ajax').off('click', '.filter-submit');
	            $('#datatable_ajax').off('click', '.filter-cancel');
                drawCallback(json);
            }
        }
    }
};
 



// Register an API method that will empty the pipelined data, forcing an Ajax
// fetch on the next draw (i.e. `table.clearPipeline().draw()`)
$.fn.dataTable.Api.register( 'clearPipeline()', function () {
    return this.iterator( 'table', function ( settings ) {
        settings.clearCache = true;
    } );
} );
 

var grid = new Datatable(); 
    var TableDatatablesAjax = function () {

     var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }	

    var handleRecords = function () {
        grid.init("#datatable_ajax" , "../controllers/index.php?accion=Ciclos2");
        $('#datatable_ajax tbody').on( 'click', 'button', function () {
             var data = grid.tableEdit.row( $(this).parents('tr') ).data();
             if(data.length > 0){
                document.location.href = "/newCiclo?id=" + data[0];
             }
        } );

        $('#datatable_ajax tbody').on( 'click', 'span', function () {
            var id = this.id;
            var data = grid.tableEdit.row( $(this).parents('tr') ).data();
            if(confirm("Esta seguro de cambiar de estado el registro?")){
                // changeStatus
                var data = {
                    accion : "Ciclos2.changeStatus",
                    id : data[0] , 
                    estado : id
                }
                ahttp.post("./controllers/index.php" , getInfo , data);
            }


            function getInfo(r , b){
                b();
                if(r){
                    alert("Registro cambiado" , "Ciclos" , "success" , function(){
                        grid.reload();
                    });
                }
            }
        });

        $(".newCiclo").on("click" , function(){
            document.location.href = "/newCiclo";
        });
    }

    return {

        //main function to initiate the module
        init: function () {

            initPickers();
            handleRecords();
        }

    };

}();

var printCal = function (){
	
	var data = {
			grupo: $("#gruposcalendario").val(),
        	}
        	ahttp.post("./controllers/index.php?accion=Ordenes.calendario" , function(r, b){
				b();
				printCalendar(r,b);
            	//$scope.formData.materiales = r.materiales;
            }, data);
}
 $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
          // e.target // newly activated tab
          var tab = $(e.target).attr("href");
          if(tab == "#calendar_tab"){
			  eventCalendar();
				printCal();
          }
		  else if(tab == "#listado"){
				
    grid.getDataTable().ajax.reload();
		  }
          // e.relatedTarget // previous active tab
        });
		
		var initDrag = function(el) {
                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim(el.text()) // use the element's text as the event title
                };
                // store the Event Object in the DOM element so we can get to it later
                el.data('eventObject', eventObject);
                // make the event draggable using jQuery UI
                el.draggable({
                    zIndex: 999,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0 //  original position after the drag
                });
            };

            addEvent = function(title) {
                title = title.length === 0 ? "Untitled Event" : title;
                var html = $('<div class="external-event label label-default">' + title + '</div>');
                jQuery('#event_box').append(html);
                initDrag(html);
            };

				
var printCalendar = function(r , b){
   b();
   console.log(r);
   if(r){
    var events = [] , calendario = {} , fecha ;
    for(var d in r.calendario){
     
     fecha = moment(r.calendario[d]['fecha_agendada']).format("YYYY-MM-DD HH:mm");
	 
	 fecha2 = moment(r.calendario[d]['fecha_fin']).format("YYYY-MM-DD HH:mm");
     // fecha = moment(r.data[d][2]).format("id");
	 if(r.calendario[d]['grupo_trabajo']==1){
     events.push({
      title :  r.calendario[d]['id'],
      tipo_trabajo :  r.calendario[d]['tipo_trabajo'],
      cliente :  r.calendario[d]['cliente'],
      start : new Date(fecha),
      end : new Date(fecha2),
      backgroundColor : App.getBrandColor('blue'),
      description : "Orden tipo "+ r.calendario[d]['id']+ " para el Cliente "+r.calendario[d]['fecha_agendada'] ,
     });}
	 else if(r.calendario[d]['grupo_trabajo']==2){
     events.push({
      title :  r.calendario[d]['id'],
      tipo_trabajo :  r.calendario[d]['tipo_trabajo'],
      cliente :  r.calendario[d]['cliente'],
      start : new Date(fecha),
      end : new Date(fecha2),
      backgroundColor : App.getBrandColor('red'),
      description : "Orden tipo "+ r.calendario[d]['id']+ " para el Cliente "+r.calendario[d]['fecha_agendada'] ,
     });}
	 else if(r.calendario[d]['grupo_trabajo']==3){
     events.push({
      title :  r.calendario[d]['id'],
      tipo_trabajo :  r.calendario[d]['tipo_trabajo'],
      cliente :  r.calendario[d]['cliente'],
      start : new Date(fecha),
      end : new Date(fecha2),
      backgroundColor : App.getBrandColor('yellow'),
      description : "Orden tipo "+ r.calendario[d]['id']+ " para el Cliente "+r.calendario[d]['fecha_agendada'] ,
     });}
	 
     //console.log(r.calendario[d]['fecha_agendada']);
    }
    calendar.reload(events);
   }
  }
  
  var eventCalendar = function () {
	  $('#event_box').empty();
  var data = {
        	}
        	ahttp.post("./controllers/index.php?accion=Ordenes.calendarioEvent" , function(r, b){
				b();
				console.log(r.calendario);
				for(var d in r.calendario){
				addEvent(''+r.calendario[d]['id']);
				}
            	//$scope.formData.materiales = r.materiales;
            }, data);
  }
jQuery(document).ready(function() {
	console.log(ahttp);
    TableDatatablesAjax.init();
	$('#summernote_1').summernote({height: 300});
	var tiempo = $('#clockface_2').clockface('getTime');
});

    // var app = angular.module('app', []);
    app.controller('orderController', ['$scope','$http','$interval','$controller' , 'client', function($scope,$http,$interval,$controller ,client){
        $scope.formData = {
        	areas : [],
        	roles : [],
        	trabajos_tipos : ['Correctivo' , 'Instalacion' , 'Mantenimiento']
        };

        $scope.get = function(id){
            $scope.limpiar();
            if(id > 0){
                $scope.getOrden(id);
            }
        }
		$scope.editRowMaterial = function(data , index){
		if(data){
			$scope.formData.materiales[index].edit = 1;
		}
		}
		$scope.editRowHerramienta = function(data , index){
		if(data){
			$scope.formData.herramientas[index].edit = 1;
		}
		}
		$scope.saveRowMaterial = function(material , index){
			var data = {
				id_cotizacion : $scope.formData.id_order,
				id_material : $scope.formData.materiales[index]['id'],
        		cantidad : $("#cantidad"+index).val()
        	}
			 client.post("./controllers/index.php?accion=Ordenes.editMaterial" , function(r, b){
				b();
            	$scope.formData.materiales = r.materiales;
            }, data);
		}
		$scope.saveRowHerramienta = function(material , index){
			var data = {
				id_cotizacion : $scope.formData.id_order,
				id_herramienta : $scope.formData.herramientas[index]['id'],
        		herramienta : $("#herramienta"+index).val(),
        		requerimientos : $("#requerimientos"+index).val()        		
        	}
			 client.post("./controllers/index.php?accion=Ordenes.editHerramienta" , function(r, b){
				b();
            	$scope.formData.herramientas = r.herramientas;
            }, data);
		}
		
		
  
        $scope.getOrden = function (ordenID){
            $scope.formData.id_order = ordenID;
            var data = {
            id_orden: ordenID
             }
            client.post("./controllers/index.php?accion=Ordenes.edit" , function(r, b){
            b();
            $("a[href='#registro']").tab("show");
                    //$("#registro").css("active");
                   // $("#registro_2").css("active");
                    //$("#listado").css("active");
                    //$("#listado_2").css("active");
                 var reg = angular.element( document.querySelector( '#registro' ) );
                 reg.addClass('active'); 
                 var reg2 = angular.element( document.querySelector( '#registro_2' ) );
                 reg2.addClass('active');  
                var lis = angular.element( document.querySelector( '#listado' ) );
                lis.removeClass('active');  
                var lis2 = angular.element( document.querySelector( '#listado_2' ) );
                lis2.removeClass('active'); 
                console.log(r);
				//printCalendar(r,b);
                $scope.formData.fecha = r.data.fecha_aprobacion;
                $scope.formData.cliente = r.data.nombre;
                $scope.formData.tipo_cliente = r.data.tipo_cliente;
                $scope.formData.direccion = r.data.direccion;
                $scope.formData.tipos_trabajo = r.data.tipo_trabajo;
                $scope.formData.tiempo = r.data.tiempo_estimado;
				$scope.formData.materiales = r.materiales;
				$("#summernote_1").code(r.data.obser);
				$scope.formData.insclimA = r.insclimA;
				$scope.formData.manclimA = r.manclimA;
				$scope.formData.corclimA = r.corclimA;
				$scope.formData.insclimB = r.insclimB;
				$scope.formData.manclimB = r.manclimB;
				$scope.formData.corclimB = r.corclimB;
				$scope.formData.herramientas2 = r.herramientas2;
				$scope.formData.materiales2 = r.materiales2;
				$scope.formData.insVentilacion = r.insVentilacion;
				$scope.formData.manVentilacion = r.manVentilacion;
				$scope.formData.corVentilacion = r.corVentilacion;
				$scope.formData.insRefri = r.insRefri;
				$scope.formData.manRefri = r.manRefri;
				$scope.formData.corRefri = r.corRefri;
				$scope.formData.herramientas = r.herramientas;
				$scope.formData.equiposclimA = r.equiposclimA;
				$scope.formData.equiposclimB = r.equiposclimB;
				$scope.formData.equiposRefri = r.equiposRefri;
				$scope.formData.equiposVentilacion = r.equiposVentilacion;

            }, data);
        }


		$scope.addRowMateriales = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		item : $("#materiales2").val(),
        		cantidad : $("#cantidad").val()
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addMaterial" , function(r, b){
				b();
            	$scope.formData.materiales = r.materiales;
            }, data);
        }
		
		$scope.addRowinsclimA = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipo").val(),
				table : 'ins_caracteristicas_climatizacion_a'
        		
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsclimA" , function(r, b){
				b();
            	$scope.formData.insclimA = r.insclimA;
            }, data);
        }
		
		$scope.addRowmanclimA = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoMA").val(),
				table : 'man_caracteristicas_climatizacion_a'
        		
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsclimA" , function(r, b){
				b();
            	$scope.formData.manclimA = r.insclimA;
            }, data);
        }
		
		$scope.addRowcorclimA = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoCA").val(),
				table : 'cor_caracteristicas_climatizacion_a'
        		
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsclimA" , function(r, b){
				b();
            	$scope.formData.corclimA = r.insclimA;
            }, data);
        }
		
		$scope.addRowinsclimB = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoB").val(),
				table : 'ins_caracteristicas_climatizacion_b'
        		
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsclimB" , function(r, b){
				b();
				//console.log(r.data);
            	$scope.formData.insclimB = r.insclimB;
            }, data);
        }
		
		$scope.addRowmanclimB = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoMB").val(),
        		table : 'man_caracteristicas_climatizacion_b'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsclimB" , function(r, b){
				b();
				//console.log(r.data);
            	$scope.formData.manclimB = r.insclimB;
            }, data);
        }
		
		$scope.addRowcorclimB = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoCB").val(),
        		table : 'cor_caracteristicas_climatizacion_b'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsclimB" , function(r, b){
				b();
				//console.log(r.data);
            	$scope.formData.corclimB = r.insclimB;
            }, data);
        }
		
		$scope.addRowinsVenti = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoV").val(),
        		table : 'ins_caracteristicas_ventilacion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsVenti" , function(r, b){
				b();
            	$scope.formData.insVentilacion = r.insVentilacion;
            }, data);
        }
		
		$scope.addRowmanVenti = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoMV").val(),
        		table : 'man_caracteristicas_ventilacion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsVenti" , function(r, b){
				b();
            	$scope.formData.manVentilacion = r.insVentilacion;
            }, data);
        }
		
		$scope.addRowcorVenti = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoCV").val(),
        		table : 'cor_caracteristicas_ventilacion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsVenti" , function(r, b){
				b();
            	$scope.formData.corVentilacion = r.insVentilacion;
            }, data);
        }
		
		$scope.addRowinsRefri = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoR").val(),
        		table : 'ins_caracteristicas_refrigeracion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsRefri" , function(r, b){
				b();
            	$scope.formData.insRefri = r.insRefri;
            }, data);
        }
		
		$scope.addRowmanRefri = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoMR").val(),
        		table : 'man_caracteristicas_refrigeracion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsRefri" , function(r, b){
				b();
            	$scope.formData.manRefri = r.insRefri;
            }, data);
        }
		
		$scope.addRowcorRefri = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		equipo : $("#equipoCR").val(),
        		table : 'cor_caracteristicas_refrigeracion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addinsRefri" , function(r, b){
				b();
            	$scope.formData.corRefri = r.insRefri;
            }, data);
        }
		
		$scope.addRowHerramientas = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
        		herramienta : $("#herramientas").val(),
        	}
			 client.post("./controllers/index.php?accion=Ordenes.addHerramienta" , function(r, b){
				b();
				console.log(r);
            	$scope.formData.herramientas = r.herramientas;
            }, data);
        }

		$scope.deleteMaterial = function(item , index){
        	var data = {
				id_material : $scope.formData.materiales[index]['id'],
				id_cotizacion : $scope.formData.id_order
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteMaterial" , function(r, b){
				b();
            	$scope.formData.materiales = r.materiales;
            }, data);
        	
        }
		

		$scope.deleteinsclimA = function(item , index){
        	var data = {
				id : $scope.formData.insclimA[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'ins_caracteristicas_climatizacion_a'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsclimA" , function(r, b){
				b();
            	$scope.formData.insclimA = r.insclimA;
            }, data);
        	
        }
		
		$scope.deletemanclimA = function(item , index){
        	var data = {
				id : $scope.formData.manclimA[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'man_caracteristicas_climatizacion_a'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsclimA" , function(r, b){
				b();
            	$scope.formData.manclimA = r.insclimA;
            }, data);
        	
        }
		
		$scope.deletecorclimA = function(item , index){
        	var data = {
				id : $scope.formData.corclimA[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'cor_caracteristicas_climatizacion_a'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsclimA" , function(r, b){
				b();
            	$scope.formData.corclimA = r.insclimA;
            }, data);
        	
        }
		
		$scope.deleteinsVenti = function(item , index){
        	var data = {
				id : $scope.formData.insVentilacion[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'ins_caracteristicas_ventilacion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsVentilacion" , function(r, b){
				b();
            	$scope.formData.insVentilacion = r.insVentilacion;
            }, data);
        	
        }
		
		$scope.deletemanVenti = function(item , index){
        	var data = {
				id : $scope.formData.manVentilacion[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'man_caracteristicas_ventilacion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsVentilacion" , function(r, b){
				b();
            	$scope.formData.manVentilacion = r.insVentilacion;
            }, data);
        	
        }
		
		$scope.deletecorVenti = function(item , index){
        	var data = {
				id : $scope.formData.corVentilacion[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'cor_caracteristicas_ventilacion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsVentilacion" , function(r, b){
				b();
            	$scope.formData.corVentilacion = r.insVentilacion;
            }, data);
        	
        }
		
		$scope.deleteinsRefri = function(item , index){
        	var data = {
				id : $scope.formData.insRefri[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'ins_caracteristicas_refrigeracion'				
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsRefri" , function(r, b){
				b();
            	$scope.formData.insRefri = r.insRefri;
            }, data);
        	
        }
		
		$scope.deletemanRefri = function(item , index){
        	var data = {
				id : $scope.formData.manRefri[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'man_caracteristicas_refrigeracion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsRefri" , function(r, b){
				b();
            	$scope.formData.manRefri = r.insRefri;
            }, data);
        	
        }
		$scope.deletecorRefri = function(item , index){
        	var data = {
				id : $scope.formData.corRefri[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'cor_caracteristicas_refrigeracion'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsRefri" , function(r, b){
				b();
            	$scope.formData.corRefri = r.insRefri;
            }, data);
        	
        }
		$scope.deleteinsclimB = function(item , index){
        	var data = {
				id : $scope.formData.insclimB[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'ins_caracteristicas_climatizacion_b'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsclimB" , function(r, b){
				b();
            	$scope.formData.insclimB = r.insclimB;
            }, data);
        	
        }
		
		$scope.deletemanclimB = function(item , index){
        	var data = {
				id : $scope.formData.manclimB[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'man_caracteristicas_climatizacion_b'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsclimB" , function(r, b){
				b();
            	$scope.formData.manclimB = r.insclimB;
            }, data);
        	
        }
		$scope.deletecorclimB = function(item , index){
        	var data = {
				id : $scope.formData.corclimB[index]['id'],
				id_cotizacion : $scope.formData.id_order,
				table : 'cor_caracteristicas_climatizacion_b'
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteinsclimB" , function(r, b){
				b();
            	$scope.formData.corclimB = r.insclimB;
            }, data);
        	
        }
		$scope.deleteHerramienta = function(item , index){
        	var data = {
				id_herramienta : $scope.formData.herramientas[index]['id'],
				id_cotizacion : $scope.formData.id_order
        	}
			 client.post("./controllers/index.php?accion=Ordenes.deleteHerramienta" , function(r, b){
				b();
            	$scope.formData.herramientas = r.herramientas;
            }, data);
        	
        }
		
		$scope.editDatos = function(){
        	var data = {
				id_cotizacion : $scope.formData.id_order,
				tiempo_estimado : $('#clockface_2').clockface('getTime'),
				observaciones :$("#summernote_1").code(),
        	}
			 client.post("./controllers/index.php?accion=Ordenes.editDatos" , function(r, b){
				b();
				location.href="http://cegaservices2.procesos-iq.com/ordenesTrabajo";
            	//$scope.formData.herramientas = r.herramientas;
            }, data);
        	
        }

       

        $scope.limpiar = function(){
            $scope.formData.fecha = "";
            $scope.formData.cliente = "";
            $scope.formData.tipo_cliente = 0;
            $scope.formData.direccion = "";
            $scope.formData.tipo_trabajo = 0;
			$scope.formData.observaciones = "";
			$("#summernote_1").code("");
			$scope.formData.tiempo = "";
            $scope.formData.insclimA = [];
            $scope.formData.manclimA = [];
            $scope.formData.corclimA = [];
            $scope.formData.insclimB = [];
            $scope.formData.manclimB = [];
            $scope.formData.corclimB = [];
            $scope.formData.insRefri = [];
            $scope.formData.herramientas2 = [];
            $scope.formData.materiales2 = [];
            $scope.formData.manRefri = [];
            $scope.formData.corRefri = [];
			$scope.formData.insVentilacion = [];
			$scope.formData.manVentilacion = [];
			$scope.formData.corVentilacion = [];
            $scope.formData.materiales = [];
            $scope.formData.herramientas = [];
            $scope.formData.roles = [];
            $scope.formData.equiposclimA = [];
            $scope.formData.equiposRefri = [];

        }


        $(".save").on("click" , function(){
            if($scope.formData.fecha != "" && $scope.formData.cliente != "" && $scope.formData.tipo_cliente != "" && $scope.formData.direccion != "" && $scope.formData.tipo_cliente != "" && $scope.formData.areas.length > 0){
                console.log($scope.formData);
            }else{
                alert("Favor de completar el formulario", "Ordenes de trabajo", "error");
            }
        })

    }]);