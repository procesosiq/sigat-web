'use strict';
app.filter('startfrom', function () {
    return function (input, start) {
        if (input === undefined) {
            return "loading...";
        }
        start = +start; //parse to int
        return input.slice(start);
    };
});

app.filter('num', function () {
    return function (input) {
        return parseInt(input, 10);
    };
});

app.service('pilotos', function($http){
    var service = {};
    var options = {
        url: "./controllers/index.php",
        method: "POST"
    };

    service.listado = function () {
        var option = options;
        option.data = {
            accion: "Pilotos"
        };
        return $http(option);
    };

    service.fumigadoras = function(){
        var option = options;
        option.data = {
            accion: "Pilotos.getFumigadoras"
        };
        return $http(option);
    }

    service.placas = function(id_fumigadora){
        var option = options;
        option.data = {
            accion: "Pilotos.getPlacas",
            id_fumigadora: id_fumigadora
        };
        return $http(option);
    }

    service.show = function (id_piloto) {
        var option = options;
        option.data = {
            accion: "Pilotos.show",
            id_piloto: id_piloto
        };
        return $http(option);
    };

    service.create = function (producto) {
        var option = options;
        producto.accion = "Pilotos.create";
        option.data = producto;
        return $http(option);
    };

    service.update = function (producto) {
        var option = options;
        producto.accion = "Pilotos.update";
        option.data = producto;
        return $http(option);
    };

    return service;
});

app.service('configuracion', function ($http) {
    var configuracion = {};
    var options = {
        url: "./controllers/index.php",
        method: "POST"
    };

    configuracion.save = function (detalle) {
        var option = options;
        detalle.accion = "Pilotos.saveConfiguracion";
        option.data = detalle;
        return $http(option);
    };

    return configuracion;
});

app.controller('pilotos_controller', ['$scope', '$http', '$interval', '$controller', 'pilotos' , 'configuracion', function ($scope, $http, $interval, $controller, pilotos , configuracion) {

    // 26/04/2017 - TAG: CONFIGURACION
    $scope.configuracion = {
        state: "listado",
        modo: "",
        nombre: "",
        fumigadora: "",
        fumigadoras: [],
        campo: "",
        load : function(){
            pilotos.fumigadoras().then(function (data) {
                $scope.configuracion.fumigadoras = $scope.proccess(data);
            }).catch(function (error) {
                return console.log(error);
            });
        },
        save: function save() {
            if (this.campo != "") {
                var nombre = this.nombre;
                var modo = this.modo;
                var campo = this.campo;
                var fumigadora = this.fumigadora;
                var data = {
                    nombre: nombre,
                    modo: modo,
                    fumigadora: fumigadora,
                    campo: campo
                };
                $('#configuracion').modal('hide');
                configuracion.save(data).then(function (data) {
                    if ($scope.configuracion.state != "listado") {
                        if ($scope.configuracion.nombre == "Fumigadora"){
                            $scope.getFumigadoras();
                            $scope.piloto.placa = ""
                            $scope.getPlacas();
                        } 
                        if ($scope.configuracion.nombre == "Placa"){
                            $scope.getPlacas();
                        }

                        $scope.configuracion.fumigadora = ""
                        $scope.configuracion.campo = ""
                    }
                }).catch(function (error) {
                    return console.log(error);
                });
            }
        }
    };

    $scope.setTipo = function (tipo) {
        $scope.configuracion.nombre = tipo;
        if ($scope.configuracion.nombre == "Fumigadora") $scope.configuracion.modo = "fumigadora";
        if ($scope.configuracion.nombre == "Placa") $scope.configuracion.modo = "placa";
    };
    // 26/04/2017 - TAG: CONFIGURACION

    // 11/05/2017 - TAG: FORM PILOTO
    $scope.proccess = function (r) {
        if (r.hasOwnProperty("data")) {
            return r.data;
        }
    };

     $scope.piloto = {
        id_piloto: 0,
        codigo: "",
        fumigadora : "",
        placa : ""
     }

     $scope.oldPiloto = angular.copy($scope.piloto);


    $scope.clear = function () {
        $scope.piloto.id_piloto = 0;
        $scope.piloto.codigo = "";
        $scope.piloto.nombre = "";
        $scope.piloto.fumigadora = "";
        $scope.piloto.placa = "";
    };

    $scope.fumigadoras = [];
    $scope.placas = [];
    $scope.getFumigadoras = function () {
        pilotos.fumigadoras().then(function (data) {
            $scope.fumigadoras = $scope.proccess(data);
            $scope.configuracion.fumigadoras = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.getPlacas = function () {
        pilotos.placas($scope.piloto.fumigadora).then(function (data) {
            return $scope.placas = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };


     $scope.show = function () {
        pilotos.show($scope.piloto.id_piloto).then(function (data) {
            var response = $scope.proccess(data);
            if (response.hasOwnProperty("success") && response.success == 200) {
                $scope.piloto = response.data;
                $scope.getPlacas();
            }
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.init = function () {
        $scope.getFumigadoras();
        $scope.show();
        $("#fumigadora").on("change" , function(){
            $scope.piloto.fumigadora = $(this).val();
            $scope.getPlacas()
        })
        $scope.configuracion.state = "piloto";
    };

    $scope.getPilot = function (id_piloto) {
        var tab = "createpiloto";
        $scope.clear();
        $scope.piloto.id_piloto = id_piloto || 0;
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        $scope.init();
    };

    $scope.save = function(){
        if (!angular.isObject($scope.piloto)) {
            return false;
        } else if ($scope.piloto.nombre.length <= 0 || $scope.piloto.nombre == "") {
            alert("Favor de ingresar Nombre del Piloto");
        }else{
            if ($scope.piloto.id_piloto <= 0) {
                pilotos.create($scope.piloto).then(function (data) {
                    return $scope.success(data);
                }).catch(function (error) {
                    return console.log(error);
                });
            } else {
                pilotos.update($scope.piloto).then(function (data) {
                    return $scope.success(data);
                }).catch(function (error) {
                    return console.log(error);
                });
            }
        }
    }

     $scope.success = function (r) {
        var response = $scope.proccess(r);
        $scope.piloto.id_piloto = response.data;
        $scope.piloto.codigo = response.data;
        alert("Registro registrado/modificado con el ID " + $scope.piloto.id_piloto, "Pilotos", "success");
    };
    
    // 11/05/2017 - TAG: FORM PILOTO


    $scope.cancel = function(){
        var tab = "listado";
        $scope.piloto.id_piloto = 0;
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        $scope.clear();
        $scope.index();
    }

    // 25/04/2017 - TAG: SECCION FOR TABLE 
    $scope.table = [];


    $scope.index = function () {
        pilotos.listado().then(function (data) {
            return $scope.table = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
        $scope.configuracion.state = "listado";
        $scope.configuracion.load();
    };

    $scope.search = {
        nombre: "",
        orderBy: "codigo",
        reverse: false,
        limit: 10,
        actual_page: 1 // las paginas comienzan apartir del 1
    };
    

    //ordenamiento por columnas
    $scope.changeSort = function (column) {
        if ($scope.search.orderBy != column) {
            var previous = $("th.selected")[0];
            $(previous).removeClass("selected");
            $(previous).removeClass("sorting_asc");
            $(previous).removeClass("sorting_desc");
        }
        $scope.search.reverse = $scope.search.orderBy != column ? false : !$scope.search.reverse;
        $scope.search.orderBy = column;
        var actual_select = $("#" + column + "_column");
        if (!actual_select.hasClass("selected")) {
            actual_select.addClass("selected");
        }
        if ($scope.search.reverse) {
            actual_select.addClass("sorting_desc");
            actual_select.removeClass("sorting_asc");
        } else {
            actual_select.addClass("sorting_asc");
            actual_select.removeClass("sorting_desc");
        }
    };

    //ir a la siguiente pagina
    $scope.next = function (dataSource) {
        if ($scope.search.actual_page < parseInt(dataSource.length / parseInt($scope.search.limit)) + (dataSource.length % parseInt($scope.search.limit) == 0 ? 0 : 1)) $scope.search.actual_page++;
    };

    //ir a la pagina anterior
    $scope.prev = function (dataSource) {
        if ($scope.search.actual_page > 1) $scope.search.actual_page--;
    };
}]);
