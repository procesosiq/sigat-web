
            // chart2("temp_min");
            // chart2("temp_min2");
            // chart2("precp");
            // chart2("temp_max");
            // chart2("radiacion");
            // chart2("humedad");



app.controller('3metros', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){
    
    $scope.semanas = [];
    $scope.default = ["#d12610","#000000"];
    $scope.color = ["#028DFF", "#29D61D", "#ff4829", "#fe952c"];
    $scope.FocosColor = {};
    $scope.charts = {
        libre_strias : [],
        hojas_3 : [],
        hojas_4 : [],
        hojas_5 : [],
        hojas_total : [],
        hoja_vieja_menor_5 : [],
    }
    $scope.generateWeeks = function(){
        for (var i = 1; 53 > i; i++) {
            $scope.semanas.push(i);
        }
        // console.log($scope.semanas);
    }

    $scope.init = function(){
        $scope.generateWeeks();
        $scope.getData();
        $interval($scope.getData, 30000);
    }

    $scope.random = [];

    $scope.getData = function(){
        var data = {
            opt : 'HMVLDQMEN5'
        }
        client.post("./controllers/3_metros.php" ,$scope.printGraphycMenor5 , data);
        
        data = {};
        data = {
            opt : "HMVLDE"
        }
        
        client.post("./controllers/3_metros.php" ,$scope.printGraphycLibreStrias , data);

        data = {};
        data = {
            opt : "HOJA4"
        }
        
        client.post("./controllers/3_metros.php" ,$scope.printGraphycHoja4 , data);


        data = {};
        data = {
            opt : "HOJA3"
        }
        
        client.post("./controllers/3_metros.php" ,$scope.printGraphycHoja3 , data);

        data = {};
        data = {
            opt : "HOJA5"
        }
        
        client.post("./controllers/3_metros.php" ,$scope.printGraphycHoja5 , data);

        data = {};
        data = {
            opt : "HOJTOT"
        }
        
        client.post("./controllers/3_metros.php" ,$scope.printGraphycHojasTotales , data);

    }

    $scope.printGraphycMenor5 = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hoja_vieja_menor_5");
            $scope.charts.hoja_vieja_menor_5 = r.tabla;
            // console.log("hoja_vieja_menor_5")
        }
    }

    $scope.printGraphycLibreStrias = function(r , b){
        b();
        if(r){
            $scope.printData(r , "libre_estrias");
            $scope.charts.libre_strias = r.tabla;
            // console.log("libre_estrias")
        }
    }

    $scope.printGraphycHoja4 = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hoja_4");
            $scope.charts.hojas_4 = r.tabla;
            // console.log("hojas_4")
        }
    }    
    $scope.printGraphycHoja5 = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hoja_5");
            $scope.charts.hojas_5 = r.tabla;
            // console.log("hojas_5")
        }
    }

    $scope.printGraphycHoja3 = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hoja_3");
            $scope.charts.hojas_3 = r.tabla;
            // console.log("hojas_3")
        }
    }

    $scope.printGraphycHojasTotales = function(r , b){
        b();
        if(r){
            $scope.printData(r , "hojas_total");
            $scope.charts.hojas_total = r.tabla;
            // console.log("hojas_total")
        }
    }

    $scope.printData = function(r , id){
        if(r){  
            var data = [];
            var minimo = (r.min > 2) ? (r.min - 2) : r.min;
            if(id == "hoja_vieja_menor_5" || id == "hojas_total"){
                if(r.min <= 8 && r.min > 0){
                    minimo = r.min-1
                }else{
                    minimo = 6;
                }
            }
            if(id == "libre_estrias"){
                if(r.min <= 6 && r.min > 0){
                    minimo = r.min-1
                }else{
                    minimo = 6;
                }
            }


            // if(!$scope.FocosColor.hasOwnProperty(id)){
            //     $scope.FocosColor[id.toString()] = {};
            // }
            console.log($scope.FocosColor)
            // console.log(id + minimo);
            var options =  {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.05
                                }, {
                                    opacity: 0.01
                                }]
                            }
                        },
                        points: {
                            show: false,
                            radius: 3,
                            lineWidth: 1
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderColor: "#eee",
                        borderWidth: 1
                    },
                    colors: ["#d12610", "#37b7f3", "#33691E", "#AB47BC", "#957100", "#00897B", "#CDDC39", "#FF9800", "#795548"],
                    xaxis: {
                        ticks: 12,
                        tickDecimals: 0,
                        tickColor: "#eee",
                    },
                    yaxis: {
                        ticks: 12,
                        tickDecimals: 0,
                        tickColor: "#eee",
                        min: minimo,
                        max: (id == "hoja_5") ? r.max : null
                    },
                    legend:{         
                        backgroundOpacity: 0.5,
                        noColumns: 0,  
                        container : $("#label_"+id)
                    }
                };
            var id = id;
            var color = "";
            var count = 0;


            var contenedor = $("#check_"+id);
            var i = 0;
            var check = 'checked="checked"';
            contenedor.empty();
            var label = "";
            var etiqueta = "";
            for(var info in r.datos){
                // console.log(info)
                // console.log(Object.keys(r.datos[info]))
                etiqueta = Object.keys(r.datos[info])[0];
                // console.log(etiqueta)
                if(r.validate != null && r.validate.hasOwnProperty(id)){
                    if(Object.keys(r.validate).length > 0 && r.validate[id].indexOf(etiqueta) != -1){
                        check = "";
                    } else{
                        check = 'checked="checked"';
                    } 
                }

                if(info == 0){
                    check = 'checked="checked"';
                }

                var li = $('<li />').appendTo(contenedor);
                label = $('<label>', {
                    text: etiqueta
                }).appendTo(li);
                $('<input name="' + etiqueta + '" id="' + etiqueta + '" data-name="'+etiqueta+'" class="labels" data-id="'+id+'" type="checkbox" '+check+' />').appendTo(label);
            }

            function plotAccordingToChoices() {
                var data = [];
                options.legend.container = $("#label_"+id);
                var etiqueta = "";
                contenedor.find("input:checked").each(function() {
                    var key = this.name;
                    for(var info in r.datos){
                        // console.log(info)
                        // console.log(Object.keys(r.datos[info]))
                        etiqueta = Object.keys(r.datos[info])[0];
                        // console.log(etiqueta)
                        if($scope.FocosColor.hasOwnProperty(etiqueta)){
                            color = $scope.FocosColor[etiqueta];
                        }else{
                            if(info == 0){
                                color = $scope.default[0];
                            }
                            else if(etiqueta.toLowerCase() == ('Resto Finca').toLowerCase()){
                                color = $scope.default[1];
                            }else{
                                $scope.FocosColor[etiqueta] = $scope.color[count];
                                count++;
                                color = $scope.FocosColor[etiqueta];
                            }
                        }
                        console.log(color)
                        if (key === etiqueta) {
                            data.push({
                                data : r.datos[info][etiqueta],
                                label: etiqueta,
                                info : info,
                                color : color,
                                lines: {
                                    lineWidth: 1,
                                },
                                shadowSize: 0
                            });
                        }
                    }
                });
                var plot = $.plot($('#'+id), data,options);
            }

            plotAccordingToChoices();
            contenedor.find("input").change(plotAccordingToChoices);


            $(".miniature").on("click" ,".labels", function(event){
                var id = $(this).attr("data-id");
                var key = [];
                var name = "";
                event.stopImmediatePropagation();
                var contenedor = $("#check_"+id);
                contenedor.find("input:checked").each(function(item , element) {
                    if(isNaN(parseFloat(this.name))){
                        name = this.name;
                        key.push(name);
                    }
                });
                $scope.saveLabel(id , key);
            });
            function getValues(){
            }
            getValues();


            function showTooltip(x, y, contents) {
                $('<div id="tooltip">' + contents + '</div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y + 5,
                    left: x - 30,
                    border: '1px solid #333',
                    padding: '4px',
                    color: '#fff',
                    'border-radius': '3px',
                    'background-color': '#333',
                    opacity: 0.80
                }).appendTo("body").fadeIn(200);
            }

            var previousPoint = null;
            $('#'+id).bind("plothover", function(event, pos, item) {
                $("#x").text(pos.x.toFixed(0));
                $("#y").text(pos.y.toFixed(2));

                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;

                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(2),
                            y = item.datapoint[1].toFixed(2);

                        showTooltip(item.pageX, item.pageY,"Año " + item.series.label + " Semana " + x + " Valor " + y);
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            
            });
        }   
    }

    $scope.saveLabel = function(type , labels){
        if(labels.length > 0){
            var data = {
                opt : "SAVE_LABELS",
                type : type,
                labels : labels
            }
            client.post("./controllers/3_metros.php" ,function(r,b){b();} , data);
        }
    }
}]);
