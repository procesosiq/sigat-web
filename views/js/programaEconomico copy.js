'use strict';

app.filter('startfrom', function () {
    return function (input, start) {
        if (input === undefined) {
            return "loading...";
        }
        start = +start; //parse to int
        return input.slice(start);
    };
});

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('num', function () {
    return function (input) {
        return parseFloat(input, 10);
    };
});

app.service('programa', function($http){
    var service = {};
    var options = {
        url: "./controllers/index.php",
        method: "POST"
    };

    service.listado = function () {
        var option = options;
        option.data = {
            accion: "CiclosAplicacionHistorico.economico"
        };
        return $http(option);
    };

    return service;
});

app.controller('programa', ['$scope', '$http', '$interval', '$controller', 'programa', function ($scope, $http, $interval, $controller, programa) {

    $scope.init = function(){
        $(".portlet-body").delegate(".table-container div .bootstrap-switch-container", "click", $scope.changeMode);
        $(".portlet-body .table-container div .bootstrap-switch-handle-on").click($scope.changeMode);
        $(".portlet-body .table-container div .bootstrap-switch-handle-off").click($scope.changeMode);
        $scope.index();
    }

    $scope.proccess = function (r) {
        if (r.hasOwnProperty("data")) {
            return r.data;
        }
    };

    $scope.index = function(){
        programa.listado().then(function (data) {
            var data = $scope.proccess(data);
            $scope.table = data.table;
            $scope.table_real = data.table_real;
            $scope.fincas = data.fincas;
        }).catch(function (error) {
            return console.log(error);
        });
    }

    $scope.changeMode = function(){

    }
    
    $scope.init();

    $scope.search = {
        FINCA : "MATEO",
        orderBy: "CICLO",
        reverse: false,
    };
    $scope.search_real = {
        orderBy : "CICLO",
        reverse: false
    };
    $scope.search_estimado = {
        orderby : "CICLO",
        revser: false
    };

    $scope.disableColumns = function(table, column, event){
        var cells = $("#datatable_ajax_"+table+" th:nth-child("+column+"), td:nth-child("+column+")");
        var isActive = $(event.target).parent().hasClass("active");
        if(isActive){
            $($(event.target).parent()).removeClass("active");
            $($($($("#columns_table_"+table+" label[class='active']")[column-1]).find("span")[1]).find("i")).addClass("hide");
        }else{
            $($(event.target).parent()).addClass("active");
            $($($($("#columns_table_"+table+" label[class='active']")[column-1]).find("span")[1]).find("i")).removeClass("hide");
        }

        $.each(cells, function(index, value){
            if(!isActive)
                $(value).removeClass("hide");
            else
                $(value).addClass("hide");
        });
    }

    //ordenamiento por columnas
    $scope.changeSort = function (options, column) {
        if ($scope[options].orderBy != column) {
            var previous = $("th.selected")[0];
            $(previous).removeClass("selected");
            $(previous).removeClass("sorting_asc");
            $(previous).removeClass("sorting_desc");
        }
        $scope[options].reverse = $scope[options].orderBy != column ? false : !$scope[options].reverse;
        $scope[options].orderBy = column;
        var actual_select = $("#" + column + "_column");
        if (!actual_select.hasClass("selected")) {
            actual_select.addClass("selected");
        }
        if ($scope[options].reverse) {
            actual_select.addClass("sorting_desc");
            actual_select.removeClass("sorting_asc");
        } else {
            actual_select.addClass("sorting_asc");
            actual_select.removeClass("sorting_desc");
        }
    };

    //ir a la siguiente pagina
    $scope.next = function (dataSource) {
        if ($scope.search.actual_page < parseInt(dataSource.length / parseInt($scope.search.limit)) + (dataSource.length % parseInt($scope.search.limit) == 0 ? 0 : 1)) $scope.search.actual_page++;
    };

    //ir a la pagina anterior
    $scope.prev = function (dataSource) {
        if ($scope.search.actual_page > 1) $scope.search.actual_page--;
    };

}]);