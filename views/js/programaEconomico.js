'use strict';

app.filter('startfrom', function () {
    return function (input, start) {
        if (input === undefined) {
            return "loading...";
        }
        start = +start; //parse to int
        return input.slice(start);
    };
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        return sum / count;
    }
})

app.filter('countOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                count++;
            }
        });
        return count;
    }
})

app.filter('maxOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;

        var max = null;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                if(max == null || max < parseFloat(value[key])) max = parseFloat(value[key])
            }
        });
        return max;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('parseInt', function () {
    return function (input) {
        return parseFloat(input, 10);
    };
});

app.filter('num', function() {
    return function(input) {
        if(input)
            return new Number(input)
        else
            return ''
    }
});

app.service('programa', function($http){
    var service = {};
    var options = {
        url: "./controllers/index.php",
        method: "POST"
    };

    service.listado = function (params) {
        var option = options;
        option.data = {
            accion: "CiclosAplicacionHistorico.economico",
            params : params
        };
        return $http(option);
    };

    return service;
});

app.controller('programa', ['$scope', '$http', '$interval', '$controller', 'programa', function ($scope, $http, $interval, $controller, programa) {

    $("#container_angular").fadeIn('slow')

    $scope.filters = {
        year :  moment().format("YYYY"),
        gerente : 2 , 
        finca : 49,
        tipoHectarea : '',
        programa : 'sigatoka'
    }

    $scope.tipoPrograma = {
        'sigatoka' : 'SIGATOKA',
        'foliar' : 'FOLIAR',
        'plagas' : 'PLAGAS',
        'erwinia' : 'CONTROL ERWINIA',
        'sanidad' : 'SANIDAD',
        'todos' : 'TODOS'
    }

    $scope.tipoHectarea = {
        '' : 'FUMIGACIÓN',
        'h_produccion' : 'PRODUCCIÓN',
        'h_neta' : 'NETA',
        'h_banano' : 'BANANO'
    }

    // 20/06/2017 - TAG: FILTERS
    $scope.years = [];
    $scope.gerentes = [];
    $scope.fincas = [];
    // 20/06/2017 - TAG: FILTERS

    $scope.init = function(){
        $(".portlet-body").delegate(".table-container div .bootstrap-switch-container", "click", $scope.changeMode);
        $(".portlet-body .table-container div .bootstrap-switch-handle-on").click($scope.changeMode);
        $(".portlet-body .table-container div .bootstrap-switch-handle-off").click($scope.changeMode);
        $scope.index();
    }

    $scope.changeGerente = function(){
        if($scope.fincas.hasOwnProperty($scope.filters.gerente)){
            var idFinca = $scope.fincas[$scope.filters.gerente][0].id
            $scope.filters.finca = idFinca
            $scope.index();
        }
    }

    $scope.changeFinca = function(){
        $scope.index();
    }

    $scope.proccess = function (r) {
        if (r.hasOwnProperty("data")) {
            return r.data;
        }
    };

    $scope.index = function(){
        load.block()
        programa.listado($scope.filters).then(function (data) {
            load.unblock()
            var data = $scope.proccess(data);

            $scope.table_horizontal = []
            for(let i in data.table){
                let row = data.table[i]

                let row_p = {
                    ciclo : row.ciclo_programa,
                    programa : row.programa,
                    sem : row.sem,
                    fecha_real : row.fecha_real,
                    fecha_prog : row.fecha_prog,
                    atraso : row.atraso,
                    motivo : row.motivo,
                    ha : row.ha,
                    ha_oper : row.ha_oper,
                    oper : row.ha_oper * row.ha,
                    costo_total : row.costo_total,
                    costo_ha : row.costo_ha
                }

                row_p.fungicidas = []
                row_p.coadyuvantes = []
                row_p.erwinias = []
                row_p.foliares = []
                row_p.insecticidas = []
                row_p.reguladores = []
                row_p.aceites = []

                for(let j in row.detalle){
                    let prod = row.detalle[j]
                    switch(prod.tipo.toUpperCase()){
                        case 'FUNGICIDA': {
                            row_p.fungicidas.push(prod)
                            break;
                        }
                        case 'CONTROL ERWINIA': {
                            row_p.erwinias.push(prod)
                            break;
                        }
                        case 'FOLIAR': {
                            row_p.foliares.push(prod)
                            break;
                        }
                        case 'INSECTICIDA': {
                            row_p.insecticidas.push(prod)
                            break;
                        }
                        case 'COADYUVANTE': {
                            row_p.coadyuvantes.push(prod)
                            break;
                        }
                        case 'ACEITE': {
                            row_p.aceites.push(prod)
                            break;
                        }
                        case 'REGULADOR PH': {
                            row_p.reguladores.push(prod)
                            break;
                        }
                    }
                }

                $scope.table_horizontal.push(row_p)
            }

            $scope.table_horizontal_parcial = []
            for(let i in data.table_parcial){
                let row = data.table_parcial[i]

                let row_p = {
                    ciclo : row.ciclo_programa,
                    programa : row.programa,
                    sem : row.sem,
                    fecha_real : row.fecha_real,
                    fecha_prog : row.fecha_prog,
                    atraso : row.atraso,
                    motivo : row.motivo,
                    ha : row.ha,
                    ha_oper : row.ha_oper,
                    oper : row.ha_oper * row.ha,
                    costo_total : row.costo_total,
                    costo_ha : row.costo_ha
                }

                row_p.fungicidas = []
                row_p.coadyuvantes = []
                row_p.erwinias = []
                row_p.foliares = []
                row_p.insecticidas = []
                row_p.reguladores = []
                row_p.aceites = []

                for(let j in row.detalle){
                    let prod = row.detalle[j]
                    switch(prod.tipo.toUpperCase()){
                        case 'FUNGICIDA': {
                            row_p.fungicidas.push(prod)
                            break;
                        }
                        case 'CONTROL ERWINIA': {
                            row_p.erwinias.push(prod)
                            break;
                        }
                        case 'FOLIAR': {
                            row_p.foliares.push(prod)
                            break;
                        }
                        case 'INSECTICIDA': {
                            row_p.insecticidas.push(prod)
                            break;
                        }
                        case 'COADYUVANTE': {
                            row_p.coadyuvantes.push(prod)
                            break;
                        }
                        case 'ACEITE': {
                            row_p.aceites.push(prod)
                            break;
                        }
                        case 'REGULADOR PH': {
                            row_p.reguladores.push(prod)
                            break;
                        }
                    }
                }

                $scope.table_horizontal_parcial.push(row_p)
            }

            $scope.table = data.table;
            $scope.table_parcial = data.table_parcial;
            $scope.table_real = data.table_real;

            if(data.hasOwnProperty("years")){
                $scope.years = data.years;
            }
            if(data.hasOwnProperty("gerentes")){
                $scope.gerentes = data.gerentes;
            }
            if(data.hasOwnProperty("fincas")){
                $scope.fincas = data.fincas;
            }
        }).catch(function (error) {
            return console.log(error);
        });
    }
    
    $scope.init();

    $scope.search = {
        FINCA : "MATEO",
        orderBy: "sem",
        reverse: false,
        limit: 10,
        actual_page: 1, // las paginas comienzan apartir del 1
        columns : [
            false, false, true, true, false, true, true, false, false,
            true, true, true, false, false, false, true, true, true,
            true, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, true, true,
            true, false, false, false, false, false, false, false, false,
            false, true, true, true, false, false, false, false,
            false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false,
            false, false, false, false, true, true, true, false, false,
            false, false
        ]
    };
    $scope.search_real = {
        orderBy : "ciclo",
        reverse: false
    };
    $scope.search_estimado = {
        orderby : "ciclo",
        revser: false
    };
    $scope.search_parcial = {
        orderby : "ciclo",
        revser: false
    };

    $scope.disableColumns = function(column, table, event){
        var cells = $("#datatable_ajax_1 th:nth-child("+column+"), #datatable_ajax_1 tbody td:nth-child("+column+")");
        var isActive = !$($($("#columns_table_"+table+" div.checkbox label span i"))[column-1]).hasClass("hide");
        $scope.search.columns[column-1] = !isActive
        
        if(isActive){
            $($(event.target).parent()).removeClass("active");
            $($($("#columns_table_"+table+" div.checkbox label span i"))[column-1]).addClass("hide");
            $($($("#columns_table_"+table+" label[class='active']")[column-1]).find("input")[0]).prop("checked", false);
        }else{
            $($(event.target).parent()).addClass("active");
            $($($("#columns_table_"+table+" div.checkbox label span i"))[column-1]).removeClass("hide");
            $($($("#columns_table_"+table+" label[class='active']")[column-1]).find("input")[0]).prop("checked", true);            
        }

        $.each(cells, function(index, value){
            if(!isActive)
                $(value).removeClass("hide");
            else
                $(value).addClass("hide");
        });
    }

    //ordenamiento por columnas
    $scope.changeSort = function (options, column) {
        $scope[options].reverse = $scope[options].orderBy != column ? false : !$scope[options].reverse;
        $scope[options].orderBy = column;

    };

    //ir a la siguiente pagina
    $scope.next = function (dataSource) {
        if ($scope.search.actual_page < parseInt(dataSource.length / parseInt($scope.search.limit)) + (dataSource.length % parseInt($scope.search.limit) == 0 ? 0 : 1)) $scope.search.actual_page++;
    };

    //ir a la pagina anterior
    $scope.prev = function (dataSource) {
        if ($scope.search.actual_page > 1) $scope.search.actual_page--;
    };

    $scope.exportExcel = function(id_table, title){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var contentTable = table.innerHTML
                // remove filters
                /*var cut = contentTable.search('<tr role="row" class="filter">')
                var cut2 = contentTable.search('</thead>')

                var part1 = contentTable.substring(0, cut)
                var part2 = contentTable.substring(cut2, contentTable.length)
                contentTable = part1 + part2*/
                
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(id_table, title || "")
    }

    $scope.exportPrint = function(id_table){
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;
        // remove filters
        /*var cut = contentTable.search('<tr role="row" class="filter">')
        var cut2 = contentTable.search('</thead>')
        var part1 = contentTable.substring(0, cut)
        var part2 = contentTable.substring(cut2, contentTable.length)
        // add image
        contentTable = image + part1 + part2*/

        var newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
    }

    $scope.sum = (a, b) => {
        return parseFloat(a) + parseFloat(b)
    }

}]);