app.service('resumen', function($http){
    let service = {};
    let options = {
        url: "./controllers/index.php",
        method: "POST"
    };

    service.init = function (params) {
        let option = options;
        option.data = {
            accion: "Datos_Resumen.getIndicadores",
            params : params
        };
        return $http(option);
    };

    service.last = function (params) {
        let option = options;
        option.data = {
            accion: "Datos_Resumen.last",
            params : params
        };
        return $http(option);
    };

    service.indicadores = function(params){
        let option = options;
        option.data = {
            accion: "Datos_Ciclos.getIndicadores" , 
            params : params
        };
        return $http(option);
    }

    return service;
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
            let sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        let sum = 0, count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum += parseFloat(value[key], 10);
                count++
            }
        });
        return sum/count;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
        let filtered = [];
    	angular.forEach(items, function(item) {
      		filtered.push(item);
        });
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('num', function() {
	return function(val) {
        return parseFloat(val)
  	};
});

app.controller('resumenCiclos', ['$scope', 'resumen', '$filter', function ($scope, resumen, $filter) {

    $("#container_angular").fadeIn('slow')

    $scope.filters = {
        gerente : "",
        sem : "SEM",
        sem2 : "SEM",
        gerenteName : "SIGAT",
        encargadoName : 'SIGAT',
        year : moment().format("YYYY"),
        variable : '',
        variable : 'sigatoka',
        tipoHectarea : '',
        tipoCiclo : ''
    }
    $scope.tiposCiclo = {
        'CICLO' : 'CICLO',
        'PARCIAL' : 'PARCIAL',
        '' : 'TODOS'
    }
    $scope.tipoHectarea = {
        '' : 'FUMIGACIÓN',
        'produccion' : 'PRODUCCIÓN',
        'neta' : 'NETA',
        'banano' : 'BANANO'
    }
    $scope.tipoPrograma = {
        'sigatoka' : 'SIGATOKA',
        'foliar' : 'FOLIAR',
        'plagas' : 'PLAGAS',
        'erwinia' : 'CONTROL ERWINIA',
        'sanidad' : 'SANIDAD',
        'todos' : 'TODOS'
    }
    $scope.tables = {
        ciclos: {
            orderBy : 'finca',
            reverse : false
        },
        dolares : {
            orderBy : 'finca',
            reverse : false
        },
        dolares_ha : {
            orderBy : 'finca',
            reverse : false
        }
    }
    $scope.proccess = function (r) {
        if (r.hasOwnProperty("data")) {
            return r.data;
        }
    };
    $scope.semaforoSaldo = (value) => {
        if($.isNumeric(value)){
            if(value > 0){
                return 'bg-green-jungle bg-font-green-jungle'
            }else if(value == 0){
                return 'bg-yellow-gold bg-font-yellow-gold'
            }else{
                return 'bg-red-thunderbird bg-font-red-thunderbird'
            }
        }
    }

    getDolaresHaAnioFromResumenCostos = (_finca, _anio) => {
        if(_finca && _anio){
            let _data = angular.copy($scope.resumen_costos)
            for(let i in _data){
                let row = _data[i]
                if(row.finca == _finca){
                    return row[_anio]   
                }
            }
        }
        return null
    }

    $scope.years = [];
    $scope.gerentes = [];
    $scope.semanas = [];
    $scope.ciclos = [];
    $scope.data_ciclos = [];
    $scope.ciclos_aplicados = [];
    $scope.ciclos_aplicados_sum_anio = [];
    $scope.ciclos_aplicados_anio = [];
    $scope.chart_ciclos_aplicados = new echartsPlv();
    $scope.chart_ciclos_aplicados_sum = new echartsPlv();
    $scope.chart_ciclos_aplicados_avg = new echartsPlv();
    $scope.motivo_char = new echartsPlv();
    $scope.success = function(indicador){
        let r = $scope.proccess(indicador)

        $scope.encargados = r.encargados

        if(r.hasOwnProperty("years")){
            $scope.years = r.years;
        }
        if(r.hasOwnProperty("gerentes")){
            $scope.gerentes = r.gerentes;
        }
        if(r.hasOwnProperty("ciclos_aplicados")){
            $scope.ciclos_aplicados = r.ciclos_aplicados;
        }
        if(r.hasOwnProperty("ciclos_aplicados_sum_anio")){
            setTimeout(() => {
                // TAG $/HA CICLO
                let _data_order = angular.copy(r.ciclos_aplicados_anio);
                _data_order = $filter('orderObjectBy')(_data_order.filter(val => val[$scope.filters.year] > 0), $scope.filters.year);

                if(!(_data_order.length >= 2)){
                    _data_order = angular.copy(r.ciclos_aplicados_anio);
                    _data_order = $filter('orderObjectBy')(_data_order, $scope.filters.year);
                }
                
                $scope.tags[3].valor = $filter('avgOfValue')(r.ciclos_aplicados_anio, $scope.filters.year) || 0
                $scope.tags[3].promedio = $scope.tags[3].valor || 0

                let min = _data_order[0], 
                    max = _data_order[_data_order.length-1]
                $scope.tags[4].valor = max[$scope.filters.year] ? max[$scope.filters.year] : 0
                $scope.tags[4].promedio = max[$scope.filters.year] ? max[$scope.filters.year] : 0
                $scope.tags[4].tittle = max.finca+". Mayor $/Ha Ciclos"

                $scope.tags[5].valor = min[$scope.filters.year] ? min[$scope.filters.year] : 0
                $scope.tags[5].promedio = min[$scope.filters.year] ? min[$scope.filters.year] : 0
                $scope.tags[5].tittle = min.finca+". Menor $/Ha Ciclos"

                // TAG $/HA AÑO
                $scope.ciclos_aplicados_sum_anio = r.ciclos_aplicados_sum_anio;
                $scope.total_ciclos_aplicados_sum_anio = r.total_ciclos_aplicados_sum_anio;

                let _data_order2 = angular.copy(r.resumen_costos);
                _data_order2 = $filter('orderObjectBy')(_data_order2.filter(val => val[$scope.filters.year] > 0), $scope.filters.year);

                if(!(_data_order2.length >= 2)){
                    _data_order2 = angular.copy(r.resumen_costos);
                    _data_order2 = $filter('orderObjectBy')(_data_order2, $scope.filters.year);
                }
                
                $scope.tags[6].valor = $filter('avgOfValue')(r.resumen_costos, $scope.filters.year) || 0
                $scope.tags[6].promedio = $scope.tags[6].valor || 0

                let min2 = _data_order2[0] || 0, 
                    max2 = _data_order2[_data_order2.length-1] || 0

                $scope.tags[7].valor = max2[$scope.filters.year]
                $scope.tags[7].promedio = max2[$scope.filters.year]
                $scope.tags[7].tittle = max2.finca+". Mayor $/Ha año"

                $scope.tags[8].valor = min2[$scope.filters.year]
                $scope.tags[8].promedio = min2[$scope.filters.year]
                $scope.tags[8].tittle = min2.finca+". Menor $/Ha año"

                let props = {
                    colums : 3,
                    tags: $scope.tags,
                    withTheresholds: false
                }
                ReactDOM.render(React.createElement(ListTags, props), document.getElementById('indicadores'))

                // GRAFICA $/HA AÑO
                let _data_chart = angular.copy(r.ciclos_aplicados_sum_chart)
                for(let _anio in _data_chart.data){
                    for(let i in _data_chart.data[_anio].data){
                        let _finca = _data_chart.legend[i]
                        let new_val = getDolaresHaAnioFromResumenCostos(_finca, _anio)
                        _data_chart.data[_anio].data[i] = new_val ? Math.round(new_val * 100) / 100 : null
                    }
                }
                let data_chart = {
                    series: _data_chart.data,
                    legend: _data_chart.legend,
                    umbral: null,
                    id: "ciclos_aplicacion_sum"
                }
                ReactDOM.render(React.createElement(Historica, data_chart), document.getElementById('contenedor_ciclos_aplicacion_sum'));
            }, 500)
        }
        if(r.hasOwnProperty("ciclos_aplicados_anio")){
            $scope.ciclos_aplicados_anio = r.ciclos_aplicados_anio;
            $scope.total_ciclos_aplicados_anio = r.total_ciclos_aplicados_anio;
        }
        if(r.hasOwnProperty("ciclos")){
            $scope.ciclos = r.ciclos;
        }
        if(r.hasOwnProperty("data_ciclos")){
            $scope.data_ciclos = r.data_ciclos;
        }
        if(r.hasOwnProperty("resumen_costos")){
            $scope.resumen_costos = r.resumen_costos;
        }
        if(r.hasOwnProperty("ciclos_aplicados_chart")){
            let data = {
                series: r.ciclos_aplicados_chart.data,
                legend: r.ciclos_aplicados_chart.legend,
                umbral: null,
                id: "ciclos_aplicacion"
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('contenedor_ciclos_aplicacion'));
        }
        if(r.hasOwnProperty("ciclos_aplicados_sum_chart")){
            /*var data = {
                series: r.ciclos_aplicados_sum_chart.data,
                legend: r.ciclos_aplicados_sum_chart.legend,
                umbral: null,
                id: "ciclos_aplicacion_sum"
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('contenedor_ciclos_aplicacion_sum'));*/
        }
        if(r.hasOwnProperty("ciclos_aplicados_avg_chart")){
            $scope.table_frac = r.table_frac;
        }
        if(r.hasOwnProperty("ciclos_aplicados_avg_chart")){
            let data = {
                series: r.ciclos_aplicados_avg_chart.data,
                legend: r.ciclos_aplicados_avg_chart.legend || ['NO HAY INFORMACIÓN'],
                umbral: null,
                id: "ciclos_aplicacion_avg"
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('contenedor_ciclos_aplicacion_avg'));
        }
        if(r.hasOwnProperty("motivo")){
            $scope.motivo_char.init("contenedor_motivo" , r.motivo);
        }
    }

    $scope.setFilterGerente = function(data){
        if(data){
            $scope.filters.gerente = data.id
            $scope.filters.gerenteName = data.label
            $scope.init($scope.filters);
        }
    }
    $scope.setFilterHectarea = function(data){
        $scope.filters.tipoHectarea = data;
        $scope.init($scope.filters);
    }
    $scope.setFilterYears = function(data){
        if(data){
            $scope.filters.year = data.years

            $scope.last().then(() => {
                $scope.init($scope.filters)
            })
        }
    }
    $scope.setFilterVariable = function(data){
        $scope.filters.variable = data
        $scope.init($scope.filters)
    }
    $scope.setFilterEncargado = function(data){
        $scope.filters.encargadoName = angular.copy(data.label)
        $scope.filters.encargado = angular.copy(data.id)
        $scope.init($scope.filters)
    }
    $scope.setFilterTipoCiclo = function(data){
        $scope.filters.tipoCiclo = data
        $scope.init($scope.filters)
    }
    $scope.setFilterWeeks = function(data){
        if(data){
            $scope.filters.sem = data.label
            $scope.init($scope.filters);
        }
    }
    $scope.setFilterWeeks2 = function(data){
        if(data){
            $scope.filters.sem2 = data.label
            $scope.init($scope.filters);
        }
    }
    
    $scope.error = function(err){
        console.error(err)
    }

    $scope.semaforoDll = function(oldValue, newValue, position){
        position = parseInt(position)
        className = ""
        if(position > 0){
            oldValue = parseFloat(oldValue)
            newValue = parseFloat(newValue)
            if(newValue > oldValue)
                className = "fa fa-arrow-up font-red-thunderbird"
            else if(newValue < oldValue)
                className = "fa fa-arrow-down font-green-haze"
            else
                className = "fa fa-arrow-right font-yellow-gold"
        }
        
        return className;
    }

    $scope.porcentajeDiferencia = function(oldValue, newValue, position){
        position = parseInt(position)
        className = ""
        if(position > 0){
            oldValue = parseFloat(oldValue)
            newValue = parseFloat(newValue)
            
            if(oldValue == 0 || oldValue == newValue) return '0%'
            else if(newValue > oldValue){
                let p = parseFloat(100 - parseFloat(oldValue/newValue*100)).toFixed(2)
                return "+"+p+'%'
            }
            else if(oldValue > newValue){
                let p = parseFloat(100 - parseFloat(newValue/oldValue*100)).toFixed(2)
                return "-"+p+'%'
            }
        }else{
            return ''
        }
    }

    $scope.getClassName = function(oldValue , newValue , position){
        position = parseInt(position)
        className = ""
        if(position > 0){
            oldValue = parseFloat(oldValue)
            newValue = parseFloat(newValue)
            if(newValue > oldValue)
                className = "bg-red-thunderbird bg-font-red-thunderbird"
            else if(newValue < oldValue)
                className = "bg-green-haze bg-font-green-haze"
            else
                className = "bg-yellow-gold bg-font-yellow-gold"
        }
        
        return className;
    }

    $scope.last = function(){
        return new Promise((resolve, reject) => {
            var data = $scope.filters
            resumen.last(data)
                .then((r) => {
                    if(r.data){
                        $scope.semanas = r.data.semanas

                        if(r.data.semanas.length > 0){
                            let k_sem = Object.keys(r.data.semanas)
                            $scope.filters.sem = r.data.semanas[k_sem[0]].label
                            $scope.filters.sem2 = r.data.semanas[k_sem[k_sem.length-1]].label
                        }else{
                            $scope.filters.sem = 'SEM'
                            $scope.filters.sem2 = 'SEM'
                        }
                        resolve()
                    }
                })
                .catch($scope.error)
        })
    }

    $scope.init = function(){
        let data = $scope.filters
        load.block('indicadores')
        load.block()
        resumen.indicadores($scope.filters)
        .then(function(indicador){
            load.unblock('indicadores')
            let r = $scope.proccess(indicador)
            if(r.hasOwnProperty("tags")){
                $scope.tags = r.tags
            }

            resumen.init(data)
            .then($scope.success)
            .then(() => {
                load.unblock()
            })
            .catch($scope.error)
        })
    }
    
    $scope.exportExcel = function(id_table, title){
        let tableToExcel = (function() {
            let uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                let contentTable = table.innerHTML
                // remove filters
                /*var cut = contentTable.search('<tr role="row" class="filter">')
                var cut2 = contentTable.search('</thead>')

                var part1 = contentTable.substring(0, cut)
                var part2 = contentTable.substring(cut2, contentTable.length)
                contentTable = part1 + part2*/
                
                let ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(id_table, title || "")
    }

    $scope.fnExcelReport = function(id_table, title){
        let data = new Blob([document.getElementById(id_table).outerHTML], {
            type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'
        })
        saveAs(data, title+'.xls')
    }

    $scope.exportPDF = async function(){
        let doc = new jsPDF();
        load.block()

        await addImage(doc, $("#resumenCostos .portlet"))
        doc.addPage()
        await addImage(doc, $("#ciclosAplicados .portlet"), 210, 160)
        doc.addPage()
        await addImage(doc, $("#contenedor_ciclos_aplicacion").parent().parent(), 210, 140) //Grafica
        doc.addPage()
        await addImage(doc, $("#dolaresHectareaAño .portlet"), 210, 160)
        doc.addPage()
        await addImage(doc, $("#contenedor_ciclos_aplicacion_sum").parent().parent(), 210, 140) //Grafica
        doc.addPage()
        await addImage(doc, $("#dolaresHectareaCiclo .portlet"), 210, 160)
        doc.addPage()
        await addImage(doc, $("#contenedor_ciclos_aplicacion_avg").parent().parent(), 210, 140) //Grafica
        doc.addPage()
        await addImage(doc, $("#frac .portlet"), 210, 160)
        doc.addPage()
        await addImage(doc, $("#diasAtraso .portlet"), 210, 160)
        doc.addPage()
        await addImage(doc, $("#motivosAtraso .portlet"), 210, 160)

        load.unblock()
        doc.save('Resumen Ciclos.pdf')
    }

    $scope.exportPrint = function(id_table){
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;
        // remove filters
        /*var cut = contentTable.search('<tr role="row" class="filter">')
        var cut2 = contentTable.search('</thead>')
        var part1 = contentTable.substring(0, cut)
        var part2 = contentTable.substring(cut2, contentTable.length)
        // add image
        contentTable = image + part1 + part2*/

        var newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
    }

    function addImage(doc, el, width = 210, height = 180){
        return new Promise((resolve, reject) => {
            html2canvas(el, {
                onrendered: function(canvas) {
                    let dataUrl = canvas.toDataURL('image/jpeg');
                    doc.addImage(dataUrl, 'JPEG', 0, 10, width, height)
                    resolve()
                }
            });
        })
    }

    $scope.last().then(() => {
        $scope.init($scope.filters)
    })

}])