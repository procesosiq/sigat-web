app.service('resumen', function($http){
    var service = {};
    var options = {
        url: "./controllers/index.php",
        method: "POST"
    };

    service.init = function (params) {
        var option = options;
        option.data = {
            accion: "Datos_Resumen_Demo.getIndicadores",
            params : params
        };
        return $http(option);
    };

    service.indicadores = function(params){
        var option = options;
        option.data = {
            accion: "Datos_Ciclos_Demo.getIndicadores" , 
            params : params
        };
        return $http(option);
    }

    return service;
});

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
        var filtered = [];
    	angular.forEach(items, function(item) {
      		filtered.push(item);
        });
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.controller('resumenCiclos', ['$scope', '$http', '$interval', '$controller', 'resumen', function ($scope, $http, $interval, $controller, resumen) {
    $scope.filters = {
        gerente : "",
        sem : "SEM",
        sem2 : "SEM",
        gerenteName : "SIGAT DEMO",
        year : moment().format("YYYY"),
        variable : '',
        variable : 'CICLO',
        tipoHectarea : ''
    }

    $scope.tipoHectarea = {
        '' : 'FUMIGICACIÓN',
        'h_produccion' : 'PRODUCCIÓN',
        'h_neta' : 'NETA',
        'h_banano' : 'BANANO'
    }

    $scope.tables = {
        ciclos: {
            orderBy : 'finca',
            reverse : false
        },
        dolares : {
            orderBy : 'finca',
            reverse : false
        },
        dolares_ha : {
            orderBy : 'finca',
            reverse : false
        }
    }
    
    $scope.proccess = function (r) {
        if (r.hasOwnProperty("data")) {
            return r.data;
        }
    };

    $scope.semaforoSaldo = (value) => {
        if($.isNumeric(value)){
            if(value > 0){
                return 'bg-green-jungle bg-font-green-jungle'
            }else if(value == 0){
                return 'bg-yellow-gold bg-font-yellow-gold'
            }else{
                return 'bg-red-thunderbird bg-font-red-thunderbird'
            }
        }
    }

    $scope.years = [];
    $scope.gerentes = [];
    $scope.semanas = [];
    // 19/06/2017 - TAG: TABLAS
    $scope.ciclos = [];
    $scope.data_ciclos = [];
    $scope.ciclos_aplicados = [];
    $scope.ciclos_aplicados_sum_anio = [];
    $scope.ciclos_aplicados_anio = [];
    // 19/06/2017 - TAG: TABLAS
    // 19/06/2017 - TAG: GRAFICAS
    $scope.chart_ciclos_aplicados = new echartsPlv();
    $scope.chart_ciclos_aplicados_sum = new echartsPlv();
    $scope.chart_ciclos_aplicados_avg = new echartsPlv();
    $scope.motivo_char = new echartsPlv();
    // 19/06/2017 - TAG: GRAFICAS
    $scope.success = function(indicador){
        var r = $scope.proccess(indicador)
        if(r.hasOwnProperty("years")){
            $scope.years = r.years;
        }
        if(r.hasOwnProperty("gerentes")){
            $scope.gerentes = r.gerentes;
        }
        if(r.hasOwnProperty("semanas")){
            $scope.semanas = r.semanas;
        }
        // 19/06/2017 - TAG: TABLAS
        if(r.hasOwnProperty("ciclos_aplicados")){
            $scope.ciclos_aplicados = r.ciclos_aplicados;
        }
        if(r.hasOwnProperty("ciclos_aplicados_sum_anio")){
            $scope.ciclos_aplicados_sum_anio = r.ciclos_aplicados_sum_anio;
            $scope.total_ciclos_aplicados_sum_anio = r.total_ciclos_aplicados_sum_anio;
        }
        if(r.hasOwnProperty("ciclos_aplicados_anio")){
            $scope.ciclos_aplicados_anio = r.ciclos_aplicados_anio;
            $scope.total_ciclos_aplicados_anio = r.total_ciclos_aplicados_anio;
        }
        if(r.hasOwnProperty("ciclos")){
            $scope.ciclos = r.ciclos;
        }
        if(r.hasOwnProperty("data_ciclos")){
            $scope.data_ciclos = r.data_ciclos;
        }
        // 19/06/2017 - TAG: TABLAS
        // 19/06/2017 - TAG: GRAFICAS
        if(r.hasOwnProperty("ciclos_aplicados_chart")){
            var data = {
                series: r.ciclos_aplicados_chart.data,
                legend: r.ciclos_aplicados_chart.legend,
                umbral: null,
                id: "ciclos_aplicacion"
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('contenedor_ciclos_aplicacion'));
            // $scope.chart_ciclos_aplicados.init("contenedor_ciclos_aplicacion" , r.ciclos_aplicados_chart);
        }
        if(r.hasOwnProperty("ciclos_aplicados_sum_chart")){
            // $scope.chart_ciclos_aplicados_sum.init("contenedor_ciclos_aplicacion_sum" , r.ciclos_aplicados_sum_chart);
            var data = {
                series: r.ciclos_aplicados_sum_chart.data,
                legend: r.ciclos_aplicados_sum_chart.legend,
                umbral: null,
                id: "ciclos_aplicacion_sum"
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('contenedor_ciclos_aplicacion_sum'));
            // $scope.chart_ciclos_aplicados_sum.init("contenedor_ciclos_aplicacion_sum" , r.ciclos_aplicados_sum_chart);
        }
        if(r.hasOwnProperty("ciclos_aplicados_avg_chart")){
            $scope.table_frac = r.table_frac;
        }
        if(r.hasOwnProperty("ciclos_aplicados_avg_chart")){
            var data = {
                series: r.ciclos_aplicados_avg_chart.data,
                legend: r.ciclos_aplicados_avg_chart.legend,
                umbral: null,
                id: "ciclos_aplicacion_avg"
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('contenedor_ciclos_aplicacion_avg'));
            // $scope.chart_ciclos_aplicados_avg.init("contenedor_ciclos_aplicacion_avg" , r.ciclos_aplicados_avg_chart);
        }
        if(r.hasOwnProperty("motivo")){
            $scope.motivo_char.init("contenedor_motivo" , r.motivo);
        }
        // 19/06/2017 - TAG: GRAFICAS
    }


    $scope.setFilterGerente = function(data){
        if(data){
            $scope.filters.gerente = data.id
            $scope.filters.gerenteName = data.label
            $scope.init($scope.filters);
        }
    }
    $scope.setFilterHectarea = function(data){
        $scope.filters.tipoHectarea = data;
        $scope.init($scope.filters);
    }
    $scope.setFilterYears = function(data){
        if(data){
            $scope.filters.year = data.years
            $scope.init($scope.filters);
        }
    }
    $scope.setFilterVariable = function(data){
        $scope.filters.variable = data
        $scope.init($scope.filters)
    }
    $scope.setFilterWeeks = function(data){
        if(data){
            $scope.filters.sem = data.label
            $scope.init($scope.filters);
        }
    }
    $scope.setFilterWeeks2 = function(data){
        if(data){
            $scope.filters.sem2 = data.label
            $scope.init($scope.filters);
        }
    }
    
    $scope.error = function(err){
        console.error(err)
    }



    $scope.getClassName = function(oldValue , newValue , position){
        position = parseInt(position)
        className = ""
        if(position > 0){
            oldValue = parseFloat(oldValue)
            newValue = parseFloat(newValue)
            if(newValue > oldValue)
                className = "bg-red-thunderbird bg-font-red-thunderbird"
            else if(newValue < oldValue)
                className = "bg-green-haze bg-font-green-haze"
            else
                className = "bg-yellow-gold bg-font-yellow-gold"
        }
        
        return className;
    }

    $scope.init = function(){
        var data = $scope.filters
        resumen.init(data)
            .then($scope.success)
            .catch($scope.error)

        resumen.indicadores($scope.filters).then(function(indicador){
            var r = $scope.proccess(indicador)
            if(r.hasOwnProperty("tags")){
                var data = {
                    colums : 3,
                    tags: r.tags,
                    withTheresholds: false
                }
                ReactDOM.render(React.createElement(ListTags, data), document.getElementById('indicadores'))
            }
        })
    }
    
    $scope.exportExcel = function(id_table, title){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var contentTable = table.innerHTML
                // remove filters
                /*var cut = contentTable.search('<tr role="row" class="filter">')
                var cut2 = contentTable.search('</thead>')

                var part1 = contentTable.substring(0, cut)
                var part2 = contentTable.substring(cut2, contentTable.length)
                contentTable = part1 + part2*/
                
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(id_table, title || "")
    }

    $scope.exportPrint = function(id_table){
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;
        // remove filters
        /*var cut = contentTable.search('<tr role="row" class="filter">')
        var cut2 = contentTable.search('</thead>')
        var part1 = contentTable.substring(0, cut)
        var part2 = contentTable.substring(cut2, contentTable.length)
        // add image
        contentTable = image + part1 + part2*/

        var newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
    }

}])