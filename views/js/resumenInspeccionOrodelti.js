'use strict';

app.service('request', ['$http', ($http) => {

    let service = {}
    
    service.last = async (params) => {
        let r = await $http.post('controllers/index.php?accion=ResumenInspeccion.last', params || {})
        return r.data
    }

    service.semanas = async (params) => {
        let r = await $http.post('controllers/index.php?accion=ResumenInspeccion.semanas', params || {})
        return r.data
    }

    service.variables = async (params) => {
        let r = await $http.post('controllers/index.php?accion=ResumenInspeccion.variables', params || {})
        return r.data
    }

    service.index = async (params) => {
        load.block()
        let r = await $http.post('controllers/index.php?accion=ResumenInspeccion.index', params || {})
        load.unblock()
        return r.data
    }

    return service

}])

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        return sum / count;
    }
})

app.filter('countOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                count++;
            }
        });
        return count;
    }
})

app.controller('programa', ['$scope', 'request', function ($scope, $request) {

    $scope.filters = {
        anio : moment().year(),
        semana : moment().week()
    }

    $scope.data = []

    $scope.init = async () => {
        let r = await $request.index($scope.filters)
        $scope.data = r.data

        $scope.$apply()
    }

    $scope.variables = async () => {
        let r = await $request.variables($scope.filters)
        $scope.gerentes = r.gerentes
        $scope.fincas = r.fincas

        let e = false
        for(let i in r.fincas){
            let f = r.fincas[i]
            if(f.id == $scope.filters.finca) e = true;
        }
        if(!e){
            $scope.filters.finca = r.fincas[0].id
        }

        $scope.init()
    }

    $scope.semanas = async () => {
        let r = await $request.semanas($scope.filters)
        $scope.semanas = r.semanas
        $scope.variables()
    }

    $scope.last = async () => {
        let r = await $request.last()
        $scope.filters.anio = r.anio
        $scope.filters.semana = r.semana
        $scope.anios = r.anios
        $scope.semanas()
    }

    $scope.excel = () => {
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var contentTable = table.innerHTML
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel("table", "REPORTE SEMANAL DE CAMPO")
    }

    $scope.last()
    
}]);