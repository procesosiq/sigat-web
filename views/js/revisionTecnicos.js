'use strict';

app.service('request', function ($http) {
    var service = {};
    var options = {
        url: "./controllers/index.php",
        method: "POST"
    };

    service.get = function (callback, filters) {
        var option = options;
        filters.accion = "RevisionTecnico";
        option.data = filters;
        $http(option)
            .then((r) => callback(r.data))
            .catch((r) => callback(null))
    };

    service.getProductos = function(callback, filters){
        var option = options
        filters.accion = "RevisionTecnico.productos"
        option.data = filters
        $http(option)
        .then((r) => {
            callback(r.data)
        })
    }

    service.save = function(callback, filters){
        var option = options;
        filters.accion = "RevisionTecnico.save";
        option.data = filters;
        $http(option)
            .then((r) => callback(r))
            .catch((r) => callback(null))
    }

    return service;
});

app.controller('tecnicos', ['$scope', 'request', function ($scope, $request) {

    $scope.filters = { id : "<?= $_GET['id'] ?>" }
    $scope.init = ( ) => {
        $scope.getProductos().then(() => {
            $scope.get()
        })
    }
    
    $scope.get = ( ) => {
        $request.get((r) => {
            r.data.fungicidas = r.data.fungicidas.map((p) => {
                return Object.assign(p, { dosis : parseFloat(p.dosis) })
            })
            r.data.emulsificantes = r.data.emulsificantes.map((p) => {
                return Object.assign(p, { dosis : parseFloat(p.dosis) })
            })
            r.data.foliares = r.data.foliares.map((p) => {
                return Object.assign(p, { dosis : parseFloat(p.dosis) })
            })
            r.data.insecticidas = r.data.insecticidas.map((p) => {
                return Object.assign(p, { dosis : parseFloat(p.dosis) })
            })
            r.data.sigatokas = r.data.sigatokas.map((p) => {
                return Object.assign(p, { dosis : parseFloat(p.dosis) })
            })
            r.data.erwinias = r.data.erwinias.map((p) => {
                return Object.assign(p, { dosis : parseFloat(p.dosis) })
            })
            r.data.precio_operacion = Number(r.data.precio_operacion)
            
            $scope.data = r.data
            
            setTimeout(() => {
                r.data.fungicidas.map((p,i) => {
                    $("#fungicidas_dosis_"+i).val(p.dosis)
                })
                r.data.emulsificantes.map((p,i) => {
                    $("#emulsificantes_dosis_"+i).val(p.dosis)
                })
                r.data.foliares.map((p,i) => {
                    $("#foliares_dosis_"+i).val(p.dosis)
                })
                r.data.erwinias.map((p,i) => {
                    $("#erwinias_dosis_"+i).val(p.dosis)
                })
                r.data.sigatokas.map((p,i) => {
                    $("#sigatokas_dosis_"+i).val(p.dosis)
                })
                r.data.insecticidas.map((p,i) => {
                    $("#insecticidas_dosis_"+i).val(p.dosis)
                })
            }, 200)
        }, $scope.filters)
    }

    $scope.getProductos = () => {
        return new Promise((resolve) => {
            $request.getProductos((r) => {
                $scope.fincas = r.fincas
                $scope.sectores = r.sectores
                $scope.fumigadoras = r.fumigadoras
                $scope.pilotos = r.pilotos
                $scope.placas = r.placas
                $scope.motivos = r.motivos
                //--PRODUCTOS
                $scope.fungicidas = r.fungicidas
                $scope.aceites = r.aceites
                $scope.bioestimulantes = r.bioestimulantes
                $scope.emulsificantes = r.emulsificantes
                $scope.foliares = r.foliares
                $scope.insecticidas = r.insecticidas
                $scope.erwinias = r.erwinias
                $scope.sigatokas = r.sigatokas
                resolve()
            }, $scope.filters)
        })
    }

    $scope.select_sector = (num_sector) => {
        let sector_id = $scope.data.sectores[num_sector-1].id_sector
        let sector = $scope.sectores.filter((value) => value.id == sector_id)[0]
        if(sector){
            $scope.data.sectores[num_sector-1].hectareas = Number(sector.hectareas)
        }else{
            $scope.data.sectores[num_sector-1].hectareas = Number(0)
        }

        var total_hectareas = 0
        $scope.data.sectores.map((sec) => {
            total_hectareas += Number(sec.hectareas)
        })
        $scope.data.total_hectareas = total_hectareas
    }

    $scope.save = () => {
        $request.save((r) => {
            if(r){
                alert(`Guardado con éxito`, `Revisión`, `success`, () => {
                    window.location.href = 'listRevisionTecnico'
                })
            }else{
                alert(`Ocurrio algo inesperado`)
            }
        }, { data: $scope.data })
    }

    $scope.init()

}]);