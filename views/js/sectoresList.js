var TableDatatablesAjax = function () {

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }

    var handleRecords = function () {
        grid.init("#datatable_ajax" , "../controllers/index.php?accion=Sectores");
        $('#datatable_ajax tbody').on( 'click', 'button', function () {
             var data = grid.tableEdit.row( $(this).parents('tr') ).data();
             if(data.length > 0){
                document.location.href = "/newSector?id=" + data[1];
             }
        } );

        $('#datatable_ajax tbody').on( 'click', 'span', function () {
            var id = this.id;
            var data = grid.tableEdit.row( $(this).parents('tr') ).data();
            if(confirm("Esta seguro de cambiar de estado el registro?")){
                // changeStatus
                var data = {
                    accion : "Sectores.changeStatus",
                    id : data[1] , 
                    estado : id
                }
                ahttp.post("./controllers/index.php" , getInfo , data);
            }


            function getInfo(r , b){
                b();
                if(r){
                    alert("Registro cambiado" , "Sectores" , "success" , function(){
                        grid.reload();
                    });
                }
            }
        });

        $(".newAgrupacion").on("click" , function(){
            document.location.href = "/newSector";
        })
        
        
    }

    return {

        //main function to initiate the module
        init: function () {

            initPickers();
            handleRecords();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesAjax.init();
});