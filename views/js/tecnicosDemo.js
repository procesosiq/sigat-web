'use strict';

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.service('table1', function ($http) {
    var service = {};
    var options = {
        url: "./controllers/index.php",
        method: "POST"
    };

    service.listado = function (filters) {
        var option = options;
        filters.accion = "TecnicoDemo";
        option.data = filters;
        return $http(option);
    };

    return service;
});

app.controller('tecnicos', ['$scope', '$http', '$interval', 'table1', '$controller', function ($scope, $http, $interval, table1, $controller) {

    $scope.filters = {
        sem : "SEM",
        sem2 : "SEM"
    };

    $scope.init = function(){
        $scope.getTable1();
    }

    $scope.proccess = function (r) {
        if (r.hasOwnProperty("data")) {
            return r.data;
        }
    };

    $scope.setFilterWeeks = (sem) => {
        $scope.filters.sem = sem
        $scope.getTable1()
    }
    $scope.setFilterWeeks2 = (sem) => {
        $scope.filters.sem2 = sem
        $scope.getTable1()
    }

    $scope.getTable1 = function () {
        table1.listado($scope.filters).then(function (data) {
            var r = $scope.proccess(data);
            $scope.table1 = r.table1 || [];
            $scope.table2 = r.table2 || [];
            $scope.table_export = r.table_to_export || [];
            $scope.semanas = r.semanas ||  [];
            $scope.ciclos = r.ciclos || [];
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.exportExcel = function(table_id){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table);
                var contentTable = table.innerHTML;
                var ctx = {worksheet: name || 'Worksheet', table: contentTable};
                window.location.href = uri + base64(format(template, ctx));
            }
        })()
        tableToExcel(table_id, "Reporte Tecnico");
    };

    $scope.exportPdf = function(table_id) {
        var pdf = new jsPDF('landscape', 'pt', 'letter');

        pdf.cellInitialize();
        pdf.setFontSize(8);
        $.each( $('#'+table_id+' tr'), function (i, row){
            $.each( $(row).find("td, th"), function(j, cell){
                var txt = $(cell).text().trim().substring(0, 8) || " ";
                var width = 40;
                pdf.cell(10, 50, width, 30, txt, i);
            });
        });

        pdf.save('tecnico.pdf');
    }

    $scope.search = {
        orderBy : "fecha_real",
        reverse : false
    };

    $scope.search2 = {
        orderBy : "fecha_real",
        reverse : false
    };

    $scope.disableColumns = function(table, column, event){
        var cells = $("#datatable_ajax_"+table+" th:nth-child("+column+"), td:nth-child("+column+")");
        var isActive = $(event.target).parent().hasClass("active");
        if(isActive){
            $($(event.target).parent()).removeClass("active");
            $($($($("#columns_table_"+table+" label[class='active']")[column-1]).find("span")[1]).find("i")).addClass("hide");
        }else{
            $($(event.target).parent()).addClass("active");
            $($($($("#columns_table_"+table+" label[class='active']")[column-1]).find("span")[1]).find("i")).removeClass("hide");
        }

        $.each(cells, function(index, value){
            if(!isActive)
                $(value).removeClass("hide");
            else
                $(value).addClass("hide");
        });
    }

    $scope.changeSort = function(column, options){        
        if($scope[options].orderBy != column){
            var previous = $("th.selected")[0];
            $(previous).removeClass("selected");
            $(previous).removeClass("sorting_asc");
            $(previous).removeClass("sorting_desc");
        }
        $scope[options].reverse = ($scope[options].orderBy != column) ? false : !$scope[options].reverse;
        $scope[options].orderBy = column;
        var actual_select = $("#"+column+"_column");
        if(!actual_select.hasClass("selected")){
            actual_select.addClass("selected");
        }
        if($scope.search.reverse){
            actual_select.addClass("sorting_desc");
            actual_select.removeClass("sorting_asc");
        }else{
            actual_select.addClass("sorting_asc");
            actual_select.removeClass("sorting_desc");
        }
    }

    $scope.init();

}]);