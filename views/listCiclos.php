<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=$cdn?>global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Ciclos Aplicados
                                <small>Ciclos Aplicados actualmente registrados</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="#">Ciclos</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Listado</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                   <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Ciclos Aplicados <small>(<?= $session->finca_name ?>)</small></span>
                                    </div>
                                    <div class="actions">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select name="fecha" class="form-control" id="fecha">
                                                <option value="2017">2017</option>
                                                <option value="2016">2016</option>
                                                <option value="2015">2015</option>
                                                <option value="2014">2014</option>
                                                <option value="2013">2013</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <?php
                                                if(Session::getInstance()->logged == 29){
                                            ?>
                                                <select name="fincas" class="form-control" id="fincas">
                                                    <option value="">Fincas</option>
                                                </select>
                                            <?
                                                }
                                            ?>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                                            <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                                Columnas <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu main pull-left">
                                                <li id="col_fecha" data-event="fecha" class="active">
                                                    <a href="javascript:;">Fecha</a>
                                                </li>
                                                <li id="col_semana" data-event="semana" class="active">
                                                    <a href="javascript:;">Semana</a>
                                                </li>
                                                <li id="col_ciclo" data-event="ciclo" class="active">
                                                    <a href="javascript:;">Ciclo</a>
                                                </li>
                                                <li id="col_compañia" data-event="compania_de_aplicacion">
                                                    <a href="javascript:;">Compañia de Aplicación</a>
                                                </li>
                                                <li id="col_equipo" data-event="equipo_aplicacion">
                                                    <a href="javascript:;">Equipo de Aplicación</a>
                                                </li>
                                                <li id="col_costo_aplicacion" data-event="costo_app_ha">
                                                    <a href="javascript:;">Costo de Aplicación por Ha</a>
                                                </li>
                                                <li id="col_costo_total" data-event="costo_total_app">
                                                    <a href="javascript:;">Costo total de Aplicación</a>
                                                </li>
                                                <li id="col_frecuencia" data-event="frecuencia" class="active">
                                                    <a href="javascript:;">Frecuencia</a>
                                                </li>
                                                <li id="col_fungicida_1" data-event="producto1" class="active">
                                                    <a href="javascript:;">Fungicida 1</a>
                                                </li>
                                                <li id="col_dosis_1" data-event="dosis1" class="active">
                                                    <a href="javascript:;">Dosis 1</a>
                                                </li>
                                                <li id="col_fungicida_2" data-event="producto2" class="active">
                                                    <a href="javascript:;">Fungicida 2</a>
                                                </li>
                                                <li id="col_dosis_2" data-event="dosis2" class="active">
                                                    <a href="javascript:;">Dosis 2</a>
                                                </li>
                                                <li id="col_area" data-event="area_app" class="active">
                                                    <a href="javascript:;">Área</a>
                                                </li>
                                            </ul>
                                            <input id="dateFilter" type="checkbox" class="make-switch" data-on-text="&nbsp;Estimada&nbsp;" data-off-text="&nbsp;Real&nbsp;">
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="10%"> ID </th>
                                                    <th width="10%"> Fecha </th>
                                                    <th width="10%"> Sem  </th>
                                                    <th width="10%"> Ciclo  </th>
                                                    <th width="10%"> Frec  </th>
                                                    <th width="10%"> Fungicida 1 </th>
                                                    <th width="10%"> Dosis 1 </th>
                                                    <th width="10%"> Fungicida 2 </th>
                                                    <th width="10%"> Dosis 2 </th>
                                                    <th width="10%"> Área </th>
                                                    <th width="10%"> Acciones </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td></td>
                                                    <td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="search_date_from" placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                            <input type="text" class="form-control form-filter input-sm" readonly name="search_date_to" placeholder="To">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_semana"></td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_ciclo"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_frecuencia"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_producto_1"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_dosis_1"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_producto_2"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_dosis_2"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_area"> </td>
                                                    <td>
                                                        <input type="hidden" class="form-control form-filter input-sm" id="search_fecha" name="search_fecha"/>
                                                        <input type="hidden" class="form-control form-filter input-sm" id="search_tipo" name="search_tipo"/>
                                                        <input type="hidden" class="form-control form-filter input-sm" id="show_columns" name="show_columns"/>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-search"></i> </button>
                                                            <button class="btn btn-sm red btn-outline filter-cancel"><i class="fa fa-times"></i> </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Programa Estimado <small>(<?= $session->finca_name ?>)</small></span>
                                    </div>
                                    <div class="actions">
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="btn-group btn-group-devided hide" data-toggle="buttons">
                                            <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                                Columnas <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu estimado pull-left">
                                                <li id="col_fecha" data-event="fecha" class="active">
                                                    <a href="javascript:;">Fecha</a>
                                                </li>
                                                <li id="col_semana" data-event="semana" class="active">
                                                    <a href="javascript:;">Semana</a>
                                                </li>
                                                <li id="col_ciclo" data-event="ciclo" class="active">
                                                    <a href="javascript:;">Ciclo</a>
                                                </li>
                                                <li id="col_compañia" data-event="compania_de_aplicacion">
                                                    <a href="javascript:;">Compañia de Aplicación</a>
                                                </li>
                                                <li id="col_equipo" data-event="equipo_aplicacion">
                                                    <a href="javascript:;">Equipo de Aplicación</a>
                                                </li>
                                                <li id="col_costo_aplicacion" data-event="costo_app_ha">
                                                    <a href="javascript:;">Costo de Aplicación por Ha</a>
                                                </li>
                                                <li id="col_costo_total" data-event="costo_total_app">
                                                    <a href="javascript:;">Costo total de Aplicación</a>
                                                </li>
                                                <li id="col_frecuencia" data-event="frecuencia" class="active">
                                                    <a href="javascript:;">Frecuencia</a>
                                                </li>
                                                <li id="col_fungicida_1" data-event="producto1" class="active">
                                                    <a href="javascript:;">Fungicida 1</a>
                                                </li>
                                                <li id="col_dosis_1" data-event="dosis1" class="active">
                                                    <a href="javascript:;">Dosis 1</a>
                                                </li>
                                                <li id="col_fungicida_2" data-event="producto2" class="active">
                                                    <a href="javascript:;">Fungicida 2</a>
                                                </li>
                                                <li id="col_dosis_2" data-event="dosis2" class="active">
                                                    <a href="javascript:;">Dosis 2</a>
                                                </li>
                                                <li id="col_area" data-event="area_app" class="active">
                                                    <a href="javascript:;">Área</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_estimado">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="10%"> ID </th>
                                                    <th width="10%"> Fecha </th>
                                                    <th width="10%"> Sem  </th>
                                                    <th width="10%"> Ciclo  </th>
                                                    <th width="10%"> Frec  </th>
                                                    <th width="10%"> Fungicida 1 </th>
                                                    <th width="10%"> Fungicida 2 </th>
                                                </tr>
                                                <tr role="row" class="filter hide">
                                                    <td></td>
                                                    <td>
                                                        <input class="form-control form-filter input-sm" type="hidden" value="Estimado" name="search_tipo"/>
                                                        <input class="form-control form-filter input-sm" type="hidden" vaelue="2017" id="search_fecha_estimado" name="search_fecha"/>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Programa Real <small>(<?= $session->finca_name ?>)</small></span>
                                    </div>
                                    <div class="actions">
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="btn-group btn-group-devided hide" data-toggle="buttons">
                                            <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                                Columnas <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu real pull-left">
                                                <li id="col_fecha" data-event="fecha" class="active">
                                                    <a href="javascript:;">Fecha</a>
                                                </li>
                                                <li id="col_semana" data-event="semana" class="active">
                                                    <a href="javascript:;">Semana</a>
                                                </li>
                                                <li id="col_ciclo" data-event="ciclo" class="active">
                                                    <a href="javascript:;">Ciclo</a>
                                                </li>
                                                <li id="col_compañia" data-event="compania_de_aplicacion">
                                                    <a href="javascript:;">Compañia de Aplicación</a>
                                                </li>
                                                <li id="col_equipo" data-event="equipo_aplicacion">
                                                    <a href="javascript:;">Equipo de Aplicación</a>
                                                </li>
                                                <li id="col_costo_aplicacion" data-event="costo_app_ha">
                                                    <a href="javascript:;">Costo de Aplicación por Ha</a>
                                                </li>
                                                <li id="col_costo_total" data-event="costo_total_app">
                                                    <a href="javascript:;">Costo total de Aplicación</a>
                                                </li>
                                                <li id="col_frecuencia" data-event="frecuencia" class="active">
                                                    <a href="javascript:;">Frecuencia</a>
                                                </li>
                                                <li id="col_fungicida_1" data-event="producto1" class="active">
                                                    <a href="javascript:;">Fungicida 1</a>
                                                </li>
                                                <li id="col_dosis_1" data-event="dosis1" class="active">
                                                    <a href="javascript:;">Dosis 1</a>
                                                </li>
                                                <li id="col_fungicida_2" data-event="producto2" class="active">
                                                    <a href="javascript:;">Fungicida 2</a>
                                                </li>
                                                <li id="col_dosis_2" data-event="dosis2" class="active">
                                                    <a href="javascript:;">Dosis 2</a>
                                                </li>
                                                <li id="col_area" data-event="area_app" class="active">
                                                    <a href="javascript:;">Área</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_real">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="10%"> ID </th>
                                                    <th width="10%"> Fecha </th>
                                                    <th width="10%"> Sem  </th>
                                                    <th width="10%"> Ciclo  </th>
                                                    <th width="10%"> Frec  </th>
                                                    <th width="10%"> Fungicida 1 </th>
                                                    <th width="10%"> Fungicida 2 </th>
                                                </tr>
                                                <tr role="row" class="filter hide">
                                                    <td>
                                                        <input class="form-control form-filter input-sm" type="hidden" value="Real" name="search_tipo"/>
                                                        <input type="hidden" class="form-control form-filter input-sm" id="search_fecha_real" name="search_fecha"/>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->