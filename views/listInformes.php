<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    $session = Session::getInstance();
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>

<style>
.modal-dialog {
    width : 820px;
}
</style>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?=$cdn?>global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN CONTENT BODY -->
<div class="page-content" ng-app="app" ng-controller="informe" ng-cloak>
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head" ng-init="init()">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>ENVIAR INFORME
                <!-- <small>Mapas</small> -->
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="index.php">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Informe</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <!-- BEGIN DASHBOARD STATS 1-->
    <?php #include("clima_tags.php");?>
    <!-- BEGIN INTERACTIVE CHART PORTLET-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXTRAS PORTLET-->
            <div class="portlet light form-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Resumen</span>
                    </div>
                    <div class="actions">

                    </div>
                </div>
                <div class="portlet-body form">                             
                    <div class="row">
                        <form class="form-horizontal form-bordered" method="GET" action="informe_to_pdf">
                            <div class="form-body">
                                <div class="form-group last">
                                    <label class="control-label col-md-2">Informes</label>
                                    <div class="col-md-10">
                                        <div class="portlet box green-meadow">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Listados Anteriores </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body"> 
                                                <table class="table table-striped table-hover table-bordered" id="list_informe">
                                                    <thead>
                                                        <tr>
                                                            <th> # </th>
                                                            <th> ID </th>
                                                            <th> Productor </th>
                                                            <th> Hacienda </th>
                                                            <th> Semana </th>
                                                            <th> Estado </th>
                                                            <th> UID </th>
                                                            <th> Acciones </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->