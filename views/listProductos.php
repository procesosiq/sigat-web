<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Productos
                                <small>Productos actualmente registrados</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="#">Productos</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Listado</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                   <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">LISTADO DE PRODUCTOS</span>
                                    </div>
                                    <!--<div class="actions">
                                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                                            <a class="btn newProduct sbold uppercase btn-outline blue">Nuevo Ciclo</a>
                                        </div>
                                    </div>-->
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="10%"> ID </th>
                                                    <th width="10%"> Agroquímico </th>
                                                    <th width="10%"> Nombre Comercial  </th>
                                                    <th width="10%"> Ingrediente Activo  </th>
                                                    <th width="10%"> Casa Comercial  </th>
                                                    <th width="10%"> Dosis 1 </th>
                                                    <th width="10%"> Dosis 2 </th>
                                                    <th width="10%"> Dosis 3 </th>
                                                    <th width="10%"> Precio </th>
                                                    <th width="10%"> Status </th>
                                                    <th width="10%"> Acciones </th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td></td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_agroquimico"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_nombre_comercial"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_ingrediente_activo"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_casa_comercial"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_dosis"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_dosis_2"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_dosis_3"> </td>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="search_precio"> </td>
                                                    <td>
                                                        <select name="search_status" class="form-control form-filter">
                                                            <option value="">Todos</option>
                                                            <option value="1">Activos</option>
                                                            <option value="0">Inactivos</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-search"></i> </button>
                                                            <button class="btn btn-sm red btn-outline filter-cancel"><i class="fa fa-times"></i> </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->