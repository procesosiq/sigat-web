<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    $cdn = "http://cdn.procesos-iq.com/";
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>

<div ng-app="app" ng-controller="tecnicos">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>REVISION REPORTE SEMANAL 
                    <small></small>
                </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="resumenCiclos">Inicio</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Revisión</span>
            </li>
        </ul>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            
                        </div>
                        <div class="tools">
                            
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>FINCA</th>
                                        <th>SECTORES</th>
                                        <th>HA</th>
                                        <th>CICLO</th>
                                        <th>PROGRAMA</th>
                                        <th>FECHA PROG</th>
                                        <th>FECHA REAL</th>
                                        <th>REFERENCIA</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in data">
                                        <td>{{ row.finca }}</td>
                                        <td>{{ row.sectores }}</td>
                                        <td>{{ row.ha }}</td>
                                        <td>{{ row.ciclo }}</td>
                                        <td>{{ row.programa }}</td>
                                        <td>{{ row.fecha_programada }}</td>
                                        <td>{{ row.fecha_real }}</td>
                                        <td>{{ row.json }}</td>
                                        <td>
                                            <a class="btn btn-primary" href="revisionTecnicos?id={{row.id}}">Editar</a>
                                            <a class="btn btn-danger" ng-click="borrar(row)">Borrar</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>