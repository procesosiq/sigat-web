<?php
	/**
	
		ALIAS :
		- $arrayConfig [Arreglo que contiene todos los alias de las vistas con respecto a su clase] 
	
	 */
	// echo "<br>"."Usuario ".Session::getInstance()->logged;
 //    echo "<br>"."Cliente ".Session::getInstance()->client;
 //    echo "<br>"."Finca ".Session::getInstance()->finca;
	$arrayConfig = array();
	
	$arrayConfig["index"] = "indexMain";
	$arrayConfig["listProduc"] = "Productores";
	$arrayConfig["newProduc"] = "Productores";
	$arrayConfig["agrupacionList"] = "Agrupaciones";
	$arrayConfig["newAgrupacion"] = "Agrupaciones";
	$arrayConfig["fincaList"] = "Fincas";
	$arrayConfig["newFinca"] = "Fincas";
	$arrayConfig["LotesList"] = "Lotes";
	$arrayConfig["newLote"] = "Lotes";
	$arrayConfig["importarInformacion"] = "importInformacion";
	$arrayConfig["cicloList"] = "ciclos2";
	$arrayConfig["newCiclo"] = "ciclos2";


	// $arrayConfig["fotos"] = "fotos";

	// print_r(Session::getInstance());
	/*----------  VALIDACION DE PERMISOS  ----------*/
	 if((Session::getInstance()->membresia->membresia == 'Completa' 
	 	|| Session::getInstance()->membresia->membresia == 'Media') && 
		Session::getInstance()->privileges->access == 1){
			// $arrayConfig["listCiclos"] = "Ciclos";
			// $arrayConfig["newCiclos"] = "Ciclos";
	 	// return true;
	 }elseif(array_key_exists($page, $arrayConfig)){
	 	if(isset(Session::getInstance()->access_fincas->access_special) && Session::getInstance()->access_fincas->access_special > 0 && $page == "index"){

	 	}else{
	 		if(Session::getInstance()->logged == 15){
	 			header('Location: http://sigat.procesos-iq.com/clima');
	 		}else{
	 			die("Acceso no Autorizado");
	 		}
	 	}
	 }
	// print_r($arrayConfig);
	/*----------  PATHS y OBJETO GLOBAL ----------*/
	$path = "./controllers";
	$loader = (object)array();
	/*----------  PATHS y OBJETO GLOBAL ----------*/

	/*----------  CARGAMOS LA CLASE DE LA VISTA  ----------*/
	$controlName = $path."/".$arrayConfig[$page].".php";
	if (file_exists($controlName)){
		include $path.'/conexion.php';
		include ($controlName);
		/*----------  CARGAMOS LA CLASE DE LA VISTA  ----------*/

		/*----------  CREAMOS LA INSTANCIA DE LA VISTA  ----------*/
		$nameClass = $arrayConfig[$page];
		// print_r($clientes);
		// die($nameClass);
		// $loader = new clientes();
		// die($nameClass);
		$loader = new $nameClass();
		/*----------  CREAMOS LA INSTANCIA DE LA VISTA  ----------*/
	}

?>