<?php
    $response = json_decode($loader->edit());
    // print_r($response);
?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Módulo de Agrupaciones</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="listProduc">Listado de Agrupaciones</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Registro de Agrupaciones</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-pane" id="tab_1">
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>AGREGAR NUEVA AGRUPACION</div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="#" class="horizontal-form" id="formcli" method="post">
                                                    <div class="form-actions right">
                                                        <button type="button" class="btn default cancel">Cancelar</button>
                                                        <button type="button" class="btn blue btnadd" >
                                                            <i class="fa fa-check"></i>Registrar</button>
                                                    </div>
                                                    <div class="form-body">
                                                        <h3 class="form-section">INFORMACIÓN DE LA AGRUPACION</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Nombre</label>
                                                                    <input type="text" value="<?php echo $response->data->nombre?>" id="txtnom" class="form-control">
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Productor</label>
                                                                    <select class="form-control" data-placeholder="Seleccione un Productor" tabindex="1" id="s_sucursales">
                                                                    <option value="">Seleccione</option>
                                                                    option
                                                                    <?php
                                                                        if(count($response->clientes) > 0){
                                                                            $selected = "";
                                                                            foreach ($response->clientes as $key => $value) {
                                                                                $selected = "";
                                                                                if(isset($response->data->id_cliente) && $value->id == $response->data->id_cliente){
                                                                                    $selected="selected";
                                                                                }
                                                                                echo '<option value="'.$value->id.'" '.$selected.'>'.$value->nombre.'</option>';
                                                                            }
                                                                        }

                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Finca</label>
                                                                    <select class="form-control" data-placeholder="Seleccione una Finca" tabindex="1" id="s_sucursales_finca">
                                                                    <?php
                                                                        if(count($response->fincas) > 0){
                                                                            $selected = "";
                                                                            foreach ($response->fincas as $key => $value) {
                                                                                $selected = "";
                                                                                if(isset($response->data->id_hacienda) && $value->id == $response->data->id_hacienda){
                                                                                    $selected="selected";
                                                                                }
                                                                                echo '<option value="'.$value->id.'" '.$selected.'>'.$value->nombre.'</option>';
                                                                            }
                                                                        }

                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                           <!--  <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Fecha de Registro</label>
                                                                    <input disabled type="text" value="<?php echo $response->data->fecha?>" id="txtfec" class="form-control" placeholder="dd/mm/yyyy"> </div>
                                                            </div> -->
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
<!--                                                         <h3 class="form-section">INFORMACIÓN DE FACTURACIÓN</h3>
                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="form-group">
                                                                    <label>Razón Social</label>
                                                                    <input type="text" class="form-control" id="txtrazon"> </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>RUC</label>
                                                                    <input type="text" class="form-control" id="txtruc"> </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Dirección</label>
                                                                    <input type="text" class="form-control" id="txtdircli"> </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Teléfono</label>
                                                                    <input type="text" class="form-control" id="txttel"> </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Ciudad</label>
                                                                    <input type="text" class="form-control" id="txtciudad"> </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                    <div class="form-actions right">
                                                        <button type="button" class="btn default cancel">Cancelar</button>
                                                        <button type="button" class="btn blue btnadd" >
                                                            <i class="fa fa-check"></i>Registrar</button>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->