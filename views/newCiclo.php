<?php
    $response = json_decode($loader->edit());
    //print_r($response);
?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=$cdn?>global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="<?=$cdn?>global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=$cdn?>global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=$cdn?>pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Módulo de Ciclos</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="/listCiclos">Listado de Ciclos</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Registro de ciclos</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
					<div class="row">
                        <div class="col-md-12">
                            <div class="tab-pane" id="tab_1">
                            	<div class="portlet box blue-dark">
                            		<div class="portlet-title">
                            			<div id="caption_nuevo" class="caption">
                                        	CREAR NUEVO CICLO
                                        </div>
                            		</div>
                            		<div class="portlet-body form">
                            			<!-- BEGIN FORM-->
                            			<div class="form-actions right">
                                            <button type="button" id="btnaddciclo" class="btn blue btnadd" >Registrar</button>
                                            <button type="button" class="btn default cancel">Cancelar</button>
                                        </div>
                                        <div class="form-body">
                                        	<div class="row">
                                        		<div class="col-md-12">
	                                                <div class="col-md-3" style="margin-top: 0.6%;">
	                                                	<span class="pull-right">Fecha</span>
	                                                </div>
	                                                <div class="col-md-4">
	                                        		<div class="form-group">
	                                                    <input type="text" placeholder="dd/mm/yyyy" value="<?= $response->data->fecha?>" id="txtFecha" name="txtFecha" class="table-group-action-input form-control date-picker ng-pristine ng-valid ng-touched" "="" data-date-format="yyyy-mm-dd" readonly="">
	                                                </div>
	                                                </div>
	                                            	<div class="col-md-5">
	                                            		&nbsp;
	                                            	</div>
                                            	</div>
                                        	</div>
                                        	<div class="row">
                                        		<div class="col-md-12">
                                        			<div class="col-md-3" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Ciclo</span>
                                        			</div>
                                        			<div class="col-md-4">
                                        			<div class="form-group">
                                                        <input type="text" value="<?php echo $response->data->ciclo?>" id="txtciclo" name="txtciclo" class="form-control">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                    	&nbsp;
                                                    </div>
                                        		</div>
                                        	</div>
                                        	<div class="row">
                                        		<div class="col-md-12">
                                        			<div class="col-md-3" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Compañía de Aplicación</span>
                                        			</div>
                                        			<div class="col-md-4">
                                        			<div class="form-group">
                                                        <input type="text" value="<?php echo $response->data->compania_de_aplicacion?>" id="txtcompañia" name="txtcompañia" class="form-control">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                    	&nbsp;
                                                    </div>
                                        		</div>
                                        	</div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-3" style="margin-top: 0.6%;">
                                                        <span class="pull-right">Equipo de Aplicación</span>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="equipo_aplicacion" name="equipo_aplicacion" class="form-control">
                                                            <option value="Avión">Avión</option>
                                                            <option value="Helicóptero">Helicóptero</option>
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                            </div>
                                        	<div class="row">
                                        		<div class="col-md-12">
                                        			<div class="col-md-3" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Área de Aplicación</span>
                                        			</div>
                                        			<div class="col-md-4">
                                        			<div class="form-group">
                                                        <input type="number" value="<?= $response->data->area_app?>" id="txtarea" name="txtarea" class="form-control">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-5" style="margin-top: 0.6%;">
                                                    	Ha
                                                    </div>
                                        		</div>
                                        	</div>
                                        	<div class="row">
                                        		<div class="col-md-12">
                                        			<div class="col-md-3 right" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Costo de Aplicación por Ha</span>
                                        			</div>
                                        			<div class="col-md-4">
                                        			<div class="form-group">
                                                        <input type="text" value="<?= $response->data->costo_app_ha?>" id="txtcosto" name="txtcosto" class="form-control">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-5" style="margin-top: 0.6%;">
                                                    	dólares/Ha
                                                    </div>
                                        		</div>
                                        	</div>
                                        	<div class="row">
                                        		<div class="col-md-12">
                                        			<div class="col-md-3 right" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Costo total de Aplicación</span>
                                        			</div>
                                        			<div class="col-md-4">
                                        			<div class="form-group">
                                                        <input type="text" value="<?= $response->data->costo_total_app?>"  disabled id="txtotal" name="txtotal" class="form-control" placeholder="Fórmula">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-5" style="margin-top: 0.6%;">
                                                    	dólares/ciclo
                                                    </div>
                                        		</div>
                                        	</div>
                                        </div>
                            		</div>
                            	</div>
                            </div>
                        </div>
                    </div>
                    <!--END FIRST FORM-->
                    <!--BEGIN SECOND FORM-->
					<?php
                    //if(isset($_GET['id']) && $_GET['id'] > 0):
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="tab-pane" id="tab_2">
                            	<div class="portlet box red-mint">
                            		<div class="portlet-title">
                            			<div class="caption">
                                        	FUNGICIDA 1
                                        </div>
                            		</div>
                            		<div class="portlet-body form">
                            			<div class="form-actions left">
                                            <button type="button" id="btnproduct1" class="btn blue btnadd" >Registrar</button>
                                            <button type="button" class="btn default cancel">Cancelar</button>
                                            <input id="filterFungicida1" type="checkbox" class="make-switch right" data-on-text="&nbsp;Desbloqueado&nbsp;" data-off-text="&nbsp;Filtrado&nbsp;">
                                        </div>
                                        <div class="form-body">
                                        	<div class="row">
                                        		<div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Nombre común/comercial Fungicida 1</label>
                                                        <!--<input type="text" value="<?php echo $response->producto1->nombre_comercial?>" id="nomProd1" name="nomProd1" class="form-control">-->
                                                        <input type="hidden" id="id_producto_1" value="<?= $response->data->id_producto_1 ?>"/>
                                                        <select id="nomProd1" name="nomProd1" class="form-control">
                                                            <option value=""><< Seleccionar >></option>
                                                            <?php foreach($response->productos as $producto):
                                                                if(
                                                                    ($producto->nombre_comercial != null || $producto->ingrediente_activo != null)
                                                                    && ($producto->ingrediente_activo == $response->data->ingrediente1)
                                                                ):?>
                                                                <option value="<?= $producto->id ?>" 
                                                                    <?= ($producto->id == $response->data->id_producto_1)
                                                                        ? "selected"
                                                                        : "" 
                                                                    ?>
                                                                >
                                                                <?= (($producto->nombre_comercial != null)
                                                                        ? $producto->nombre_comercial
                                                                        : $producto->ingrediente_activo) 
                                                                ?>
                                                                </option>
                                                            <?php endif; endforeach;?>
                                                        </select>
                                                    </div>
                                                </div>
                                        	</div>
                                        	<div class="row">
                                        		<div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Nombre molécula/ingrediente activo Fungicida 1</label>
                                                        <input type="text" value="" id="molProd1" name="molProd1" class="form-control" readonly>
                                                    </div>
                                                </div>
                                        	</div>
                                        	<div class="row">
                                        		<div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Casa Comercial</label>
                                                        <input type="text" value="" id="provProd1" name="provProd1" class="form-control" readonly>
                                                    </div>
                                                </div>
                                        	</div>
                                            <div class="row">
                                            	<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Dosis (L o Kg/Ha)</label>
                                                        <input type="hidden" id="dosis_num_1" value="<?= $response->data->dosis_num_1 ?>" />
                                                        <select id="dosProd1" name="dosProd1" class="form-control">
                                                        </select>
                                                    </div>
                                            		<!--<div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label">Dosis (L o Kg/Ha)</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="hidden" id="dosis_num_1" value="<?= $response->data->dosis_num_1 ?>" />
                                                        <select id="dosProd1" name="dosProd1" class="form-control">
                                                        </select>
                                                    </div>-->
                                            	</div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Costo (L o Kg)</label>
                                                        <input type="text" value="<?= $response->producto1->costo?>" id="costoProd1" name="costoProd1" class="form-control">
                                                    </div>
                                                    <!--<div class="col-md-8">
                                                        <div class="form-group">
                                                            <label class="control-label">Costo (L o Kg)</label>
                                                            <input type="text" value="<?= $response->producto1->costo?>" id="costoProd1" name="costoProd1" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" style="margin-top: 6%;">
                                                        <div class="row">dólares/L o Kg</div>
                                                    </div>-->
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Total L o Kg</label>
                                                        <input type="text" value="" placeholder="Fórmula" disabled id="totalL1" name="totalL1" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Costo total Fungicida 1</label>
                                                        <input type="text" value="" placeholder="Fórmula" disabled id="totalProd1" name="totalProd1" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Costo total Ha</label>
                                                        <input type="text" value="" placeholder="Fórmula" disabled id="totalHa1" name="totalHa1" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                            	</div>
                            </div>
                        </div>
                    <!--END SECOND FORM-->
                    <!--BEGIN THIRD FORM-->
                        <div class="col-md-6">
                            <div class="tab-pane" id="tab_3">
                            	<div class="portlet box purple-wisteria">
                            		<div class="portlet-title">
                            			<div class="caption">
                                        	FUNGICIDA 2
                                        </div>
                            		</div>
                            		<div class="portlet-body form">
                            			<div class="form-actions">
                                            <button type="button" id="btnproduct2" class="btn blue btnadd" >Registrar</button>
                                            <button type="button" class="btn default cancel">Cancelar</button>
                                            <input id="filterFungicida2" type="checkbox" class="make-switch right" data-on-text="&nbsp;Desbloqueado&nbsp;" data-off-text="&nbsp;Filtrado&nbsp;">
                                        </div>
                                        <div class="form-body">
                                        	<div class="row">
                                        		<div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Nombre común/comercial Fungicida 2</label>
                                                        <input type="hidden" id="id_producto_2" value="<?= $response->data->id_producto_2 ?>"/>
                                                        <select id="nomProd2" name="nomProd2" class="form-control">
                                                            <option value=""><< Seleccionar >></option>
                                                            <?php foreach($response->productos as $producto):
                                                                if(
                                                                    ($producto->nombre_comercial != null || $producto->ingrediente_activo != null)
                                                                    && ($producto->ingrediente_activo == $response->data->ingrediente2)
                                                                ):?>
                                                                <option value="<?= $producto->id ?>" 
                                                                    <?= ($producto->id == $response->data->id_producto_2)
                                                                        ? "selected"
                                                                        : "" 
                                                                    ?>
                                                                >
                                                                    <?= (($producto->nombre_comercial != null)
                                                                            ? $producto->nombre_comercial
                                                                            : $producto->ingrediente_activo) ?>
                                                                </option>
                                                            <?php endif; endforeach;?>
                                                        </select>
                                                    </div>
                                                </div>
                                        	</div>
                                        	<div class="row">
                                        		<div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Nombre molécula/ingrediente activo Fungicida 2</label>
                                                        <input type="text" value="" id="molProd2" name="molProd2" class="form-control" readonly>
                                                    </div>
                                                </div>
                                        	</div>
                                        	<div class="row">
                                        		<div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Casa Comercial</label>
                                                        <input type="text" value="" id="provProd2" name="provProd2" class="form-control" readonly>
                                                    </div>
                                                </div>
                                        	</div>
                                        	<div class="row">
                                        		<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Dosis</label>
                                                        <input type="hidden" id="dosis_num_2" value="<?= $response->data->dosis_num_2 ?>" />
                                                        <!--<input type="text" value="" id="dosProd2" name="dosProd2" class="form-control">-->
                                                        <select id="dosProd2" name="dosProd2" class="form-control">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Costo (L o Kg)</label>
                                                        <input type="text" value="<?= $response->producto2->costo?>" id="costoProd2" name="costoProd2" class="form-control">
                                                    </div>
                                                </div>
                                        	</div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Total L o Kg</label>
                                                        <input type="text" value="" placeholder="Fórmula" disabled id="totalL2" name="totalL2" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Costo total Fungicida 2</label>
                                                        <input type="text" value="" placeholder="Fórmula" disabled id="totalProd2" name="totalProd2" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 col-md-offset-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Costo total Ha</label>
                                                        <input type="text" value="" placeholder="Fórmula" disabled id="totalHa2" name="totalHa2" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                            	</div>
                            </div>
                        </div>
					</div>
                    <!--END THIRD FORM-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-pane" id="tab_4">
                            	<div class="portlet box blue-dark">
                            		<div class="portlet-title">
                            			<div class="caption">
                                        	DATOS GENERALES
                                        </div>
                            		</div>
                            		<div class="portlet-body form">
                            			<div class="form-actions right">
                                            <button type="button" id="btnaddgen" class="btn blue btnadd" >Registrar</button>
                                            <button type="button" class="btn default cancel">Cancelar</button>
                                        </div>
                                    <div class="form-body">
                                    <div class="row">
                                    <div class="row col-md-12">EMULSIFICANTE</div>
                                        <div class="row">
                                        	<div class="col-md-12">
                                        		<div class="col-md-3 right" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Nombre de emulsificante</span>
                                        		</div>
                                        		<div class="col-md-4">
                                        		<div class="form-group">
                                                    <input type="hidden" id="id_emulsificante" value="<?= $response->data->id_emulsificante ?>"/>
                                                    <!--<input type="text" value="<?= $response->datosg->nombre_emu?>" id="nomEmul" name="nomEmul" class="form-control">-->
                                                    <select id="nomEmul" name="nomEmul" class="form-control">
                                                        <option value=""><< Seleccionar >></option>
                                                        <?php foreach($response->productos as $producto):
                                                            if($producto->nombre_comercial != null || $producto->ingrediente_activo != null):?>
                                                            <option value="<?= $producto->id ?>" <?= ($producto->id == $response->data->id_emulsificante)?"selected":"" ?>><?= (($producto->nombre_comercial != null)?$producto->nombre_comercial:$producto->ingrediente_activo) ?></option>
                                                        <?php endif; endforeach;?>
                                                    </select>
                                                </div>
                                                </div>
                                                <div class="col-md-5">
                                                    	&nbsp;
                                                </div>
                                        	</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-3 right" style="margin-top: 0.6%;">
                                                        <span class="pull-right">Dosis</span>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="hidden" id="dosis_emulsificante" value="<?= $response->data->dosis_num_emulsificante ?>" />
                                                        <!--<input type="text" value="" id="dosProd1" name="dosProd1" class="form-control">-->
                                                        <select id="select_dosis_emulsificante" name="select_dosis_emulsificante" class="form-control">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-top: 0.6%;">
                                                        L o Kg/Ha
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-3 right" style="margin-top: 0.6%;">
                                                        <span class="pull-right">Total L o Kg</span>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" value="" placeholder="Fórmula" disabled id="totalLEmul" name="totalLEmul" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-top: 0.6%;">
                                                        &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                        	<div class="col-md-12">
                                        		<div class="col-md-3 right" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Costo litro de emulsificante</span>
                                        		</div>
                                        		<div class="col-md-4">
                                        		<div class="form-group">
                                                    <input type="text" value="<?= $response->datosg->costo_litro_emu?>" id="litroEmul" name="litroEmul" class="form-control">
                                                </div>
                                                </div>
                                                <div class="col-md-5" style="margin-top: 0.6%;">
                                                    	dólares/litro
                                                </div>
                                        	</div>
                                        </div>
                                        <div class="row">
                                        	<div class="col-md-12">
                                        		<div class="col-md-3 right" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Costo total emulsificante</span>
                                        		</div>
                                        		<div class="col-md-4">
                                        		<div class="form-group">
                                                    <input type="text" placeholder="Fórmula" value="<?php echo $response->datosg->costo_total_emu?>" disabled id="totalEmul" name="totalEmul" class="form-control">
                                                </div>
                                                </div>
                                                <div class="col-md-5" style="margin-top: 0.6%;">
                                                    	dólares emulsificante total
                                                </div>
                                        	</div>
                                        </div>
                                        <div class="row">
                                        	<div class="col-md-12">
                                        		<div class="col-md-3 right" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Casa comercial</span>
                                        		</div>
                                        		<div class="col-md-4">
                                        		<div class="form-group">
                                                    <input type="text" value="" id="provEmul" name="provEmul" class="form-control" readonly="">
                                                </div>
                                                </div>
                                                <div class="col-md-5">
                                                    	
                                                </div>
                                        	</div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                    	<div class="row col-md-12">ACEITE</div>
                                    	<div class="row">
                                        	<div class="col-md-12">
                                        		<div class="col-md-3 right" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Nombre de aceite</span>
                                        		</div>
                                        		<div class="col-md-4">
                                        		<div class="form-group">
                                                    <!--<input type="text" value="<?php echo $response->datosg->nombre_ace?>" id="nomAceite" name="nomAceite" class="form-control">-->
                                                    <input type="hidden" id="dosis_aceite" value="<?= $response->data->dosis_num_aceite ?>"/>
                                                    <select id="select_dosis_aceite" name="select_dosis_aceite" class="form-control">
                                                        <option value=""><< Seleccionar >></option>
                                                        <?php foreach($response->productos as $producto):
                                                            if($producto->nombre_comercial != null || $producto->ingrediente_activo != null):?>
                                                            <option value="<?= $producto->id ?>" 
                                                                <?= ($producto->id == $response->data->id_aceite)
                                                                    ? "selected"
                                                                    : "" 
                                                                ?>
                                                            >
                                                            <?= 
                                                                (($producto->nombre_comercial != null)
                                                                    ? $producto->nombre_comercial
                                                                    : $producto->ingrediente_activo)
                                                            ?>
                                                            </option>
                                                        <?php endif; endforeach;?>
                                                    </select>
                                                </div>
                                                </div>
                                                <div class="col-md-5">
                                                    	&nbsp;
                                                </div>
                                        	</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-3 right" style="margin-top: 0.6%;">
                                                        <span class="pull-right">Dosis</span>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="hidden" id="dosis_aceite" value="" />
                                                        <!--<input type="text" value="" id="dosProd1" name="dosProd1" class="form-control">-->
                                                        <select id="select_dosis_aceite" name="select_dosis_aceite" class="form-control">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-top: 0.6%;">
                                                        L/Ha
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-3 right" style="margin-top: 0.6%;">
                                                        <span class="pull-right">Total L</span>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" value="" placeholder="Fórmula" disabled id="totalLAceite" name="totalLAceite" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-top: 0.6%;">
                                                        &nbsp;
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                        	<div class="col-md-12">
                                        		<div class="col-md-3 right" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Costo litro de aceite</span>
                                        		</div>
                                        		<div class="col-md-4">
                                            		<div class="form-group">
                                                        <input type="text" value="<?php echo $response->datosg->costo_litro_ace?>" id="litroAceite" name="litroAceite" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="margin-top: 0.6%;">
                                                    	dólares/litro
                                                </div>
                                        	</div>
                                        </div>
                                        <div class="row">
                                        	<div class="col-md-12">
                                        		<div class="col-md-3 right" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Costo total aceite</span>
                                        		</div>
                                        		<div class="col-md-4">
                                        		<div class="form-group">
                                                    <input type="text" placeholder="Fórmula" value="<?php echo $response->datosg->costo_total_ace?>" disabled id="totalAceite" name="totalAceite" class="form-control">
                                                </div>
                                                </div>
                                                <div class="col-md-5" style="margin-top: 0.6%;">
                                                    	dólares aceite total
                                                </div>
                                        	</div>
                                        </div>
                                        <div class="row">
                                        	<div class="col-md-12">
                                        		<div class="col-md-3 right" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Casa Comercial</span>
                                        		</div>
                                        		<div class="col-md-4">
                                        		<div class="form-group">
                                                    <input type="text" value="" id="provAceite" name="provAceite" class="form-control" readonly="">
                                                </div>
                                                </div>
                                                <div class="col-md-5">
                                                    	
                                                </div>
                                        	</div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                    	<div class="row col-md-12">GRAN TOTAL</div>
                                    	<div class="row">
                                        	<div class="col-md-12">
                                        		<div class="col-md-3 right" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Total costo ciclo</span>
                                        		</div>
                                        		<div class="col-md-4">
                                        		<div class="form-group">
                                                    <input type="text" placeholder="Fórmula" value="<?php echo $response->datosg->total_costo?>" disabled id="totalCiclo" name="totalCiclo" class="form-control">
                                                </div>
                                                </div>
                                                <div class="col-md-5">
                                                    	
                                                </div>
                                        	</div>
                                        </div>
                                        <div class="row">
                                        	<div class="col-md-12">
                                        		<div class="col-md-3 right" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Total costo ciclo por Ha</span>
                                        		</div>
                                        		<div class="col-md-4">
                                        		<div class="form-group">
                                                    <input type="text" placeholder="Fórmula" value="<?php echo $response->datosg->total_costo_ha?>" disabled id="totalHa" name="totalHa" class="form-control">
                                                </div>
                                                </div>
                                                <div class="col-md-5">
                                                    	
                                                </div>
                                        	</div>
                                        </div>
                                    </div>
                                    </div>    	
                                    </div>
                            	</div>
                            </div>
                        </div>
                    </div> 
<?php //endif;?>					
                    <!--END BASE CONTENT-->
            </div>