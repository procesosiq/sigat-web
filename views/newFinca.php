<?php
    $cdn = "http://cdn.procesos-iq.com/";
    $response = json_decode($loader->edit());
?>
<style>
    .center-th {
        text-align : center;
    }
    #rowsFilas > tr > td {
        text-align : center;   
    }
    tfoot > tr > td {
        text-align : center;   
    }
</style>
<link href="<?=$cdn?>/global/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>Módulo de Fincas</h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="fincaList">Listado de Fincas</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Registro de Fincas</span>
            </li>
        </ul>
        <input type="hidden" id="json_gerentes" value='<?= json_encode($response->gerentes) ?>'/>

        <div class="row">
            <div class="col-md-12">
                <div class="tab-pane" id="tab_1">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>AGREGAR NUEVO FINCA
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="#" class="horizontal-form" id="formcli" method="post">
                                <div class="form-actions right">
                                    <button type="button" class="btn default cancel">Cancelar</button>
                                    <button type="button" class="btn blue btnadd" >
                                        <i class="fa fa-check"></i><span class="lbladd">Registrar<span>
                                    </button>
                                </div>
                                <div class="form-body">
                                    <h3 class="form-section row">
                                        <div class="col-md-4">
                                            INFORMACIÓN DE LA FINCA
                                        </div>
                                        <div class="col-md-4 col-md-offset-4">
                                            Fecha de Registro
                                            <input disabled type="text" value="<?php echo $response->data->fecha?>" id="txtfec" class="form-control save" placeholder="dd/mm/yyyy">
                                        </div>
                                    </h3>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Nombre</label>
                                                <input type="text" value="<?php echo $response->data->nombre?>" id="txtnom" class="form-control save">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Gerentes</label>
                                                <select id="gerentes" name="gerentes" class="form-control" style="height: 34px;">
                                                    <option></option>
                                                    <?php foreach($response->gerentes as $gerente) : ?>
                                                        <option value="<?= $gerente->id ?>" <?=$gerente->selected?> >
                                                                <?=$gerente->nombre?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row hide">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Hectárea de Producción</label>
                                                <input type="text" value="<?php echo $response->data->h_produccion?>" id="h_produccion" class="form-control save">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Hectárea Neta</label>
                                                <input type="text" value="<?php echo $response->data->h_neta?>" id="h_neta" class="form-control save">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row hide">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Hectárea de Banano</label>
                                                <input type="text" value="<?php echo $response->data->h_banano?>" id="h_banano" class="form-control save">
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        if(isset($_GET['id']) && $_GET['id'] > 0):
                                    ?>
                                    <h3 class="form-section">ASIGNAR SECTORES</h3>
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <table id="shorter" class="table table-striped table-bordered table-hover table-checkable">
                                                <thead>
                                                    <tr style="cursor: pointer;">
                                                        <th class="alphanumeric center-th" width="10%">Sector</th>
                                                        <th class="alphanumeric center-th" width="20%">Hectárea de<br> Fumigación</th>
                                                        <th width="30%">Acciones</th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <select class="form-control" data-placeholder="Seleccione un Sector" tabindex="1" id="s_sector">
                                                            <option value="A">A</option>
                                                            <option value="B">B</option>
                                                            <option value="C">C</option>
                                                            <option value="D">D</option>
                                                            <option value="E">E</option>
                                                            <option value="F">F</option>
                                                        </select>
                                                        </td>
                                                        <td><input type="text" value="" id="fumigacion" class="form-control"></td>
                                                        <td><button class="btn btn-success addRow" id="btnaddarea" type="button">
                                                                <i class="fa fa-plus"></i> Agregar</button></td>
                                                    </tr>
                                                </thead>
                                                <tbody id="rowsFilas">
                                                    <?php
                                                        if(count($response->sectores) > 0){
                                                            $hec_fumigacion = 0;
                                                            foreach ($response->sectores as $key => $value) {
                                                                $hec_fumigacion += $value->hec_fumigacion;
                                                                echo '<tr>';
                                                                echo '<td>'.$value->sector.'</td>';
                                                                echo '<td class="hec_fumigacion">'.$value->hec_fumigacion.'</td>';
                                                                echo '<td><button type="button" class="btn btn-sm red-thunderbird removeRow" id="'.$value->id.'">Eliminar</button>
                                                                </td>';
                                                                echo '</tr>';
                                                            }
                                                        }
                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td>Total : </td>
                                                        <td class="hec_fumigacion_total"><?=$hec_fumigacion?></td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <?php endif;?>
                                    
                                    <?php
                                        if(isset($_GET['id']) && $_GET['id'] > 0 && false):
                                    ?>
                                    <h3 class="form-section">ASIGNAR LOTES</h3>
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <table id="lotes" class="table table-striped table-bordered table-hover table-checkable">
                                                <thead>
                                                    <tr style="cursor: pointer;">
                                                        <th class="alphanumeric center-th" width="10%">Lote</th>
                                                        <th class="alphanumeric center-th" width="20%">Hectárea de<br> Fumigación</th>
                                                        <th width="30%">Acciones</th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="text" class="form-control" id="lote"/>
                                                        </td>
                                                        <td>
                                                            <input type="number" id="lote-fumigacion" class="form-control">
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-success" id="add-lote" type="button">
                                                                <i class="fa fa-plus"></i> Agregar
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </thead>
                                                <tbody id="body-lotes">
                                                    <?php
                                                        if(count($response->lotes) > 0){
                                                            $hec_fumigacion = 0;
                                                            foreach ($response->lotes as $key => $value) {
                                                                $hec_fumigacion += $value->hec_fumigacion;
                                                                echo '<tr>';
                                                                echo '<td>'.$value->lote.'</td>';
                                                                echo '<td class="hec_fumigacion">'.$value->hec_fumigacion.'</td>';
                                                                echo '<td><button type="button" class="btn btn-sm red-thunderbird removeLote" id="'.$value->id.'">Eliminar</button></td>';
                                                                echo '</tr>';
                                                            }
                                                        }
                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td>Total : </td>
                                                        <td class="hec_fumigacion_total"></td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <?php endif;?>
                                </div>
                                <div class="form-actions right">
                                    <button type="button" class="btn default cancel">Cancelar</button>
                                    <button type="button" class="btn blue btnadd" >
                                        <i class="fa fa-check"></i>Registrar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>