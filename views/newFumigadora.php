<?php
    $response = json_decode($loader->edit());
    // print_r($response);
?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Módulo de Fumigadoras</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="fumigadorasList">Listado de Fumigadoras</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Registro de Fumigadora</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-pane" id="tab_1">
                                <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>AGREGAR NUEVA FUMIGADORA</div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form action="#" class="horizontal-form" id="formcli" method="post">
                                            <div class="form-actions right">
                                                <button type="button" class="btn default cancel">Cancelar</button>
                                                <button type="button" class="btn blue btnadd" >
                                                    <i class="fa fa-check"></i>Registrar</button>
                                            </div>
                                            <div class="form-body">
                                                <h3 class="form-section">INFORMACIÓN DE LA FUMIGADORA</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Nombre</label>
                                                            <input type="text" value="<?php echo $response->data->nombre?>" id="txtnom" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                    if(isset($_GET['id']) && (int)$_GET['id'] > 0):
                                                ?>
                                                <div class="row">
                                                    <?php if($_GET['id'] == 1): ?>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Finca</label>
                                                            <select class="form-control" id="finca" name="finca">
                                                            <?php
                                                                if(count($response->fincas) > 0){
                                                                    $optgroup = "";
                                                                    $count = 0;
                                                                    foreach ($response->fincas as $key => $value) {
                                                                        echo '<option value="'.$key.'">'.$value.'</option>';
                                                                    }
                                                                    echo '</optgroup>';
                                                                }
                                                            ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                    <?php if($_GET['id'] != 1): ?>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Galones</label>
                                                            <select class="form-control" id="galones" name="galoes">
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Precio</label>
                                                            <input type="number" value="" id="precio" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Acciones</label>
                                                            <button type="button" class="btn form-control blue addPrice">Agregar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                         <div class="table-scrollable">
                                                            <table class="table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <td><?= $_GET['id'] == 1 ? 'Finca' : 'Galones' ?></td>
                                                                        <td>Precio</td>
                                                                        <td>Acciones</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="container-html">
                                                                </tbody> 
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            </div>
                                            <div class="form-actions right">
                                                <button type="button" class="btn default cancel">Cancelar</button>
                                                <button type="button" class="btn blue btnadd" >
                                                    <i class="fa fa-check"></i>Registrar</button>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->