$.fn.sortElements = (function(){
    
    var sort = [].sort;
    
    return function(comparator, getSortable) {
        
        getSortable = getSortable || function(){return this;};
        
        var placements = this.map(function(){
            
            var sortElement = getSortable.call(this),
                parentNode = sortElement.parentNode,
                
                // Since the element itself will change position, we have
                // to have some way of storing it's original position in
                // the DOM. The easiest way is to have a 'flag' node:
                nextSibling = parentNode.insertBefore(
                    document.createTextNode(''),
                    sortElement.nextSibling
                );
            
            return function() {
                
                if (parentNode === this) {
                    throw new Error(
                        "You can't sort elements if any one is a descendant of another."
                    );
                }
                
                // Insert before flag:
                parentNode.insertBefore(this, nextSibling);
                // Remove flag:
                parentNode.removeChild(nextSibling);
                
            };
            
        });
       
        return sort.call(this, comparator).each(function(i){
            placements[i].call(getSortable.call(this));
        });
        
    };
    
})();

$(function(){
    $(".btnadd").click(function(){
        
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre de Hacienda");
            return false;
        }
        else if($("#s_sucursales").val()==''){
            alert("Favor de ingresar un email del Hacienda");
            return false;
        }
        else{ 
            var data = {
                id : "<? echo $_GET['id']?>",
                url : "controllers/index.php",
                option : "accion=Fincas.create",
                params : "&txtnom="+$("#txtnom").val()+"&id_cliente="+$("#s_sucursales").val()+"&id=<? echo $_GET['id']?>",
            }

            if(parseInt(data.id) > 0){
                data.option = "accion=Fincas.update";
            }

            $.ajax({
				type: "POST",
				url: "controllers/index.php",
				data: data.option + data.params,
				success: function(msg){
					alert("Registro registrado/modificado con el ID "+ msg , "Fincas" , "success" , function(){
                        if(!parseInt(data.id) > 0){
                            $('input[type=text]').each(function() {
                                $(this).val('');
                            });
                            document.location.href = "fincaList";
                        }
                    });
				}
			});
        }
        return false;
	});

    $(".cancel").on("click" , function(){
        document.location.href = "fincaList";
    });

    function addRow(){
        var data = {
            id : "<? echo $_GET['id']?>",
            id_cliente : $("#s_sucursales").val(),
            id_agrupacion : $("#s_agrupacion").val(),
            lote : $("#lote").val(),
            area : $("#area").val()
        }

        if(data.lote != ""){
            console.log(data);
            ahttp.post("./controllers/index.php?accion=Fincas.addLote",printData , data);
        }
    }

    $(".addRow").on("click" , function(){
        addRow()
    });

    $("#rowsFilas").on("click" ,".removeRow", function(){
        var id = this.id;
        removeRow(id)
    });

    function removeRow(id){
        console.log(id);
        if(id && id > 0){
            var data = {
                id : "<? echo $_GET['id']?>",
                id_cliente : $("#s_sucursales").val(),
                id_lote : id
            }
          ahttp.post("./controllers/index.php?accion=Fincas.removeLote",printData , data);  
        }
    }

    function printData(r , b){
        b();
        if(r){
            var inHTML = [];

            $.each(r, function(index, value){
                inHTML.push("<tr>");
                    inHTML.push("<td>");
                    inHTML.push(value.agrupacion);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push(value.nombre);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push(value.area);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push('<button type="button" class="btn btn-sm red-thunderbird removeRow" id="'+value.id+'">Eliminar</button>');
                    inHTML.push("</td>");
                inHTML.push("</tr>"); 
            });

            $("#rowsFilas").html(inHTML.join("")); //add generated tr html to corresponding table
            $("#lote").val('');
            $("#area").val('');
        }
    }

     var table = $('table > tbody');
    var ids = [];
    $('.alphanumeric')
        .wrapInner('<span title="columna ordenada"/>')
        .each(function(){
            
            var th = $(this),
                thIndex = th.index(),
                inverse = false;
            
            th.click(function(){
                ids = [];
                table.find('td').filter(function(){
                    
                    return $(this).index() === thIndex;
                    
                }).sortElements(function(a, b){
                    
                    return $.text([a]) > $.text([b]) ?
                        inverse ? -1 : 1
                        : inverse ? 1 : -1;
                    
                }, function(){
                    
                    // parentNode is the element we want to move
                    return this.parentNode; 
                    
                });
                
                inverse = !inverse;
                  
	             var button = table.find("td > button").map(function(){
	             	var id = $(this).attr("id");
	             	ids.push(id)
	             });

	             var data = {
	                id : "<? echo $_GET['id']?>",
	                id_cliente : $("#s_sucursales").val(),
	                id_lotes : ids
	              }

	             ahttp.post("./controllers/index.php?accion=Fincas.positionLote",printData , data);  
            });
             
        });

});