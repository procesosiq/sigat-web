<?php
    $response = json_decode($loader->edit());
    // print_r($response);
?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Módulo de Fincas</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="listProduc">Listado de Fincas</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Registro de Fincas</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-pane" id="tab_1">
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>AGREGAR NUEVO FINCA</div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="#" class="horizontal-form" id="formcli" method="post">
                                                    <div class="form-actions right">
                                                        <button type="button" class="btn default cancel">Cancelar</button>
                                                        <button type="button" class="btn blue btnadd" >
                                                            <i class="fa fa-check"></i>Registrar</button>
                                                    </div>
                                                    <div class="form-body">
                                                        <h3 class="form-section">INFORMACIÓN DEL FINCA</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Nombre</label>
                                                                    <input type="text" value="<?php echo $response->data->nombre?>" id="txtnom" class="form-control">
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Cliente</label>
                                                                    <select class="form-control" data-placeholder="Seleccione un Cliente" tabindex="1" id="s_sucursales">
                                                                    <?php
                                                                        if(count($response->clientes) > 0){
                                                                            $selected = "";
                                                                            foreach ($response->clientes as $key => $value) {
                                                                                $selected = "";
                                                                                if(isset($response->data->id_cliente) && $value->id == $response->data->id_cliente){
                                                                                    $selected="selected";
                                                                                }
                                                                                echo '<option value="'.$value->id.'" '.$selected.'>'.$value->nombre.'</option>';
                                                                            }
                                                                        }

                                                                    ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Fecha de Registro</label>
                                                                    <input disabled type="text" value="<?php echo $response->data->fecha?>" id="txtfec" class="form-control" placeholder="dd/mm/yyyy"> </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <?php
                                                            if(isset($_GET['id']) && $_GET['id'] > 0):
                                                        ?>
                                                        <h3 class="form-section">INFORMACIÓN DE LOTES</h3>
                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <table id="shorter" class="table table-striped table-bordered table-hover table-checkable">
                                                                    <thead>
                                                                        <tr style="cursor: pointer;">
                                                                            <th class="alphanumeric" width="30%">Agrupacion</th>
                                                                            <th class="alphanumeric" width="20%">Lote</th>
                                                                            <th class="alphanumeric" width="20%">Area</th>
                                                                            <th width="30%">Acciones</th>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                            <select class="form-control" data-placeholder="Seleccione un Cliente" tabindex="1" id="s_agrupacion">
                                                                            <?php
                                                                                if(count($response->agrupaciones) > 0){
                                                                                    foreach ($response->agrupaciones as $key => $value) {
                                                                                        echo '<option value="'.$value->id.'">'.$value->nombre.'</option>';
                                                                                    }
                                                                                }

                                                                            ?>
                                                                            </select>
                                                                            </td>
                                                                            <td><input type="text" value="" id="lote" class="form-control"></td>
                                                                            <td><input type="text" value="" id="area" class="form-control"></td>
                                                                            <td><button class="btn btn-success addRow" id="btnaddarea" type="button">
                                                                                    <i class="fa fa-plus"></i> Agregar</button></td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="rowsFilas">
                                                                        <?php
                                                                            if(count($response->lotes) > 0){
                                                                                foreach ($response->lotes as $key => $value) {
                                                                                    echo '<tr>';
                                                                                    echo '<td>'.$value->agrupacion.'</td>';
                                                                                    echo '<td>'.$value->nombre.'</td>';
                                                                                    echo '<td>'.$value->area.'</td>';
                                                                                    echo '<td><button type="button" class="btn btn-sm red-thunderbird removeRow" id="'.$value->id.'">Eliminar</button>
                                                                                    </td>';
                                                                                    echo '</tr>';
                                                                                }
                                                                            }

                                                                        ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <?php endif;?>
                                                        <!-- <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>RUC</label>
                                                                    <input type="text" class="form-control" id="txtruc"> </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Dirección</label>
                                                                    <input type="text" class="form-control" id="txtdircli"> </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Teléfono</label>
                                                                    <input type="text" class="form-control" id="txttel"> </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Ciudad</label>
                                                                    <input type="text" class="form-control" id="txtciudad"> </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                    <div class="form-actions right">
                                                        <button type="button" class="btn default cancel">Cancelar</button>
                                                        <button type="button" class="btn blue btnadd" >
                                                            <i class="fa fa-check"></i>Registrar</button>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->