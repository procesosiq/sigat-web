<?php
    $response = json_decode($loader->edit());
    // print_r($response);
?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Módulo de Productores</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="listProduc">Listado de Productores</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Registro de Productores</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-pane" id="tab_1">
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>AGREGAR NUEVO PRODUCTOR</div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="#" class="horizontal-form" id="formProc" method="post">
                                                    <div class="form-actions right">
                                                        <button type="button" class="btn default cancel">Cancelar</button>
                                                        <button type="button" class="btn blue btnadd" >
                                                            <i class="fa fa-check"></i>Registrar</button>
                                                    </div>
                                                    <div class="form-body">
                                                        <h3 class="form-section">INFORMACIÓN DEL PRODUCTOR</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Nombre</label>
                                                                    <input type="text" id="txtnom" value="<?php echo $response->data->nombre?>" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Correo Electrónico</label>
                                                                    <input type="text" id="txtemail" value="<?php echo $response->data->email?>" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Fecha de Registro</label>
                                                                    <input type="text" disabled id="txtfec" value="<?php echo $response->data->fecha?>" class="form-control" placeholder="dd/mm/yyyy"> </div>
                                                            </div>
                                                            <?php
                                                                if(isset($_GET['id']) && $_GET['id'] > 0):
                                                            ?>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label"></label>
                                                                    <button type="button" class="form-control btn blue createUser" >
                                                                        <i class="fa fa-check"></i>Generar Usuario
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <?php
                                                                endif;
                                                            ?>
                                                        </div>
                                                        <!-- <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Cantidad Equipos</label>
                                                                    <select class="form-control" data-placeholder="Choose a Category" tabindex="1" id="s_equipos">
                                                                        <option value="0">0</option>
                                                                    </select>
                                                                </div>
                                                            </div> -->
                                                            <!-- <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">¿Sucursales?</label>
                                                                    <select class="form-control" data-placeholder="Choose a Category" tabindex="1" id="s_sucursales">
                                                                        <option value="NO">NO</option>
                                                                        <option value="SI" selected>SI</option>
                                                                    </select>
                                                                </div>
                                                            </div> -->
                                                        <!-- </div> -->
                                                        <!-- <h3 class="form-section">CREAR USUARIO</h3> -->
                                                       <!--  <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>USUARIO</label>
                                                                    <input type="text" class="form-control" id="txtruc"> </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>CONTRASEÑA</label>
                                                                    <input type="text" class="form-control" id="txtdircli"> </div>
                                                            </div>
                                                        </div> -->
                                                        <!-- <h3 class="form-section">INFORMACIÓN DE FACTURACIÓN</h3>
                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="form-group">
                                                                    <label>Razón Social</label>
                                                                    <input type="text" class="form-control" id="txtrazon"> </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>RUC</label>
                                                                    <input type="text" class="form-control" id="txtruc"> </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Dirección</label>
                                                                    <input type="text" class="form-control" id="txtdircli"> </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Teléfono</label>
                                                                    <input type="text" class="form-control" id="txttel"> </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Ciudad</label>
                                                                    <input type="text" class="form-control" id="txtciudad"> </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                    <div class="form-actions right">
                                                        <button type="button" class="btn default cancel">Cancelar</button>
                                                        <button type="button" class="btn blue btnadd" >
                                                            <i class="fa fa-check"></i>Registrar</button>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->