<?php
    $response = json_decode($loader->editProducto());
?>
	<!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Módulo de Producto</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="/listProductos">Listado de Productos</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Registro de Productos</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
					<div class="row">
                        <div class="col-md-12">
                            <div class="tab-pane" id="tab_1">
                            	<div class="portlet box blue-dark">
                            		<div class="portlet-title">
                            			<div id="caption_nuevo" class="caption">
                                        	CREAR NUEVO PRODUCTO
                                        </div>
                            		</div>
                            		<div class="portlet-body form">
                            			<!-- BEGIN FORM-->
                            			<div class="form-actions right">
                                            <button type="button" id="btnaddciclo" class="btn blue btnadd" >Registrar</button>
                                            <button type="button" class="btn default cancel">Cancelar</button>
                                        </div>
                                        <div class="form-body">
                                        	<div class="row">
                                        		<div class="col-md-12">
                                        			<div class="col-md-3" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Agroquímico</span>
                                        			</div>
                                        			<div class="col-md-4">
                                        			<div class="form-group">
                                                        <input type="text" value="<?php echo $response->data->agroquimico?>" id="agroquimico" name="agroquimico" class="form-control">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                    	&nbsp;
                                                    </div>
                                        		</div>
                                        	</div>
                                        	<div class="row">
                                        		<div class="col-md-12">
                                        			<div class="col-md-3" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Nombre Comercial</span>
                                        			</div>
                                        			<div class="col-md-4">
                                        			<div class="form-group">
                                                        <input type="text" value="<?php echo $response->data->nombre_comercial?>" id="nombre_comercial" name="nombre_comercial" class="form-control">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                    	&nbsp;
                                                    </div>
                                        		</div>
                                        	</div>
                                        	<div class="row">
                                        		<div class="col-md-12">
                                        			<div class="col-md-3" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Ingrediente Activo</span>
                                        			</div>
                                        			<div class="col-md-4">
                                        			<div class="form-group">
                                                        <input type="text" value="<?php echo $response->data->ingrediente_activo?>" id="ingrediente_activo" name="ingrediente_activo" class="form-control">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        &nbsp;
                                                    </div>
                                        		</div>
                                        	</div>
                                        	<div class="row">
                                        		<div class="col-md-12">
                                        			<div class="col-md-3 right" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Casa Comercial</span>
                                        			</div>
                                        			<div class="col-md-4">
                                        			<div class="form-group">
                                                        <input type="text" value="<?php echo $response->data->casa_comercial?>" id="casa_comercial" name="casa_comercial" class="form-control">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        &nbsp;
                                                    </div>
                                        		</div>
                                        	</div>
                                        	<div class="row">
                                        		<div class="col-md-12">
                                        			<div class="col-md-3 right" style="margin-top: 0.6%;">
                                        				<span class="pull-right">Dosis 1</span>
                                        			</div>
                                        			<div class="col-md-4">
                                        			<div class="form-group">
                                                        <input type="text" value="<?php echo $response->data->dosis?>" id="dosis" name="dosis" class="form-control" placeholder="Dosis">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        &nbsp;
                                                    </div>
                                        		</div>
                                        	</div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-3 right" style="margin-top: 0.6%;">
                                                        <span class="pull-right">Dosis 2</span>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" value="<?php echo $response->data->dosis_2?>" id="dosis_2" name="dosis_2" class="form-control" placeholder="Dosis">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-3 right" style="margin-top: 0.6%;">
                                                        <span class="pull-right">Dosis 3</span>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" value="<?php echo $response->data->dosis_3?>" id="dosis_3" name="dosis_3" class="form-control" placeholder="Dosis">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-3 right" style="margin-top: 0.6%;">
                                                        <span class="pull-right">Precio (L)</span>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" value="<?php echo $response->data->precio?>"  id="precio" name="precio" class="form-control" placeholder="Precio">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            		</div>
                            	</div>
                            </div>
                        </div>
                    </div>
                    <!--END FIRST FORM-->
                    <!--END BASE CONTENT-->
            </div>