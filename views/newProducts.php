<?php
    $cdn = "http://cdn.procesos-iq.com/";
?>
<style>
    .custom-header{
        background: #337ab7;
        color: white;
        padding: 2px;
        font-weight: bold;
        text-align: center;
    }
</style>
<link href="<?=$cdn?>/global/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Módulo de Productos</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="productosList">Listado de Productos</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Registro de Producto</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                           <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-social-dribbble font-purple-soft"></i>
                                        <span class="caption-subject font-purple-soft bold uppercase">Default Tabs</span>
                                    </div>
                                    <div class="actions">
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-cloud-upload"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-wrench"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <ul class="nav nav-tabs tabs-reversed">
                                        <li class="dropdown">
                                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Dropdown
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li class="">
                                                    <a href="#tab_reversed_1_3" tabindex="-1" data-toggle="tab" aria-expanded="false"> Option 1 </a>
                                                </li>
                                                <li class="">
                                                    <a href="#tab_reversed_1_4" tabindex="-1" data-toggle="tab" aria-expanded="false"> Option 2 </a>
                                                </li>
                                                <li class="">
                                                    <a href="#tab_reversed_1_3" tabindex="-1" data-toggle="tab" aria-expanded="false"> Option 3 </a>
                                                </li>
                                                <li class="">
                                                    <a href="#tab_reversed_1_4" tabindex="-1" data-toggle="tab" aria-expanded="false"> Option 4 </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="active">
                                            <a href="#tab_reversed_1_1" data-toggle="tab" aria-expanded="true"> Listado </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_reversed_1_2" data-toggle="tab" aria-expanded="false"> Productos </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="tab_reversed_1_1">
                                            
                                        </div>
                                        <div class="tab-pane fade" id="tab_reversed_1_2">
                                            
                                        </div>
                                        <div class="tab-pane fade" id="tab_reversed_1_3">
                                           
                                        </div>
                                        <div class="tab-pane fade" id="tab_reversed_1_4">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->