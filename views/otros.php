<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<style>

.cursor th, .cursor td {
    cursor: pointer;
    /* NO SELECCIONABLE */
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.right-th {
    text-align : right;
}

/* OCULTAR COLUMNAS */
.checkbox {
    width : 110px;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .6em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.labelFilters {
    width: 150px;
    padding-top: 9px;
}

.checkbox label {
    font-size: 12px;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none !important;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}

.textRigth {
    text-align : right;
}

.textCenter {
    text-align : center;
}
</style>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=$cdn?>global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
            <div ng-app="app" ng-controller="programa">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>PROGRAMA FOLIARES
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <div class="page-toolbar">
                            <div style="float:right">
                                <div class="col-md-3">
                                    <select name="tipoHectarea" class="form-control" id="tipoHectarea"  ng-model="filters.tipoHectarea" ng-change="changeFinca()">
                                        <option ng-repeat="(key, value) in tipoHectarea" ng-selected="filters.tipoHectarea == key" value="{{key}}">{{ value }}</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select name="year" class="form-control" id="year"  ng-model="filters.year" ng-change="index()">
                                        <option ng-repeat="(key , value) in years" ng-selected="filters.year == value" value="{{value}}">{{value}}</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select name="gerente" class="form-control" id="gerente" ng-model="filters.gerente" ng-change="changeGerente()"  ng-options="item.id as item.label for item in gerentes">
                                        <!--<option ng-repeat="(key , value) in gerentes" ng-selected="filters.gerente == key" value="{{key}}">{{value}}</option>-->
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select 
                                        name="finca" 
                                        class="form-control" 
                                        id="finca" 
                                        ng-model="filters.finca" 
                                        ng-change="changeFinca()" 
                                        ng-options="item.id as item.label for item in fincas[filters.gerente]">
                                        <!--<option ng-repeat="(key, value) in fincas[filters.gerente]" ng-selected="filters.finca == label" value="{{ value.id }}">{{ value.label }}</option>-->
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="resumenCiclos">Inicio</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Listado</span>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">PROGRAMA FOLIARES</span>
                                    </div>
                                    <div class="actions col-md-12">
                                        <!--<div style="float:left;">
                                            <div class="btn-group">
                                                <a class="btn blue-ebonyclay btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                                    Columnas <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li ng-repeat="(key , value) in columnas" ng-class="columnas[key] ? 'active' : ''">
                                                        <a ng-click="columnas[key] = !columnas[key]">{{columnasText[key]}}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>-->

                                        <div class="btn-group pull-right">
                                            <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                                Exportar <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;" ng-click="exportExcel('datatable_ajax_1')">Excel</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="bootstrap-table hide">
                                        <div class="fixed-table-container" style="height: 70%;">
                                            <div class="fixed-table-header" style="margin-right: 0px;">
                                                <table class="table table-hover" style="width: 461px;">
                                                    <thead>
                                                        <tr>
                                                            <th style data-field="id" tabindex="0">ID</th>
                                                            <th style data-field="ciclo" tabindex="0">CICLO</th>
                                                            <th style data-field="sem" tabindex="0">SEM</th>
                                                            <th style data-field="fecha" tabindex="0">FECHA</th>
                                                            <th style data-field="frec" tabindex="0">FREC</th>
                                                            <th style data-field="ha" tabindex="0">Ha</th>
                                                            <th style data-field="prod" tabindex="0">$ PROD</th>
                                                            <th style data-field="prod_ha" tabindex="0">$ PROD HA</th>
                                                            <th style data-field="oper" tabindex="0">$ OPER</th>
                                                            <th style data-field="oper_ha" tabindex="0">$ OPER HA</th>
                                                            <th style data-field="dciclo" tabindex="0">$ CICLO</th>
                                                            <th style data-field="dciclo_ha" tabindex="0">$ CICLO HA</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="fixed-table-body">
                                                <table data-toggle="table" class="table table-hover" data-height="300">
                                                    <thead>
                                                        <tr>
                                                            <th style data-field="id" tabindex="0">ID</th>
                                                            <th style data-field="ciclo" tabindex="0">CICLO</th>
                                                            <th style data-field="sem" tabindex="0">SEM</th>
                                                            <th style data-field="fecha" tabindex="0">FECHA</th>
                                                            <th style data-field="frec" tabindex="0">FREC</th>
                                                            <th style data-field="ha" tabindex="0">Ha</th>
                                                            <th style data-field="prod" tabindex="0">$ PROD</th>
                                                            <th style data-field="prod_ha" tabindex="0">$ PROD HA</th>
                                                            <th style data-field="oper" tabindex="0">$ OPER</th>
                                                            <th style data-field="oper_ha" tabindex="0">$ OPER HA</th>
                                                            <th style data-field="dciclo" tabindex="0">$ CICLO</th>
                                                            <th style data-field="dciclo_ha" tabindex="0">$ CICLO HA</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr data-index="{{$index}}" ng-repeat-start="row in table | orderObjectBy : search.orderBy : search.reverse" ng-click="row.expanded = !row.expanded">
                                                            <td class="textCenter">{{ row.id }}</td>
                                                            <td class="textCenter">{{ row.ciclo }}</td>
                                                            <td class="textCenter">{{ row.sem }}</td>
                                                            <td class="textCenter">{{ row.fecha_real }}</td>
                                                            <td class="textCenter">{{ row.frec }}</td>
                                                            <td class="textCenter">{{ row.ha }}</td>
                                                            <td class="textRigth">{{ row.total_prod = (row.detalle | sumOfValue : 'prod') | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.total_prod / row.ha | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.oper | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.ha_oper | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.total_ciclo = (row.oper + row.total_prod) | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.total_ciclo / row.ha | number: 2 }}</td>
                                                        </tr>
                                                        <tr ng-show="row.expanded">
                                                            <th class="textCenter" colspan="2">PROD</th>
                                                            <th class="textCenter">TIPO</th>
                                                            <th class="textCenter">DOSIS</th>
                                                            <th class="textCenter">CANT</th>
                                                            <th class="textRigth">PRECIO</th>
                                                            <th class="textRigth">$ PROD</th>
                                                            <th class="textRigth">$ PROD HA</th>
                                                        </tr>
                                                        <tr ng-show="row.expanded" ng-repeat-end="row" ng-repeat="prod in row.detalle">
                                                            <td class="textCenter" colspan="2">{{ prod.nombre_comercial | uppercase }}</td>
                                                            <td class="textCenter">{{ prod.tipo | uppercase }}</td>
                                                            <td class="textCenter">{{ prod.dosis | number }}</td>
                                                            <td class="textCenter">{{ prod.cantidad }}</td>
                                                            <td class="textRigth">{{ prod.precio }}</td>
                                                            <td class="textRigth">{{ prod.prod | number: 2 }}</td>
                                                            <td class="textRigth">{{ prod.prod_ha | number: 2 }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-container table-scrollable">
                                        <table class="table table-striped table-bordered table-hover table-checkable">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>CICLO</th>
                                                    <th>SEM</th>
                                                    <th>FECHA</th>
                                                    <th>FREC</th>
                                                    <th>Ha</th>
                                                    <th>$ PROD</th>
                                                    <th>$ PROD HA</th>
                                                    <th>$ OPER</th>
                                                    <th>$ OPER HA</th>
                                                    <th>$ CICLO</th>
                                                    <th>$ CICLO HA</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start="row in table | orderObjectBy : search.orderBy : search.reverse" ng-click="row.expanded = !row.expanded">
                                                    <td class="textCenter">{{ row.id }}</td>
                                                    <td class="textCenter">{{ row.ciclo }}</td>
                                                    <td class="textCenter">{{ row.sem }}</td>
                                                    <td class="textCenter">{{ row.fecha_real }}</td>
                                                    <td class="textCenter">{{ row.frec }}</td>
                                                    <td class="textCenter">{{ row.ha }}</td>
                                                    <td class="textRigth">{{ row.total_prod = (row.detalle | sumOfValue : 'prod') | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_prod / row.ha | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.oper = (row.ha_oper * row.ha) | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.ha_oper | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_ciclo = (row.oper + row.total_prod) | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_ciclo / row.ha | number: 2 }}</td>
                                                </tr>
                                                <tr ng-show="row.expanded">
                                                    <th class="textCenter" colspan="2">PROD</th>
                                                    <th class="textCenter">TIPO</th>
                                                    <th class="textCenter">DOSIS</th>
                                                    <th class="textCenter">CANT</th>
                                                    <th class="textRigth">PRECIO</th>
                                                    <th class="textRigth">$ PROD</th>
                                                    <th class="textRigth">$ PROD HA</th>
                                                </tr>
                                                <tr ng-show="row.expanded" ng-repeat-end="row" ng-repeat="prod in row.detalle">
                                                    <td class="textCenter" colspan="2">{{ prod.nombre_comercial | uppercase }}</td>
                                                    <td class="textCenter">{{ prod.tipo | uppercase }}</td>
                                                    <td class="textCenter">{{ prod.dosis | number }}</td>
                                                    <td class="textCenter">{{ prod.cantidad | number }}</td>
                                                    <td class="textRigth">{{ prod.precio }}</td>
                                                    <td class="textRigth">{{ prod.prod | number: 2 }}</td>
                                                    <td class="textRigth">{{ prod.prod_ha | number: 2 }}</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <td class="textCenter">{{ table | countOfValue:'ciclo' }}</td>
                                                    <td ></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="textCenter">{{ table | avgOfValue:'ha' | number: 2 }}</td>
                                                    <td class="textRigth">{{ table | sumOfValue:'total_prod' | number: 2 }}</td>
                                                    <td class="textRigth">{{ ((table | sumOfValue:'total_prod') / (table | sumOfValue:'ha')) | number: 2 }}</td>
                                                    <td class="textRigth">{{ table | sumOfValue:'oper' | number: 2 }}</td>
                                                    <td class="textRigth">{{ ((table | sumOfValue:'oper') / (table | sumOfValue:'ha')) | number: 2 }}</td>
                                                    <td class="textRigth">{{ table | sumOfValue:'total_ciclo' | number: 2 }}</td>
                                                    <td class="textRigth">{{ ((table | sumOfValue:'total_ciclo') / (table | sumOfValue:'ha')) | number: 2 }}</td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="portlet-body hide">
                                    <div class="table-container table-scrollable">
                                        <table class="table table-striped table-bordered table-hover table-checkable hide"  id="datatable_ajax_1">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>CICLO</th>
                                                    <th>SEM</th>
                                                    <th>FECHA</th>
                                                    <th>FREC</th>
                                                    <th>Ha</th>
                                                    <th>FOLIAR 1</th>
                                                    <th>PECIO 1</th>
                                                    <th>$ HA 1</th>
                                                    <th>$ TOTAL 1</th>
                                                    <th>FOLIAR 2</th>
                                                    <th>PRECIO 2</th>
                                                    <th>$ HA 2</th>
                                                    <th>$ TOTAL 2</th>
                                                    <th>$ COSTO TOTAL</th>
                                                    <th>$ COSTO/HA</th>
                                                    <th>$ OPER</th>
                                                    <th>$ OPER HA</th>
                                                    <th>$ CICLO</th>
                                                    <th>$ CICLO HA</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="row in table | orderObjectBy : search.orderBy : search.reverse" ng-click="row.expanded = !row.expanded">
                                                    <td class="textCenter">{{ row.id }}</td>
                                                    <td class="textCenter">{{ row.ciclo }}</td>
                                                    <td class="textCenter">{{ row.sem }}</td>
                                                    <td class="textCenter">{{ row.fecha_real }}</td>
                                                    <td class="textCenter">{{ row.frec }}</td>
                                                    <td class="textCenter">{{ row.ha }}</td>
                                                    <td>{{ row.detalle[0].nombre_comercial | uppercase }}</td>
                                                    <td>{{ row.detalle[0].precio }}</td>
                                                    <td>{{ (row.detalle[0].precio * row.detalle[0].dosis) | number : 2 }}</td>
                                                    <td>{{ row.detalle[0].prod | number: 2 }}</td>
                                                    <td>{{ row.detalle[1].nombre_comercial | uppercase }}</td>
                                                    <td>{{ row.detalle[1].precio }}</td>
                                                    <td>{{ (row.detalle[1].precio * row.detalle[0].dosis) | number : 2 }}</td>
                                                    <td>{{ row.detalle[1].prod | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_prod = (row.detalle | sumOfValue : 'prod') | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_prod / row.ha | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.oper | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.ha_oper | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_ciclo = (row.oper + row.total_prod) | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_ciclo / row.ha | number: 2 }}</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">PARCIALES</span>
                                    </div>
                                    <div class="actions col-md-12">
                                        <!--<div style="float:left;">
                                            <div class="btn-group">
                                                <a class="btn blue-ebonyclay btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                                    Columnas <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li ng-repeat="(key , value) in columnas" ng-class="columnas[key] ? 'active' : ''">
                                                        <a ng-click="columnas[key] = !columnas[key]">{{columnasText[key]}}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>-->

                                        <div class="btn-group pull-right">
                                            <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                                Exportar <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;" ng-click="exportExcel('datatable_ajax_2')">Excel</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="bootstrap-table hide">
                                        <div class="fixed-table-container" style="height: 70%;">
                                            <div class="fixed-table-header" style="margin-right: 0px;">
                                                <table class="table table-hover" style="width: 461px;">
                                                    <thead>
                                                        <tr>
                                                            <th style data-field="id" tabindex="0">ID</th>
                                                            <th style data-field="ciclo" tabindex="0">CICLO</th>
                                                            <th style data-field="sem" tabindex="0">SEM</th>
                                                            <th style data-field="fecha" tabindex="0">FECHA</th>
                                                            <th style data-field="frec" tabindex="0">FREC</th>
                                                            <th style data-field="ha" tabindex="0">Ha</th>
                                                            <th style data-field="prod" tabindex="0">$ PROD</th>
                                                            <th style data-field="prod_ha" tabindex="0">$ PROD HA</th>
                                                            <th style data-field="oper" tabindex="0">$ OPER</th>
                                                            <th style data-field="oper_ha" tabindex="0">$ OPER HA</th>
                                                            <th style data-field="dciclo" tabindex="0">$ CICLO</th>
                                                            <th style data-field="dciclo_ha" tabindex="0">$ CICLO HA</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="fixed-table-body">
                                                <table data-toggle="table" class="table table-hover" data-height="300">
                                                    <thead>
                                                        <tr>
                                                            <th style data-field="id" tabindex="0">ID</th>
                                                            <th style data-field="ciclo" tabindex="0">CICLO</th>
                                                            <th style data-field="sem" tabindex="0">SEM</th>
                                                            <th style data-field="fecha" tabindex="0">FECHA</th>
                                                            <th style data-field="frec" tabindex="0">FREC</th>
                                                            <th style data-field="ha" tabindex="0">Ha</th>
                                                            <th style data-field="prod" tabindex="0">$ PROD</th>
                                                            <th style data-field="prod_ha" tabindex="0">$ PROD HA</th>
                                                            <th style data-field="oper" tabindex="0">$ OPER</th>
                                                            <th style data-field="oper_ha" tabindex="0">$ OPER HA</th>
                                                            <th style data-field="dciclo" tabindex="0">$ CICLO</th>
                                                            <th style data-field="dciclo_ha" tabindex="0">$ CICLO HA</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr data-index="{{$index}}" ng-repeat-start="row in parciales | orderObjectBy : search.orderBy : search.reverse" ng-click="row.expanded = !row.expanded">
                                                            <td class="textCenter">{{ row.id }}</td>
                                                            <td class="textCenter">{{ row.ciclo }}</td>
                                                            <td class="textCenter">{{ row.sem }}</td>
                                                            <td class="textCenter">{{ row.fecha_real }}</td>
                                                            <td class="textCenter">{{ row.frec }}</td>
                                                            <td class="textCenter">{{ row.ha }}</td>
                                                            <td class="textRigth">{{ row.total_prod = (row.detalle | sumOfValue : 'prod') | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.total_prod / row.ha | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.oper | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.ha_oper | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.total_ciclo = (row.oper + row.total_prod) | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.total_ciclo / row.ha | number: 2 }}</td>
                                                        </tr>
                                                        <tr ng-show="row.expanded">
                                                            <th class="textCenter" colspan="2">PROD</th>
                                                            <th class="textCenter">TIPO</th>
                                                            <th class="textCenter">DOSIS</th>
                                                            <th class="textCenter">CANT</th>
                                                            <th class="textRigth">PRECIO</th>
                                                            <th class="textRigth">$ PROD</th>
                                                            <th class="textRigth">$ PROD HA</th>
                                                        </tr>
                                                        <tr ng-show="row.expanded" ng-repeat-end="row" ng-repeat="prod in row.detalle">
                                                            <td class="textCenter" colspan="2">{{ prod.nombre_comercial | uppercase }}</td>
                                                            <td class="textCenter">{{ prod.tipo | uppercase }}</td>
                                                            <td class="textCenter">{{ prod.dosis | number }}</td>
                                                            <td class="textCenter">{{ prod.cantidad | number }}</td>
                                                            <td class="textRigth">{{ prod.precio }}</td>
                                                            <td class="textRigth">{{ prod.prod | number: 2 }}</td>
                                                            <td class="textRigth">{{ prod.prod_ha | number: 2 }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-container table-scrollable">
                                        <table class="table table-striped table-bordered table-hover table-checkable">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>CICLO</th>
                                                    <th>SEM</th>
                                                    <th>FECHA</th>
                                                    <th>FREC</th>
                                                    <th>Ha</th>
                                                    <th>$ PROD</th>
                                                    <th>$ PROD HA</th>
                                                    <th>$ OPER</th>
                                                    <th>$ OPER HA</th>
                                                    <th>$ CICLO</th>
                                                    <th>$ CICLO HA</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start="row in parciales | orderObjectBy : search.orderBy : search.reverse" ng-click="row.expanded = !row.expanded">
                                                    <td class="textCenter">{{ row.id }}</td>
                                                    <td class="textCenter">{{ row.ciclo }}</td>
                                                    <td class="textCenter">{{ row.sem }}</td>
                                                    <td class="textCenter">{{ row.fecha_real }}</td>
                                                    <td class="textCenter">{{ row.frec }}</td>
                                                    <td class="textCenter">{{ row.ha }}</td>
                                                    <td class="textRigth">{{ row.total_prod = (row.detalle | sumOfValue : 'prod') | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_prod / row.ha | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.oper | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.ha_oper | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_ciclo = (row.oper + row.total_prod) | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_ciclo / row.ha | number: 2 }}</td>
                                                </tr>
                                                <tr ng-show="row.expanded">
                                                    <th class="textCenter" colspan="2">PROD</th>
                                                    <th class="textCenter">TIPO</th>
                                                    <th class="textCenter">DOSIS</th>
                                                    <th class="textCenter">CANT</th>
                                                    <th class="textRigth">PRECIO</th>
                                                    <th class="textRigth">$ PROD</th>
                                                    <th class="textRigth">$ PROD HA</th>
                                                </tr>
                                                <tr ng-show="row.expanded" ng-repeat-end="row" ng-repeat="prod in row.detalle">
                                                    <td class="textCenter" colspan="2">{{ prod.nombre_comercial | uppercase }}</td>
                                                    <td class="textCenter">{{ prod.tipo | uppercase }}</td>
                                                    <td class="textCenter">{{ prod.dosis | number }}</td>
                                                    <td class="textCenter">{{ prod.cantidad }}</td>
                                                    <td class="textRigth">{{ prod.precio }}</td>
                                                    <td class="textRigth">{{ prod.prod | number: 2 }}</td>
                                                    <td class="textRigth">{{ prod.prod_ha | number: 2 }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="portlet-body hide">
                                    <div class="table-container table-scrollable">
                                        <table class="table table-striped table-bordered table-hover table-checkable hide"  id="datatable_ajax_2">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>CICLO</th>
                                                    <th>SEM</th>
                                                    <th>FECHA</th>
                                                    <th>FREC</th>
                                                    <th>Ha</th>
                                                    <th>FOLIAR 1</th>
                                                    <th>PECIO 1</th>
                                                    <th>$ HA 1</th>
                                                    <th>$ TOTAL 1</th>
                                                    <th>FOLIAR 2</th>
                                                    <th>PRECIO 2</th>
                                                    <th>$ HA 2</th>
                                                    <th>$ TOTAL 2</th>
                                                    <th>$ COSTO TOTAL</th>
                                                    <th>$ COSTO/HA</th>
                                                    <th>$ OPER</th>
                                                    <th>$ OPER HA</th>
                                                    <th>$ CICLO</th>
                                                    <th>$ CICLO HA</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="row in table | orderObjectBy : search.orderBy : search.reverse" ng-click="row.expanded = !row.expanded">
                                                    <td class="textCenter">{{ row.id }}</td>
                                                    <td class="textCenter">{{ row.ciclo }}</td>
                                                    <td class="textCenter">{{ row.sem }}</td>
                                                    <td class="textCenter">{{ row.fecha_real }}</td>
                                                    <td class="textCenter">{{ row.frec }}</td>
                                                    <td class="textCenter">{{ row.ha }}</td>
                                                    <td>{{ row.detalle[0].nombre_comercial | uppercase }}</td>
                                                    <td>{{ row.detalle[0].precio }}</td>
                                                    <td>{{ (row.detalle[0].precio * row.detalle[0].dosis) | number : 2 }}</td>
                                                    <td>{{ row.detalle[0].prod | number: 2 }}</td>
                                                    <td>{{ row.detalle[1].nombre_comercial | uppercase }}</td>
                                                    <td>{{ row.detalle[1].precio }}</td>
                                                    <td>{{ (row.detalle[1].precio * row.detalle[0].dosis) | number : 2 }}</td>
                                                    <td>{{ row.detalle[1].prod | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_prod = (row.detalle | sumOfValue : 'prod') | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_prod / row.ha | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.oper | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.ha_oper | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_ciclo = (row.oper + row.total_prod) | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_ciclo / row.ha | number: 2 }}</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>

                </div>
            </div>