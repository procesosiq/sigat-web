<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<style>

.cursor th, .cursor td {
    cursor: pointer;
    /* NO SELECCIONABLE */
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.right-th {
    text-align : right;
}

/* OCULTAR COLUMNAS */
.checkbox {
    width : 110px;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .6em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.labelFilters {
    width: 150px;
    padding-top: 9px;
}

.checkbox label {
    font-size: 12px;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none !important;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}

.textRigth {
    text-align : right;
}

.textCenter {
    text-align : center;
}
</style>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=$cdn?>global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
            <div ng-app="app" ng-controller="programa">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>PROGRAMA FOLIARES
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <div class="page-toolbar">
                            <div style="float:right">
                                <div class="col-md-3">
                                    <select name="tipoHectarea" class="form-control" id="tipoHectarea"  ng-model="filters.tipoHectarea" ng-change="changeFinca()">
                                        <option ng-repeat="(key, value) in tipoHectarea" ng-selected="filters.tipoHectarea == key" value="{{key}}">{{ value }}</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select name="year" class="form-control" id="year"  ng-model="filters.year" ng-change="changeYear()">
                                        <option ng-repeat="(key , value) in years" ng-selected="filters.year == value" value="{{value}}">{{value}}</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select name="gerente" class="form-control" id="gerente" ng-model="filters.gerente" ng-change="changeGerente()"  ng-options="item.id as item.label for item in gerentes">
                                        <!--<option ng-repeat="(key , value) in gerentes" ng-selected="filters.gerente == key" value="{{key}}">{{value}}</option>-->
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select name="finca" class="form-control" id="finca" ng-model="filters.finca" ng-change="changeFinca()" ng-options="item.id as item.label for item in fincas[filters.gerente]">
                                        <!--<option ng-repeat="(key, value) in fincas[filters.gerente]" ng-selected="filters.finca == label" value="{{ value.id }}">{{ value.label }}</option>-->
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="resumenCiclos">Inicio</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Listado</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                   <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">PROGRAMA FOLIARES</span>
                                    </div>
                                    <div class="actions col-md-12">
                                        <div style="float:left;">
                                            <div class="btn-group">
                                                <a class="btn blue-ebonyclay btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                                    Columnas <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li ng-repeat="(key , value) in columnas" ng-class="columnas[key] ? 'active' : ''">
                                                        <a ng-click="columnas[key] = !columnas[key]">{{columnasText[key]}}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="btn-group pull-right">
                                            <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                                Exportar <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;" ng-click="exportPrint('datatable_ajax_1')"> Imprimir </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;" ng-click="exportExcel('datatable_ajax_1')">Excel</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="bootstrap-table hide">
                                        <div class="fixed-table-container" style="height: 70%;">
                                            <div class="fixed-table-header" style="margin-right: 0px;">
                                                <table class="table table-hover" style="width: 461px;">
                                                    <thead>
                                                        <tr>
                                                            <th style data-field="id" tabindex="0">ID</th>
                                                            <th style data-field="ciclo" tabindex="0">CICLO</th>
                                                            <th style data-field="sem" tabindex="0">SEM</th>
                                                            <th style data-field="fecha" tabindex="0">FECHA</th>
                                                            <th style data-field="frec" tabindex="0">FREC</th>
                                                            <th style data-field="ha" tabindex="0">Ha</th>
                                                            <th style data-field="prod" tabindex="0">$ PROD</th>
                                                            <th style data-field="prod_ha" tabindex="0">$ PROD HA</th>
                                                            <th style data-field="oper" tabindex="0">$ OPER</th>
                                                            <th style data-field="oper_ha" tabindex="0">$ OPER HA</th>
                                                            <th style data-field="dciclo" tabindex="0">$ CICLO</th>
                                                            <th style data-field="dciclo_ha" tabindex="0">$ CICLO HA</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="fixed-table-body">
                                                <table data-toggle="table" class="table table-hover" data-height="300">
                                                    <thead>
                                                        <tr>
                                                            <th style data-field="id" tabindex="0">ID</th>
                                                            <th style data-field="ciclo" tabindex="0">CICLO</th>
                                                            <th style data-field="sem" tabindex="0">SEM</th>
                                                            <th style data-field="fecha" tabindex="0">FECHA</th>
                                                            <th style data-field="frec" tabindex="0">FREC</th>
                                                            <th style data-field="ha" tabindex="0">Ha</th>
                                                            <th style data-field="prod" tabindex="0">$ PROD</th>
                                                            <th style data-field="prod_ha" tabindex="0">$ PROD HA</th>
                                                            <th style data-field="oper" tabindex="0">$ OPER</th>
                                                            <th style data-field="oper_ha" tabindex="0">$ OPER HA</th>
                                                            <th style data-field="dciclo" tabindex="0">$ CICLO</th>
                                                            <th style data-field="dciclo_ha" tabindex="0">$ CICLO HA</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr data-index="{{$index}}" ng-repeat-start="row in table | orderObjectBy : search.orderBy : search.reverse" ng-click="row.expanded = !row.expanded">
                                                            <td class="textCenter">{{ row.id }}</td>
                                                            <td class="textCenter">{{ row.ciclo }}</td>
                                                            <td class="textCenter">{{ row.sem }}</td>
                                                            <td class="textCenter">{{ row.fecha_real }}</td>
                                                            <td class="textCenter">{{ row.frec }}</td>
                                                            <td class="textCenter">{{ row.ha }}</td>
                                                            <td class="textRigth">{{ row.total_prod = (row.detalle | sumOfValue : 'prod') | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.total_prod / row.ha | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.oper | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.ha_oper | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.total_ciclo = (row.oper + row.total_prod) | number: 2 }}</td>
                                                            <td class="textRigth">{{ row.total_ciclo / row.ha | number: 2 }}</td>
                                                        </tr>
                                                        <tr ng-show="row.expanded">
                                                            <th class="textCenter" colspan="2">PROD</th>
                                                            <th class="textCenter">TIPO</th>
                                                            <th class="textCenter">DOSIS</th>
                                                            <th class="textCenter">CANT</th>
                                                            <th class="textRigth">PRECIO</th>
                                                            <th class="textRigth">$ PROD</th>
                                                            <th class="textRigth">$ PROD HA</th>
                                                        </tr>
                                                        <tr ng-show="row.expanded" ng-repeat-end="row" ng-repeat="prod in row.detalle">
                                                            <td class="textCenter" colspan="2">{{ prod.nombre_comercial | uppercase }}</td>
                                                            <td class="textCenter">{{ prod.tipo | uppercase }}</td>
                                                            <td class="textCenter">{{ prod.dosis }}</td>
                                                            <td class="textCenter">{{ prod.cantidad }}</td>
                                                            <td class="textRigth">{{ prod.precio }}</td>
                                                            <td class="textRigth">{{ prod.prod | number: 2 }}</td>
                                                            <td class="textRigth">{{ prod.prod_ha | number: 2 }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-container table-scrollable">
                                        <table class="table table-striped table-bordered table-hover table-checkable">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>CICLO</th>
                                                    <th>SEM</th>
                                                    <th>FECHA</th>
                                                    <th>FREC</th>
                                                    <th>Ha</th>
                                                    <th>$ PROD</th>
                                                    <th>$ PROD HA</th>
                                                    <th>$ OPER</th>
                                                    <th>$ OPER HA</th>
                                                    <th>$ CICLO</th>
                                                    <th>$ CICLO HA</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start="row in table | orderObjectBy : search.orderBy : search.reverse" ng-click="row.expanded = !row.expanded">
                                                    <td class="textCenter">{{ row.id }}</td>
                                                    <td class="textCenter">{{ row.ciclo }}</td>
                                                    <td class="textCenter">{{ row.sem }}</td>
                                                    <td class="textCenter">{{ row.fecha_real }}</td>
                                                    <td class="textCenter">{{ row.frec }}</td>
                                                    <td class="textCenter">{{ row.ha }}</td>
                                                    <td class="textRigth">{{ row.total_prod = (row.detalle | sumOfValue : 'prod') | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_prod / row.ha | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.oper | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.ha_oper | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_ciclo = (row.oper + row.total_prod) | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_ciclo / row.ha | number: 2 }}</td>
                                                </tr>
                                                <tr ng-show="row.expanded">
                                                    <th class="textCenter" colspan="2">PROD</th>
                                                    <th class="textCenter">TIPO</th>
                                                    <th class="textCenter">DOSIS</th>
                                                    <th class="textCenter">CANT</th>
                                                    <th class="textRigth">PRECIO</th>
                                                    <th class="textRigth">$ PROD</th>
                                                    <th class="textRigth">$ PROD HA</th>
                                                </tr>
                                                <tr ng-show="row.expanded" ng-repeat-end="row" ng-repeat="prod in row.detalle">
                                                    <td class="textCenter" colspan="2">{{ prod.nombre_comercial | uppercase }}</td>
                                                    <td class="textCenter">{{ prod.tipo | uppercase }}</td>
                                                    <td class="textCenter">{{ prod.dosis }}</td>
                                                    <td class="textCenter">{{ prod.cantidad }}</td>
                                                    <td class="textRigth">{{ prod.precio }}</td>
                                                    <td class="textRigth">{{ prod.prod | number: 2 }}</td>
                                                    <td class="textRigth">{{ prod.prod_ha | number: 2 }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <div class="portlet-body hide">
                                    <div class="table-container table-scrollable">
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_1">
                                            <thead>
                                                <tr class="cursor">
                                                    <th width="10%" ng-click="changeSort('search', 'id')"> ID </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ciclo')"> CICLO  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha')"> HA  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'fecha_real')"> FECHA PROG  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'fecha_real')"> FECHA REAL </th>
                                                    <th width="10%" ng-click="changeSort('search', 'sem')"> SEM </th>
                                                    <th width="10%" ng-click="changeSort('search', 'atraco')"> ATRASO </th>
                                                    <th width="10%" ng-click="changeSort('search', 'motivo')"> MOTIVO </th>
                                                    <!-- BIOESTIMULANTE -->
                                                    <th width="10%" ng-click="changeSort('search', 'bioestimulantes_1')"> BIOESTIMULANTE 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'precio_3')" > PRECIO 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'cantidad_3')" > CANTIDAD 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'dosis_3')" > DOSIS 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_3')" > $ HA 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'total_3')" > $ TOTAL 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'bioestimulantes_2')"> BIOESTIMULANTE 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'precio_4')" > PRECIO 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'cantidad_4')" > CANTIDAD 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'dosis_4')" > DOSIS 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_4')" > $ HA 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'total_4')" > $ TOTAL 2 </th>
                                                    <!-- BIOESTIMULANTE -->
                                                    <th width="10%" ng-click="changeSort('search', 'foliar_1')"> FOLIAR 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'precio_8')" > PRECIO 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'cantidad_8')" > CANTIDAD 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'dosis_8')" > DOSIS 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_8')" > $ HA 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'total_8')" > $ TOTAL 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'foliar_2')"> FOLIAR 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'precio_9')" > PRECIO 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'cantidad_9')" > CANTIDAD 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'dosis_9')" > DOSIS 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_9')" > $ HA 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'total_9')" > $ TOTAL 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'foliar_3')"> FOLIAR 3 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'precio_10')" > PRECIO 3 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'cantidad_10')" > CANTIDAD 3 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'dosis_10')" > DOSIS 3 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_10')" > $ HA 3 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'total_10')" > $ TOTAL 3 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'coadyuvante_2')" > COADYUVANTE 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'precio_6')" > PRECIO </th>
                                                    <th width="10%" ng-click="changeSort('search', 'cantidad_6')" > CANTIDAD </th>
                                                    <th width="10%" ng-click="changeSort('search', 'dosis_6')" > DOSIS  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_6')" > $ HA </th>
                                                    <th width="10%" ng-click="changeSort('search', 'total_6')" > $ TOTAL </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_oper')" > $/HA OPER </th>
                                                    <th width="10%" ng-click="changeSort('search', 'oper')" > OPER </th>
                                                    <th width="10%" ng-click="changeSort('search', 'costo_total')" > $ COSTO TOTAL </th>
                                                    <th width="10%" ng-click="changeSort('search', 'costo_ha')" > $COSTO/HA </th>
                                                </tr>
                                            </thead>
                                            <tbody> 
                                                <tr ng-repeat="row in table | orderObjectBy : search.orderBy : search.reverse">  
                                                    <td>{{ row.id }}</td>
                                                    <td>{{ row.ciclo }}</td>
                                                    <td >{{ row.ha }}</td>
                                                    <td>{{ row.fecha_prog }}</td>
                                                    <td>{{ row.fecha_real }}</td>
                                                    <td>{{ row.sem }}</td>
                                                    <td>{{ row.atraso }}</td>
                                                    <td>{{ row.motivo }}</td>
                                                    <!-- BIOESTIMULANTE -->
                                                    <td>{{ row.bioestimulantes_1 }}</td>
                                                    <td class="textRigth" ng-if="row.precio_3 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.precio_3 > 0">{{ row.precio_3  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.cantidad_3 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.cantidad_3 > 0">{{ row.cantidad_3 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.dosis_3 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.dosis_3 > 0">{{ row.dosis_3 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_3 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_3 > 0">{{ row.ha_3  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.total_3 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.total_3 > 0">{{ row.total_3  | number : 2 }}</td>
                                                    <td>{{ row.bioestimulantes_2 }}</td>
                                                    <td class="textRigth" ng-if="row.precio_4 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.precio_4 > 0">{{ row.precio_4  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.cantidad_4 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.cantidad_4 > 0">{{ row.cantidad_4 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.dosis_4 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.dosis_4 > 0">{{ row.dosis_4 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_4 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_4 > 0">{{ row.ha_4  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.total_4 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.total_4 > 0">{{ row.total_4  | number : 2 }}</td>
                                                    <!-- BIOESTIMULANTE -->
                                                    <td>{{ row.foliar_1 }}</td>
                                                    <td class="textRigth" ng-if="row.precio_8 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.precio_8 > 0">{{ row.precio_8 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.cantidad_8 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.cantidad_8 > 0">{{ row.cantidad_8 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.dosis_8 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.dosis_8 > 0">{{ row.dosis_8 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_8 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_8 > 0">{{ row.ha_8  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.total_8 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.total_8 > 0">{{ row.total_8  | number : 2 }}</td>
                                                    <td>{{ row.foliar_2 }}</td>
                                                    <td class="textRigth" ng-if="row.precio_9 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.precio_9 > 0">{{ row.precio_9  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.cantidad_9 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.cantidad_9 > 0">{{ row.cantidad_9 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.dosis_9 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.dosis_9 > 0">{{ row.dosis_9 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_9 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_9 > 0">{{ row.ha_9  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.total_9 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.total_9 > 0">{{ row.total_9  | number : 2 }}</td>
                                                    <td>{{ row.foliar_3 }}</td>
                                                    <td class="textRigth" ng-if="row.precio_10 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.precio_10 > 0">{{ row.precio_10  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.cantidad_10 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.cantidad_10 > 0">{{ row.cantidad_10 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.dosis_10 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.dosis_10 > 0">{{ row.dosis_10 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_10 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_10 > 0">{{ row.ha_10  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.total_10 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.total_10 > 0">{{ row.total_10  | number : 2 }}</td>
                                                    <td>{{ row.coadyuvante_2 }}</td>
                                                    <td class="textRigth" ng-if="row.precio_6 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.precio_6 > 0">{{ row.precio_6  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.cantidad_6 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.cantidad_6 > 0">{{ row.cantidad_6 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.dosis_6 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.dosis_6 > 0">{{ row.dosis_6 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_6 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_6 > 0">{{ row.ha_6  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.total_6 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.total_6 > 0">{{ row.total_6  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_oper <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_oper > 0">{{ row.ha_oper  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.oper <= 0"></td>
                                                    <td class="textRigth" ng-if="row.oper > 0">{{ row.oper  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.costo_total <= 0"></td>
                                                    <td class="textRigth" ng-if="row.costo_total > 0">{{ row.costo_total  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.costo_ha <= 0"></td>
                                                    <td class="textRigth" ng-if="row.costo_ha > 0">{{ row.costo_ha  | number : 2 }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>