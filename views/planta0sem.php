<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
    <style>

        .listado{
            list-style-type: none;
            display: inline-block;
        }
        .listado > li{
            display: inline;
            margin-right: 10px;
        }
        .miniature{
            display: inline-block;
        }

        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .legendLabel{
            padding: 3px !important;
        }
        .textTittle {
            font-size: 11px !important;
        }
        table > th {
            text-align: center !important;
        }
        tbody > td {
            text-align: center !important;
        }
    </style>
                <!-- BEGIN CONTENT BODY -->
                <div ng-app="app" class="page-content" ng-controller="plantas0Sem" ng-cloak>
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head" ng-init="init()">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>PLANTAS 0 SEMANAS
                                <small>Estadisticas de Plantas 0 semanas</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <?php include './views/clientes_tags.php';?>
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Plantas 0 semanas</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <!-- BEGIN DASHBOARD STATS 1-->
                        <?php include("clima_tags.php");?>
                    <!-- BEGIN INTERACTIVE CHART PORTLET-->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-dark sbold uppercase">
                                            <h3>
                                               <i class="icon-settings font-dark"></i> Plantas 0 semanas
                                            </h3>
                                            <small>Hojas totales</small>
                                        </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="label_hojas_total" class="label_chart"> </div>
                                    <div id="hojas_total" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_hojas_total" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Datos 
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                                    </div>
                                </div>
                                <div class="portlet-body" style="display: none;">
                                    <div class="caption">
                                            <span class="caption-subject font-dark sbold uppercase">
                                                <h3>
                                                   <i class="icon-settings font-dark"></i> Plantas 0 semanas
                                                </h3>
                                                <small>Hojas totales</small>
                                            </span>
                                    </div>
                                    <div class="table-scrollable"> 
                                        <table class="table table-striped table-bordered table-hover textTittle">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th class="textTittle" colspan="{{semanas.length}}"> Semanas </th>
                                                </tr>
                                                <tr>
                                                    <th>Foco</th>
                                                    <th ng-repeat="(key, value) in semanas">{{value}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="(key,value) in charts.hojas_total">
                                                    <td class="textTittle">{{key}}</td>
                                                    <td class="textTittle" ng-repeat="(llave, valor) in semanas">
                                                        {{value[valor] | number : 1}}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END INTERACTIVE CHART PORTLET-->
                    <!-- END DASHBOARD STATS 1-->
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-dark sbold uppercase">
                                            <h3>
                                               <i class="icon-settings font-dark"></i> Plantas 0 semanas
                                            </h3>
                                            <small >Hoja mas vieja libre de quema menor a 5%</small>
                                        </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div ng-bind-html="grafica_menor_5" id="label_hoja_vieja_menor_5" class="label_chart"> </div>
                                    <div ng-bind-html="grafica_menor_5" id="hoja_vieja_menor_5" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_hoja_vieja_menor_5" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-dark sbold uppercase">
                                            <h3>
                                               <i class="icon-settings font-dark"></i> Plantas 0 semanas
                                            </h3>
                                            <small>Hoja mas vieja libre de estrias</small>
                                        </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="label_libre_estrias" class="label_chart"> </div>
                                    <div id="libre_estrias" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_libre_estrias" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                        <div class="col-md-6">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Datos 
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                                    </div>
                                </div>
                                <div class="portlet-body" style="display: none;">
                                    <div class="caption">
                                        <span class="caption-subject font-dark sbold uppercase">
                                            <h3>
                                               <i class="icon-settings font-dark"></i> Plantas 0 semanas
                                            </h3>
                                            <small >Hoja mas vieja libre de quema menor a 5%</small>
                                        </span>
                                    </div>
                                    <div class="table-scrollable"> 
                                        <table class="table table-striped table-bordered table-hover textTittle">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th class="textTittle" colspan="{{semanas.length}}"> Semanas </th>
                                                </tr>
                                                <tr>
                                                    <th>Foco</th>
                                                    <th ng-repeat="(key, value) in semanas">{{value}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="(key,value) in charts.hoja_vieja_menor_5">
                                                    <td class="textTittle">{{key}}</td>
                                                    <td class="textTittle" ng-repeat="(llave, valor) in semanas">
                                                        {{value[valor] | number : 1}}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Datos 
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                                    </div>
                                </div>
                                <div class="portlet-body" style="display: none;">
                                    <div class="caption">
                                        <span class="caption-subject font-dark sbold uppercase">
                                            <h3>
                                               <i class="icon-settings font-dark"></i> Plantas 0 semanas
                                            </h3>
                                            <small>Hoja mas vieja libre de estrias</small>
                                        </span>
                                    </div>
                                    <div class="table-scrollable"> 
                                        <table class="table table-striped table-bordered table-hover textTittle">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th class="textTittle" colspan="{{semanas.length}}"> Semanas </th>
                                                </tr>
                                                <tr>
                                                    <th>Foco</th>
                                                    <th ng-repeat="(key, value) in semanas">{{value}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="(key,value) in charts.libre_estrias">
                                                    <td class="textTittle">{{key}}</td>
                                                    <td class="textTittle" ng-repeat="(llave, valor) in semanas">
                                                        {{value[valor] | number : 1}}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END DASHBOARD STATS 1-->
                    <div class="row hide">
                        <div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-dark sbold uppercase">
                                            <h3>
                                               <i class="icon-settings font-dark"></i> Plantas 0 semanas
                                            </h3>
                                            <small>Libre de cirugia</small>
                                        </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="label_libre_cirugia" class="label_chart"> </div>
                                    <div id="libre_cirugia" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_libre_cirugia" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                        <div class="col-md-6 col-sm-6 hide">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-dark sbold uppercase">
                                            <h3>
                                               <i class="icon-settings font-dark"></i> Plantas 0 semanas
                                            </h3>
                                            <small>Hoja mas vieja libre de quema mayor a 5%</small>
                                        </span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="label_hoja_vieja_mayor_5" class="label_chart"> </div>
                                    <div id="hoja_vieja_mayor_5" class="chart"> </div>
                                </div>
                                <div class="portlet-footer">
                                    <div class="miniature">
                                        <ul id="check_hoja_vieja_mayor_5" class="listado">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                        <div class="col-md-6 hide">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Datos 
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                                    </div>
                                </div>
                                <div class="portlet-body" style="display: none;">
                                    <div class="caption">
                                        <span class="caption-subject font-dark sbold uppercase">
                                            <h3>
                                               <i class="icon-settings font-dark"></i> Plantas 0 semanas
                                            </h3>
                                            <small>Libre de cirugia</small>
                                        </span>
                                    </div>
                                    <div class="table-scrollable"> 
                                        <table class="table table-striped table-bordered table-hover textTittle">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th class="textTittle" colspan="{{semanas.length}}"> Semanas </th>
                                                </tr>
                                                <tr>
                                                    <th>Foco</th>
                                                    <th ng-repeat="(key, value) in semanas">{{value}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="(key,value) in charts.libre_cirugia">
                                                    <td class="textTittle">{{key}}</td>
                                                    <td class="textTittle" ng-repeat="(llave, valor) in semanas">
                                                        {{value[valor] | number : 1}}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 hide">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Datos 
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                                    </div>
                                </div>
                                <div class="portlet-body" style="display: none;">
                                    <div class="caption">
                                        <span class="caption-subject font-dark sbold uppercase">
                                            <h3>
                                               <i class="icon-settings font-dark"></i> Plantas 0 semanas
                                            </h3>
                                            <small>Hoja mas vieja libre de quema mayor a 5%</small>
                                        </span>
                                    </div>
                                    <div class="table-scrollable"> 
                                        <table class="table table-striped table-bordered table-hover textTittle">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th class="textTittle" colspan="{{semanas.length}}"> Semanas </th>
                                                </tr>
                                                <tr>
                                                    <th>Foco</th>
                                                    <th ng-repeat="(key, value) in semanas">{{value}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="(key,value) in charts.hoja_vieja_mayor_5">
                                                    <td class="textTittle">{{key}}</td>
                                                    <td class="textTittle" ng-repeat="(llave, valor) in semanas">
                                                        {{value[valor] | number : 1}}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->