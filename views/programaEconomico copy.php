<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<style>
.cursor th, .cursor td {
    cursor: pointer;
    /* NO SELECCIONABLE */
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

.center-th {
    text-align : center;
}
.right-th {
    text-align : right;
}
th, td {
    min-width: 80px;
}

/* OCULTAR COLUMNAS */
.checkbox {
    width : 110px;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .6em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.labelFilters {
    width: 150px;
    padding-top: 9px;
}

.checkbox label {
    font-size: 12px;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none !important;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
</style>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=$cdn?>global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
            <div ng-app="app" ng-controller="programa">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>PROGRAMA ECONOMICO
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="resumenCiclos">Inicio</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Listado</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                   <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">PROGRAMA ECONOMICO <small>({{ search.FINCA }})</small></span>
                                    </div>
                                    <div class="actions col-md-12">
                                        <div style="float:left;">
                                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                                    Columnas <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu main pull-left">
                                                    
                                                </ul>
                                                <input id="dateFilter" type="checkbox" class="make-switch" data-on-text="&nbsp;Estimada&nbsp;" data-off-text="&nbsp;Real&nbsp;">
                                            </div>
                                        </div>
                                        <div style="float:right;">
                                            <div class="col-md-6">
                                                <select name="fecha" class="form-control" id="fecha">
                                                    <option value="2017">2017</option>
                                                    <option value="2016">2016</option>
                                                    <option value="2015">2015</option>
                                                    <option value="2014">2014</option>
                                                    <option value="2013">2013</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <?php
                                                    if(Session::getInstance()->logged == 29){
                                                ?>
                                                    <select name="fincas" class="form-control" id="fincas" ng-model="search.FINCA">
                                                        <option ng-repeat="(key, value) in fincas" value="{{ key }}">{{ value }}</option>
                                                    </select>
                                                <?
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container table-scrollable">
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                            <thead>
                                                <tr role="row" class="heading cursor">
                                                    <th width="10%" ng-click="changeSort('search', 'CICLO')"> CICLO </th>
                                                    <th width="10%" ng-click="changeSort('search', 'HA')"> HA </th>
                                                    <th width="10%" ng-click="changeSort('search', 'FECHA_REAL')"> FECHA_REAL  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'FUNGICIDA_1')"> FUNGICIDA 1  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'HA_1')"> $HA 1  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'FUNGICIDA_2')"> FUNGICIDA 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'HA_2')"> $HA 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'COADYUVANTE_1')"> COADYUVANTE 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'HA_5')"> $HA 5 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ACEITE')"> ACEITE </th>
                                                    <th width="10%" ng-click="changeSort('search', 'HA_7')"> $HA 7 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'Ha_OPER')"> $/HA OPER </th>
                                                    <th width="10%" ng-click="changeSort('search', 'COSTO_Ha')"> $COSTO/HA </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="row in table | filter : { FINCA : search.FINCA } | orderObjectBy : search.orderBy : search.reverse">
                                                    <td>{{ row.CICLO }}</td>
                                                    <td>{{ row.HA }}</td>
                                                    <td>{{ row.FECHA_REAL }}</td>
                                                    <td>{{ row.FUNGICIDA_1 }}</td>
                                                    <td class="right-th">{{ row.Ha_1 }}</td>
                                                    <td>{{ row.FUNGICIDA_2 }}</td>
                                                    <td class="right-th">{{ row.Ha_2 }}</td>
                                                    <td>{{ row.COADYUVANTE_1 }}</td>
                                                    <td class="right-th">{{ row.Ha_5 }}</td>
                                                    <td>{{ row.ACEITE }}</td>
                                                    <td class="right-th">{{ row.Ha_7 }}</td>
                                                    <td class="right-th">{{ row.Ha_OPER }}</td>
                                                    <td class="right-th">{{ row.COSTO_Ha }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Programa Estimado <small>({{ search.FINCA }})</small></span>
                                    </div>
                                    <div class="actions">
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container table-scrollable">
                                        <div class="btn-group btn-group-devided hide" data-toggle="buttons">
                                            <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                                Columnas <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu estimado pull-left">
                                                <li id="col_fecha" data-event="fecha" class="active">
                                                    <a href="javascript:;">Fecha</a>
                                                </li>
                                                <li id="col_semana" data-event="semana" class="active">
                                                    <a href="javascript:;">Semana</a>
                                                </li>
                                                <li id="col_ciclo" data-event="ciclo" class="active">
                                                    <a href="javascript:;">Ciclo</a>
                                                </li>
                                                <li id="col_compañia" data-event="compania_de_aplicacion">
                                                    <a href="javascript:;">Compañia de Aplicación</a>
                                                </li>
                                                <li id="col_equipo" data-event="equipo_aplicacion">
                                                    <a href="javascript:;">Equipo de Aplicación</a>
                                                </li>
                                                <li id="col_costo_aplicacion" data-event="costo_app_ha">
                                                    <a href="javascript:;">Costo de Aplicación por Ha</a>
                                                </li>
                                                <li id="col_costo_total" data-event="costo_total_app">
                                                    <a href="javascript:;">Costo total de Aplicación</a>
                                                </li>
                                                <li id="col_frecuencia" data-event="frecuencia" class="active">
                                                    <a href="javascript:;">Frecuencia</a>
                                                </li>
                                                <li id="col_fungicida_1" data-event="producto1" class="active">
                                                    <a href="javascript:;">Fungicida 1</a>
                                                </li>
                                                <li id="col_dosis_1" data-event="dosis1" class="active">
                                                    <a href="javascript:;">Dosis 1</a>
                                                </li>
                                                <li id="col_fungicida_2" data-event="producto2" class="active">
                                                    <a href="javascript:;">Fungicida 2</a>
                                                </li>
                                                <li id="col_dosis_2" data-event="dosis2" class="active">
                                                    <a href="javascript:;">Dosis 2</a>
                                                </li>
                                                <li id="col_area" data-event="area_app" class="active">
                                                    <a href="javascript:;">Área</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_estimado">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="10%"> ID </th>
                                                    <th width="10%"> Fecha </th>
                                                    <th width="10%"> Sem  </th>
                                                    <th width="10%"> Ciclo  </th>
                                                    <th width="10%"> Frec  </th>
                                                    <th width="10%"> Fungicida 1 </th>
                                                    <th width="10%"> Fungicida 2 </th>
                                                </tr>
                                                <tr role="row" class="filter hide">
                                                    <td></td>
                                                    <td>
                                                        <input class="form-control form-filter input-sm" type="hidden" value="Estimado" name="search_tipo"/>
                                                        <input class="form-control form-filter input-sm" type="hidden" vaelue="2017" id="search_fecha_estimado" name="search_fecha"/>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Programa Real <small>({{ search.FINCA }})</small></span>
                                    </div>
                                    <div class="actions">
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container table-scrollable">
                                        <div class="btn-group btn-group-devided hide" data-toggle="buttons">
                                            <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                                Columnas <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu real pull-left">
                                                
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_real">
                                            <thead>
                                                <tr role="row" class="heading cursor">
                                                    <th width="10%" ng-click="changeSort('search_real', 'CICLO')"> CICLO </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'FUNGICIDA_1')"> FUNGICIDA 1 </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'Ha_1')"> HA 1 </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'FUNGICIDA_2')"> FUNGICIDA 2 </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'Ha_2')"> HA 2 </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'Ha_COCTEL')"> HA COCTEL </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'COSTO_Ha')"> COSTO/HA </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="row in table_real | filter : { FINCA : search.FINCA } | orderObjectBy : search_real.orderBy : search_real.reverse">
                                                    <td>{{ row.CICLO }}</td>
                                                    <td>{{ row.FUNGICIDA_1 }}</td>
                                                    <td class="right-th">{{ row.Ha_1 }}</td>
                                                    <td>{{ row.FUNGICIDA_2 }}</td>
                                                    <td class="right-th">{{ row.Ha_2 }}</td>
                                                    <td class="right-th">{{ row.Ha_COCTEL | number : 2 }}</td>
                                                    <td class="right-th">{{ row.COSTO_Ha }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>