
<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<style>

.cursor th, .cursor td {
    cursor: pointer;
    /* NO SELECCIONABLE */
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.right-th {
    text-align : right;
}

/* OCULTAR COLUMNAS */
.checkbox {
    width : 110px;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .6em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.labelFilters {
    width: 150px;
    padding-top: 9px;
}

.checkbox label {
    font-size: 12px;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none !important;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}

.textRigth {
    text-align : right;
}

.textCenter {
    text-align : center;
}
</style>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=$cdn?>global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
            <div ng-app="app" ng-controller="programa">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>PROGRAMA ECONOMICO
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <div class="page-toolbar">
                            <div style="float:right">
                                <div class="col-md-3">
                                    <select name="tipoHectarea" class="form-control" id="tipoHectarea"  ng-model="filters.tipoHectarea" ng-change="changeFinca()">
                                        <option ng-repeat="(key, value) in tipoHectarea" ng-selected="filters.tipoHectarea == key" value="{{key}}">{{ value }}</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select name="year" class="form-control" id="year"  ng-model="filters.year" ng-change="changeYear()">
                                        <option ng-repeat="(key , value) in years" ng-selected="filters.year == value" value="{{value}}">{{value}}</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select name="gerente" class="form-control" id="gerente" ng-model="filters.gerente" ng-change="changeGerente()"  ng-options="item.id as item.label for item in gerentes">
                                        <!--<option ng-repeat="(key , value) in gerentes" ng-selected="filters.gerente == key" value="{{key}}">{{value}}</option>-->
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select name="finca" class="form-control" id="finca" ng-model="filters.finca" ng-change="changeFinca()" ng-options="item.id as item.label for item in fincas[filters.gerente]">
                                        <!--<option ng-repeat="(key, value) in fincas[filters.gerente]" ng-selected="filters.finca == label" value="{{ value.id }}">{{ value.label }}</option>-->
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="resumenCiclos">Inicio</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Listado</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                   <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">PROGRAMA ECONOMICO</span>
                                    </div>
                                    <div class="actions col-md-12">
                                        <div style="float:left;">
                                            <div class="btn-group">
                                                <a class="btn blue-ebonyclay btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                                    Columnas <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li ng-repeat="(key , value) in columnas" ng-class="columnas[key] ? 'active' : ''">
                                                        <a ng-click="columnas[key] = !columnas[key]">{{columnasText[key]}}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="btn-group pull-right">
                                            <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                                Exportar <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;" ng-click="exportPrint('datatable_ajax_1')"> Imprimir </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;" ng-click="exportExcel('datatable_ajax_1')">Excel</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container table-scrollable">
                                        <table class="table table-striped table-bordered table-hover table-checkable">
                                            <thead>
                                                <tr>
                                                    <th class="textCenter">ID</th>
                                                    <th class="textCenter">CICLO</th>
                                                    <th class="textCenter">SEM</th>
                                                    <th class="textCenter">FECHA</th>
                                                    <th class="textCenter">FREC</th>
                                                    <th class="textCenter">Ha</th>
                                                    <th class="textRigth">$ PROD</th>
                                                    <th class="textRigth">$ PROD HA</th>
                                                    <th class="textRigth">$ OPER</th>
                                                    <th class="textRigth">$ OPER HA</th>
                                                    <th class="textRigth">$ CICLO</th>
                                                    <th class="textRigth">$ CICLO HA</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start="row in table | orderObjectBy : search.orderBy : search.reverse" ng-click="row.expanded = !row.expanded">
                                                    <td class="textCenter">{{ row.id }}</td>
                                                    <td class="textCenter">{{ row.ciclo }}</td>
                                                    <td class="textCenter">{{ row.sem }}</td>
                                                    <td class="textCenter">{{ row.fecha_real }}</td>
                                                    <td class="textCenter">{{ row.frec }}</td>
                                                    <td class="textCenter">{{ row.ha }}</td>
                                                    <td class="textRigth">{{ row.total_prod = (row.detalle | sumOfValue : 'prod') | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.detalle | sumOfValue : 'prod_ha' | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.oper | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.ha_oper | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_ciclo = ( (row.oper | num) + (row.total_prod | num) ) | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_ciclo / row.ha | number: 2 }}</td>
                                                </tr>
                                                <tr ng-show="row.expanded">
                                                    <th class="textCenter" colspan="2">PROD</th>
                                                    <th class="textCenter">TIPO</th>
                                                    <th class="textCenter">DOSIS</th>
                                                    <th class="textCenter">CANT</th>
                                                    <th class="textCenter">PRECIO</th>
                                                    <th class="textCenter">$ PROD</th>
                                                    <th class="textCenter">$ PROD HA</th>
                                                </tr>
                                                <tr ng-show="row.expanded" ng-repeat-end="row" ng-repeat="prod in row.detalle">
                                                    <td class="textCenter" colspan="2">{{ prod.nombre_comercial | uppercase }}</td>
                                                    <td class="textCenter">{{ prod.tipo | uppercase }}</td>
                                                    <td class="textCenter">{{ prod.dosis }}</td>
                                                    <td class="textCenter">{{ prod.cantidad }}</td>
                                                    <td class="textCenter">{{ prod.precio }}</td>
                                                    <td class="textCenter">{{ prod.prod | number: 2 }}</td>
                                                    <td class="textCenter">{{ prod.prod_ha | number: 2 }}</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="textCenter">{{ table | avgOfValue:'ha' | number: 2 }}</td>
                                                    <td class="textRigth">{{ table | sumOfValue:'total_prod' | number: 2 }}</td>
                                                    <td class="textRigth">{{ ((table | sumOfValue:'total_prod') / (table | sumOfValue:'ha')) | number:2 }}</td>
                                                    <td class="textRigth">{{ table | sumOfValue:'oper' | number: 2 }}</td>
                                                    <td class="textRigth">{{ ((table | sumOfValue:'oper') / (table | sumOfValue:'ha')) | number:2 }}</td>
                                                    <td class="textRigth">{{ table | sumOfValue:'total_ciclo' | number: 2 }}</td>
                                                    <td class="textRigth">{{ ((table | sumOfValue:'total_ciclo') / (table | sumOfValue:'ha')) | number:2 }}</td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="portlet-body hide">
                                    <div class="table-container table-scrollable">
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_1">
                                            <thead>
                                                <tr class="cursor">
                                                    <th width="10%" ng-click="changeSort('search', 'id')"> ID </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ciclo')"> CICLO  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha')"> HA  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'fecha_real')"> FECHA PROG  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'fecha_real')"> FECHA REAL </th>
                                                    <th width="10%" ng-click="changeSort('search', 'sem')"> SEM </th>
                                                    <th width="10%" ng-click="changeSort('search', 'atraco')"> ATRASO </th>
                                                    <th width="10%" ng-click="changeSort('search', 'motivo')"> MOTIVO </th>
                                                    <th width="10%" ng-click="changeSort('search', 'fungicida_1')"> FUNGICIDA 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'precio_1')" > PRECIO 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_1')" > $ HA 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'total_1')" > $ TOTAL 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'fungicida_2')"> FUNGICIDA 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'precio_2')" > PRECIO 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_2')" > $ HA 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'total_2')" > $ TOTAL 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'coadyuvante_1')" > COADYUVANTE 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'precio_5')" > PRECIO </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_5')" > $ HA </th>
                                                    <th width="10%" ng-click="changeSort('search', 'total_5')" > $ TOTAL </th>
                                                    <th width="10%" ng-click="changeSort('search', 'aceite')" > ACEITE </th>
                                                    <th width="10%" ng-click="changeSort('search', 'precio_7')" > PRECIO </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_7')" > $ HA </th>
                                                    <th width="10%" ng-click="changeSort('search', 'total_7')" > $ TOTAL </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_oper')" > $/HA OPER </th>
                                                    <th width="10%" ng-click="changeSort('search', 'oper')" > OPER </th>
                                                    <th width="10%" ng-click="changeSort('search', 'costo_total')" > $ COSTO TOTAL </th>
                                                    <th width="10%" ng-click="changeSort('search', 'costo_ha')" > $COSTO/HA </th>
                                                </tr>
                                            </thead>
                                            <tbody> 
                                                <tr ng-repeat="row in table | orderObjectBy : search.orderBy : search.reverse">  
                                                    <td>{{ row.id }}</td>
                                                    <td>{{ row.ciclo }}</td>
                                                    <td >{{ row.ha }}</td>
                                                    <td>{{ row.fecha_prog }}</td>
                                                    <td>{{ row.fecha_real }}</td>
                                                    <td>{{ row.sem }}</td>
                                                    <td>{{ row.atraso }}</td>
                                                    <td>{{ row.motivo }}</td>
                                                    <td>{{ row.fungicida_1 }}</td>
                                                    <td class="textRigth" ng-if="row.precio_1 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.precio_1 > 0">{{ row.precio_1 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_1 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_1 > 0">{{ row.ha_1  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.total_1 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.total_1 > 0">{{ row.total_1  | number : 2 }}</td>
                                                    <td>{{ row.fungicida_2 }}</td>
                                                    <td class="textRigth" ng-if="row.precio_2 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.precio_2 > 0">{{ row.precio_2  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_2 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_2 > 0">{{ row.ha_2  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.total_2 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.total_2 > 0">{{ row.total_2  | number : 2 }}</td>
                                                    <td>{{ row.coadyuvante_1 }}</td>
                                                    <td class="textRigth" ng-if="row.precio_5 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.precio_5 > 0">{{ row.precio_5  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_5 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_5 > 0">{{ row.ha_5  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.total_5 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.total_5 > 0">{{ row.total_5  | number : 2 }}</td>
                                                    <td>{{ row.aceite }}</td>
                                                    <td class="textRigth" ng-if="row.precio_7 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.precio_7 > 0">{{ row.precio_7  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_7 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_7 > 0">{{ row.ha_7  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.total_7 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.total_7 > 0">{{ row.total_7  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_oper <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_oper > 0">{{ row.ha_oper  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.oper <= 0"></td>
                                                    <td class="textRigth" ng-if="row.oper > 0">{{ row.oper  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.costo_total <= 0"></td>
                                                    <td class="textRigth" ng-if="row.costo_total > 0">{{ row.costo_total  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.costo_ha <= 0"></td>
                                                    <td class="textRigth" ng-if="row.costo_ha > 0">{{ row.costo_ha  | number : 2 }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Parciales</span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group pull-right">
                                            <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                                Exportar <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;" ng-click="exportPrint('datatable_ajax_parcial')"> Imprimir </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;" ng-click="exportExcel('datatable_ajax_parcial')">Excel</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container table-scrollable">
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_parcial">
                                            <thead>
                                                <tr>
                                                    <th class="textCenter">ID</th>
                                                    <th class="textCenter">CICLO</th>
                                                    <th class="textCenter">SEM</th>
                                                    <th class="textCenter">FECHA</th>
                                                    <th class="textCenter">FREC</th>
                                                    <th class="textCenter">Ha</th>
                                                    <th class="textRigth">$ PROD</th>
                                                    <th class="textRigth">$ PROD HA</th>
                                                    <th class="textRigth">$ OPER</th>
                                                    <th class="textRigth">$ OPER HA</th>
                                                    <th class="textRigth">$ CICLO</th>
                                                    <th class="textRigth">$ CICLO HA</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start="row in table_parcial | orderObjectBy : search.orderBy : search.reverse" ng-click="row.expanded = !row.expanded">
                                                    <td class="textCenter">{{ row.id }}</td>
                                                    <td class="textCenter">{{ row.ciclo }}</td>
                                                    <td class="textCenter">{{ row.sem }}</td>
                                                    <td class="textCenter">{{ row.fecha_real }}</td>
                                                    <td class="textCenter">{{ row.frec }}</td>
                                                    <td class="textCenter">{{ row.ha }}</td>
                                                    <td class="textRigth">{{ row.total_prod = (row.detalle | sumOfValue : 'prod') | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_prod_ha = (row.detalle | sumOfValue : 'prod_ha') | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.oper | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.ha_oper | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_ciclo = (sum(row.oper, row.total_prod)) | number: 2 }}</td>
                                                    <td class="textRigth">{{ row.total_ciclo / row.ha | number: 2 }}</td>
                                                </tr>
                                                <tr ng-show="row.expanded">
                                                    <th class="textCenter" colspan="2">PROD</th>
                                                    <th class="textCenter">TIPO</th>
                                                    <th class="textCenter">DOSIS</th>
                                                    <th class="textCenter">CANT</th>
                                                    <th class="textCenter">PRECIO</th>
                                                    <th class="textCenter">$ PROD</th>
                                                    <th class="textCenter">$ PROD HA</th>
                                                </tr>
                                                <tr ng-show="row.expanded" ng-repeat-end="row" ng-repeat="prod in row.detalle">
                                                    <td class="textCenter" colspan="2">{{ prod.nombre_comercial | uppercase }}</td>
                                                    <td class="textCenter">{{ prod.tipo | uppercase }}</td>
                                                    <td class="textCenter">{{ prod.dosis }}</td>
                                                    <td class="textCenter">{{ prod.cantidad }}</td>
                                                    <td class="textCenter">{{ prod.precio }}</td>
                                                    <td class="textCenter">{{ prod.prod | number: 2 }}</td>
                                                    <td class="textCenter">{{ prod.prod_ha | number: 2 }}</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <td class="textCenter">{{ table_parcial | sumOfValue:'ciclo' }}</td>
                                                    <td ></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="textCenter">{{ table_parcial | avgOfValue:'ha' | number: 2 }}</td>
                                                    <td class="textRigth">{{ table_parcial | sumOfValue:'total_prod' | number: 2 }}</td>
                                                    <td class="textRigth">{{ ((table_parcial | sumOfValue:'total_prod') / (table_parcial | sumOfValue:'ha')) | number: 2 }}</td>
                                                    <td class="textRigth">{{ table_parcial | sumOfValue:'oper' | number: 2 }}</td>
                                                    <td class="textRigth">{{ ((table_parcial | sumOfValue:'oper') / (table_parcial | sumOfValue:'ha')) | number: 2 }}</td>
                                                    <td class="textRigth">{{ table_parcial | sumOfValue:'total_ciclo' | number: 2 }}</td>
                                                    <td class="textRigth">{{ ((table_parcial | sumOfValue:'total_ciclo') / (table_parcial | sumOfValue:'ha')) | number: 2 }}</td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="portlet-body hide">
                                    <div class="table-container table-scrollable">
                                        <div class="btn-group btn-group-devided hide" data-toggle="buttons">
                                            <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                                Columnas <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu parcial pull-left">
                                                <li id="col_fecha" data-event="fecha" class="active">
                                                    <a href="javascript:;">Fecha</a>
                                                </li>
                                                <li id="col_semana" data-event="semana" class="active">
                                                    <a href="javascript:;">Semana</a>
                                                </li>
                                                <li id="col_ciclo" data-event="ciclo" class="active">
                                                    <a href="javascript:;">Ciclo</a>
                                                </li>
                                                <li id="col_compañia" data-event="compania_de_aplicacion">
                                                    <a href="javascript:;">Compañia de Aplicación</a>
                                                </li>
                                                <li id="col_equipo" data-event="equipo_aplicacion">
                                                    <a href="javascript:;">Equipo de Aplicación</a>
                                                </li>
                                                <li id="col_costo_aplicacion" data-event="costo_app_ha">
                                                    <a href="javascript:;">Costo de Aplicación por Ha</a>
                                                </li>
                                                <li id="col_costo_total" data-event="costo_total_app">
                                                    <a href="javascript:;">Costo total de Aplicación</a>
                                                </li>
                                                <li id="col_frecuencia" data-event="frecuencia" class="active">
                                                    <a href="javascript:;">Frecuencia</a>
                                                </li>
                                                <li id="col_fungicida_1" data-event="producto1" class="active">
                                                    <a href="javascript:;">Fungicida 1</a>
                                                </li>
                                                <li id="col_dosis_1" data-event="dosis1" class="active">
                                                    <a href="javascript:;">Dosis 1</a>
                                                </li>
                                                <li id="col_fungicida_2" data-event="producto2" class="active">
                                                    <a href="javascript:;">Fungicida 2</a>
                                                </li>
                                                <li id="col_dosis_2" data-event="dosis2" class="active">
                                                    <a href="javascript:;">Dosis 2</a>
                                                </li>
                                                <li id="col_area" data-event="area_app" class="active">
                                                    <a href="javascript:;">Área</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_parcial">
                                            <thead>
                                                <tr class="cursor">
                                                    <th width="10%" ng-click="changeSort('search', 'id')"> ID </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ciclo')"> CICLO  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha')"> HA  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'fecha_real')"> FECHA PROG  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'fecha_real')"> FECHA REAL </th>
                                                    <th width="10%" ng-click="changeSort('search', 'sem')"> SEM </th>
                                                    <th width="10%" ng-click="changeSort('search', 'atraco')"> ATRASO </th>
                                                    <th width="10%" ng-click="changeSort('search', 'motivo')"> MOTIVO </th>
                                                    <th width="10%" ng-click="changeSort('search', 'fungicida_1')"> FUNGICIDA 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'precio_1')" > PRECIO 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_1')" > $ HA 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'total_1')" > $ TOTAL 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'fungicida_2')"> FUNGICIDA 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'precio_2')" > PRECIO 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_2')" > $ HA 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'total_2')" > $ TOTAL 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'coadyuvante_1')" > COADYUVANTE 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'precio_5')" > PRECIO </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_5')" > $ HA </th>
                                                    <th width="10%" ng-click="changeSort('search', 'total_5')" > $ TOTAL </th>
                                                    <th width="10%" ng-click="changeSort('search', 'aceite')" > ACEITE </th>
                                                    <th width="10%" ng-click="changeSort('search', 'precio_7')" > PRECIO </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_7')" > $ HA </th>
                                                    <th width="10%" ng-click="changeSort('search', 'total_7')" > $ TOTAL </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ha_oper')" > $/HA OPER </th>
                                                    <th width="10%" ng-click="changeSort('search', 'oper')" > OPER </th>
                                                    <th width="10%" ng-click="changeSort('search', 'costo_total')" > $ COSTO TOTAL </th>
                                                    <th width="10%" ng-click="changeSort('search', 'costo_ha')" > $COSTO/HA </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="row in table_parcial | orderObjectBy : search.orderBy : search.reverse">  
                                                    <td>{{ row.id }}</td>
                                                    <td>{{ row.ciclo }}</td>
                                                    <td >{{ row.ha }}</td>
                                                    <td>{{ row.fecha_prog }}</td>
                                                    <td>{{ row.fecha_real }}</td>
                                                    <td>{{ row.sem }}</td>
                                                    <td>{{ row.atraso }}</td>
                                                    <td>{{ row.motivo }}</td>
                                                    <td>{{ row.fungicida_1 }}</td>
                                                    <td class="textRigth" ng-if="row.precio_1 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.precio_1 > 0">{{ row.precio_1 | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_1 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_1 > 0">{{ row.ha_1  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.total_1 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.total_1 > 0">{{ row.total_1  | number : 2 }}</td>
                                                    <td>{{ row.fungicida_2 }}</td>
                                                    <td class="textRigth" ng-if="row.precio_2 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.precio_2 > 0">{{ row.precio_2  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_2 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_2 > 0">{{ row.ha_2  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.total_2 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.total_2 > 0">{{ row.total_2  | number : 2 }}</td>
                                                    <td>{{ row.coadyuvante_1 }}</td>
                                                    <td class="textRigth" ng-if="row.precio_5 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.precio_5 > 0">{{ row.precio_5  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_5 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_5 > 0">{{ row.ha_5  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.total_5 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.total_5 > 0">{{ row.total_5  | number : 2 }}</td>
                                                    <td>{{ row.aceite }}</td>
                                                    <td class="textRigth" ng-if="row.precio_7 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.precio_7 > 0">{{ row.precio_7  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_7 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_7 > 0">{{ row.ha_7  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.total_7 <= 0"></td>
                                                    <td class="textRigth" ng-if="row.total_7 > 0">{{ row.total_7  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.ha_oper <= 0"></td>
                                                    <td class="textRigth" ng-if="row.ha_oper > 0">{{ row.ha_oper  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.oper <= 0"></td>
                                                    <td class="textRigth" ng-if="row.oper > 0">{{ row.oper  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.costo_total <= 0"></td>
                                                    <td class="textRigth" ng-if="row.costo_total > 0">{{ row.costo_total  | number : 2 }}</td>
                                                    <td class="textRigth" ng-if="row.costo_ha <= 0"></td>
                                                    <td class="textRigth" ng-if="row.costo_ha > 0">{{ row.costo_ha  | number : 2 }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide">
                        <div class="col-md-6">
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Programa Estimado </span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group pull-right">
                                            <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                                Exportar <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;" ng-click="exportPrint('datatable_ajax_estimado')"> Imprimir </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;" ng-click="exportExcel('datatable_ajax_estimado')">Excel</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container table-scrollable">
                                        <div class="btn-group btn-group-devided hide" data-toggle="buttons">
                                            <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                                Columnas <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu estimado pull-left">
                                                <li id="col_fecha" data-event="fecha" class="active">
                                                    <a href="javascript:;">Fecha</a>
                                                </li>
                                                <li id="col_semana" data-event="semana" class="active">
                                                    <a href="javascript:;">Semana</a>
                                                </li>
                                                <li id="col_ciclo" data-event="ciclo" class="active">
                                                    <a href="javascript:;">Ciclo</a>
                                                </li>
                                                <li id="col_compañia" data-event="compania_de_aplicacion">
                                                    <a href="javascript:;">Compañia de Aplicación</a>
                                                </li>
                                                <li id="col_equipo" data-event="equipo_aplicacion">
                                                    <a href="javascript:;">Equipo de Aplicación</a>
                                                </li>
                                                <li id="col_costo_aplicacion" data-event="costo_app_ha">
                                                    <a href="javascript:;">Costo de Aplicación por Ha</a>
                                                </li>
                                                <li id="col_costo_total" data-event="costo_total_app">
                                                    <a href="javascript:;">Costo total de Aplicación</a>
                                                </li>
                                                <li id="col_frecuencia" data-event="frecuencia" class="active">
                                                    <a href="javascript:;">Frecuencia</a>
                                                </li>
                                                <li id="col_fungicida_1" data-event="producto1" class="active">
                                                    <a href="javascript:;">Fungicida 1</a>
                                                </li>
                                                <li id="col_dosis_1" data-event="dosis1" class="active">
                                                    <a href="javascript:;">Dosis 1</a>
                                                </li>
                                                <li id="col_fungicida_2" data-event="producto2" class="active">
                                                    <a href="javascript:;">Fungicida 2</a>
                                                </li>
                                                <li id="col_dosis_2" data-event="dosis2" class="active">
                                                    <a href="javascript:;">Dosis 2</a>
                                                </li>
                                                <li id="col_area" data-event="area_app" class="active">
                                                    <a href="javascript:;">Área</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_estimado">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="10%"> ID </th>
                                                    <th width="10%"> Fecha </th>
                                                    <th width="10%"> Sem  </th>
                                                    <th width="10%"> Ciclo  </th>
                                                    <th width="10%"> Frec  </th>
                                                    <th width="10%"> Fungicida 1 </th>
                                                    <th width="10%"> Fungicida 2 </th>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Programa Real </span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group pull-right">
                                            <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                                Exportar <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;" ng-click="exportPrint('datatable_ajax_real')"> Imprimir </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;" ng-click="exportExcel('datatable_ajax_real')">Excel</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container table-scrollable">
                                        <div class="btn-group btn-group-devided hide" data-toggle="buttons">
                                            <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                                Columnas <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu real pull-left">
                                                
                                            </ul>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_real">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="10%" ng-click="changeSort('search_real', 'ciclo')"> CICLO </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'fungicida_1')"> FUNGICIDA 1 </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'Ha_1')"> HA 1 </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'fungicida_2')"> FUNGICIDA 2 </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'Ha_2')"> HA 2 </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'ha_coctel')"> HA COCTEL </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'costo_ha')"> COSTO/HA </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="row in table_real | filter : { finca : search.finca } | orderObjectBy : search_real.orderBy : search_real.reverse">
                                                    <td>{{ row.ciclo }}</td>
                                                    <td>{{ row.fungicida_1 }}</td>
                                                    <td class="textCenter" ng-if="row.ha_1 > 0">{{ row.ha_1 | number : 2 }}</td>
                                                    <td class="textCenter" ng-if="row.ha_1 <= 0"></td>
                                                    <td>{{ row.fungicida_2 }}</td>
                                                    <td class="textCenter" ng-if="row.ha_2 > 0">{{ row.ha_2 | number : 2 }}</td>
                                                    <td class="textCenter" ng-if="row.ha_2 <= 0"></td>
                                                    <td class="textCenter" ng-if="row.ha_coctel > 0">{{ row.ha_coctel | number : 2 }}</td>
                                                    <td class="textCenter" ng-if="row.ha_coctel <= 0"></td>
                                                    <td class="textCenter" ng-if="row.costo_ha > 0">{{ row.costo_ha | number : 2 }}</td>
                                                    <td class="textCenter" ng-if="row.costo_ha <= 0"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>