<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<style>

.cursor th, .cursor td {
    cursor: pointer;
    /* NO SELECCIONABLE */
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.right-th {
    text-align : right;
}

/* OCULTAR COLUMNAS */
.checkbox {
    width : 110px;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .6em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.labelFilters {
    width: 150px;
    padding-top: 9px;
}

.checkbox label {
    font-size: 12px;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none !important;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
</style>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=$cdn?>global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
            <div ng-app="app" ng-controller="programa">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>PROGRAMA TECNICO
                                <small></small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="resumenCiclos">Inicio</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Listado</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                   <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">PROGRAMA TECNICO <small>({{ search.FINCA }})</small></span>
                                    </div>
                                    <div class="actions col-md-12">
                                        <div style="float:left;">
                                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                                    Columnas <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu main pull-left" id="columns_table_1">
                                                    <li>
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <td colspan="10">
                                                                        <span class="label label-default">MOSTRAR/OCULTAR COLUMNAS</span>
                                                                    </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="checkbox">
                                                                            <label ng-click="disableColumns(1, 1, $event)">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                ID
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox">
                                                                            <label ng-click="disableColumns(2, 1, $event)">
                                                                                <input type="checkbox" value="" >
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                FINCA
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(3, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                CICLO
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(4, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                HA
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(5, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                FECHA_PROG
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(6, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                FECHA_REAL
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(7, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                SEM
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(8, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                ATRASO
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(9, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                MOTIVO
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                    <li>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(10, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                FUNGICIDA_1
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(11, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                DOSIS 1
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(12, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                CANTIDAD 1
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(13, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $PRECIO 1
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(14, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $HA 1
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(15, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $TOTAL 1
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(16, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                FUNGICIDA 2
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(17, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                DOSIS 2
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(18, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                CANTIDAD 2
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                    <li>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(19, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                CANTIDAD 2
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(20, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon- hide"></i></span>
                                                                                $PRECIO 2
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(21, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $HA 2
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(22, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $TOTAL 2
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(23, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                BIOESTIMULANTES 1
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(24, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                DOSIS 3
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(25, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                CANTIDAD 3
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(26, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $PRECIO 3
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(27, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $HA 3
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                    <li>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(28, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $TOTAL 3
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(29, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                BIOESTIMULANTES 2
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(30, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                DOSIS 4
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(31, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                CANTIDAD 4
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(32, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $PRECIO 4
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(33, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $HA 4
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(34, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $TOTAL 4
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(35, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                COADYUVANTE 1
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(36, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                DOSIS 5
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                    <li>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(37, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                CANTIDAD 5
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(38, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $PRECIO 5
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(39, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $HA 5
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(40, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $TOTAL 5
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(41, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                COADYUVANTE 2
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(42, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                DOSIS 6
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(43, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                CANTIDAD 6
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(44, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $PRECIO 6
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(45, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $HA 6
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                    <li>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(46, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $TOTAL 6
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(47, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                ACEITE
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(48, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                DOSIS 7
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(49, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                CANTIDAD 7
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(50, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $PRECIO 7
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(51, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $HA 7
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(52, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $TOTAL 7
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(53, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                FOLIAR 1
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(54, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                DOSIS 8
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                    <li>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(55, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                CANTIDAD 8
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(56, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $PRECIO 8
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(57, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $HA 8
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(58, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $TOTAL 8
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(59, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                FOLIAR 2
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(60, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                DOSIS 9
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(61, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                CANTIDAD 9
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(62, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $PRECIO 9
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(63, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $HA 9
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                    <li>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(64, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $TOTAL 9
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(65, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                FOLIAR 3
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(66, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                DOSIS 10
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(67, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                CANTIDAD 10
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(68, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" >
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $PRECIO 10
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(69, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $HA 10
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(70, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" >
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $TOTAL 10
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(71, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                INSECTICIDA
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(72, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                DOSIS 11
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                    <li>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(73, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                CANTIDAD 11
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(74, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $PRECIO 11
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(75, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $HA 11
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(76, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $TOTAL 11
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(77, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                AGUA
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(78, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                DOSIS 12
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(79, 1, $event)" class="active">
                                                                                <input type="checkbox" value="" checked="checked">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                CANTIDAD 12
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(80, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $HA OPER
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(81, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                OPER
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                    <li>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(82, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $COSTO TOTAL
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="checkbox active">
                                                                            <label ng-click="disableColumns(83, 1, $event)" class="active">
                                                                                <input type="checkbox" value="">
                                                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok hide"></i></span>
                                                                                $COSTO HA
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </li>
                                                </ul>
                                            </div>
                                            <input id="dateFilter" type="checkbox" class="make-switch" data-on-text="&nbsp;Estimada&nbsp;" data-off-text="&nbsp;Real&nbsp;">
                                        </div>
                                        <div style="float:right">
                                            <div class="col-md-6">
                                                <select name="fecha" class="form-control" id="fecha">
                                                    <option value="2017">2017</option>
                                                    <option value="2016">2016</option>
                                                    <option value="2015">2015</option>
                                                    <option value="2014">2014</option>
                                                    <option value="2013">2013</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <?php
                                                    if(Session::getInstance()->logged == 29){
                                                ?>
                                                    <select name="fincas" class="form-control" id="fincas" ng-model="search.FINCA">
                                                        <option ng-repeat="(key, value) in fincas" value="{{ key }}">{{ value }}</option>
                                                    </select>
                                                <?
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container table-scrollable">
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_1">
                                            <thead>
                                                <tr class="cursor">
                                                    <th width="10%" ng-click="changeSort('search', 1)" class="{{ search.columns[0] ? '' : 'hide' }}"> ID </th>
                                                    <th width="10%" ng-click="changeSort('search', 2)" class="{{ search.columns[1] ? '' : 'hide' }}"> FINCA </th>
                                                    <th width="10%" ng-click="changeSort('search', 'CICLO')" class="{{ search.columns[2] ? '' : 'hide' }}"> CICLO  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'HA')" class="{{ search.columns[3] ? '' : 'hide' }}"> HA  </th>
                                                    <th width="10%" ng-click="changeSort('search', 5)" class="{{ search.columns[4] ? '' : 'hide' }}"> FECHA PROG  </th>
                                                    <th width="10%" ng-click="changeSort('search', 'FECHA_REAL')" class="{{ search.columns[5] ? '' : 'hide' }}"> FECHA REAL </th>
                                                    <th width="10%" ng-click="changeSort('search', 'SEM')" class="{{ search.columns[6] ? '' : 'hide' }}"> SEM </th>
                                                    <th width="10%" ng-click="changeSort('search', 8)" class="{{ search.columns[7] ? '' : 'hide' }}"> ATRASO </th>
                                                    <th width="10%" ng-click="changeSort('search', 9)" class="{{ search.columns[8] ? '' : 'hide' }}"> MOTIVO </th>
                                                    <th width="10%" ng-click="changeSort('search', 'FUNGICIDA_1')" class="{{ search.columns[9] ? '' : 'hide' }}"> FUNGICIDA 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'DOSIS_1')" class="{{ search.columns[10] ? '' : 'hide' }}"> DOSIS 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'CANTIDAD_1')" class="{{ search.columns[11] ? '' : 'hide' }}"> CANTIDAD 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 13)" class="{{ search.columns[12] ? '' : 'hide' }}"> $PRECIO 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 14)" class="{{ search.columns[13] ? '' : 'hide' }}"> $HA 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 15)" class="{{ search.columns[14] ? '' : 'hide' }}"> $TOTAL 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'FUNGICIDA_2')" class="{{ search.columns[15] ? '' : 'hide' }}"> FUNGICIDA 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'DOSIS_2')" class="{{ search.columns[16] ? '' : 'hide' }}"> DOSIS 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'CANTIDAD_2')" class="{{ search.columns[17] ? '' : 'hide' }}"> CANTIDAD 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 19)" class="{{ search.columns[18] ? '' : 'hide' }}"> $PRECIO 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 20)" class="{{ search.columns[19] ? '' : 'hide' }}"> $HA 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 21)" class="{{ search.columns[20] ? '' : 'hide' }}"> $TOTAL 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 22)" class="{{ search.columns[21] ? '' : 'hide' }}"> BIOESTIMULANTES 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 23)" class="{{ search.columns[22] ? '' : 'hide' }}"> DOSIS 3 </th>
                                                    <th width="10%" ng-click="changeSort('search', 24)" class="{{ search.columns[23] ? '' : 'hide' }}"> CANTIDAD 3 </th>
                                                    <th width="10%" ng-click="changeSort('search', 25)" class="{{ search.columns[24] ? '' : 'hide' }}"> $PRECIO 3 </th>
                                                    <th width="10%" ng-click="changeSort('search', 26)" class="{{ search.columns[25] ? '' : 'hide' }}"> $HA 3 </th>
                                                    <th width="10%" ng-click="changeSort('search', 27)" class="{{ search.columns[26] ? '' : 'hide' }}"> $TOTAL 3 </th>
                                                    <th width="10%" ng-click="changeSort('search', 28)" class="{{ search.columns[27] ? '' : 'hide' }}"> BIOESTIMULANTES 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 29)" class="{{ search.columns[28] ? '' : 'hide' }}"> DOSIS 4 </th>
                                                    <th width="10%" ng-click="changeSort('search', 30)" class="{{ search.columns[29] ? '' : 'hide' }}"> CANTIDAD 4 </th>
                                                    <th width="10%" ng-click="changeSort('search', 31)" class="{{ search.columns[30] ? '' : 'hide' }}"> $PRECIO 4 </th>
                                                    <th width="10%" ng-click="changeSort('search', 32)" class="{{ search.columns[31] ? '' : 'hide' }}"> $HA 4 </th>
                                                    <th width="10%" ng-click="changeSort('search', 33)" class="{{ search.columns[32] ? '' : 'hide' }}"> $TOTAL 4 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'COADYUVANTE_1')" class="{{ search.columns[33] ? '' : 'hide' }}"> COADYUVANTE 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'DOSIS_5')" class="{{ search.columns[34] ? '' : 'hide' }}"> DOSIS 5 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'CANTIDAD_5')" class="{{ search.columns[35] ? '' : 'hide' }}"> CANTIDAD 5 </th>
                                                    <th width="10%" ng-click="changeSort('search', 37)" class="{{ search.columns[36] ? '' : 'hide' }}"> $PRECIO 5 </th>
                                                    <th width="10%" ng-click="changeSort('search', 38)" class="{{ search.columns[37] ? '' : 'hide' }}"> $HA 5 </th>
                                                    <th width="10%" ng-click="changeSort('search', 39)" class="{{ search.columns[38] ? '' : 'hide' }}"> $TOTAL 5 </th>
                                                    <th width="10%" ng-click="changeSort('search', 40)" class="{{ search.columns[39] ? '' : 'hide' }}"> COADYUVANTE 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 41)" class="{{ search.columns[40] ? '' : 'hide' }}"> DOSIS 6 </th>
                                                    <th width="10%" ng-click="changeSort('search', 42)" class="{{ search.columns[41] ? '' : 'hide' }}"> CANTIDAD 6 </th>
                                                    <th width="10%" ng-click="changeSort('search', 43)" class="{{ search.columns[42] ? '' : 'hide' }}"> $PRECIO 6 </th>
                                                    <th width="10%" ng-click="changeSort('search', 44)" class="{{ search.columns[43] ? '' : 'hide' }}"> $HA 6 </th>
                                                    <th width="10%" ng-click="changeSort('search', 45)" class="{{ search.columns[44] ? '' : 'hide' }}"> $TOTAL 6 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'ACEITE')" class="{{ search.columns[45] ? '' : 'hide' }}"> ACEITE </th>
                                                    <th width="10%" ng-click="changeSort('search', 'DOSIS_7')" class="{{ search.columns[46] ? '' : 'hide' }}"> DOSIS 7 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'CANTIDAD_7')" class="{{ search.columns[47] ? '' : 'hide' }}"> CANTIDAD 7 </th>
                                                    <th width="10%" ng-click="changeSort('search', 49)" class="{{ search.columns[48] ? '' : 'hide' }}"> $PRECIO 7 </th>
                                                    <th width="10%" ng-click="changeSort('search', 50)" class="{{ search.columns[49] ? '' : 'hide' }}"> $HA 7 </th>
                                                    <th width="10%" ng-click="changeSort('search', 51)" class="{{ search.columns[50] ? '' : 'hide' }}"> $TOTAL 7 </th>
                                                    <th width="10%" ng-click="changeSort('search', 52)" class="{{ search.columns[51] ? '' : 'hide' }}"> FOLIAR 1 </th>
                                                    <th width="10%" ng-click="changeSort('search', 53)" class="{{ search.columns[52] ? '' : 'hide' }}"> DOSIS 8 </th>
                                                    <th width="10%" ng-click="changeSort('search', 54)" class="{{ search.columns[53] ? '' : 'hide' }}"> CANTIDAD 8 </th>
                                                    <th width="10%" ng-click="changeSort('search', 55)" class="{{ search.columns[54] ? '' : 'hide' }}"> $PRECIO 8 </th>
                                                    <th width="10%" ng-click="changeSort('search', 56)" class="{{ search.columns[55] ? '' : 'hide' }}"> $HA 8 </th>
                                                    <th width="10%" ng-click="changeSort('search', 57)" class="{{ search.columns[56] ? '' : 'hide' }}"> $TOTAL 8 </th>
                                                    <th width="10%" ng-click="changeSort('search', 58)" class="{{ search.columns[57] ? '' : 'hide' }}"> FOLIAR 2 </th>
                                                    <th width="10%" ng-click="changeSort('search', 59)" class="{{ search.columns[58] ? '' : 'hide' }}"> DOSIS 9 </th>
                                                    <th width="10%" ng-click="changeSort('search', 60)" class="{{ search.columns[59] ? '' : 'hide' }}"> CANTIDAD 9 </th>
                                                    <th width="10%" ng-click="changeSort('search', 61)" class="{{ search.columns[60] ? '' : 'hide' }}"> $PRECIO 9 </th>
                                                    <th width="10%" ng-click="changeSort('search', 62)" class="{{ search.columns[61] ? '' : 'hide' }}"> $HA 9 </th>
                                                    <th width="10%" ng-click="changeSort('search', 63)" class="{{ search.columns[62] ? '' : 'hide' }}"> $TOTAL 9 </th>
                                                    <th width="10%" ng-click="changeSort('search', 64)" class="{{ search.columns[63] ? '' : 'hide' }}"> FOLIAR 3 </th>
                                                    <th width="10%" ng-click="changeSort('search', 65)" class="{{ search.columns[64] ? '' : 'hide' }}"> DOSIS 10 </th>
                                                    <th width="10%" ng-click="changeSort('search', 66)" class="{{ search.columns[65] ? '' : 'hide' }}"> CANTIDAD 10 </th>
                                                    <th width="10%" ng-click="changeSort('search', 67)" class="{{ search.columns[66] ? '' : 'hide' }}"> $PRECIO 10 </th>
                                                    <th width="10%" ng-click="changeSort('search', 68)" class="{{ search.columns[67] ? '' : 'hide' }}"> $HA 10 </th>
                                                    <th width="10%" ng-click="changeSort('search', 69)" class="{{ search.columns[68] ? '' : 'hide' }}"> $TOTAL 10 </th>
                                                    <th width="10%" ng-click="changeSort('search', 70)" class="{{ search.columns[69] ? '' : 'hide' }}"> INSECTICIDA </th>
                                                    <th width="10%" ng-click="changeSort('search', 71)" class="{{ search.columns[70] ? '' : 'hide' }}"> DOSIS 11 </th>
                                                    <th width="10%" ng-click="changeSort('search', 72)" class="{{ search.columns[71] ? '' : 'hide' }}"> CANTIDAD 11 </th>
                                                    <th width="10%" ng-click="changeSort('search', 73)" class="{{ search.columns[72] ? '' : 'hide' }}"> $PRECIO 11 </th>
                                                    <th width="10%" ng-click="changeSort('search', 74)" class="{{ search.columns[73] ? '' : 'hide' }}"> $HA 11 </th>
                                                    <th width="10%" ng-click="changeSort('search', 75)" class="{{ search.columns[74] ? '' : 'hide' }}"> $TOTAL 11 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'AGUA')" class="{{ search.columns[75] ? '' : 'hide' }}"> AGUA </th>
                                                    <th width="10%" ng-click="changeSort('search', 'DOSIS_12')" class="{{ search.columns[76] ? '' : 'hide' }}"> DOSIS 12 </th>
                                                    <th width="10%" ng-click="changeSort('search', 'CANTIDAD_12')" class="{{ search.columns[77] ? '' : 'hide' }}"> CANTIDAD 12 </th>
                                                    <th width="10%" ng-click="changeSort('search', 79)" class="{{ search.columns[78] ? '' : 'hide' }}"> $/HA OPER </th>
                                                    <th width="10%" ng-click="changeSort('search', 80)" class="{{ search.columns[79] ? '' : 'hide' }}"> $OPER </th>
                                                    <th width="10%" ng-click="changeSort('search', 81)" class="{{ search.columns[80] ? '' : 'hide' }}"> $COSTO TOTAL </th>
                                                    <th width="10%" ng-click="changeSort('search', 82)" class="{{ search.columns[81] ? '' : 'hide' }}"> $COSTO/HA </th>
                                                </tr>
                                            </thead>
                                            <tbody> 
                                                <tr ng-repeat="row in table | filter : { finca: search.finca } | orderObjectBy : search.orderBy : search.reverse">  
                                                    <td class="{{ search.columns[0] ? '' : 'hide' }}">{{ row.id }}</td>
                                                    <td class="{{ search.columns[1] ? '' : 'hide' }}">{{ row.finca }}</td>
                                                    <td class="{{ search.columns[2] ? '' : 'hide' }}">{{ row.ciclo }}</td>
                                                    <td class="right-th {{ search.columns[3] ? '' : 'hide' }}">{{ row.ha }}</td>
                                                    <td class="{{ search.columns[4] ? '' : 'hide' }}">{{ row.fecha_prog }}</td>
                                                    <td class="{{ search.columns[5] ? '' : 'hide' }}">{{ row.fecha_real }}</td>
                                                    <td class="{{ search.columns[6] ? '' : 'hide' }}">{{ row.sem }}</td>
                                                    <td class="{{ search.columns[7] ? '' : 'hide' }}">{{ row.atraco }}</td>
                                                    <td class="{{ search.columns[8] ? '' : 'hide' }}">{{ row.motivo }}</td>
                                                    <td class="{{ search.columns[9] ? '' : 'hide' }}">{{ row.fungicida_1 }}</td>
                                                    <td class="{{ search.columns[10] ? '' : 'hide' }}">{{ row.dosis_1 }}</td>
                                                    <td class="{{ search.columns[11] ? '' : 'hide' }}">{{ row.cantidad_1 }}</td>
                                                    <td class="{{ search.columns[12] ? '' : 'hide' }}">{{ row.precio_1 }}</td>
                                                    <td class="{{ search.columns[13] ? '' : 'hide' }}">{{ row.HA_1 }}</td>
                                                    <td class="{{ search.columns[14] ? '' : 'hide' }}">{{ row.TOTAL_1 }}</td>
                                                    <td class="{{ search.columns[15] ? '' : 'hide' }}">{{ row.FUNGICIDA_2 }}</td>
                                                    <td class="{{ search.columns[16] ? '' : 'hide' }}">{{ row.DOSIS_2 }}</td>
                                                    <td class="{{ search.columns[17] ? '' : 'hide' }}">{{ row.CANTIDAD_2 }}</td>
                                                    <td class="{{ search.columns[18] ? '' : 'hide' }}">{{ row.PRECIO_2 }}</td>
                                                    <td class="{{ search.columns[19] ? '' : 'hide' }}">{{ row.HA_2 }}</td>
                                                    <td class="{{ search.columns[20] ? '' : 'hide' }}">{{ row.TOTAL_2 }}</td>
                                                    <td class="{{ search.columns[21] ? '' : 'hide' }}">{{ row.BIOESTIMULANTES_1 }}</td>
                                                    <td class="{{ search.columns[22] ? '' : 'hide' }}">{{ row.DOSIS_3 }}</td>
                                                    <td class="{{ search.columns[23] ? '' : 'hide' }}">{{ row.CANTIDAD_3 }}</td>
                                                    <td class="{{ search.columns[24] ? '' : 'hide' }}">{{ row.HA_3 }}</td>
                                                    <td class="{{ search.columns[25] ? '' : 'hide' }}">{{ row.TOTAL_3 }}</td>
                                                    <td class="{{ search.columns[26] ? '' : 'hide' }}">{{ row.BIOESTIMULANTES_2 }}</td>
                                                    <td class="{{ search.columns[27] ? '' : 'hide' }}">{{ row.DOSIS_4 }}</td>
                                                    <td class="{{ search.columns[28] ? '' : 'hide' }}">{{ row.CANTIDAD_4 }}</td>
                                                    <td class="{{ search.columns[29] ? '' : 'hide' }}">{{ row.PRECIO_4 }}</td>
                                                    <td class="{{ search.columns[30] ? '' : 'hide' }}">{{ row.HA_4 }}</td>
                                                    <td class="{{ search.columns[31] ? '' : 'hide' }}">{{ row.TOTAL_4 }}</td>
                                                    <td class="{{ search.columns[32] ? '' : 'hide' }}">{{ row.COADYUVANTE_1 }}</td>
                                                    <td class="{{ search.columns[33] ? '' : 'hide' }}">{{ row.DOSIS_5 }}</td>
                                                    <td class="{{ search.columns[34] ? '' : 'hide' }}">{{ row.CANTIDAD_5 }}</td>
                                                    <td class="{{ search.columns[35] ? '' : 'hide' }}">{{ row.PRECIO_5 }}</td>
                                                    <td class="{{ search.columns[36] ? '' : 'hide' }}">{{ row.HA_5 }}</td>
                                                    <td class="{{ search.columns[37] ? '' : 'hide' }}">{{ row.TOTAL_5 }}</td>
                                                    <td class="{{ search.columns[38] ? '' : 'hide' }}">{{ row.COADYUVANTE_2 }}</td>
                                                    <td class="{{ search.columns[39] ? '' : 'hide' }}">{{ row.DOSIS_6 }}</td>
                                                    <td class="{{ search.columns[40] ? '' : 'hide' }}">{{ row.CANTIDAD_6 }}</td>
                                                    <td class="{{ search.columns[41] ? '' : 'hide' }}">{{ row.PRECIO_6 }}</td>
                                                    <td class="{{ search.columns[42] ? '' : 'hide' }}">{{ row.HA_6 }}</td>
                                                    <td class="{{ search.columns[43] ? '' : 'hide' }}">{{ row.TOTAL_6 }}</td>
                                                    <td class="{{ search.columns[44] ? '' : 'hide' }}">{{ row.ACEITE }}</td>
                                                    <td class="{{ search.columns[45] ? '' : 'hide' }}">{{ row.DOSIS_7 }}</td>
                                                    <td class="{{ search.columns[46] ? '' : 'hide' }}">{{ row.CANTIDAD_7 }}</td>
                                                    <td class="{{ search.columns[47] ? '' : 'hide' }}">{{ row.PRECIO_7 }}</td>
                                                    <td class="{{ search.columns[48] ? '' : 'hide' }}">{{ row.HA_7 }}</td>
                                                    <td class="{{ search.columns[49] ? '' : 'hide' }}">{{ row.TOTAL_7 }}</td>
                                                    <td class="{{ search.columns[50] ? '' : 'hide' }}">{{ row.FOLIAR_1 }}</td>
                                                    <td class="{{ search.columns[51] ? '' : 'hide' }}">{{ row.DOSIS_8 }}</td>
                                                    <td class="{{ search.columns[52] ? '' : 'hide' }}">{{ row.CANTIDAD_8 }}</td>
                                                    <td class="{{ search.columns[53] ? '' : 'hide' }}">{{ row.PRECIO_8 }}</td>
                                                    <td class="{{ search.columns[54] ? '' : 'hide' }}">{{ row.HA_8 }}</td>
                                                    <td class="{{ search.columns[55] ? '' : 'hide' }}">{{ row.TOTAL_8 }}</td>
                                                    <td class="{{ search.columns[56] ? '' : 'hide' }}">{{ row.FOLIAR_2 }}</td>
                                                    <td class="{{ search.columns[57] ? '' : 'hide' }}">{{ row.DOSIS_9 }}</td>
                                                    <td class="{{ search.columns[58] ? '' : 'hide' }}">{{ row.CANTIDAD_9 }}</td>
                                                    <td class="{{ search.columns[59] ? '' : 'hide' }}">{{ row.PRECIO_9 }}</td>
                                                    <td class="{{ search.columns[60] ? '' : 'hide' }}">{{ row.HA_9 }}</td>
                                                    <td class="{{ search.columns[61] ? '' : 'hide' }}">{{ row.TOTAL_9 }}</td>
                                                    <td class="{{ search.columns[62] ? '' : 'hide' }}">{{ row.FOLIAR_3 }}</td>
                                                    <td class="{{ search.columns[63] ? '' : 'hide' }}">{{ row.DOSIS_10 }}</td>
                                                    <td class="{{ search.columns[64] ? '' : 'hide' }}">{{ row.CANTIDAD_10 }}</td>
                                                    <td class="{{ search.columns[65] ? '' : 'hide' }}">{{ row.PRECIO_10 }}</td>
                                                    <td class="{{ search.columns[66] ? '' : 'hide' }}">{{ row.HA_10 }}</td>
                                                    <td class="{{ search.columns[67] ? '' : 'hide' }}">{{ row.TOTAL_10 }}</td>
                                                    <td class="{{ search.columns[68] ? '' : 'hide' }}">{{ row.INSECTICIDA }}</td>
                                                    <td class="{{ search.columns[69] ? '' : 'hide' }}">{{ row.DOSIS_11 }}</td>
                                                    <td class="{{ search.columns[70] ? '' : 'hide' }}">{{ row.CANTIDAD_11 }}</td>
                                                    <td class="{{ search.columns[71] ? '' : 'hide' }}">{{ row.PRECIO_11 }}</td>
                                                    <td class="{{ search.columns[72] ? '' : 'hide' }}">{{ row.HA_11 }}</td>
                                                    <td class="{{ search.columns[73] ? '' : 'hide' }}">{{ row.TOTAL_11 }}</td>
                                                    <td class="{{ search.columns[74] ? '' : 'hide' }}">{{ row.AGUA }}</td>
                                                    <td class="{{ search.columns[75] ? '' : 'hide' }}">{{ row.DOSIS_12 }}</td>
                                                    <td class="{{ search.columns[76] ? '' : 'hide' }}">{{ row.CANTIDAD_12 }}</td>
                                                    <td class="{{ search.columns[77] ? '' : 'hide' }}">{{ row.Ha_OPER }}</td>
                                                    <td class="{{ search.columns[78] ? '' : 'hide' }}">{{ row.OPER }}</td>
                                                    <td class="{{ search.columns[79] ? '' : 'hide' }}">{{ row.COSTO_TOTAL }}</td>
                                                    <td class="{{ search.columns[80] ? '' : 'hide' }}">{{ row.COSTO_Ha }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Programa Estimado <small>({{ search.FINCA }})</small></span>
                                    </div>
                                    <div class="actions">
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container table-scrollable">
                                        <div class="btn-group btn-group-devided hide" data-toggle="buttons">
                                            <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                                Columnas <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu estimado pull-left">
                                                <li id="col_fecha" data-event="fecha" class="active">
                                                    <a href="javascript:;">Fecha</a>
                                                </li>
                                                <li id="col_semana" data-event="semana" class="active">
                                                    <a href="javascript:;">Semana</a>
                                                </li>
                                                <li id="col_ciclo" data-event="ciclo" class="active">
                                                    <a href="javascript:;">Ciclo</a>
                                                </li>
                                                <li id="col_compañia" data-event="compania_de_aplicacion">
                                                    <a href="javascript:;">Compañia de Aplicación</a>
                                                </li>
                                                <li id="col_equipo" data-event="equipo_aplicacion">
                                                    <a href="javascript:;">Equipo de Aplicación</a>
                                                </li>
                                                <li id="col_costo_aplicacion" data-event="costo_app_ha">
                                                    <a href="javascript:;">Costo de Aplicación por Ha</a>
                                                </li>
                                                <li id="col_costo_total" data-event="costo_total_app">
                                                    <a href="javascript:;">Costo total de Aplicación</a>
                                                </li>
                                                <li id="col_frecuencia" data-event="frecuencia" class="active">
                                                    <a href="javascript:;">Frecuencia</a>
                                                </li>
                                                <li id="col_fungicida_1" data-event="producto1" class="active">
                                                    <a href="javascript:;">Fungicida 1</a>
                                                </li>
                                                <li id="col_dosis_1" data-event="dosis1" class="active">
                                                    <a href="javascript:;">Dosis 1</a>
                                                </li>
                                                <li id="col_fungicida_2" data-event="producto2" class="active">
                                                    <a href="javascript:;">Fungicida 2</a>
                                                </li>
                                                <li id="col_dosis_2" data-event="dosis2" class="active">
                                                    <a href="javascript:;">Dosis 2</a>
                                                </li>
                                                <li id="col_area" data-event="area_app" class="active">
                                                    <a href="javascript:;">Área</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_estimado">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="10%"> ID </th>
                                                    <th width="10%"> Fecha </th>
                                                    <th width="10%"> Sem  </th>
                                                    <th width="10%"> Ciclo  </th>
                                                    <th width="10%"> Frec  </th>
                                                    <th width="10%"> Fungicida 1 </th>
                                                    <th width="10%"> Fungicida 2 </th>
                                                </tr>
                                            </thead>
                                            <tbody> </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portlet light portlet-fit portlet-datatable bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">Programa Real <small>({{ search.FINCA }})</small></span>
                                    </div>
                                    <div class="actions">
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-container table-scrollable">
                                        <div class="btn-group btn-group-devided hide" data-toggle="buttons">
                                            <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                                Columnas <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu real pull-left">
                                                
                                            </ul>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_real">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="10%" ng-click="changeSort('search_real', 'FECHA_REAL')"> Fecha </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'SEM')"> Sem  </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'CICLO')"> Ciclo  </th>
                                                    <th width="10%"> Frec  </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'FUNGICIDA_1')"> Fungicida 1 </th>
                                                    <th width="10%" ng-click="changeSort('search_real', 'FUNGICIDA_2')"> Fungicida 2 </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="row in table_real | filter : { finca : search.finca } | orderObjectBy : search_real.orderBy : search_real.reverse">
                                                    <td>{{ row.fecha_real }}</td>
                                                    <td>{{ row.sem }}</td>
                                                    <td>{{ row.ciclo }}</td>
                                                    <td>{{  }}</td>
                                                    <td>{{ row.fungicida_1 }}</td>
                                                    <td>{{ row.fungicida_2 }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>