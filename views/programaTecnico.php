<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<style>

.cursor th, .cursor td {
    cursor: pointer;
    /* NO SELECCIONABLE */
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.right-th {
    text-align : right;
}

/* OCULTAR COLUMNAS */
.checkbox {
    width : 110px;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .6em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.labelFilters {
    width: 150px;
    padding-top: 9px;
}

.checkbox label {
    font-size: 12px;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none !important;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
.textCenter {
    text-align : center;
}
#body .fixed-table-header {
    height : 1px !important;
}
</style>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?=$cdn?>global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<div ng-app="app" ng-controller="programa">

    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>PROGRAMA TECNICO
                    <small></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
            <div class="page-toolbar">
                <div style="float:right">
                    <div class="col-md-4">
                        <select name="year" class="form-control" id="year"  ng-model="filters.year" ng-change="index()">
                            <option ng-repeat="(key , value) in years" ng-selected="filters.year == value" value="{{value}}">{{value}}</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select name="gerente" class="form-control" id="gerente" ng-model="filters.gerente" ng-change="changeGerente()"  ng-options="item.id as item.label for item in gerentes">
                            <!--<option ng-repeat="(key , value) in gerentes" ng-selected="filters.gerente == key" value="{{key}}">{{value}}</option>-->
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select name="finca" class="form-control" id="finca" ng-model="filters.finca" ng-change="changeFinca()" ng-options="item.id as item.label for item in fincas[filters.gerente]">
                            <!--<option ng-repeat="(key, value) in fincas[filters.gerente]" ng-selected="filters.finca == label" value="{{ value.id }}">{{ value.label }}</option>-->
                        </select>
                    </div>
                </div>
                <div class="row pull-right">
                    <a class="btn green-jungle sbold" data-toggle="modal" ng-click="showModal()"> FRAC </a>
                </div>
            </div>
        </div>

        <div class="page-head">
            <div class="page-toolbar">
                <div class="col-md-12">
                    <select name="programa" class="form-control" id="programa" ng-model="filters.programa" ng-change="init()">
                        <option value="{{key}}" ng-repeat="(key, value) in tipoPrograma" ng-selected="key == filters.programa">{{value}}</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="resumenCiclos">Inicio</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Listado</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-6">
                <!-- Begin: life time stats -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">PROGRAMA TECNICO <small>(Ciclos)</small></span>
                        </div>
                        <div class="actions">
                            <div class="btn-group pull-right">
                                <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('datatable_ajax_1')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container table-scrollable">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="textCenter">ID</th>
                                        <th class="textCenter">CICLO</th>
                                        <th class="textCenter">SEM</th>
                                        <th class="textCenter">FECHA</th>
                                        <th class="textCenter">FREC</th>
                                        <th class="textCenter">Ha</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat-start="row in table | orderObjectBy:search.orderBy:search.reverse" ng-click="row.expanded = !row.expanded">
                                        <td class="textCenter">{{ row.id }}</td>
                                        <td class="textCenter">{{ row.ciclo }}</td>
                                        <td class="textCenter">{{ row.sem }}</td>
                                        <td class="textCenter">{{ row.fecha_real }}</td>
                                        <td class="textCenter">{{ row.frec }}</td>
                                        <td class="textCenter">{{ row.ha }}</td>
                                    </tr>
                                    <tr ng-show="row.expanded">
                                        <th class="textCenter" colspan="2">PROD</th>
                                        <th class="textCenter">TIPO</th>
                                        <th class="textCenter">DOSIS</th>
                                        <th class="textCenter">CANT</th>
                                    </tr>
                                    <tr ng-show="row.expanded" ng-repeat-end="row" ng-repeat="prod in row.detalle">
                                        <td class="textCenter" colspan="2">{{ prod.nombre_comercial | uppercase }}</td>
                                        <td class="textCenter">{{ prod.tipo | uppercase }}</td>
                                        <td class="textCenter">{{ prod.dosis | num }}</td>
                                        <td class="textCenter">{{ prod.cantidad | number }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="portlet-body hide">
                        <div class="table-container table-scrollable">
                            <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_1">
                                <thead>
                                    <tr class="cursor">
                                        <th width="10%"> ID </th>
                                        <th width="10%"> CICLO  </th>
                                        <th width="10%"> HA  </th>
                                        <th width="10%"> FECHA PROG  </th>
                                        <th width="10%"> FECHA REAL </th>
                                        <th width="10%"> SEM </th>
                                        <th width="10%"> ATRASO </th>
                                        <th width="10%"> MOTIVO </th>
                                        <th width="10%"> FUNGICIDA 1 </th>
                                        <th width="10%"> DOSIS 1 </th>
                                        <th width="10%"> CANTIDAD 1 </th>
                                        <th width="10%"> FUNGICIDA 2 </th>
                                        <th width="10%"> DOSIS 2 </th>
                                        <th width="10%"> CANTIDAD 2 </th>
                                        <th width="10%"> COADYUVANTE 1 </th>
                                        <th width="10%"> DOSIS 1 </th>
                                        <th width="10%"> CANTIDAD 2 </th>
                                        <th width="10%"> COADYUVANTE 2 </th>
                                        <th width="10%"> DOSIS 2 </th>
                                        <th width="10%"> CANTIDAD 2 </th>
                                        <th width="10%"> ACEITE </th>
                                        <th width="10%"> DOSIS </th>
                                        <th width="10%"> CANTIDAD </th>
                                        <th width="10%"> AGUA </th>
                                        <th width="10%"> DOSIS </th>
                                        <th width="10%"> CANTIDAD </th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    <tr ng-repeat="row in table | orderObjectBy : search.orderBy : search.reverse">  
                                        <td>{{ row.id }}</td>
                                        <td>{{ row.ciclo }}</td>
                                        <td >{{ row.ha }}</td>
                                        <td>{{ row.fecha_prog }}</td>
                                        <td>{{ row.fecha_real }}</td>
                                        <td>{{ row.sem }}</td>
                                        <td>{{ row.atraso }}</td>
                                        <td>{{ row.motivo }}</td>
                                        <td>{{ (row.dosis_1 > 0 || row.cantidad_1 > 0) ? row.fungicida_1 : '' }}</td>
                                        <td>{{ (row.dosis_1 > 0) ? row.dosis_1 : '' }}</td>
                                        <td>{{ (row.cantidad_1 > 0) ? row.cantidad_1 : '' }}</td>
                                        <td>{{ (row.dosis_2 > 0 || row.cantidad_2 > 0) ? row.fungicida_2 : '' }}</td>
                                        <td>{{ (row.dosis_2 > 0) ? row.dosis_2 : '' }}</td>
                                        <td>{{ (row.cantidad_2 > 0) ? row.cantidad_2 : '' }}</td>
                                        <td>{{ (row.dosis_5 > 0 || row.cantidad_5 > 0) ? row.coadyuvante_1 : '' }}</td>
                                        <td>{{ (row.dosis_5 > 0) ? row.dosis_5 : '' }}</td>
                                        <td>{{ (row.cantidad_5 > 0) ? row.cantidad_5 : '' }}</td>
                                        <td>{{ (row.dosis_6 > 0 || row.cantidad_6 > 0) ? row.coadyuvante_2 : '' }}</td>
                                        <td>{{ (row.dosis_6 > 0) ? row.dosis_6 : '' }}</td>
                                        <td>{{ (row.cantidad_6 > 0) ? row.cantidad_6 : '' }}</td>
                                        <td>{{ (row.dosis_7 > 0 || row.cantidad_7 > 0) ? row.aceite : '' }}</td>
                                        <td>{{ (row.dosis_7 > 0) ? row.dosis_7 : '' }}</td>
                                        <td>{{ (row.cantidad_7 > 0) ? row.cantidad_7 : '' }}</td>
                                        <td>{{ (row.dosis_13 > 0 || row.cantidad_13 > 0) ? row.agua : '' }}</td>
                                        <td>{{ (row.dosis_13 > 0) ? row.dosis_13 : '' }}</td>
                                        <td>{{ (row.cantidad_13 > 0) ? row.cantidad_13 : '' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
            <div class="col-md-6">
                <!-- Begin: life time stats -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">PROGRAMA TECNICO <small>(CICLOS)</small></span>
                        </div>
                        <div class="actions col-md-12">
                            <div style="float:left;">
                                <div class="btn-group hide">
                                    <a class="btn blue-ebonyclay btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                        Columnas <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li ng-repeat="(key , value) in columnas" ng-class="columnas[key] ? 'active' : ''">
                                            <a ng-click="columnas[key] = !columnas[key]">{{columnasText[key]}}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="btn-group pull-right hide">
                                <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('datatable_ajax_1')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container table-scrollable">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>CICLO</th>
                                        <th>FUNGICIDA 1</th>
                                        <th>DOSIS</th>
                                        <th>FUNGICIDA 2</th>
                                        <th>DOSIS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in table | orderObjectBy:search.orderBy:search.reverse">
                                        <td>{{ row.ciclo }}</td>
                                        <td ng-repeat-start="prod in row.detalle" ng-if="prod.tipo == 'Fungicida'">{{ prod.nombre_comercial }}</td>
                                        <td ng-repeat-end="prod" ng-if="prod.tipo == 'Fungicida'">{{ prod.dosis | num }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="tools">
                            <div class="btn-group pull-right">
                                <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('datatable_ajax_estimado')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container table-scrollable">
                            <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_estimado">
                                <thead>
                                    <tr>
                                        <th class="textCenter">CICLO</th>
                                        <th>FINCA</th>
                                        <th class="textCenter">FECHA PROG</th>
                                        <th class="textCenter">FECHA REAL</th>
                                        <th class="textCenter">FREC PROG</th>
                                        <th class="textCenter">FREC REAL</th>
                                        <th class="textCenter">FUNGICIDA 1</th>
                                        <th class="textCenter">FUNGICIDA 2</th>
                                        <th class="textCenter">DP</th>
                                        <th class="textCenter">DF</th>
                                        <th class="textCenter">MOTIVO</th>
                                    </tr>
                                <thead>
                                <tbody>
                                    <tr ng-repeat="row in table_frecuencia | orderObjectBy:search_frec.orderBy:search_frec.reverse">
                                        <td class="textCenter">{{row.ciclo}}</td>
                                        <td>{{row.finca}}</td>
                                        <td class="textCenter">{{row.fecha_prog}}</td>
                                        <td class="textCenter">{{row.fecha_real}}</td>
                                        <td class="textCenter">{{row.frec_prog}}</td>
                                        <td class="textCenter">{{row.frec_real}}</td>
                                        <td class="textCenter">{{row.fungicida_1}}</td>
                                        <td class="textCenter">{{row.fungicida_2}}</td>
                                        <td class="textCenter {{ semaforoFrec(row.dias_diff+1) }}"> {{ row.dias_diff }}</td>
                                        <td class="textCenter {{ semaforoFrec(row.frec_prog/row.frec_real) }}"> {{ row.frec_prog - row.frec_real }}</td>
                                        <td class="textCenter">{{row.motivo}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">Parciales</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group pull-right">
                                <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('datatable_ajax_parcial')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container table-scrollable">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="textCenter">ID</th>
                                        <th class="textCenter">CICLO</th>
                                        <th class="textCenter">SEM</th>
                                        <th class="textCenter">FECHA</th>
                                        <th class="textCenter">FREC</th>
                                        <th class="textCenter">Ha</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat-start="row in table_parcial" ng-click="row.expanded = !row.expanded">
                                        <td class="textCenter">{{ row.id }}</td>
                                        <td class="textCenter">{{ row.ciclo }}</td>
                                        <td class="textCenter">{{ row.sem }}</td>
                                        <td class="textCenter">{{ row.fecha_real }}</td>
                                        <td class="textCenter">{{ row.frec }}</td>
                                        <td class="textCenter">{{ row.ha }}</td>
                                    </tr>
                                    <tr ng-show="row.expanded">
                                        <th class="textCenter" colspan="2">PROD</th>
                                        <th class="textCenter">TIPO</th>
                                        <th class="textCenter">DOSIS</th>
                                        <th class="textCenter">CANT</th>
                                    </tr>
                                    <tr ng-show="row.expanded" ng-repeat-end="row" ng-repeat="prod in row.detalle">
                                        <td class="textCenter" colspan="2">{{ prod.nombre_comercial | uppercase }}</td>
                                        <td class="textCenter">{{ prod.tipo | uppercase }}</td>
                                        <td class="textCenter">{{ prod.dosis | num }}</td>
                                        <td class="textCenter">{{ prod.cantidad }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="portlet-body hide">
                        <div class="table-container table-scrollable">
                            <!--<div class="btn-group btn-group-devided hide" data-toggle="buttons">
                                <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                    Columnas <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu parcial pull-left">
                                    <li id="col_fecha" data-event="fecha" class="active">
                                        <a href="javascript:;">Fecha</a>
                                    </li>
                                    <li id="col_semana" data-event="semana" class="active">
                                        <a href="javascript:;">Semana</a>
                                    </li>
                                    <li id="col_ciclo" data-event="ciclo" class="active">
                                        <a href="javascript:;">Ciclo</a>
                                    </li>
                                    <li id="col_compañia" data-event="compania_de_aplicacion">
                                        <a href="javascript:;">Compañia de Aplicación</a>
                                    </li>
                                    <li id="col_equipo" data-event="equipo_aplicacion">
                                        <a href="javascript:;">Equipo de Aplicación</a>
                                    </li>
                                    <li id="col_costo_aplicacion" data-event="costo_app_ha">
                                        <a href="javascript:;">Costo de Aplicación por Ha</a>
                                    </li>
                                    <li id="col_costo_total" data-event="costo_total_app">
                                        <a href="javascript:;">Costo total de Aplicación</a>
                                    </li>
                                    <li id="col_frecuencia" data-event="frecuencia" class="active">
                                        <a href="javascript:;">Frecuencia</a>
                                    </li>
                                    <li id="col_fungicida_1" data-event="producto1" class="active">
                                        <a href="javascript:;">Fungicida 1</a>
                                    </li>
                                    <li id="col_dosis_1" data-event="dosis1" class="active">
                                        <a href="javascript:;">Dosis 1</a>
                                    </li>
                                    <li id="col_fungicida_2" data-event="producto2" class="active">
                                        <a href="javascript:;">Fungicida 2</a>
                                    </li>
                                    <li id="col_dosis_2" data-event="dosis2" class="active">
                                        <a href="javascript:;">Dosis 2</a>
                                    </li>
                                    <li id="col_area" data-event="area_app" class="active">
                                        <a href="javascript:;">Área</a>
                                    </li>
                                </ul>
                            </div>-->

                            <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_parcial">
                                <thead>
                                    <tr class="cursor">
                                        <th width="10%"> ID </th>
                                        <th width="10%"> CICLO  </th>
                                        <th width="10%"> HA  </th>
                                        <th width="10%"> FECHA PROG  </th>
                                        <th width="10%"> FECHA REAL </th>
                                        <th width="10%"> SEM </th>
                                        <th width="10%"> ATRASO </th>
                                        <th width="10%"> MOTIVO </th>
                                        <th width="10%"> FUNGICIDA 1 </th>
                                        <th width="10%"> DOSIS 1 </th>
                                        <th width="10%"> CANTIDAD 1 </th>
                                        <th width="10%"> FUNGICIDA 2 </th>
                                        <th width="10%"> DOSIS 2 </th>
                                        <th width="10%"> CANTIDAD 2 </th>
                                        <th width="10%"> COADYUVANTE 1 </th>
                                        <th width="10%"> DOSIS 1 </th>
                                        <th width="10%"> CANTIDAD 2 </th>
                                        <th width="10%"> COADYUVANTE 2 </th>
                                        <th width="10%"> DOSIS 2 </th>
                                        <th width="10%"> CANTIDAD 2 </th>
                                        <th width="10%"> ACEITE </th>
                                        <th width="10%"> DOSIS </th>
                                        <th width="10%"> CANTIDAD </th>
                                        <th width="10%"> AGUA </th>
                                        <th width="10%"> DOSIS </th>
                                        <th width="10%"> CANTIDAD </th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    <tr ng-repeat="row in table_parcial">
                                        <td>{{ row.id }}</td>
                                        <td>{{ row.ciclo }}</td>
                                        <td >{{ row.ha }}</td>
                                        <td>{{ row.fecha_prog }}</td>
                                        <td>{{ row.fecha_real }}</td>
                                        <td>{{ row.sem }}</td>
                                        <td>{{ row.atraso }}</td>
                                        <td>{{ row.motivo }}</td>
                                        <td>{{ (row.dosis_1 > 0 || row.cantidad_1 > 0) ? row.fungicida_1 : '' }}</td>
                                        <td>{{ (row.dosis_1 > 0) ? (row.dosis_1 | number) : '' }}</td>
                                        <td>{{ (row.cantidad_1 > 0) ? row.cantidad_1 : '' }}</td>
                                        <td>{{ (row.dosis_2 > 0 || row.cantidad_2 > 0) ? row.fungicida_2 : '' }}</td>
                                        <td>{{ (row.dosis_2 > 0) ? (row.dosis_2 | number) : '' }}</td>
                                        <td>{{ (row.cantidad_2 > 0) ? row.cantidad_2 : '' }}</td>
                                        <td>{{ (row.dosis_5 > 0 || row.cantidad_5 > 0) ? row.coadyuvante_1 : '' }}</td>
                                        <td>{{ (row.dosis_5 > 0) ? (row.dosis_5 | number) : '' }}</td>
                                        <td>{{ (row.cantidad_5 > 0) ? row.cantidad_5 : '' }}</td>
                                        <td>{{ (row.dosis_6 > 0 || row.cantidad_6 > 0) ? row.coadyuvante_2 : '' }}</td>
                                        <td>{{ (row.dosis_6 > 0) ? (row.dosis_6 | number) : '' }}</td>
                                        <td>{{ (row.cantidad_6 > 0) ? row.cantidad_6 : '' }}</td>
                                        <td>{{ (row.dosis_7 > 0 || row.cantidad_7 > 0) ? row.aceite : '' }}</td>
                                        <td>{{ (row.dosis_7 > 0) ? (row.dosis_7 | number) : '' }}</td>
                                        <td>{{ (row.cantidad_7 > 0) ? row.cantidad_7 : '' }}</td>
                                        <td>{{ (row.dosis_13 > 0 || row.cantidad_13 > 0) ? row.agua : '' }}</td>
                                        <td>{{ (row.dosis_13 > 0) ? (row.dosis_13 | number) : '' }}</td>
                                        <td>{{ (row.cantidad_13 > 0) ? row.cantidad_13 : '' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row hide">
            <div class="col-md-6">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">Programa Estimado </span>
                        </div>
                        <div class="actions">
                            <div class="btn-group pull-right">
                                <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('datatable_ajax_estimado')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container table-scrollable">
                            <!--<div class="btn-group btn-group-devided hide" data-toggle="buttons">
                                <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                    Columnas <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu estimado pull-left">
                                    <li id="col_fecha" data-event="fecha" class="active">
                                        <a href="javascript:;">Fecha</a>
                                    </li>
                                    <li id="col_semana" data-event="semana" class="active">
                                        <a href="javascript:;">Semana</a>
                                    </li>
                                    <li id="col_ciclo" data-event="ciclo" class="active">
                                        <a href="javascript:;">Ciclo</a>
                                    </li>
                                    <li id="col_compañia" data-event="compania_de_aplicacion">
                                        <a href="javascript:;">Compañia de Aplicación</a>
                                    </li>
                                    <li id="col_equipo" data-event="equipo_aplicacion">
                                        <a href="javascript:;">Equipo de Aplicación</a>
                                    </li>
                                    <li id="col_costo_aplicacion" data-event="costo_app_ha">
                                        <a href="javascript:;">Costo de Aplicación por Ha</a>
                                    </li>
                                    <li id="col_costo_total" data-event="costo_total_app">
                                        <a href="javascript:;">Costo total de Aplicación</a>
                                    </li>
                                    <li id="col_frecuencia" data-event="frecuencia" class="active">
                                        <a href="javascript:;">Frecuencia</a>
                                    </li>
                                    <li id="col_fungicida_1" data-event="producto1" class="active">
                                        <a href="javascript:;">Fungicida 1</a>
                                    </li>
                                    <li id="col_dosis_1" data-event="dosis1" class="active">
                                        <a href="javascript:;">Dosis 1</a>
                                    </li>
                                    <li id="col_fungicida_2" data-event="producto2" class="active">
                                        <a href="javascript:;">Fungicida 2</a>
                                    </li>
                                    <li id="col_dosis_2" data-event="dosis2" class="active">
                                        <a href="javascript:;">Dosis 2</a>
                                    </li>
                                    <li id="col_area" data-event="area_app" class="active">
                                        <a href="javascript:;">Área</a>
                                    </li>
                                </ul>
                            </div>-->
                            <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_estimado">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="10%"> ID </th>
                                        <th width="10%"> Fecha </th>
                                        <th width="10%"> Sem  </th>
                                        <th width="10%"> Ciclo  </th>
                                        <th width="10%"> Frec  </th>
                                        <th width="10%"> Fungicida 1 </th>
                                        <th width="10%"> Fungicida 2 </th>
                                    </tr>
                                </thead>
                                <tbody> </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">Programa Real </span>
                        </div>
                        <div class="actions">
                            <div class="btn-group pull-right">
                                <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportPrint('datatable_ajax_real')"> Imprimir </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('datatable_ajax_real')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container table-scrollable">
                            <div class="btn-group btn-group-devided hide" data-toggle="buttons">
                                <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                    Columnas <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu real pull-left">
                                    
                                </ul>
                            </div>
                            <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_real">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="10%" ng-click="changeSort('search_real', 'FECHA_REAL')"> Fecha </th>
                                        <th width="10%" ng-click="changeSort('search_real', 'SEM')"> Sem  </th>
                                        <th width="10%" ng-click="changeSort('search_real', 'CICLO')"> Ciclo  </th>
                                        <th width="10%"> Frec  </th>
                                        <th width="10%" ng-click="changeSort('search_real', 'FUNGICIDA_1')"> Fungicida 1 </th>
                                        <th width="10%" ng-click="changeSort('search_real', 'FUNGICIDA_2')"> Fungicida 2 </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in table_real | filter : { finca : search.finca } | orderObjectBy : search_real.orderBy : search_real.reverse">
                                        <td>{{ row.fecha_real }}</td>
                                        <td>{{ row.sem }}</td>
                                        <td>{{ row.ciclo }}</td>
                                        <td>{{  }}</td>
                                        <td>{{ row.fungicida_1 }}</td>
                                        <td>{{ row.fungicida_2 }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
        <!-- END PAGE BASE CONTENT -->
    </div>


    <div id="nsecado" class="modal fade" tabindex="-1" data-width="461">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header blue">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">FRAC</h4>
                </div>
                <div class="modal-body form">
                    <div class="table-container table-scrollable">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="textCenter">GRUPO</th>
                                    <th class="textCenter">FRAC</th>
                                    <th class="textCenter">APLIC</th>
                                    <th class="textCenter">SALDO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in frac">
                                    <td class="textCenter">{{ row.nombre | uppercase }}</td>
                                    <td class="textCenter">{{ (row.frac > 0) ? (row.frac | number: 0) : '-' }}</td>
                                    <td class="textCenter">{{ row.prog }}</td>
                                    <td class="textCenter {{ semaforoSaldo(saldo) }}">{{ saldo = ((row.frac > 0) ? (row.frac - row.prog) : '') }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>