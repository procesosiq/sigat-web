<?php
    $cdn = "http://cdn.procesos-iq.com/";
?>
<style>
    .center-th {
        text-align: center;
    }
    .left-th {
        text-align: left !important;
    }
    .charts-500 {
        height:500px;
    }
    .cursor td, .cursor th{
        cursor: pointer;
    }
    .asc {
        background-position: right;
        background-repeat: no-repeat;
        padding-right: 30px;
        background-image : url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAATCAYAAAByUDbMAAAAZ0lEQVQ4y2NgGLKgquEuFxBPAGI2ahhWCsS/gDibUoO0gPgxEP8H4ttArEyuQYxAPBdqEAxPBImTY5gjEL9DM+wTENuQahAvEO9DMwiGdwAxOymGJQLxTyD+jgWDxCMZRsEoGAVoAADeemwtPcZI2wAAAABJRU5ErkJggg==');
    }
    .desc {
        background-position: right;
        background-repeat: no-repeat;
        padding-right: 30px;
        background-image : url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAATCAYAAAByUDbMAAAAZUlEQVQ4y2NgGAWjYBSggaqGu5FA/BOIv2PBIPFEUgxjB+IdQPwfC94HxLykus4GiD+hGfQOiB3J8SojEE9EM2wuSJzcsFMG4ttQgx4DsRalkZENxL+AuJQaMcsGxBOAmGvopk8AVz1sLZgg0bsAAAAASUVORK5CYII=');
    }
    .row-second-level {
        background-color: #f5f5f5;
    }
    .row-third-level {
        background-color: #e5e5e5;
    }
</style>
<script src="assets/global/plugins/FileSaver.min.js"></script>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper" id="container_angular" style="display:none;" ng-app="app">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" ng-controller="resumenCiclos" id="resumenCiclos">
        <!-- BEGIN PAGE HEAD-->
        <input type="hidden" id="ciclos_aplicados_chart" value='<?=json_encode($data->ciclos_aplicados_chart)?>'>
        <input type="hidden" id="ciclos_aplicados_sum_chart" value='<?=json_encode($data->ciclos_aplicados_sum_chart)?>'>
        <input type="hidden" id="ciclos_aplicados_avg_chart" value='<?=json_encode($data->ciclos_aplicados_avg_chart)?>'>
        <input type="hidden" id="motivo_input" value='<?=json_encode($data->motivo)?>'>
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Resumen Ejecutivo</h1>
            </div>
            <!-- END PAGE TITLE -->
            <div class="page-toolbar">
                <div class="row hide">
                    <div class="pull-right">
                        <a class="btn red" ng-click="exportPDF()">PDF</a>
                    </div>
                </div>
                <div class="row">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                            {{filters.encargadoName}}
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li ng-click="setFilterEncargado({id : '' , label : 'ORODELTI'})">
                                <a href="#">
                                    ORODELTI
                                </a>
                            </li>
                            <li ng-repeat="value in encargados" ng-click="setFilterEncargado(value)">
                                <a href="#">
                                    {{value.label}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                            {{filters.gerenteName}}
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li ng-click="setFilterGerente({id : '' , label : 'ORODELTI'})">
                                <a href="#">
                                    ORODELTI
                                </a>
                            </li>
                            <li ng-repeat="(key , value) in gerentes" ng-click="setFilterGerente(value)">
                                <a href="#">
                                    {{value.label}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                            HASTA {{filters.sem2}}
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <!-- style="height: 400px;overflow-y: scroll;" -->
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li ng-click="setFilterWeeks2({id : '' , label : 'SEM'})">
                                <a href="#">
                                    SEMANAS
                                </a>
                            </li>
                            <li ng-repeat="(key , value) in semanas" ng-click="setFilterWeeks2(value)">
                                <a href="#">
                                    {{value.label}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                            DESDE {{filters.sem}}
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li ng-click="setFilterWeeks({id : '' , label : 'SEM'})">
                                <a href="#">
                                    SEMANAS
                                </a>
                            </li>
                            <li ng-repeat="(key , value) in semanas" ng-click="setFilterWeeks(value)">
                                <a href="#">
                                    {{value.label}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                            {{filters.year}}
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li ng-repeat="(key , value) in years" ng-click="setFilterYears(value)">
                                <a href="#">
                                    {{value.years}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                            {{ filters.variable == '' ? 'TODOS' : tipoPrograma[filters.variable] }}
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li ng-click="setFilterVariable(key)" ng-repeat="(key, value) in tipoPrograma">
                                <a href="">
                                    {{value}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                            {{ tiposCiclo[filters.tipoCiclo] }}
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li ng-click="setFilterTipoCiclo(key)" ng-repeat="(key, value) in tiposCiclo">
                                <a href="">
                                    {{value}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                            {{ tipoHectarea[filters.tipoHectarea] }}
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li ng-click="setFilterHectarea('')">
                                <a href="#">
                                    FUMIGACIÓN
                                </a>
                            </li>
                            <li ng-click="setFilterHectarea('produccion')">
                                <a href="#">
                                    PRODUCCIÓN
                                </a>
                            </li>
                            <li ng-click="setFilterHectarea('neta')">
                                <a href="#">
                                    NETA
                                </a>
                            </li>
                            <li ng-click="setFilterHectarea('banano')">
                                <a href="#">
                                    BANANO
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <span class="active">Resumen</span>
            </li>
        </ul>

        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div id="indicadores"></div>
            </div>

            <div class="col-md-12">
                <div class="portlet box green-haze">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase">TABLA RESUMEN</span>
                        </div>

                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tbl_resumen_costos" data-toggle="tab" aria-expanded="true"> $/Ha Año </a>
                            </li>
                            <li>
                                <a href="#tbl_dolares_ha_ciclo" data-toggle="tab" aria-expanded="false"> $/Ha Ciclo </a>
                            </li>
                            <li>
                                <a href="#tbl_ciclos_aplicados" data-toggle="tab" aria-expanded="false"> Ciclos </a>
                            </li>
                        </ul>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tbl_resumen_costos">
                                <div class="col-md-12 text-right">
                                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                                        <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                                    </div>
                                    <div class="btn-group pull-right">
                                        <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                            Exportar <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="javascript:;" ng-click="exportPrint('datatable_dolares_hectarea')"> Imprimir </a>
                                            </li>
                                            <li class="divider"> </li>
                                            <li>
                                                <a href="javascript:;" ng-click="fnExcelReport('datatable_dolares_hectarea', '$/Ha Año')">Excel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="table-container table-resposive table-scrollable">
                                    <table class="table table-bordered table-hover" id="datatable_dolares_hectarea">
                                        <thead>
                                            <tr class="cursor">
                                                <th class="center-th {{ (tables.dolares.orderBy == 'finca') ? ((tables.dolares.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.dolares.orderBy = 'finca'; tables.dolares.reverse = !tables.dolares.reverse;">FINCAS</th>
                                                <th class="center-th {{ (tables.dolares.orderBy == value.years) ? ((tables.dolares.reverse) ? 'desc' : 'asc') : '' }}" 
                                                    ng-click="tables.dolares.orderBy = value.years; tables.dolares.reverse = !tables.dolares.reverse;" 
                                                    ng-repeat-start="(key , value) in years">{{value.years}}</th>
                                                <td ng-repeat-end ng-if="key > 0">
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat-start="(llave , valor) in resumen_costos | orderObjectBy:tables.dolares.orderBy:tables.dolares.reverse"
                                                ng-click="valor.expand = !valor.expand">
                                                <td class="left-th">{{valor.finca}}</td>
                                                <td class="text-right" ng-repeat-start="(key , value) in years">
                                                    <!--{{ valor[value.years] | number : 2 }} -->
                                                    {{ valor[value.years] = (valor.detalle | sumOfValue : value.years) | number : 2 }}
                                                    <span class="hide">{{ valor[value.years+'_form'] = valor[value.years]*valor.porc }}</span>
                                                </td>
                                                <td class="text-right" ng-repeat-end ng-if="key > 0">
                                                    <small>{{ porcentajeDiferencia(valor[value.years-1] , valor[value.years], key) }}</small>
                                                    <i class="{{ semaforoDll(valor[value.years-1] , valor[value.years] , key) }}"></i>
                                                </td>
                                            </tr>
                                            <tr ng-show="valor.expand" ng-click="nivel2.expand = !nivel2.expand" ng-repeat-start="nivel2 in valor.detalle" class="row-second-level">
                                                <td class="left-th">&nbsp;&nbsp;&nbsp;&nbsp;{{nivel2.tipo_producto}}</td>
                                                <td class="text-right" ng-repeat-start="(key2 , y2) in years">
                                                    <!--{{ nivel2[y2.years] | number : 2 }} -->
                                                    {{ nivel2[y2.years] = (nivel2.detalle | sumOfValue : y2.years) | number : 2 }}
                                                </td>
                                                <td class="text-right" ng-repeat-end ng-if="key2 > 0">
                                                    <small>{{ porcentajeDiferencia(nivel2[y2.years-1] , nivel2[y2.years], key2) }}</small>
                                                    <i class="{{ semaforoDll(nivel2[y2.years-1] , nivel2[y2.years] , key2) }}"></i>
                                                </td>
                                            </tr>
                                            <tr ng-show="nivel2.expand" ng-repeat="nivel3 in nivel2.detalle | filter : { tipo_ciclo : 'CICLO' }" class="row-third-level">
                                                <td class="left-th">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ nivel3.tipo_ciclo == 'PARCIAL' ? 'P' : '' }} {{nivel3.ciclo}}</td>
                                                <td class="text-right" ng-repeat-start="(key3 , y3) in years">
                                                    {{ nivel3[y3.years] | number : 2 }}
                                                </td>
                                                <td class="text-right" ng-repeat-end ng-if="key3 > 0">
                                                    <small>{{ porcentajeDiferencia(nivel3[y3.years-1] , nivel3[y3.years], key3) }}</small>
                                                    <i class="{{ semaforoDll(nivel3[y3.years-1] , nivel3[y3.years] , key3) }}"></i>
                                                </td>
                                            </tr>
                                            <tr ng-show="nivel2.expand">
                                                <th colspan="{{years.length+2}}"></th>
                                            </tr>
                                            <tr ng-show="nivel2.expand" ng-repeat="nivel3 in nivel2.detalle | filter : { tipo_ciclo : 'PARCIAL' }" class="row-third-level">
                                                <td class="left-th">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ nivel3.tipo_ciclo == 'PARCIAL' ? 'P' : '' }} {{nivel3.ciclo}}</td>
                                                <td class="text-right" ng-repeat-start="(key3 , y3) in years">
                                                    {{ nivel3[y3.years] | number : 2 }}
                                                </td>
                                                <td class="text-right" ng-repeat-end ng-if="key3 > 0">
                                                    <small>{{ porcentajeDiferencia(nivel3[y3.years-1] , nivel3[y3.years], key3) }}</small>
                                                    <i class="{{ semaforoDll(nivel3[y3.years-1] , nivel3[y3.years] , key3) }}"></i>
                                                </td>
                                            </tr>
                                            <tr ng-repeat-end ng-hide="true"></tr>
                                            <tr ng-repeat-end ng-hide="true"></tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="center-th">TOTALES</th>
                                                <th class="center-th {{getClassName(total_ciclos_aplicados_sum_anio[(value.years-1)] , total_ciclos_aplicados_sum_anio[value.years] , $index)}}" 
                                                    ng-repeat-start="(key , value) in years">
                                                    {{ total_ciclos_aplicados_sum_anio[value.years] = (resumen_costos | sumOfValue : value.years+'_form') | number: 2 }}
                                                </th>
                                                <th ng-repeat-end ng-if="key > 0"></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="tbl_ciclos_aplicados">
                                <div class="col-md-12 text-right">
                                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                                        <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                                    </div>
                                    <div class="btn-group pull-right">
                                        <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                            Exportar <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="javascript:;" ng-click="exportPrint('datatable_ciclos_aplicados')"> Imprimir </a>
                                            </li>
                                            <li class="divider"> </li>
                                            <li>
                                                <a href="javascript:;" ng-click="fnExcelReport('datatable_ciclos_aplicados', 'Ciclos Aplicados')">Excel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-12 pull-right">
                                    <ul style="list-style: none;">
                                        <li><div style="width:10px; height:10px; display: inline-block;" class="bg-red-thunderbird bg-font-red-thunderbird"></div> Más número de ciclos que el año pasado</li>
                                        <li><div style="width:10px; height:10px; display: inline-block;" class="bg-yellow-gold bg-font-yellow-gold"></div> Mismo número de ciclos que el año pasado</li>
                                        <li><div style="width:10px; height:10px; display: inline-block;" class="bg-green-haze bg-font-green-haze"></div> Menor número de ciclos que el año pasado</li>
                                    </ul>
                                </div>
                                <div class="table-container">
                                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ciclos_aplicados">
                                        <thead>
                                            <tr class="cursor">
                                                <th class="center-th {{ (tables.ciclos.orderBy == 'finca') ? ((tables.ciclos.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.ciclos.orderBy='finca'; tables.ciclos.reverse=!tables.ciclos.reverse;">FINCAS</th>
                                                <th class="center-th {{ (tables.ciclos.orderBy == value.years) ? ((tables.ciclos.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.ciclos.orderBy=value.years; tables.ciclos.reverse=!tables.ciclos.reverse;" ng-repeat="(key , value) in years">{{value.years}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="valor in ciclos_aplicados | orderObjectBy:tables.ciclos.orderBy:tables.ciclos.reverse">
                                                <td class="left-th">{{valor.finca}}</td>
                                                <td class="center-th {{getClassName(valor[(value.years-1)] , valor[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                                    {{ (filters.variable != 'CICLO') 
                                                        ? (valor[value.years] | number: 2)
                                                        : valor[value.years] }}
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="center-th"></th>
                                                <th class="center-th" ng-repeat="data in years"></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="tbl_dolares_ha_ciclo">
                                <div class="col-md-12">
                                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                                        <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                                    </div>
                                    <div class="btn-group pull-right">
                                        <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                            Exportar <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="javascript:;" ng-click="exportPrint('datatable_dolares_hectarea_ciclo')"> Imprimir </a>
                                            </li>
                                            <li class="divider"> </li>
                                            <li>
                                                <a href="javascript:;" ng-click="fnExcelReport('datatable_dolares_hectarea_ciclo', '$/Ha Ciclo')">Excel</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-12 pull-right">
                                    <ul style="list-style: none;">
                                        <li><div style="width:10px; height:10px; display: inline-block;" class="bg-red-thunderbird bg-font-red-thunderbird"></div> Más número de hec/ciclo que el año pasado</li>
                                        <li><div style="width:10px; height:10px; display: inline-block;" class="bg-yellow-gold bg-font-yellow-gold"></div> Mismo número de hec/ciclo que el año pasado</li>
                                        <li><div style="width:10px; height:10px; display: inline-block;" class="bg-green-haze bg-font-green-haze"></div> Menor número de hec/ciclo que el año pasado</li>
                                    </ul>
                                </div>
                                <div class="table-container">
                                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_dolares_hectarea_ciclo">
                                        <thead>
                                            <tr class="cursor">
                                                <th class="center-th {{ (tables.dolares_ha.orderBy == 'finca') ? ((tables.dolares_ha.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.dolares_ha.orderBy = 'finca'; tables.dolares_ha.reverse = !tables.dolares_ha.reverse;">FINCAS</th>
                                                <th class="center-th {{ (tables.dolares_ha.orderBy == value.years) ? ((tables.dolares_ha.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.dolares_ha.orderBy = value.years; tables.dolares_ha.reverse = !tables.dolares_ha.reverse;" ng-repeat="(key , value) in years">{{value.years}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="(llave , valor) in ciclos_aplicados_anio | orderObjectBy:tables.dolares_ha.orderBy:tables.dolares_ha.reverse">
                                                <td class="left-th">{{valor.finca | uppercase }}</td>
                                                <td class="center-th  {{getClassName(valor[(value.years-1)] , valor[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                                    {{valor[value.years] | number : 2}}
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="center-th"></th>
                                                <th class="center-th {{getClassName(total_ciclos_aplicados_anio[(value.years-1)] , total_ciclos_aplicados_anio[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                                    {{total_ciclos_aplicados_anio[value.years] | number: 2}}
                                                </th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="tbl_dolares_ha_anio">
                                <div class="col-md-5 col-sm-12 pull-right">
                                    <ul style="list-style: none;">
                                        <li><div style="width:10px; height:10px; display: inline-block;" class="bg-red-thunderbird bg-font-red-thunderbird"></div> Más número de dolares que el año pasado</li>
                                        <li><div style="width:10px; height:10px; display: inline-block;" class="bg-yellow-gold bg-font-yellow-gold"></div> Mismo número de dolares que el año pasado</li>
                                        <li><div style="width:10px; height:10px; display: inline-block;" class="bg-green-haze bg-font-green-haze"></div> Menor número de dolares que el año pasado</li>
                                    </ul>
                                </div>
                                <div class="table-container">
                                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_dolares_hectarea">
                                        <thead>
                                            <tr class="cursor">
                                                <th class="center-th {{ (tables.dolares.orderBy == 'finca') ? ((tables.dolares.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.dolares.orderBy = 'finca'; tables.dolares.reverse = !tables.dolares.reverse;">FINCAS</th>
                                                <th class="center-th {{ (tables.dolares.orderBy == value.years) ? ((tables.dolares.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.dolares.orderBy = value.years; tables.dolares.reverse = !tables.dolares.reverse;" ng-repeat="(key , value) in years">{{value.years}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="(llave , valor) in ciclos_aplicados_sum_anio | orderObjectBy:tables.dolares.orderBy:tables.dolares.reverse">
                                                <td class="left-th">{{valor.finca}}</td>
                                                <td class="center-th  {{getClassName(valor[(value.years-1)] , valor[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                                    {{ valor[value.years] | number : 2 }}
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="center-th">TOTALES</th>
                                                <th class="center-th {{getClassName(total_ciclos_aplicados_sum_anio[(value.years-1)] , total_ciclos_aplicados_sum_anio[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                                    {{ total_ciclos_aplicados_sum_anio[value.years] = (ciclos_aplicados_sum_anio | avgOfValue : value.years) | number: 2 }}
                                                </th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php /*
            <div class="col-md-12" id="resumenCostos">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption"><span class="caption-subject font-dark sbold uppercase">RESUMEN DE COSTOS</span></div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container table-resposive table-scrollable">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr class="cursor">
                                        <th class="center-th {{ (tables.dolares.orderBy == 'finca') ? ((tables.dolares.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.dolares.orderBy = 'finca'; tables.dolares.reverse = !tables.dolares.reverse;">FINCAS</th>
                                        <th class="center-th {{ (tables.dolares.orderBy == value.years) ? ((tables.dolares.reverse) ? 'desc' : 'asc') : '' }}" 
                                            ng-click="tables.dolares.orderBy = value.years; tables.dolares.reverse = !tables.dolares.reverse;" 
                                            ng-repeat-start="(key , value) in years">{{value.years}}</th>
                                        <td ng-repeat-end ng-if="key > 0">
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat-start="(llave , valor) in resumen_costos | orderObjectBy:tables.dolares.orderBy:tables.dolares.reverse"
                                        ng-click="valor.expand = !valor.expand">
                                        <td class="left-th">{{valor.finca}}</td>
                                        <td class="text-right" ng-repeat-start="(key , value) in years">
                                            <!--{{ valor[value.years] | number : 2 }} -->
                                            {{ valor[value.years] = (valor.detalle | sumOfValue : value.years) | number : 2 }}
                                        </td>
                                        <td class="text-right" ng-repeat-end ng-if="key > 0">
                                            <small>{{ porcentajeDiferencia(valor[value.years-1] , valor[value.years], key) }}</small>
                                            <i class="{{ semaforoDll(valor[value.years-1] , valor[value.years] , key) }}"></i>
                                        </td>
                                    </tr>
                                    <tr ng-show="valor.expand" ng-click="nivel2.expand = !nivel2.expand" ng-repeat-start="nivel2 in valor.detalle" class="row-second-level">
                                        <td class="left-th">&nbsp;&nbsp;&nbsp;&nbsp;{{nivel2.tipo_producto}}</td>
                                        <td class="text-right" ng-repeat-start="(key2 , y2) in years">
                                            <!--{{ nivel2[y2.years] | number : 2 }} -->
                                            {{ nivel2[y2.years] = (nivel2.detalle | sumOfValue : y2.years) | number : 2 }}
                                        </td>
                                        <td class="text-right" ng-repeat-end ng-if="key2 > 0">
                                            <small>{{ porcentajeDiferencia(nivel2[y2.years-1] , nivel2[y2.years], key2) }}</small>
                                            <i class="{{ semaforoDll(nivel2[y2.years-1] , nivel2[y2.years] , key2) }}"></i>
                                        </td>
                                    </tr>
                                    <tr ng-show="nivel2.expand" ng-repeat="nivel3 in nivel2.detalle" class="row-third-level">
                                        <td class="left-th">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{nivel3.ciclo}}</td>
                                        <td class="text-right" ng-repeat-start="(key3 , y3) in years">
                                            {{ nivel3[y3.years] | number : 2 }}
                                        </td>
                                        <td class="text-right" ng-repeat-end ng-if="key3 > 0">
                                            <small>{{ porcentajeDiferencia(nivel3[y3.years-1] , nivel3[y3.years], key3) }}</small>
                                            <i class="{{ semaforoDll(nivel3[y3.years-1] , nivel3[y3.years] , key3) }}"></i>
                                        </td>
                                    </tr>
                                    <tr ng-repeat-end ng-hide="true"></tr>
                                    <tr ng-repeat-end ng-hide="true"></tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="center-th">TOTALES</th>
                                        <th class="center-th {{getClassName(total_ciclos_aplicados_sum_anio[(value.years-1)] , total_ciclos_aplicados_sum_anio[value.years] , $index)}}" 
                                            ng-repeat-start="(key , value) in years">
                                            {{ total_ciclos_aplicados_sum_anio[value.years] = (resumen_costos | avgOfValue : value.years) | number: 2 }}
                                        </th>
                                        <th ng-repeat-end ng-if="key > 0"></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>    
                </div>
            </div>

            <div class="col-md-12" id="ciclosAplicados">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">CICLOS APLICADOS</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                            </div>
                            <div class="btn-group pull-right">
                                <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportPrint('datatable_ciclos_aplicados')"> Imprimir </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('datatable_ciclos_aplicados')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="col-md-5 col-sm-12 pull-right">
                            <ul style="list-style: none;">
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-red-thunderbird bg-font-red-thunderbird"></div> Más número de ciclos que el año pasado</li>
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-yellow-gold bg-font-yellow-gold"></div> Mismo número de ciclos que el año pasado</li>
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-green-haze bg-font-green-haze"></div> Menor número de ciclos que el año pasado</li>
                            </ul>
                        </div>
                        <div class="table-container">
                            <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ciclos_aplicados">
                                <thead>
                                    <tr class="cursor">
                                        <th class="center-th {{ (tables.ciclos.orderBy == 'finca') ? ((tables.ciclos.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.ciclos.orderBy='finca'; tables.ciclos.reverse=!tables.ciclos.reverse;">FINCAS</th>
                                        <th class="center-th {{ (tables.ciclos.orderBy == value.years) ? ((tables.ciclos.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.ciclos.orderBy=value.years; tables.ciclos.reverse=!tables.ciclos.reverse;" ng-repeat="(key , value) in years">{{value.years}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="valor in ciclos_aplicados | orderObjectBy:tables.ciclos.orderBy:tables.ciclos.reverse">
                                        <td class="left-th">{{valor.finca}}</td>
                                        <td class="center-th {{getClassName(valor[(value.years-1)] , valor[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                            {{ (filters.variable != 'CICLO') 
                                                ? (valor[value.years] | number: 2)
                                                : valor[value.years] }}
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="center-th"></th>
                                        <th class="center-th" ng-repeat="data in years"></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12" id="dolaresHectareaAño">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">DOLARES POR HECTAREA/AÑO</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                            </div>
                            <div class="btn-group pull-right">
                                <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportPrint('datatable_dolares_hectarea')"> Imprimir </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('datatable_dolares_hectarea')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="col-md-5 col-sm-12 pull-right">
                            <ul style="list-style: none;">
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-red-thunderbird bg-font-red-thunderbird"></div> Más número de dolares que el año pasado</li>
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-yellow-gold bg-font-yellow-gold"></div> Mismo número de dolares que el año pasado</li>
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-green-haze bg-font-green-haze"></div> Menor número de dolares que el año pasado</li>
                            </ul>
                        </div>
                        <div class="table-container">
                            <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_dolares_hectarea">
                                <thead>
                                    <tr class="cursor">
                                        <th class="center-th {{ (tables.dolares.orderBy == 'finca') ? ((tables.dolares.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.dolares.orderBy = 'finca'; tables.dolares.reverse = !tables.dolares.reverse;">FINCAS</th>
                                        <th class="center-th {{ (tables.dolares.orderBy == value.years) ? ((tables.dolares.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.dolares.orderBy = value.years; tables.dolares.reverse = !tables.dolares.reverse;" ng-repeat="(key , value) in years">{{value.years}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="(llave , valor) in ciclos_aplicados_sum_anio | orderObjectBy:tables.dolares.orderBy:tables.dolares.reverse">
                                        <td class="left-th">{{valor.finca}}</td>
                                        <td class="center-th  {{getClassName(valor[(value.years-1)] , valor[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                            {{ valor[value.years] | number : 2 }}
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="center-th">TOTALES</th>
                                        <th class="center-th {{getClassName(total_ciclos_aplicados_sum_anio[(value.years-1)] , total_ciclos_aplicados_sum_anio[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                            {{ total_ciclos_aplicados_sum_anio[value.years] = (ciclos_aplicados_sum_anio | avgOfValue : value.years) | number: 2 }}
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12" id="dolaresHectareaCiclo">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">DOLARES POR HECTAREA/CICLO</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                            </div>
                            <div class="btn-group pull-right">
                                <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportPrint('datatable_dolares_hectarea_ciclo')"> Imprimir </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('datatable_dolares_hectarea_ciclo')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="col-md-5 col-sm-12 pull-right">
                            <ul style="list-style: none;">
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-red-thunderbird bg-font-red-thunderbird"></div> Más número de hec/ciclo que el año pasado</li>
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-yellow-gold bg-font-yellow-gold"></div> Mismo número de hec/ciclo que el año pasado</li>
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-green-haze bg-font-green-haze"></div> Menor número de hec/ciclo que el año pasado</li>
                            </ul>
                        </div>
                        <div class="table-container">
                            <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_dolares_hectarea_ciclo">
                                 <thead>
                                    <tr class="cursor">
                                        <th class="center-th {{ (tables.dolares_ha.orderBy == 'finca') ? ((tables.dolares_ha.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.dolares_ha.orderBy = 'finca'; tables.dolares_ha.reverse = !tables.dolares_ha.reverse;">FINCAS</th>
                                        <th class="center-th {{ (tables.dolares_ha.orderBy == value.years) ? ((tables.dolares_ha.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.dolares_ha.orderBy = value.years; tables.dolares_ha.reverse = !tables.dolares_ha.reverse;" ng-repeat="(key , value) in years">{{value.years}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="(llave , valor) in ciclos_aplicados_anio | orderObjectBy:tables.dolares_ha.orderBy:tables.dolares_ha.reverse">
                                        <td class="left-th">{{valor.finca | uppercase }}</td>
                                        <td class="center-th  {{getClassName(valor[(value.years-1)] , valor[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                            {{valor[value.years] | number : 2}}
                                        </td>
                                        <!--<td class="center-th"><span class="fa fa-arrow-up font-red-thunderbird"></span></td>
                                        <td class="center-th"><span class="fa fa-arrow-right font-yellow-gold"></span> </td>
                                        <td class="center-th"><span class="fa fa-arrow-down font-green-jungle"></span> </td>
                                        <td class="center-th"><span class="fa fa-arrow-down font-green-jungle"></span> </td>-->
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="center-th"></th>
                                        <th class="center-th {{getClassName(total_ciclos_aplicados_anio[(value.years-1)] , total_ciclos_aplicados_anio[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                            {{total_ciclos_aplicados_anio[value.years] | number: 2}}
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            */?>

            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">COMPARACION CICLOS POR AÑO</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="contenedor_ciclos_aplicacion"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">DOLARES POR HECTAREA/AÑO</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="contenedor_ciclos_aplicacion_sum"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">DOLARES POR HECTAREA/CICLO</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="contenedor_ciclos_aplicacion_avg"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12" id="frac">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">FRAC</span>
                        </div>
                        <div class="actions">
                            <!--<div class="col-md-12">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td class="center-th bg-green-jungle bg-font-green-jungle">Saldo</td>
                                            <td class="center-th bg-yellow-gold bg-font-yellow-gold">Sin Saldo</td>
                                            <td class="center-th bg-red-thunderbird bg-font-red-thunderbird">Pasado</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>-->
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="col-md-2 col-sm-12 pull-right">
                            <ul style="list-style: none;">
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-red-thunderbird bg-font-red-thunderbird"></div> Pasado</li>
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-yellow-gold bg-font-yellow-gold"></div> Sin Saldo</li>
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-green-haze bg-font-green-haze"></div> Saldo</li>
                            </ul>
                        </div>
                        <div class="table-scrollable">                                
                            <table class="table table-striped table-bordered table-hover" id="datatable_ciclos_aplicados">
                                 <thead>
                                    <tr>
                                        <th class="center-th" ng-repeat="(key , value) in table_frac[0]">{{key}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="valor in table_frac">
                                        <td class="center-th {{ semaforoSaldo(data) }}" ng-repeat="(campo, data) in valor">
                                            {{ (campo == 'finca') ? data : (data | number: 0) }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12" id="diasAtraso">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">DIAS DE ATRASO</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="col-md-3 col-sm-12 pull-right">
                            <ul style="list-style: none;">
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-red-thunderbird bg-font-red-thunderbird"></div> Días de atraso</li>
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-green-haze bg-font-green-haze"></div> Se aplico el día programado</li>
                                <li><div style="width:10px; height:10px; display: inline-block;" class="bg-yellow-gold bg-font-yellow-gold"></div> Días de adelanto</li>
                            </ul>
                        </div>
                        <div class="table-scrollable">                                
                            <table class="table table-striped table-bordered table-hover" id="datatable_ciclos_aplicados">
                                 <thead>
                                    <tr>
                                        <th class="center-th">FINCAS</th>
                                        <th class="center-th" ng-repeat="(key , value) in ciclos | orderObjectBy : 'ciclo'">{{value.ciclo}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="(llave , valor) in data_ciclos">
                                        <td class="left-th">{{valor.finca}}</td>
                                        <td class="center-th {{valor.className[value.ciclo]}}" ng-repeat="(key , value) in ciclos | orderObjectBy : 'ciclo'">
                                            {{valor.ciclos[value.ciclo]}}
                                        </td>
                                        <!--<td class="center-th"><span class="fa fa-arrow-up font-red-thunderbird"></span></td>
                                        <td class="center-th"><span class="fa fa-arrow-right font-yellow-gold"></span> </td>
                                        <td class="center-th"><span class="fa fa-arrow-down font-green-jungle"></span> </td>
                                        <td class="center-th"><span class="fa fa-arrow-down font-green-jungle"></span> </td>-->
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="center-th"></th>
                                        <th class="center-th" ng-repeat="data in years"></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12" id="motivosAtraso">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">MOTIVOS DE ATRASO</span>
                        </div>
                        <div class="actions">
                             <!--div class="btn-group pull-right">
                                <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                                    Fincas
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                <?php
                                    foreach ($data->fincas as $key => $value) {
                                ?>
                                    <li>
                                        <a href="#">
                                            <?=$value->label?>
                                        </a>
                                    </li>
                                <?php
                                    }
                                ?>
                            </div-->
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="contenedor_motivo" style="height:500px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>