<?php
    $cdn = "http://cdn.procesos-iq.com/";
?>
<style>
    .center-th {
        text-align: center;
    }
    .left-th {
        text-align: left !important;
    }
    .charts-500 {
        height:500px;
    }
    .cursor td, .cursor th{
        cursor: pointer;
    }
    .asc {
        background-position: right;
        background-repeat: no-repeat;
        padding-right: 30px;
        background-image : url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAATCAYAAAByUDbMAAAAZ0lEQVQ4y2NgGLKgquEuFxBPAGI2ahhWCsS/gDibUoO0gPgxEP8H4ttArEyuQYxAPBdqEAxPBImTY5gjEL9DM+wTENuQahAvEO9DMwiGdwAxOymGJQLxTyD+jgWDxCMZRsEoGAVoAADeemwtPcZI2wAAAABJRU5ErkJggg==');
    }
    .desc {
        background-position: right;
        background-repeat: no-repeat;
        padding-right: 30px;
        background-image : url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAATCAYAAAByUDbMAAAAZUlEQVQ4y2NgGAWjYBSggaqGu5FA/BOIv2PBIPFEUgxjB+IdQPwfC94HxLykus4GiD+hGfQOiB3J8SojEE9EM2wuSJzcsFMG4ttQgx4DsRalkZENxL+AuJQaMcsGxBOAmGvopk8AVz1sLZgg0bsAAAAASUVORK5CYII=');
    }
</style>
<link href="<?=$cdn?>/global/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper" ng-app="app">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" ng-controller="resumenCiclos">
        <!-- BEGIN PAGE HEAD-->
        <input type="hidden" id="ciclos_aplicados_chart" value='<?=json_encode($data->ciclos_aplicados_chart)?>'>
        <input type="hidden" id="ciclos_aplicados_sum_chart" value='<?=json_encode($data->ciclos_aplicados_sum_chart)?>'>
        <input type="hidden" id="ciclos_aplicados_avg_chart" value='<?=json_encode($data->ciclos_aplicados_avg_chart)?>'>
        <input type="hidden" id="motivo_input" value='<?=json_encode($data->motivo)?>'>
        <div class="page-head" ng-init="init()">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Resumen Ejecutivo</h1>
            </div>
            <!-- END PAGE TITLE -->
            <div class="page-toolbar">
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                        {{filters.gerenteName}}
                        <i class="fa fa-angle-down"></i>
                    </button>
                   <ul class="dropdown-menu pull-right" role="menu">
                        <li ng-click="setFilterGerente({id : '' , label : 'SIGAT DEMO'})">
                            <a href="#">
                                SIGAT DEMO
                            </a>
                        </li>
                        <li ng-repeat="(key , value) in gerentes" ng-click="setFilterGerente(value)">
                            <a href="#">
                                {{value.label}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                        HASTA {{filters.sem2}}
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <!-- style="height: 400px;overflow-y: scroll;" -->
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li ng-click="setFilterWeeks2({id : '' , label : 'SEM'})">
                            <a href="#">
                                SEMANAS
                            </a>
                        </li>
                        <li ng-repeat="(key , value) in semanas" ng-click="setFilterWeeks2(value)">
                            <a href="#">
                                {{value.label}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                        DESDE {{filters.sem}}
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li ng-click="setFilterWeeks({id : '' , label : 'SEM'})">
                            <a href="#">
                                SEMANAS
                            </a>
                        </li>
                        <li ng-repeat="(key , value) in semanas" ng-click="setFilterWeeks(value)">
                            <a href="#">
                                {{value.label}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                        {{filters.year}}
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li ng-repeat="(key , value) in years" ng-click="setFilterYears(value)">
                            <a href="#">
                                {{value.years}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                        {{ filters.variable == '' ? 'TODOS' : filters.variable }}
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li ng-click="setFilterVariable('')">
                            <a href="#">
                                TODOS
                            </a>
                        </li>
                        <li ng-click="setFilterVariable('CICLO')">
                            <a href="#">
                                CICLO
                            </a>
                        </li>
                        <li ng-click="setFilterVariable('PARCIAL')">
                            <a href="#">
                                PARCIAL
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                        {{ tipoHectarea[filters.tipoHectarea] }}
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li ng-click="setFilterHectarea('')">
                            <a href="#">
                                FUMIGACIÓN
                            </a>
                        </li>
                        <li ng-click="setFilterHectarea('h_produccion')">
                            <a href="#">
                                PRODUCCIÓN
                            </a>
                        </li>
                        <li ng-click="setFilterHectarea('h_neta')">
                            <a href="#">
                                NETA
                            </a>
                        </li>
                        <li ng-click="setFilterHectarea('h_banano')">
                            <a href="#">
                                BANANO
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <span class="active">Resumen</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div id="indicadores"></div>        
            </div>
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">CICLOS APLICADOS</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                            </div>
                            <div class="btn-group pull-right">
                                <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportPrint('datatable_ciclos_aplicados')"> Imprimir </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('datatable_ciclos_aplicados')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container"> 
                            <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ciclos_aplicados">
                                <thead>
                                    <tr class="cursor">
                                        <th class="center-th {{ (tables.ciclos.orderBy == 'finca') ? ((tables.ciclos.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.ciclos.orderBy='finca'; tables.ciclos.reverse=!tables.ciclos.reverse;">FINCAS</th>
                                        <th class="center-th {{ (tables.ciclos.orderBy == value.years) ? ((tables.ciclos.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.ciclos.orderBy=value.years; tables.ciclos.reverse=!tables.ciclos.reverse;" ng-repeat="(key , value) in years">{{value.years}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="valor in ciclos_aplicados | orderObjectBy:tables.ciclos.orderBy:tables.ciclos.reverse">
                                        <td class="left-th">{{valor.finca}}</td>
                                        <td class="center-th {{getClassName(valor[(value.years-1)] , valor[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                            {{ (filters.variable != 'CICLO') 
                                                ? (valor[value.years] | number: 2)
                                                : valor[value.years] }}
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="center-th"></th>
                                        <th class="center-th" ng-repeat="data in years"></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">COMPARACION CICLOS POR AÑO</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="contenedor_ciclos_aplicacion"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">DOLARES POR HECTAREA/AÑO</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                            </div>
                            <div class="btn-group pull-right">
                                <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportPrint('datatable_dolares_hectarea')"> Imprimir </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('datatable_dolares_hectarea')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container">                                
                            <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_dolares_hectarea">
                                <thead>
                                    <tr class="cursor">
                                        <th class="center-th {{ (tables.dolares.orderBy == 'finca') ? ((tables.dolares.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.dolares.orderBy = 'finca'; tables.dolares.reverse = !tables.dolares.reverse;">FINCAS</th>
                                        <th class="center-th {{ (tables.dolares.orderBy == value.years) ? ((tables.dolares.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.dolares.orderBy = value.years; tables.dolares.reverse = !tables.dolares.reverse;" ng-repeat="(key , value) in years">{{value.years}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="(llave , valor) in ciclos_aplicados_sum_anio | orderObjectBy:tables.dolares.orderBy:tables.dolares.reverse">
                                        <td class="left-th">{{valor.finca}}</td>
                                        <td class="center-th  {{getClassName(valor[(value.years-1)] , valor[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                            {{ valor[value.years] | number : 2 }}
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="center-th">TOTALES</th>
                                        <th class="center-th {{getClassName(total_ciclos_aplicados_sum_anio[(value.years-1)] , total_ciclos_aplicados_sum_anio[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                            {{ total_ciclos_aplicados_sum_anio[value.years] | number: 2 }}
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">DOLARES POR HECTAREA/AÑO</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="contenedor_ciclos_aplicacion_sum"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">DOLARES POR HECTAREA/CICLO</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                            </div>
                            <div class="btn-group pull-right">
                                <a class="btn blue btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportPrint('datatable_dolares_hectarea_ciclo')"> Imprimir </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('datatable_dolares_hectarea_ciclo')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container">                                
                            <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_dolares_hectarea_ciclo">
                                 <thead>
                                    <tr class="cursor">
                                        <th class="center-th {{ (tables.dolares_ha.orderBy == 'finca') ? ((tables.dolares_ha.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.dolares_ha.orderBy = 'finca'; tables.dolares_ha.reverse = !tables.dolares_ha.reverse;">FINCAS</th>
                                        <th class="center-th {{ (tables.dolares_ha.orderBy == value.years) ? ((tables.dolares_ha.reverse) ? 'desc' : 'asc') : '' }}" ng-click="tables.dolares_ha.orderBy = value.years; tables.dolares_ha.reverse = !tables.dolares_ha.reverse;" ng-repeat="(key , value) in years">{{value.years}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="(llave , valor) in ciclos_aplicados_anio | orderObjectBy:tables.dolares_ha.orderBy:tables.dolares_ha.reverse">
                                        <td class="left-th">{{valor.finca | uppercase }}</td>
                                        <td class="center-th  {{getClassName(valor[(value.years-1)] , valor[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                            {{valor[value.years] | number : 2}}
                                        </td>
                                        <!--<td class="center-th"><span class="fa fa-arrow-up font-red-thunderbird"></span></td>
                                        <td class="center-th"><span class="fa fa-arrow-right font-yellow-gold"></span> </td>
                                        <td class="center-th"><span class="fa fa-arrow-down font-green-jungle"></span> </td>
                                        <td class="center-th"><span class="fa fa-arrow-down font-green-jungle"></span> </td>-->
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="center-th"></th>
                                        <th class="center-th {{getClassName(total_ciclos_aplicados_anio[(value.years-1)] , total_ciclos_aplicados_anio[value.years] , $index)}}" ng-repeat="(key , value) in years">
                                            {{total_ciclos_aplicados_anio[value.years] | number: 2}}
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">DOLARES POR HECTAREA/CICLO</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="contenedor_ciclos_aplicacion_avg"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">FRAC</span>
                        </div>
                        <div class="actions">
                            <div class="col-md-12">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td class="center-th bg-green-jungle bg-font-green-jungle">Saldo</td>
                                            <td class="center-th bg-yellow-gold bg-font-yellow-gold">Sin Saldo</td>
                                            <td class="center-th bg-red-thunderbird bg-font-red-thunderbird">Pasado</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">                                
                            <table class="table table-striped table-bordered table-hover" id="datatable_ciclos_aplicados">
                                 <thead>
                                    <tr>
                                        <th class="center-th" ng-repeat="(key , value) in table_frac[0]">{{key}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="valor in table_frac">
                                        <td class="center-th {{ semaforoSaldo(data) }}" ng-repeat="(campo, data) in valor">
                                            {{ (campo == 'finca') ? data : (data | number: 0) }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">DIAS DE ATRASO</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">                                
                            <table class="table table-striped table-bordered table-hover" id="datatable_ciclos_aplicados">
                                 <thead>
                                    <tr>
                                        <th class="center-th">FINCAS</th>
                                        <th class="center-th" ng-repeat="(key , value) in ciclos | orderObjectBy : 'ciclo'">{{value.ciclo}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="(llave , valor) in data_ciclos">
                                        <td class="left-th">{{valor.finca}}</td>
                                        <td class="center-th {{valor.className[value.ciclo]}}" ng-repeat="(key , value) in ciclos | orderObjectBy : 'ciclo'">
                                                {{valor.ciclos[value.ciclo]}}
                                        </td>
                                        <!--<td class="center-th"><span class="fa fa-arrow-up font-red-thunderbird"></span></td>
                                        <td class="center-th"><span class="fa fa-arrow-right font-yellow-gold"></span> </td>
                                        <td class="center-th"><span class="fa fa-arrow-down font-green-jungle"></span> </td>
                                        <td class="center-th"><span class="fa fa-arrow-down font-green-jungle"></span> </td>-->
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="center-th"></th>
                                        <th class="center-th" ng-repeat="data in years"></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">MOTIVOS DE ATRASO</span>
                        </div>
                        <div class="actions">
                             <!--div class="btn-group pull-right">
                                <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                                    Fincas
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                <?php
                                    foreach ($data->fincas as $key => $value) {
                                ?>
                                    <li>
                                        <a href="#">
                                            <?=$value->label?>
                                        </a>
                                    </li>
                                <?php
                                    }
                                ?>
                            </div-->
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="contenedor_motivo" style="height:500px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>