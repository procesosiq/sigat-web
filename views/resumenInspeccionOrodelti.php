<style>
    td, th {
        text-align : center;
    }
</style>

<div ng-app="app" ng-controller="programa">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>
                    REPORTE SEMANAL DE CAMPO
                </h1>
            </div>
            <div class="page-toolbar">
                <div style="float:right" class="col-md-12">
                    <div class="col-md-6">
                        Año
                        <select class="form-control" id="anio"  ng-model="filters.anio" ng-change="index()">
                            <option ng-repeat="(key , value) in anios" ng-selected="filters.anio == value" value="{{value}}">{{value}}</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        Semana
                        <select class="form-control" id="semana"  ng-model="filters.semana" ng-change="init()">
                            <option ng-repeat="(key , value) in semanas" ng-selected="filters.semana == value" value="{{value}}">{{value}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        Gerente
                        <select class="form-control" id="gerente" ng-model="filters.gerente" ng-change="init()">
                            <option value="">Orodelti</option>
                            <option value="{{ value.id }}" ng-repeat="(key, value) in gerentes" ng-selected="filters.gerentes == value.id">{{ value.nombre }}</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        Finca
                        <select class="form-control" id="finca" ng-model="filters.finca" ng-change="init()">
                            <option value="{{ value.id }}" ng-repeat="(key, value) in fincas" ng-selected="filters.finca == value.id">{{ value.nombre }}</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="resumenCiclos">Inicio</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Listado</span>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject font-dark sbold uppercase">REPORTE</span>
                        </div>
                        <div class="actions">
                            <button class="btn green-haze" ng-click="excel()">
                                Excel
                            </button>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-hover table-stripped table-bordered" id="table">
                                <thead>
                                    <tr>
                                        <th colspan="13">Planta Joven</th>
                                        <th colspan="5">Planta 0 Semanas</th>
                                        <th colspan="5">Planta 6 Semanas</th>
                                        <th colspan="5">Planta 11 Semanas</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="2">LOTE</th>
                                        <th rowspan="2">TIPO</th>
                                        <!-- 3M -->
                                        <th rowspan="2">HT</th>
                                        <th colspan="2">HOJA 2</th>
                                        <th colspan="2">HOJA 3</th>
                                        <th colspan="2">HOJA 4</th>
                                        <th colspan="2">HVLE</th>
                                        <th colspan="2">HVLQ</th>
                                        <!-- 0 SEM -->
                                        <th rowspan="2">HT</th>
                                        <th colspan="2">HVLE</th>
                                        <th colspan="2">HVLQ</th>
                                        <!-- 6 SEM -->
                                        <th rowspan="2">HT</th>
                                        <th colspan="2">HVLE</th>
                                        <th colspan="2">HVLQ</th>
                                        <!-- 11 SEM -->
                                        <th rowspan="2">HT</th>
                                        <th colspan="2">HVLE</th>
                                        <th colspan="2">HVLQ</th>
                                    </tr>
                                    <tr>
                                        <!-- 3 M -->
                                        <th>I</th>
                                        <th>S</th>
                                        <th>I</th>
                                        <th>S</th>
                                        <th>I</th>
                                        <th>S</th>
                                        <th>H</th>
                                        <th>AFA</th>
                                        <th>H</th>
                                        <th>AFA</th>
                                        <!-- 0 SEM -->
                                        <th>H</th>
                                        <th>AFA</th>
                                        <th>H</th>
                                        <th>AFA</th>
                                        <!-- 6 SEM -->
                                        <th>H</th>
                                        <th>AFA</th>
                                        <th>H</th>
                                        <th>AFA</th>
                                        <!-- 11 SEM -->
                                        <th>H</th>
                                        <th>AFA</th>
                                        <th>H</th>
                                        <th>AFA</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in data">
                                        <td>{{ row.lote }}</td>
                                        <td>{{ row.tipo_finca }}</td>
                                        <!-- 3M -->
                                        <td>{{ row.ht_3m | number }}</td>
                                        <td>{{ row.h2_3m | number }}</td>
                                        <td>{{ row.h2s_3m | number }}</td>
                                        <td>{{ row.h3_3m | number }}</td>
                                        <td>{{ row.h3s_3m | number }}</td>
                                        <td>{{ row.h4_3m | number }}</td>
                                        <td>{{ row.h4s_3m | number }}</td>
                                        <td>{{ row.hvl_3m | number }}</td>
                                        <td>{{ row.hvl_afa_3m | number }}</td>
                                        <td>{{ row.hvlq_3m | number }}</td>
                                        <td>{{ row.hvlq_afa_3m | number }}</td>
                                        <!-- 0 S -->
                                        <td>{{ row.ht_0s | number }}</td>
                                        <td>{{ row.hvl_0s | number }}</td>
                                        <td>{{ row.hvl_afa_0s | number }}</td>
                                        <td>{{ row.hvlq_0s | number }}</td>
                                        <td>{{ row.hvlq_afa_0s | number }}</td>
                                        <!-- 6 S -->
                                        <td>{{ row.ht_6s | number }}</td>
                                        <td>{{ row.hvl_6s | number }}</td>
                                        <td>{{ row.hvl_afa_6s | number }}</td>
                                        <td>{{ row.hvlq_6s | number }}</td>
                                        <td>{{ row.hvlq_afa_6s | number }}</td>
                                        <!-- 11 S -->
                                        <td>{{ row.ht_11s | number }}</td>
                                        <td>{{ row.hvl_11s | number }}</td>
                                        <td>{{ row.hvl_afa_11s | number }}</td>
                                        <td>{{ row.hvlq_11s | number }}</td>
                                        <td>{{ row.hvlq_afa_11s | number }}</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Total</th>
                                        <th></th>
                                        
                                        <th>{{ data | sumOfValue : 'ht_3m' | number }}</th>
                                        <th>{{ data | sumOfValue : 'h2_3m' | number }}</th>
                                        <th>{{ data | sumOfValue : 'h2s_3m' | number }}</th>
                                        <th>{{ data | sumOfValue : 'h3_3m' | number }}</th>
                                        <th>{{ data | sumOfValue : 'h3s_3m' | number }}</th>
                                        <th>{{ data | sumOfValue : 'h4_3m' | number }}</th>
                                        <th>{{ data | sumOfValue : 'h4s_3m' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvl_3m' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvl_afa_3m' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvlq_3m' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvlq_afa_3m' | number }}</th>
                                        <!-- 0 S -->
                                        <th>{{ data | sumOfValue : 'ht_0s' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvl_0s' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvl_afa_0s' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvlq_0s' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvlq_afa_0s' | number }}</th>
                                        <!-- 6 S -->
                                        <th>{{ data | sumOfValue : 'ht_6s' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvl_6s' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvl_afa_6s' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvlq_6s' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvlq_afa_6s' | number }}</th>
                                        <!-- 11 S -->
                                        <th>{{ data | sumOfValue : 'ht_11s' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvl_11s' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvl_afa_11s' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvlq_11s' | number }}</th>
                                        <th>{{ data | sumOfValue : 'hvlq_afa_11s' | number }}</th>
                                    </tr>
                                    <tr>
                                        <th>Prom</th>
                                        <th></th>
                                        
                                        <th>{{ data | avgOfValue : 'ht_3m' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'h2_3m' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'h2s_3m' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'h3_3m' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'h3s_3m' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'h4_3m' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'h4s_3m' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvl_3m' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvl_afa_3m' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvlq_3m' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvlq_afa_3m' | number : 2 }}</th>
                                        <!-- 0 S -->
                                        <th>{{ data | avgOfValue : 'ht_0s' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvl_0s' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvl_afa_0s' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvlq_0s' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvlq_afa_0s' | number : 2 }}</th>
                                        <!-- 6 S -->
                                        <th>{{ data | avgOfValue : 'ht_6s' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvl_6s' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvl_afa_6s' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvlq_6s' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvlq_afa_6s' | number : 2 }}</th>
                                        <!-- 11 S -->
                                        <th>{{ data | avgOfValue : 'ht_11s' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvl_11s' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvl_afa_11s' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvlq_11s' | number : 2 }}</th>
                                        <th>{{ data | avgOfValue : 'hvlq_afa_11s' | number : 2 }}</th>
                                    </tr>
                                    <tr>
                                        <th>%</th>
                                        <th></th>
                                        <th></th>
                                        <th>{{ (data | countOfValue : 'h2_3m') > 0 ? ((data | countOfValue : 'h2_3m' | number : 2)/data.length*100 | number : 2)+'%' : '' }}</th>
                                        <th>{{ (data | countOfValue : 'h2s_3m') > 0 ? ((data | countOfValue : 'h2s_3m' | number : 2)/data.length*100 | number : 2)+'%' : '' }}</th>
                                        <th>{{ (data | countOfValue : 'h3_3m') > 0 ? ((data | countOfValue : 'h3_3m' | number : 2)/data.length*100 | number : 2)+'%' : '' }}</th>
                                        <th>{{ (data | countOfValue : 'h3s_3m') > 0 ? ((data | countOfValue : 'h3s_3m' | number : 2)/data.length*100 | number : 2)+'%' : '' }}</th>
                                        <th>{{ (data | countOfValue : 'h4_3m') > 0 ? ((data | countOfValue : 'h4_3m' | number : 2)/data.length*100 | number : 2)+'%' : '' }}</th>
                                        <th>{{ (data | countOfValue : 'h4s_3m') > 0 ? ((data | countOfValue : 'h4s_3m' | number : 2)/data.length*100 | number : 2)+'%' : '' }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>