<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    $cdn = "http://cdn.procesos-iq.com/";
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>

<style>
    .red-error {
        border : 2px red solid;
        border-radius : 1px;
    }
</style>
        
<div ng-app="app" ng-controller="tecnicos">
    <div class="page-content">
        <div class="page-head">
            <div class="page-title">
                <h1>REVISION REPORTE SEMANAL 
                    <small></small>
                </h1>
            </div>
        </div>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="resumenCiclos">Inicio</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Revisión</span>
            </li>
        </ul>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            
                        </div>
                        <div class="tools">
                            <button class="btn green-jungle" ng-click="save()">Guardar</button>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <form>
                                    <legend>Encabezado</legend>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Fecha Programación:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control {{ data.fecha_programada == '' ? 'red-error' : '' }}" data-provide="datepicker" data-date-format="yyyy-mm-dd" ng-model="data.fecha_programada">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Fecha Real:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control {{ data.fecha_real == '' ? 'red-error' : '' }}" data-provide="datepicker" data-date-format="yyyy-mm-dd" ng-model="data.fecha_real">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Hora:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" ng-model="data.hora">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Finca:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control {{ data.id_finca == '' ? 'red-error' : '' }}" ng-model="data.id_finca">
                                                <option value="{{ f.id }}" ng-repeat="f in fincas">{{ f.nombre }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Sector 1:</label>
                                        </div>
                                        <div class="col-md-5">
                                            <select class="form-control" ng-model="data.sectores[0].id_sector" ng-change="select_sector(1)">
                                                <option value="">N/A</option>
                                                <option value="{{s.id}}" ng-repeat="s in sectores | filter : { id_finca : data.id_finca }">{{s.sector}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            {{ data.sectores[0].hectareas }} HA
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Sector 2:</label>
                                        </div>
                                        <div class="col-md-5">
                                            <select class="form-control" ng-model="data.sectores[1].id_sector" ng-change="select_sector(2)">
                                                <option value="">N/A</option>
                                                <option value="{{s.id}}" ng-repeat="s in sectores | filter : { id_finca : data.id_finca }">{{s.sector}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            {{ data.sectores[1].hectareas }} HA
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Sector 3:</label>
                                        </div>
                                        <div class="col-md-5">
                                            <select class="form-control" ng-model="data.sectores[2].id_sector" ng-change="select_sector(3)">
                                                <option value="">N/A</option>
                                                <option value="{{s.id}}" ng-repeat="s in sectores | filter : { id_finca : data.id_finca }">{{s.sector}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            {{ data.sectores[2].hectareas }} HA
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Sector 4:</label>
                                        </div>
                                        <div class="col-md-5">
                                            <select class="form-control" ng-model="data.sectores[3].id_sector" ng-change="select_sector(4)">
                                                <option value="">N/A</option>
                                                <option value="{{s.id}}" ng-repeat="s in sectores | filter : { id_finca : data.id_finca }">{{s.sector}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            {{ data.sectores[3].hectareas }} HA
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Sector 5:</label>
                                        </div>
                                        <div class="col-md-5">
                                            <select class="form-control" ng-model="data.sectores[4].id_sector" ng-change="select_sector(5)">
                                                <option value="">N/A</option>
                                                <option value="{{s.id}}" 
                                                    ng-repeat="s in sectores | filter : { id_finca : data.id_finca }">{{s.sector}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            {{ data.sectores[4].hectareas }} HA
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Total Hectareas:</label>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" value="{{ data.total_hectareas | number: 2 }}" readonly>
                                        </div>
                                        <div class="col-md-3">
                                            {{ data.sector_ha_5 }} HA
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Programa:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" ng-model="data.programa">
                                                <option value="Sigatoka">Sigatoka</option>
                                                <option value="Sigatoka Plagas">Sigatoka Plagas</option>
                                                <option value="Sigatoka Foliar">Sigatoka Foliar</option>
                                                <option value="Plagas">Plagas</option>
                                                <option value="Foliar">Foliar</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Tipo de Ciclo:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="radio" ng-click="data.tipo_ciclo = 'Ciclo'">
                                                <span class="{{ data.tipo_ciclo == 'Ciclo' ? 'checked' : '' }}">
                                                    <input type="text" ng-model="data.tipo_ciclo" style="cursor: pointer;">
                                                </span>
                                            </div>
                                            Ciclo
                                            <div class="radio"  ng-click="data.tipo_ciclo = 'Parcial'">
                                                <span class="{{ data.tipo_ciclo == 'Parcial' ? 'checked' : '' }}">
                                                    <input type="text" ng-model="data.tipo_ciclo" style="cursor: pointer;">
                                                </span>
                                            </div>
                                            Parcial
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Ciclo #:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control {{ data.num_ciclo == '' ? 'red-error' : '' }}" ng-model="data.num_ciclo">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Fumigadora:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" ng-model="data.id_fumigadora">
                                                <option value="{{f.id}}" ng-repeat="f in fumigadoras.fumigadoras">{{f.nombre}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Galones:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" ng-model="data.galones">
                                                <option value="">SELECCIONE</option>
                                                <option value="{{f.galones}}" ng-repeat="f in fumigadoras.galones | filter : { id_fumigadora : data.id_fumigadora, id_finca : data.id_finca } : strict">{{f.galones}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Precio operación:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="number" ng-model="data.precio_operacion" class="form-control {{ data.precio_operacion == '' ? 'red-error' : '' }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Piloto:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" ng-model="data.id_piloto">
                                                <option value="{{p.id}}" ng-repeat="p in pilotos">{{p.nombre}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Placa avión:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" ng-model="data.id_placa">
                                                <option value="{{p.id}}" ng-repeat="p in placas">{{p.placa}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Hora de salida:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" ng-model="data.hora_salida">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Hora de llegada:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" ng-model="data.hora_llegada">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label class="control-label">Motivo de atraso:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" ng-model="data.motivo">
                                                <option value="">N/A</option>
                                                <option value="{{m.nombre}}" ng-repeat="m in motivos">{{m.nombre}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Fungicida 1</legend>

                                        <input type="hidden" ng-model="data.fungicidas[0].id_reporte_producto">

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label class="label-control">Proveedor</label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.fungicidas[0].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in fungicidas.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label class="label-control">Fungicida 1</label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.fungicidas[0].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{prod.id}}" 
                                                        ng-if="data.fungicidas[0].id_proveedor > 0" 
                                                        ng-repeat="prod in fungicidas.productos | filter: { id_proveedor : data.fungicidas[0].id_proveedor } : strict">
                                                        {{prod.nombre_comercial}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label class="label-control">Dosis</label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.fungicidas[0].dosis" id="fungicidas_dosis_0">
                                                    <option value="">N/A</option>
                                                    <option value="{{dosis.dosis}}"
                                                        ng-if="data.fungicidas[0].id_producto > 0" 
                                                        ng-selected="dosis.dosis == data.fungicidas[0].dosis"
                                                        ng-repeat="dosis in fungicidas.dosis | filter: { id_producto : data.fungicidas[0].id_producto } : strict">
                                                        {{dosis.dosis}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" ng-if="data.fungicidas[0].id_producto > 0 && data.fungicidas[0].dosis > 0">
                                            <div class="col-md-4">
                                                <label class="label-control">Cantidad</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly value="{{ data.fungicidas[0].cantidad = (data.fungicidas[0].dosis * data.hectareas_fumigacion) }}">
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>

                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Fungicida 2</legend>

                                        <input type="hidden" ng-model="data.fungicidas[1].id_reporte_producto">

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label class="label-control">Proveedor</label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.fungicidas[1].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in fungicidas.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label class="label-control">Fungicida 2</label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.fungicidas[1].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{prod.id}}" 
                                                        ng-if="data.fungicidas[1].id_proveedor > 0" 
                                                        ng-repeat="prod in fungicidas.productos | filter: { id_proveedor : data.fungicidas[0].id_proveedor } : strict">
                                                        {{prod.nombre_comercial}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label class="label-control">Dosis</label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.fungicidas[1].dosis" id="fungicidas_dosis_1">
                                                    <option value="">N/A</option>
                                                    <option value="{{dosis.dosis}}" 
                                                        ng-if="data.fungicidas[1].id_producto > 0" 
                                                        ng-repeat="dosis in fungicidas.dosis | filter: { id_producto : data.fungicidas[1].id_producto }  : strict">
                                                        {{dosis.dosis}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" ng-if="data.fungicidas[1].id_producto > 0 && data.fungicidas[1].dosis > 0">
                                            <div class="col-md-4">
                                                <label class="label-control">Cantidad</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly value="{{ data.fungicidas[1].cantidad = (data.fungicidas[1].dosis * data.hectareas_fumigacion) }}">
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>

                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Aceite y Agua</legend>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label class="label-control">Proveedor</label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.id_prov_aceite">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in aceites.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label class="label-control">Aceite</label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.id_aceite">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in aceites.productos | filter : { id_proveedor : data.id_prov_aceite }  : strict"
                                                        ng-if="data.id_prov_aceite > 0">
                                                        {{p.nombre_comercial}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label class="label-control">Dosis</label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.aceite_dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-selected="d.dosis == data.aceite_dosis"
                                                        ng-repeat="d in aceites.dosis | filter : { id_producto : data.id_aceite } : strict">
                                                        {{d.dosis}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label class="label-control">Agua</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="Agua" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label class="label-control">Dosis</label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.dosis_agua">
                                                    <option value="3"   ng-selected="data.dosis_agua == 3">3</option>
                                                    <option value="3.5" ng-selected="data.dosis_agua == 3.5">3.5</option>
                                                    <option value="4"   ng-selected="data.dosis_agua == 4">4</option>
                                                    <option value="4.5" ng-selected="data.dosis_agua == 4.5">4.5</option>
                                                    <option value="5"   ng-selected="data.dosis_agua == 5">5</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6" ng-repeat="(i, e) in [1,2,3,4,5,6,7]">
                                <fieldset>
                                    <form>
                                        <legend>Emulsificante {{e}}</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[i].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in emulsificantes.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Emulsificante
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[i].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in emulsificantes.productos | filter : { id_proveedor : data.emulsificantes[i].id_proveedor } : strict"
                                                        ng-if="data.emulsificantes[i].id_proveedor > 0">
                                                        {{p.nombre_comercial}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[i].dosis" id="emulsificantes_dosis_{{i}}">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}"
                                                        ng-selected="d.dosis == data.emulsificantes[i].dosis"
                                                        ng-repeat="d in emulsificantes.dosis | filter : { id_producto : data.emulsificantes[i].id_producto } : strict"
                                                        ng-if="data.emulsificantes[i].id_producto > 0">
                                                        {{d.dosis}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly value="{{ data.total_hectareas * data.emulsificantes[i].dosis | number: 2 }}">
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <!--<div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Emulsificante 1</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[0].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in emulsificantes.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Emulsificante
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[0].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in emulsificantes.productos | filter : { id_proveedor : data.emulsificantes[0].id_proveedor } : strict"
                                                        ng-if="data.emulsificantes[0].id_proveedor > 0">
                                                        {{p.nombre_comercial}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[0].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-selected="d.dosis == data.emulsificantes[0].dosis"
                                                        ng-repeat="d in emulsificantes.dosis | filter : { id_producto : data.emulsificantes[0].id_producto } : strict"
                                                        ng-if="data.emulsificantes[0].id_producto > 0">
                                                        {{d.dosis}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly value="{{ data.total_hectareas * data.emulsificantes[0].dosis | number: 2 }}">
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Emulsificante 2</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[1].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in emulsificantes.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Emulsificante
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[1].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in emulsificantes.productos | filter : { id_proveedor : data.emulsificantes[1].id_proveedor } : strict"
                                                        ng-if="data.emulsificantes[1].id_proveedor > 0">
                                                        {{p.nombre_comercial}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[1].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}"
                                                        ng-repeat="d in emulsificantes.dosis | filter : { id_producto : data.emulsificantes[1].id_producto } : strict"
                                                        ng-selected="d.dosis == data.emulsificantes[1].dosis"
                                                        ng-if="data.emulsificantes[1].id_producto > 0">
                                                        {{d.dosis}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly value="{{ data.total_hectareas * data.emulsificantes[1].dosis | number: 2 }}">
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Emulsificante 3</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[2].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in emulsificantes.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Emulsificante
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[2].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in emulsificantes.productos | filter : { id_proveedor : data.emulsificantes[2].id_proveedor } : strict"
                                                        ng-if="data.emulsificantes[2].id_proveedor > 0">
                                                        {{p.nombre_comercial}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[2].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-selected="d.dosis == data.emulsificantes[2].dosis"
                                                        ng-repeat="d in emulsificantes.dosis | filter : { id_producto : data.emulsificantes[2].id_producto } : strict"
                                                        ng-if="data.emulsificantes[2].id_producto > 0">
                                                        {{d.dosis}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly value="{{ data.total_hectareas * data.emulsificantes[2].dosis | number: 2 }}">
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Emulsificante 4</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[3].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in emulsificantes.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Emulsificante
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[3].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in emulsificantes.productos | filter : { id_proveedor : data.emulsificantes[3].id_proveedor } : strict"
                                                        ng-if="data.emulsificantes[3].id_proveedor > 0">
                                                        {{p.nombre_comercial}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[3].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in emulsificantes.dosis | filter : { id_producto : data.emulsificantes[3].id_producto } : strict"
                                                        ng-if="data.emulsificantes[3].id_producto > 0">
                                                        {{d.dosis}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly value="{{ data.total_hectareas * data.emulsificantes[3].dosis | number: 2 }}">
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Emulsificante 5</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[4].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in emulsificantes.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Emulsificante
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[4].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in emulsificantes.productos | filter : { id_proveedor : data.emulsificantes[4].id_proveedor } : strict"
                                                        ng-if="data.emulsificantes[4].id_proveedor > 0">
                                                        {{p.nombre_comercial}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[4].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in emulsificantes.dosis | filter : { id_producto : data.emulsificantes[4].id_producto } : strict"
                                                        ng-if="data.emulsificantes[4].id_producto > 0">
                                                        {{d.dosis}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly value="{{ data.total_hectareas * data.emulsificantes[4].dosis | number: 2 }}">
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Emulsificante 6</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[5].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in emulsificantes.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Emulsificante
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[5].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in emulsificantes.productos | filter : { id_proveedor : data.emulsificantes[5].id_proveedor } : strict"
                                                        ng-if="data.emulsificantes[5].id_proveedor > 0">
                                                        {{p.nombre_comercial}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[5].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in emulsificantes.dosis | filter : { id_producto : data.emulsificantes[5].id_producto } : strict"
                                                        ng-if="data.emulsificantes[5].id_producto > 0">
                                                        {{d.dosis}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly value="{{ data.total_hectareas * data.emulsificantes[5].dosis | number: 2 }}">
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Emulsificante 7</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[6].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in emulsificantes.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Emulsificante
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[6].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in emulsificantes.productos | filter : { id_proveedor : data.emulsificantes[6].id_proveedor } : strict"
                                                        ng-if="data.emulsificantes[6].id_proveedor > 0">
                                                        {{p.nombre_comercial}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.emulsificantes[6].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in emulsificantes.dosis | filter : { id_producto : data.emulsificantes[6].id_producto } : strict"
                                                        ng-if="data.emulsificantes[6].id_producto > 0">
                                                        {{d.dosis}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" readonly value="{{ data.total_hectareas * data.emulsificantes[6].dosis | number: 2 }}">
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>-->
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6" ng-repeat="(i, e) in [1,2,3,4,5,6,7]">
                                <fieldset>
                                    <form>
                                        <legend>Foliar {{e}}</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[i].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in foliares.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Foliar
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[i].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in foliares.productos | filter: { id_proveedor : data.foliares[i].id_proveedor } : strict"
                                                        ng-if="data.foliares[i].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[i].dosis" id="foliares_dosis_{{i}}">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in foliares.dosis | filter: { id_producto : data.foliares[i].id_producto } : strict"
                                                        ng-if="data.foliares[i].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.foliares[i].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <!--<div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Foliar 1</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[0].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in foliares.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Foliar
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[0].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in foliares.productos | filter: { id_proveedor : data.foliares[0].id_proveedor } : strict"
                                                        ng-if="data.foliares[0].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[0].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in foliares.dosis | filter: { id_producto : data.foliares[0].id_producto } : strict"
                                                        ng-if="data.foliares[0].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.foliares[0].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Foliar 2</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[1].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in foliares.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Foliar
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[1].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in foliares.productos | filter: { id_proveedor : data.foliares[1].id_proveedor } : strict"
                                                        ng-if="data.foliares[1].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[1].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in foliares.dosis | filter: { id_producto : data.foliares[1].id_producto } : strict"
                                                        ng-if="data.foliares[1].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.foliares[1].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Foliar 3</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[2].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in foliares.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Foliar
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[2].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in foliares.productos | filter: { id_proveedor : data.foliares[2].id_proveedor } : strict"
                                                        ng-if="data.foliares[2].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[2].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in foliares.dosis | filter: { id_producto : data.foliares[2].id_producto } : strict"
                                                        ng-if="data.foliares[2].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.foliares[2].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Foliar 4</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[3].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in foliares.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Foliar
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[3].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in foliares.productos | filter: { id_proveedor : data.foliares[3].id_proveedor } : strict"
                                                        ng-if="data.foliares[3].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[3].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in foliares.dosis | filter: { id_producto : data.foliares[3].id_producto } : strict"
                                                        ng-if="data.foliares[3].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.foliares[3].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Foliar 5</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[4].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in foliares.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Foliar
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[4].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in foliares.productos | filter: { id_proveedor : data.foliares[4].id_proveedor } : strict"
                                                        ng-if="data.foliares[4].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[4].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in foliares.dosis | filter: { id_producto : data.foliares[4].id_producto } : strict"
                                                        ng-if="data.foliares[4].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.foliares[4].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Foliar 6</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[5].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in foliares.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Foliar
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[5].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in foliares.productos | filter: { id_proveedor : data.foliares[5].id_proveedor } : strict"
                                                        ng-if="data.foliares[5].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[5].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in foliares.dosis | filter: { id_producto : data.foliares[5].id_producto } : strict"
                                                        ng-if="data.foliares[5].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.foliares[5].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Foliar 7</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[6].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in foliares.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Foliar
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[6].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in foliares.productos | filter: { id_proveedor : data.foliares[6].id_proveedor } : strict"
                                                        ng-if="data.foliares[6].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.foliares[6].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in foliares.dosis | filter: { id_producto : data.foliares[6].id_producto } : strict"
                                                        ng-if="data.foliares[6].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.foliares[6].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>-->
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6" ng-repeat="(i, e) in [1,2,3,4,5,6,7]">
                                <fieldset>
                                    <form>
                                        <legend>Control Erwinia {{e}}</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[i].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in erwinias.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Control Erwinia
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[i].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in erwinias.productos | filter: { id_proveedor : data.erwinias[i].id_proveedor } : strict"
                                                        ng-if="data.erwinias[i].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[i].dosis" id="erwinias_dosis_{{i}}">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in erwinias.dosis | filter: { id_producto : data.erwinias[i].id_producto } : strict"
                                                        ng-if="data.erwinias[i].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.erwinias[i].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <!--<div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Control Erwinia 1</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[0].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in erwinias.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Control Erwinia
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[0].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in erwinias.productos | filter: { id_proveedor : data.erwinias[0].id_proveedor } : strict"
                                                        ng-if="data.erwinias[0].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[0].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in erwinias.dosis | filter: { id_producto : data.erwinias[0].id_producto } : strict"
                                                        ng-if="data.erwinias[0].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.erwinias[0].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Control Erwinia 2</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[1].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in erwinias.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Control Erwinia
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[1].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in erwinias.productos | filter: { id_proveedor : data.erwinias[1].id_proveedor } : strict"
                                                        ng-if="data.erwinias[1].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[1].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in erwinias.dosis | filter: { id_producto : data.erwinias[1].id_producto } : strict"
                                                        ng-if="data.erwinias[1].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.erwinias[1].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Control Erwinia 3</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[2].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in erwinias.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Control Erwinia
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[2].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in erwinias.productos | filter: { id_proveedor : data.erwinias[2].id_proveedor } : strict"
                                                        ng-if="data.erwinias[2].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[2].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in erwinias.dosis | filter: { id_producto : data.erwinias[2].id_producto } : strict"
                                                        ng-if="data.erwinias[2].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.erwinias[2].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Control Erwinia 4</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[3].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in erwinias.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Control Erwinia
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[3].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in erwinias.productos | filter: { id_proveedor : data.erwinias[3].id_proveedor } : strict"
                                                        ng-if="data.erwinias[3].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[3].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in erwinias.dosis | filter: { id_producto : data.erwinias[3].id_producto } : strict"
                                                        ng-if="data.erwinias[3].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.erwinias[3].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Control Erwinia 5</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[4].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in erwinias.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Control Erwinia
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[4].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in erwinias.productos | filter: { id_proveedor : data.erwinias[4].id_proveedor } : strict"
                                                        ng-if="data.erwinias[4].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[4].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in erwinias.dosis | filter: { id_producto : data.erwinias[4].id_producto } : strict"
                                                        ng-if="data.erwinias[4].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.erwinias[4].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Control Erwinia 6</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[5].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in erwinias.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Control Erwinia
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[5].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in erwinias.productos | filter: { id_proveedor : data.erwinias[5].id_proveedor } : strict"
                                                        ng-if="data.erwinias[5].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[5].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in erwinias.dosis | filter: { id_producto : data.erwinias[5].id_producto } : strict"
                                                        ng-if="data.erwinias[5].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.erwinias[5].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Control Erwinia 7</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[6].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in erwinias.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Control Erwinia
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[6].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in erwinias.productos | filter: { id_proveedor : data.erwinias[6].id_proveedor } : strict"
                                                        ng-if="data.erwinias[6].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.erwinias[6].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in erwinias.dosis | filter: { id_producto : data.erwinias[6].id_producto } : strict"
                                                        ng-if="data.erwinias[6].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.erwinias[6].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>-->
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6" ng-repeat="(i, e) in [1,2,3,4,5,6,7]">
                                <fieldset>
                                    <form>
                                        <legend>Otro Sigatoka {{e}}</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[i].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in sigatokas.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Otro Sigatoka
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[i].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in sigatokas.productos | filter: { id_proveedor : data.sigatokas[i].id_proveedor } : strict"
                                                        ng-if="data.sigatokas[i].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[i].dosis" id="sigatokas_dosis_{{i}}">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in sigatokas.dosis | filter: { id_producto : data.sigatokas[i].id_producto } : strict"
                                                        ng-if="data.sigatokas[i].id_producto > 0"
                                                        ng-selected="data.sigatokas[i].dosis == d.dosis">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.sigatokas[i].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <!--<div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Otro Sigatoka 1</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[0].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in sigatokas.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Otro Sigatoka
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[0].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in sigatokas.productos | filter: { id_proveedor : data.sigatokas[0].id_proveedor } : strict"
                                                        ng-if="data.sigatokas[0].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[0].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in sigatokas.dosis | filter: { id_producto : data.sigatokas[0].id_producto } : strict"
                                                        ng-if="data.sigatokas[0].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.sigatokas[0].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Otro Sigatoka 2</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[1].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in sigatokas.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Otro Sigatoka
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[1].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in sigatokas.productos | filter: { id_proveedor : data.sigatokas[1].id_proveedor } : strict"
                                                        ng-if="data.sigatokas[1].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[1].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in sigatokas.dosis | filter: { id_producto : data.sigatokas[1].id_producto } : strict"
                                                        ng-if="data.sigatokas[1].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.sigatokas[1].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Otro Sigatoka 3</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[2].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in sigatokas.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Otro Sigatoka
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[2].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in sigatokas.productos | filter: { id_proveedor : data.sigatokas[2].id_proveedor } : strict"
                                                        ng-if="data.sigatokas[2].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[2].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in sigatokas.dosis | filter: { id_producto : data.sigatokas[2].id_producto } : strict"
                                                        ng-if="data.sigatokas[2].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.sigatokas[2].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Otro Sigatoka 4</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[3].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in sigatokas.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Otro Sigatoka
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[3].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in sigatokas.productos | filter: { id_proveedor : data.sigatokas[3].id_proveedor } : strict"
                                                        ng-if="data.sigatokas[3].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[3].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in sigatokas.dosis | filter: { id_producto : data.sigatokas[3].id_producto } : strict"
                                                        ng-if="data.sigatokas[3].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.sigatokas[3].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Otro Sigatoka 5</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[4].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in sigatokas.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Otro Sigatoka
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[4].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in sigatokas.productos | filter: { id_proveedor : data.sigatokas[4].id_proveedor } : strict"
                                                        ng-if="data.sigatokas[4].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[4].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in sigatokas.dosis | filter: { id_producto : data.sigatokas[4].id_producto } : strict"
                                                        ng-if="data.sigatokas[4].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.sigatokas[4].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Otro Sigatoka 6</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[5].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in sigatokas.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Otro Sigatoka
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[5].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in sigatokas.productos | filter: { id_proveedor : data.sigatokas[5].id_proveedor } : strict"
                                                        ng-if="data.sigatokas[5].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[5].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in sigatokas.dosis | filter: { id_producto : data.sigatokas[5].id_producto } : strict"
                                                        ng-if="data.sigatokas[5].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.sigatokas[5].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Otro Sigatoka 7</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[6].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in sigatokas.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Otro Sigatoka
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[6].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}" 
                                                        ng-repeat="p in sigatokas.productos | filter: { id_proveedor : data.sigatokas[6].id_proveedor } : strict"
                                                        ng-if="data.sigatokas[6].id_proveedor > 0">
                                                        {{ p.nombre_comercial }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.sigatokas[6].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}" 
                                                        ng-repeat="d in sigatokas.dosis | filter: { id_producto : data.sigatokas[6].id_producto } : strict"
                                                        ng-if="data.sigatokas[6].id_producto > 0">
                                                        {{ d.dosis }}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.sigatokas[6].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>-->
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6" ng-repeat="(i, e) in [1,2]">
                                <fieldset>
                                    <form>
                                        <legend>Insecticida {{e}}</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.insecticidas[i].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in insecticidas.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Insecticida
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.insecticidas[i].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}"
                                                        ng-repeat="p in insecticidas.productos | filter: { id_proveedor: data.insecticidas[i].id_proveedor } : strict"
                                                        ng-if="data.insecticidas[i].id_proveedor > 0">
                                                        {{p.nombre_comercial}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.insecticidas[i].dosis" id="insecticidas_dosis_{{i}}">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}"
                                                        ng-repeat="d in insecticidas.dosis | filter: { id_producto: data.insecticidas[i].id_producto } : strict"
                                                        ng-if="data.insecticidas[i].id_producto > 0">
                                                        {{d.dosis}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.insecticidas[i].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <!--<div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Insecticida 1</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.insecticidas[0].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in insecticidas.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Insecticida
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.insecticidas[0].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}"
                                                        ng-repeat="p in insecticidas.productos | filter: { id_proveedor: data.insecticidas[0].id_proveedor } : strict"
                                                        ng-if="data.insecticidas[0].id_proveedor > 0">
                                                        {{p.nombre_comercial}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.insecticidas[0].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}"
                                                        ng-repeat="d in insecticidas.dosis | filter: { id_producto: data.insecticidas[0].id_producto } : strict"
                                                        ng-if="data.insecticidas[0].id_producto > 0">
                                                        {{d.dosis}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.insecticidas[0].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <form>
                                        <legend>Insecticida 2</legend>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Proveedores
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.insecticidas[1].id_proveedor">
                                                    <option value="">N/A</option>
                                                    <option value="{{key}}" ng-repeat="(key, value) in insecticidas.proveedores">{{value}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Insecticida
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.insecticidas[1].id_producto">
                                                    <option value="">N/A</option>
                                                    <option value="{{p.id}}"
                                                        ng-repeat="p in insecticidas.productos | filter: { id_proveedor: data.insecticidas[1].id_proveedor } : strict"
                                                        ng-if="data.insecticidas[1].id_proveedor > 0">
                                                        {{p.nombre_comercial}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Dosis
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <select class="form-control" ng-model="data.insecticidas[1].dosis">
                                                    <option value="">N/A</option>
                                                    <option value="{{d.dosis}}"
                                                        ng-repeat="d in insecticidas.dosis | filter: { id_producto: data.insecticidas[1].id_producto } : strict"
                                                        ng-if="data.insecticidas[1].id_producto > 0">
                                                        {{d.dosis}}
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="control-label">
                                                    Cantidad
                                                </label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" value="{{ data.total_hectareas * data.insecticidas[1].dosis | number: 2 }}" readonly>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>