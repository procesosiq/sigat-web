<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    $cdn = "http://cdn.procesos-iq.com/";
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<style>
    .cursor {
        cursor: pointer;
    }
    td, th {
        min-width : 100px;
        text-align : center;
    }

    /* OCULTAR COLUMNAS */
.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.labelFilters {
    width: 150px;
    padding-top: 9px;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none !important;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
</style>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=$cdn?>/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?=$cdn?>/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
    <div ng-app="app" ng-controller="tecnicos">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>REPORTE SEMANAL 
                        <small></small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="resumenCiclos">Inicio</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">Semanal</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <!-- Begin: life time stats -->
                    <div class="portlet light portlet-fit portlet-datatable bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                
                            </div>
                            <div class="actions col-md-12 col-sm-12">
                                <div class="col-md-3">
                                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                                        <a class="btn dark btn-outline btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                            <i class="glyphicon glyphicon-th icon-th"></i>
                                        </a>
                                        <ul class="dropdown-menu main pull-left" id="columns_table_1">
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 1, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        ID
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 2, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        FINCA
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 3, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        SECTOR
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 4, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        HA
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 5, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        fECHA REAL
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 6, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        ATRASO
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 7, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        FUNGICIDA 1
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 8, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        DOSIS 1
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 9, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        FUNGICIDA 2
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 10, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        DOSIS 2
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 11, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        BIOESTIMULANTES
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 12, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        DOSIS 3
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 13, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        COADYUVANTE
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 14, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        DOSIS 4
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 15, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        ACEITE
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 16, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        DOSIS 5
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 17, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        AGUA
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(1, 18, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        DOSIS 6
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <div class="btn-group btn-group-devided" data-toggle="buttons" style="float:right;">
                                        <div class="btn-group" style="float: right;">
                                            <button class="btn btn-primary dropdown-toggle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                                Exportar
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="javascript:;" ng-click="exportExcel('export', 'Información General')">Excel</a></li>
                                                <li><a href="javascript:;" ng-click="exportPdf('export')">PDF</a></li>
                                                <li><a href="javascript:;">Imprimir</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-group pull-right">
                                    <button type="button" style="height: 36px;" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                                        HASTA SEM {{filters.sem2}}
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <!-- style="height: 400px;overflow-y: scroll;" -->
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li ng-click="setFilterWeeks2('')">
                                            <a href="#">
                                                SEMANAS
                                            </a>
                                        </li>
                                        <li ng-repeat="(key , value) in semanas" ng-click="setFilterWeeks2(value)">
                                            <a href="#">
                                                {{value}}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="btn-group pull-right">
                                    <button type="button" style="height: 36px;" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                                        DESDE SEM {{filters.sem}}
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li ng-click="setFilterWeeks('')">
                                            <a href="#">
                                                SEMANAS
                                            </a>
                                        </li>
                                        <li ng-repeat="(key , value) in semanas" ng-click="setFilterWeeks(value)">
                                            <a href="#">
                                                {{value}}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-container table-scrollable">
                                <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_1">
                                    <thead>
                                        <tr class="cursor">
                                            <th width="5%" class="selected sorting_asc" id="id_column" ng-click="changeSort('id', 'search')"> ID </th>
                                            <th width="15%" id="finca_column" ng-click="changeSort('finca', 'search')"> FINCA </th>
                                            <th width="25%" id="sector_column" ng-click="changeSort('sector', 'search')"> SECTOR </th>
                                            <th width="10%" id="ha_column" ng-click="changeSort('ha', 'search')"> HA </th>
                                            <th width="25%" id="fecha_real_column" ng-click="changeSort('fecha_real', 'search')"> FECHA REAL </th>
                                            <th width="15%" id="atraso_column" ng-click="changeSort('atraso', 'search')"> ATRASO </th>
                                            <th width="15%" id="fungicida_1_column" ng-click="changeSort('fungicidad_1', 'search')"> FUNGICIDA 1 </th>
                                            <th width="15%" id="dosis_1_column" ng-click="changeSort('dosis_1', 'search')"> DOSIS 1 </th>
                                            <th width="15%" id="fungicida_2_column" ng-click="changeSort('fungicidad_2', 'search')"> FUNGICIDA 2 </th>
                                            <th width="15%" id="dosis_2_column" ng-click="changeSort('dosis_2', 'search')"> DOSIS 2 </th>
                                            <th width="15%" id="bioestimulantes_column" ng-click="changeSort('bioestimulantes', 'search')"> BIOESTIMULANTES </th>
                                            <th width="15%" id="dosis_3_column" ng-click="changeSort('dosis_3', 'search')"> DOSIS 3 </th>
                                            <th width="15%" id="coadyuvante_column" ng-click="changeSort('coadyuvante', 'search')"> COADYUVANTE </th>
                                            <th width="15%" id="dosis_4_column" ng-click="changeSort('dosis_4', 'search')"> DOSIS 4 </th>
                                            <th width="15%" id="aceite_column" ng-click="changeSort('aceite', 'search')"> ACEITE </th>
                                            <th width="15%" id="dosis_5_column" ng-click="changeSort('dosis_5', 'search')"> DOSIS 5 </th>
                                            <th width="15%" id="agua_column" ng-click="changeSort('agua', 'search')"> AGUA </th>
                                            <th width="15%" id="dosis_6_column" ng-click="changeSort('dosis_6', 'search')"> DOSIS 6 </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in table1 | orderObjectBy : search.orderBy : search.reverse">
                                            <td>{{ row.id }}</td>
                                            <td>{{ row.finca }}</td>
                                            <td>{{ row.sector }}</td>
                                            <td>{{ row.ha }}</td>                                            
                                            <td>{{ row.fecha_real }}</td>
                                            <td>{{ row.atraso }}</td>
                                            <td>{{ row.fungicidad_1 }}</td>
                                            <td>{{ row.dosis_1 | number }}</td>
                                            <td>{{ row.fungicidad_2 }}</td>
                                            <td>{{ row.dosis_2 | number }}</td>
                                            <td>{{ row.bioestimulantes }}</td>
                                            <td>{{ row.dosis_3 | number }}</td>
                                            <td>{{ row.coadyuvante }}</td>
                                            <td>{{ row.dosis_4 | number }}</td>
                                            <td>{{ row.aceite }}</td>
                                            <td>{{ row.dosis_5 | number }}</td>
                                            <td>{{ row.agua }}</td>
                                            <td>{{ row.dosis_6 | number }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End: life time stats -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- Begin: life time stats -->
                    <div class="portlet light portlet-fit portlet-datatable bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <!--<i class="icon-settings font-dark"></i>
                                <span class="caption-subject font-dark sbold uppercase">Listado de Tecnicos</span>-->
                            </div>
                            <div class="actions col-md-12 col-sm-12">
                                <div style="float:left;">
                                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                                        <a class="btn dark btn-outline btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                            <i class="glyphicon glyphicon-th icon-th"></i>
                                        </a>
                                        <ul class="dropdown-menu main pull-left" id="columns_table_2">
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(2, 1, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        ID
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(2, 2, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        FINCA
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(2, 3, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        FUMIGADORA
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(2, 4, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        PILOTO
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(2, 5, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        PLACA
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(2, 6, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        HORA SALIDA
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(2, 7, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        HORA LLEGADA
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(2, 8, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        MOTIVO ATRASO
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(2, 9, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        NOTIFICADA
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(2, 10, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        SECTOR APLICADO
                                                    </label>
                                                </div>
                                            </li>
                                            <li class="active">
                                                <div class="checkbox active">
                                                    <label ng-click="disableColumns(2, 11, $event)" class="active">
                                                        <input type="checkbox" value="" checked="checked">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        OBSERVACIONES
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-container table-scrollable">
                                <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_2">
                                    <thead>
                                        <tr class="cursor">
                                            <th width="5%" id="id_column" ng-click="changeSort('id', 'search2')"> ID </th>
                                            <th width="15%" id="finca_column" ng-click="changeSort('finca', 'search2')"> FINCA </th>
                                            <th width="25%" id="fumigadora_column" ng-click="changeSort('fumigadora', 'search2')"> FUMIGADORA </th>
                                            <th width="10%" id="piloto_column" ng-click="changeSort('piloto', 'search2')"> PILOTO </th>
                                            <th width="25%" id="placa_column" ng-click="changeSort('placa', 'search2')"> PLACA </th>
                                            <th width="15%" id="hora_salida_column" ng-click="changeSort('hora_salida', 'search2')"> HORA SALIDA </th>
                                            <th width="15%" id="hora_llegada_column" ng-click="changeSort('hora_llegada', 'search2')"> HORA LLEGADA </th>
                                            <th width="15%" id="motivo_atraso_column" ng-click="changeSort('motivo_atraso', 'search2')"> MOTIVO ATRASO </th>
                                            <th width="15%" id="notificada_column" ng-click="changeSort('notificada', 'search2')"> NOTIFICADA </th>
                                            <th width="15%" id="sector_aplicado_column" ng-click="changeSort('sector_aplicado', 'search2')"> SECTOR APLICADO </th>
                                            <th width="15%" id="observaciones_column" ng-click="changeSort('observaciones', 'search2')"> OBSERVACIONES </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in table2 | orderObjectBy : search2.orderBy : search2.reverse">
                                            <td>{{ row.id }}</td>
                                            <td>{{ row.finca }}</td>
                                            <td>{{ row.fumigadora }}</td>
                                            <td>{{ row.piloto }}</td>
                                            <td>{{ row.placa }}</td>
                                            <td>{{ row.hora_salida }}</td>
                                            <td>{{ row.hora_llegada }}</td>
                                            <td>{{ row.motivo_atraso }}</td>
                                            <td>{{ row.notificada }}</td>
                                            <td>{{ row.sector_aplicado }}</td>
                                            <td>{{ row.observaciones }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="portlet-body hide">
                            <div class="table-container table-scrollable">
                                <table class="table table-striped table-bordered table-hover table-checkable" id="export">
                                    <thead>
                                        <tr class="cursor">
                                            <th ng-repeat="(field, value) in table_export[0]">{{ field | uppercase }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in table_export">
                                            <td ng-repeat="(field, value) in row">{{ value }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End: life time stats -->
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>